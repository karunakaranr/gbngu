import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ICostCenter } from '../../models/ICostCenter';
import { CostCenterService } from '../../services/CostCenter/CostCenter.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import * as CostCenterjson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CostCenter.json'
import * as CostCenterjson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CostCenter.json'
import { URLS } from '../../URLS/urls';
// import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';


@Component({
  selector: "app-CostCenter",
  templateUrl: "./CostCenter.component.html",
  styleUrls: ["./CostCenter.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'CostCenterId' },
    { provide: 'url', useValue: URLS.CostCenter },
    { provide: 'DataService', useClass: CostCenterService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class CostCenterComponent extends GBBaseDataPageComponentWN<ICostCenter> {
  title: string = "CostCenter"
  CostCenterjson = CostCenterjson;



  form: GBDataFormGroupWN<ICostCenter> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'CostCenter', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.CostCenterFillFunction(arrayOfValues.CostCenterId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


//   public confirmBox():void {

//     if(this.form.get('CostCenterTypeName').value == "") {

//   const dialogRef = this.dialog.open(DialogBoxComponent, {
//     width: '400px',
//     data: {
//       message: 'Please Select the Cost Center Type',
//       heading: 'Info',
//     }
//   })
//   }
// }
    
    

  public CostCenterFillFunction(SelectedPicklistData: string): void {

  console.log("filling", SelectedPicklistData)
    if(SelectedPicklistData) {
      console.log("fillingifff", SelectedPicklistData)
    this.gbps.dataService.getData(SelectedPicklistData).subscribe(CostCenter => {
      this.form.patchValue(CostCenter);

      console.log("fillingallllf", SelectedPicklistData)
    })
  }
}


  public CostCenterPatchValue(SelectedPicklistDatas: any): void {

  this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>", SelectedPicklistDatas)
}
}



export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICostCenter> {
  const dbds: GBBaseDBDataService<ICostCenter> = new GBBaseDBDataService<ICostCenter>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ICostCenter>, dbDataService: GBBaseDBDataService<ICostCenter>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICostCenter> {
  return new GBDataPageService<ICostCenter>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
