import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { NarrationMasterService } from '../../services/NarrationMaster/NarrationMaster.service';
import { INarrationMaster } from '../../models/INarrationMaster';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as NarrationMasterJSON  from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/NarrationMaster.json'
import{URLS} from '../../URLS/urls'


@Component({
  selector: 'app-NarrationMaster',
  templateUrl: './NarrationMaster.component.html',
  styleUrls: ['./NarrationMaster.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'NarrationId'},
    { provide: 'url', useValue: URLS.Narration },
    { provide: 'DataService', useClass: NarrationMasterService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class NarrationMasterComponent extends GBBaseDataPageComponentWN<INarrationMaster > {
  title: string = "NarrationMaster"
  NarrationMasterJSON = NarrationMasterJSON;


  form: GBDataFormGroupWN<INarrationMaster > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'NarrationMaster', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.NarrationMasterFillFunction(arrayOfValues.NarrationId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public NarrationMasterFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public NarrationMasterPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<INarrationMaster > {
  const dbds: GBBaseDBDataService<INarrationMaster > = new GBBaseDBDataService<INarrationMaster >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<INarrationMaster >, dbDataService: GBBaseDBDataService<INarrationMaster >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<INarrationMaster > {
  return new GBDataPageService<INarrationMaster >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
