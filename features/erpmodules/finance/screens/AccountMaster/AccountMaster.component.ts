import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';

import { Store } from '@ngxs/store';

import { IAccountMaster } from '../../models/IAccountMaster';
import { AccountMasterService } from '../../services/AccountMaster/AccountMaster.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';


import * as AccountMasterJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/AccountMaster.json'

import { URLS } from './../../URLS/urls'
import { HRMSURLS } from 'features/erpmodules/hrms/URLS/urls';


@Component({
  selector: "app-AccountMaster",
  templateUrl: "./AccountMaster.component.html",
  styleUrls: ["./AccountMaster.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'AccountId' },
    { provide: 'url', useValue: URLS.Account },
    { provide: 'DataService', useClass: AccountMasterService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class AccountMasterComponent extends GBBaseDataPageComponentWN<IAccountMaster> {
  title: string = "AccountMaster"
  AccountMasterJson = AccountMasterJson;



  form: GBDataFormGroupWN<IAccountMaster> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'AccountMaster', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.AccountMasterFillFunction(arrayOfValues.AccountId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }



  public addnew(templateRef) {
    let dialogRef = this.dialog.open(templateRef, {
      width: '600px'
    });
  }

  savedata
  public BankRetrival(formdata): void {
    if (formdata) {
      this.savedata = formdata
    
    }
  }

  public newsave(){
    console.log("SAVEEE",this.savedata)
    let title = 'Bank';
    let url = HRMSURLS.GCM
    this.formaction.savemodel(this.savedata,title,url)
    this.dialog.closeAll()
  }


  public AccountMasterFillFunction(SelectedPicklistData: string): void {
  if(SelectedPicklistData) {
    this.gbps.dataService.getData(SelectedPicklistData).subscribe(AccountMaster => {
      this.form.patchValue(AccountMaster);
    })
  }
}

  
  public AccountMasterPatchValue(SelectedPicklistDatas: any): void {
  this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
}

  public ChangeTab(event, TabName: string) : void {
  var i: number, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for(i = 0; i <tabcontent.length; i++) {
  tabcontent[i].style.display = "none";
}
tablinks = document.getElementsByClassName("FormTabLinks");
for (i = 0; i < tablinks.length; i++) {
  tablinks[i].className = tablinks[i].className.replace(" active", "");
}
document.getElementById(TabName).style.display = "block";
console.log("Event:", event)
event.currentTarget.className += " active";
  }
}



export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IAccountMaster> {
  const dbds: GBBaseDBDataService<IAccountMaster> = new GBBaseDBDataService<IAccountMaster>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IAccountMaster>, dbDataService: GBBaseDBDataService<IAccountMaster>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IAccountMaster> {
  return new GBDataPageService<IAccountMaster>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
