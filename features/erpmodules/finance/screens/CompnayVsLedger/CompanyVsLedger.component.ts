import { Component, OnInit } from '@angular/core';
import { CompanyVsLedger } from '../../services/CompanyVsLedger/CompanyVsLedger.service';

@Component({
  selector: 'app-CompanyVsLedger',
  templateUrl: './CompanyVsLedger.component.html',
  styleUrls: ['./CompanyVsLedger.component.scss']
})

export class CompanyVsLedgerComponent implements OnInit {
  title = 'Company Vs Ledger'
  public rowData = []

  constructor(public service: CompanyVsLedger) { }
  ngOnInit(): void {
    this.companyvsledger();
  }
  public companyvsledger() {
    this.service.companyvsledgerview().subscribe(menudetails => {
      let servicejsondetail = menudetails
      this.service.Rowservice(servicejsondetail).subscribe(res => {
        this.rowData = res.ReportDetail;
        console.log("dkd", res);

        let json = this.rowData;
          var finalizedArray = []
          json.map((row,index) => {
          finalizedArray.push({
            sno:index,
            nature: row['AccountScheduleNatureName'],
            grpcode: decodeURIComponent(JSON.parse('"' + row['AccountGroupCode'].replace(/\"/g, '\\"') + '"')),
            grpname: decodeURIComponent(JSON.parse('"' + row['AccountGroupName'].replace(/\"/g, '\\"') + '"')),
            schcode: row['AccountScheduleCode'],
            schname: row['AccountScheduleName'],
            ctrlcode: row['ControlAccountCode'],
            ctrlname: row['ControlAccountName'],
            code: row['AccountCode'],
            name: row['AccountName'],
            shortname: row['AccountShortName'],
            type: row['AccountTypeName'],
            curr: row['CurrencyCode'],
          })
        })
        console.log("finalizedArray:",finalizedArray);
        const final = {};
        finalizedArray.forEach(detail => {
          final[detail.nature] = {
            accountGroup1 :{},
            code:detail.nature,
            name:"",
            shortname:"",
            type:"",
            curr:"",
            ...final[detail.nature], 
          }

          final[detail.nature].accountGroup1[detail.schcode] =  {
            accountGroup2: {},
            code: detail.schcode,
            name: detail.schname,
            shortname: "",
            type: "",
            curr: "",
            ...final[detail.nature].accountGroup1[detail.schcode],
          }
          final[detail.nature].accountGroup1[detail.schcode].accountGroup2[detail.grpcode] = {
            accountGroup3:{},
            code: detail.grpcode,
            name: detail.grpname,
            shortname: "",
            type:"",
            curr: "",
            ...final[detail.nature].accountGroup1[detail.schcode].accountGroup2[detail.grpcode],
          }
          final[detail.nature].accountGroup1[detail.schcode].accountGroup2[detail.grpcode].accountGroup3[detail.ctrlcode] = {
            accounts:{},
            code: detail.ctrlcode,
            name: detail.ctrlname,
            shortname: "",
            type:"",
            curr: "",
            ...final[detail.nature].accountGroup1[detail.schcode].accountGroup2[detail.grpcode].accountGroup3[detail.ctrlcode]
          }
          final[detail.nature].accountGroup1[detail.schcode].accountGroup2[detail.grpcode].accountGroup3[detail.ctrlcode].accounts[detail.sno] = {
            code: detail.code,
            name: detail.name,
            shortname: detail.shortname,
            type:detail.type,
            curr: detail.curr,
          }
        })


        const grpcodes = Object.keys(final)

        const tableData = []
        grpcodes.forEach(code => {
          const accountGroup1 = Object.keys(final[code].accountGroup1)

          tableData.push({
            code: final[code].code,
            name: "",
            shortname:"",
            type:"",
            curr:"",
            bold: true,
          })

          accountGroup1.forEach(ag => {
            tableData.push({
              code: "  "+final[code].accountGroup1[ag].code,
              name: "  "+final[code].accountGroup1[ag].name,
              shortname:"",
              type:"",
              curr:"",
              bold: true,
            })
        
            const accountGroup2 = Object.keys(final[code].accountGroup1[ag].accountGroup2)
            accountGroup2.forEach(account => {
              tableData.push({
                code: "    "+final[code].accountGroup1[ag].accountGroup2[account].code,
                name: "    "+final[code].accountGroup1[ag].accountGroup2[account].name,
                shortname:"",
                type: "",
                curr: "",
                bold: true,
              })
              const accountGroup3 =  Object.keys(final[code].accountGroup1[ag].accountGroup2[account].accountGroup3)
              accountGroup3.forEach(acc => {
                tableData.push({
                    code: "      "+final[code].accountGroup1[ag].accountGroup2[account].accountGroup3[acc].code,
                    name: "      "+final[code].accountGroup1[ag].accountGroup2[account].accountGroup3[acc].name,
                    shortname:"",
                    type: "",
                    curr: "",
                    bold: true,
                })
                const accounts = Object.keys(final[code].accountGroup1[ag].accountGroup2[account].accountGroup3[acc].accounts)
                accounts.forEach(ac =>{
                    tableData.push({
                        code: "        "+final[code].accountGroup1[ag].accountGroup2[account].accountGroup3[acc].accounts[ac].code,
                        name: "        "+final[code].accountGroup1[ag].accountGroup2[account].accountGroup3[acc].accounts[ac].name,
                        shortname: final[code].accountGroup1[ag].accountGroup2[account].accountGroup3[acc].accounts[ac].shortname,
                        type: final[code].accountGroup1[ag].accountGroup2[account].accountGroup3[acc].accounts[ac].type,
                        curr: final[code].accountGroup1[ag].accountGroup2[account].accountGroup3[acc].accounts[ac].curr,
                    })
                })
              })
            })
          })
        })
        this.rowData = tableData;
  });
})

}

}