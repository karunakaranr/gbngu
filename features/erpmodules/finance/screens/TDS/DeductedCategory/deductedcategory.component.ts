
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';


@Component({
    selector: 'app-deductedcategory',
    templateUrl: './deductedcategory.component.html',
    styleUrls: ['./deductedcategory.component.scss'],
})
export class deductedcategoryComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    public TotalBill;
    public TotalTds;
    public TotalCess;

    constructor() { }
    ngOnInit(): void {
        this.getdeductedcategory();
    }

    public getdeductedcategory() {
        this.rowdatacommon$.subscribe(data => {
            this.rowData = data;
            console.log("Deducted Category Details :", this.rowData);
            let groupingData = this.rowData;
            var finalizedGroup = [];

            groupingData.map((inputData, index) => {
                finalizedGroup.push({
                    sno: index,
                    billnum: inputData['BillNumber'],
                    bildate: inputData['BillDate'],
                    Vocnum: inputData['VoucherNumber'],
                    Vocdate: inputData['VoucherDate'],
                    billamnt: inputData['BillAmount'],
                    tdsamnt: inputData['TDSAmount'],
                    Cessamnt: inputData['CessAmount'],
                    certnum: inputData['CertificateNumber'],
                    certdate: inputData['CertificateDate'],
                    tdscatname: inputData['TDSCategoryName'],
                    branchprtyname: inputData['PartyBranchName'],

                });
            });

            const final = {};
            finalizedGroup.forEach((detailData) => {
                final[detailData.tdscatname] = {
                    deductedgroup: {},
                    sno: detailData.tdscatname,
                    billnum: "",
                    bildate: "",
                    Vocnum: "",
                    Vocdate: "",
                    billamnt: 0,
                    tdsamnt: 0,
                    Cessamnt: 0,
                    certnum: "",
                    certdate: "",

                    ...final[detailData.tdscatname]

                };

                final[detailData.tdscatname].deductedgroup[detailData.branchprtyname] = {
                    deductedmultigrup: {},
                    sno: detailData.branchprtyname,
                    billnum: "",
                    bildate: "",
                    Vocnum: "",
                    Vocdate: "",
                    billamnt: 0,
                    tdsamnt: 0,
                    Cessamnt: 0,
                    certnum: "",
                    certdate: "",

                    ...final[detailData.tdscatname].deductedgroup[detailData.branchprtyname],
                }

                final[detailData.tdscatname].deductedgroup[detailData.branchprtyname].deductedmultigrup[detailData.sno] = {
                    sno: detailData.sno,
                    billnum: detailData.billnum,
                    bildate: detailData.bildate,
                    Vocnum: detailData.Vocnum,
                    Vocdate: detailData.Vocdate,
                    billamnt: detailData.billamnt,
                    tdsamnt: detailData.tdsamnt,
                    Cessamnt: detailData.Cessamnt,
                    certnum: detailData.certnum,
                    certdate: detailData.certdate,

                }
            });

            const dedGroups = Object.keys(final);

            const tableData = [];

            dedGroups.forEach((Details) => {
                
               

                tableData.push({
                    sno: final[Details].sno,
                    billnum: "",
                    bildate: "",
                    Vocnum: "",
                    Vocdate: "",
                    billamnt:"",
                    tdsamnt: "",
                    Cessamnt:"",
                    certnum: "",
                    certdate: "",
                    bold: true,
                });


                const deductedgroup = Object.keys(final[Details].deductedgroup);
                deductedgroup.forEach(ag => {
                    
                    
                    tableData.push({
                        sno: final[Details].deductedgroup[ag].sno,
                        billnum: "",
                        bildate: "",
                        Vocnum: "",
                        Vocdate: "",
                        billamnt:'',
                        tdsamnt: '',
                        Cessamnt:'',
                        certnum: "",
                        certdate: "",
                        bold: true,
                    })

                    const deductedmultigrup = Object.keys(final[Details].deductedgroup[ag].deductedmultigrup)
                    deductedmultigrup.forEach(account => {
                        tableData.push({
                            sno: final[Details].deductedgroup[ag].deductedmultigrup[account].sno,
                            billnum: final[Details].deductedgroup[ag].deductedmultigrup[account].billnum,
                            bildate: final[Details].deductedgroup[ag].deductedmultigrup[account].bildate,
                            Vocnum: final[Details].deductedgroup[ag].deductedmultigrup[account].Vocnum,
                            Vocdate: final[Details].deductedgroup[ag].deductedmultigrup[account].Vocdate,
                            billamnt: final[Details].deductedgroup[ag].deductedmultigrup[account].billamnt,
                            tdsamnt: final[Details].deductedgroup[ag].deductedmultigrup[account].tdsamnt,
                            Cessamnt: final[Details].deductedgroup[ag].deductedmultigrup[account].Cessamnt,
                            certnum: final[Details].deductedgroup[ag].deductedmultigrup[account].certnum,
                            certdate: final[Details].deductedgroup[ag].deductedmultigrup[account].certdate,

                        })
                    })
                })

            });
            this.rowData = tableData;

            console.log("Final Data:", this.rowData[0])



        });
    }
}
