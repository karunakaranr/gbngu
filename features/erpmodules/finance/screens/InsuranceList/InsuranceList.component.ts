import { Component, OnInit } from '@angular/core';
import { InsuranceList } from './../../services/InsuranceList/InsuranceList.service';
import { DatePipe } from '@angular/common'
const col = require('./../../../../../apps/Goodbooks/Goodbooks/src/assets/data/insurancelist.json')


@Component({
  selector: 'app-InsuranceList',
  templateUrl: './InsuranceList.component.html',
  styleUrls: ['./InsuranceList.component.scss']
})

export class InsuranceListComponent implements OnInit {
  title = 'Insurance List'
  public rowData = []
  public columnData = []

  constructor(public service: InsuranceList) { }
  ngOnInit(): void {
    this.insurancelist();
  }
  public insurancelist() {
    this.service.insurancelistview().subscribe(menudetails => {
      this.columnData = col;
      let servicejsondetail = menudetails
      this.service.Rowservice(servicejsondetail).subscribe(res => {
        this.rowData = res.ReportDetail;
        var datePipe = new DatePipe("en-US");
        for (let data of this.rowData) {
            if (data.OUName=='NONE'){
                data.OUName=""
            }
            data.InsuranceStartDate = datePipe.transform(JSON.parse(data.InsuranceStartDate.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy');
          data.InsuranceEndDate = datePipe.transform(JSON.parse(data.InsuranceEndDate.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy');
          data.InsuranceLastPaidOn = datePipe.transform(JSON.parse(data.InsuranceLastPaidOn.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy');
          data.InsuranceLastPaidTill = datePipe.transform(JSON.parse(data.InsuranceLastPaidTill.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy');
          data.InsurancePremiumDueOn = datePipe.transform(JSON.parse(data.InsurancePremiumDueOn.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy');
          data.InsuranceCoverageAmount  = data.InsuranceCoverageAmount.toLocaleString(undefined, { maximumFractionDigits: 2, minimumFractionDigits: 2 })
          data.InsurancePremiumAmount  = data.InsurancePremiumAmount.toLocaleString(undefined, { maximumFractionDigits: 2, minimumFractionDigits: 2 })
        }
  });
})

}

}