
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { DateFormaterPipe } from 'libs/gbcommon/src/lib/Date-Formater.pipe';


@Component({
    selector: 'app-bankbook',
    templateUrl: './bankbook.component.html',
    styleUrls: ['./bankbook.component.scss'],
})
export class bankbookComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    private dateFormatter: DateFormaterPipe = new DateFormaterPipe(this.store);
    public rowData: any;
    tabledata;
    public TotalDebit;
    public TotalCredit;
    constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string) { }
    ngOnInit(): void {
        this.getbankbook();
    }

    public getbankbook() {
        this.sharedService.getRowInfo().subscribe(data => {
            if (data.length > 0) {
            this.rowData = data;
            console.log("Bank Book Details :", this.rowData)

            this.TotalDebit = 0;
            this.TotalCredit = 0;
            let json = this.rowData;
            var finalizedArray = [];
            for (let data of this.rowData) {
                this.TotalDebit = this.TotalDebit + data.Debit;
                this.TotalCredit = this.TotalCredit + data.Credit;
            }
            json.map((row) => {
                finalizedArray.push({
                    vdate: this.dateFormatter.transform(row['VoucherDate'], 'date'),


                    vno: row['VoucherNumber'],
                    accname: row['AccountName'],
                    debit: row['Debit'],
                    credit: row['Credit'],
                    insno: row['InstrumentNumber']
                });
            });


            const final = {};
            finalizedArray.forEach((detail) => {
                final[detail.vdate] = {
                    accountGroups: {},
                    vno: detail.vdate,
                    accname: "",
                    debit: 0,
                    credit: 0,
                    insno: "",
                    ...final[detail.vdate],
                };

                final[detail.vdate].accountGroups[detail.vno] = {
                    vno: detail.vno,
                    accname: detail.accname,
                    debit: detail.debit,
                    credit: detail.credit,
                    insno: detail.insno,
                };
            });

            const grpcodes = Object.keys(final);

            const tableData = [];
            grpcodes.forEach((vno) => {
                const accountGroups = Object.keys(final[vno].accountGroups);
                let sumdebit = 0;
                let sumcredit = 0;
                accountGroups.forEach(account => {
                    sumdebit = sumdebit + parseFloat(final[vno].accountGroups[account].debit);
                    sumcredit = sumcredit + parseFloat(final[vno].accountGroups[account].credit);
                })
                final[vno].debit = sumdebit;
                final[vno].credit = sumcredit;



                tableData.push({

                    vno: final[vno].vno,

                    debit: final[vno].debit,
                    credit: final[vno].credit,

                    bold: true,
                });

                const accounts = Object.keys(final[vno].accountGroups);
                accounts.forEach((account) => {
                    tableData.push({
                        vno: final[vno].accountGroups[account].vno,
                        accname: final[vno].accountGroups[account].accname,
                        debit: final[vno].accountGroups[account].debit,
                        credit: final[vno].accountGroups[account].credit,
                        insno: final[vno].accountGroups[account].insno,
                    });
                });
            });
            this.tabledata = tableData;

            console.log("Final Data:", this.rowData)
        }
        });
    }
}
