import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';


@Component({
    selector: 'app-supplygroup',
    templateUrl: './supplygroup.component.html',
    styleUrls: ['./supplygroup.component.scss'],
})
export class supplygroupComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowDatas: any;
    constructor() { }
    ngOnInit(): void {
        this.getsupplygroup();
    }

    public getsupplygroup() {
        this.rowdatacommon$.subscribe(data => {
            this.rowDatas = data;
            console.log("Supply Group Details :", this.rowDatas)

            let groupingData = this.rowDatas;
            var finalizedGroup = [];
            
            groupingData.map((inputData,index) => {
                finalizedGroup.push({
                    sno:index,
                    prtycode:inputData['PartyCode'],
                    oucode: inputData['OUCode'],
                    ouname: inputData['OUName'],
                    supgrupcode:inputData['SupplyGroupCode'],
                    supgrupname: inputData['SupplyGroupName'],
                    partyname:inputData['PartyName'],
                    partybranchname:inputData['PartyBranchName'],
                    particular:inputData['Particulars'],
                    shareper: inputData['SharePercent'],
                    remarks:inputData['Remarks'],

                });
            }); 
            
            const final = {};
            finalizedGroup.forEach((detailData) => {
                final[detailData.oucode] ={
                    supplyGroups:{},
                    sno:detailData.prtycode,
                    ouname: detailData.oucode,
                    supgrupcode:"",
                    supgrupname: "",
                    partyname:"",
                    partybranchname:"",
                    particular:"",
                    shareper: "",
                    remarks:"",
                    ...final[detailData.oucode]

                };

                final[detailData.oucode].supplyGroups[detailData.sno] = {
                    sno:detailData.sno,
                    ouname: detailData.ouname,
                    supgrupcode:detailData.supgrupcode,
                    supgrupname: detailData.supgrupname,
                    partyname:detailData.partyname,
                    partybranchname:detailData.partybranchname,
                    particular:detailData.particular,
                    shareper: detailData.shareper,
                    remarks:detailData.remarks,
                }
            });

            const supGroups = Object.keys(final);

            const tableData = [];

            supGroups.forEach((ounameData) => {
                tableData.push({
                    sno:final[ounameData].sno,
                    ouname: final[ounameData].ouname,
                    supgrupcode:"",
                    supgrupname: "",
                    partyname:"",
                    partybranchname:"",
                    particular:"",
                    shareper: "",
                    remarks:"",
                    bold: true,
                });

                const accounts = Object.keys(final[ounameData].supplyGroups);
                accounts.forEach((account) => {
                    tableData.push({
                        sno:final[ounameData].supplyGroups[account].sno,
                        ouname: final[ounameData].supplyGroups[account].ouname,
                        supgrupcode: final[ounameData].supplyGroups[account].supgrupcode,
                        supgrupname:  final[ounameData].supplyGroups[account].supgrupname,
                        partyname: final[ounameData].supplyGroups[account].partyname,
                        partybranchname: final[ounameData].supplyGroups[account].partybranchname,
                        particular: final[ounameData].supplyGroups[account].particular,
                        shareper:  final[ounameData].supplyGroups[account].shareper,
                        remarks: final[ounameData].supplyGroups[account].remarks,
                    });

            });

        });
        this.rowDatas=tableData;
      
        console.log("Final Data:",this.rowDatas[0])
      });
      }
}

