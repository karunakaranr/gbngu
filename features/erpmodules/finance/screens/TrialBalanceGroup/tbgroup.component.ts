
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-tbgroup',
    templateUrl: './tbgroup.component.html',
    styleUrls: ['./tbgroup.component.scss'],
})
export class tbgroupComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    public tabledata;
    constructor(public sharedService: SharedService,public store: Store,  @Inject(LOCALE_ID) public locale: string) { }
    ngOnInit(): void {
        this.gettbgroup();
    }

    public gettbgroup() {
        this.sharedService.getRowInfo().subscribe(data => {
            if (data.length > 0) {
            this.rowData = data;
            console.log("Trial Balance Group :", this.rowData)

            let json = this.rowData;
            var finalizedArray = [];
            json.map((row,index) => {
                finalizedArray.push({
                    sno: index,
                    ascode: row['AccountScheduleCode'],
                    asname: row['AccountScheduleName'],


                    agcode: row['AccountGroupCode'],
                    agname: row['AccountGroupName'],
                    debit: row['Debit'],
                    credit: row['Credit']
                });
            });


            const final = {};
            finalizedArray.forEach((detail) => {
                final[detail.ascode] = {
                    accountGroups: {},
                    agcode: detail.ascode,
                    agname: detail.asname,
                    debit: '',
                    credit: '',
                    ...final[detail.ascode],
                };

                final[detail.ascode].accountGroups[detail.sno] = {
                    agcode: detail.agcode,
                    agname: detail.agname,
                    debit: detail.debit,
                    credit: detail.credit,
                };
            });

            const grpcodes = Object.keys(final);

            const tableData = [];
            grpcodes.forEach((grp) => {
                tableData.push({
                    agcode: final[grp].agcode,
                    agname: final[grp].agname,
                    debit: final[grp].debit,
                    credit: final[grp].credit,
                    bold: true,
                });

                const accounts = Object.keys(final[grp].accountGroups);
                accounts.forEach((account) => {
                    tableData.push({
                        agcode: final[grp].accountGroups[account].agcode,
                        agname: final[grp].accountGroups[account].agname,
                        debit: final[grp].accountGroups[account].debit,
                        credit: final[grp].accountGroups[account].credit,
                    });
                });
            });
            this.tabledata = tableData;

            console.log("Final Data:", this.rowData)
        }
        });
    }
}


