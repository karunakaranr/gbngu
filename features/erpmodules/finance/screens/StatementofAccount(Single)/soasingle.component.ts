import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-soasingle',
    templateUrl: './soasingle.component.html',
    styleUrls: ['./soasingle.component.scss'],
})
export class soasingleComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    showNarration: boolean = false; // Declare the showNarration property and initialize it
    constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string) { }
    ngOnInit(): void {
        this.getsoasingle();
    }

    public getsoasingle() {
        this.sharedService.getRowInfo().subscribe(data => {
            if (data.length > 0) {
            this.rowData = data;
            console.log("Statement of Account(Single) :", this.rowData)

            let json = this.rowData;
            var finalizedArray = [];
            json.map((row) => {
                finalizedArray.push({
                    ledcode: row['LedgerCode'],
                    ledname: row['LedgerName'],

                    vnarration: row['VoucherNarration'],

                    vdate: row['VoucherDate'],
                    bizcode: row['BizTransactionTypeCode'],
                    vno: row['VoucherNumber'],
                    vaccname: row['VoucherDetailAccountName'],
                    debit: row['Debit'],
                    credit: row['Credit'],
                    balance: row['RunningTotal'],
                });
            });


            const final = {};
            finalizedArray.forEach((detail) => {
                final[detail.ledcode] = {
                    accountGroups: {},
                    vdate: "",
                    bizcode: detail.ledcode,
                    vno: detail.ledname,
                    vnarration: detail.vnarration,
                    vaccname: "",
                    debit: "",
                    credit: "",
                    balance: detail.balance,
                    ...final[detail.ledcode],
                };

                // final[detail.parname].accountGroups[detail.ouname] = {
                //   accounts: {},
                //   vdate: detail.parname,
                //   asname:  detail.ouname,
                //   igremarks: "",
                //   igselect: "",

                //   ...final[detail.parname].accountGroups[detail.ouname],
                // };
                final[detail.ledcode].accountGroups[detail.vdate] = {
                    vdate: detail.vdate,
                    bizcode: detail.ledcode,
                    vno: detail.ledname,
                    vaccname: detail.vaccname,
                    vnarration: detail.vnarration,
                    debit: detail.debit,
                    credit: detail.credit,
                    balance: detail.balance,
                };
            });

            const grpcodes = Object.keys(final);

            const tableData = [];
            grpcodes.forEach((vdate) => {
                const accountGroups = Object.keys(final[vdate].accountGroups);

                tableData.push({
                    vdate: "",
                    bizcode: final[vdate].bizcode,
                    vno: final[vdate].vno,
                    vaccname: "",
                    vnarration: "",
                    debit: "",
                    credit: "",
                    balance: final[vdate].balance,
                    bold: true,
                });

                // accountGroups.forEach((ag) => {
                //   tableData.push({
                //     vdate: final[vdate].vdate,
                //     // asname: final[vdate].accountGroups[ag].asname,
                //     bold: true,
                //   });

                const accounts = Object.keys(final[vdate].accountGroups);
                accounts.forEach((account) => {
                    tableData.push({
                        vdate: final[vdate].accountGroups[account].vdate,
                        bizcode: final[vdate].accountGroups[account].bizcode,
                        vno: final[vdate].accountGroups[account].vno,
                        vaccname: final[vdate].accountGroups[account].vaccname,
                        vnarration: final[vdate].accountGroups[account].vnarration,
                        debit: final[vdate].accountGroups[account].debit,
                        credit: final[vdate].accountGroups[account].credit,
                        balance: final[vdate].accountGroups[account].balance,
                    });
                });
            });
            this.rowData = tableData;

            console.log("Final Data:", this.rowData[0])
        }
        });
    }
}


