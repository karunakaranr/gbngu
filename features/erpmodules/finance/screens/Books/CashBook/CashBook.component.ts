import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { GbApexChartDTO } from 'features/erpmodules/samplescreens/stores/apexchart/apexchart.action';
import { Observable } from 'rxjs';
import { EventReceiver } from 'libs/gbcommon/src/lib/services/EventReceiver/EventReceiver.service';
import { SharedService } from 'features/commonreport/service/datapassing.service';
const jsonvalue = require('./CashBook.json')
@Component({
  selector: 'app-CashBook',
  templateUrl: './CashBook.component.html',
  styleUrls: ['./CashBook.component.scss']
})

export class CashBookComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData: any;
  total: any[];
  showNarration: boolean = false; // Declare the showNarration property and initialize it
  constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string) { }
  ngOnInit(): void {
    this.getcashbook();
  }

  onShowNarrationChange() {
    console.log("showNarration changed:", this.showNarration);
    this.getcashbook(); // Ensure that getdaybook is called when showNarration changes
}

  public getcashbook() {
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Cash Book Details :", this.rowData)

      let json = this.rowData;
      var finalizedArray = [];
      json.map((row,index) => {
        finalizedArray.push({
          sno: index,
          vdate: row['VoucherDate'],

          vnarration: row['VoucherNarration'],

          vno: row['VoucherNumber'],
          bizcode: row['BizTransactionTypeCode'],
          refno: row['ReferenceNumber'],
          refdate: row['ReferenceDate'],
          vaccno: row['VoucherDetailAccountName'],
          debit: row['Debit'],
          credit: row['Credit']
        });
      });


      const final = {};
      finalizedArray.forEach((detail) => {
        final[detail.vdate] = {
          accountGroups: {},
          vno: detail.vdate,
          bizcode: "",
          refno: "",
          refdate: "",
          vaccno: "",
          vnarration: detail.vnarration,
          debit: "",
          credit: "",
          ...final[detail.vdate],
        };

        // final[detail.parname].accountGroups[detail.ouname] = {
        //   accounts: {},
        //   vdate: detail.parname,
        //   asname:  detail.ouname,
        //   igremarks: "",
        //   igselect: "",

        //   ...final[detail.parname].accountGroups[detail.ouname],
        // };
        final[detail.vdate].accountGroups[detail.sno] = {
          vno: detail.vno,
          bizcode: detail.bizcode,
          refno: detail.refno,
          refdate: detail.refdate,
          vaccno: detail.vaccno,
          vnarration: detail.vnarration,
          debit: detail.debit,
          credit: detail.credit,
        };
      });

      const grpcodes = Object.keys(final);

      const tableData = [];
      grpcodes.forEach((vno) => {
        const accountGroups = Object.keys(final[vno].accountGroups);

        tableData.push({
          vno: final[vno].vno,
          bizcode: "",
          refno: "",
          refdate: "",
          vaccno: "",
          vnarration:"",
          debit: "",
          credit: "",
          bold: true,
        });

        // accountGroups.forEach((ag) => {
        //   tableData.push({
        //     vdate: final[vdate].vdate,
        //     // asname: final[vdate].accountGroups[ag].asname,
        //     bold: true,
        //   });

        const accounts = Object.keys(final[vno].accountGroups);
        accounts.forEach((account) => {
          tableData.push({
            vno: final[vno].accountGroups[account].vno,
            bizcode: final[vno].accountGroups[account].bizcode,
            refno: final[vno].accountGroups[account].refno,
            refdate: final[vno].accountGroups[account].refdate,
            vaccno: final[vno].accountGroups[account].vaccno,
            vnarration: final[vno].accountGroups[account].vnarration,
            debit: final[vno].accountGroups[account].debit,
            credit: final[vno].accountGroups[account].credit,
          });
        });
      });
      this.total = tableData;

      console.log("Final Data:", this.rowData)
    }
    });
  }
}