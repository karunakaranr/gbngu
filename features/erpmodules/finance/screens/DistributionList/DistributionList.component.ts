import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IDistributionList } from '../../models/IDistributionList';
import { DistributionListService } from '../../services/DistributionList/DistributionList.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';

import { URLS } from './../../URLS/urls'
// const urls = require('./../../URLS/urls.json');

@Component({
  selector: 'app-DistributionList',
  templateUrl: './DistributionList.component.html',
  styleUrls: ['./DistributionList.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'DistributionListId' },
    { provide: 'url', useValue: URLS.DistributionList },
    { provide: 'DataService', useClass: DistributionListService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class DistributionListComponent extends GBBaseDataPageComponentWN<IDistributionList> {
  title: string = "DistributionList"
  DistributionListJSON: IBaseField
  JSONCalling = 0;
  form: GBDataFormGroupWN<IDistributionList> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "DistributionList", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.Getselectedid(arrayOfValues.DistributionListId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.Getselectedid('0')
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FindPicklist(FieldName: string): IPickListDetail {
    if (this.DistributionListJSON != undefined) {
      return this.DistributionListJSON.ObjectFields.find((field: any) => field.Name === FieldName).FormPicklist
    } else {
      setTimeout(() => {
        this.FindPicklist(FieldName)
      }, 500)

    }
  }


  public Getselectedid(SelectedPicklistData: string): void {
    console.log("picklistersss",SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(DistributionList => {
       
        if (SelectedPicklistData != '0') {
          this.form.patchValue(DistributionList);
        }
        if (this.JSONCalling == 0) {
          this.localhttp.get('assets/FormJSONS/DistributionList.json').subscribe((DistributionListJson: IBaseField) => {
            this.DistributionListJSON = DistributionListJson
          })
          this.JSONCalling = 1;
        }
      })
    }
    console.log("afterpicklistersss",SelectedPicklistData)
  }


  public Getselectedids(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IDistributionList> {
  const dbds: GBBaseDBDataService<IDistributionList> = new GBBaseDBDataService<IDistributionList>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IDistributionList>, dbDataService: GBBaseDBDataService<IDistributionList>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IDistributionList> {
  return new GBDataPageService<IDistributionList>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
