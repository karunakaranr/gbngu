import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Select, Store } from '@ngxs/store';
import { IBank } from '../../models/IBank';
import { BankService } from '../../services/Bank/Bank.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import { ObjectFields } from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Bank.json'
// import { FrameworkURLS } from '../../URLS/url';
import { URLS } from '../../URLS/urls';
import * as BankJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Bank.json'
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';
@Component({
  selector: "app-Bank",
  templateUrl: "./Bank.component.html",
  styleUrls: ["./Bank.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'GcmId' },
    { provide: 'url', useValue: URLS.Bank },
    { provide: 'DataService', useClass: BankService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class BankComponent extends GBBaseDataPageComponentWN<IBank > implements OnInit{
  @Select(LayoutState.GCMTypeId) GCMTypeId$: Observable<any>;
  title: string = 'Bank'
  BankJSON = BankJSON;  
  GCMTypeId:number
  @Output() Onselectdata: EventEmitter<any> = new EventEmitter<string>();


  form: GBDataFormGroupWN<IBank > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Bank', {}, this.gbps.dataService);
  ngOnInit(): void {
    this.GCMTypeId$.subscribe((gcmtypeid:number)=>{
      console.log("gcmtypeid:",gcmtypeid)
      this.GCMTypeId = gcmtypeid;
  })
    this.thisConstructor();
    this.subscribeToFormChanges();
  }


  // ngOnInit(): void {
  //   this.thisConstructor();
  //   this.subscribeToFormChanges();
  // }
  thisConstructor() {
    console.log("BankJSON:",this.BankJSON)
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.BankFillFunction(arrayOfValues.GcmId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

  private subscribeToFormChanges(): void {
    console.log("Bank",this.form.value)
    this.form.valueChanges.subscribe(() => {
      console.log("Bankssss",this.form.value)
      this.Onselectdata.emit(this.form.value)
    });
  }

  // private subscribeToFormChanges(): void {
  //   this.form.valueChanges.subscribe(() => {
  //     console.log("UOMMM",this.form.value)
  //     this.Onselectdata.emit(this.form.value)
  //     // alert('Form value changed!');
  //   });
  // }


  public BankFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Bank => {
        this.form.patchValue(Bank);
        console.log("Bank:",this.form.value)
        console.log("GcmTypeCode:",this.form.get("GcmTypeCode").value)
      })
    }
  }


  public BankPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IBank> {
  const dbds: GBBaseDBDataService<IBank> = new GBBaseDBDataService<IBank>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IBank>, dbDataService: GBBaseDBDataService<IBank>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IBank> {
  return new GBDataPageService<IBank>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
