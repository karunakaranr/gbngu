import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { AccountGroup } from '../../../services/AccountList/AccountGroup/AccountGroup.service';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Observable } from 'rxjs';
import { SharedService } from 'features/commonreport/service/datapassing.service';

@Component({
  selector: 'app-AccountGroup',
  templateUrl: './AccountGroup.component.html',
  styleUrls: ['./AccountGroup.component.scss']
})

export class AccountGroupComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  title = 'Account Group'
  public rowData = []

  constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string) { }
  ngOnInit(): void {
    this.accountgroup();
  }
  public accountgroup() {

    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Account Group List :", this.rowData)

      let json = this.rowData;
      var finalizedArray = []
      json.map(row => {
        finalizedArray.push({
          nature: row['AccountGroupNatureName'],
          schcode: row['AccountGroupAccountScheduleCode'],
          schname: row['AccountGroupAccountScheduleName'],
          code: row['AccountGroupCode'],
          name: row['AccountGroupName'],
          shortname: row['AccountGroupShortName'],
          opposite: row['OppositeGroupName'],
        })
      })

      const final = {};
      finalizedArray.forEach(detail => {
        final[detail.nature] = {
          accountGroups: {},
          code: detail.nature,
          name: "",
          shortname: "",
          opposite: "",
          ...final[detail.nature],
        }

        final[detail.nature].accountGroups[detail.schcode] = {
          accounts: {},
          code: detail.schcode,
          name: detail.schname,
          shortname: "",
          opposite: "",
          ...final[detail.nature].accountGroups[detail.schcode],
        }
        final[detail.nature].accountGroups[detail.schcode].accounts[detail.code] = {
          code: detail.code,
          name: detail.name,
          shortname: detail.shortname,
          opposite: detail.opposite,
        }
      })


      const grpcodes = Object.keys(final)

      const tableData = []
      grpcodes.forEach(code => {
        const accountGroups = Object.keys(final[code].accountGroups)

        tableData.push({
          code: final[code].code,
          name: "",
          shortname: "",
          opposite: "",
          bold: true,
        })

        accountGroups.forEach(ag => {
          tableData.push({
            code: final[code].accountGroups[ag].code,
            name: final[code].accountGroups[ag].name,
            shortname: "",
            opposite: "",
            bold: true,
          })

          const accounts = Object.keys(final[code].accountGroups[ag].accounts)
          accounts.forEach(account => {
            tableData.push({
              code: final[code].accountGroups[ag].accounts[account].code,
              name: final[code].accountGroups[ag].accounts[account].name,
              shortname: final[code].accountGroups[ag].accounts[account].shortname,
              opposite: final[code].accountGroups[ag].accounts[account].opposite,
            })
          })
        })
      })
      this.rowData = tableData;
      console.log("Final Data:", this.rowData)
    }
    });
  }
}