import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { AccountList } from '../../../services/AccountList/AccountList/AccountList.service';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Observable } from 'rxjs';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
  selector: 'app-AccountList',
  templateUrl: './AccountList.component.html',
  styleUrls: ['./AccountList.component.scss']
})

export class AccountListComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  title = 'Account List'
  public rowData = []

  constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string) { }
  ngOnInit(): void {
    this.accountlist();
  }
  public accountlist() {
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Account List :", this.rowData)
   


    
        let json = this.rowData;
        var finalizedArray = []
        json.map(row => {
          finalizedArray.push({
            grpcode: row['AccountGroupCode'],
            grpname: row['AccountGroupName'],
            parcode: row['ParentCode'],
            parname: row['ParentName'],
            ac: row['AccountCode'],
            an: row['AccountName'],
            type: row['AccountTypeName'],
            curr: row['CurrencyCode']
          })
        })

        const final = {};
        finalizedArray.forEach(detail => {
          final[detail.grpcode] = {
            accountGroups :{},
            ac: detail.grpcode,
            an: detail.grpname,
            type: "",
            curr: "",
            ...final[detail.grpcode], 
          }

          final[detail.grpcode].accountGroups[detail.parcode] =  {
            accounts: {},
            ac: detail.parcode,
            an: detail.parname,
            type: "",
            curr: "",
            ...final[detail.grpcode].accountGroups[detail.parcode],
          }
          final[detail.grpcode].accountGroups[detail.parcode].accounts[detail.ac] = {
            ac: detail.ac,
            an: detail.an,
            type: detail.type,
            curr: detail.curr,
          }
        })


        const grpcodes = Object.keys(final)

        const tableData = []
        grpcodes.forEach(ac => {
          const accountGroups = Object.keys(final[ac].accountGroups)

          tableData.push({
            ac: final[ac].ac,
            an: final[ac].an,
            bold: true,
          })

          accountGroups.forEach(ag => {
            tableData.push({
              ac: final[ac].accountGroups[ag].ac,
              an: final[ac].accountGroups[ag].an,
              bold: true,
            })
        
            const accounts = Object.keys(final[ac].accountGroups[ag].accounts)
            accounts.forEach(account => {
              tableData.push({
                ac: final[ac].accountGroups[ag].accounts[account].ac,
                an: final[ac].accountGroups[ag].accounts[account].an,
                type: final[ac].accountGroups[ag].accounts[account].type,
                curr: final[ac].accountGroups[ag].accounts[account].curr,
              })
            })
          })
        })
        this.rowData = tableData;
      }
      });
    }
  }