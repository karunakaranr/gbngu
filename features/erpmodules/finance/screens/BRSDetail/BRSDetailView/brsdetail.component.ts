import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-brsdetail',
    templateUrl: './brsdetail.component.html',
    styleUrls: ['./brsdetail.component.scss'],
})
export class brsdetailComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData = [];
    public TotalAmount;
    constructor(public sharedService: SharedService,public store: Store) { }

    ngOnInit(): void {
        this.brsdetail();
    }

    public brsdetail() {
        this.sharedService.getRowInfo().subscribe(data => {
            if (data.length > 0) {
            this.rowData = data;
            console.log("BRS Detail Data :", this.rowData)

            this.TotalAmount = 0;
            let json = this.rowData;
            var finalizedArray = [];

            for (let data of this.rowData) {
                this.TotalAmount = this.TotalAmount + data.InstrumentAmount;
                
            }

            

            json.map((row,index) => {
                let instrumentStatus;
    
                // Mapping InstrumentStatus based on provided logic
                if (row['InstrumentStatus'] == 0) {
                    instrumentStatus = "Pending";
                } else if (row['InstrumentStatus'] == 1) {
                    instrumentStatus = "Deposit";
                } else if (row['InstrumentStatus'] == 2) {
                    instrumentStatus = "Realised";
                } else if (row['InstrumentStatus'] == 3) {
                    instrumentStatus = "Bounced";
                } else if (row['InstrumentStatus'] == 4) {
                    instrumentStatus = "Hold";
                } else if (row['InstrumentStatus'] == 5) {
                    instrumentStatus = "Cancelled";
                }
            
                finalizedArray.push({
                    sno: index,
                    bal: "Balance as Per Book",
                    sts: row['Status'],
                    inssts: instrumentStatus,
                    closing: row['Closing'],

                    vdate: row['VoucherDate'],
                    insno: row['InstrumentNumber'],
                    insdate: row['InstrumentDate'],
                    vno: row['VoucherNumber'],
                    ptname: row['PartyName'],
                    favname: row['FavouringName'],
                    insamt: row['InstrumentAmount']
                    
                });
            });

            const final = {};
            finalizedArray.forEach((detail) => {
                final[detail.bal] = {
                    accountGroups: {},
                    vdate: detail.bal,
                    insno: "",
                    insdate: "",
                    vno: "",
                    ptname: "",
                    favname: "",
                    insamt: detail.closing,
                    ...final[detail.bal],
                };

                final[detail.bal].accountGroups[detail.sts] = {
                    accounts: {},
                    vdate: detail.sts,
                    insno: "",
                    insdate: "",
                    vno: "",
                    ptname: "",
                    favname: "",
                    insamt: "",
                    ...final[detail.bal].accountGroups[detail.sts],
                };

                final[detail.bal].accountGroups[detail.sts].accounts[detail.inssts] = {
                    accountsList: {},
                    vdate: "CHQ : "+detail.inssts,
                    insno: "",
                    insdate: "",
                    vno: "",
                    ptname: "",
                    favname: "",
                    insamt: "",
                    ...final[detail.bal].accountGroups[detail.sts].accounts[detail.inssts],
                };

                final[detail.bal].accountGroups[detail.sts].accounts[detail.inssts].accountsList[detail.vdate] = {
                    vdate: detail.vdate,
                    insno: detail.insno,
                    insdate: detail.insdate,
                    vno: detail.vno,
                    ptname: detail.ptname,
                    favname: detail.favname,
                    insamt: detail.insamt,
                };
            });

            const grpcodes = Object.keys(final);

            const tableData = [];
            grpcodes.forEach((acccode) => {
                const accountGroups = Object.keys(final[acccode].accountGroups);

                
                    
                    tableData.push({
                        vdate: final[acccode].vdate,
                        insno: final[acccode].insno,
                        insdate: final[acccode].insdate,
                        vno: final[acccode].vno,
                        ptname: final[acccode].ptname,
                        favname: final[acccode].favname,
                        insamt: final[acccode].insamt,
                        bold: true,
                    });

                    accountGroups.forEach((ag) => {
                    tableData.push({
                        vdate: final[acccode].accountGroups[ag].vdate,
                        insno: final[acccode].accountGroups[ag].insno,
                        insdate: final[acccode].accountGroups[ag].insdate,
                        vno: final[acccode].accountGroups[ag].vno,
                        ptname: final[acccode].accountGroups[ag].ptname,
                        favname: final[acccode].accountGroups[ag].favname,
                        insamt: final[acccode].accountGroups[ag].insamt,
                        bold: true,
                    });

                
                    const accounts = Object.keys(final[acccode].accountGroups[ag].accounts);
                    accounts.forEach((account) => {
                        tableData.push({
                            vdate: final[acccode].accountGroups[ag].accounts[account].vdate,
                            insno: final[acccode].accountGroups[ag].accounts[account].insno,
                            insdate: final[acccode].accountGroups[ag].accounts[account].insdate,
                            vno: final[acccode].accountGroups[ag].accounts[account].vno,
                            ptname: final[acccode].accountGroups[ag].accounts[account].ptname,
                            favname: final[acccode].accountGroups[ag].accounts[account].favname,
                            insamt: final[acccode].accountGroups[ag].accounts[account].insamt,
                            bold: true,
                        });
                  

                    const accountsList = Object.keys(final[acccode].accountGroups[ag].accounts[account].accountsList);
                    accountsList.forEach((acclist) => {
                        tableData.push({
                            vdate: final[acccode].accountGroups[ag].accounts[account].accountsList[acclist].vdate,
                            insno: final[acccode].accountGroups[ag].accounts[account].accountsList[acclist].insno,
                            insdate: final[acccode].accountGroups[ag].accounts[account].accountsList[acclist].insdate,
                            vno: final[acccode].accountGroups[ag].accounts[account].accountsList[acclist].vno,
                            ptname: final[acccode].accountGroups[ag].accounts[account].accountsList[acclist].ptname,
                            favname: final[acccode].accountGroups[ag].accounts[account].accountsList[acclist].favname,
                            insamt: final[acccode].accountGroups[ag].accounts[account].accountsList[acclist].insamt,
                        });
                        });
                    });
                });
            });
            this.rowData = tableData;
            console.log("Final Data:", this.rowData);
        }
        });
    }

}
