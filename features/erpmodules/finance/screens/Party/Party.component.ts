import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';

import { IParty } from '../../models/IParty';
import { PartyService } from '../../services/Party/Party.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

import * as Partyjson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Party.json'


import { URLS } from '../../URLS/urls';

@Component({
  selector: "app-Party",
  templateUrl: "./Party.component.html",
  styleUrls: ["./Party.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'PartyId' },
    { provide: 'url', useValue: URLS.Party },
    { provide: 'DataService', useClass: PartyService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PartyComponent extends GBBaseDataPageComponentWN<IParty> {
  title: string = "Party"
  Partyjson = Partyjson;



  public IsAccount():void{
    console.log('IsAccount function is started');
    if (this.form.get('PartyIsAccount').value == "1") {
        
        console.log('PartyIsAccount value set to 1');
        this.form.get('AccountLinkName').patchValue
        this.form.get('AccountLinkCode').patchValue
        this.form.get('AccountLinkId').patchValue
        
      }
      else if (this.form.get('PartyIsAccount').value == "0"){

        console.log('PartyIsAccount value set to 0');
        this.form.get('AccountLinkName').patchValue('NONE');
        this.form.get('AccountLinkCode').patchValue('NONE');
        this.form.get('AccountLinkId').patchValue('-1'); 
      }

   }

   public Passvalue():void{
    
    var name = this.form.get('PartyName').value;
    console.log('Passvalue is working:',name);
    if(this.form.get('PartyName').value!=""){
      console.log('Passvalue is working');
      this.form.get('PartyFavouringName').patchValue(name);

    }
   }

   
  form: GBDataFormGroupWN<IParty> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Party', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PartyPicklist(arrayOfValues.PartyId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }




  public PartyPicklist(SelectedPicklistData: string): void {

    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Party => {
        this.form.patchValue(Party);
      })
    }
  }


  public PartyPicklists(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}




export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IParty> {
  const dbds: GBBaseDBDataService<IParty> = new GBBaseDBDataService<IParty>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IParty>, dbDataService: GBBaseDBDataService<IParty>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IParty> {
  return new GBDataPageService<IParty>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
