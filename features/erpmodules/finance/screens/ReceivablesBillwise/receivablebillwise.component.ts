import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Console } from 'console';


@Component({
    selector: 'app-receivablebillwise',
    templateUrl: './receivablebillwise.component.html',
    styleUrls: ['./receivablebillwise.component.scss'],
})
export class receivablebillwiseComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    public TotalPending;
    public TotalUnadjusted;
    public TotalBalance;
    tabledata: any[];
    constructor(public store: Store, @Inject(LOCALE_ID) public locale: string) { }
    ngOnInit(): void {
        this.getreceivablebillwise();
    }

    public getreceivablebillwise() {
        this.rowdatacommon$.subscribe(data => {
            this.rowData = data;
            console.log("Receivables Billwise Reports :", this.rowData)

            this.TotalPending = 0;
            this.TotalUnadjusted = 0;
            this.TotalBalance = 0;
            let json = this.rowData;
            var finalizedArray = [];
            for (let data of this.rowData) {
                this.TotalPending = this.TotalPending + data.Receivable;
                this.TotalUnadjusted = this.TotalUnadjusted + data.Payable;
                this.TotalBalance = this.TotalBalance + data.Balance;
            }
            json.map((row) => {
                finalizedArray.push({
                    oucode: row['OUCode'],
                    ouname: row['OUName'],

                    rtcode: row['RoutingCode'],
                    rtname: row['RoutingName'],

                    pcccode: row['PriceCategoryCode'],
                    pcname: row['PriceCategoryName'],

                    acccode: row['AccountCode'],
                    accname: row['AccountName'],

                    grptype: row['GroupType'],

                    voucherno: row['VoucherNumber'],
                    vdate: row['VoucherDate'],
                    refno: row['ReferenceNumber'],
                    refdate: row['ReferenceDate'],
                    amountfc: row['FullAmountFc'],
                    receivable: row['Receivable'],
                    payable: row['Payable'],
                    bal: row['Balance'],
                    curcode: row['CurrencyCode'],
                    duedate: row['DueDate'],
                    dueage: row['DueAge'],
                    billage: row['BillAge'],
                    balancefc: row['BalanceFC'],
                    detailcurcode: row['DetailCurrencyCode'],
                });
            });
            console.log("Json:",finalizedArray);
            const final = {};
            finalizedArray.forEach((detail) => {
                final[detail.oucode] = {
                    accountGroups: {},
                    voucherno: detail.grptype === 'OU' ? detail.oucode : 
                //  detail.grptype === 'INCHARGE' ? detail.pcccode : 
                 detail.grptype === 'ROUTE' ? detail.rtcode : detail.pcccode,
                    vdate: detail.grptype === 'OU' ? detail.ouname : 
                    // detail.grptype === 'INCHARGE' ? detail.pcname : 
                    detail.grptype === 'ROUTE' ? detail.rtname : detail.pcname,
                    refno: "",
                    refdate: "",
                    amountfc: "",
                    receivable: detail.receivable,
                    payable:detail.payable,
                    bal: detail.bal,
                    curcode: "",
                    duedate: "",
                    dueage: "",
                    billage: "",
                    balancefc: "",
                    detailcurcode: "",

                    ...final[detail.oucode],
                };

                final[detail.oucode].accountGroups[detail.acccode] = {
                    accounts: {},
                    voucherno: detail.acccode,
                    vdate: detail.accname,
                    refno: "",
                    refdate: "",
                    amountfc: "",
                    receivable: detail.receivable,
                    payable: detail.payable,
                    bal: detail.bal,
                    curcode: "",
                    duedate: "",
                    dueage: "",
                    billage: "",
                    balancefc: "",
                    detailcurcode: "",

                    ...final[detail.oucode].accountGroups[detail.acccode],
                };

                final[detail.oucode].accountGroups[detail.acccode].accounts[detail.voucherno] = {
                    voucherno: detail.voucherno,
                    vdate: detail.vdate,
                    refno: detail.refno,
                    refdate: detail.refdate,
                    amountfc: detail.amountfc,
                    receivable: detail.receivable,
                    payable: detail.payable,
                    bal: detail.bal,
                    curcode: detail.curcode,
                    duedate: detail.duedate,
                    dueage: detail.dueage,
                    billage: detail.billage,
                    balancefc: detail.balancefc,
                    detailcurcode: detail.detailcurcode,
                };
            });
            console.log("Final:",final);
            const grpcodes = Object.keys(final);

            const tableData = [];
            grpcodes.forEach((voucherno) => {
                const accountGroups = Object.keys(final[voucherno].accountGroups);
                let sumpending = 0;
                let sumunadjusted = 0;
                let sumbalance = 0;

                accountGroups.forEach((ag) => {
                    const accounts = Object.keys(final[voucherno].accountGroups[ag].accounts);
                    accounts.forEach((secsubtotal) => {
                        sumpending = sumpending + parseFloat(final[voucherno].accountGroups[ag].accounts[secsubtotal].receivable);
                        sumunadjusted = sumunadjusted + parseFloat(final[voucherno].accountGroups[ag].accounts[secsubtotal].payable);
                        sumbalance = sumbalance + parseFloat(final[voucherno].accountGroups[ag].accounts[secsubtotal].bal);
                        
                    })
                    // sumpending = sumpending + parseFloat(final[voucherno].accountGroups[ag].receivable);
                    // sumunadjusted = sumunadjusted + parseFloat(final[voucherno].accountGroups[ag].payable);
                    // sumbalance = sumbalance + parseFloat(final[voucherno].accountGroups[ag].bal);
                    
                })
                console.log("sumpending:",sumpending)
                console.log("sumunadjusted:",sumunadjusted)
                console.log("sumbalance:",sumbalance)
                final[voucherno].receivable = sumpending;
                final[voucherno].payable = sumunadjusted;
                final[voucherno].bal = sumbalance;
                tableData.push({
                    voucherno: final[voucherno].voucherno,
                    vdate: final[voucherno].vdate,
                    refno: "",
                    refdate: "",
                    amountfc: "",
                    receivable: final[voucherno].receivable,
                    payable: final[voucherno].payable,
                    bal: final[voucherno].bal,
                    curcode: "",
                    duedate: "",
                    dueage: "",
                    billage: "",
                    balancefc: "",
                    detailcurcode: "",
                    bold: true,
                });
                console.log("Tabledata1:",tableData)
                accountGroups.forEach((ag) => {
                    const accounts = Object.keys(final[voucherno].accountGroups[ag].accounts);
                    let sumpending = 0;
                    let sumunadjusted = 0;
                    let sumbalance = 0;

                    accounts.forEach((secsubtotal) => {
                        sumpending = sumpending + parseFloat(final[voucherno].accountGroups[ag].accounts[secsubtotal].receivable);
                        sumunadjusted = sumunadjusted + parseFloat(final[voucherno].accountGroups[ag].accounts[secsubtotal].payable);
                        sumbalance = sumbalance + parseFloat(final[voucherno].accountGroups[ag].accounts[secsubtotal].bal);
                    })
                    final[voucherno].accountGroups[ag].receivable = sumpending;
                    final[voucherno].accountGroups[ag].payable = sumunadjusted;
                    final[voucherno].accountGroups[ag].bal = sumbalance;
                    tableData.push({
                        voucherno: final[voucherno].accountGroups[ag].voucherno,
                        vdate: final[voucherno].accountGroups[ag].vdate,
                        refno: "",
                        refdate: "",
                        amountfc: "",
                        receivable: final[voucherno].accountGroups[ag].receivable,
                        payable: final[voucherno].accountGroups[ag].payable,
                        bal: final[voucherno].accountGroups[ag].bal,
                        curcode: "",
                        duedate: "",
                        dueage: "",
                        billage: "",
                        balancefc: "",
                        detailcurcode: "",
                        bold: true,
                    });
                    
                    accounts.forEach((account) => {
                        tableData.push({
                            voucherno: final[voucherno].accountGroups[ag].accounts[account].voucherno,
                            vdate: final[voucherno].accountGroups[ag].accounts[account].vdate,
                            refno: final[voucherno].accountGroups[ag].accounts[account].refno,
                            refdate: final[voucherno].accountGroups[ag].accounts[account].refdate,
                            amountfc: final[voucherno].accountGroups[ag].accounts[account].amountfc,
                            receivable: final[voucherno].accountGroups[ag].accounts[account].receivable,
                            payable: final[voucherno].accountGroups[ag].accounts[account].payable,
                            bal: final[voucherno].accountGroups[ag].accounts[account].bal,
                            curcode: final[voucherno].accountGroups[ag].accounts[account].curcode,
                            duedate: final[voucherno].accountGroups[ag].accounts[account].duedate,
                            dueage: final[voucherno].accountGroups[ag].accounts[account].dueage,
                            billage: final[voucherno].accountGroups[ag].accounts[account].billage,
                            balancefc: final[voucherno].accountGroups[ag].accounts[account].balancefc,
                            detailcurcode: final[voucherno].accountGroups[ag].accounts[account].detailcurcode,
                        });
                    });
                });
                this.tabledata = tableData;
                
            });
        });
    }
}