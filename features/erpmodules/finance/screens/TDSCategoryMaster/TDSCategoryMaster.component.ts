import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';

import { ITDSCategoryMaster } from '../../models/ITDSCategoryMaster';
import { TDSCategoryMasterService } from '../../services/TDSCategoryMaster/TDSCategoryMaster.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

import * as TDSCategoryJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/TDSCategoryMaster.json'

import { URLS } from '../../URLS/urls'; 

@Component({
  selector: "app-TDSCategoryMaster",
  templateUrl: "./TDSCategoryMaster.component.html",
  styleUrls: ["./TDSCategoryMaster.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'TDSCategoryId' },
    { provide: 'url', useValue: URLS.TDSCategoryMaster },
    { provide: 'DataService', useClass: TDSCategoryMasterService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class TDSCategoryMasterComponent extends GBBaseDataPageComponentWN<ITDSCategoryMaster> {
  title: string = "TDSCategoryMaster"
  TDSCategoryJson = TDSCategoryJson;



  form: GBDataFormGroupWN<ITDSCategoryMaster> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'TDSCategoryMaster', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.TDSCategoryMasterFillFunction(arrayOfValues.TDSCategoryId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public TDSCategoryMasterFillFunction(SelectedPicklistData: string): void {

   console.log("filling",SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(TDSCategoryMaster => {
        this.form.patchValue(TDSCategoryMaster);
      })
    }
  }


  public TDSCategoryMasterPatchValue(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ITDSCategoryMaster> {
  const dbds: GBBaseDBDataService<ITDSCategoryMaster> = new GBBaseDBDataService<ITDSCategoryMaster>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ITDSCategoryMaster>, dbDataService: GBBaseDBDataService<ITDSCategoryMaster>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ITDSCategoryMaster> {
  return new GBDataPageService<ITDSCategoryMaster>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
