import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';

import { ICostCenterPattern } from '../../models/ICostCenterPattern';
import { CostCenterPatternService } from '../../services/CostCenterPattern/CostCenterPattern.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

import * as CostcenterpatternJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CostCenterPattern.json'
import{URLS} from '../../URLS/urls'


@Component({
  selector: "app-CostCenterPattern",
  templateUrl: "./CostCenterPattern.component.html",
  styleUrls: ["./CostCenterPattern.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'CostCenterPatternId' },
    { provide: 'url', useValue: URLS.CostCenterPattern },
    { provide: 'DataService', useClass: CostCenterPatternService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class CostCenterPatternComponent extends GBBaseDataPageComponentWN<ICostCenterPattern> {
  title: string = "CostCenterPattern"
  CostcenterpatternJson = CostcenterpatternJson;



  form: GBDataFormGroupWN<ICostCenterPattern> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'CostCenterPattern', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.CostCenterPatternFillFunction(arrayOfValues.CostCenterPatternId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public CostCenterPatternFillFunction(SelectedPicklistData: string): void {

   console.log("filling",SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(CostCenterPattern => {
        this.form.patchValue(CostCenterPattern);
      })
    }
  }


  public CostCenterPatternPatchValue(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICostCenterPattern> {
  const dbds: GBBaseDBDataService<ICostCenterPattern> = new GBBaseDBDataService<ICostCenterPattern>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ICostCenterPattern>, dbDataService: GBBaseDBDataService<ICostCenterPattern>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICostCenterPattern> {
  return new GBDataPageService<ICostCenterPattern>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
