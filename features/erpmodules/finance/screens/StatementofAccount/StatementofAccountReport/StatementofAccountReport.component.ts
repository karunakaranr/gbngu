import { Component, OnInit } from '@angular/core';
import { StatementOfAccountReportService } from './../../../services/statementofaccounts/statementofaccounts.service'
import { DatePipe } from '@angular/common';

const col = require('./../../../../../../apps/Goodbooks/Goodbooks/src/assets/data/StatementOfAccount.json')

@Component({
  selector: 'app-StatementofAccountReport.component',
  templateUrl: './StatementofAccountReport.component.html',
  styleUrls: ['./StatementofAccountReport.component.scss']
})

export class StatementOfAccountReportComponent implements OnInit {
  title = 'Statement Of Account'
  GTDebit
  GTCredit
  FOpening
  FClosing

  statementofaccountarray: any[] = [];
  constructor(
    private statementofaccountserivce: StatementOfAccountReportService) { }
  ngOnInit() {
    this.Getstatementofaccount();
  }
  Getstatementofaccount(): void {
    this.statementofaccountserivce.Getstatementofaccount().subscribe(menuDetails => {
      const menuDet = menuDetails;
      this.statementofaccountserivce.Rowservice(menuDet).subscribe(res => {
        const final = {};
        const tableData = [];
        let AccountCodes;
        let data = res.ReportDetail;
        this.GTDebit = 0;
        this.GTCredit = 0;
        this.FOpening = 0;
        this.FClosing = 0;

        for (let gt of data) {
          this.GTDebit = gt.TotalDebit;
          this.GTCredit = gt.TotalCredit;
          this.FOpening = gt.FinalOpening;
          this.FClosing = gt.FinalClosing;
        }

        for (let s of data) {
          var datePipe = new DatePipe("en-US");
          s.VoucherDate = datePipe.transform(JSON.parse(s.VoucherDate.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy');
          if (s.Debit != 0) {
            s.Debit = s.Debit.toFixed(2);
            var parts = s.Debit.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            s.Debit = parts.join(".");
          }
          else {
            s.Debit = "";
          }
          if (s.Credit != 0) {
            s.Credit = s.Credit.toFixed(2);
            var parts = s.Credit.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            s.Credit = parts.join(".");
          }
          else {
            s.Credit = "";
          }
          if (s.RunningTotal != 0) {
            s.RunningTotal = s.RunningTotal.toFixed(2);
            var parts = s.RunningTotal.toString().split(".");
            parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            s.RunningTotal = parts.join(".");
          }
          else {
            s.RunningTotal = "";
          }
        }

        data.forEach((detail) => {
          final[detail.LedgerCode] = {
            AccountCodeGroups: {},
            name: detail.LedgerName,
            slno: '',
            vnumber: '',
            vdate: '',
            type: '',
            particulars: '',
            debit: 0,
            credit: 0,
            balance: 0,
            ...final[detail.LedgerCode]
          };

          final[detail.LedgerCode].AccountCodeGroups[detail.LineNumber] = {
            name: detail.VoucherNumber,
            vdate: detail.VoucherDate,
            type: detail.BizTransactionTypeName,
            particulars: detail.VoucherDetailAccountName,
            debit: detail.Debit,
            credit: detail.Credit,
            balance: detail.RunningTotal,
          };
        });
        AccountCodes = Object.keys(final);

        AccountCodes.forEach((LedgerGroup) => {
          tableData.push({
            code: LedgerGroup,
            name: final[LedgerGroup].name,
            vdate: final[LedgerGroup].vdate,
            type: final[LedgerGroup].BizTransactionTypeName,
            particulars: final[LedgerGroup].VoucherDetailAccountName,
            debit: final[LedgerGroup].Debit,
            credit: final[LedgerGroup].Credit,
            balance: final[LedgerGroup].RunningTotal,
            bold: true
          });

          let check = Object.keys(final[LedgerGroup].AccountCodeGroups);
          check.forEach((AccountGroup) => {
            tableData.push({
              code: AccountGroup,
              name: final[LedgerGroup].AccountCodeGroups[AccountGroup].name,
              vdate: final[LedgerGroup].AccountCodeGroups[AccountGroup].vdate,
              type: final[LedgerGroup].AccountCodeGroups[AccountGroup].type,
              particulars: final[LedgerGroup].AccountCodeGroups[AccountGroup].particulars,
              debit: final[LedgerGroup].AccountCodeGroups[AccountGroup].debit,
              credit: final[LedgerGroup].AccountCodeGroups[AccountGroup].credit,
              balance: final[LedgerGroup].AccountCodeGroups[AccountGroup].balance,
            });
          });
        });
        this.statementofaccountarray = tableData;


      });
    });



  }


}


