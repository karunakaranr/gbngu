
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';


@Component({
    selector: 'app-receivablesdetail',
    templateUrl: './receivablesdetail.component.html',
    styleUrls: ['./receivablesdetail.component.scss'],
})
export class receivablesdetailComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    public TotalPending;
    public TotalUnadjusted;
    public TotalBalance;
    tabledata: any[];

    constructor(public store: Store,  @Inject(LOCALE_ID) public locale: string) { }
    ngOnInit(): void {
        this.getreceivablesdetail();
    }

    public getreceivablesdetail() {
        this.rowdatacommon$.subscribe(data => {
            this.rowData = data;
            console.log("Receivables Detail Reports :", this.rowData)

            this.TotalPending = 0;
            this.TotalUnadjusted = 0;
            this.TotalBalance = 0;
            let json = this.rowData;
            var finalizedArray = [];
            for (let data of this.rowData) {
                this.TotalPending = this.TotalPending + data.Receivable;
                this.TotalUnadjusted = this.TotalUnadjusted + data.Payable;
                this.TotalBalance = this.TotalBalance + data.Balance;
            }
            json.map((row) => {
                finalizedArray.push({
                    oucode: row['OUCode'],
                    ouname: row['OUName'],

                    rtcode: row['RouteCode'],
                    rtname: row['RouteName'],

                    pcccode: row['PriceCategoryCode'],
                    pcname: row['PriceCategoryName'],

                    grptype: row['GroupType'],


                    code: row['Code'],
                    name: row['Name'],

                    acccode: row['AccountCode'],
                    accname: row['AccountName'],
                    receivable: row['Receivable'],
                    payable: row['Payable'],
                    bal: row['Balance'],
                });
            });
            console.log("Json:",finalizedArray);
            const final = {};
            finalizedArray.forEach((detail) => {
                final[detail.code] = {
                    accountGroups: {},
                    acccode: detail.grptype === 'OU' ? detail.oucode :  detail.grptype === 'ROUTE' ? detail.rtcode : null,
                    accname: detail.grptype === 'OU' ? detail.ouname : detail.grptype === 'ROUTE' ? detail.rtname : null,
                    receivable: detail.receivable,
                    payable:detail.payable,
                    bal: detail.bal,
                    ...final[detail.code],
                };

                final[detail.code].accountGroups[detail.acccode] = {
                    acccode: detail.acccode,
                    accname: detail.accname,
                    receivable: detail.receivable,
                    payable: detail.payable,
                    bal: detail.bal,
                };
            });
            console.log("Final:",final);
            const grpcodes = Object.keys(final);

            const tableData = [];
            grpcodes.forEach((acccode) => {
                const accountGroups = Object.keys(final[acccode].accountGroups);
                let sumpending = 0;
                let sumunadjusted = 0;
                let sumbalance = 0;

                accountGroups.forEach((ag) => {
                    
                        sumpending = sumpending + parseFloat(final[acccode].accountGroups[ag].receivable);
                        sumunadjusted = sumunadjusted + parseFloat(final[acccode].accountGroups[ag].payable);
                        sumbalance = sumbalance + parseFloat(final[acccode].accountGroups[ag].bal);
                        
                    })
                    // sumpending = sumpending + parseFloat(final[acccode].accountGroups[ag].receivable);
                    // sumunadjusted = sumunadjusted + parseFloat(final[acccode].accountGroups[ag].payable);
                    // sumbalance = sumbalance + parseFloat(final[acccode].accountGroups[ag].bal);
                    
               
                console.log("sumpending:",sumpending)
                console.log("sumunadjusted:",sumunadjusted)
                console.log("sumbalance:",sumbalance)
                final[acccode].receivable = sumpending;
                final[acccode].payable = sumunadjusted;
                final[acccode].bal = sumbalance;
                
                tableData.push({
                    acccode: final[acccode].acccode,
                    accname: final[acccode].accname,
                    receivable: final[acccode].receivable,
                    payable: final[acccode].payable,
                    bal: final[acccode].bal,
                    bold: true,
                });
                console.log("Tabledata1:",tableData)
                
                const accounts = Object.keys(final[acccode].accountGroups);
                accounts.forEach((account) => {
                        tableData.push({
                            acccode: final[acccode].accountGroups[account].acccode,
                            accname: final[acccode].accountGroups[account].accname,
                            receivable: final[acccode].accountGroups[account].receivable,
                            payable: final[acccode].accountGroups[account].payable,
                            bal: final[acccode].accountGroups[account].bal,
                        })
                        });
                    });
                    this.tabledata = tableData;
                });
                
            }
        }
            
    
