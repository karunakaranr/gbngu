import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-paymentsummary',
    templateUrl: './paymentsummary.component.html',
    styleUrls: ['./paymentsummary.component.scss'],
})
export class paymentsummaryComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    public TotalDebit;
    public TotalCredit;
    totalamount: any[];

    constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string) { }
    ngOnInit(): void {
        this.getpaymentsummary();
    }

    public getpaymentsummary() {
        this.sharedService.getRowInfo().subscribe(data => {
            if (data.length > 0) {
            this.rowData = data;
            console.log("Payment Summary :", this.rowData)


            this.TotalDebit = 0;
            this.TotalCredit = 0;
            let json = this.rowData;
            var finalizedArray = [];
            for (let data of this.rowData) {
                this.TotalDebit = this.TotalDebit + data.Debit;
                this.TotalCredit = this.TotalCredit + data.Credit;
            }
            json.map((row,index) => {
                finalizedArray.push({
                    sno:index,
                    agcode: row['AccountGroupCode'],
                    agname: row['AccountGroupName'],
          
                    acccode: row['AccountCode'],
                    accname: row['AccountName'],
                    debit: row['Debit'],
                    credit: row['Credit'],
                });
            });

            const final = {};
            finalizedArray.forEach((detail) => {
                final[detail.agcode] = {
                    accountGroups: {},
                    acccode: detail.agcode,
                    accname: detail.agname,
                    debit: detail.debit,
                    credit: detail.credit,
                    
                    ...final[detail.agcode],
                };

                final[detail.agcode].accountGroups[detail.acccode] = {
                    acccode: detail.acccode,
                    accname: detail.accname,
                    debit: detail.debit,
                    credit: detail.credit,
                };
            });

            const grpcodes = Object.keys(final);

            const tableData = [];
            grpcodes.forEach((acccode) => {
                const accountGroups = Object.keys(final[acccode].accountGroups);
                let sumdebit = 0;
                let sumcredit = 0;

                accountGroups.forEach((ag) => {
                    sumdebit = sumdebit + parseFloat(final[acccode].accountGroups[ag].debit);
                    sumcredit = sumcredit + parseFloat(final[acccode].accountGroups[ag].credit);
                })
                final[acccode].debit = sumdebit;
                final[acccode].credit = sumcredit;

                tableData.push({
                    acccode: final[acccode].acccode,
                    accname: final[acccode].accname,
                    debit: final[acccode].debit,
                    credit: final[acccode].credit,
                    bold: true,
                });

                const accounts = Object.keys(final[acccode].accountGroups);
                accounts.forEach((account) => {
                    tableData.push({
                        acccode: final[acccode].accountGroups[account].acccode,
                        accname: final[acccode].accountGroups[account].accname,
                        debit: final[acccode].accountGroups[account].debit,
                        credit: final[acccode].accountGroups[account].credit,
                    });
                });
            });
            this.totalamount = tableData;
            console.log("Summary Register Data:", tableData)
        }
        });
    }
}
