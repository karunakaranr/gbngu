
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-tbscheduledetail',
    templateUrl: './tbscheduledetail.component.html',
    styleUrls: ['./tbscheduledetail.component.scss'],
})
export class tbscheduledetailComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    constructor(public sharedService: SharedService,public store: Store,  @Inject(LOCALE_ID) public locale: string) { }
    ngOnInit(): void {
        this.gettbscheduledetail();
    }

    public gettbscheduledetail() {
        this.sharedService.getRowInfo().subscribe(data => {
            if (data.length > 0) {
            this.rowData = data;
            console.log("Trial Balance Schedule Detail :", this.rowData)
            }
        });
    }
}


