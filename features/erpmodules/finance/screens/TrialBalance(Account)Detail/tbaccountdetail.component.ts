
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Drilldownsetting } from 'features/commonreport/datastore/commonreport.action';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-tbaccountdetail',
    templateUrl: './tbaccountdetail.component.html',
    styleUrls: ['./tbaccountdetail.component.scss'],
})
export class tbaccountdetailComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    @Select(ReportState.Sourcecriteriadrilldown) sourcerowcriteria$: Observable<any>;
    public rowData: any;
    public TotalOpening;
    public TotalDebit;
    public TotalCredit;
    public TotalClosing;
    tabledata: any[];
    constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string) { }
    ngOnInit(): void {
        this.gettbaccountdetail();
    }

    public gettbaccountdetail() {
        this.sharedService.getRowInfo().subscribe(data => {
            if (data.length > 0) { 
            this.rowData = data;
            console.log("Trial Balance Account Detail :", this.rowData)

            this.TotalOpening = 0;
            this.TotalDebit = 0;
            this.TotalCredit = 0;
            this.TotalClosing = 0;
            let json = this.rowData;
            var finalizedArray = [];

            for (let data of this.rowData) {
                this.TotalOpening = this.TotalOpening + data.Opening;
                this.TotalDebit = this.TotalDebit + data.Debit;
                this.TotalCredit = this.TotalCredit + data.Credit;
                this.TotalClosing = this.TotalClosing + data.Closing;
            }

            json.map((row) => {
                finalizedArray.push({
                    AccountId:row['AccountId'],

                    ascode: row['AccountScheduleCode'],
                    asname: row['AccountScheduleName'],
                    agcode: row['AccountGroupCode'],
                    agname: row['AccountGroupName'],

                    acccode: row['AccountCode'],
                    accname: row['AccountName'],
                    opg: row['Opening'],
                    debit: row['Debit'],
                    credit: row['Credit'],
                    cls: row['Closing'],
                });
            });

            const final = {};
            finalizedArray.forEach((detail) => {
                final[detail.ascode] = {
                    accountGroups: {},
                    acccode: detail.ascode,
                    accname: detail.asname,
                    opg: detail.opg,
                    debit: detail.debit,
                    credit: detail.credit,
                    cls: detail.cls,
                    AccountId:"",
                    ...final[detail.ascode],
                };

                final[detail.ascode].accountGroups[detail.agcode] = {
                    accounts: {},
                    acccode: detail.agcode,
                    accname: detail.agname,
                    opg: detail.opg,
                    debit: detail.debit,
                    credit: detail.credit,
                    cls: detail.cls,
                    AccountId:"",
                    ...final[detail.ascode].accountGroups[detail.agcode],
                };

                final[detail.ascode].accountGroups[detail.agcode].accounts[detail.acccode] = {
                    acccode: detail.acccode,
                    accname: detail.accname,
                    opg: detail.opg,
                    debit: detail.debit,
                    credit: detail.credit,
                    cls: detail.cls,
                    AccountId:detail.AccountId,
                };
            });

            const grpcodes = Object.keys(final);

            const tableData = [];
            grpcodes.forEach((acccode) => {
                const accountGroups = Object.keys(final[acccode].accountGroups);

                 // Initialize totals for the top-level account schedule
                 let acccodeTotalOpening = 0;
                 let acccodeTotalDebit = 0;
                 let acccodeTotalCredit = 0;
                 let acccodeTotalClosing = 0;


                 accountGroups.forEach((ag) => {
                let sumopening = 0;
                let sumdebit = 0;
                let sumcredit = 0;
                let sumclosing = 0;

                const accounts = Object.keys(final[acccode].accountGroups[ag].accounts);
                    accounts.forEach(account => {
                    sumopening = sumopening + parseFloat(final[acccode].accountGroups[ag].accounts[account].opg);
                    sumdebit = sumdebit + parseFloat(final[acccode].accountGroups[ag].accounts[account].debit);
                    sumcredit = sumcredit + parseFloat(final[acccode].accountGroups[ag].accounts[account].credit);
                    sumclosing = sumclosing + parseFloat(final[acccode].accountGroups[ag].accounts[account].cls);
                })
                final[acccode].accountGroups[ag].opg = sumopening;
                final[acccode].accountGroups[ag].debit = sumdebit;
                final[acccode].accountGroups[ag].credit = sumcredit;
                final[acccode].accountGroups[ag].cls = sumclosing;


                 // Update totals for the current account group
                 acccodeTotalOpening += sumopening;
                 acccodeTotalDebit += sumdebit;
                 acccodeTotalCredit += sumcredit;
                 acccodeTotalClosing += sumclosing;

                tableData.push({
                    acccode: final[acccode].accountGroups[ag].acccode,
                    accname: final[acccode].accountGroups[ag].accname,
                    opg: final[acccode].accountGroups[ag].opg,
                    debit: final[acccode].accountGroups[ag].debit,
                    credit: final[acccode].accountGroups[ag].credit,
                    cls: final[acccode].accountGroups[ag].cls,
                    AccountId:"",
                    bold: true,
                });

                // Update totals for the current account schedule
                final[acccode].opg = acccodeTotalOpening;
                final[acccode].debit = acccodeTotalDebit;
                final[acccode].credit = acccodeTotalCredit;
                final[acccode].cls = acccodeTotalClosing;

                tableData.push({
                    acccode: final[acccode].acccode,
                    accname: final[acccode].accname,
                    opg: final[acccode].opg,
                    debit: final[acccode].debit,
                    credit: final[acccode].credit,
                    cls: final[acccode].cls,
                    AccountId:"",
                    bold: true,
                });

                // const accounts = Object.keys(final[acccode].accountGroups);
                accounts.forEach((account) => {
                    tableData.push({
                        acccode: final[acccode].accountGroups[ag].accounts[account].acccode,
                        accname: final[acccode].accountGroups[ag].accounts[account].accname,
                        opg: final[acccode].accountGroups[ag].accounts[account].opg,
                        debit: final[acccode].accountGroups[ag].accounts[account].debit,
                        credit: final[acccode].accountGroups[ag].accounts[account].credit,
                        cls: final[acccode].accountGroups[ag].accounts[account].cls,
                        AccountId: final[acccode].accountGroups[ag].accounts[account].AccountId,
                    });
                });
            }); 
        });
            this.tabledata = tableData;
            console.log("Final Data:", this.rowData);
    }
        });
    }

    private handleSourceRowCriteria(selectedrowdatavalue) {
    

        console.log("VoucherDetailAccountId",selectedrowdatavalue.AccountId)
        this.sourcerowcriteria$.subscribe(data => {
          let destinatopnfieldmapname = "VoucherDetailAccountId"
          let selectedrowdataid= selectedrowdatavalue.AccountId;
          let sourcecriteria = data.CriteriaDTO;
          let drillmenuid = -1399996346;
          let drilldownsettingdata = [selectedrowdataid, sourcecriteria, drillmenuid,destinatopnfieldmapname];
    
          
          this.store.dispatch(new Drilldownsetting(drilldownsettingdata));
    
          console.log('Clicked on Document Number:', selectedrowdatavalue);
        });
      }
}


