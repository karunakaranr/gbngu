
import { GBBaseDataService, GBBaseDataServiceWN} from './../../../../../libs/gbdata/src/lib/services/gbbasedata.service';
import {GBBaseDBDataService} from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service'
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {GbToasterService} from './../../../../../libs/gbcommon/src/lib/services/toaster/gbtoaster.service';
import { PartyWithBranch } from '../../models/IPartyWithBranch';
import { PartyWithBranchService } from '../../services/PartyWithBranch/PartyWithBranch.service';
import { PartyWithBranchDBService } from '../../dbservices/PartyWithBranch/PartyWithBranchDB.service';
import { Store } from '@ngxs/store';
import { FormBuilder } from '@angular/forms';
import {GBDataPageService, GBDataPageServiceWN} from './../../../../../libs/uicore/src/lib/services/gbpage.service';
import { ActivatedRoute, Router } from '@angular/router';

export function getThisDBDataService(http: GBHttpService): GBBaseDBDataService<PartyWithBranch> {
  return new PartyWithBranchDBService(http);
}
export function getThisDataServiceWN(dbDataService: GBBaseDBDataService<PartyWithBranch>): GBBaseDataServiceWN<PartyWithBranch> {
  return new PartyWithBranchService(dbDataService);
}
export function getThisDataService(dbDataService: GBBaseDBDataService<PartyWithBranch>): GBBaseDataService<PartyWithBranch> {
  return new PartyWithBranchService(dbDataService);
}
export function getThisGBPageService(store: Store, dataService: GBBaseDataService<PartyWithBranch>, dbDataService: GBBaseDBDataService<PartyWithBranch>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster:GbToasterService): GBDataPageService<PartyWithBranch> {
  return new GBDataPageService<PartyWithBranch>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
export function getThisGBPageServiceWN(store: Store, dataService: GBBaseDataServiceWN<PartyWithBranch>, dbDataService: GBBaseDBDataService<PartyWithBranch>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router, toaster:GbToasterService): GBDataPageServiceWN<PartyWithBranch> {
  return new GBDataPageServiceWN<PartyWithBranch>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
