import { PartyWithBranch } from '../../models/IPartyWithBranch';
import { GBDataFormGroupStoreWN, GBDataFormGroupWN } from './../../../../../libs/uicore/src/lib/classes/GBFormGroup';
import { HttpClient } from '@angular/common/http';
import { PartyWithBranchService } from '../../services/PartyWithBranch/PartyWithBranch.service';
import { Store } from '@ngxs/store';
import { PartyWithBranchStateActionFactory } from '../../stores/PartyWithBranch/PartyWithBranch.actionfactory';

export class PartyWithBranchFormgroup extends GBDataFormGroupWN<PartyWithBranch> {
  constructor(http: HttpClient, dataService: PartyWithBranchService) {
    super(http, 'SC0020', {}, dataService)
  }

  public clear() {
    super.clear();
  }

  public delete() {
    super.delete();
  }

  public save() {
    super.save();
  }

  public moveFirst() {
    super.moveFirst();
  }

  public movePrev() {
    super.movePrev();
  }

  public moveNext() {
    super.moveNext();
  }

  public moveLast() {
    super.moveLast();
  }
}
export class PartyWithBranchFormgroupStore extends GBDataFormGroupStoreWN<PartyWithBranch> {
  constructor(http: HttpClient, store: Store) {
    super(http, 'SC0020', {}, store, new PartyWithBranchStateActionFactory())
  }

  public clear() {
    super.clear();
  }

  public delete() {
    super.delete();
  }

  public save() {
    super.save();
  }

  public moveFirst() {
    super.moveFirst();
  }

  public movePrev() {
    super.movePrev();
  }

  public moveNext() {
    super.moveNext();
  }

  public moveLast() {
    super.moveLast();
  }
}

