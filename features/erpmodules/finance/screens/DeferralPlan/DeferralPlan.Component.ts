import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';

import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { URLS } from '../../URLS/urls';
import { DeferalPlanService } from '../../services/DeferralPlan/DeferralPlan.Service';
import { IDeferralPlan } from '../../models/IDeferralPlan';
import * as DeferralPlanJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/DeferralPlan.json'
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';



@Component({
  selector: 'app-DeferralPlan',
  templateUrl: './DeferralPlan.component.html',
  styleUrls: ['./DeferralPlan.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'DeferalPlanId'},
    { provide: 'url', useValue: URLS.DeferralType },
    { provide: 'DataService', useClass: DeferalPlanService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class DeferralPlanComponent extends GBBaseDataPageComponentWN<IDeferralPlan> {
  title: string = 'DeferralPlan'
  DeferralPlanJSON = DeferralPlanJSON;


  form: GBDataFormGroupWN<IDeferralPlan > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'DeferralPlan', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.DeferalPlanId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }

  validatePercent(event: any): void {
    let value = event.target.value;

    value = Number(value);

    if (value > 100) {
      this.form.get('DeferalPlanDeferalPercent').setValue(0);
      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: 'Please Enter Valid Percentage.',
          heading: 'Info',
        }
      });
    } 
    else {
      this.form.get('DeferalPlanDeferalPercent').setValue(value);
    }
  }

  
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IDeferralPlan > {
  const dbds: GBBaseDBDataService<IDeferralPlan > = new GBBaseDBDataService<IDeferralPlan >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IDeferralPlan >, dbDataService: GBBaseDBDataService<IDeferralPlan >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IDeferralPlan > {
  return new GBDataPageService<IDeferralPlan >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


