import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { DateFormaterPipe } from 'libs/gbcommon/src/lib/Date-Formater.pipe';


@Component({
  selector: 'app-registerdetail',
  templateUrl: './registerdetail.component.html',
  styleUrls: ['./registerdetail.component.scss'],
})
export class registerdetailComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  private dateFormatter: DateFormaterPipe = new DateFormaterPipe(this.store);

  public rowData = [];
  public TotalCredit;
  public TotalDebit;
  register: any[];
  showNarration: boolean = false; // Declare the showNarration property and initialize it
  constructor(public sharedService: SharedService,public store: Store) { }

  ngOnInit(): void {
    this.registerdetail();
  }
  
  public registerdetail() {
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Register Detail Reports :", this.rowData)

      this.TotalCredit = 0;
      this.TotalDebit = 0;
      let json = this.rowData;
      var finalizedArray = [];
      for (let data of this.rowData) {
          this.TotalCredit = this.TotalCredit + data.Credit;
          this.TotalDebit = this.TotalDebit + data.Debit;
      }
      json.map((row,index) => {
          finalizedArray.push({
              sno: index,
              vdate: this.dateFormatter.transform(row['VoucherDate'], 'date'),
              biztypecode: row['BizTransactionTypeCode'],
              
              vnarration: row['VoucherNarration'],

              vno: row['VoucherNumber'],
              vaccno: row['VoucherAccountName'],
              vrefno: row['VoucherReferenceNumber'],
              vrefdate: row['VoucherReferenceDate'],
              debit: row['Debit'],
              credit: row['Credit']
          });
      });

      const final = {};
      finalizedArray.forEach((detail) => {
          final[detail.vdate] = {
              accountGroups: {},
              vno: detail.vdate,
              vaccno: detail.biztypecode,
              vnarration: detail.vnarration,
              vrefno: "",
              vrefdate: "",
              debit: 0,
              credit: 0,
              ...final[detail.vdate],
          };

          final[detail.vdate].accountGroups[detail.sno] = {
            vno: detail.vno,
            vaccno: detail.vaccno,
            vnarration: detail.vnarration,
            vrefno: detail.vrefno,
            vrefdate: detail.vrefdate,
            debit: detail.debit,
            credit: detail.credit,
          };
      });
console.log("finalizedArray",finalizedArray)
      const grpcodes = Object.keys(final);

      const tableData = [];
      grpcodes.forEach((vdate) => {
          const accountGroups = Object.keys(final[vdate].accountGroups);
          let sumcredit = 0;
          let sumdebit = 0;

          accountGroups.forEach((ag) => {
              sumcredit = sumcredit + parseFloat(final[vdate].accountGroups[ag].credit);
              sumdebit = sumdebit + parseFloat(final[vdate].accountGroups[ag].debit);
          })
          final[vdate].credit = sumcredit;
          final[vdate].debit = sumdebit;

          tableData.push({
            vno: final[vdate].vno,
            vaccno: final[vdate].vaccno,
            vnarration: "",
            vrefno: "",
            vrefdate: "",
            debit: final[vdate].debit,
            credit: final[vdate].credit,
              bold: true,
          });

          const accounts = Object.keys(final[vdate].accountGroups);
          accounts.forEach((account) => {
              tableData.push({
                vno: final[vdate].accountGroups[account].vno,
                vaccno: final[vdate].accountGroups[account].vaccno,
                vnarration: final[vdate].accountGroups[account].vnarration,
                vrefno: final[vdate].accountGroups[account].vrefno,
                vrefdate: final[vdate].accountGroups[account].vrefdate,
                debit: final[vdate].accountGroups[account].debit,
                credit: final[vdate].accountGroups[account].credit,
              });
          });
      });
      this.register = tableData;
      console.log("Register Detail Data:", this.rowData)
    }
    });
  }

}
