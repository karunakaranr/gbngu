export interface IChequeBook {
    ChequeBookId: number
    BankId: number
    BankCode: string
    BankName: string
    InstrumentId: number
    InstrumentName: string
    ChequeBookBookNo: string
    ChequeBookNoOfLeaves: number
    ChequeBookStartNo: number
    ChequeBookRemarks: string
    ChequeBookBookType: number
    ChequeBookChequePrefix: string
    ChequeBookStatus: number
    ChequeBookVersion: number
    ChequeBookCreatedById: number
    ChequeBookCreatedOn: string
    ChequeBookModifiedById: number
    ChequeBookModifiedOn: string
    ChequeBookDetailArray: ChequeBookDetailArray[]
  }
  
  export interface ChequeBookDetailArray {
    ChequeBookDetailId: number
    ChequeBookId: number
    ChequeBookDetailChequeNo: string
    ChequeBookDetailStatus: number
    ChequeBookDetailCancelReason: string
  }
  