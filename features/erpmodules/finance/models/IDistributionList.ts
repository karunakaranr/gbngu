export interface IDistributionList {
    DistributionListId: number
    DistributionListCode: string
    DistributionListName: string
    DistributionListSortOrder: number
    DistributionListStatus: number
    DistributionListVersion: number
    DistributionListCreatedById: number
    DistributionListCreatedByName: string
    DistributionListCreatedOn: string
    DistributionListModifiedById: number
    DistributionListModifiedByName: string
    DistributionListModifiedOn: string
    DistributionListSourceType: number
    DistributionListDetailArray: DistributionListDetailArray[]
  }
  
  export interface DistributionListDetailArray {
    DistributionListDetailSlNo: string
    DistributionListId: string
    DistributionListDetailId: string
    undefined: string
    OuId: string
    OuName: string
    DistributionListDetailContactIds: number
    DistributionListDetailMailIds: string
  }
  