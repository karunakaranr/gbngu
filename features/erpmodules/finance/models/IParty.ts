export interface IParty {
    PartyCode: string
    PartyName: string
    PartyShortName: string
    PartyIsVendor: number
    PartyIsServiceProvider: number
    PartyIsAccount: number
    PartyIsCustomer: number
    PartyIsProspect: number
    PartyIsBank: number
    ControlAccountId: string
    AccountLinkId: number
    CodeDefineId: string
    ControlAccountCodeDefineId: number
    AccountGroupId: number
    CurrencyId: number
    PartySortOrder: number
    PartySourceType: number
    DefaultAddressId: number
    PartyVersion: number
    PartyStatus: string
    PartyFavouringName: string
  }