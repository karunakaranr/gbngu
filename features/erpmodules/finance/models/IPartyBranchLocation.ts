export interface IPartyBranchLocation {
  PartyBranchLocationId: number
  PartyBranchLocationCode: string
  PartyBranchLocationName: string
  PartyBranchId: number
  PartyBranchCode: string
  PartyBranchName: string
  PartyBranchShortName: string
  ParentLocationId: number
  ParentLocationCode: string
  ParentLocationName: string
  PartyBranchLocationInchargeId: number
  PartyBranchLocationNature: string
  PartyBranchLocationLevels: number
  PartyBranchLocationParentIds: string
  PartyBranchLocationCreatedById: number
  PartyBranchLocationCreatedOn: string
  PartyBranchLocationModifiedById: number
  PartyBranchLocationModifiedOn: string
  PartyBranchLocationSortOrder: number
  PartyBranchLocationStatus: number
  PartyBranchLocationVersion: number
  }