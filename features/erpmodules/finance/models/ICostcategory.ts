export interface ICostCategory {
    CostCategoryId: number,
    CostCategoryCode: string,
    CostCategoryName: string,
    CostCategoryShortName: string,
    CostCategoryCostCenterTypeId: number,
    CostCategoryCostCenterTypeCode: string,
    CostCategoryCostCenterTypeName: string,
    CostCategoryCreatedById: number,
    CostCategoryCreatedOn: string,
    CostCategoryCreatedByName: string,
    CostCategoryModifiedById: number,
    CostCategoryModifiedOn: string,
    CostCategoryModifiedByName: string,
    CostCategorySortOrder: number,
    CostCategoryStatus: number,
    CostCategoryVersion: number,
    CostCategoryConditionArray: CostCategoryConditionArrays[]
}

export interface CostCategoryConditionArrays{
    CostCategoryConditionId: number,
    CostCategoryId: number,
    CostCategoryCode: string,
    CostCategoryName: string,
    CostCategoryShortName: string,
    CostCategoryConditionType: number,
    AccountGroupId: number,
    AccountGroupCode: string,
    AccountGroupName: string,
    AccountGroupShortName: string,
    AccountGroupNature: number,
    AccountGroupAccountGroupAccountScheduleNature: number,
    CostCategoryConditionNature: number
}
