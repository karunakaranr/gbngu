export interface IEBank {
  AccountNumber: string
  SecretKey: number
  AccountHolderName: string
  BankIFSCCode: string
  BankName: string
  BankBranch: string
  BranchCode: string
}