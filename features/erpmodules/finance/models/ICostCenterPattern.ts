export interface ICostCenterPattern {
    CostCenterPatternId: number
    CostCenterPatternCode: string
    CostCenterPatternName: string
    CostCenterPatternRemarks: string
    CostCenterTypeId: string
    CostCenterPatternVersion: number
    CostCenterPatternStatus: number
    CostCenterPatternDetailArray: CostCenterPatternDetailArray[]
  }
  
  export interface CostCenterPatternDetailArray {
    CostCenterPatternDetailId: string
    CostCenterPatternDetailSlNo: number
    undefined: string
    CostCenterId: string
    CostCenterCode: string
    CostCenterName: string
    CostCenterPatternDetailAllocationPercentage: string
  }
  