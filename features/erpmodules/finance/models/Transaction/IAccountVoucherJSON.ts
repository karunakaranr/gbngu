export interface IAccountVoucherJSON {
    default?:any
    ObjectFields?: any
  }
  
  export interface ObjectField {
    BizTransactionTypeName?: BizTransactionTypeName
    Voucher?: Voucher
    Name?: string
    Type?: string
    Required?: boolean
    ValidRegEx?: string
    DefaultValue: any
    PostData?: boolean
    Label?: string
    MaxLength?: number
    Width?: number
  }
  
  export interface BizTransactionTypeName {
    PicklistUrl: string
    PicklistDisplayText: string
    PicklistSearchFields: string
    Multiple: boolean
    LinkId: string
    PicklistSelection: string
    PicklistTitle: string
    Label: string
    Width: number
    AddNew: boolean
    MaxLength: number
    ColumnWidth: string
    ColumnType: string
    isKeyField: boolean
    PicklistCriteria: string
  }
  
  export interface Voucher {
    PicklistUrl: string
    PicklistDisplayText: string
    PicklistSearchFields: string
    Multiple: boolean
    PicklistSelection: string
    PicklistTitle: string
    Label: string
    Width: number
    AddNew: boolean
    MaxLength: number
    ColumnWidth: string
    ColumnType: string
    LinkId: string
    isKeyField: boolean
    PicklistCriteria: string
  }
  