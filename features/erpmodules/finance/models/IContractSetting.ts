export interface IContractSetting {
    ContractSettingId: number
    PartyId: string
    PartyBranchId: string
    ContractSettingVersionNo: number
    ContractSettingEffectiveFrom: string
    ContractSettingEffectiveTo: string
    ContractSettingNumber: string
    ContractSettingReferenceNumber: string
    ContractStatusId: string
    ContractSettingPricingType: string
    ContractSettingBasePrice: string
    ContractSettingChangePrice: string
    ContractSettingChangePercent: string
    ContractSettingIsFirstHitchCondition: string
    ContractSettingVolumeDiscountType: string
    ContractSettingEligibleVolume: string
    ContractSettingVolumeDiscountPercent: string
    ContractSettingMaxVolumeDiscountPercent: string
    ContractSettingRemarks: string
    ContractSettingStatus: number
    ContractSettingVersion: number
}