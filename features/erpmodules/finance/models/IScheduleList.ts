export interface ISchedule {
    map(arg0: (row: any) => void): unknown;
    "AccountScheduleId": number,
    "AccountScheduleCode": string,
    "AccountScheduleName": string,
    "AccountScheduleShortName": string,
    "AccountScheduleNature": number,
    "AccountScheduleNatureName": string,
    "ParentId": number,
    "ParentCode": string,
    "ParentName": string,
    "AccountScheduleCreatedById": number,
    "AccountScheduleCreatedByName": any,
    "AccountScheduleCreatedOn": any,
    "AccountScheduleModifiedById": number,
    "AccountScheduleModifiedByName": any,
    "AccountScheduleModifiedOn": any,
    "AccountScheduleSortOrder": number,
    "AccountScheduleStatus": number,
    "AccountScheduleVersion": number,
    "AccountScheduleSourceType": number
}