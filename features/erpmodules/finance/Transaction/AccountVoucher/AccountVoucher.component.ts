import { Component } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { AccountVoucherService } from '../../services/Transaction/AccountVoucher/AccountVoucher.service';
import { IAccountVoucher,VoucherDetailArray, VoucherInstrumentArray } from '../../models/Transaction/IAccountVoucher';
import { IPickListValue } from '../../models/IPickListPatch';
import { IAccountVoucherJSON } from '../../models/Transaction/IAccountVoucherJSON';
import { IGridLightBoxDetail } from '../../models/IGridLightBoxDetail';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as AccountVoucherJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/AccountVoucher.json';
import { URLS } from '../../URLS/urls';

@Component({
  selector: 'app-AccountVoucher',
  templateUrl: './AccountVoucher.component.html',
  styleUrls: ['./AccountVoucher.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'VoucherId'},
    { provide: 'url', useValue: URLS.Voucher },
    { provide: 'DataService', useClass: AccountVoucherService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class AccountVoucherComponent extends GBBaseDataPageComponentWN<IAccountVoucher> {
  title: string = 'AccountVoucher'
  AccountVoucherJSON : IAccountVoucherJSON = AccountVoucherJSON;
  InstrumentArray : VoucherInstrumentArray;
  orderForm: FormGroup;
  items: FormArray;
  FormGridIndex: number;
  form: GBDataFormGroupWN<IAccountVoucher> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'AccountVoucher', {}, this.gbps.dataService);
  thisConstructor() {
      this.store.dispatch(new ButtonVisibility("Forms"))
  }

  public AccountVoucherFillFunction(VoucherId: string): void {
    if (VoucherId) {
      this.gbps.dataService.getData(VoucherId).subscribe((AccountVoucher:IAccountVoucher) => {
        this.form.patchValue(AccountVoucher);
        this.DebitAndCreditCalculator(AccountVoucher.VoucherDetailArray);
      })
    }
  }

  public DebitAndCreditCalculator(VoucherArrayDetail:VoucherDetailArray[]):void{
    let TotalDebit : number = 0;
    let TotalCredit : number = 0;
    for(let data of VoucherArrayDetail){
      if(data.VoucherDetailDetailType == 0){
        TotalDebit = TotalDebit + data.VoucherDetailAmount
      } else if(data.VoucherDetailDetailType == 1){
        TotalCredit = TotalCredit + data.VoucherDetailAmount
      }
    }
    this.form.get('TotalDebit').patchValue(TotalDebit);
    this.form.get('TotalCredit').patchValue(TotalCredit);
  }

  public InstrumentLightBox(LightBoxDetail:IGridLightBoxDetail): void{
    if(LightBoxDetail.FieldValue == 4){
      this.FormGridIndex = LightBoxDetail.index
      if(this.form.value.VoucherDetailArray[LightBoxDetail.index].VoucherInstrumentArray != null){
        this.InstrumentArray = this.form.value.VoucherDetailArray[LightBoxDetail.index].VoucherInstrumentArray
        this.form.get('VoucherInstrumentArray').patchValue(this.InstrumentArray)
      }
      document.getElementById("myModal").style.display = "block";
    }
  }

  public saveInstrumentLightBox() : void{
      let VoucherDetailArray = JSON.stringify(this.form.get('VoucherDetailArray').value)
      let VoucherDetailArray1 = JSON.parse(VoucherDetailArray)
      VoucherDetailArray1.forEach((field:VoucherDetailArray, fieldIndex:number) => {
        if(fieldIndex == this.FormGridIndex){
          field.VoucherInstrumentArray = this.form.get('VoucherInstrumentArray').value
        }
      })
      this.form.get('VoucherDetailArray').patchValue(VoucherDetailArray1)
      document.getElementById("myModal").style.display = "none";
  }

  public LightBoxClose(): void{
    document.getElementById("myModal").style.display = "none";
  }

  public PicklistPatchValue(PickListValue: IPickListValue): void {
    this.form.get(PickListValue.id).patchValue(PickListValue.SelectedData)
  }
}

export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IAccountVoucher> {
  const dbds: GBBaseDBDataService<IAccountVoucher> = new GBBaseDBDataService<IAccountVoucher>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IAccountVoucher>, dbDataService: GBBaseDBDataService<IAccountVoucher>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IAccountVoucher> {
  return new GBDataPageService<IAccountVoucher>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
