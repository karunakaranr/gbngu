export class URLS {
    public static PartyWithBranch = '/as/Party.svc/';
    public static CostCategory = '/as/CostCategory.svc/';
    public static InsuranceMaster = '/as/Insurance.svc/';
    public static Instrument = '/as/Instrument.svc/';
    public static Account = '/as/Account.svc/';
    public static OU = '/ads/UserAccessRights.svc/';
    public static OUGroup = '/ads/OrganizationGroup.svc/';
    public static DistributionList = "/as/DistributionList.svc/";
    public static CostCenterPattern = "/as/CostCenterPattern.svc/";
    public static AccountSchedule = "/as/AccountSchedule.svc/";
    public static Party = "/as/Party.svc/";
    public static ContractSetting = "/as/ContractSetting.svc/";
    public static Voucher = '/as/Voucher.svc/';
    public static PartyBranchLocation: "/fms/PartyBranchLocation.svc/";
    public static TDSCategoryMaster = '/as/TDSCategory.svc/';
    public static BudgetMaster = '/as/Budget.svc/';
    public static CostCenter = '/as/CostCenter.svc/';
    public static Bank = '/fws/GCM.svc/';
    public static Picklist = '/as/AccountGroup.svc/SelectList';
    public static DeferralType = "/as/DeferalPlan.svc/";
    public static ChequeBook = '/as/ChequeBook.svc/';
    public static PartyOU = '/cs/Criteria.svc/List/?ObjectCode=PARTYACCOUNTOU';
    public static PartyOUsave = '/as/PartyOU.svc/';
    public static Narration = '/as/Narration.svc/'
    public static WarehouseOU = '/ads/OrganizationUnit.svc/'
    public static BudgetSave = '/gb4/as/BudgetData.svc/';
    public static BudgetData = '/cs/Criteria.svc/List/?ObjectCode=BUDGETDATA'
} 