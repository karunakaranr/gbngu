import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StatementOfAccountReportComponent } from './screens/StatementofAccount/StatementofAccountReport/StatementofAccountReport.component';

import { FinanceComponent } from './finance.component';
import { PartyBranchLocationComponent } from './screens/PartyBranchLocation/PartyBranchLocation.component';
import { PartyBranchListComponent } from './screens/AccountList/PartyBranchList/PartyBranch.component'
import { AccountListComponent } from './screens/AccountList/AccountList/AccountList.component'
import { AccountScheduleComponent } from './screens/AccountList/AccountSchedule/AccountSchedule.component'
import { AccountScheduleFormComponent} from './screens/AccountSchedule/AccountSchedule.component';
import { AccountGroupComponent } from './screens/AccountList/AccountGroup/AccountGroup.component'
import { CompanyVsLedgerComponent } from './screens/CompnayVsLedger/CompanyVsLedger.component'
import { ContractSettingComponent } from './screens/ContractSetting/ContractSetting.component';
import { InstrumentListComponent } from './screens/InstrumentList/InstrumentList.component'
import { InsuranceListComponent } from'./screens/InsuranceList/InsuranceList.component'
import { TDSContactComponent } from './screens/TDSContact/TDSContact.component';
import { PartyWithBranchComponent } from './screens/PartyWithBranch/PartyWithBranch.component';
import { CashBookComponent } from './screens/Books/CashBook/CashBook.component';
import { schedulelistComponent } from './screens/Schedule/ScheduleList/schedulelist.component';
import { paymentsummaryComponent } from './screens/Payment/PaymentSummary/paymentsummary.component';
import { tbscheduleComponent } from './screens/TrialBalanceSchedule/tbschedule.component';
import { tbgroupComponent } from './screens/TrialBalanceGroup/tbgroup.component';
import { tbaccountComponent } from './screens/TrialBalanceAccount/tbaccount.component';
import { tbscheduledetailComponent } from './screens/TrialBalance(Schedule)Detail/tbscheduledetail.component';
// import { CostCategoryComponent } from './screens/CostCategory/CostCategory.component';

import { CostCenterComponent } from './screens/CostCenter/CostCenter.component';
import { tbgroupdetailComponent } from './screens/TrialBalance(Group)Detail/tbgroupdetail.component';
import { tbaccountdetailComponent } from './screens/TrialBalance(Account)Detail/tbaccountdetail.component';
import { InstrumentComponent } from './screens/Instrument/Instrument.component';
import { receivablebillwiseComponent } from './screens/ReceivablesBillwise/receivablebillwise.component';
import { receivablesdetailComponent } from './screens/ReceivablesDetail/receivablesdetail.component';
import { receivablessummaryComponent } from './screens/ReceivablesSummary/receivablessummary.component';
import { bankbookComponent } from './screens/BankBook/bankbook.component';
import { soasingleComponent } from './screens/StatementofAccount(Single)/soasingle.component';
import { daybookComponent } from './screens/DayBook/DayBook.component';
import { deductedcategoryComponent } from './screens/TDS/DeductedCategory/deductedcategory.component';
import { registerdetailComponent } from './screens/RegisterDetail/RegisterDetailSummary/registerdetail.component';
import { brssummaryComponent } from './screens/BRSSummary/BRSSummaryView/brssummary.component';
import { brsdetailComponent } from './screens/BRSDetail/BRSDetailView/brsdetail.component';
import { brsregisterComponent } from './screens/BRSRegister/BRSRegisterView/brsregister.component';
import { BankComponent } from './screens/Bank/Bank.component';
import { PartyComponent } from './screens/Party/Party.component';
import { TDSCategoryMasterComponent } from './screens/TDSCategoryMaster/TDSCategoryMaster.component';
import { BudgetMasterComponent } from './screens/BudgetMaster/BudgetMaster.component';
import { InsuranceMasterComponent } from './screens/InsuranceMaster/InsuranceMaster.component';
import { DistributionListComponent } from './screens/DistributionList/DistributionList.component';
import { CostCategoryComponent } from './screens/CostCategory/costcategory.component';
import { CostCenterPatternComponent } from './screens/CostCenterPattern/CostCenterPattern.component';
import { ebankComponent } from './screens/E-Bank/E-Bank/ebank.component';
import { AccountVoucherComponent } from './Transaction/AccountVoucher/AccountVoucher.component';
import { AccountMasterComponent } from './screens/AccountMaster/AccountMaster.component';
import { DeferralPlanComponent } from './screens/DeferralPlan/DeferralPlan.Component';
import { ChequeBookComponent } from './screens/ChequeBook/chequebook.component';
import { PartyOUComponent } from './screens/PartyOU/partyou.component';
import { NarrationMasterComponent } from './screens/NarrationMaster/NarrationMaster.component';
import { BudgetDataComponent } from './screens/BudgetData/BudgetData.component';
const routes: Routes = [
  {
    path: 'budgetdata',
    component: BudgetDataComponent
  },
  {
    path: 'narration',
    component: NarrationMasterComponent
  },
  {
    path: '',
    component: FinanceComponent
  },
  {
    path: 'ChequeBook',
    component: ChequeBookComponent
  },
  {
    path: 'partyou',
    component: PartyOUComponent
  },
  {
    path: 'DeferralPlan',
    component: DeferralPlanComponent
  },
  {
    path: 'accountmaster',
    component: AccountMasterComponent
  },
  {
    path: 'party',
    component: PartyComponent
  },
  {
    path: 'costcategory',
    component: CostCategoryComponent
  },
  {
    path: 'voucher',
    component: AccountVoucherComponent
  },
  {
    path: 'costcenterpattern',
    component: CostCenterPatternComponent
  },
  {
    path:'distributionlist',
    component:DistributionListComponent
  },
  {
    path: 'insurancemaster',
    component: InsuranceMasterComponent
  },
  {
    path: 'tdscategorymaster',
    component: TDSCategoryMasterComponent
  },
  {
    path: 'budgetmaster',
    component: BudgetMasterComponent
  },
  {
    path: 'statementofaccount',
    children: [
      {
        path: 'list',
        component: StatementOfAccountReportComponent
      },
    ]
  },
  
  { path: 'themestest', loadChildren: () => import(`./../../themes/themes.module`).then(m => m.ThemesModule) },
  {
    path:'partybranch',
    children: [
      {
        path: 'list',
        component: PartyBranchListComponent
      },
    ]
  },
  {
    path:'accountlist',
    children: [
      {
        path: 'list',
        component: AccountListComponent
      },
    ]
  },
  {
    path:'paymentsummary',
    children: [
      {
        path: 'list',
        component: paymentsummaryComponent
      },
    ]
  },
  {
        path: 'contractsetting',
        component: ContractSettingComponent
  },
  {
    path: 'bank',
    component: BankComponent
},
  {
    path: 'costcenter',
    component: CostCenterComponent
},
  {
    path: 'partybranchlocation',
    component: PartyBranchLocationComponent
},
  {
    path:'trialbalanceschedule',
    children: [
      {
        path: 'list',
        component: tbscheduleComponent
      },
    ]
  },
  {
    path:'trialbalancegroup',
    children: [
      {
        path: 'list',
        component: tbgroupComponent
      },
    ]
  },
  {
    path:'tdscontact',
    children: [
      {
        path: 'list',
        component: TDSContactComponent
      },
    ]
  },
  {
    path:'schedule',
    children: [
      {
        path: 'list',
        component: schedulelistComponent
      },
    ]
  },
  {
    path:'deductedcategory',
    children: [
      {
        path: 'list',
        component: deductedcategoryComponent
      },
    ]
  },
  {
    path:'insurancelist',
    children: [
      {
        path: 'list',
        component: InsuranceListComponent
      },
    ]
  },
  {
    path:'trialbalanceaccount',
    children: [
      {
        path: 'list',
        component: tbaccountComponent
      },
    ]
  },
  {
    path:'trialbalancescheduledetail',
    children: [
      {
        path: 'list',
        component: tbscheduledetailComponent
      },
    ]
  },
  {
    path:'trialbalancegroupdetail',
    children: [
      {
        path: 'list',
        component: tbgroupdetailComponent
      },
    ]
  },
  {
    path:'trialbalanceaccountdetail',
    children: [
      {
        path: 'list',
        component: tbaccountdetailComponent
      },
    ]
  },
  {
    path:'statementofaccountsingle',
    children: [
      {
        path: 'list',
        component: soasingleComponent
      },
    ]
  },
  {
    path:'receivablebillwisereports',
    children: [
      {
        path: 'list',
        component: receivablebillwiseComponent
      },
    ]
  },
  {
    path:'receivablesdetailreports',
    children: [
      {
        path: 'list',
        component: receivablesdetailComponent
      },
    ]
  },
  {
    path:'receivablessummaryreports',
    children: [
      {
        path: 'list',
        component: receivablessummaryComponent
      },
    ]
  },
  {
    path:'bankbook',
    children: [
      {
        path: 'list',
        component: bankbookComponent
      },
    ]
  },
  {
    path:'daybook',
    children: [
      {
        path: 'list',
        component: daybookComponent
      },
    ]
  },
  {
    path:'cashbook',
    children: [
      {
        path: 'list',
        component: CashBookComponent
      },
    ]
  },
  {
    path:'companyvsledger',
    children: [
      {
        path: 'list',
        component: CompanyVsLedgerComponent
      },
    ]
  },
  {
    path:'accountgroup',
    children: [
      {
        path: 'list',
        component: AccountGroupComponent
      },
    ]
  },
  {
    path:'instrument',
    children: [
      {
        path: 'list',
        component: InstrumentListComponent
      },
      {
        path: '',
        component: InstrumentComponent
      },
      {
        path: ':id',
        component: InstrumentComponent
      },
    ]
  },
  {
    path:'accountschedule',
    children: [
      {
        path: 'list',
        component: AccountScheduleComponent
      },
      {
        path: '',
        component: AccountScheduleFormComponent
      }
    ]
  },
  {
    path:'partywithbranch',
    children: [
      {
        path: '',
        component: PartyWithBranchComponent
      },
    ]
  },

  {
    path:'registerdetailreports',
    children: [
      {
        path: '',
        component: registerdetailComponent
      },
    ]
  },
  {
    path:'brssummary',
    children: [
      {
        path: '',
        component: brssummaryComponent
      },
    ]
  },
  {
    path:'brsdetail',
    children: [
      {
        path: '',
        component: brsdetailComponent
      },
    ]
  },
  {
    path:'brsregister',
    children: [
      {
        path: '',
        component: brsregisterComponent
      },
    ]
  },
  {
    path:'ebank',
    children: [
      {
        path: '',
        component: ebankComponent
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FinanceRoutingModule { }
