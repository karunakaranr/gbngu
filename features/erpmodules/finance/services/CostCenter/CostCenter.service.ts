import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { ICostCenter } from '../../models/ICostCenter';

 
@Injectable({
  providedIn: 'root'
})
export class CostCenterService extends GBBaseDataServiceWN<ICostCenter> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICostCenter>) {
    super(dbDataService, 'CostCenterId');
  }
}
