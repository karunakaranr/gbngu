import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { ITDSCategoryMaster } from '../../models/ITDSCategoryMaster';


 
@Injectable({
  providedIn: 'root'
})
export class TDSCategoryMasterService extends GBBaseDataServiceWN<ITDSCategoryMaster> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ITDSCategoryMaster>) {
    super(dbDataService, 'TDSCategoryId');
  }
}
