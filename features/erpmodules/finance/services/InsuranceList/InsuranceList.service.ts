import { Injectable } from '@angular/core';
import { InsuranceListDbService } from './../../dbservices/InsuranceList/InsuranceListDb.service';
@Injectable({
  providedIn: 'root'
})
export class InsuranceList {
  constructor(public dbservice: InsuranceListDbService) { }

  public insurancelistview(){               
    return this.dbservice.picklist();
  }

  public Rowservice(obj){
    return this.dbservice.reportData(obj);
  }
}