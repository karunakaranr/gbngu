import { Injectable } from '@angular/core';
import { StatementOfAccountbservice } from './../../dbservices/Statementofaccount/StatementofAccountdb.service';
@Injectable({
  providedIn: 'root'
})
export class StatementOfAccountReportService {
  constructor(public dbservice: StatementOfAccountbservice) { }

  public Getstatementofaccount(){                      
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                    
    return this.dbservice.reportData(obj);
  }
}