import { Injectable } from '@angular/core';
import { AccountGroupDbService } from './../../../dbservices/AccountList/AccountGroup/AccountGroupDb.service';
@Injectable({
  providedIn: 'root'
})
export class AccountGroup {
  constructor(public dbservice: AccountGroupDbService) { }

  public accountgroupview(){                      //col
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                    //row
    return this.dbservice.reportData(obj);
  }
}