import { Injectable } from '@angular/core';
import { AccountScheduleDbService } from './../../../dbservices/AccountList/AccountSchedule/AccountScheduleDb.service';
@Injectable({
  providedIn: 'root'
})
export class AccountScheduleList {
  constructor(public dbservice: AccountScheduleDbService) { }

  public accountscheduleview(){                      
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                    
    return this.dbservice.reportData(obj);
  }
}