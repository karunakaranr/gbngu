import { Injectable } from '@angular/core';
import { PartyBranchListDbService } from './../../../dbservices/AccountList/PartyBranchList/PartyBranchDb.service';
@Injectable({
  providedIn: 'root'
})
export class PartyBranchList {
  constructor(public dbservice: PartyBranchListDbService) { }

  public partylistview(){                     
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                   
    return this.dbservice.reportData(obj);
  }
}