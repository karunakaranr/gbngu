import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IPartyOU } from '../../models/IPartyOU';

@Injectable({
  providedIn: 'root'
})
export class PartyOUService extends GBBaseDataServiceWN<IPartyOU> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPartyOU>) {
    super(dbDataService, 'PartyOUId',"PartyOU");
  }
}
