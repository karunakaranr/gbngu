import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';
import { IChequeBook } from '../../models/IChequeBook';



 
@Injectable({
  providedIn: 'root'
})
export class ChequeBookService extends GBBaseDataServiceWN<IChequeBook> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IChequeBook>) {
    super(dbDataService, 'ChequeBookId');
  }
}
