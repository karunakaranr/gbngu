import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { INarrationMaster } from '../../models/INarrationMaster';


 
@Injectable({
  providedIn: 'root'
})
export class NarrationMasterService extends GBBaseDataServiceWN<INarrationMaster> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<INarrationMaster>) {
    super(dbDataService, 'NarrationId');
  }
}
