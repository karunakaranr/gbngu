import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IAccountVoucher } from './../../../models/Transaction/IAccountVoucher';

@Injectable({
  providedIn: 'root'
})
export class AccountVoucherService extends GBBaseDataServiceWN<IAccountVoucher> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IAccountVoucher>) {
    super(dbDataService, 'VoucherId');
  }
}
