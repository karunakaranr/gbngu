import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { IBank } from '../../models/IBank';


 
@Injectable({
  providedIn: 'root'
})
export class BankService extends GBBaseDataServiceWN<IBank> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IBank>) {
    super(dbDataService, 'GcmId');
  }
}
