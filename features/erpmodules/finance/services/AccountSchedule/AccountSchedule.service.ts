import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { IAccountSchedule } from '../../models/IAccountSchedule';


 
@Injectable({
  providedIn: 'root'
})
export class AccountScheduleService extends GBBaseDataServiceWN<IAccountSchedule> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IAccountSchedule>) {
    super(dbDataService,'AccountScheduleId');
  }
}
