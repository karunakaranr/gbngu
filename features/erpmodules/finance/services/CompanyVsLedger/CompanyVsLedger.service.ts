import { Injectable } from '@angular/core';
import { CompanyVsLedgerDbService } from './../../dbservices/CompanyVsLedger/CompanyVsLedgerDb.service';
@Injectable({
  providedIn: 'root'
})
export class CompanyVsLedger {
  constructor(public dbservice: CompanyVsLedgerDbService) { }

  public companyvsledgerview(){                     
    return this.dbservice.picklist();
  }

  public Rowservice(obj){
    return this.dbservice.reportData(obj);
  }
}