import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';

import { IPartyBranchLocation } from '../../models/IPartyBranchLocation';

 
@Injectable({
  providedIn: 'root'
})
export class PartyBranchLocationService extends GBBaseDataServiceWN<IPartyBranchLocation> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPartyBranchLocation>) {
    super(dbDataService, 'PartyBranchLocationId');
  }
}
