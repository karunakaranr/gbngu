import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';

import { ICostCategory } from '../../models/ICostCategory';

 
@Injectable({
  providedIn: 'root'
})
export class CostCategoryService extends GBBaseDataServiceWN<ICostCategory> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICostCategory>) {
    super(dbDataService, 'CostCategoryId');
  }
}
