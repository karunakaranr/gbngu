import { Injectable } from '@angular/core';
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {ISelectListCriteria } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
import {GBBaseDBDataService} from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ICostcategory } from '../../models/ICostCategory';
const urls = require('../../URLS/urls.json');

@Injectable({
  providedIn: 'root',
})
export class CostcategoryDBService extends GBBaseDBDataService<ICostcategory> {
  endPoint: string = urls.Costcategory;

  constructor(http: GBHttpService) {
    super(http);
  }

  putData(data: ICostcategory) {
    return super.putData(data);
  }

  postData(data: ICostcategory) {
    return super.postData(data);
  }

  getData(idField: string, id: string): Observable<ICostcategory> {
    return super.getData(idField, id);
  }

  deleteData(idField: string, id: string) {
    return super.deleteData(idField, id)
  }

  getList(scl: ISelectListCriteria): Observable<any> {
    return super.getList(scl);
  }
}
