import { Injectable } from '@angular/core';
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {ISelectListCriteria } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
import {GBBaseDBDataService} from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service';
import { Observable } from 'rxjs';

import { PartyWithBranch} from '../../models/IPartyWithBranch';
const urls = require('../../URLS/urls.json');

@Injectable({
  providedIn: 'root',
})
export class PartyWithBranchDBService extends GBBaseDBDataService<PartyWithBranch> {
  endPoint: string = urls.PartyWithBranch;
  firstNumber = 1;
  maxResult = 50;
  useAPIPagination=true;
  constructor(http: GBHttpService) {
    
    super(http);
  }

  putData(data: PartyWithBranch) {
    return super.putData(data);
  }

  postData(data: PartyWithBranch) {
    return super.postData(data);
  }

  getData(idField: string, id: string): Observable<PartyWithBranch> {
    
    return super.getData(idField, id);
  }

  deleteData(idField: string, id: string) {
    return super.deleteData(idField, id)
  }

  getList(scl: ISelectListCriteria): Observable<any> {
    return super.getList(scl);
  }
}
