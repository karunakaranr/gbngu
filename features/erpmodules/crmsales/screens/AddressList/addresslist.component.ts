import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { NavigationExtras, Router } from '@angular/router';
import { ButtonVisibility, Path,Title } from 'features/layout/store/layout.actions';
import { LayoutState } from 'features/layout/store/layout.state';
import { Iaddresslist } from '../../models/Iaddresslist';
import { AddressListService } from '../../services/addresslist.service';
@Component({
  selector: 'app-addresslist',
  templateUrl: './addresslist.component.html',
  styleUrls: ['./addresslist.component.scss'],
})
export class AddressListComponent implements OnInit {
  @Select(LayoutState.FormEdit) edit$: Observable<any>;
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
 
  public addresslist :Iaddresslist;
  public sharedData: string;
  screenHeight: number;
  searchText: string = '';
  AddressId: number;
  constructor(public DataService:AddressListService,public sharedService: SharedService, public router: Router, public store: Store) {
  }
  // In your component.ts file
getAddressTypeLabel(value: number): string {
  switch (value) {
    case 0:
      return 'Present';
    case 1:
      return 'Permanent';
    case 2:
      return 'Work';
    case 3:
      return 'Main';
    case 4:
      return 'Work Fax';
    case 5:
      return 'Main Fax';
    case 6:
      return 'Others';
    default:
      return 'Unknown Type'; // Provide a default label for unknown values
  }
}

getDirections() {
 
  console.log('Button clicked! ');
  this.router.navigate(['GoodBooks/MapAllocation'])
}
  ngOnInit(): void {
    // this.store.dispatch(new Title("Address List"))
    // this.DataService.ContactListwithImageURL(1,10,'').subscribe(contactserviceresponse=> {
    //           this.addresslist = contactserviceresponse;
    //           console.log("addresslist:",this.addresslist)
              
              this.screenHeight = window.innerHeight;
              this.setStyle('--Tableheight', this.screenHeight - 230 + 'px');
    //         })
           
    console.log("Report Button")
    this.store.dispatch(new Title("Address List"))
    this.store.dispatch(new ButtonVisibility("Report&forms"))
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
        this.addresslist = data
        this.store.dispatch(new Path('crmsales/contactDetail'))
      }
    })   

    const inputField = document.getElementById('SearchInput');
    inputField.addEventListener('keyup', (event: KeyboardEvent) => {
      
      if (event.key === 'Enter') {
         
          this.ContactlistSearch(); 
      }  
    })
   
    
  }
  ContactlistSearch(): void {
    this.DataService.ContactListwithImageURL(1,25,this.searchText).subscribe(contactserviceresponse=> {
      this.addresslist = contactserviceresponse;
      console.log("list:",contactserviceresponse)
    })
    
  }
  setStyle(name: string, value: string): void { //height
    document.documentElement.style.setProperty(name, value); 
  }
  clearSearch(event: Event): void {
    event.preventDefault();
    
    this.searchText = '';  
    
    console.log('Clear icon clicked');
  
    this.ContactlistSearch();
  }
  
  

  focusSearchInput(searchInput: HTMLInputElement): void {
    if (searchInput) {
      searchInput.focus();
    }
  }

public AddressDetail(AddressId : number){  //drilldown
  console.log("AddressId:",AddressId)
  const queryParams: any = {};
  queryParams.AddressId = AddressId   //query.contactid is passed conactinformationcomponent map id
  const navigationExtras: NavigationExtras = {
    queryParams
  };
  console.log("navigationExtras:",navigationExtras)
  this.router.navigate(['crmsales/address'], navigationExtras);
}
}
