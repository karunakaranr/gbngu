import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { ICriteriaDTODetail } from 'features/erpmodules/samplescreens/models/IFilterDetail';
import { ILeadStage, ILeadStageSummaryDetail } from '../../models/ILeadStage';
import { LeadStageSummaryService } from '../../services/LeadStageSummary.service';

@Component({
  selector: 'app-leadstagesummary',
  templateUrl: './leadstagesummary.component.html',
  styleUrls: ['./leadstagesummary.component.scss'],
})
export class LeadStageSummaryComponent implements OnChanges {
  @Input() StageName: string;
  @Input() FilterData: ICriteriaDTODetail;
  @Input() LeadStageData: ILeadStage[];
  @Input() selected: number;
  @Output() StatusIcon: EventEmitter<boolean> = new EventEmitter<boolean>();
  isHighlighted: boolean = false;
  ListTableVisible: boolean = false;
  EmployeeId: number;
  SummaryStageName: string;
  LeadStageSummaryToListEvent: HTMLElement;
  LeadStageSummaryToListFlag = 0;
  RowData: ILeadStageSummaryDetail[];
  TableSortType: string = 'ascending';
  TableModifiedHead: string = '';

  constructor(public summaryservice: LeadStageSummaryService) { }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.StageName || changes.selected) {
      this.ListTableVisible = false;
      this.StatusIcon.emit(false);
      this.summaryservice
        .LeadStageSummaryview(this.FilterData, this.selected)
        .subscribe((menudetails) => {
          this.RowData = menudetails;
        });
    }
  }

  onSortClick(sortitem: string) {
    if (this.TableModifiedHead !== sortitem) {
      this.TableSortType = 'ascending';
    }
    this.TableModifiedHead = sortitem;
    if (this.TableSortType === 'ascending') {
      this.RowData.sort((a, b) => (a[sortitem] > b[sortitem] ? 1 : -1));
      this.TableSortType = 'descending';
    } else {
      this.RowData.sort((a, b) => (a[sortitem] > b[sortitem] ? -1 : 1));
      this.TableSortType = 'ascending';
    }
  }

  shouldShowElement(name: String) {
    return this.LeadStageData.some(function (el) {
      return el.StageName === name;
    });
  }

  LeadStageSummaryToList(
    data: ILeadStageSummaryDetail,
    event: { target: HTMLElement },
    StageName: string
  ) {
    if (event.target.innerText == '0') {
      return;
    } else {
      this.EmployeeId = data.Id;
      this.SummaryStageName = StageName;
      const clickedCell = event.target as HTMLElement;
      if (this.LeadStageSummaryToListFlag == 0) {
        this.LeadStageSummaryToListFlag = 1;
        this.LeadStageSummaryToListEvent = clickedCell;
        clickedCell.classList.toggle('highlighted');
      } else {
        this.LeadStageSummaryToListEvent.classList.toggle('highlighted');
        clickedCell.classList.toggle('highlighted');
        this.LeadStageSummaryToListEvent = clickedCell;
      }
      this.StatusIcon.emit(true);
      this.ListTableVisible = true;
    }
  }
}
