import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { ContactlistService } from '../../services/contactlist.service';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { NavigationExtras, Router } from '@angular/router';
import { Icontactlist } from '../../models/Icontactlist';
import { ButtonVisibility, Path,Title } from 'features/layout/store/layout.actions';
import { LayoutState } from 'features/layout/store/layout.state';
@Component({
  selector: 'app-contactlist',
  templateUrl: './contactlist.component.html',
  styleUrls: ['./contactlist.component.scss'],
})
export class ContactListComponent implements OnInit {
  @Select(LayoutState.FormEdit) edit$: Observable<any>;
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
 
  public contactlist :Icontactlist;
  public sharedData: string;
  screenHeight: number;
  searchText: string = '';
  ContactId: number;
  constructor(public DataService:ContactlistService,public sharedService: SharedService, public router: Router, public store: Store) {
  }
  ngOnInit(): void {
    console.log("Report Button")
    this.store.dispatch(new Title("Contact List"))
    this.store.dispatch(new ButtonVisibility("Report&forms"))
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
        this.contactlist = data
        this.store.dispatch(new Path('crmsales/contactDetail'))
      }
    })
    const inputField = document.getElementById('SearchInput');
    inputField.addEventListener('keyup', (event: KeyboardEvent) => {
      
      if (event.key === 'Enter') {
         
          this.ContactlistSearch(); 
      }  
    })
   
    this.screenHeight = window.innerHeight;
    this.setStyle('--Tableheight', this.screenHeight - 230 + 'px');
    // this.ContactlistSearch(); 
    
  }
  
  clearSearch(event: Event): void {
    event.preventDefault();
    
    this.searchText = '';  
    
    console.log('Clear icon clicked');
  
    this.ContactlistSearch();
  }
  
  
  ContactlistSearch(): void {
    this.DataService.ContactListwithImageURL(1,25,this.searchText).subscribe(contactserviceresponse=> {
      this.contactlist = contactserviceresponse;
      console.log("list:",contactserviceresponse)
    })
    
  }

  setStyle(name: string, value: string): void { //height
    document.documentElement.style.setProperty(name, value); 
  }
  focusSearchInput(searchInput: HTMLInputElement): void {
    if (searchInput) {
      searchInput.focus();
    }
  }
  public ContactDetail(ContactId : number){  //drilldown
    const queryParams: any = {};
    queryParams.ContactId = ContactId   //query.contactid is passed conactinformationcomponent map id
    const navigationExtras: NavigationExtras = {
      queryParams
    };
    this.router.navigate(['crmsales/contactDetail'], navigationExtras);
  }

  
}
