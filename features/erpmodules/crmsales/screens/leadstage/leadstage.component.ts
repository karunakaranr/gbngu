import { Component, OnInit } from '@angular/core';
import { DashboardFunction } from 'libs/gbcommon/src/lib/services/DashboardFunction/dashboard.service';
import { ILeadStage, ISummarySelectDetail } from './../../models/ILeadStage';
import { Swiper } from 'swiper';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ICriteriaDTODetail } from 'features/erpmodules/samplescreens/models/IFilterDetail';

@Component({
  selector: 'app-leadstage',
  templateUrl: './leadstage.component.html',
  styleUrls: ['./leadstage.component.scss'],
})
export class LeadStageComponent implements OnInit {
  @Select(ReportState.Sourcecriteriadrilldown) filtercriteria$: Observable<any>;
  LeadStageData: ILeadStage[] = [];
  StageName: string;
  RandomBackground: number[] = [];
  SummaryValue: number;
  SummaryOrList: string[] = ['Summary', 'List'];
  SummaryOrListModel = { option: 'Summary' };
  ReportType: number;
  LeadStageTableVisible: boolean = false;
  selected: number = 0;
  MonthTypeselected: number = 0;
  FilterData: ICriteriaDTODetail[];
  StatusIcon: boolean = false;
  TotalRecord: number;
  Currentmonth: number = new Date().getMonth()
  Monthobj: string[] = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  Summary: ISummarySelectDetail[] = [
    {
      id: 0,
      name: 'Incharge',
    },
    {
      id: 1,
      name: 'Source Incharge',
    },
    {
      id: 2,
      name: 'Category',
    },
    {
      id: 3,
      name: 'Source Type',
    },
    {
      id: 4,
      name: 'Source',
    },
    {
      id: 5,
      name: 'Source Detail',
    },
  ];

  MonthType: ISummarySelectDetail[] = [
    {
      id: 0,
      name: 'All',
    },
    {
      id: 1,
      name: 'Overdue',
    },
    {
      id: 2,
      name: this.Monthobj[this.Currentmonth],
    },
    {
      id: 3,
      name: this.Monthobj[this.Currentmonth + 1],
    },
    {
      id: 4,
      name: this.Monthobj[this.Currentmonth + 2],
    },
    {
      id: 5,
      name: '> ' + this.Monthobj[this.Currentmonth + 2],
    },
  ];

  constructor(
    public sharedService: SharedService,
    public commonfunction: DashboardFunction
  ) { }

  ngOnInit(): void {
    console.log("Current Month:", this.Currentmonth)
    this.sharedService.getRowInfo().subscribe((data) => {
      this.LeadStageData = data;
      console.log('LeadStage Data:', this.LeadStageData);
      if (data.length > 0) {
        this.RandomBackground = this.commonfunction.RandomBackground(
          data.length
        );
      }
    });
  }

  public LeadStageSummaryServiceCall(data: ILeadStage, reportType: number) {
    this.StatusIcon = false;
    this.ReportType = reportType;
    this.filtercriteria$.subscribe((filterdata) => {
      this.FilterData = filterdata.CriteriaDTO;
      this.LeadStageTableVisible = false;
      this.StageName = null;
    });
    this.StageName = data.StageName;
    this.TotalRecord = data.StageCount;
    this.LeadStageTableVisible = true;
  }

  public StatusIconChange(status: boolean): void {
    this.StatusIcon = status;
  }
  ngAfterViewInit() {
    new Swiper('.swiper-container', {
      slidesPerView: 5,
      spaceBetween: 0,
      loop: false,
    });
  }
}
