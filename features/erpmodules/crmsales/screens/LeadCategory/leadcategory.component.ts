import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { LeadCategoryDetailService } from '../../services/leadcategory.service'; 
import { ILeadCategory } from '../../models/ILeadCategory';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as LeadCategoryJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/LeadCategory.json'
import { URLS } from '../../urls/Url';

@Component({
  selector: 'app-leadcategory',
  templateUrl: './leadcategory.component.html',
  styleUrls: ['./leadcategory.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'LeadCategoryId'},
    { provide: 'url', useValue: URLS.LeadCategory },
    { provide: 'DataService', useClass: LeadCategoryDetailService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class LeadCategoryComponent extends GBBaseDataPageComponentWN<ILeadCategory> {
  title : string = "LeadCategory"
  LeadCategoryJSON =LeadCategoryJSON;
  form: GBDataFormGroupWN<ILeadCategory> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "LeadCategory", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
      this.LeadCategoryFillData(arrayOfValues.LeadCategoryId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }

  public LeadCategoryFillData(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormVariable => {
          this.form.patchValue(FormVariable);
        })
    }
  }


  public LeadCategoryPatchids(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ILeadCategory> {
  const dbds: GBBaseDBDataService<ILeadCategory> = new GBBaseDBDataService<ILeadCategory>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ILeadCategory>, dbDataService: GBBaseDBDataService<ILeadCategory>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ILeadCategory> {
  return new GBDataPageService<ILeadCategory>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
