import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { URLS } from '../../urls/Url';
import { addressService } from '../../services/address.service';
import { Iaddress } from '../../models/Iaddress';
import * as AddressJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Address.json'
import { IPunchDetail } from 'features/erpmodules/hrms/models/IPunchDetail';
type AddressTab = 'personalpage' | 'personalpage1' | 'personalpage2' | 'businesspage1' | 'businesspage2' | 'businesspage3';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'AddressId'},
    { provide: 'url', useValue: URLS.address },
    { provide: 'DataService', useClass: addressService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class addressComponent extends GBBaseDataPageComponentWN<Iaddress> {
  RowData: any = [{
    "GeoCode": "11.017440537376613,76.96825700980101",
    "Type": "In"
  }]
  title: string = 'Address'
  activeTab: AddressTab = 'personalpage';
  AddressJSON =AddressJSON;
  
  form: GBDataFormGroupWN<Iaddress> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Address', {}, this.gbps.dataService);

  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('AddressId'))
    if(arrayOfValues != null){
      this.AddressFillData(arrayOfValues)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }

  public AddressFillData(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormVariable => {
          this.form.patchValue(FormVariable);
        })
    }
  }


  public AddressPatchids(SelectedPicklistDatas: any): void {
    console.log("addd",SelectedPicklistDatas)
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
  personalBtnStyles = {
    backgroundColor: '#6E3AB6',
    color: '#fff'
  };

  businessBtnStyles = {
    backgroundColor: '#fff',
    color: '#000'
  };
 previousButtonClick(): void {
    if (this.activeTab.startsWith('personal')) {
      // Personal pages navigation logic
      if (this.activeTab === 'personalpage2') {
        // Going back from personalpage2 to personalpage1
        this.activeTab = 'personalpage1';
      } else if (this.activeTab === 'personalpage1') {
        // Going back from personalpage1 to personalpage
        this.activeTab = 'personalpage';
      } else {
        // Additional logic if needed
      }
    } else if (this.activeTab.startsWith('business')) {
      // Business pages navigation logic
      if (this.activeTab === 'businesspage3') {
        // Going back from businesspage3 to businesspage2
        this.activeTab = 'businesspage2';
      } else if (this.activeTab === 'businesspage2') {
        // Going back from businesspage2 to businesspage1
        this.activeTab = 'businesspage1';
      } else if (this.activeTab === 'businesspage1') {
        // Going back from businesspage1 to personalpage2
        this.activeTab = 'personalpage';
      }
    }
    // Additional logic if needed
  }


  nextButtonClick(): void {
    if (this.activeTab.startsWith('personal')) {
      if (this.activeTab === 'personalpage') {
        // Going forward from personalpage to personalpage1
        this.activeTab = 'personalpage1';
      } else if (this.activeTab === 'personalpage1') {
        // Going forward from personalpage1 to personalpage2
        this.activeTab = 'personalpage2';
      } else {
        // Additional logic if needed
      }
    } else if (this.activeTab.startsWith('business')) {
      if (this.activeTab === 'businesspage1') {
        // Going forward from businesspage1 to businesspage2
        this.activeTab = 'businesspage2';
      } else if (this.activeTab === 'businesspage2') {
        // Going forward from businesspage2 to businesspage3
        this.activeTab = 'businesspage3';
      } else {
        // Additional logic if needed
      }
    }
  
  }

  changeColor(buttonType: string): void {
    // Reset the styles for both buttons
    this.personalBtnStyles = { backgroundColor: '#fff', color: '#000' };
    this.businessBtnStyles = { backgroundColor: '#fff', color: '#000' };
  
    // Set the styles for the clicked button
    if (buttonType === 'personal') {
      this.personalBtnStyles = { backgroundColor: '#6E3AB6', color: '#fff' };
      // Set the activeTab to 'personalpage1' to show the personal pages
      this.activeTab = 'personalpage1';
    } else if (buttonType === 'business') {
      this.businessBtnStyles = { backgroundColor: '#6E3AB6', color: '#fff' };
      // Set the activeTab to 'businesspage1' to show the business pages
      this.activeTab = 'businesspage1';
    }
  }
  
  
  


  ngOnInit() {
    this.showPrefinal();
    
  }
  
  showPrefinal() {
    
    document.getElementById("prefinal").style.display = "block";
    document.getElementById("final").style.display = "none";
    console.log("GeoCode:",this.RowData.ParentData.GeoCode)
  }
  
  showFinal() {
    document.getElementById("final").style.display = "block";
    document.getElementById("prefinal").style.display = "none";
  }
  public personal() {
    this.showPrefinal();
  }
  
  public bussiness() {
    this.showFinal();
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<Iaddress> {
  const dbds: GBBaseDBDataService<Iaddress> = new GBBaseDBDataService<Iaddress>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<Iaddress>, dbDataService: GBBaseDBDataService<Iaddress>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<Iaddress> {
  return new GBDataPageService<Iaddress>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
