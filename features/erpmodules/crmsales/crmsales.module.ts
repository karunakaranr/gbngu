import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UicoreModule } from '@goodbooks/uicore';
import { UifwkUifwkmaterialModule } from '@goodbooks/uifwk/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslocoRootModule } from './../../transloco/transloco-root.module';
import { GbdataModule } from '@goodbooks/gbdata';
import { environment } from '../../environments/environment';
import { NgBreModule } from '@unisoft/ng-bre';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GbchartModule } from '../../common/shared/screens/gbchart/gbchart.module';
import { EntitylookupModule } from './../../entitylookup/entitylookup.module';
import { geolocationModule } from './../../geolocation/geolocation.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { AgGridModule } from 'ag-grid-angular';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { GbgridModule } from 'features/gbgrid/components/gbgrid/gbgrid.module';
import { CommonReportModule } from './../../commonreport/commonreport.module';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MaterialModule } from 'features/common/material.module';
import { MatInputModule } from '@angular/material/input';
import { SamplescreensModule } from '../samplescreens/samplescreens.module';

//Screens
import { CrmSalesComponent } from './crmsales.component';
import { CrmsalesroutingModule } from './crmsales.routing.module';
import { LeadStageComponent } from './screens/leadstage/leadstage.component';
import { LeadStageSummaryComponent } from './screens/leadstagesummary/leadstagesummary.component';
import { LeadStageListComponent } from './screens/leadstagelist/leadstagelist.component';
import { ContactListComponent } from './screens/checklist/contactlist.component';
import { ContactComponent } from './screens/ContactlistInformation/contact.component';
import { LeadCategoryComponent } from './screens/LeadCategory/leadcategory.component';
import { LeadSourceComponent } from './screens/LeadSource/leadsource.component';
import { addressComponent } from './screens/Address/address.component';
import { AddressListComponent } from './screens/AddressList/addresslist.component';
import { ContactAddressComponent } from './screens/ContactAddress/contactaddress.component';
import { LeadCycleComponent } from './screens/LeadCycle/leadcycle.component';
@NgModule({
  declarations: [
    AddressListComponent,
    ContactAddressComponent,
    addressComponent,
    ContactComponent,
    ContactListComponent,
    LeadCycleComponent,
    CrmSalesComponent,
    LeadStageComponent,
    LeadStageSummaryComponent,
    LeadStageListComponent,
    LeadCategoryComponent,
    LeadSourceComponent
  ],
  imports: [
    SamplescreensModule,
    CommonModule,
    CrmsalesroutingModule,
    ReactiveFormsModule,
    FormsModule,
    UicoreModule,
    NgBreModule,
    UifwkUifwkmaterialModule,
    TranslocoRootModule,
    AgGridModule,
    GbgridModule,
    GbdataModule.forRoot(environment),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDvFSDxJXXR1nrdRDIzvp6DRZB9v649My8',
      libraries: ['places', 'geometry', 'drawing'],
    }),
    AgmDirectionModule,
    NgxChartsModule,
    GbchartModule,
    EntitylookupModule,
    geolocationModule,
    NgSelectModule,
    MaterialModule,
    MatSortModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatListModule,
    MatTabsModule,
    MatCheckboxModule,
    MatTableModule,
    MatIconModule,
    MatExpansionModule,
    MatDividerModule,
    MatPaginatorModule,
    CommonReportModule,
    MatFormFieldModule,
    MatInputModule,
  ],
  exports: [
    CrmSalesComponent,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
  ],
})
export class CrmsalesModule {
  constructor() {}
}
