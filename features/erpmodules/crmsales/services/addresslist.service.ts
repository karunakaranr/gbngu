import { Injectable } from '@angular/core';
import { AddressListDBService } from '../dbservices/addresslistdb.service';


@Injectable({
  providedIn: 'root'    
})
export class AddressListService {
  constructor(public dbDataService:AddressListDBService ) {
  }

  ContactListwithImageURL(firstnumber:number,maxresult:number,searchtext:string){
    return this.dbDataService.ContactListwithImageURL(firstnumber,maxresult,searchtext);
  }

}
