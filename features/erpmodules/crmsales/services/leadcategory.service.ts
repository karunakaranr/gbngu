import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { ILeadCategory } from '../models/ILeadCategory';
 
@Injectable({
  providedIn: 'root'
})
export class LeadCategoryDetailService extends GBBaseDataServiceWN<ILeadCategory> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ILeadCategory>) {
    super(dbDataService, 'LeadCategoryId');
  }
}


