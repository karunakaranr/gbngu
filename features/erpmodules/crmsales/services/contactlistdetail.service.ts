import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { Icontactlistinformation } from '../models/Icontactlistinformation';
 
@Injectable({
  providedIn: 'root'
})
export class ContactlistDetailService extends GBBaseDataServiceWN<Icontactlistinformation> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<Icontactlistinformation>) {
    super(dbDataService, 'ContactId');
  }
}


