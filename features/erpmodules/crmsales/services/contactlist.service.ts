import { Injectable } from '@angular/core';
import { ContactListdbService } from '../dbservices/contactlistdb.service';


@Injectable({
  providedIn: 'root'    
})
export class ContactlistService {
  constructor(public dbDataService:ContactListdbService ) {
  }

  ContactListwithImageURL(firstnumber:number,maxresult:number,searchtext:string){
    return this.dbDataService.ContactListwithImageURL(firstnumber,maxresult,searchtext);
  }

}
