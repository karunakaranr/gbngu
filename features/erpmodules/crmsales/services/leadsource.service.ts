import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { ILeadSource } from '../models/ILeadSource';
 
@Injectable({
  providedIn: 'root'
})
export class LeadSourceDetailService extends GBBaseDataServiceWN<ILeadSource> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ILeadSource>) {
    super(dbDataService, 'LeadSourceId');
  }
}


