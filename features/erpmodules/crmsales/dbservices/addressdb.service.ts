import { Injectable } from '@angular/core';
import {GBHttpService} from './../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { url } from 'inspector';

const urls = require('./../URLS/urls.json');

@Injectable({
  providedIn: 'root',
})
export class addressDBService{
  endPoint: string = urls.address;
 
  idField:string = "?AddressId="
  selectid : string;
  constructor(public http: GBHttpService) {
  }

  getall(){
    const url = this.endPoint + 'SelectList';
    let criteria = "";
    return this.http.httppost(url,criteria);
  }


  getid(id){
    this.selectid = id
    const url = this.endPoint + this.idField + id;
    return this.http.httpget(url);
  }

}
