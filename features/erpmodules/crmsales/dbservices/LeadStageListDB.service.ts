import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';
import {
  IAttributeCriteriaDetail,
  ICriteriaDTODetail,
  ISectionCriteriaDetail,
} from 'features/erpmodules/samplescreens/models/IFilterDetail';
const StageId = require('./../../crmsales/models/StandardStageId.json');
const urls = require('./../urls/urls.json');

@Injectable({
  providedIn: 'root',
})
export class LeadStageListDbService {
  endPoint: string = urls.LeadStageStatusReport;
  TempCriteria: ICriteriaDTODetail[];
  TempSectionCriteriaDetail: ISectionCriteriaDetail[];
  TempIAttributeCriteriaDetail: IAttributeCriteriaDetail[];
  constructor(public http: GBHttpService) { }

  public LeadStageListService(
    FilterData: ICriteriaDTODetail,
    selected: number,
    StageName: string,
    EmployeeId: number,
    MonthTypeselected: number,
    firstnumber: number,
    lastnumber: number
  ): Observable<any> {
    this.TempIAttributeCriteriaDetail =
      FilterData.SectionCriteriaList[0].AttributesCriteriaList;
    var newObjStandardStageId: any;
    var newObjEmployeeId: any;
    var newObjMonthType: any;

    if (StageName != 'Total') {
      if (StageName == 'Others') {
        newObjStandardStageId = {
          FieldName: 'StandardStageId',
          OperationType: 6,
          FieldValue: StageId[StageName], JoinType: 2,
        };
      } else {
        newObjStandardStageId = {
          FieldName: 'StandardStageId',
          OperationType: 5,
          FieldValue: StageId[StageName],
          JoinType: 2,
        };

      }

      this.TempIAttributeCriteriaDetail = [
        ...this.TempIAttributeCriteriaDetail,
        newObjStandardStageId,
      ];

      let TempFilterData = JSON.stringify(FilterData);
      FilterData = JSON.parse(TempFilterData);
      FilterData.SectionCriteriaList[0].AttributesCriteriaList = this.TempIAttributeCriteriaDetail;
    }


    newObjMonthType = {
      FieldName: "MonthType",
      OperationType: 5,
      FieldValue: MonthTypeselected,
      JoinType: 2,
    };
    this.TempIAttributeCriteriaDetail = [
      ...this.TempIAttributeCriteriaDetail,
      newObjMonthType,
    ];


    let TempFilterData = JSON.stringify(FilterData);
    FilterData = JSON.parse(TempFilterData);
    FilterData.SectionCriteriaList[0].AttributesCriteriaList = this.TempIAttributeCriteriaDetail;

    if (EmployeeId != undefined) {
      newObjEmployeeId = {
        FieldName: StageId[selected],
        OperationType: 5,
        FieldValue: EmployeeId,
        JoinType: 2,
      };
      this.TempIAttributeCriteriaDetail = [
        ...this.TempIAttributeCriteriaDetail,
        newObjEmployeeId,
      ];
      let TempFilterData = JSON.stringify(FilterData);
      FilterData = JSON.parse(TempFilterData);
      FilterData.SectionCriteriaList[0].AttributesCriteriaList = this.TempIAttributeCriteriaDetail;
    }

    const url = this.endPoint + '?FirstNumber=' + firstnumber + '&MaxResult=' + lastnumber;
    return this.http.httppost(url, FilterData);
  }
}
