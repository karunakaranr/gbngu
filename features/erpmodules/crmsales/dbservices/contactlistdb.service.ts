import { Injectable } from '@angular/core';
import {GBHttpService} from './../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { max } from 'rxjs/operators';
const urls = require('./../URLS/urls.json');
@Injectable({
  providedIn: 'root',
})
export class ContactListdbService{
  ContactURL:string =urls.ContactList;
  idField1:string = "?FirstNumber="; 
  idField2:string = "&MaxResult=";  
  constructor(public http: GBHttpService) {
  } 

  ContactListwithImageURL(firstnumber: number,maxresult: number,searchtext:string){
    const url=this.ContactURL+ this.idField1+firstnumber+this.idField2+maxresult;
    let criteria = {
      "SectionCriteriaList": [
        {
          "SectionId": 0,
          "AttributesCriteriaList": [
            {
              "FieldName": "Search",
              "OperationType": 11,
              "FieldValue": searchtext,
              "InArray": null,
              "JoinType": 2,
              "CriteriaAttributeName": null,
              "IsCompulsory": 0,
              "IsHeader": 0,
              "CriteriaAttributeType": 0
            }
          ],
          "OperationType": 0
        }
      ]
    };
    return this.http.httppost(url,criteria);
  }

}
