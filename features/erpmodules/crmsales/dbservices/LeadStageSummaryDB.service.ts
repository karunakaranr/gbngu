import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';
import { ICriteriaDTODetail } from 'features/erpmodules/samplescreens/models/IFilterDetail';
const urls = require('./../urls/urls.json');

@Injectable({
  providedIn: 'root',
})
export class LeadStageSummaryDbService {
  endPoint: string = urls.LeadStageSummaryReport;
  constructor(public http: GBHttpService) { }

  public LeadStageSummaryService(
    FilterData: ICriteriaDTODetail,
    selected: number
  ): Observable<any> {
    const url = this.endPoint + '?Type=' + selected;
    return this.http.httppost(url, FilterData);
  }
}
