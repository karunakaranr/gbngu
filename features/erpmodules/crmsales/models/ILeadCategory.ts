export interface ILeadCategory {
  LeadCategoryIsProdcutRequiredName: string
  LeadCategoryId: number
  LeadCategoryCode: string
  LeadCategoryName: string
  LeadCycleId: number
  LeadCycleCode: string
  LeadCycleName: string
  LeadCategoryIsProductRequired: number
  LeadCategoryRemarks: string
  LeadCategoryCreatedById: number
  LeadCategoryCreatedOn: string
  LeadCategoryModifiedById: number
  LeadCategoryModifiedOn: string
  LeadCategoryBaseClinetId: number
  LeadCategorySortOrder: number
  LeadCategoryStatus: number
  LeadCategoryVersion: number
  LeadCategorySourceType: number
  LeadCategoryItemCategoryIds: string
  LeadCategoryItemCategoryNames: string
  LeadCategoryApplicableType: number
} 