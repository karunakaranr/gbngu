export interface ILeadStage {
  StandardStageId: number;
  StageName: string;
  StageCount: number;
  StageValue: number;
  NumberOfRecords: number;
  Label: string;
}

export interface ISummarySelectDetail {
  id: number;
  name: string;
}

export interface ILeadStageSummaryDetail {
  Id: number;
  Code: string;
  Name: string;
  Rawdata: number;
  Unqualified: number;
  MQL: number;
  SAL: number;
  SQL: number;
  Opportunity: number;
  Pipeline: number;
  Closing: number;
  WonLoss: number;
  Others: number;
  Total: number;
  SlNo: number;
  RawdataValue: number;
  UnqualifiedValue: number;
  MQLValue: number;
  SALValue: number;
  SQLValue: number;
  OpportunityValue: number;
  PipelineValue: number;
  ClosingValue: number;
  WonLossValue: number;
  OthersValue: number;
  TotalValue: number;
  NumberOfRecords: number;
  Label: string;
}

export interface ILeadStageListDetail {
  StandardStage: string;
  LeadId: number;
  LeadCode: string;
  LeadName: string;
  EmployeeFirstName: string;
  EmployeeName: string;
  StageCode: string;
  StageName: string;
  LeadCategoryCode: string;
  LeadCategoryName: string;
  ExpectedDate: string;
  ExpectedRevenue: number;
  Status: string;
  Lastaction: string;
  ActivityName?: any;
  Remarks?: any;
  NextDate: string;
  FollowupTaskName?: any;
  FollowupActivityName?: any;
  NumberOfRecords: number;
  Label: string;
}
