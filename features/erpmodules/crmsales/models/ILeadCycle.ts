export interface ILeadCycle {
    LeadCycleId: number
    LeadCycleCode: string
    LeadCycleName: string
    LeadCycleRemarks: string
    LeadCycleStatus: number
    LeadCycleVersion: number
    LeadCycleDetailArray: LeadCycleDetailArray[]
    LeadCycleCreatedOn: string
    LeadCycleModifiedOn: string
    LeadCycleModifiedByName: string
    LeadCycleCreatedByName: string
    LeadCycleSourceType: number
    LeadCycleApplicableType: string
  }
  
  export interface LeadCycleDetailArray {
    LeadCycleDetailSlNo: number
    LeadCycleDetailId: string
    LeadCycleDetailIsOpen: string
    LeadCycleDetailStageCode: string
    LeadCycleDetailStageName: string
    LeadCycleDetailWightage: string
    LeadCycleDetailStandardDuration: string
    LeadCycleDetailStandardGap: string
    LeadCycleDetailMaxDropPercentage: string
    LeadCycleDetailRemarks: string
    LeadCycleDetailPreStageSlNo: number
    undefined: string
    StandardStageId: string
    LeadCycleId: number
  }
  