export class URLS {
    public static LeadCategory = '/mkts/LeadCategory.svc/';
    public static LeadSource = '/mkts/LeadSource.svc/';
    public static LeadCycle = '/mkts/LeadCycle.svc/';
    public static ContactList= '/fws/Contact.svc/ContactWithImage/';
    public static ContactListDetail= '/fws/Contact.svc/GetContactAddress/';
    public static ContactSave='/fws/Contact.svc/SaveContactDetail';
    public static fileUpload= '/fws/Entity.svc/FileUploadWebWithMetadata/?ObjectId=';
    public static fileDownload= '/fws/Entity.svc/FileDownload';
    public static LeadStageStatusReport= '/mkts/Lead.svc/LeadStageStatusReport/';
    public static LeadStageSummaryReport= '/mkts/Lead.svc/LeadStageSummaryReport/';
    public static address='/fws/Address.svc/'; 
    public static addresstype='/as/Party.svc/GetAddress/';
    public static AddressList='/as/Party.svc/AddressContactSearch/'

    // /mkts/LeadCycle.svc/
}