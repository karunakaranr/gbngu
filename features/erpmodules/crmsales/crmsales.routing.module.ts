import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LeadStageComponent } from './screens/leadstage/leadstage.component';
import { ContactListComponent } from './screens/checklist/contactlist.component';
import { ContactComponent } from './screens/ContactlistInformation/contact.component';
import { LeadCategoryComponent } from './screens/LeadCategory/leadcategory.component';
import { LeadSourceComponent } from './screens/LeadSource/leadsource.component';
import { addressComponent } from './screens/Address/address.component';
import { AddressListComponent } from './screens/AddressList/addresslist.component';
import { ContactAddressComponent } from './screens/ContactAddress/contactaddress.component';
import { LeadCycleComponent } from './screens/LeadCycle/leadcycle.component';
const routes: Routes = [


  {
    path: 'Leadstage',
    children: [
      {
        path: 'list',
        component: LeadStageComponent,
      },
    ],
  },
  {
    path: 'themestest',
    loadChildren: () =>
      import(`../../themes/themes.module`).then((m) => m.ThemesModule),
  },
  {
    path: 'contactaddresscomponent',
    component: ContactAddressComponent
  },
  {
    path: 'leadcycle',
    component: LeadCycleComponent
  },
  {
    path: 'contactlistcomponent',
    component: ContactListComponent
  },
  {
    path: 'contactDetail',
    component: ContactComponent
  },
  { 
    path: 'leadcategory',
    component: LeadCategoryComponent
  },
  {
    path: 'leadsource',
    component: LeadSourceComponent
  },
  {
    path: 'address',
    component: addressComponent
  },
  {
    path: 'AddressList',
    component: AddressListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrmsalesroutingModule {}
