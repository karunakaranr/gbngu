export interface ICodeDefine {
    length: number
    CodeDefineId: number
    CodeDefineSlNo: number
    CodeDefineCategoryCode: string
    CodeDefineCategoryName: string
    CodeDefineGenerationType: number
    CodeDefineCatPrefix: string
    CodeDefineCatSuffix: string
    CodeDefineFormulaId: number
    CodeDefineFormulaCode: string
    CodeDefineFormulaName: string
    CodeDefineFormulaBasedFields: string
    CodeDefineCatStartNo: number
    CodeDefineCatEndNo: number
    CodeDefineCatLastNo: number
    CodeDefineAutoLoadId: number
    CodeDefineRemarks: string
    CodeDefineCodeSize: number
    CodeDefineTemplateId: number
    CodeDefineCreatedById: number
    CodeDefineCreatedOn: string
    CodeDefineModifiedById: number
    CodeDefineModifiedOn: string
    CodeDefineSortOrder: number
    CodeDefineStatus: number
    CodeDefineVersion: number
    CodeDefineSourceType: number
    EntityId: number
    EntityCode: string
    EntityName: string
  }
  