export interface IFile {
    FileId: number
    FileName: string
    FileLocation: string
    FileRemarks: string
    FileType: number
    ReportId: number
    ReportName: string
    WebServiceId: number
    WebServiceName: string
    TenantId: number
    TenantCode: string
    TenantName: string
    FileCreatedById: number
    FileCreatedOn: string
    FileCreatedByName: any
    FileModifiedById: number
    FileModifiedOn: string
    FileModifiedByName: any
    FileSortOrder: number
    FileStatus: number
    FileVersion: number
    FileSourceType: number
  }