export interface ICopyNameType {
    CopyNameTypeId: number
    CopyNameTypeStatus: number
    CopyNameTypeName: string
    CopyNameTypeVersion: number
    CopyNameTypeCreatedOn: string
    CopyNameTypeModifiedOn: string
    CopyNameTypeModifiedByName: string
    CopyNameTypeCreatedByName: string
    CopyNameTypeDetailId: number
    CopyNameTypeDetailArray: CopyNameTypeDetailArray[]
  }
  
  export interface CopyNameTypeDetailArray {
    CopyNameTypeDetailCopyNumber: number
    CopyNameTypeDetailCopyName: string
  }
  