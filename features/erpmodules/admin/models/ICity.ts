export interface ICity {
    stdcode: string
    CityId: number
    CityCode: string
    CityName: string
    CountryId: number
    CountryCode: string
    CountryName: string
    StateId: number
    StateCode: string
    StateName: string
    StateTINStart: string
    CityCreatedById: number
    CityCreatedOn: string
    CityCreatedByName: any
    CityModifiedById: number
    CityModifiedOn: string
    CityModifiedByName: any
    CitySortOrder: number
    CityStatus: number
    CityVersion: number
    CitySourceType: number
    TrafficRemarks: string
    CityGeolocation: any
    CityBorder: any
    StdCode: string
    CityClassId: number
    CityClassCode: string
    CityClassName: string
  }
  