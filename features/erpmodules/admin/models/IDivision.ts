export interface IDivision {
  DivisionId: number,
  DivisionCode: string,
  DivisionName: string,
  DivisionShortName: string,
  DivisionCreatedById: number,
  DivisionCreatedByName: any,
  DivisionCreatedOn: string,
  DivisionModifiedById: number,
  DivisionModifiedByName: any,
  DivisionModifiedOn: string,
  DivisionSortOrder: number,
  DivisionStatus: number,
  DivisionVersion: number
}