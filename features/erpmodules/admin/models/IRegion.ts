export interface IRegion {
    RegionId: number
    RegionCode: string
    RegionName: string
    RegionTypeId: number
    RegionTypeCode: string
    RegionTypeName: string
    ParentRegionId: number
    ParentRegionCode: string
    ParentRegionName: string
    RegionParentRegionIds: string
    RegionLevels: number
    RegionCreatedById: number
    RegionCreatedByName: any
    RegionCreatedOn: string
    RegionModifiedById: number
    RegionModifiedByName: any
    RegionModifiedOn: string
    RegionSortOrder: number
    RegionStatus: number
    RegionVersion: number
  }
  