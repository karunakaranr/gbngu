export interface IRegionType {
    RegionTypeId: number
    RegionTypeCode: string
    RegionTypeName: string
    RegionTypeCreatedById: number
    RegionTypeCreatedOn: string
    RegionTypeCreatedByName: any
    RegionTypeModifiedById: number
    RegionTypeModifiedOn: string
    RegionTypeModifiedByName: any
    RegionTypeSortOrder: number
    RegionTypeStatus: number
    RegionTypeVersion: number
  }