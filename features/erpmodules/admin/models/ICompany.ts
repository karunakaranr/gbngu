export interface ICompany {
    CompanyId: number
    CompanyCode: string
    CompanyName: string
    CompanyShortName: string
    CompanyLogo: string
    CompanyLogo2: string
    CompanyCINNumber: string
    ImportExportCode: string
    IECDate: string
    CompanyMSMERegNo: string
    CompanyCreatedById: number
    CompanyCreatedOn: string
    CompanyCreatedByName: string
    CompanyModifiedById: number
    CompanyModifiedOn: string
    CompanyModifiedByName: string
    CompanySortOrder: number
    CompanyStatus: number
    CompanyVersion: number
  }
  