export interface IMerge {
    MergeId: number
    MergeCode: string
    MergeName: string
    ModuleId: number
    ModuleCode: string
    ModuleName: string
    MergeIsConstraintBased: number
    MergeDataType: number
    MergeLength: number
    MergePrec: number
    MergeScale: number
    MergePurpose: string
    MergeRemarks: any
    MergeInputType: number
    PicklistId: number
    PicklistCode: string
    PicklistName: string
    PicklistUri: string
    MergeDetails: MergeDetail[]
    SourceDBObjectId: number
    SourceDBObjectName: string
    SourceDbObjectFieldId: number
    SourceDbObjectFieldName: string
    MergeCreatedById: number
    MergeCreatedOn: string
    MergeModifiedById: number
    MergeModifiedOn: string
    MergeSortOrder: number
    MergeStatus: number
    MergeVersion: number
    MergeSourceType: number
  }
  
  export interface MergeDetail {
    MergeDetailId: number
    MergeId: number
    MergeCode: string
    MergeName: string
    MergeDetailSlNo: number
    MergeDetailRemarks: string
    DBObjectId: number
    DBObjectName: string
    DbObjectFieldId: number
    DbObjectFieldName: string
  }
  