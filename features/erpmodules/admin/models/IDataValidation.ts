export interface IDataValidation {
    DataValidationId: number
    DataValidationCode: string
    DataValidationName: string
    DataValidationSection: string
    ModuleId: number
    ModuleCode: string
    ModuleName: string
    DataValidationDetails: string
    DataValidationQuery: string
    DataValidationSolution: string
    DataValidationCreatedById: number
    DataValidationCreatedOn: string
    DataValidationModifiedById: number
    DataValidationModifiedOn: string
    DataValidationSortOrder: number
    DataValidationStatus: number
    DataValidationVersion: number
    DataValidationSourceType: number
  }
  