export interface IDepartment {
    DepartmentId: number
    DepartmentCode: string
    DepartmentName: string
    DepartmentIsProduction: number
    ProcessId: number
    ProcessCode: string
    ProcessName: string
    CalendarId: number
    CalendarCode: string
    CalendarName: string
    DepartmentCreatedById: number
    DepartmentCreatedByName: string
    DepartmentCreatedOn: string
    DepartmentModifiedById: number
    DepartmentModifiedByName: string
    DepartmentModifiedOn: string
    DepartmentSortOrder: number
    DepartmentStatus: number
    DepartmentVersion: number
    DepartmentHeadId: number
    DepartmentApplicableOUIds: string
    DepartmentApplicableOUNames: string
  }
  