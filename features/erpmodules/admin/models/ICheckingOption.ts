export interface ICheckingOption
 {
    CheckingOptionId: number
    CheckingOptionType: number
    EntityId: number
    EntityName: string
    BIZTransactionClassId: number
    BIZTransactionClassName: string
    CheckingOptionVersion: number
    CheckingOptionStatus: number
    CheckingOptionDetailArray: CheckingOptionDetailArray[]
    CheckingOptionCreatedOn: string
    CheckingOptionModifiedOn: string
    CheckingOptionModifiedByName: string
    CheckingOptionCreatedByName: string
    CheckingOptionSourceType: number
  }
  
  export interface CheckingOptionDetailArray {
    CheckingOptionDetailSlNo: number
    undefined: string
    CheckingId: string
    CheckingName: string
    CheckingOptionDetailDisplayText: string
  }
  