export interface ISecurityQuestion {
    SecurityQuestionId: number;
    SecurityQuestionQuestion: string;
    SecurityQuestionRemarks: string;
    SecurityQuestionStatus: number;
    SecurityQuestionVersion: number;
    SecurityQuestionCreatedByName: string;
    SecurityQuestionCreatedOn: string;
    SecurityQuestionModifiedByName: string;
    SecurityQuestionModifiedOn: string;
  }