export interface IOrganizationGroup {
    OrganizationGroupId: number
    OrganizationGroupCode: string
    OrganizationGroupName: string
    OrganizationGroupCreatedById: number
    OrganizationGroupCreatedOn: string
    OrganizationGroupCreatedByName: string
    OrganizationGroupModifiedById: number
    OrganizationGroupModifiedOn: string
    OrganizationGroupModifiedByName: string
    OrganizationGroupSortOrder: number
    OrganizationGroupStatus: number
    OrganizationGroupVersion: number
    OrganizationGroupDetailArray: OrganizationGroupDetailArray[]
  }
  
  export interface OrganizationGroupDetailArray {
    OrganizationGroupDetailId: number
    OrganizationGroupId: number
    OrganizationGroupCode: string
    OrganizationGroupName: string
    OrganizationGroupDetailSlNo: number
    OrganizationGroupDetailOperationType: number
    OrganizationGroupDetailGroupType: number
    OrganizationUnitId: number
    OrganizationUnitCode: string
    OrganizationUnitName: string
    OrganizationUnitShortName: string
    CompanyId: number
    CompanyCode: string
    CompanyName: string
    CompanyShortName: string
    BranchId: number
    BranchCode: string
    BranchName: string
    BranchShortName: string
    DivisionId: number
    DivisionCode: string
    DivisionName: string
    DivisionShortName: string
    OrganizationGroupParentId: number
    OrganizationGroupParentCode: string
    OrganizationGroupParentName: string
  }
  