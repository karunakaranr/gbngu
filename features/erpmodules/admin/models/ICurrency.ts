export interface ICurrency {
    CurrencyId: number
    CurrencyCode: string
    CurrencyName: string
    CurrencySymbol: string
    CurrencyFull: string
    CurrencyCurrencyDecimal: string
    CurrencyCreatedById: number
    CurrencyCreatedByName: string
    CurrencyCreatedOn: string
    CurrencyModifiedById: number
    CurrencyModifiedByName: string
    CurrencyModifiedOn: string
    CurrencySortOrder: number
    CurrencyStatus: number
    CurrencyVersion: number
    GainAccountId: number
    GainAccountCode: string
    GainAccountName: string
    LossAccountId: number
    LossAccountCode: string
    LossAccountName: string
    CurrencyDecimalName: string
    CurrencyGstCurrencyCode: string
    CurrencyNumberOfDecimal: number
  }
  