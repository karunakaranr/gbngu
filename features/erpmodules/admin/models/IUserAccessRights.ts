
export interface IUserAccessRights {
  UserAccessRightsId: number
  UserId: number
  UserCode: string
  UserName: string
  UserAccessRightsSlno: number
  UserAccessRightsType: number
  OrganizationUnitId: number
  OrganizationUnitCode: string
  OrganizationUnitName: string
  OrganizationGroupId: number
  OrganizationGroupCode: string
  OrganizationGroupName: string
  UserAccessRightsNoOfPeriod: number
  UserAccessRightsNoOfReadPeriod: number
  MySettingRequired: number
  UserThemeOption: number
}
