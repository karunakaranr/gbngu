export interface IState {
    StateId: number
    StateCode: string
    StateName: string
    CountryId: number
    CountryCode: string
    CountryName: string
    StateGSTStateCode: string
    StateTINStart: string
    StateCreatedById: number
    StateCreatedOn: string
    StateCreatedByName: string
    StateModifiedById: number
    StateModifiedOn: string
    StateModifiedByName: string
    StateSortOrder: number
    StateStatus: number
    StateVersion: number
    StateSourceType: number
    StateGeolocation: string
    StateBorder: string
  }
  