export interface IFormulaField {
    FormulaFieldId: number
    FormulaFieldCode: string
    FormulaFieldName: string
    ModuleId: number
    ModuleCode: string
    ModuleName: string
    FormulaFieldSection: string
    FormulaFieldDataType: number
    FormulaFieldDataSize: number
    FormulaFieldAddedType: number
    EntityId: number
    EntityCode: string
    EntityName: string
    FormulaFieldCreatedById: number
    FormulaFieldCreatedByName: any
    FormulaFieldCreatedOn: string
    FormulaFieldModifiedById: number
    FormulaFieldModifiedByName: any
    FormulaFieldModifiedOn: string
    FormulaFieldSortOrder: number
    FormulaFieldStatus: number
    FormulaFieldVersion: number
    FormulaFieldSourceType: number
  }