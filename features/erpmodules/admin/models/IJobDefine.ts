export interface IJobDefine {
    JobdefineId: number
    JobdefineName: string
    WebServiceId: number
    WebServiceName: string
    SchedulerId: number
    SchedulerName: string
    CriteriaConfigId: number
    CriteriaConfigName: string
    MenuId: number
    MenuCode: string
    MenuName: string
    JobdefineLastRunTime: string
    JobdefineType: number
    JobdefineUriTemplateValue: string
    JobdefineStatus: number
    JobdefineRemarks: string
    JobdefineCreatedById: number
    JobdefineCreatedOn: string
    JobdefineModifiedById: number
    JobdefineModifiedOn: string
    JobdefineModifiedByName: string
    JobdefineCreatedByName: string
    JobdefineSourceType: number
  }
  