export interface IInspectionArea {
    InspectionAreaId: number
    WorkCenterId: number
    WorkCenterCode: string
    WorkCenterName: string
    InspectionAreaType: string
    InspectionAreaArea: string
    InspectionAreaRemarks: string
    InspectionAreaCreatedById: number
    InspectionAreaCreatedOn: string
    InspectionAreaModifiedById: number
    InspectionAreaModifiedOn: string
    InspectionAreaCreatedByName: string
    InspectionAreaModifiedByName: string
    InspectionAreaSortOrder: number
    InspectionAreaStatus: number
    InspectionAreaVersion: number
  }
  