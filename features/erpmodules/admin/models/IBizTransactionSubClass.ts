export interface IBizTransactionSubClass {
    BIZTransactionSubClassId: number
    BIZTransactionSubClassCode: string
    BIZTransactionSubClassName: string
    BIZTransactionClassId: number
    BIZTransactionClassCode: string
    BIZTransactionClassName: string
    BIZTransactionSubClassCreatedById: number
    BIZTransactionSubClassCreatedOn: string
    BIZTransactionSubClassCreatedByName: string
    BIZTransactionSubClassModifiedById: number
    BIZTransactionSubClassModifiedOn: string
    BIZTransactionSubClassModifiedByName: string
    BIZTransactionSubClassSortOrder: number
    BIZTransactionSubClassStatus: number
    BIZTransactionSubClassVersion: number
    BIZTransactionSubClassSourceType: number
  }
  