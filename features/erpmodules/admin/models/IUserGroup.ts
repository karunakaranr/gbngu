export interface IUserGroup {
    UserGroupId: number
    UserGroupCode: string
    UserGroupName: string
    UserGroupCreatedById: number
    UserGroupCreatedOn: string
    UserGroupCreatedByName: string
    UserGroupModifiedById: number
    UserGroupModifiedOn: string
    UserGroupModifiedByName: string
    UserGroupSortOrder: number
    UserGroupStatus: number
    UserGroupVersion: number
    UserGroupSourceType: number
    UserGroupArray: UserGroupArray[]
  }
  
  export interface UserGroupArray {
    UserGroupDetailId: number
    UserGroupId: number
    UserGroupCode: string
    UserGroupName: string
    UserGroupDetailSlNo: number
    UserId: number
    UserCode: string
    UserName: string
  }
  