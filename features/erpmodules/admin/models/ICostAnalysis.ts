export interface ICostAnalysis {
    CostAnalysisId: number
    CostAnalysisCode: string
    CostAnalysisName: string
    CostAnalysisCostingType: number
    CostAnalysisPeriodFrom: string
    CostAnalysisPeriodTo: string
    CostAnalysisBasicRateType: number
    CostAnalysisMaterialCostType: number
    CostAnalysisProcessCostType: number
    CostAnalysisResourceCostType: number
    CostAnalysisIsCostpostToStock: number
    CostAnalysisSortOrder: number
    CostAnalysisStatus: number
    CostAnalysisVersion: number
    CostAnalysisCreatedById: number
    CostAnalysisCreatedOn: string
    CostAnalysisCreatedByName: string
    CostAnalysisModifiedById: number
    CostAnalysisModifiedOn: string
    CostAnalysisModifiedByName: string
  }
  