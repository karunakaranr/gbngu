import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { ICountry } from '../models/ICountry';


 
@Injectable({
  providedIn: 'root'
})
export class CountryService extends GBBaseDataServiceWN<ICountry> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICountry>) {
    super(dbDataService, 'CountryId');
  }
}
