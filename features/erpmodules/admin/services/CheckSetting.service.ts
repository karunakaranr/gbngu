import { Injectable } from '@angular/core';
import { CheckSettingDbService } from '../dbservices/CheckSetting/CheckSettingDb.service';
@Injectable({
  providedIn: 'root'
})
export class CheckSetting {
  constructor(public dbservice: CheckSettingDbService) { }

  public checksettingview(){                     //col
    return this.dbservice.picklist();
  }

  public Rowservice(obj, criteria){         //row
    return this.dbservice.reportData(obj, criteria);
  }
}