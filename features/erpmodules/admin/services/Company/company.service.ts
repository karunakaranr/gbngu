import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { ICompany } from '../../models/ICompany';


 
@Injectable({
  providedIn: 'root'
})
export class CompanyService extends GBBaseDataServiceWN<ICompany> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICompany>) {
    super(dbDataService, 'CompanyId');
  }
}
