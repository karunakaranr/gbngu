import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IAllocation } from '../../models/IAllocation';


@Injectable({
  providedIn: 'root'
})
export class AllocationService extends GBBaseDataServiceWN<IAllocation> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IAllocation>) {
    super(dbDataService, 'MAllocationId');
  }
}