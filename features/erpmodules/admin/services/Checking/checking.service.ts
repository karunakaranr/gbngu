import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IChecking } from '../../models/IChecking';




 
@Injectable({
  providedIn: 'root'
})
export class CheckingService extends GBBaseDataServiceWN<IChecking> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IChecking>) {
    super(dbDataService, 'CheckingId');
  }
}
