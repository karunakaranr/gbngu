import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IPaymentTerm } from '../../models/IPaymentTerm';




 
@Injectable({
  providedIn: 'root'
})
export class PaymentTermService extends GBBaseDataServiceWN<IPaymentTerm> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPaymentTerm>) {
    super(dbDataService, 'PaymentTermId');
  }
}
