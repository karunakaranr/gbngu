import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IPartyTaxType } from '../../models/IPartyTaxType';


 
@Injectable({
  providedIn: 'root'
})
export class PartyTaxTypeService extends GBBaseDataServiceWN<IPartyTaxType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPartyTaxType>) {
    super(dbDataService, 'PartyTaxTypeId');
  }
}
