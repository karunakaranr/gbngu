import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IFile } from '../../models/IFile';


@Injectable({
  providedIn: 'root'
})
export class FileService extends GBBaseDataServiceWN<IFile> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IFile>) {
    super(dbDataService, 'FileId');
  }
}