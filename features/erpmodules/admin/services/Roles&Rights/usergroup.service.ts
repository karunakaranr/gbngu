import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IUserGroup } from '../../models/IUserGroup';


 
@Injectable({
  providedIn: 'root'
})
export class UserGroupService extends GBBaseDataServiceWN<IUserGroup> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IUserGroup>) {
    super(dbDataService, 'UserGroupId');
  }
}
