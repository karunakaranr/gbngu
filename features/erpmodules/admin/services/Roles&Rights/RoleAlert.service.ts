import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IRoleAlert } from '../../models/IRoleAlert';


 
@Injectable({
  providedIn: 'root'
})
export class RoleAlertService extends GBBaseDataServiceWN<IRoleAlert[]> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IRoleAlert[]>) {
    super(dbDataService,'RoleAlertId', "Role");
  }
}
