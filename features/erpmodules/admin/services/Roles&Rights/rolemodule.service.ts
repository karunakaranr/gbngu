import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IRoleModule } from '../../models/IRoleModule';


 
@Injectable({
  providedIn: 'root'
})
export class RoleModuleService extends GBBaseDataServiceWN<IRoleModule[]> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IRoleModule[]>) {
    super(dbDataService, 'RoleId');
  }
}
