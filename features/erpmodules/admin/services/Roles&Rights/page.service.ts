import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IPage } from '../../models/IPage';




 
@Injectable({
  providedIn: 'root'
})
export class PageService extends GBBaseDataServiceWN<IPage> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPage>) {
    super(dbDataService, 'PageId');
  }
}
