import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IUserAccessRights } from '../../models/IUserAccessRights';


 
@Injectable({
  providedIn: 'root'
})
export class UserAccessRightService extends GBBaseDataServiceWN<IUserAccessRights> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IUserAccessRights>) {
    super(dbDataService, 'User.Id');
  }
}
