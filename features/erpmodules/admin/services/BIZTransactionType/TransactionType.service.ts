import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { ITransactionType } from '../../models/ITransactionType';


 
@Injectable({
  providedIn: 'root'
})
export class TransactionTypeService extends GBBaseDataServiceWN<ITransactionType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ITransactionType>) {
    super(dbDataService, 'TransactionTypeId');
  }
}
