import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IChargeElement } from '../../models/IChargeElement';


 
@Injectable({
  providedIn: 'root'
})
export class ChargeElementService extends GBBaseDataServiceWN<IChargeElement> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IChargeElement>) {
    super(dbDataService, 'ChargeElementId');
  }
}
