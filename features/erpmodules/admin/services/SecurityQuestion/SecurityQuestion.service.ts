import { Inject, Injectable } from '@angular/core';
// import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { ISecurityQuestion } from '../../models/ISecurityQuestion';

 
@Injectable({
  providedIn: 'root'
})
export class SecurityQuestionService extends GBBaseDataServiceWN<ISecurityQuestion> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ISecurityQuestion>) {
    super(dbDataService, 'SecurityQuestionId');
  }
}
