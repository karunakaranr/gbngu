import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';
import { ICopyNameType } from '../../models/ICopyNameType';


 
@Injectable({
  providedIn: 'root'
})
export class CopyNameTypeService extends GBBaseDataServiceWN<ICopyNameType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICopyNameType>) {
    super(dbDataService, 'CopyNameTypeId');
  }
}
