import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IPort } from '../../models/IPort';


 
@Injectable({
  providedIn: 'root'
})
export class PortService extends GBBaseDataServiceWN<IPort> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPort>) {
    super(dbDataService, 'PortId');
  }
}
