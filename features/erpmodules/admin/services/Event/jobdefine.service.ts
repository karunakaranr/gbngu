import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IJobDefine } from '../../models/IJobDefine';


 
@Injectable({
  providedIn: 'root'
})
export class JobDefineService extends GBBaseDataServiceWN<IJobDefine> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IJobDefine>) {
    super(dbDataService, 'JobdefineId');
  }
}
