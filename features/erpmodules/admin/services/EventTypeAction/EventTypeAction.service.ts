import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';



import { IEventTypeAction } from '../../models/IEventTypeAction';


 
@Injectable({
  providedIn: 'root'
})
export class EventTypeActionService extends GBBaseDataServiceWN<IEventTypeAction> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IEventTypeAction>) {
    super(dbDataService, "EventTypeAction",'EventTypeActionId');
  }
}
