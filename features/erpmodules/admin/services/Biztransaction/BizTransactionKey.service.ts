import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IBizTransactionKey } from '../../models/IBizTransactionKey';


 
@Injectable({
  providedIn: 'root'
})
export class BIZTransactionKeyService extends GBBaseDataServiceWN<IBizTransactionKey> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IBizTransactionKey>) {
    super(dbDataService, 'BIZTransactionTypeId');
  }
}
