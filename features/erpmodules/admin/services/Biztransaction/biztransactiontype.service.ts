import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IBizTransactionType } from '../../models/IBizTransactionType';


 
@Injectable({
  providedIn: 'root'
})
export class BizTransactionTypeService extends GBBaseDataServiceWN<IBizTransactionType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IBizTransactionType>) {
    super(dbDataService, 'BIZTransactionTypeId');
  }
}
