import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { ICostCenterType } from '../../models/ICostCenterType';


 
@Injectable({
  providedIn: 'root'
})
export class CostCenterTypeService extends GBBaseDataServiceWN<ICostCenterType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICostCenterType>) {
    super(dbDataService, 'CostCenterTypeId');
  }
}
