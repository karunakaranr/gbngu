import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IOrganizationGroup } from '../../models/IOrganizationGroup';


 
@Injectable({
  providedIn: 'root'
})
export class OrganizationGroupService extends GBBaseDataServiceWN<IOrganizationGroup> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IOrganizationGroup>) {
    super(dbDataService, 'OrganizationGroupId');
  }
}
