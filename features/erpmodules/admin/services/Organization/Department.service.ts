import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IDepartment } from '../../models/IDepartment';


 
@Injectable({
  providedIn: 'root'
})
export class DepartmentService extends GBBaseDataServiceWN<IDepartment> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IDepartment>) {
    super(dbDataService, 'DepartmentId');
  }
}
