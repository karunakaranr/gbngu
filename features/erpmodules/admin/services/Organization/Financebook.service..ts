import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IFinanceBook } from '../../models/IFinanceBook';


 
@Injectable({
  providedIn: 'root'
})
export class FinanceBookService extends GBBaseDataServiceWN<IFinanceBook> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IFinanceBook>) {
    super(dbDataService, 'FinanceBookId');
  }
}
