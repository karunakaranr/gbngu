import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IWorkFlowRule } from '../../models/IWorkFlowRule ';
import { WorkFlowRuleService } from '../../services/WorkFlowRule/WorkFlowRule.service'
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { ADMINURLS } from '../../URLS/url';
import * as WorkFlowRuleJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/WorkFlowRule.json'

@Component({
  selector: 'app-WorkFlowRule',
  templateUrl: 'workflowrule.component.html',
  styleUrls:['workflowrule.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'WorkFlowRuleId' },
    { provide: 'url', useValue:ADMINURLS.Workflow},
    { provide: 'DataService', useClass: WorkFlowRuleService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class WorkFlowRuleComponent extends GBBaseDataPageComponentWN<IWorkFlowRule> {
  title: string = "WorkFlowRule"
  WorkFlowRuleJSON= WorkFlowRuleJSON;

  form: GBDataFormGroupWN<IWorkFlowRule> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "WorkFlowRule", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.WorkFlowRulevalue(arrayOfValues.WorkFlowRuleId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public WorkFlowRulevalue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(WorkFlowRule => {
          this.form.patchValue(WorkFlowRule);
      })
    }
  }
  public WorkFlowRuleNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IWorkFlowRule> {
  const dbds: GBBaseDBDataService<IWorkFlowRule> = new GBBaseDBDataService<IWorkFlowRule>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IWorkFlowRule>, dbDataService: GBBaseDBDataService<IWorkFlowRule>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IWorkFlowRule> {
  return new GBDataPageService<IWorkFlowRule>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
