import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IAllocationType } from '../../models/IAllocationType';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { AllocationTypeService } from '../../services/Allocation/allocationtype.service';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import * as AllocationTypeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/AllocationType.json'

@Component({


  selector: 'app-Allocation',
  templateUrl: './allocationtype.component.html',
  styleUrls: ['./allocationtype.component.scss'],

  providers: [
    { provide: 'IdField', useValue: 'AllocationTypeId'},
    { provide: 'url', useValue: ADMINURLS.AllocationType },
    { provide: 'DataService', useClass: AllocationTypeService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class AllocationTypeComponent extends GBBaseDataPageComponentWN<IAllocationType> {
  title : string = "AllocationType"
  AllocationTypeJSON = AllocationTypeJSON

form: GBDataFormGroupWN<IAllocationType> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "AllocationType", {}, this.gbps.dataService);
  thisConstructor() {

    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
   
      this.AllocationTypePicklistFillValue(arrayOfValues. AllocationTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }


  public AllocationTypePicklistFillValue(SelectedPicklistData: string): void {
    console.log("SelectedPicklistData**",SelectedPicklistData)
    if (SelectedPicklistData) {
          console.log("category",SelectedPicklistData)
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormVariable => {
        console.log("FormVariable",FormVariable)
          this.form.patchValue(FormVariable);

        })
    }
  }


  public AllocationTypePicklistPatchValue(SelectedPicklistDatas: any): void {
    console.log("category",SelectedPicklistDatas)
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)

  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IAllocationType> {
  const dbds: GBBaseDBDataService<IAllocationType> = new GBBaseDBDataService<IAllocationType>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IAllocationType>, dbDataService: GBBaseDBDataService<IAllocationType>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IAllocationType> {
  return new GBDataPageService<IAllocationType>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
