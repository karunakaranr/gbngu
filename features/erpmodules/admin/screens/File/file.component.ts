import { Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Select, Store } from '@ngxs/store';
import { FileService } from '../../services/File/File.Service';
import { IFile } from '../../models/IFile';
import { ADMINURLS } from '../../URLS/url';
import * as FileJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/File.json'
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-File',
  templateUrl: 'file.component.html',
  styleUrls:['file.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'FileId' },
    { provide: 'url', useValue:ADMINURLS.File},
    { provide: 'DataService', useClass: FileService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class FileComponent extends GBBaseDataPageComponentWN<IFile> {
  @ViewChild('fileInput') fileInput: ElementRef;
  @Select(LayoutState.FormEdit) edit$: Observable<any>;
  title: string = "File"
  FileJSON= FileJSON;
  selectedFile: File;
  isreadables:boolean;
  form: GBDataFormGroupWN<IFile> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "File", {}, this.gbps.dataService);
  thisConstructor() {
    this.edit$.subscribe(res => {
      this.isreadables = res
    })
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.Filevalue(arrayOfValues.FileId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public Filevalue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(File => {
          this.form.patchValue(File);
      })
    }
  }

  openFileInput() {
    if(this.isreadables){
      this.fileInput.nativeElement.click();
    }
  }

  public browse(event){
    console.log("Event:",event)
    const selectedFile = event.target.files[0].name
    this.form.get('FileName').patchValue(selectedFile)
  }
 
  public FileNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
 

 
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IFile> {
  const dbds: GBBaseDBDataService<IFile> = new GBBaseDBDataService<IFile>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IFile>, dbDataService: GBBaseDBDataService<IFile>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IFile> {
  return new GBDataPageService<IFile>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
