import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { CityService } from '../../services/City/City.Service';
import { ICity } from '../../models/ICity';
import { ADMINURLS } from '../../URLS/url';
import * as CityJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/City.json'
import { ButtonVisibility } from 'features/layout/store/layout.actions';
@Component({
  selector: 'app-City',
  templateUrl: 'city.component.html',
  styleUrls:['city.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'CityId' },
    { provide: 'url', useValue:ADMINURLS.City},
    { provide: 'DataService', useClass: CityService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class CityComponent extends GBBaseDataPageComponentWN<ICity> {
  title: string = "City"
  CityJSON= CityJSON;

  form: GBDataFormGroupWN<ICity> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "City", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.Cityvalue(arrayOfValues.CityId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public Cityvalue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(City => {
          this.form.patchValue(City);
      })
    }
  }
  public CityNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICity> {
  const dbds: GBBaseDBDataService<ICity> = new GBBaseDBDataService<ICity>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ICity>, dbDataService: GBBaseDBDataService<ICity>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICity> {
  return new GBDataPageService<ICity>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
