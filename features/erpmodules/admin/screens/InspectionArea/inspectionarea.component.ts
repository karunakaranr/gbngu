import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import { InspectionAreaService } from '../../services/InspectionArea/inspectionarea.service';
import { IInspectionArea } from '../../models/IInspectionArea';
import * as InspectionAreaJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/InspectionArea.json'

@Component({
    selector: 'app-InspectionArea',
    templateUrl:'./inspectionarea.component.html',
    styleUrls: ['./inspectionarea.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'InspectionAreaId' },
        { provide: 'url', useValue: ADMINURLS.InspectionArea },
        { provide: 'DataService', useClass: InspectionAreaService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class InspectionAreaComponent extends GBBaseDataPageComponentWN<IInspectionArea> {
    title: string = "InspectionArea"
    InspectionAreaJSON = InspectionAreaJSON

    form: GBDataFormGroupWN<IInspectionArea> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "InspectionArea", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.InspectionAreaPicklistFillValue(arrayOfValues.InspectionAreaId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }
  
    public InspectionAreaPicklistFillValue(SelectedPicklistData: string): void {
        // console.log("id",SelectedPicklistData)
        if (SelectedPicklistData) {
            // console.log("id",SelectedPicklistData)

            this.gbps.dataService.getData(SelectedPicklistData).subscribe(InspectionArea => {
                // console.log("InspectionArea",InspectionArea)
                    this.form.patchValue(InspectionArea);                
               
            })
        }
    }

    public InspectionAreaPicklistPatchValue(SelectedPicklistDatas: any): void {
        // console.log("id",SelectedPicklistDatas)
        console.log("SelectedPicklistDatasRetrival",SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IInspectionArea> {
    const dbds: GBBaseDBDataService<IInspectionArea> = new GBBaseDBDataService<IInspectionArea>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<IInspectionArea>, dbDataService: GBBaseDBDataService<IInspectionArea>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IInspectionArea> {
    return new GBDataPageService<IInspectionArea>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
