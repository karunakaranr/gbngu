import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { PeriodService } from '../../services/Period/period.service';
import { IPeriod } from '../../models/IPeriod';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as PeriodJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Period.json';

import { ADMINURLS } from '../../URLS/urls';


@Component({
  selector: 'app-period',
  templateUrl: './period.component.html',
  styleUrls: ['./period.component.scss'], 
  providers: [
    { provide: 'IdField', useValue: 'PeriodId'},
    { provide: 'url', useValue: ADMINURLS.Period},
    { provide: 'DataService', useClass: PeriodService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PeriodComponent extends GBBaseDataPageComponentWN<IPeriod> {
  title: string = 'Period'
  PeriodJson = PeriodJson;

  form: GBDataFormGroupWN<IPeriod> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Period', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PeriodRetrivalValue(arrayOfValues.PeriodId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public PeriodRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Period => {
        this.form.patchValue(Period);
      })
    }
  }
  public PeriodPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPeriod> {
  const dbds: GBBaseDBDataService<IPeriod> = new GBBaseDBDataService<IPeriod>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPeriod>, dbDataService: GBBaseDBDataService<IPeriod>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPeriod> {
  return new GBDataPageService<IPeriod>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
