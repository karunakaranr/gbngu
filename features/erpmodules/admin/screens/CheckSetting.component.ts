import { Component, OnInit } from '@angular/core';
import { CheckSetting } from '../../admin/services/CheckSetting.service';
const col = require('./../../../../apps/Goodbooks/Goodbooks/src/assets/data/checksetting.json')

@Component({
  selector: 'app-CheckSetting',
  templateUrl: './CheckSetting.component.html',
  styleUrls: ['./CheckSetting.component.scss']
})

export class CheckSettingComponent implements OnInit {
  title = 'Checking Setting'
  public rowData = []
  public columnData = []

  constructor(public service: CheckSetting) { }
  ngOnInit(): void {
    this.checksetting();
  }
  public checksetting() {
    this.service.checksettingview().subscribe(menudetails => {
      this.columnData = col;
      let servicejsondetail = menudetails
      this.service.Rowservice(servicejsondetail, '').subscribe(res => {
        this.rowData = res.ReportDetail;
        console.log("dkd", res);

        let json = this.rowData;
          var finalizedArray = []
          json.map(row => {
          finalizedArray.push({
            oucode: row['OUCode'],
            classcode: row['BizTransactionClassCode'],
            typecode: row['BizTransactionTypeCode'],
            code: row['CheckingCode'],
            name: row['CheckingName'],
            display: row['CheckingDisplayText'],
            type: row['CheckingType'],
          })
        })

        const final = {};
        finalizedArray.forEach(detail => {
          final[detail.oucode] = {
            accountGroup1 :{},
            code:detail.oucode,
            name:"",
            display:"",
            type:"",
            ...final[detail.oucode], 
          }

          final[detail.oucode].accountGroup1[detail.classcode] =  {
            accountGroup2: {},
            code: detail.classcode,
            name: "",
            display: "",
            type: "",
            ...final[detail.oucode].accountGroup1[detail.classcode],
          }
          final[detail.oucode].accountGroup1[detail.classcode].accountGroup2[detail.typecode] = {
            accountGroup3:{},
            code: detail.typecode,
            name: "",
            display: "",
            type:"",
            ...final[detail.oucode].accountGroup1[detail.classcode].accountGroup2[detail.typecode],
          }
          final[detail.oucode].accountGroup1[detail.classcode].accountGroup2[detail.typecode].accountGroup3[detail.code] = {
            code: detail.code,
            name: detail.name,
            display: detail.display,
            type: detail.type,
            ...final[detail.oucode].accountGroup1[detail.classcode].accountGroup2[detail.typecode].accountGroup3[detail.code]
          }
        })

        const grpcodes = Object.keys(final)

        const tableData = []
        grpcodes.forEach(code => {
          const accountGroup1 = Object.keys(final[code].accountGroup1)

          tableData.push({
            code: final[code].code,
            name: final[code].name,
            display:"",
            type:"",
            bold: true,
          })

          accountGroup1.forEach(ag => {
            tableData.push({
              code: final[code].accountGroup1[ag].code,
              name: final[code].accountGroup1[ag].name,
              display:"",
              type:"",
              bold: true,
            })
        
            const accountGroup2 = Object.keys(final[code].accountGroup1[ag].accountGroup2)
            accountGroup2.forEach(account => {
              tableData.push({
                code: final[code].accountGroup1[ag].accountGroup2[account].code,
                name: final[code].accountGroup1[ag].accountGroup2[account].name,
                display:"",
                type: "",
                bold: true,
              })
              const accountGroup3 =  Object.keys(final[code].accountGroup1[ag].accountGroup2[account].accountGroup3)
              accountGroup3.forEach(acc => {
                tableData.push({
                    code: final[code].accountGroup1[ag].accountGroup2[account].accountGroup3[acc].code,
                    name: final[code].accountGroup1[ag].accountGroup2[account].accountGroup3[acc].name,
                    display: final[code].accountGroup1[ag].accountGroup2[account].accountGroup3[acc].display,
                    type: final[code].accountGroup1[ag].accountGroup2[account].accountGroup3[acc].type,
                })
              })
            })
          })
        })
        this.rowData = tableData;
        });

    })
    
    }
    
    }