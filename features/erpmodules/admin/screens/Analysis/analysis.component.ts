import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { AnalysisService } from '../../services/Analysis/analysis.service';
import { IAnalysis } from '../../models/IAnalysis';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as AnalysisJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Analysis.json';
import { ADMINURLS } from '../../URLS/urls';



@Component({
  selector: 'app-analysis',
  templateUrl: './analysis.component.html',
  styleUrls: ['./analysis.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'AnalysisId'},
    { provide: 'url', useValue: ADMINURLS.analysis },
    { provide: 'DataService', useClass: AnalysisService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class AnalysisComponent extends GBBaseDataPageComponentWN<IAnalysis> {
  title: string = "Analysis"
  AnalysisJSON = AnalysisJSON;
IAnalysis

  form: GBDataFormGroupWN<IAnalysis> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Analysis", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.AnalysisRetrivalValue(arrayOfValues.AnalysisId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public AnalysisRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(analysis => {
        this.form.patchValue(analysis);
      })
    }
  }


  public AnalysisPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IAnalysis> {
  const dbds: GBBaseDBDataService<IAnalysis> = new GBBaseDBDataService<IAnalysis>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IAnalysis>, dbDataService: GBBaseDBDataService<IAnalysis>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IAnalysis> {
  return new GBDataPageService<IAnalysis>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
