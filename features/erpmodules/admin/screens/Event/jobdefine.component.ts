import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { IJobDefine } from 'features/erpmodules/admin/models/IJobDefine';
import { JobDefineService } from '../../services/Event/jobdefine.service';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls'
import * as JobDefineJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/JobDefine.json'

@Component({
    selector: 'app-Event',
    templateUrl: './jobdefine.component.html',
    styleUrls: ['./jobdefine.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'JobdefineId' },
        { provide: 'url', useValue: ADMINURLS.JobDefine },
        { provide: 'DataService', useClass: JobDefineService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class JobDefineComponent extends GBBaseDataPageComponentWN<IJobDefine> {
    title: string = "JobDefine"
    JobDefineJSON = JobDefineJSON
    form: GBDataFormGroupWN<IJobDefine> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "JobDefine", {}, this.gbps.dataService);

    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.JobDefinePicklistFillValue(arrayOfValues.JobdefineId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }
  

    public JobDefinePicklistFillValue(SelectedPicklistData: string): void {
        if (SelectedPicklistData) {
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(JobDefine => {
                    // console.log("JobDefine:", JobDefine)
                    this.form.patchValue(JobDefine);
                    // this.dependentPicklistJobDefine[0].values = this.form.get("ModuleId").value;
                    // console.log("Form Value:", this.form.value)       
                
            })
        }
    }



    public JobDefinePicklistPatchValue(SelectedPicklistDatas: any): void {
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
       
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IJobDefine> {
    const dbds: GBBaseDBDataService<IJobDefine> = new GBBaseDBDataService<IJobDefine>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<IJobDefine>, dbDataService: GBBaseDBDataService<IJobDefine>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IJobDefine> {
    return new GBDataPageService<IJobDefine>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
