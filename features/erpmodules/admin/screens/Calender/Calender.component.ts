import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { CalenderService } from '../../services/Calender/Calender.service';
import { ICalender } from '../../models/ICalender';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as CalenderJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Calender.json';
import { ADMINURLS } from '../../URLS/urls';
import { DatePipe } from '@angular/common';


@Component({
  selector: "app-Calender",
  templateUrl: './Calender.component.html',
  styleUrls: ['./Calender.component.scss'],
  providers: [
    { provide: 'IdField', useValue: "CalenderId"},
    { provide: 'url', useValue: ADMINURLS.Calender },
    { provide: 'DataService', useClass: CalenderService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class CalenderComponent extends GBBaseDataPageComponentWN<ICalender> {
  title: string = "Calender"
  CalenderJSON = CalenderJSON;
  CalendarFromDate: string;
  CalendarToDate: string;
  form: GBDataFormGroupWN<ICalender> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Calender", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.CalenderFormFillFunction(arrayOfValues.CalenderId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public CalenderFormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }

  public PicklistPatchValueForMultipleControls(SelectedPicklistDatas: any): void {
    let linkids = SelectedPicklistDatas.id.split(',')
    let linkvalues = SelectedPicklistDatas.value.split(',')
    for(let i = 0;i<linkids.length;i++){
        this.form.get(linkids[i]).patchValue(SelectedPicklistDatas.Selected[linkvalues[i]])
    }
  }

  public GridFill(){
    let url = "/ads/Calendar.svc/Get/Calendar/Load/Beween/Dates/?CalendarId="+this.form.get('CalendarId').value+"&FromDate="+this.CalendarFromDate+"&ToDate="+this.CalendarToDate
    this.http.httpget(url).subscribe(res=>{
        this.form.get('CalendarDetailArray').patchValue(res.CalendarDetailArray)
    })
  }

  public FromDate(FromDate:string):void{
    var datePipe = new DatePipe("en-US");
    this.CalendarFromDate = datePipe.transform(FromDate,'dd/MMM/yyyy')
  }

  public ToDate(ToDate:string):void{
    var datePipe = new DatePipe("en-US");
    this.CalendarToDate = datePipe.transform(ToDate,'dd/MMM/yyyy')
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICalender> {
  const dbds: GBBaseDBDataService<ICalender> = new GBBaseDBDataService<ICalender>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ICalender>, dbDataService: GBBaseDBDataService<ICalender>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICalender> {
  return new GBDataPageService<ICalender>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
