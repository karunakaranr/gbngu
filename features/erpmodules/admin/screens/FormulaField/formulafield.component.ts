import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IFormulaField } from '../../models/IFormulaField';
import { FormulaFieldService } from '../../services/FormulaField/FormulaField.service';
import { ADMINURLS } from '../../URLS/url';


import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as FormulaFieldJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/FormulaField.json'
@Component({
  selector: 'app-FormulaField',
  templateUrl: 'formulafield.component.html',
  styleUrls: ['formulafield.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'FormulaFieldId' },
    { provide: 'url', useValue: ADMINURLS.FormulaField },
    { provide: 'DataService', useClass: FormulaFieldService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class FormulaFieldComponent extends GBBaseDataPageComponentWN<IFormulaField> {
  title: string = "FormulaField"
  FormulaFieldJSON = FormulaFieldJSON;


  form: GBDataFormGroupWN<IFormulaField> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "FormulaField", {}, this.gbps.dataService);
  thisConstructor() {

    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormulaFieldvalue(arrayOfValues.FormulaFieldId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

  public Clickcombobox(): void {

    if (this.form.get('FormulaFieldDataType').value == "0") {
      this.form.get('FormulaFieldDataSize').patchValue('18.4');
    }
    if (this.form.get('FormulaFieldDataType').value == "1") {
      this.form.get('FormulaFieldDataSize').patchValue('');
    }
    if (this.form.get('FormulaFieldDataType').value == "2") {
      this.form.get('FormulaFieldDataSize').patchValue('');
    }
    if (this.form.get('FormulaFieldDataType').value == "3") {
      this.form.get('FormulaFieldDataSize').patchValue('0');
    }
    
  
  }
  public FormulaFieldvalue(SelectedPicklistData: string): void {

    if (SelectedPicklistData) {
      console.log("hi", SelectedPicklistData)
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormulaField => {
        this.form.patchValue(FormulaField);
      })
    }
  }
  public FormulaFieldNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IFormulaField> {
  const dbds: GBBaseDBDataService<IFormulaField> = new GBBaseDBDataService<IFormulaField>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IFormulaField>, dbDataService: GBBaseDBDataService<IFormulaField>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IFormulaField> {
  return new GBDataPageService<IFormulaField>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
