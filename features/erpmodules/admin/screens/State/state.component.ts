import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { StateService } from '../../services/State/State.service';
import { IState } from '../../models/IState';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as StateJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/State.json';
import { ADMINURLS } from '../../URLS/urls';


@Component({
  selector: 'app-state',
  templateUrl: './state.component.html',
  styleUrls: ['./state.component.scss'], 
  providers: [
    { provide: 'IdField', useValue: 'StateId'},
    { provide: 'url', useValue: ADMINURLS.State},
    { provide: 'DataService', useClass: StateService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class StateComponent extends GBBaseDataPageComponentWN<IState> {
  title: string = 'State'
  StateJson = StateJson;

  form: GBDataFormGroupWN<IState> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'State', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.StateRertivalValue(arrayOfValues.StateId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public StateRertivalValue(SelectedPicklistData: string): void { 
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(State => {
        this.form.patchValue(State);
      })
    }
  }
  public StatePatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IState> {
  const dbds: GBBaseDBDataService<IState> = new GBBaseDBDataService<IState>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IState>, dbDataService: GBBaseDBDataService<IState>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IState> {
  return new GBDataPageService<IState>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
