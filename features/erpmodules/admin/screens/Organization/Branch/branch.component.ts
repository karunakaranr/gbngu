import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBranch } from 'features/erpmodules/admin/models/IBranch';
import { BranchService } from 'features/erpmodules/admin/services/Branch/branch.service';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import * as BranchJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Branch.json'

@Component({
    selector: 'app-Branch',
    templateUrl:'./branch.component.html',
    styleUrls: ['./branch.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'BranchId' },
        { provide: 'url', useValue: ADMINURLS.Branch },
        { provide: 'DataService', useClass: BranchService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class BranchComponent extends GBBaseDataPageComponentWN<IBranch> {
    title: string = "Branch"
    BranchJSON = BranchJSON

    form: GBDataFormGroupWN<IBranch> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Branch", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.BranchPicklistFillValue(arrayOfValues.BranchId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }
  
    public BranchPicklistFillValue(SelectedPicklistData: string): void {
        // console.log("id",SelectedPicklistData)
        if (SelectedPicklistData) {
            // console.log("id",SelectedPicklistData)

            this.gbps.dataService.getData(SelectedPicklistData).subscribe(Branch => {
                // console.log("Branch",Branch)
                    this.form.patchValue(Branch);                
               
            })
        }
    }

    public BranchPicklistPatchValue(SelectedPicklistDatas: any): void {
        // console.log("id",SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IBranch> {
    const dbds: GBBaseDBDataService<IBranch> = new GBBaseDBDataService<IBranch>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<IBranch>, dbDataService: GBBaseDBDataService<IBranch>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IBranch> {
    return new GBDataPageService<IBranch>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
