import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { ICompany } from 'features/erpmodules/admin/models/ICompany';
import { CompanyService } from 'features/erpmodules/admin/services/Company/company.service';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import * as CompanyJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Company.json'


@Component({
    selector: 'app-Company',
    templateUrl:'./company.component.html',
    styleUrls: ['./company.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'CompanyId' },
        { provide: 'url', useValue: ADMINURLS.Company },
        { provide: 'DataService', useClass: CompanyService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class CompanyComponent extends GBBaseDataPageComponentWN<ICompany> {
    title: string = "Company"
    CompanyJSON =CompanyJSON

    form: GBDataFormGroupWN<ICompany> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Company", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.CompanyPicklistFillValue(arrayOfValues.CompanyId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }

    public CompanyPicklistFillValue(SelectedPicklistData: string): void {
        if (SelectedPicklistData) {
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(Company => {
                    this.form.patchValue(Company);    
            })
        }
    }



    public CompanyPicklistPatchValue(SelectedPicklistDatas: any): void {
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICompany> {
    const dbds: GBBaseDBDataService<ICompany> = new GBBaseDBDataService<ICompany>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<ICompany>, dbDataService: GBBaseDBDataService<ICompany>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICompany> {
    return new GBDataPageService<ICompany>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
