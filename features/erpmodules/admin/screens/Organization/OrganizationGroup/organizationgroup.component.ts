import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { IOrganizationGroup } from 'features/erpmodules/admin/models/IOrganizationGroup';
import { OrganizationGroupService } from 'features/erpmodules/admin/services/Organization/Organizationgroup.service';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import * as OrganizationGroupJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/OrganizationGroup.json'
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';

@Component({
    selector: 'app-OrganizationGroup',
    templateUrl:'./organizationgroup.component.html',
    styleUrls: ['./organizationgroup.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'OrganizationGroupId' },
        { provide: 'url', useValue: ADMINURLS.OrganizationGroup },
        { provide: 'DataService', useClass: OrganizationGroupService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class OrganizationGroupComponent extends GBBaseDataPageComponentWN<IOrganizationGroup> {
    title: string = "OrganizationGroup"
    OrganizationGroupJSON = OrganizationGroupJSON

    form: GBDataFormGroupWN<IOrganizationGroup> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "OrganizationGroup", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.OrganizationGroupPicklistFillValue(arrayOfValues.OrganizationGroupId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }

  
    public OrganizationGroupPicklistFillValue(SelectedPicklistData: string): void {
        // console.log("id",SelectedPicklistData)
        if (SelectedPicklistData) {
            // console.log("id",SelectedPicklistData)

            this.gbps.dataService.getData(SelectedPicklistData).subscribe(organization => {
                // console.log("id",SelectedPicklistData)
                    this.form.patchValue(organization);
 
            })
        }
    }



    public OrganizationGroupPicklistPatchValue(SelectedPicklistDatas: any): void {
        // console.log("id",SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }

}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IOrganizationGroup> {
    const dbds: GBBaseDBDataService<IOrganizationGroup> = new GBBaseDBDataService<IOrganizationGroup>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<IOrganizationGroup>, dbDataService: GBBaseDBDataService<IOrganizationGroup>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IOrganizationGroup> {
    return new GBDataPageService<IOrganizationGroup>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
