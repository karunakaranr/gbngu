import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import { CodeDefineService } from 'features/erpmodules/admin/services/Organization/CodeDefine.service';
import { ICodeDefine } from 'features/erpmodules/admin/models/ICodeDefine';
import * as CodeDefineJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CodeDefine.json'

@Component({
    selector: 'app-CodeDefine',
    templateUrl: './codedefine.component.html',
    styleUrls: ['./codedefine.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'EntityId' },
        { provide: 'url', useValue: ADMINURLS.CodeDefine },
        { provide: 'DataService', useClass: CodeDefineService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class CodeDefineComponent extends GBBaseDataPageComponentWN<ICodeDefine> {
    title: string = "CodeDefine"
    CodeDefineJSON = CodeDefineJSON

    form: GBDataFormGroupWN<ICodeDefine> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "CodeDefine", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.CodeDefinePicklistFillValue(arrayOfValues.CodeDefineId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }
// public GetGridValue(GridValues:any){
//     console.log("GridValues",GridValues)
//     this.form.get('EntityId').patchValue(GridValues);

// }
    public CodeDefinePicklistFillValue(SelectedPicklistData): void {
        if (SelectedPicklistData) {
            console.log("SelectedPicklistData", SelectedPicklistData)
            this.gbps.dataService.DgetData(SelectedPicklistData).subscribe(CodeDefine => {
                console.log("KKKK", CodeDefine);

                if (CodeDefine[0].CodeDefineSlNo == 0) {
                    this.form.get('MultiCategory').patchValue(2)
                    this.form.get('CodeDefineArray').patchValue(CodeDefine);
                    this.form.get('EntityId').patchValue(CodeDefine[0].EntityId)

                }
                else{
                    this.form.get('MultiCategory').patchValue(1)
                    this.form.patchValue(CodeDefine[0]);
                }
                console.log("this.form",this.form.value)
            })
        }
    }


    public CodeDefinePicklistPatchValue(SelectedPicklistDatas: any): void {
        console.log("MMM", SelectedPicklistDatas);
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICodeDefine> {
    const dbds: GBBaseDBDataService<ICodeDefine> = new GBBaseDBDataService<ICodeDefine>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<ICodeDefine>, dbDataService: GBBaseDBDataService<ICodeDefine>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICodeDefine> {
    return new GBDataPageService<ICodeDefine>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
