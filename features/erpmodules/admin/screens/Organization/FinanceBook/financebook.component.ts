import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { FinanceBookService } from 'features/erpmodules/admin/services/Organization/Financebook.service.';
import { IFinanceBook } from 'features/erpmodules/admin/models/IFinanceBook';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import * as FinanceBookJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/FinanceBook.json'

@Component({
    selector: 'app-FinanceBook',
    templateUrl: './financebook.component.html',
    styleUrls: ['./financebook.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'FinanceBookId' },
        { provide: 'url', useValue: ADMINURLS.FinanceBook },
        { provide: 'DataService', useClass: FinanceBookService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class FinanceBookComponent extends GBBaseDataPageComponentWN<IFinanceBook> {
    title: string = "FinanceBook"
    FinanceBookJSON = FinanceBookJSON


    form: GBDataFormGroupWN<IFinanceBook> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "FinanceBook", {}, this.gbps.dataService);
    FinanceBookBookType: number;
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.FinanceBookPicklistValue(arrayOfValues.FinanceBookId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }
    settingPatchValueToZero(value: number): void {
        this.FinanceBookBookType = value;
        if (this.form.get('FinanceBookBookType').value == "0" ) {
            this.form.get('BaseBookId').patchValue('-1');
            this.form.get('BaseBookName').patchValue('NONE');
            this.form.get('FromCurrencyId').patchValue('-1');
            this.form.get('FromCurrencyName').patchValue('NONE');
            this.form.get('DifferenceAccountId').patchValue('-1');
            this.form.get('ToCurrencyName').patchValue('NONE');
            this.form.get('BIZTransactionId').patchValue('-1');
            this.form.get('BIZTransactionName').patchValue('NONE');
            this.form.get('ToCurrencyId').patchValue('-1');
            this.form.get('DifferenceAccountName').patchValue('NONE');
            this.form.get('FinanceBookBookIds').patchValue("-1");
            this.form.get('FinanceBookOUNames').patchValue("NONE");
            this.form.get('FinanceBookOUIds').patchValue("-1");
            this.form.get('FinanceBookBookNames').patchValue("NONE");
        }
        if(this.form.get('FinanceBookBookType').value == "1"){
            
        }
        if(this.form.get('FinanceBookBookType').value == "2"){
            this.form.get('FromCurrencyId').patchValue('-1');
            this.form.get('FromCurrencyName').patchValue('NONE');
            this.form.get('DifferenceAccountId').patchValue('-1');
            this.form.get('ToCurrencyName').patchValue('NONE');
            this.form.get('BIZTransactionId').patchValue('-1');
            this.form.get('BIZTransactionName').patchValue('NONE');
            this.form.get('ToCurrencyId').patchValue('-1');
            this.form.get('DifferenceAccountName').patchValue('NONE');
            this.form.get('FinanceBookBookIds').patchValue("-1");
            this.form.get('FinanceBookOUNames').patchValue("NONE");
            this.form.get('FinanceBookOUIds').patchValue("-1");
            this.form.get('FinanceBookBookNames').patchValue("NONE");
        }
        if(this.form.get('FinanceBookBookType').value == "3"){
            this.form.get('FromCurrencyId').patchValue('-1');
            this.form.get('FromCurrencyName').patchValue('NONE');
            this.form.get('BIZTransactionId').patchValue('-1');
            this.form.get('BIZTransactionName').patchValue('NONE');
            this.form.get('ToCurrencyId').patchValue('-1');
            this.form.get('ToCurrencyName').patchValue('NONE');
            this.form.get('DifferenceAccountId').patchValue('-1');
            this.form.get('DifferenceAccountName').patchValue('NONE');
            this.form.get('FinanceBookBookIds').patchValue("-1");
            this.form.get('FinanceBookOUNames').patchValue("NONE");
            this.form.get('FinanceBookOUIds').patchValue("-1");
            this.form.get('FinanceBookBookNames').patchValue("NONE");

        }
        if(this.form.get('FinanceBookBookType').value == "4"){
            this.form.get('BaseBookId').patchValue('-1');
            this.form.get('BaseBookName').patchValue('NONE');
            this.form.get('FromCurrencyId').patchValue('-1');
            this.form.get('FromCurrencyName').patchValue('NONE');
            this.form.get('ToCurrencyId').patchValue('-1');
            this.form.get('ToCurrencyName').patchValue('NONE');
            this.form.get('BIZTransactionId').patchValue('-1');
            this.form.get('BIZTransactionName').patchValue('NONE');
            this.form.get('DifferenceAccountId').patchValue('-1');
            this.form.get('DifferenceAccountName').patchValue('NONE');  
    
        }
    }
 
    public FinanceBookPicklistValue(SelectedPicklistData: string): void {
        // console.log("id", SelectedPicklistData)
        if (SelectedPicklistData) {
            // console.log("id", SelectedPicklistData)

            this.gbps.dataService.getData(SelectedPicklistData).subscribe(Financebook => {
                // console.log("id", SelectedPicklistData)
                    this.form.patchValue(Financebook);
                    this.FinanceBookBookType = this.form.get('FinanceBookBookType').value;
                     
            })
        }
    }


    public FinanceBookPicklistFillValue(SelectedPicklistDatas: any): void {
        // console.log("id", SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)

    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IFinanceBook> {
    const dbds: GBBaseDBDataService<IFinanceBook> = new GBBaseDBDataService<IFinanceBook>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<IFinanceBook>, dbDataService: GBBaseDBDataService<IFinanceBook>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IFinanceBook> {
    return new GBDataPageService<IFinanceBook>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
