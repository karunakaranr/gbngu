import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { IUserGroup } from 'features/erpmodules/admin/models/IUserGroup';
import { UserGroupService } from 'features/erpmodules/admin/services/Roles&Rights/usergroup.service';

import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import * as UserGroupJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/UserGroup.json'

@Component({


  selector: 'app-UserGroup',
  templateUrl: './usergroup.component.html',
  styleUrls: ['./usergroup.component.scss'],

  providers: [
    { provide: 'IdField', useValue: 'UserGroupId'},
    { provide: 'url', useValue: ADMINURLS.UserGroup },
    { provide: 'DataService', useClass: UserGroupService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class UserGroupComponent extends GBBaseDataPageComponentWN<IUserGroup> {
  title : string = "UserGroup"
  UserGroupJSON = UserGroupJSON

form: GBDataFormGroupWN<IUserGroup> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "UserGroup", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
      this.UserGroupPicklistFillValue(arrayOfValues. UserGroupId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }



  public UserGroupPicklistFillValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormVariable => {
          this.form.patchValue(FormVariable);

        })
    }
  }


  public UserGroupPicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IUserGroup> {
  const dbds: GBBaseDBDataService<IUserGroup> = new GBBaseDBDataService<IUserGroup>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IUserGroup>, dbDataService: GBBaseDBDataService<IUserGroup>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IUserGroup> {
  return new GBDataPageService<IUserGroup>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
