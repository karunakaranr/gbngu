import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import * as RoleAlertJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/RoleAlert.json'
import { IRoleAlert } from 'features/erpmodules/admin/models/IRoleAlert';
import { RoleAlertService } from 'features/erpmodules/admin/services/Roles&Rights/RoleAlert.service';
@Component({


  selector: 'app-RoleAlert',
  templateUrl: './rolealert.component.html',
  styleUrls: ['./rolealert.component.scss'],

  providers: [
    { provide: 'IdField', useValue: 'RoleAlertId'},
    { provide: 'url', useValue: ADMINURLS.RoleAlert },
    { provide: 'saveurl', useValue: ADMINURLS.RoleAlertSave },
    { provide: 'DataService', useClass: RoleAlertService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class RoleAlertComponent extends GBBaseDataPageComponentWN<IRoleAlert[]> {
  title : string = "RoleAlert"
  RoleAlertJSON =  RoleAlertJSON

form: GBDataFormGroupWN<IRoleAlert[]> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "RoleAlert", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
      this.RoleAlertPicklistFillValue(arrayOfValues. RoleAlertId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }


  public RoleAlertPicklistFillValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.postData(SelectedPicklistData).subscribe(RoleAlert => {
          this.form.get('RoleAlertArray').patchValue(RoleAlert)
        })
    }
  }


  public RoleAlertPicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string,saveurl:string): GBBaseDBDataService<IRoleAlert[]> {
  const dbds: GBBaseDBDataService<IRoleAlert[]> = new GBBaseDBDataService<IRoleAlert[]>(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IRoleAlert[]>, dbDataService: GBBaseDBDataService<IRoleAlert[]>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IRoleAlert[]> {
  return new GBDataPageService<IRoleAlert[]>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
