import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ICostCenterType } from '../../models/ICostCenterType';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { CostCenterTypeService } from '../../services/CostCenter/costcentertype.service';
import { ADMINURLS} from 'features/erpmodules/admin/URLS/urls'
import * as CostCenterTypeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CostCenterType.json'

@Component({


  selector: 'app-CostCenter',
  templateUrl: './costcentertype.component.html',
  styleUrls: ['./costcentertype.component.scss'],

  providers: [
    { provide: 'IdField', useValue: 'CostCenterTypeId'},
    { provide: 'url', useValue: ADMINURLS.CostCenterType },
    { provide: 'DataService', useClass: CostCenterTypeService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class CostCenterTypeComponent extends GBBaseDataPageComponentWN<ICostCenterType> {
  title : string = "CostCenterType"
  CostCenterTypeJSON = CostCenterTypeJSON

form: GBDataFormGroupWN<ICostCenterType> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "CostCenterType", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
      this.CostCenterTypePicklistFillValue(arrayOfValues. CostCenterTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }


  public CostCenterTypePicklistFillValue(SelectedPicklistData: string): void {
    console.log("SelectedPicklistData",SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormVariable => {
          this.form.patchValue(FormVariable);
        })
    }
  }


  public CostCenterTypePicklistPatchValue(SelectedPicklistDatas: any): void {
    console.log("SelectedPicklistDatasss",SelectedPicklistDatas)
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("Form Value:",this.form.value)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICostCenterType> {
  const dbds: GBBaseDBDataService<ICostCenterType> = new GBBaseDBDataService<ICostCenterType>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ICostCenterType>, dbDataService: GBBaseDBDataService<ICostCenterType>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICostCenterType> {
  return new GBDataPageService<ICostCenterType>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
