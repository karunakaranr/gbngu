import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IPartyTaxType } from '../../models/IPartyTaxType';

// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
import { PartyTaxTypeService } from '../../services/PartyTaxType/PartyTaxType.service';
import * as PartyTaxTypeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PartyTaxType.json'
import { ADMINURLS } from '../../URLS/urls';


@Component({
  selector: 'app-PartyTaxType',
  templateUrl: './PartyTaxType.component.html',
  styleUrls: ['./PartyTaxType.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'PartyTaxTypeId'},
    { provide: 'url', useValue: ADMINURLS.PartyTaxType },
    { provide: 'DataService', useClass: PartyTaxTypeService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PartyTaxTypeComponent extends GBBaseDataPageComponentWN<IPartyTaxType > {
  title: string = 'PartyTaxType'
  PartyTaxTypeJSON = PartyTaxTypeJSON;


  form: GBDataFormGroupWN<IPartyTaxType > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'PartyTaxType', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PartyTaxTypeFillFunction(arrayOfValues.PartyTaxTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public PartyTaxTypeFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PartyTaxTypePatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPartyTaxType > {
  const dbds: GBBaseDBDataService<IPartyTaxType > = new GBBaseDBDataService<IPartyTaxType >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPartyTaxType >, dbDataService: GBBaseDBDataService<IPartyTaxType >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPartyTaxType > {
  return new GBDataPageService<IPartyTaxType >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
