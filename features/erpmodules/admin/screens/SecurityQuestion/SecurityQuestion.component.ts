import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
// import { ISecurityQuestion } from '../../models/ISecurityQuestion';
// import { SecurityQuestionService } from '../../services/SecurityQuestion/SecurityQuestion.service';
import { ISecurityQuestion } from '../../models/ISecurityQuestion';
import { SecurityQuestionService } from '../../services/SecurityQuestion/SecurityQuestion.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

import * as SecurityQuestionjson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/SecurityQuestion.json'

import { ADMINURLS } from '../../URLS/urls';

@Component({
  selector: "app-SecurityQuestion",
  templateUrl: "./SecurityQuestion.component.html",
  styleUrls: ["./SecurityQuestion.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'SecurityQuestionId' },
    { provide: 'url', useValue: ADMINURLS.SecurityQuestion },
    { provide: 'DataService', useClass: SecurityQuestionService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class SecurityQuestionComponent extends GBBaseDataPageComponentWN<ISecurityQuestion> {
  title: string = "SecurityQuestion"
  SecurityQuestionjson = SecurityQuestionjson;



  form: GBDataFormGroupWN<ISecurityQuestion> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'SecurityQuestion', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.SecurityQuestionFillFunction(arrayOfValues.SecurityQuestionId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public SecurityQuestionFillFunction(SelectedPicklistData: string): void {

   console.log("filling",SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(SecurityQuestion => {
        this.form.patchValue(SecurityQuestion);
      })
    }
  }


  public SecurityQuestionPatchValue(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ISecurityQuestion> {
  const dbds: GBBaseDBDataService<ISecurityQuestion> = new GBBaseDBDataService<ISecurityQuestion>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ISecurityQuestion>, dbDataService: GBBaseDBDataService<ISecurityQuestion>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ISecurityQuestion> {
  return new GBDataPageService<ISecurityQuestion>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
