import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { ADMINURLS } from 'features/erpmodules/admin/URLS/urls';
import { CheckingService } from 'features/erpmodules/admin/services/Checking/checking.service';
import { IChecking } from 'features/erpmodules/admin/models/IChecking';
import * as CheckingJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Checking.json'

@Component({
    selector: 'app-Checking',
    templateUrl:'./checking.component.html',
    styleUrls: ['./checking.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'CheckingId' },
        { provide: 'url', useValue: ADMINURLS.Checking },
        { provide: 'DataService', useClass: CheckingService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class CheckingComponent extends GBBaseDataPageComponentWN<IChecking> {
    title: string = "Checking"
    CheckingJSON = CheckingJSON

    form: GBDataFormGroupWN<IChecking> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Checking", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.CheckingPicklistFillValue(arrayOfValues.CheckingId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }


  
    public CheckingPicklistFillValue(SelectedPicklistData: string): void {
        // console.log("id",SelectedPicklistData)
        if (SelectedPicklistData) {
            // console.log("id",SelectedPicklistData)
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(Checking => {
                // console.log("id",SelectedPicklistData)
                    this.form.patchValue(Checking);
                              
            })
        }
    }



    public CheckingPicklistPatchValue(SelectedPicklistDatas: any): void {
        // console.log("id",SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IChecking> {
    const dbds: GBBaseDBDataService<IChecking> = new GBBaseDBDataService<IChecking>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<IChecking>, dbDataService: GBBaseDBDataService<IChecking>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IChecking> {
    return new GBDataPageService<IChecking>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
