import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { DivisionService } from '../../services/Division/Division.service';
import {ADMINURLS} from '../../URLS/urls'
import { IDivision } from '../../models/IDivision';
import * as DivisionJSONName from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Division.json'
import { ButtonVisibility } from 'features/layout/store/layout.actions';


@Component({
  selector: 'app-Division',
  templateUrl: './Division.component.html',
  styleUrls: ['./Division.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'DivisionId'},
    { provide: 'url', useValue: ADMINURLS.Division },
    { provide: 'DataService', useClass: DivisionService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class DivisionComponent extends GBBaseDataPageComponentWN<IDivision > {
  title: string = 'Division'
  DivisionJSONName = DivisionJSONName;


  form: GBDataFormGroupWN<IDivision > = new GBDataFormGroupWN(this.gbps.gbhttp.http,'Division', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.DivisionId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IDivision > {
  const dbds: GBBaseDBDataService<IDivision > = new GBBaseDBDataService<IDivision >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IDivision >, dbDataService: GBBaseDBDataService<IDivision >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IDivision > {
  return new GBDataPageService<IDivision >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


