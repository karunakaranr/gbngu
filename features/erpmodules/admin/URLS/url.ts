export class ADMINURLS {

    public static DBJoin = '/ans/DBJoin.svc/';
    public static Country = '/fws/Country.svc/';
    public static TransactionType = '/mms/TransactionType.svc/';
    public static Department = '/ads/Department.svc/';
    public static Company = '/ads/Company.svc/';
    public static Currency = '/ads/Currency.svc/';
    public static Branch = '/ads/Branch.svc/';
    public static Process = '/mms/Process.svc/';
    public static Biztransaction = '/ads/BIZTransaction.svc/';
    public static BiztransactionSubclass = '/fws/BIZTransactionSubClass.svc/';
    public static FinanceBook = '/ads/FinanceBook.svc/';
    public static State = '/fws/State.svc/';
    public static Period = '/ads/Period.svc/';
    public static OrgUnit = '/ads/OrganizationUnit.svc/';
    public static Account = '/gb4/as/Account.svc/';
    public static Port = '/fws/Port.svc/';
    public static PortCodeName = '/fws/Port.svc/';
    public static PortCountry = '/fws/Country.svc/';
    public static CostCenterType = '/as/CostCenterType.svc/';
    public static AllocationType = '/ads/AllocationType.svc/';
    public static JobDefine = '/fws/JobDefine.svc/';
    public static Scheduler = '/fws/Scheduler.svc/';
    public static WebService = '/fws/WebService.svc/';
    public static CerteriaConfig = '/fws/CriteriaConfig.svc/';
    public static Menu = '/fws/Menu.svc';
    public static Module = '/fws/Module.svc';
    public static Page = '/fws/Page.svc/';
    public static BankBranch = '/as/BankBranch.svc/';
    public static Banks = '/fws/GCM.svc/';
    public static CostAnalysis = '/cts/CostAnalysis.svc/';
    public static OrganizationGroup = '/ads/OrganizationGroup.svc/';
    public static RoleModule = '/fws/RoleVsModules.svc/GetRoleVsModuleWithUserId/';
    public static UserGroup = '/fws/UserGroup.svc/';
    public static RoleModuleSave = "/fws/RoleVsModules.svc/";
    public static UserAccessRights = '/cs/Criteria.svc/List/';
    public static PaymentTerm = '/mms/PaymentTerm.svc/';
    public static CopyTypeName = '/fws/CopyTypeName.svc/';
    public static Role = '/fws/Role.svc/';
    public static Workflow = '/ads/Workflow.svc/';
    public static Deployment = '/fws/Deployment.svc/';
    public static Entity = '/fws/Entity.svc/';
    public static WorkflowRule = '/ads/WorkflowRule.svc/';
    public static EntityMember = '/fws/EntityMember.svc/';
    public static EventTypeAction = '/cs/Criteria.svc/List/?ObjectCode=EVENTTYPEACTION';
    public static EventTypeSave = ' /fws/EventTypeAction.svc/';
    public static RegionType = '/fws/RegionType.svc/';
    public static City = '/fws/City.svc/';
    public static FormulaField = '/fws/FormulaField.svc/';
    public static File = '/fws/File.svc/';
    public static Allocationserve = '/ads/Allocation.svc/';
    public static Allocation = '/ads/AllocationType.svc/';
    public static Region = '/fws/Region.svc/';


}
