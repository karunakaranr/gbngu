import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DriverComponent } from './screen/Driver/Driver.Component';




const routes: Routes = [
  {
    path: 'themestest',
    loadChildren: () =>
      import(`../../themes/themes.module`).then((m) => m.ThemesModule),
  },
  {
    path: 'Driver',
    component: DriverComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class logisticsroutingModule {}
