import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { IRPartywise } from 'features/erpmodules/purchase/models/IRPartywise';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { DateFormaterPipe } from 'libs/gbcommon/src/lib/Date-Formater.pipe';

@Component({
  selector: 'app-registerpartywise',
  templateUrl: './registerpartywise.component.html',
  styleUrls: ['./registerpartywise.component.scss'],
})
export class RegisterPartyWiseComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  private dateFormatter: DateFormaterPipe = new DateFormaterPipe(this.store);

  public rowData: IRPartywise[];
  public tabledata;
  
  public qtys:number;
  public value: number;
  public valuefc:number;
  public add: number;
  public less:number;
  public total: number;
  public totalfc:number;
  
  

  constructor(public store: Store, public SharedService: SharedService) { }
  ngOnInit(): void {
    this.getpartywisefun();
  }
  
  public getpartywisefun() {
    this.SharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Party Wise :", this.rowData)
        
      this.qtys=0;
      this.value=0;
      this.valuefc=0;
      this.add=0;
      this.less=0;
      this.total=0;
      this.totalfc=0;
    

      let json = this.rowData;
      var finalizedArray = [];

      for (let data of this.rowData) {
		this.qtys = this.qtys + data.TransactionActualQuantity;
        this.value = this.value + data.ItemBasicValue;
        this.valuefc = this.valuefc + data.ItemBasicValueFC;
        this.add = this.add + data.ItemPlus;
        this.less = this.less + data.ItemMinus;
        this.total = this.total + data.ItemNetValue;
        this.totalfc = this.totalfc + data.ItemNetValueFC;
      }



      json.map((row,index) => {
          finalizedArray.push({
              sno: index,
			  acccode:row['AccountCode'],
              accname: row['AccountName'],
              prefno: row['PartyReferenceNumber'],
              prefdate: this.dateFormatter.transform(row['PartyReferenceDate'], 'date'),
              
              docno:row['DocumentNumber'],
              docdate: this.dateFormatter.transform(row['DocumentDate'], 'date'),
              refno: row['ReferenceNumber'],
              refdate: row['ReferenceDate'],
              depcode: row['DepartmentCode'],
              icode: row['ItemCode'],
              bnum: row['BinNumber'],
              descr: row['ProductDescription'],
              actqty: row['TransactionActualQuantity'],
              uom: row['TransactionRateUOM'],
              curcode:row['CurrencyCode'],
              rate: row['Rate'],
              ratefc: row['RateFc'],
              ibasicval: row['ItemBasicValue'],
              ibasicvalfc: row['ItemBasicValueFC'],
              iplus: row['ItemPlus'],
              iminus: row['ItemMinus'],
              netval: row['ItemNetValue'],
              netvalfc: row['ItemNetValueFC'],
          });
      });


      const final = {};
      finalizedArray.forEach((detail) => {
          final[detail.acccode] = {
              accountGroups: {},
              
              docno:detail.acccode,
              docdate: detail.accname,
              refno: "",
              refdate: detail.prefno,
              depcode: detail.prefdate,
              icode: "",
              bnum:"",
              descr: "",
              actqty: "",
              uom: "",
              curcode:"",
              rate: "",
              ratefc: "",
              ibasicval: "",
              ibasicvalfc: "",
              iplus: "",
              iminus: "",
              netval: "",
              netvalfc: "",
              ...final[detail.acccode],
          };

          final[detail.acccode].accountGroups[detail.sno] = {
            docno:detail.acccode,
              docdate: detail.docdate,
              refno: detail.refno,
              refdate: detail.refdate,
              depcode: detail.depcode,
              icode: detail.icode,
              bnum:detail.bnum,
              descr:detail. descr,
              actqty:detail.actqty ,
              uom:detail.uom ,
              curcode:detail.curcode,
              rate: detail.rate,
              ratefc: detail.ratefc,
              ibasicval:detail.ibasicval ,
              ibasicvalfc: detail.ibasicvalfc,
              iplus: detail.iplus,
              iminus: detail.iminus,
              netval:detail.netval,
              netvalfc:detail.netvalfc ,
          };
      });

      const productionsummary = Object.keys(final);

      const tableData = [];
      productionsummary.forEach((prodsum) => {
          tableData.push({
        
              docno:final[prodsum].docno,
              docdate: final[prodsum].docdate,
              refno: "",
              refdate: final[prodsum].refdate,
              depcode: final[prodsum].depcode,
              icode: "",
              bnum:"",
              descr: "",
              actqty: "",
              uom: "",
              curcode:"",
              rate: "",
              ratefc: "",
              ibasicval: "",
              ibasicvalfc: "",
              iplus: "",
              iminus: "",
              netval: "",
              netvalfc: "",
              bold: true,
          });

          const summary = Object.keys(final[prodsum].accountGroups);
          summary.forEach((psummary) => {
              tableData.push({
                docno: final[prodsum].accountGroups[psummary].docno,
                docdate: final[prodsum].accountGroups[psummary].docdate,
                refno: final[prodsum].accountGroups[psummary].refno,
                refdate: final[prodsum].accountGroups[psummary].refdate,
                depcode:final[prodsum].accountGroups[psummary].depcode,
                icode: final[prodsum].accountGroups[psummary].icode,
                bnum: final[prodsum].accountGroups[psummary].bnum,
                descr: final[prodsum].accountGroups[psummary].descr,
                actqty: final[prodsum].accountGroups[psummary].actqty,
                uom: final[prodsum].accountGroups[psummary].uom,
                curcode: final[prodsum].accountGroups[psummary].curcode,
				        rate: final[prodsum].accountGroups[psummary].rate,
                ratefc: final[prodsum].accountGroups[psummary].ratefc,
                ibasicval: final[prodsum].accountGroups[psummary].ibasicval,
                ibasicvalfc: final[prodsum].accountGroups[psummary].ibasicvalfc,
                iplus: final[prodsum].accountGroups[psummary].iplus,
                iminus: final[prodsum].accountGroups[psummary].iminus,
                netval: final[prodsum].accountGroups[psummary].netval,
                netvalfc: final[prodsum].accountGroups[psummary].netvalfc,
               

              });
          });
      });
      this.tabledata = tableData;

      console.log("Final Data:", this.rowData)
    }
    });
  }

}