
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { IRDetailtwo } from 'features/erpmodules/purchase/models/IRDetailtwo';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { DateFormaterPipe } from 'libs/gbcommon/src/lib/Date-Formater.pipe';

@Component({
  selector: 'app-registerdetailtwo',
  templateUrl: './registerdetailtwo.component.html',
  styleUrls: ['./registerdetailtwo.component.scss'],
})
export class RegisterDetailTwoComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  private dateFormatter: DateFormaterPipe = new DateFormaterPipe(this.store);

  public rowData: IRDetailtwo[];
  public tabledata;
 
  public value:number;
  public add: number;
  public less:number;
  public total: number;

  constructor(public store: Store, public SharedService: SharedService ) { }
  ngOnInit(): void {
    this.getregisterdetailtwofun();
  }

  public getregisterdetailtwofun() {
    this.SharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {      
        this.rowData = data;
      console.log("Register Detail :", this.rowData)
 
	  this.value=0;
      this.add = 0;
	  this.less=0;
      this.total = 0;

      let json = this.rowData;
      var finalizedArray = [];

      for (let data of this.rowData) {
		this.value = this.value + data.ItemBasicValue;
        this.add = this.add + data.ItemPlus;
        this.less = this.less + data.ItemMinus;
        this.total = this.total + data.ItemNetValue;
      }

      json.map((row, index) => {
        finalizedArray.push({
          sno: index,
          docdate:  this.dateFormatter.transform(row['DocumentDate'], 'date'),//1grouping
          docno: row['DocumentNumber'],
		  accname:row['AccountName'],
		  storename:row['StoreName'],
		  currencycode:row['CurrencyCode'],
		  amtplusfc:row['TotalOtherAmountPlusFC'],
		  amtminusfc:row['TotalOtherAmountMinusFC'],
		  netvalue:row['TotalNetValue'],
		  
          itemcode: row['ItemCode'],
          itemname: row['ItemName'],
          transqty: row['TransactionQuantity'],
          scheddate: row['ScheduleDate'],
          schedqty: row['ScheduleQuantity'],
          transuom: row['TransactionUOM'],
          rate: row['Rate'],
          ibasicval: row['ItemBasicValue'],
          iplus: row['ItemPlus'],
          iminus: row['ItemMinus'],
          totals:row['ItemNetValue'],
        });
      });

      console.log("finalizedArray:", finalizedArray)
      const final = {};
      finalizedArray.forEach((detail) => {
        final[detail.docdate] = {
          accountGroups: {},
          itemcode:detail.docdate,
          itemname: "",
          transqty: "",
          scheddate: "",
          schedqty: "",
          transuom: "",
          rate: "",
          ibasicval: "0",
          iplus: "0",
          iminus: "0",
          totals:"",

          ...final[detail.docdate],
        };
        final[detail.docdate].accountGroups[detail.docno] = {
          accountGroup2: {},
		  itemcode:detail.docno,
          itemname: detail.accname,
          transqty: detail.storename,
          scheddate:detail.currencycode,
          schedqty: "",
          transuom: detail.amtplusfc,
          rate: detail.amtminusfc,
          ibasicval: detail.netvalue,
          iplus: "",
          iminus: "",
          totals:"",
		  
          ...final[detail.docdate].accountGroups[detail.docno],
        }

        final[detail.docdate].accountGroups[detail.docno].accountGroup2[detail.sno] = {

          itemcode:detail.itemcode,
          itemname: detail.itemname,
          transqty: detail.transqty,
          scheddate:detail.scheddate,
          schedqty: detail.schedqty,
          transuom: detail.transuom,
          rate: detail.rate,
          ibasicval: detail.ibasicval,
          iplus: detail.iplus,
          iminus: detail.iminus,
          totals:detail.totals,
        };
        //console.log("currency code:", final[detail.docdate].accountGroups[detail.docno].actualqty)
      });


      const registerdetails = Object.keys(final);

      const tableData = [];
      registerdetails.forEach((registerdetail) => {

        const accountGroup = Object.keys(final[registerdetail].accountGroups);
		
        let val = 0;
		

        accountGroup.forEach((rgdetail) => {

		  let valr = 0;
		  


          const accountGroup22 = Object.keys(final[registerdetail].accountGroups[rgdetail].accountGroup2)
          accountGroup22.forEach((sdetail) => {
            valr = valr + parseFloat(final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].ibasicval);
            

          })
          final[registerdetail].accountGroups[rgdetail].valr = valr;
          

          val += valr;
          

          final[registerdetail].ibasicval = val;
          

          tableData.push({
			  itemcode:final[registerdetail].itemcode,
			  itemname: "",
			  transqty: "",
			  scheddate: "",
			  schedqty: "",
			  transuom: "",
			  rate: "",
			  ibasicval: "",
			  iplus: "",
			  iminus: "",
              totals:"",
			  bold: true,
          });

          tableData.push({
          
            itemcode: final[registerdetail].accountGroups[rgdetail].itemcode,
            itemname: final[registerdetail].accountGroups[rgdetail].itemname,
			transqty: final[registerdetail].accountGroups[rgdetail].transqty,
            scheddate: final[registerdetail].accountGroups[rgdetail].scheddate,
			schedqty: final[registerdetail].accountGroups[rgdetail].schedqty,
            transuom: final[registerdetail].accountGroups[rgdetail].transuom,
            rate: final[registerdetail].accountGroups[rgdetail].rate,
            ibasicval: final[registerdetail].accountGroups[rgdetail].ibasicval,
			iplus: final[registerdetail].accountGroups[rgdetail].iplus,
            iminus: final[registerdetail].accountGroups[rgdetail].iminus,
            totals: final[registerdetail].accountGroups[rgdetail].totals,
            bold: true,
          });




          //   const accountGroups = Object.keys(final[registerdetail].accountGroups);
          accountGroup22.forEach(sdetail => {


            tableData.push({
              
              itemcode: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].itemcode,
              itemname: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].itemname,
			  transqty: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].transqty,
              scheddate: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].scheddate,
              schedqty: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].schedqty,
              transuom: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].transuom,
              rate: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].rate,
			   ibasicval: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].ibasicval,
              iplus: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].iplus,
              iminus: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].iminus,
              totals: final[registerdetail].accountGroups[rgdetail].accountGroup2[sdetail].totals,
            })


          })
        })
      });

      this.tabledata = tableData;

      console.log("Final Data:", this.tabledata)
    }
    });
  }
}