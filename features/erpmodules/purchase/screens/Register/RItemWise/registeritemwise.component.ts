import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { IRItemwise } from 'features/erpmodules/purchase/models/IRItemwise';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
  selector: 'app-registeritemwise',
  templateUrl: './registeritemwise.component.html',
  styleUrls: ['./registeritemwise.component.scss'],
})
export class RegisterItemWiseComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData: IRItemwise[];
  public tabledata;

  public qtys: number;
  public value: number;
  public add: number;
  public less: number;
  public total: number;




  constructor(public store: Store , public SharedService: SharedService) { }
  ngOnInit(): void {
    this.getitemwisefun();
  }

  public getitemwisefun() {
    this.SharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {      
        this.rowData = data;
      console.log("Item Wise :", this.rowData)

      this.qtys = 0;
      this.value = 0;
      this.add = 0;
      this.less = 0;
      this.total = 0;



      let json = this.rowData;
      var finalizedArray = [];

      for (let data of this.rowData) {
        this.qtys = this.qtys + data.TransactionActualQuantity;
        this.value = this.value + data.ItemBasicValue;
        this.add = this.add + data.ItemPlus;
        this.less = this.less + data.ItemMinus;
        this.total = this.total + data.ItemNetValue;

      }



      json.map((row, index) => {
        finalizedArray.push({
          sno: index,
          icode: row['ItemCode'],
          iname: row['ItemName'],
          prefno: row['PartyReferenceNumber'],
          prefdate: row['PartyReferenceDate'],
          curcode: row['CurrencyCode'],
          totalqty: row['TotalQuantity'],
          totalbasval: row['TotalItemBasicValue'],
          totalplus: row['TotalItemAmountPlus'],
          totalmin: row['TotalItemAmountMinus'],
          totalnetval: row['TotalNetValue'],





          docno: row['DocumentNumber'],
          docdate: row['DocumentDate'],
          refno: row['ReferenceNumber'],
          refdate: row['ReferenceDate'],
          accname: row['AccountName'],
          depcode: row['DepartmentCode'],
          actqty: row['TransactionActualQuantity'],
          uom: row['TransactionRateUOM'],
          rate: row['Rate'],
          ibasicval: row['ItemBasicValue'],
          iplus: row['ItemPlus'],
          iminus: row['ItemMinus'],
          netval: row['ItemNetValue'],

        });
      });


      const final = {};
      finalizedArray.forEach((detail) => {
        final[detail.icode] = {
          accountGroups: {},

          docno: detail.icode,
          docdate: detail.iname,
          refno: detail.prefno,
          refdate: detail.prefdate,
          accname: "",
          depcode: detail.curcode,
          actqty: "",
          uom: detail.totalqty,
          rate: "",
          ibasicval: detail.totalbasval,
          iplus: detail.totalplus,
          iminus: detail.totalmin,
          netval: detail.totalnetval,

          ...final[detail.icode],
        };

        final[detail.icode].accountGroups[detail.sno] = {
          docno: detail.icode,
          docdate: detail.docdate,
          refno: detail.refno,
          refdate: detail.refdate,
          accname: detail.accname,
          depcode: detail.depcode,
          actqty: detail.actqty,
          uom: detail.uom,
          curcode: detail.curcode,
          rate: detail.rate,
          ibasicval: detail.ibasicval,
          iplus: detail.iplus,
          iminus: detail.iminus,
          netval: detail.netval,

        };
      });

      const productionsummary = Object.keys(final);

      const tableData = [];
      productionsummary.forEach((prodsum) => {
        tableData.push({

          docno: final[prodsum].docno,
          docdate: final[prodsum].docdate,
          refno: final[prodsum].refno,
          refdate: final[prodsum].refdate,
          accname: "",
          depcode: final[prodsum].depcode,
          actqty: "",
          uom: final[prodsum].uom,
          rate: "",
          ibasicval: final[prodsum].ibasicval,
          iplus: final[prodsum].iplus,
          iminus: final[prodsum].iminus,
          netval: final[prodsum].netval,

          bold: true,
        });

        const summary = Object.keys(final[prodsum].accountGroups);
        summary.forEach((psummary) => {
          tableData.push({
            docno: final[prodsum].accountGroups[psummary].docno,
            docdate: final[prodsum].accountGroups[psummary].docdate,
            refno: final[prodsum].accountGroups[psummary].refno,
            refdate: final[prodsum].accountGroups[psummary].refdate,

            accname: final[prodsum].accountGroups[psummary].accname,
            depcode: final[prodsum].accountGroups[psummary].depcode,
            actqty: final[prodsum].accountGroups[psummary].actqty,
            uom: final[prodsum].accountGroups[psummary].uom,

            rate: final[prodsum].accountGroups[psummary].rate,

            ibasicval: final[prodsum].accountGroups[psummary].ibasicval,

            iplus: final[prodsum].accountGroups[psummary].iplus,
            iminus: final[prodsum].accountGroups[psummary].iminus,
            netval: final[prodsum].accountGroups[psummary].netval,



          });
        });
      });
      this.tabledata = tableData;

      console.log("Final Data:", this.rowData)
    }
    });
  }

}