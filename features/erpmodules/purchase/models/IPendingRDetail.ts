export interface IPendingRDetail {
    AdvanceAmount: number
    AmendmentSlNo: number
    CreatedByName: string
    ModifiedByName: string
    BillValue: number
    BillValueDetail: number
    ItemGroupCode: string
    ItemGroupName: string
    FinalItemNetValueFC: number
    FinalItemRealisationValueFC: number
    FinalItemCostValueFC: number
    FinalItemTareWeight: number
    FinalItemNetWeight: number
    FinalItemCostValue: number
    FinalItemBasicValueFC: number
    FinalPendingOtherValue: number
    FinalPendingRejectedValue: number
    FinalPendingReworkValue: number
    FinalPendingGoodValue: number
    FinalPendingValue: number
    FinalItemNetCost: number
    FinalItemPostedCost: number
    FinalItemNetValue: number
    FinalPendingQty: number
    FinalPostedCost: number
    FinalItemGrossValue: number
    FinalItemGrossValueFC: number
    FinalInputValue: number
    FinalOutPutValue: number
    InputValue: number
    OutPutValue: number
    ProductDescription: string
    ItemDescription: string
    NoOfDaysPending: number
    DocumentId: number
    BIZTransactionTypeId: number
    BIZTransactionTypeCode: string
    BIZTransactionTypeName: string
    BIZTransactionClassId: number
    BIZTransactionClassCode: string
    BIZTransactionClassName: string
    OUId: number
    OUCode: string
    OUName: string
    DocumentDate: string
    DocumentNumber: string
    ReferenceNumber: string
    ReferenceDate: string
    PartyReferenceNumber: string
    PartyReferenceDate: string
    PeriodId: number
    PartyId: number
    PartyCode: string
    PartyName: string
    PartyShortName: string
    PartyBranchId: number
    TaxTransactionTypeId: number
    TaxtransactionTypeCode: string
    TaxtransactionTypeName: string
    TotalNetValue: number
    AllocationId: number
    AllocationName: string
    AllocationDescription: string
    AllocationShortName: string
    DepartmentId: number
    DepartmentCode: string
    DepartmentName: string
    InchargeId: number
    InchargeCode: string
    InchargeName: string
    CurrencyId: number
    CurrencyCode: string
    CurrencyName: string
    CurrencySymbol: string
    CurrencyFull: string
    CurrencyDecimal: string
    DocumentDetailId: number
    SlNo: number
    ItemId: number
    ItemCode: string
    ItemName: string
    ItemCategoryId: number
    ItemCategoryCode: string
    ItemCategoryName: string
    ActualQuantity: number
    StockPostType: string
    TransactionActualQuantity: number
    UOMCode: string
    Rate: number
    ItemNetCost: number
    ItemPostedCost: number
    ItemNetValue: number
    RequiredDeliveryDate: string
    PlanDeliveryDate: string
    Remarks: string
    PendingQuantity: number
    PendingValue: number
    PendingGoodQuantity: number
    PendingGoodValue: number
    PendingReworkQuantity: number
    PendingReworkValue: number
    PendingRejectedQuantity: number
    PendingRejectedValue: number
    PendingOtherQuantity: number
    PendingOtherValue: number
    ModeOfDelivery: number
    ModeOfDeliveryName: string
    RateFC: number
    DeliveryTerms: string
    DeliveryInstructions: string
    PaymentTermsId: number
    PaymentTermsCode: string
    PaymentTermsName: string
    PaymentTermsRemarks: string
    SpecialInstruction: string
    PreDocumentId: number
    PreDocumentDate: string
    PreDocumentNumber: number
    PrePreDocumentId: number
    PrePreDocumentDate: string
    PrePreDocumentNumber: number
    TotalItemGrossValue: number
    TotalItemGrossValueFC: number
    ToCountryId: number
    ToCountryCode: string
    ToCountryName: string
    DocumentRealisationValueFC: number
    ItemGrossValue: number
    ItemGrossValueFC: number
    ItemBasicValueFC: number
    ItemAmountPlusFC: number
    ItemAmountMinusFC: number
    ItemOtherAmountPlusFC: number
    ItemOtherAmountMinusFC: number
    ItemCostValue: number
    ItemNetWeight: number
    ItemTareWeight: number
    ItemCostValueFC: number
    ItemRealisationValueFC: number
    ItemNetValueFC: number
    FromCountryId: number
    FromCountryCode: string
    FromCountryName: string
    FromPortId: number
    FromPortName: string
    ToPortId: number
    ToPortName: string
    NoOfDays: number
    GoodQuantity: number
    RejectedQuantity: number
    ReworkQuantity: number
    OtherQuantity: number
    CurrentMonth: number
    DocumentMonth: number
    SKUId: number
    SKUName: string
    SKUCode: string
    ExpectedDateOfDelivery: string
    ExpectedMonthOfDelivery: number
    Label: string
    GCCurrencyConversion: number
    TotalItemBasicValueGC: number
    TotalItemAmountPlusGC: number
    TotalItemAmountMinusGC: number
    TotalOtherAmountplusGC: number
    TotalOtherAmountMinusGC: number
    TotalNetValueGC: number
    RoundOffGC: number
    DocumentValueGC: number
    TotalItemGrossValueGC: number
    DocumentRealisationValueGC: number
    DocumentCostValueGC: number
    BillValueGC: number
    DetailGCCurrencyConversion: number
    RateGC: number
    ItemBasicValueGC: number
    ItemAmountPlusGC: number
    ItemAmountMinusGC: number
    ItemOtherAmountPlusGC: number
    ItemOtherAmountMinusGC: number
    ItemGrossValueGC: number
    ItemCostValueGC: number
    ItemRealisationValueGC: number
    ItemNetValueGC: number
    BillValueDetailGC: number
    ClosedQuantity: number
    ClosingValue: number
    ClosingValueGC: number
    PendingValueGC: number
    QualityRemarks: string
    ExciseRegisterLastNo: number
    ExciseRegisterPageNo: number
    ReasonName: string
    PendingValueFC: number
    StoreId: number
    StoreCode: string
    StoreName: string
    ModifiedOn: string
    MFGDate: string
    ExpiryDate: string
    D1: number
    D2: number
    D3: number
    D4: number
    D5: number
    FreeQuantity: number
    SizeValue: string
    ContactName: string
    ContactFirstName: string
    ContactMailId: string
    ContactDOB: string
    ContactAddress: string
    ContactAddressLine1: string
    ContactAddressLine2: string
    ContactAddressLine3: string
    ContactAddressLine4: string
    ContactAddressLine5: string
    ContactAddressPhone: number
    ContactAddressMobile: number
    ContactAddressMail: string
    OpenPhone: string
    OpenName: string
    OpenMail: string
    Destination: string
    PackCode: string
    PackName: string
    StockGoodQuantity: number
    AgentId: number
    AgentCode: string
    AgentName: string
    StateCode: string
    StateName: string
    AreaCode: string
    AreaName: string
    ItemSubCategoryCode: string
    ItemSubCategoryName: string
    CostCenterId: number
    CostCenterCode: string
    CostCenterName: string
    ItemRealisationValue: number
    DeliveryStatus: string
    DetailReasonId: number
    DetailReasonCode: string
    DetailReasonName: string
    DocumentDetailStatus: string
    ItemStatus: string
    DetailRemarks: string
    StatusofDelivery: number
    HeadDocumentStatus: string
    BasicAfterDisc: number
    NumberOfRecords: number
  }
  