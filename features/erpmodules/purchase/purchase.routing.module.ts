import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PurchaseComponent } from './purchase.component';
import { RegisterDetailoneComponent } from './screens/Register/RDetailone/registerdetailone.component';
import { RegisterDetailTwoComponent } from './screens/Register/RDetailtwo/registerdetailtwo.component';
import { RegisterPartyWiseComponent } from './screens/Register/RPartyWise/registerpartywise.component';
import { RegisterItemWiseComponent } from './screens/Register/RItemWise/registeritemwise.component';
import { PendingRegisterDetailComponent } from './screens/PendingRegister/PRDetail/pendingregisterdetail.component';
import { PendingRegisterWOItemComponent } from './screens/PendingRegister/PRWOItem/pendingregisterwoitem.component';
import { PendingRegisterPartyWiseComponent } from './screens/PendingRegister/PRPartyWise/pendingrPartyWise.component';
import { RatingComponent } from './screens/Rating/rating.component';


const routes: Routes = [
  {
    path: '',
    component: PurchaseComponent
  },
  {
    path: 'rating',
    component: RatingComponent
  },
  {
    path: 'pendingrpartywise',
    component: PendingRegisterPartyWiseComponent
  },
  {
    path: 'pendingregisterwoitem',
    component: PendingRegisterWOItemComponent
  },
  {
    path: 'pendingregisterdetail',
    component: PendingRegisterDetailComponent
  },
  {
    path: 'registeritemwise',
    component: RegisterItemWiseComponent
  },
  {
    path: 'registerpartywise',
    component: RegisterPartyWiseComponent
  },
  {
    path: 'registerdetailtwo',
    component: RegisterDetailTwoComponent
  },
  {
    path: 'registerdetailone',
    component: RegisterDetailoneComponent
  },
  
  { path: 'themestest', loadChildren: () => import(`./../../themes/themes.module`).then(m => m.ThemesModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseRoutingModule { }
