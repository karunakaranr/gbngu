import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CostHeadComponent } from './screens/CostHead/costhead.component';
import { CostDistributionComponent } from './screens/CostDistribution/costdistribution.component';
import { CostHeadSetComponent } from './screens/CostHeadSet/costheadset.component';
import { ResourceComponent } from './screens/Resource/resource.component';
import { BudgetComponent } from './screens/Budget/Budget.component';
import { SettingComponent } from './screens/Setting/Setting.component';
const routes: Routes = [
  {
    path: 'themestest',
    loadChildren: () =>
      import(`../../themes/themes.module`).then((m) => m.ThemesModule),
  },
  {
    path: 'setting',
    component: SettingComponent
  },
  {
    path: 'resource',
    component: ResourceComponent
  },
  {
    path: 'costheadset',
    component: CostHeadSetComponent
  },
  {
    path: 'costhead',
    component: CostHeadComponent
  },
  {
    path: 'costdistribution',
    component: CostDistributionComponent
  },
  {
    path: 'budget',
    component: BudgetComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CostingroutingModule {}
