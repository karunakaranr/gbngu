import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { IOverHeadCost } from '../../models/IOverHeadCost';

 
@Injectable({
  providedIn: 'root'
})
export class OverHeadCostService extends GBBaseDataServiceWN<IOverHeadCost> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IOverHeadCost>) {
    super(dbDataService, 'LotTypeId');
  }
}
