import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { ISetting } from '../../models/ISetting';


 
@Injectable({
  providedIn: 'root'
})
export class SettingService extends GBBaseDataServiceWN<ISetting> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ISetting>) {
    super(dbDataService, 'CostAddOnFieldsId');
  }
}
