import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { ICostHead } from '../models/ICostHead';


 
@Injectable({
  providedIn: 'root'
})

export class CostHeadService extends GBBaseDataServiceWN<ICostHead> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICostHead>) {
    super(dbDataService, 'CostHeadId');
  }
}
