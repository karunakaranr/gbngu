import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { ICostHead } from '../models/ICostHead';
import { ICostHeadSet } from '../models/ICostHeadSet';


 
@Injectable({
  providedIn: 'root'
})

export class CostHeadSetService extends GBBaseDataServiceWN<ICostHeadSet> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICostHeadSet>) {
    super(dbDataService, 'CostHeadSetId');
  }
}
