import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';

import { IOverHeadCost } from '../../models/IOverHeadCost';
import { OverHeadCostService } from '../../services/OverHeadCost/OverHeadCost.service';

import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as OverHeadCostJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/OverHeadCost.json'

import {COSTINGURLS} from '../../URLS/url'

@Component({
  selector: "app-OverHeadCost",
  templateUrl: "./OverHeadCost.component.html",
  styleUrls: ["./OverHeadCost.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'CostCenterId' },
    { provide: 'url', useValue: COSTINGURLS.OverHeadCost },
    { provide: 'DataService', useClass: OverHeadCostService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class OverHeadCostComponent extends GBBaseDataPageComponentWN<IOverHeadCost> {
  title: string = "OverHeadCost"
  OverHeadCostJson = OverHeadCostJson;



  form: GBDataFormGroupWN<IOverHeadCost> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'OverHeadCost', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.OverHeadCostFillFunction(arrayOfValues.CostCenterId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public OverHeadCostFillFunction(SelectedPicklistData: string): void {

   console.log("filling",SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(OverHeadCost => {
        this.form.patchValue(OverHeadCost);
      })
    }
  }


  public OverHeadCostPatchValue(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IOverHeadCost> {
  const dbds: GBBaseDBDataService<IOverHeadCost> = new GBBaseDBDataService<IOverHeadCost>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IOverHeadCost>, dbDataService: GBBaseDBDataService<IOverHeadCost>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IOverHeadCost> {
  return new GBDataPageService<IOverHeadCost>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
