import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { COSTINGURLS } from 'features/erpmodules/costing/URLS/url';
import { IResource } from '../../models/IResource';
import { ResourceService } from '../../services/Resource.service';
import * as ResourceJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Resource.json'
@Component({
    selector: 'app-Resource',
    templateUrl:'./resource.component.html',
    styleUrls: ['./resource.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'ResourceId' },
        { provide: 'url', useValue: COSTINGURLS.Resource },
        { provide: 'DataService', useClass: ResourceService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class ResourceComponent extends GBBaseDataPageComponentWN<IResource> {
    title: string = "Resource"
    ResourceJSON = ResourceJSON

    form: GBDataFormGroupWN<IResource> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Resource", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.ResourcePicklistFillValue(arrayOfValues.ResourceId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }

    public ResourcePicklistFillValue(SelectedPicklistData: string): void {
        console.log("SelectedPicklistData",SelectedPicklistData)
        if (SelectedPicklistData) {
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(Resource => {
                console.log("Resource",Resource)
                    this.form.patchValue(Resource);
                 
            })
        }
    }



    public ResourcePicklistPatchValue(SelectedPicklistDatas: any): void {
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
    public ChangeTab(event, TabName:string) :void{
        var i:number, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
          tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("FormTabLinks");
        for (i = 0; i < tablinks.length; i++) {
          tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(TabName).style.display = "block";
        console.log("Event:",event)
        event.currentTarget.className += " active";
      }
    
    }



export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IResource> {
    const dbds: GBBaseDBDataService<IResource> = new GBBaseDBDataService<IResource>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<IResource>, dbDataService: GBBaseDBDataService<IResource>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IResource> {
    return new GBDataPageService<IResource>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}