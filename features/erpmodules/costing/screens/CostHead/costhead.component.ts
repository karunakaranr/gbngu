import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as CostHeadJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CostHead.json'
import { ICostHead } from '../../models/ICostHead';
import { CostHeadService } from '../../services/CostHead.service';
import { COSTINGURLS } from 'features/erpmodules/costing/URLS/url';

@Component({
    selector: 'app-CostHead',
    templateUrl:'./costhead.component.html',
    styleUrls: ['./costhead.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'CostHeadId' },
        { provide: 'url', useValue: COSTINGURLS.CostHead },
        { provide: 'DataService', useClass: CostHeadService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class CostHeadComponent extends GBBaseDataPageComponentWN<ICostHead> {
    title: string = "CostHead"
    CostHeadJSON = CostHeadJSON

    form: GBDataFormGroupWN<ICostHead> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "CostHead", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.CostHeadPicklistFillValue(arrayOfValues.CostHeadId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }

    public CostHeadPicklistFillValue(SelectedPicklistData: string): void {
        if (SelectedPicklistData) {
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(CostHead => {
                    this.form.patchValue(CostHead);    
            })
        }
    }



    public CostHeadPicklistPatchValue(SelectedPicklistDatas: any): void {
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICostHead> {
    const dbds: GBBaseDBDataService<ICostHead> = new GBBaseDBDataService<ICostHead>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<ICostHead>, dbDataService: GBBaseDBDataService<ICostHead>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICostHead> {
    return new GBDataPageService<ICostHead>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}