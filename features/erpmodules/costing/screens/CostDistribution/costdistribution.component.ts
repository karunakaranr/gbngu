import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as CostDistributionJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CostDistribution.json'
import { COSTINGURLS } from 'features/erpmodules/costing/URLS/url';
import { CostDistributionService } from '../../services/CostDistribution.service';
import { ICostDistribution } from '../../models/ICostDistribution';

@Component({
    selector: 'app-CostDistribution',
    templateUrl:'./costdistribution.component.html',
    styleUrls: ['./costdistribution.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'CostDistributionId' },
        { provide: 'url', useValue: COSTINGURLS.CostDistribution },
        { provide: 'DataService', useClass: CostDistributionService },
        { provide: 'saveurl', useValue: COSTINGURLS.SaveURL},
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class CostDistributionComponent extends GBBaseDataPageComponentWN<ICostDistribution> {
    title: string = "CostDistribution"
    CostDistributionJSON = CostDistributionJSON
    CostDistributionFieldType : number;
    form: GBDataFormGroupWN<ICostDistribution> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "CostDistribution", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.CostDistributionPicklistFillValue(arrayOfValues.CostDistributionId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }

    public CostDistributionPicklistFillValue(SelectedPicklistData: string): void {
        if (SelectedPicklistData) {
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(CostDistribution => {
                    this.form.patchValue(CostDistribution);    
            })
        }
    }
    settingPatchValueToZero(value: number): void {
        this.CostDistributionFieldType = value;
        if (this.form.get('CostDistributionFieldType').value == "0" ) {
            
        
        }
        if(this.form.get('CostDistributionFieldType').value == "1"){
            this.form.get('FieldId').patchValue('-1');
            this.form.get('FieldName').patchValue('NONE');
            this.form.get('FormulaId').patchValue('-1');
            this.form.get('FormulaName').patchValue('NONE');
        }
        if(this.form.get('CostDistributionFieldType').value == "2"){
            this.form.get('FieldId').patchValue('-1');
            this.form.get('FieldName').patchValue('NONE');
            this.form.get('FormulaId').patchValue('-1');
            this.form.get('FormulaName').patchValue('NONE');
        }
       
       
        
    }


    public CostDistributionPicklistPatchValue(SelectedPicklistDatas: any): void {
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
        // let FieldCode = this.form.get('FieldCode').value
        // this.form.get('FieldName').patchValue(FieldCode)
        this.CostDistributionFieldType = this.form.get('CostDistributionFieldType').value;
        console.log("MMM",this.form.value);
    }
}


export function getThisDBDataService(http: GBHttpService, url: string, saveurl: string): GBBaseDBDataService<ICostDistribution> {
    const dbds: GBBaseDBDataService<ICostDistribution> = new GBBaseDBDataService<ICostDistribution>(http);
    dbds.endPoint = url;
    dbds.saveurl = saveurl

    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<ICostDistribution>, dbDataService: GBBaseDBDataService<ICostDistribution>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICostDistribution> {
    return new GBDataPageService<ICostDistribution>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}