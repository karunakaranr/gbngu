export interface ICostHead {
    CostHeadId: number
    CostHeadCode: string
    CostHeadName: string
    CostHeadCostHeadType: number
    CostHeadCostHeadNature: number
    CostHeadCreatedById: number
    CostHeadCreatedOn: string
    CostHeadCreatedByName: string
    CostHeadModifiedById: number
    CostHeadModifiedOn: string
    CostHeadModifiedByName: string
    CostHeadBaseClinetId: number
    CostHeadSortOrder: number
    CostHeadStatus: number
    CostHeadVersion: number
    CostHeadSourceType: number
  }