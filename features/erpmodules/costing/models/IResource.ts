export interface IResource {
    ResourceId: number
    ResourceCode: string
    ResourceName: string
    ResourceTypeId: number
    ResourceTypeCode: string
    ResourceTypeName: string
    CategoryId: number
    CategoryCode: string
    CategoryName: string
    SubCategoryId: number
    SubCategoryCode: string
    SubCategoryName: string
    ModelId: number
    ModelCode: string
    ModelName: string
    BrandId: number
    BrandCode: string
    BrandName: string
    ResourceYOM: number
    ResourceCapacity: number
    UOMId: number
    UOMCode: string
    UOMName: string
    ResourceMinCapacity: number
    ResourceMaxCapacity: number
    ResourceSpeed: number
    ResourceLotPerDay: number
    ItemId: number
    ItemCode: string
    ItemName: string
    ResourceIsCritical: number
    ResourceServiceStatus: number
    ResourceServiceFromDate: string
    ResourceServiceToDate: string
    ServiceProviderId: number
    ServiceProviderCode: string
    ServiceProviderName: string
    SupplierId: number
    SupplierCode: string
    SupplierName: string
    ResourceBillNumber: string
    ResourceBillDate: string
    ResourceValueInBill: number
    ResourcePurchaseDate: string
    ResourceInstallationDate: string
    ResourceResourceOStatus: number
    ResourceMfrSlNo: string
    ResourceResourceValue: number
    WorkCenterId: number
    WorkCenterCode: string
    WorkCenterName: string
    ProcessId: number
    ProcessCode: string
    ProcessName: string
    BatchId: number
    BatchNumber: string
    ResourceItemLinkType: number
    ResourceLastNumber: number
    ResourceEfficiency: number
    ResourceCreatedById: number
    ResourceCreatedOn: string
    ResourceModifiedById: number
    ResourceModifiedOn: string
    ResourceSortOrder: number
    ResourceStatus: number
    ResourceVersion: number
    OrganizationUnitId: number
    OrganizationUnitCode: string
    OrganizationUnitName: string
  }
  