export interface ISetting {
    CostAddOnFieldsId: number
    CostAddOnFieldsCode: string
    CostAddOnFieldsName: string
    CostAddOnFieldsDisplayName: string
    CostAddOnFieldsDataType: string
    CostAddOnFieldsDataSize: string
    CostAddOnFieldsApplicableType: number
    CostAddOnFieldsNature: number
    CostAddOnFieldsIsDaily: number
    LovTypeId: string
    CostAddOnFieldsDefaultValue: string
    CostAddOnFieldsRemarks: string
    CostAddOnFieldsStatus: number
    CostAddOnFieldsVersion: number
    CostAddOnFieldsCreatedByName: string
    CostAddOnFieldsCreatedOn: string
    CostAddOnFieldsModifiedByName: string
    CostAddOnFieldsModifiedOn: string
  }
  