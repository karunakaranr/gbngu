export interface ICostDistribution {
    CostDistributionId: number
    CostDistributionCode: string
    CostDistributionName: string
    CostDistributionCostDistributionType: number
    CostDistributionFieldType: number
    CostDistributionRemarks: string
    FieldId: number
    FieldCode: string
    FieldName: string
    FormulaId: number
    FormulaCode: string
    FormulaName: string
    CostDistributionCreatedById: number
    CostDistributionCreatedOn: string
    CostDistributionCreatedByName: string
    CostDistributionModifiedById: number
    CostDistributionModifiedOn: string
    CostDistributionModifiedByName: string
    CostDistributionBaseClinetId: number
    CostDistributionSortOrder: number
    CostDistributionStatus: number
    CostDistributionVersion: number
    CostDistributionSourceType: number
  }
  