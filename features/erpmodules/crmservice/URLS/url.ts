export class CRMServiceURLS {

    public static ProblemVsSolution = '/crm/ProblemSolution.svc/';
    public static ProblemSolution ='/cs/Criteria.svc/List/?ObjectCode=PROBLEMSOLUTION';
    public static CallSet = '/crm/CallSet.svc/';
    public static WorkType = '/crm/WorkType.svc/';

   }