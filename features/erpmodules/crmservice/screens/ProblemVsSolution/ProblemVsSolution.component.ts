import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';

import { IProblemVsSolution } from '../../models/IProblemVsSolution';
import { ProblemVsSolutionService } from '../../services/ProblemVsSolution/ProblemVsSolution.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

import * as ProblemVsSolutionJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ProblemVsSolution.json'


import { CRMServiceURLS } from '../../URLS/url';


@Component({
    selector: "app-ProblemVsSolution",
    templateUrl: "./ProblemVsSolution.component.html",
    styleUrls: ["./ProblemVsSolution.component.scss"],
    providers: [
        { provide: 'IdField', useValue: 'Problem.Id' },
        { provide: 'url', useValue: CRMServiceURLS.ProblemSolution },
        { provide: 'saveurl', useValue: CRMServiceURLS.ProblemVsSolution },
        { provide: 'deleteurl', useValue: CRMServiceURLS.ProblemVsSolution },
        { provide: 'DataService', useClass: ProblemVsSolutionService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl', 'deleteurl'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class ProblemVsSolutionComponent extends GBBaseDataPageComponentWN<IProblemVsSolution> {
    title: string = "ProblemVsSolution"
    ProblemVsSolutionJson = ProblemVsSolutionJson;


form: GBDataFormGroupWN<IProblemVsSolution> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'ProblemVsSolution', {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.ProblemVsSolutionFillFunction(arrayOfValues.ProblemVsSolutionId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }



    public ProblemVsSolutionFillFunction(SelectedPicklistData: string): void {
        console.log("retrivig", SelectedPicklistData)
     
        if (SelectedPicklistData) {
            this.gbps.dataService.postData(SelectedPicklistData).subscribe(ProblemVsSolution => {
                this.form.get('ProblemId').patchValue(ProblemVsSolution[0].ProblemId)
                this.form.get('ProblemName').patchValue(ProblemVsSolution[0].ProblemName)
                this.form.get('ProblemVsSolutionArray').patchValue(ProblemVsSolution)
            })
        }

    }

    public ProblemVsSolutionPatchValue(SelectedPicklistDatas: any): void {

        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)

    }
}



export function getThisDBDataService(http: GBHttpService, url: string, saveurl: string, deleteurl:string,IdField:string): GBBaseDBDataService<IProblemVsSolution> {
    const dbds: GBBaseDBDataService<IProblemVsSolution> = new GBBaseDBDataService<IProblemVsSolution>(http);
    dbds.endPoint = url;
    dbds.saveurl = saveurl;
    dbds.deleteurl = deleteurl;
    dbds.IdField = IdField;
    return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IProblemVsSolution>, dbDataService: GBBaseDBDataService<IProblemVsSolution>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IProblemVsSolution> {
    return new GBDataPageService<IProblemVsSolution>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
