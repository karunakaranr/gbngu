import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';

import { WorkTypeService } from '../../services/WorkType/WorkType.service';
import { IWorkType, WorkTypeDetailArray } from '../../models/IWorkType';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { CRMServiceURLS } from '../../URLS/url';
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';

import * as WorkTypeJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/WorkType.json'


@Component({
  selector: "app-WorkType",
  templateUrl: "./WorkType.component.html",
  styleUrls: ["./WorkType.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'WorkTypeId' },
    { provide: 'url', useValue: CRMServiceURLS.WorkType },
    { provide: 'DataService', useClass: WorkTypeService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class WorkTypeComponent extends GBBaseDataPageComponentWN<IWorkType> {
  title: string = "WorkType"
  WorkTypeJson = WorkTypeJson;




  form: GBDataFormGroupWN<IWorkType> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'WorkType', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.WorkTypeFillFunction(arrayOfValues.WorkTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }




  public FindReplace(): void {

    var detailarray = JSON.stringify(this.form.get('WorkTypeDetailArray').value)
    var detailarrays = JSON.parse(detailarray)

    var findInput = (document.getElementById('findInput') as HTMLInputElement).value;
    var replaceInput: string = (document.getElementById('replaceInput') as HTMLInputElement).value;

    if (detailarrays !== "") {
      if (findInput !== "") {

        for (let data of detailarrays) {

       
          data.WorkTypeDetailDescription = data.WorkTypeDetailDescription.replace(new RegExp(findInput, 'g'), replaceInput);
          data.WorkTypeDetailRemarks = data.WorkTypeDetailRemarks.replace(new RegExp(findInput, 'g'), replaceInput);

 
        }

        this.form.get('WorkTypeDetailArray').patchValue(detailarrays);

      }
    }
    else {
      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: 'please provide the value in find input.',
          heading: 'Info',
        }
      });
     
    }
  }



  public WorkTypeFillFunction(SelectedPicklistData: string): void {

    console.log("filling", SelectedPicklistData)
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(WorkType => {
        this.form.patchValue(WorkType);
      })
    }
  }


  public WorkTypePatchValue(SelectedPicklistDatas: any): void {

    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>", SelectedPicklistDatas)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IWorkType> {
  const dbds: GBBaseDBDataService<IWorkType> = new GBBaseDBDataService<IWorkType>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IWorkType>, dbDataService: GBBaseDBDataService<IWorkType>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IWorkType> {
  return new GBDataPageService<IWorkType>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
