import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';

// import { IWorkType } from '../../models/IWorkType';
import { IWorkType } from '../../models/IWorkType';
 
@Injectable({
  providedIn: 'root'
})
export class WorkTypeService extends GBBaseDataServiceWN<IWorkType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IWorkType>) {
    super(dbDataService, 'WorkTypeId');
  }
}


