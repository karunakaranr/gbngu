export interface IClient {
    ClientId: number
    ClientCode: string
    ClientName: string
    ClientShortName: string
    ClientMFAApplicableType: number
    MFATypeId: number
    MFATypeCode: string
    MFATypeName: string
    ClientMFAClientAccountId: string
    ClientMFAClientSecretKey: string
    PasswordPolicyId: number
    PasswordPolicyCode: any
    PasswordPolicyName: any
    ClientCreatedById: number
    ClientCreatedOn: string
    ClientModifiedById: number
    ClientModifiedOn: string
    ClientCreatedByName: any
    ClientModifiedByName: any
    ClientSortOrder: number
    ClientStatus: number
    ClientVersion: number
    ClientSourceType: number
    ClientDetailDTO: any
    ProcessInstanceId: string
  }
  