export interface IClientSite {
    ClientSiteId: number
    ClientSiteCode: string
    ClientSiteName: string
    ClientSiteShortName: string
    ClientId: string
    ClientName: string
    RegionId: string
    RegionName: string
    InchargeId: string
    InchargeName: string
    ClientSiteVersion: number
    ClientSiteType: number
    ClientSiteCreatedOn: string
    ClientSiteModifiedOn: string
    ClientSiteCreatedByName: string
    ClientSiteModifiedByName: string
    ClientSiteSourceType: number
  }
  