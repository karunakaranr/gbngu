export interface IEventChannel {
    EventChannelId: number
    EventChannelCode: string
    EventChannelName: string
    OwnerId: string
    OwnerName: string
    AdministratorId: string
    AdministratorName: string
    EventChannelSortOrder: string
    EventChannelStatus: number
    EventChannelVersion: number
    EventChannelCreatedByName: string
    EventChannelCreatedOn: string
    EventChannelModifiedByName: string
    EventChannelModifiedOn: string
  }
  