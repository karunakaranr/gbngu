export interface IProgramType {
    ProgramTypeId: number
    ProgramTypeCode: string
    ProgramTypeName: string
    ProgramTypeRemarks: string
    ProgramTypeCreatedById: number
    ProgramTypeCreatedByName: any
    ProgramTypeCreatedOn: string
    ProgramTypeModifiedById: number
    ProgramTypeModifiedByName: any
    ProgramTypeModifiedOn: string
    ProgramTypeSortOrder: number
    ProgramTypeStatus: number
    ProgramTypeVersion: number
    ProgramTypeSourceType: number
    ProgramTypeScreenOperationMode: number
  }
  