export interface IPortletType {
  PortletTypeId: number
  PortletTypeCode: string
  PortletTypeName: string
  PortletTypeIsMaximize: number
  PortletTypeIsResize: number
  PortletTypeIsPrint: number
  PortletTypeIsExport: number
  PortletTypeIsSettingsId: number
  PortletTypeIsChangeSettings: number
  PortletTypeCreatedById: number
  PortletTypeCreatedOn: string
  PortletTypeModifiedById: number
  PortletTypeModifiedOn: string
  PortletTypeCreatedByName: string
  PortletTypeModifiedByName: string
  PortletTypeSortOrder: number
  PortletTypeStatus: number
  PortletTypeVersion: number
  PortletTypeSourceType: number
  }