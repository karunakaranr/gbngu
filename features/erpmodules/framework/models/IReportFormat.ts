export interface IReportFormat {
    ReportFormatId: number
    ReportFormatReportFile: string
    ReportFormatCode: string
    ReportFormatName: string
    ReportFormatDataSourceFileName: string
    ReportFormatPurpose: string
    ModuleId: number
    ModuleCode: string
    ModuleName: string
    WebServiceId: number
    WebServiceName: string
    WebServiceUriTemplate: string
    Client: string
    ClientCode: any
    ClientName: string
    ReportFormatRepPath: string
    ReportFormatRemarks: string
    ReportFormatIsPrintLogo: number
    ReportFormatSection: string
    ReportFormatCreatedById: number
    ReportFormatCreatedOn: string
    ReportFormatModifiedById: number
    ReportFormatModifiedOn: string
    ReportFormatCreatedByName: any
    ReportFormatModifiedByName: any
    ReportFormatSortOrder: number
    ReportFormatStatus: number
    ReportFormatVersion: number
    ReportFormatSourceType: number
    ReportFormatISOReferenceNumber: string
    MailId: any
    GcmSectionId: number
    GcmSectionCode: string
    GcmSectionName: string
    ReportFormatDisplayFileName: string
  }
  