export interface IPayTaxType {
    PayTaxTypeId: number
    PayTaxTypeCode: string
    PayTaxTypeName: string
    PayTaxTypeRemarks: string
    PayTaxTypeSortOrder: string
    PeriodId: number
    PayTaxTypeDetailArray: PayTaxTypeDetailArray[]
    PayTaxTypeStatus: number
    PayTaxTypeVersion: number
  }
  
  export interface PayTaxTypeDetailArray {
    PayTaxTypeDetailSlNo: number
    PayTaxTypeDetailId: number
    PeriodId: string
    PayTaxTypeDetailSection: string
    PayTaxTypeDetailFieldName: string
    PayTaxTypeDetailDisplayName: string
    PayTaxTypeDetailFieldRemarks: string
    PayTaxTypeDetailType: number
    PayTaxTypeDetailDataType: number
    PayTaxTypeDetailSize: string
    PayTaxTypeDetailEnumValues: string
    PayTaxTypeDetailIsDetail: number
    PayTaxTypeDetailIsAttachment: number
    PayTaxTypeDetailDisplayOrder: string
    PayTaxTypeId: number
  }
  