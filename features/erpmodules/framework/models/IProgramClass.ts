export interface IProgramClass {
    ProgramClassId: number,
    ProgramClassCode: string,
    ProgramClassName: string,
    ProgramClassRemarks: string,
    ProgramAssemblyId: number,
    ProgramAssemblyCode: any,
    ProgramAssemblyName: string,
    ProgramClassSection: string,
    ProgramClassCreatedById: number,
    ProgramClassCreatedOn: string,
    ProgramClassCreatedByName: any,
    ProgramClassModifiedById: number,
    ProgramClassModifiedOn: string,
    ProgramClassModifiedByName: any,
    ProgramClassSortOrder: number,
    ProgramClassStatus: number,
    ProgramClassVersion: number,
    ProgramClassSourceType: number,
    ProgramClassScreenOperationMode: number
  }
  