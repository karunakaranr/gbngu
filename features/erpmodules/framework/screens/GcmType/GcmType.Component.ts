import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { GcmTypeService } from '../../services/GcmType.Service';
import { FrameworkURLS } from '../../URLS/url';
import { IGcmType } from '../../models/IGcmType';
import * as GcmTypeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/GcmType.json'
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';



@Component({
  selector: 'app-GcmType',
  templateUrl: './GcmType.component.html',
  styleUrls: ['./GcmType.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'GcmTypeId'},
    { provide: 'url', useValue: FrameworkURLS.GcmType },
    { provide: 'DataService', useClass: GcmTypeService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class GcmTypeComponent extends GBBaseDataPageComponentWN<IGcmType > {
  title: string = 'GcmType'
  GcmTypeJSON = GcmTypeJSON;


  form: GBDataFormGroupWN<IGcmType> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'GcmType', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.GcmTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IGcmType> {
  const dbds: GBBaseDBDataService<IGcmType> = new GBBaseDBDataService<IGcmType>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IGcmType>, dbDataService: GBBaseDBDataService<IGcmType>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IGcmType> {
  return new GBDataPageService<IGcmType>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


