import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { EnumService } from '../../services/Enum.service';
import { IEnum } from '../../models/IEnum';

import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { FrameworkURLS } from '../../URLS/url';
import * as EnumJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Enum.json'


@Component({
  selector: 'app-Enum',
  templateUrl: 'enum.component.html',
  styleUrls: ['enum.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'EnumarationId' },
    { provide: 'url', useValue:FrameworkURLS.Enum },
    { provide: 'DataService', useClass: EnumService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class EnumComponent extends GBBaseDataPageComponentWN<IEnum> {
  title: string = "Enum"
  EnumJSON = EnumJSON;

  form: GBDataFormGroupWN<IEnum> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Enum", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.EnumValue(arrayOfValues.EnumarationId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public EnumValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Enum => {
        this.form.patchValue(Enum);
      })
    }
  }
  public EnumNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IEnum> {
  const dbds: GBBaseDBDataService<IEnum> = new GBBaseDBDataService<IEnum>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IEnum>, dbDataService: GBBaseDBDataService<IEnum>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IEnum> {
  return new GBDataPageService<IEnum>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
