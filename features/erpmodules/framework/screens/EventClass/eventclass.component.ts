import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { EventClassService } from '../../services/EventClass.service';
import { IEventClass } from '../../models/IEventClass';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as EventClassJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/EventClass.json'
import { FrameworkURLS } from '../../URLS/url';
@Component({
  selector: 'app-EventClass',
  templateUrl:'eventclass.component.html',
  styleUrls:['eventclass.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'EventClassId' },
    { provide: 'url', useValue: FrameworkURLS.EventClass},
    { provide: 'DataService', useClass: EventClassService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class EventClassComponent extends GBBaseDataPageComponentWN<IEventClass> {
  title: string = "EventClass"
  EventClassJSON = EventClassJSON;

  form: GBDataFormGroupWN<IEventClass> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "EventClass", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.EventclassFormVal(arrayOfValues.EventClassId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public EventclassFormVal(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(EventClass => {
          this.form.patchValue(EventClass);
      })
    }
  }
  public Eventclasspatch(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IEventClass> {
  const dbds: GBBaseDBDataService<IEventClass> = new GBBaseDBDataService<IEventClass>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IEventClass>, dbDataService: GBBaseDBDataService<IEventClass>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IEventClass> {
  return new GBDataPageService<IEventClass>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
