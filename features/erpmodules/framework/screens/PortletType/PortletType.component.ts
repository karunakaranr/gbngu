import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { PortletTypeService } from '../../services/PortletType.service';
import { IPortletType } from '../../models/IPortletType';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as PortletTypeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PortletType.json'
import { FrameworkURLS } from '../../URLS/url';


@Component({
    selector: 'app-PortletType',
  templateUrl: './PortletType.component.html',
    styleUrls: ['./PortletType.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'PortletTypeId' },
        { provide: 'url', useValue: FrameworkURLS.PortletType },
        { provide: 'DataService', useClass: PortletTypeService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class PortletTypeComponent extends GBBaseDataPageComponentWN<IPortletType> {
    title: string = 'PortletType'
    PortletTypeJSON = PortletTypeJSON;


    form: GBDataFormGroupWN<IPortletType> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'PortletType', {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.PortletTypeRetrivalValue(arrayOfValues.PortletTypeId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }


    public PortletTypeRetrivalValue(SelectedPicklistData: string): void {
        console.log("PortletTypeRetrivalValue",SelectedPicklistData)
        if (SelectedPicklistData) {
            console.log("SelectedPicklistData",SelectedPicklistData)
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
                console.log("PackSet",PackSet)
                this.form.patchValue(PackSet);
            })
        }
    }


    public PortletTypePatchValue(SelectedPicklistDatas: any): void {
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPortletType> {
    const dbds: GBBaseDBDataService<IPortletType> = new GBBaseDBDataService<IPortletType>(http);
    dbds.endPoint = url;
    return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPortletType>, dbDataService: GBBaseDBDataService<IPortletType>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPortletType> {
    return new GBDataPageService<IPortletType>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
