import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EventTypeComponent } from './screens/EventType/eventtype.component';
import { EventSourceComponent } from './screens/EventSource/EventSource.component';
import { ProgramClassComponent } from './screens/ProgramClass/ProgramClass.Component';
import { GcmTypeComponent } from './screens/GcmType/GcmType.Component';
import { PortletTypeComponent } from './screens/PortletType/PortletType.component';
import { EventClassComponent } from './screens/EventClass/eventclass.component';
import { ClientComponent } from './screens/Client/client.component';
import { EnumComponent } from './screens/Enum/enum.component';
import { PicklistComponent } from './screens/Picklist/Picklist.component';
import { EventChannelComponent } from './screens/EventChannel/EventChannel.component';
import { ProgramTypeComponent } from './screens/ProgramType/ProgramType.Component';
import { ProgramComponent } from './screens/Program/Program.component';
import { ProgramAssemblyComponent } from './screens/ProgramAssembly/ProgramAssembly.component';
import { ClientSiteComponent } from './screens/ClientSite/ClientSite.Component';
import { PayTaxTypeComponent } from './screens/PayTaxType/PayTaxType.component';
import { ReportFormatComponent } from './screens/ReportFormat/ReportFormat.component';
import { EntityNewComponent } from './screens/Entity/entity.component';
import { ModuleComponent } from './screens/Module/module.component';
const routes: Routes = [

  {
    path: 'entitynew',
    component: EntityNewComponent

  },
  {
    path: 'module',
    component: ModuleComponent
  },


  {
    path: 'programassmbly',
    component: ProgramAssemblyComponent
  },
  {
    path: 'PayTaxType',
    component: PayTaxTypeComponent
  },
  {
    path: 'reportformat',
    component: ReportFormatComponent
  },
  {
    path: 'ClientSite',
    component: ClientSiteComponent
  },
  {
    path: 'Enum',
    component: EnumComponent
  },
  {
    path: 'program',
    component: ProgramComponent
  },
  {
    path: 'EventChannel',
    component: EventChannelComponent
  },
  {
    path: 'ProgramType',
    component: ProgramTypeComponent
  },
  {
    path: 'picklist',
    component: PicklistComponent
  },
  {
    path: 'Client',
    component: ClientComponent
  },
  {
    path: 'eventclass',
    component: EventClassComponent
  },

  {
    path: 'themestest',
    loadChildren: () =>
      import(`../../themes/themes.module`).then((m) => m.ThemesModule),
  },
  {
    path: 'portlettype',
    component: PortletTypeComponent
  },

  {
    path: 'eventtype',
    component: EventTypeComponent
  },

  {
    path: 'EventSource',
    component: EventSourceComponent
  },
  {
    path: 'ProgramClass',
    component: ProgramClassComponent
  },
  {
    path: 'GcmType',
    component: GcmTypeComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FrameWorkroutingModule { }