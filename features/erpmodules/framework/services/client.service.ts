import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IClient } from '../models/IClient';


// import {IClient } from './../models/IClient'
@Injectable({
  providedIn: 'root'
})
export class ClientService extends GBBaseDataServiceWN<IClient> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IClient>) {
    super(dbDataService, 'ClientId');
  }
}