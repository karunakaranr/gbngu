import { Inject, Injectable } from '@angular/core';
// import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';



import{IProgram} from '../../models/IProgram'


 
@Injectable({
  providedIn: 'root'
})
export class ProgramService extends GBBaseDataServiceWN<IProgram> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IProgram>) {
    super(dbDataService, 'ProgramId');
  }
}
