import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';



import { IReportFormat } from '../../models/IReportFormat';


 
@Injectable({
  providedIn: 'root'
})
export class ReportFormatService extends GBBaseDataServiceWN<IReportFormat> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IReportFormat>) {
    super(dbDataService, 'ReportFormatId');
  }
}
