import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IProgramType } from '../models/IProgramType';


 
@Injectable({
  providedIn: 'root'
})
export class ProgramTypeService extends GBBaseDataServiceWN<IProgramType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IProgramType >) {
    super(dbDataService, 'ProgramTypeId');
  }
}
