import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IPayTaxType } from '../models/IPayTaxType';



 
@Injectable({
  providedIn: 'root'
})
export class PayTaxTypeService extends GBBaseDataServiceWN<IPayTaxType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPayTaxType>) {
    super(dbDataService, 'PayTaxTypeId');
  }
}
