import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IGcmType } from '../models/IGcmType';





 
@Injectable({
  providedIn: 'root'
})
export class GcmTypeService extends GBBaseDataServiceWN<IGcmType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IGcmType>) {
    super(dbDataService, 'GcmTypeId');
  }
}
