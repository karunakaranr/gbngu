
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnalyticComponent } from './analytics.component';
import { DimensionComponent } from './screens/Dimension/dimension.component';
import { HierarchyComponent } from './screens/Hierarchy/hierarchy.component';
import { KPIPostingComponent } from './screens/KPIPosting/kpiposting.component';
const routes: Routes = [
    {
        path: 'kpip',
        component: KPIPostingComponent

    },
    {
        path: '',
        component: AnalyticComponent

    },
    {
        path: 'hierarchy',
        component: HierarchyComponent

    },
    {
        path: 'dimension',
        component: DimensionComponent
    },
    

    { path: 'themestest', loadChildren: () => import(`./../../themes/themes.module`).then(m => m.ThemesModule) },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class AnalyticRoutingModule { }
