import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'goodbooks-analytic',
  template: `
  `,
  styles: ['ul li a {margin-bottom: 0px; color: black}'],
  // encapsulation: ViewEncapsulation.None
})
export class AnalyticComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
