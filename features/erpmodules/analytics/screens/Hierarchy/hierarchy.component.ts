import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { HierarchyService } from 'features/erpmodules/hrms/services/Hierarchy/hierarchy.service';
import { IHierarchy } from 'features/erpmodules/analytics/models/IHierarchy';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as HierarchyJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Hierarchy.json';
import { ANALYTICSURLS } from 'features/erpmodules/analytics/URLS/urls';

@Component({
  selector: 'app-hierarchy',
  templateUrl: './hierarchy.component.html',
  styleUrls: ['./hierarchy.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'HierarchyId' },
    { provide: 'url', useValue: ANALYTICSURLS.Hierarchy },
    { provide: 'DataService', useClass: HierarchyService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class HierarchyComponent extends GBBaseDataPageComponentWN<IHierarchy> {
  title: string = "Hierarchy"
  HierarchyJson = HierarchyJson;


  form: GBDataFormGroupWN<IHierarchy> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Hierarchy", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.HierarchyRetrivalValue(arrayOfValues.HierarchyId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else { 
      this.store.dispatch(new ButtonVisibility("Forms")) 
    }
  }

  public HierarchyRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Hierarchy => {
        this.form.patchValue(Hierarchy);
      })
    }
  }

  public HierarchyPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}

export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IHierarchy> {
  const dbds: GBBaseDBDataService<IHierarchy> = new GBBaseDBDataService<IHierarchy>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IHierarchy>, dbDataService: GBBaseDBDataService<IHierarchy>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IHierarchy> {
  return new GBDataPageService<IHierarchy>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
