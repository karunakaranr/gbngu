import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { DimensionService } from '../../services/Dimensions/dimension.service';
import { IDimension } from '../../models/IDimension';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as DimensionJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Dimension.json';
import { ANALYTICSURLS } from '../../URLS/urls';

@Component({
  selector: 'app-dimension',
  templateUrl: './dimension.component.html',
  styleUrls: ['./dimension.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'DimensionId' },
    { provide: 'url', useValue: ANALYTICSURLS.Dimension },
    { provide: 'DataService', useClass: DimensionService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class DimensionComponent extends GBBaseDataPageComponentWN<IDimension> {
  title: string = "Dimension"
  DimensionJson = DimensionJson;


  form: GBDataFormGroupWN<IDimension> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Dimension", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.DimensionRetrivalValue(arrayOfValues.DimensionId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else { 
      this.store.dispatch(new ButtonVisibility("Forms")) 
    }
  }

  public DimensionRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Dimension => {
        this.form.patchValue(Dimension);
      })
    }
  }

  public DimensionPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}

export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IDimension> {
  const dbds: GBBaseDBDataService<IDimension> = new GBBaseDBDataService<IDimension>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IDimension>, dbDataService: GBBaseDBDataService<IDimension>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IDimension> {
  return new GBDataPageService<IDimension>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
