import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IKPIPosting } from '../../models/IKPIPosting';
import { KPIPostingService } from '../../services/KPIPosting/kpiposting.Service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { ANALYTICSURLS } from '../../URLS/urls';
import * as KPIPostingJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/KPIPosting.json'
@Component({
  selector: 'app-KPIPosting',
  templateUrl:'./kpiposting.component.html',
  styleUrls: ['./kpiposting.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ModuleId' },
    { provide: 'url', useValue:ANALYTICSURLS.KPIPosting },
    { provide: 'DataService', useClass: KPIPostingService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class KPIPostingComponent extends GBBaseDataPageComponentWN<IKPIPosting> {
  title: string = "KPIPosting"
  KPIPostingJSON = KPIPostingJSON;




  
  form: GBDataFormGroupWN<IKPIPosting> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "KPIPosting", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.KPIPostingValue(arrayOfValues.ModuleId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public KPIPostingValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(KPIPosting => {
        this.form.patchValue(KPIPosting);
      })
    }
  }
  public KPIPostingNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IKPIPosting> {
  const dbds: GBBaseDBDataService<IKPIPosting> = new GBBaseDBDataService<IKPIPosting>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IKPIPosting>, dbDataService: GBBaseDBDataService<IKPIPosting>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IKPIPosting> {
  return new GBDataPageService<IKPIPosting>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
