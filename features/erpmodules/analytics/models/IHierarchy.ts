export interface IHierarchy {
    HierarchyId: number
    HierarchyName: string
    HierarchyDimensionId: number
    HierarchyDimensionName: string
    DBObjectId: number
    DBObjectName: string
    HierarchyType: number
    HierarchyDetailArray: HierarchyDetailArray[]
    HierarchyStatus: number
    HierarchySortOrder: number
    HierarchyVersion: number
    HierarchySourceType: number
    HierarchyCreatedById: number
    HierarchyCreatedOn: string
    HierarchyModifiedById: number
    HierarchyModifiedOn: string
    HierarchyCreatedByName: string
    HierarchyModifiedByName: string
  }
  
  export interface HierarchyDetailArray {
    DisplayFieldNames: string
    HierarchyDetailId: number
    HierarchyId: number
    HierarchyName: string
    HierarchyDetailSlNo: number
    DBObjectFieldsId: number
    DBObjectFieldsName: string
    HierarchyDetailHierarchyOrder: number
    PicklistId: number
    PicklistName: string
  }
  