import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ActivityTypeService } from '../../services/ActivityType/ActivityType.service';
import { IActivityType } from '../../models/IActivityType';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
import * as ActivityTypeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ActivityType.json'
import { MaintenanceURLS } from '../../URLS/urls';


@Component({
  selector: 'app-ActivityType',
  templateUrl: './ActivityType.component.html',
  styleUrls: ['./ActivityType.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ActivityTypeId'},
    { provide: 'url', useValue: MaintenanceURLS.ActivityType },
    { provide: 'DataService', useClass: ActivityTypeService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ActivityTypeComponent extends GBBaseDataPageComponentWN<IActivityType > {
  title: string = 'ActivityType'
  ActivityTypeJSON = ActivityTypeJSON;


  form: GBDataFormGroupWN<IActivityType > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'ActivityType', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ActivityTypeFillFunction(arrayOfValues.ActivityTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public ActivityTypeFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public ActivityTypePatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IActivityType > {
  const dbds: GBBaseDBDataService<IActivityType > = new GBBaseDBDataService<IActivityType >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IActivityType >, dbDataService: GBBaseDBDataService<IActivityType >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IActivityType > {
  return new GBDataPageService<IActivityType >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}




