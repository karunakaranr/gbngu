import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
// import { ServiceName } from ‘ServicePath’;
// import { IAssetTypeMaterial } from ‘ModelPath’;
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { MaintenanceURLS } from '../../URLS/urls';
import { AssetTypeMaterialService } from '../../services/AssetTypeMaterial/AssetTypeMaterial.service';
import { IAssetTypeMaterial } from '../../models/IAssetTypeMaterial';
import * as AssetTypeMaterialJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/AssetTypeMaterial.json'
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
// import { url} from ‘UrlPath’;


@Component({
  selector: 'app-AssetTypeMaterial',
  templateUrl: './AssetTypeMaterial.component.html',
  styleUrls: ['./AssetTypeMaterial.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ActivityId'},
    { provide: 'url', useValue: MaintenanceURLS.AssetTypeMatrial },
    { provide: 'saveurl', useValue: MaintenanceURLS.AssetTypeMatrialsave },
    { provide: 'deleteurl', useValue: MaintenanceURLS.AssetTypeMatrialdelete },
    { provide: 'DataService', useClass: AssetTypeMaterialService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url', 'saveurl', 'deleteurl','IdField'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class AssetTypeMaterialComponent extends GBBaseDataPageComponentWN<IAssetTypeMaterial> {
  title: string = 'AssetTypeMaterial'
  AssetTypeMaterialJSON = AssetTypeMaterialJSON;


  form: GBDataFormGroupWN<IAssetTypeMaterial > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'AssetTypeMaterial', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.ActivityId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.postData(SelectedPicklistData).subscribe(AssetTypeMaterial => {
        this.form.get('ActivityId').patchValue(AssetTypeMaterial[0].ActivityId)
        this.form.get('ActivityCode').patchValue(AssetTypeMaterial[0].ActivityCode)
        this.form.get('ActivityName').patchValue(AssetTypeMaterial[0].ActivityName)
        this.form.get('AssetTypeMaterialArray').patchValue(AssetTypeMaterial)
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string, saveurl: string, deleteurl:string,IdField:string): GBBaseDBDataService<IAssetTypeMaterial > {
  const dbds: GBBaseDBDataService<IAssetTypeMaterial > = new GBBaseDBDataService<IAssetTypeMaterial >(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl;
  dbds.deleteurl = deleteurl;
  dbds.IdField = IdField
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IAssetTypeMaterial >, dbDataService: GBBaseDBDataService<IAssetTypeMaterial >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IAssetTypeMaterial > {
  return new GBDataPageService<IAssetTypeMaterial >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
