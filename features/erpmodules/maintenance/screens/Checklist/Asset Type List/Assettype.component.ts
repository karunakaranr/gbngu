
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';



@Component({
    selector: 'app-Assesttype',
    templateUrl: './Assettype.component.html',
    styleUrls: ['./Assettype.component.scss'],
})
export class AssettypeComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowDatas: any;
    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getAssettype();
    }

    public getAssettype() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowDatas = data;
            if (data.length > 0) {

            console.log("Assesttype Details:", this.rowDatas)
            }
                });
    }
}
