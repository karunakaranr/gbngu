
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-Assetactivity',
    templateUrl: './Assetactivity.component.html',
    styleUrls: ['./Assetactivity.component.scss'],
})
export class AssetactivityComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowDatas: any;
    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getAssetactivity();
    }

    public getAssetactivity() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowDatas = data;
            if (data.length > 0) {

                console.log("Assetactivity Details :", this.rowDatas)

                let groupingData = this.rowDatas;
                var finalizedGroup = [];

                groupingData.map((inputData, index) => {

                    let assetNature = '';

                    if (inputData['AssetActivityNature'] === 0) {
                        assetNature = 'Asset';
                    } else if (inputData['AssetActivityNature'] === 1) {
                        assetNature = 'Machine';
                    } else if (inputData['AssetActivityNature'] === 2) {
                        assetNature = 'Resource';
                    }


                    let code;
                    let name;
                    let AssetActivityTaskAssignedType;

                    if (inputData['AssetActivityNature'] == 0) {
                        code = inputData['AssetCode'] === 'NONE' ? '' : inputData['AssetCode'];
                        name = inputData['AssetName'] === 'NONE' ? '' : inputData['AssetName'];
                    } else if (inputData['AssetActivityNature'] == 1) {
                        code = inputData['MachineCode'] === 'NONE' ? '' : inputData['MachineCode'];
                        name = inputData['MachineName'] === 'NONE' ? '' : inputData['MachineName'];

                    } else if (inputData['AssetActivityNature'] == 2) {
                        code = inputData['ResourceCode'] === 'NONE' ? '' : inputData['ResourceCode'];
                        name = inputData['ResourceName'] === 'NONE' ? '' : inputData['ResourceName'];
                    }

                    if (inputData['AssetActivityTaskAssignedType'] == 0) {
                        AssetActivityTaskAssignedType = inputData['UserName'] === 'NONE' ? '' : inputData['UserName'];

                    } else if (inputData['AssetActivityTaskAssignedType'] == 1) {
                        AssetActivityTaskAssignedType = inputData['TaskUserGroupName'] === 'NONE' ? '' : inputData['TaskUserGroupName'];
                    } else if (inputData['AssetActivityTaskAssignedType'] == 2) {
                        AssetActivityTaskAssignedType = inputData['RoleName'] === 'NONE' ? '' : inputData['RoleName'];

                    } else if (inputData['AssetActivityTaskAssignedType'] == 3) {
                        AssetActivityTaskAssignedType = inputData['EmployeeName'] === 'NONE' ? '' : inputData['EmployeeName'];

                    }



                    finalizedGroup.push({
                        sno: index,

                        code: code,
                        name: name,
                        activity: inputData['ActivityName'],
                        Parameterset: inputData['ParameterSetName'],
                        Assignedto: AssetActivityTaskAssignedType,
                        Duration: parseFloat(inputData['AssetActivityDuration']).toFixed(2),
                        Grace: parseFloat(inputData['AssetActivityGraceTime']).toFixed(2),
                        Tickettime: parseFloat(inputData['AssetActivityTicketTime']).toFixed(2),
                        pl: inputData['AssetActivityLastGeneratedTill'],
                        sl: inputData['AssetActivityCronDescription'],


                        Asset: assetNature,


                    });
                });

                const final = {};
                finalizedGroup.forEach((detailData) => {
                    final[detailData.Asset] = {
                        assetactivitygrup: {},
                        //sno:detailData.DailyAttDate,
                        code: detailData.Asset,
                        name: "",
                        activity: "",
                        Parameterset: "",
                        Assignedto: "",
                        Duration: "",
                        Grace: "",
                        Tickettime: "",
                        pl: "",
                        sl: "",
                        ...final[detailData.Asset]

                    };

                    final[detailData.Asset].assetactivitygrup[detailData.sno] = {
                        //sno:detailData.sno,
                        code: detailData.code,
                        name: detailData.name,
                        activity: detailData.activity,
                        Parameterset: detailData.Parameterset,
                        Assignedto: detailData.Assignedto,
                        Duration: detailData.Duration,
                        Grace: detailData.Grace,
                        Tickettime: detailData.Tickettime,
                        pl: detailData.pl,
                        sl: detailData.sl,

                    }
                });

                const supGroups = Object.keys(final);

                const tableData = [];

                supGroups.forEach((codeData) => {
                    tableData.push({
                        //sno:final[codeData].sno,
                        code: final[codeData].code,
                        name: "",
                        activity: "",
                        Parameterset: "",
                        Assignedto: "",
                        Duration: "",
                        Grace: "",
                        Tickettime: "",
                        pl: "",
                        sl: "",

                        bold: true,
                    });

                    const accounts = Object.keys(final[codeData].assetactivitygrup);
                    accounts.forEach((account) => {
                        tableData.push({
                            //sno:final[codeData].assetactivitygrup[account].sno,
                            code: final[codeData].assetactivitygrup[account].code,
                            name: final[codeData].assetactivitygrup[account].name,
                            activity: final[codeData].assetactivitygrup[account].activity,
                            Parameterset: final[codeData].assetactivitygrup[account].Parameterset,
                            Assignedto: final[codeData].assetactivitygrup[account].Assignedto,
                            Duration: final[codeData].assetactivitygrup[account].Duration,
                            Grace: final[codeData].assetactivitygrup[account].Grace,
                            Tickettime: final[codeData].assetactivitygrup[account].Tickettime,
                            pl: final[codeData].assetactivitygrup[account].pl,
                            sl: final[codeData].assetactivitygrup[account].sl,

                        });

                    });

                });
                this.rowDatas = tableData;

                console.log("Final Data:", this.rowDatas[0])
            }
        });
    }
}

