
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-Equipmentkey',
    templateUrl: './Equipmentkey.component.html',
    styleUrls: ['./Equipmentkey.component.scss'],
})
export class EquipmentkeyComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getEquipmentkey();
    }

    public getEquipmentkey() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;
            if (data.length > 0) {

            console.log("Equipmentkey Details :", this.rowData)


            let groupingData = this.rowData;
            var finalizedGroup = [];

            groupingData.map((inputData, index) => {


                let nature;
                let TypeComponentNature;

                if (inputData['TypeComponentNature'] == 0) {
                    nature = inputData['AssetTypeCode'] === 'NONE' ? '' : inputData['AssetTypeCode'];
                    TypeComponentNature = inputData['AssetTypeName'] === 'NONE' ? '' : inputData['AssetTypeName'];
                } else if (inputData['TypeComponentNature'] == 1) {
                    nature = inputData['ResourceTypeCode'] === 'NONE' ? '' : inputData['ResourceTypeCode'];
                    TypeComponentNature = inputData['ResourceTypeName'] === 'NONE' ? '' : inputData['ResourceTypeName'];
                } else if (inputData['TypeComponentNature'] == 2) {
                    nature = inputData['MachineTypeCode'] === 'NONE' ? '' : inputData['MachineTypeCode'];
                    TypeComponentNature = inputData['MachineTypeName'] === 'NONE' ? '' : inputData['MachineTypeName'];
                } else if (inputData['TypeComponentNature'] == 4) {
                    nature = inputData['SubCategoryCode'] === 'NONE' ? '' : inputData['SubCategoryCode'];
                    TypeComponentNature = inputData['SubCategoryName'] === 'NONE' ? '' : inputData['SubCategoryName'];
                } else if (inputData['TypeComponentNature'] == 5) {
                    nature = inputData['ItemCode'] === 'NONE' ? '' : inputData['ItemCode'];
                    TypeComponentNature = inputData['ItemName'] === 'NONE' ? '' : inputData['ItemName'];
                }

                let code;
                let name;

                for (let detailArray of inputData['TypeComponentDetailArray']){

                    if ((detailArray && detailArray['TypeComponentDetailDetailNature']) == 0) {
                        code = detailArray['DetailAssetTypeCode'] === 'NONE' ? '' : detailArray['DetailAssetTypeCode'];
                        name = detailArray['DetailAssetTypeName'] === 'NONE' ? '' : detailArray['DetailAssetTypeName'];                    
                    }
                    else  if ((detailArray && detailArray['TypeComponentDetailDetailNature']) == 1) {
                        code = detailArray['DetailResourceTypeCode'] === 'NONE' ? '' : detailArray['DetailResourceTypeCode'];
                        name = detailArray['DetailResourceTypeName'] === 'NONE' ? '' : detailArray['DetailResourceTypeName'];                    
                    } else  if ((detailArray && detailArray['TypeComponentDetailDetailNature']) == 2) {
                        code = detailArray['DetailMachineTypeCode'] === 'NONE' ? '' : detailArray['DetailMachineTypeCode'];
                        name = detailArray['DetailMachineTypeName'] === 'NONE' ? '' : detailArray['DetailMachineTypeName'];                    
                    } else  if ((detailArray && detailArray['TypeComponentDetailDetailNature']) == 3) {
                        code = detailArray['DetailCategoryCode'] === 'NONE' ? '' : detailArray['DetailCategoryCode'];
                        name = detailArray['DetailCategoryCode'] === 'NONE' ? '' : detailArray['DetailCategoryCode'];                    
                    } else  if ((detailArray && detailArray['TypeComponentDetailDetailNature']) == 4) {
                        code = detailArray['DetailSubCategoryCode'] === 'NONE' ? '' : detailArray['DetailSubCategoryCode'];
                        name = detailArray['DetailSubCategoryName'] === 'NONE' ? '' : detailArray['DetailSubCategoryName'];                    
                    } else  if ((detailArray && detailArray['TypeComponentDetailDetailNature']) == 5) {
                        code = detailArray['DetailItemCode'] === 'NONE' ? '' : detailArray['DetailItemCode'];
                        name = detailArray['DetailItemName'] === 'NONE' ? '' : detailArray['DetailItemName'];                    
                    }

                    finalizedGroup.push({
                        sno:index,
    
                        code: code,
                        name: name,
                        qty: parseFloat(detailArray['TypeComponentDetailQuantity']).toFixed(2),
                        type: detailArray['TypeComponentDetailUsageTypeName'],
                        supplier: detailArray['PartyName'],

                        headoff: nature,
                        account: TypeComponentNature,
    
                    });
                }
                
                


            });

            const final = {};
            finalizedGroup.forEach((detailData) => {
                final[detailData.headoff] = {
                    attendancegroup: {},
                    //sno:detailData.DailyAttDate,
                    code: detailData.headoff,
                    name: detailData.account,
                    qty: "",
                    type: "",
                    supplier: "",

                    ...final[detailData.headoff]

                };

                final[detailData.headoff].attendancegroup[detailData.sno] = {
                    //sno:detailData.sno,
                    code: detailData.code,
                    name: detailData.name,
                    qty: detailData.qty,
                    type: detailData.type,
                    supplier: detailData.supplier,

                }
            });

            const supGroups = Object.keys(final);

            const tableData = [];

            supGroups.forEach((codeData) => {
                tableData.push({
                    //sno:final[codeData].sno,
                    code: final[codeData].code,
                    name: final[codeData].name,
                    qty: "",
                    type: "",
                    supplier: "",

                    bold: true,
                });

                const accounts = Object.keys(final[codeData].attendancegroup);
                accounts.forEach((account) => {
                    tableData.push({
                        //sno:final[codeData].attendancegroup[account].sno,
                        code: final[codeData].attendancegroup[account].code,
                        name: final[codeData].attendancegroup[account].name,
                        qty: final[codeData].attendancegroup[account].qty,
                        type: final[codeData].attendancegroup[account].type,
                        supplier: final[codeData].attendancegroup[account].supplier,

                    });

                });

            });
            this.rowData = tableData;

            console.log("Final Data:", this.rowData[0])
        }
        });
    }
}

