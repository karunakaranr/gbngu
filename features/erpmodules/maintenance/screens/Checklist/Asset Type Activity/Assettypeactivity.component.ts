
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-Assettypeactivity',
    templateUrl: './Assettypeactivity.component.html',
    styleUrls: ['./Assettypeactivity.component.scss'],
})
export class AssettypeactivityComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getAssettypeactivity();
    }

    public getAssettypeactivity() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;
            if (data.length > 0) {

            console.log("Assettype Activity Details :", this.rowData)
            

            let grouping = this.rowData;

            var finalizedGrp = [];

            grouping.map((inputdata,index) =>{

              let temp1; 


              if (inputdata['AssetTypeActivityPeriod'] === "none") {
                temp1 = ''; // Set temp1 to an empty string if 'AssetTypeActivityPeriod' is "none"
              } else if (inputdata['AssetTypeActivityPeriod'] == 0) {
                temp1 = "Hourly";
              } else if (inputdata['AssetTypeActivityPeriod'] == 1) {
                temp1 = "Daily";
              } else if (inputdata['AssetTypeActivityPeriod'] == 2) {
                temp1 = "Weekly";
              } else if (inputdata['AssetTypeActivityPeriod'] == 3) {
                temp1 = "FortNightly";
              } else if (inputdata['AssetTypeActivityPeriod'] == 4) {
                temp1 = "Monthly";
              } else if (inputdata['AssetTypeActivityPeriod'] == 5) {
                temp1 = "Quarterly";
              } else if (inputdata['AssetTypeActivityPeriod'] == 6) {
                temp1 = "Half Yearly";
              }

            //   if (inputdata["AssetTypeActivityFrequency"] ==0){
            //     temp2 = "Yes "
            //   } else if(inputdata["AssetTypeActivityFrequency"] ==1){
            //     temp2 ="No"
            //   } else {
            //     temp2 =""
            //   }




                finalizedGrp.push({
                    sno:index,
                    code:inputdata['ActivityCode'],
                    name:inputdata['ActivityName'],
                    Frequency:inputdata['AssetTypeActivityFrequency'] === 0 ? "Yes" : inputdata ['AssetTypeActivityFrequency'] === 1 ? "No" : inputdata ['AssetTypeActivityFrequency'],
                    Period: temp1,
                    Duration:  inputdata['AssetTypeActivityDuration'] === 0 ? '' : inputdata['AssetTypeActivityDuration'] ,

                    

                    nature: inputdata['AssetTypeCode'],
                    assettypname:inputdata['AssetTypeName'],
                });
            });

            const final = {};
            finalizedGrp.forEach((detailData) => {
                final[detailData.nature] ={
                    supplyGroups:{},       
                    code:detailData.nature,
                    name:detailData.assettypname,
                    Frequency:"",
                    Period:"",
                    Duration:"",

                    
                    ...final[detailData.nature]

                };

                final[detailData.nature].supplyGroups[detailData.sno] = {
                    //sno:detailData.sno,
                    code: detailData.code,
                    name:detailData.name,
                    Frequency: detailData.Frequency,
                    Period: detailData.Period,
                    Duration: detailData.Duration,

                
                }
            });
            const finalgrouping = Object.keys(final);
            const tableData = [];

            finalgrouping.forEach((codeData) => {
                tableData.push({
                    code:final[codeData].code,
                    name:final[codeData].name,
                    Frequency:"",
                    Period:"",
                    para:"",
                    bold: true,
                });

                const accounts = Object.keys(final[codeData].supplyGroups); 
                accounts.forEach((account) => {
                    tableData.push({
                        code:final[codeData].supplyGroups[account].code,
                        name:final[codeData].supplyGroups[account].name,
                        Frequency:final[codeData].supplyGroups[account].Frequency,
                        Period:final[codeData].supplyGroups[account].Period,
                        Duration:final[codeData].supplyGroups[account].Duration,


                    })
                })

            })

             this.rowData=tableData;
             console.log("Final Data:",this.rowData[0])
          }

        });

       
    }
}
