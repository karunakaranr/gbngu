import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';



import { IActivity } from '../../models/IActivity';


 
@Injectable({
  providedIn: 'root'
})
export class ActivityService extends GBBaseDataServiceWN<IActivity> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IActivity>) {
    super(dbDataService, 'ActivityId');
  }
}
