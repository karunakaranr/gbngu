import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IAssetTypeMaterial } from '../../models/IAssetTypeMaterial';


 
@Injectable({
  providedIn: 'root'
})
export class AssetTypeMaterialService extends GBBaseDataServiceWN<IAssetTypeMaterial> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IAssetTypeMaterial>) {
    super(dbDataService, 'AssetType.Id');
  }
}
