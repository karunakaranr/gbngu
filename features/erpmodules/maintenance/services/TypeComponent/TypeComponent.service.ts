import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { ITypeComponent } from '../../models/ITypeComponent';


 
@Injectable({
  providedIn: 'root'
})
export class TypeComponentService extends GBBaseDataServiceWN<ITypeComponent> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ITypeComponent>) {
    super(dbDataService, 'ComponentId','TypeComponent');
  }
}
