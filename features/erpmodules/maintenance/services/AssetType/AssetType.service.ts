import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


// import { ModelName } from ‘ModelPath’;
import { IAssetType } from '../../models/IAssetType';


 
@Injectable({
  providedIn: 'root'
})
export class AssetTypeService extends GBBaseDataServiceWN<IAssetType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IAssetType>) {
    super(dbDataService, 'AssetTypeId');
  }
}
