export interface  ITypeComponent {
    TypeComponentId: number;
    TypeComponentNature: string;
    AssetTypeId: number;
    AssetTypeName: string;
    ResourceTypeId: number;
    MachineTypeId: number;
    CategoryId: number;
    SubCategoryId: number;
    ItemId: number;
    TypeComponentRemarks: string;
    TypeComponentVersion: number;
    TypeComponentStatus: number;
    TypeComponentCreatedByName: string;
    TypeComponentCreatedOn: string;
    TypeComponentModifiedByName: string;
    TypeComponentModifiedOn: string;
    TypeComponentDetailArray: TypeComponentDetailArray[];
  }
  interface TypeComponentDetailArray {
    TypeComponentDetailDetailNature: string;
    undefined: string;
    DetailMachineTypeId: string;
    DetailMachineTypeName: string;
    TypecomponenetFieldName: string;
    DetailItemStockUOMName: string;
    TypeComponentDetailQuantity: string;
    TypeComponentDetailUsageType: number;
    PartyId: string;
    PartyName: string;
    TypeComponentDetailRemarks: string;
    TypeComponentDetailSlNo: number;
  }