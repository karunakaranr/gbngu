export class MaintenanceURLS {
    public static AssetType = '/fms/AssetType.svc/';
    public static Parameterset = '/fws/ParameterSet.svc/';
    public static ResourceType = '/cts/ResourceType.svc/';
    public static Account = 'as/Account.svc/';
    public static Depreciation = '/fam/Depreciation.svc/';
    public static AssetSchedule = '/fam/AssetSchedule.svc/';
    public static ActivityRecruitmentProcess = '/ms/Activity.svc/'
    public static CodeDefine = '/fws/CodeDefine.svc/'
    public static Solution = '/crm/Solution.svc/';
    public static TypeComponent = '/ms/TypeComponent.svc/';
    public static ActivityType = '/ms/ActivityType.svc/';
    public static AssetTypeMatrial = '/cs/Criteria.svc/List/?ObjectCode=ACTIVITYMATERIAL';
    public static AssetTypeMatrialsave = '/ms/ActivityMaterial.svc/';
    public static AssetTypeMatrialdelete = '/ms/ActivityMaterial.svc/?ActivityId=-1499999999&AssetTypeId=-1499999998&ResourceTypeId=-1&MachineTypeId=-1';
    public static TypeComponentGet = '/cs/Criteria.svc/List/?ObjectCode=TYPECOMPONENT'
    public static Meter = '/fms/Meter.svc/'
    
    // public static EmployeeQualification = "/cs/Criteria.svc/List/?ObjectCode=EMPLOYEEQUALIFICATION";

} 
