import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IDevice } from '../../models/IDevice';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { FacilityURLS } from '../../URLS/url';
import { DeviceService } from '../../service/device.service';
import * as DeviceJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Device.json'
@Component({


  selector: 'app-Device',
  templateUrl: './device.component.html',
  styleUrls: ['./device.component.scss'],

  providers: [
    { provide: 'IdField', useValue: 'DeviceId'},
    { provide: 'url', useValue: FacilityURLS.Device },
    { provide: 'DataService', useClass: DeviceService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class DeviceComponent extends GBBaseDataPageComponentWN<IDevice> {
  title : string = "Device"
  DeviceJSON = DeviceJSON

form: GBDataFormGroupWN<IDevice> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Device", {}, this.gbps.dataService);
  thisConstructor() {

    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
   
      this.DevicePicklistFillValue(arrayOfValues. DeviceId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }


  public DevicePicklistFillValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Device => {
          this.form.patchValue(Device);

        })
    }
  }


  public DevicePicklistPatchValue(SelectedPicklistDatas: any): void {
    // console.log("category",SelectedPicklistDatas)
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)

  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IDevice> {
  const dbds: GBBaseDBDataService<IDevice> = new GBBaseDBDataService<IDevice>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IDevice>, dbDataService: GBBaseDBDataService<IDevice>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IDevice> {
  return new GBDataPageService<IDevice>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
