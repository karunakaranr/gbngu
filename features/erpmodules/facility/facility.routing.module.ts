import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DeviceTypeComponent } from './screen/DeviceType/devicetype.component';
import { DeviceComponent } from './screen/Device/device.component';


const routes: Routes = [
  {
    path: 'themestest',
    loadChildren: () =>
      import(`../../themes/themes.module`).then((m) => m.ThemesModule),
  }, 

      {
        path: 'DeviceType',
        component: DeviceTypeComponent
      },
   
      {
        path: 'device',
        component: DeviceComponent
      }, 
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class facilityroutingModule {}
