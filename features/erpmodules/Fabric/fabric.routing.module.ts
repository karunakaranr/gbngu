import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

 

const routes: Routes = [


    {
      path: 'themestest',
      loadChildren: () =>
        import(`../../themes/themes.module`).then((m) => m.ThemesModule),
    }
  ];
  
  @NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
  })
  export class WMSroutingModule {}