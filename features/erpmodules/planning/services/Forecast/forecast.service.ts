import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IForeCast } from '../../models/IForeCast';

 
@Injectable({
  providedIn: 'root'
})
export class ForecastService extends GBBaseDataServiceWN<IForeCast> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IForeCast>) {
    super(dbDataService, 'ForeCastId');
  }
}

