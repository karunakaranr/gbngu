import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IForeCastSet } from '../models/IForeCastSet';

@Injectable({
  providedIn: 'root'
})
export class ForeCastSetService extends GBBaseDataServiceWN<IForeCastSet> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IForeCastSet>) {
    super(dbDataService, 'ForeCastSetId');
  }
}