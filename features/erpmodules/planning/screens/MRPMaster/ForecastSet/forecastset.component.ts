import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IForeCastSet } from 'features/erpmodules/planning/models/IForeCastSet';
import { ForeCastSetService } from 'features/erpmodules/planning/services/ForeCastSet.Service';
import { PLANNINGURLS } from 'features/erpmodules/planning/URLS/url';
import * as ForeCastSetJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ForeCastSet.json'

import { ButtonVisibility } from 'features/layout/store/layout.actions';

@Component({
  selector: 'app-ForeCastSet',
  templateUrl:'forecastset.component.html',
  styleUrls:['forecastset.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'ForeCastSetId' },
    { provide: 'url', useValue: PLANNINGURLS.ForeCastSet},
    { provide: 'DataService', useClass: ForeCastSetService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ForeCastSetComponent extends GBBaseDataPageComponentWN<IForeCastSet> {
  title: string = "ForeCastSet"
  ForeCastSetJSON = ForeCastSetJSON;

  form: GBDataFormGroupWN<IForeCastSet> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "ForeCastSet", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ForeCastSetFormVal(arrayOfValues.ForeCastSetId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public ForeCastSetFormVal(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(ForeCastSet => {
          this.form.patchValue(ForeCastSet);
      })
    }
  }
  public ForeCastSetpatch(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IForeCastSet> {
  const dbds: GBBaseDBDataService<IForeCastSet> = new GBBaseDBDataService<IForeCastSet>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IForeCastSet>, dbDataService: GBBaseDBDataService<IForeCastSet>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IForeCastSet> {
  return new GBDataPageService<IForeCastSet>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
