export class PLANNINGURLS {
    public static MRPMaster = '/mms/MRP.svc/';
    public static MasterSchedule = '/mms/MasterSchedule.svc/';
    public static  ForeCastSet ='/mms/ForeCastSet.svc/';
    public static Forecast = '/mms/ForeCast.svc/';
}