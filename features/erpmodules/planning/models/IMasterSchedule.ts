export interface IMasterSchedule {
    MasterScheduleId: number
    MasterScheduleCode: string
    MasterScheduleName: string
    MasterScheduleOrganizationType: number
    OUId: number
    OUCode: string
    OUName: string
    OUGroupId: number
    OUGroupCode: string
    OUGroupName: string
    MasterScheduleIsIncludeInATP: number
    MasterScheduleIsTrackConversion: number
    MasterScheduleIsAutoRelease: number
    MasterScheduleIsPending: number
    MasterScheduleDemandType: number
    MasterScheduleValidTillDate: string
    MasterScheduleCreatedById: number
    MasterScheduleCreatedOn: string
    MasterScheduleModifiedById: number
    MasterScheduleModifiedOn: string
    MasterScheduleCreatedByName: any
    MasterScheduleModifiedByName: any
    MasterScheduleSortOrder: number
    MasterScheduleStatus: number
    MasterScheduleVersion: number
    MasterScheduleDetailArray: any[]
  }
  