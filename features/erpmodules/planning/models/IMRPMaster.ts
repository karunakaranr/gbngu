export interface IMRPMaster {
    MRPId: number
    MRPCode: string
    MRPName: string
    MRPIsAutoRelease: number
    MRPIsTrackConversion: number
    MRPOverWriteType: number
    MRPOrganizationType: number
    OUId: number
    OUCode: string
    OUName: string
    OUGroupId: number
    OUGroupCode: string
    OUGroupName: string
    MasterScheduleId: number
    MasterScheduleCode: string
    MasterScheduleName: string
    MRPIsPending: number
    MRPIncludeLeadTime: number
    MRPIncludeLotSize: number
    MRPIncludeStockInTransit: number
    MRPIncludeStock: number
    MRPCreatedById: number
    MRPCreatedOn: string
    MRPModifiedById: number
    MRPModifiedOn: string
    MRPCreatedByName: any
    MRPModifiedByName: any
    MRPSortOrder: number
    MRPStatus: number
    MRPVersion: number
  }
  