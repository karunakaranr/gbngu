export class IItemCategory {
  CodeCategoryId: number
  ItemCategoryId: number
  ItemCategoryCode: string
  ItemCategoryName: string
  ItemCategoryIsSelected: number
  ItemCategoryIsSelectedName: string
  ItemCategoryRemarks: string
  ItemCategoryIsAsset: number
  ItemCategoryIsItem: number
  ItemCategoryCreatedById: number
  ItemCategoryCreatedOn: string
  ItemCategoryCreatedByName: string
  ItemCategoryModifiedById: number
  ItemCategoryModifiedOn: string
  ItemCategoryModifiedByName: string
  ItemCategoryScreenOperationMode: number
  ItemCategorySortOrder: number
  ItemCategoryStatus: number
  ItemCategoryVersion: number
    // ItemCategoryId: number;
    // ItemCategoryCode: string;
    // ItemCategoryName: string;
    // ItemShortName: string;
    // ItemCategory: string;
    // ItemBrand: string;
    // ItemCategoryIsSelected: string;
    // ItemCategoryIsAsset: string;
    // ItemCategoryIsItem: string;
    // ItemCategoryRemarks: string;
    // ItemCategoryCreatedOn: Date;
    // ItemCategoryModifiedOn: Date;
    // ItemCategoryModifiedByName: string;
    // ItemCategoryCreatedByName: string;
    // ItemCategoryStatus: number;
    // ItemCategoryVersion: number;
    // Itemsortorder: number;
    // ItemsourceType: string;
  
    // constructor() {
    //   this.ItemCategoryStatus = 1,
    //     this.ItemCategoryVersion = 1
    // }
  }
  

  export interface GridSetting{
    headerName : string;
    field : string;
    sortable : boolean;
    filter : boolean;
    checkboxSelection : boolean;
    resizable : boolean;
    editable : boolean;
    pinned : string;
    cellEditor : string;
    cellEditorParams : string;
  }
  export interface GridProperties{
    paginationAutoPageSize: boolean;
    paginationPageSize : number;
    pagination : boolean;
    undoRedoCellEditing : boolean;
    undoRedoCellEditingLimit : number;
    enableCellChangeFlash : boolean;
    enterMovesDown : boolean;
    enterMovesDownAfterEdit : boolean;
    rowSelection : string;
    enableRangeSelection : boolean;
    paginateChildRows : boolean;
  }
  