export interface IMRPPartyType {
    MRPPartyTypeId: number
    MRPPartyTypeCode: string
    MRPPartyTypeName: string
    MRPPartyTypeParticulars: string
    MRPPartyTypeDetailArray: MrppartyTypeDetailArray[]
    MRPPartyTypeSourceType: number
    MRPPartyTypeSortOrder: number
    MRPPartyTypeStatus: number
    MRPPartyTypeVersion: number
    MRPPartyTypeCreatedById: number
    MRPPartyTypeCreatedOn: string
    MRPPartyTypeModifiedById: number
    MRPPartyTypeModifiedOn: string
    MRPPartyTypeCreatedByName: string
    MRPPartyTypeModifiedByName: string
  }
  
  export interface MrppartyTypeDetailArray {
    MRPPartyTypeDetailId: number
    MRPPartyTypeDetailSlNo: number
    MRPItemTypeId: number
    MRPItemTypeCode: string
    MRPItemTypeName: string
    MRPPartyTypeDetailNature: number
    BIZTransactionId: number
    BIZTransactionCode: string
    BIZTransactionName: string
    MRPPartyTypeId: number
    MRPPartyTypeCode: string
    MRPPartyTypeName: string
  }
  