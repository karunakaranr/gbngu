export class IItemSubCategory {
    ItemSubCategoryId: number;
    ItemSubCategoryCode: string;
    ItemSubCategoryName: string;
    ItemSubCategoryItemCategoryId: string;
    ItemSubCategoryItemCategoryName: string;
    ItemSubCategoryIsSelected: string;
    ItemSubCategoryIsAsset: string;
    ItemSubCategoryIsItem: string;
    ItemSubCategoryRemarks: string;
    ItemSubCategoryCreatedOn: Date;
    ItemSubCategoryModifiedOn: Date;
    ItemSubCategoryModifiedByName: string;
    ItemSubCategoryCreatedByName: string;
    ItemSubCategoryStatus: number;
    ItemSubCategoryVersion: number;
    Itemsortorder: number;
    ItemsourceType: string;
  
    constructor() {
      this.ItemSubCategoryStatus = 1,
        this.ItemSubCategoryVersion = 1
    }
  }
  