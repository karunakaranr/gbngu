export interface IParametermap
 {
    
    ParameterMapId: number;
    ParameterMapCode: string;
    ParameterMapName: string;
    ParameterMapType: number;
    ParameterMapStartTimeParameterId: number;
    ParameterMapStartTimeParameterName: string;
    ParameterMapEndTimeParameterId: number;
    ParameterMapEndTimeParameterName: string;
    ParameterMapStartFuelParameterId: number;
    ParameterMapStartFuelParameterName: string;
    ParameterMapEndFuelParameterId: number;
    ParameterMapEndFuelParameterName: string;
    ParameterMapStartUnitParameterId: number;
    ParameterMapStartUnitParameterName: string;
    ParameterMapEndUnitParameterId: number;
    ParameterMapEndUnitParameterName: string;
    ParameterMapStatus: number;
    ParameterMapVersion: number;
    ParameterMapCreatedByName: string;
    ParameterMapCreatedOn: string;
    ParameterMapModifiedByName: string;
    ParameterMapModifiedOn: string;
    ParameterSetName: number;

  }
  