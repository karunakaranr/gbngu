export interface ICycleClass {
    CycleClassId: number
    CycleClassCycleClasso: string
    CycleClassDescription: string
    CycleClassIntervalDays: number
    CycleClassPercentageOfItems: number
    CycleClassSourceType: number
    CycleClassSortOrder: number
    CycleClassStatus: number
    CycleClassVersion: number
    CycleClassCreatedById: number
    CycleClassCreatedOn: string
    CycleClassModifiedById: number
    CycleClassModifiedOn: string

}

