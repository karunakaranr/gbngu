export interface IMaterialGroup {
    MaterialGroupId: number
    MaterialGroupCode: string
    MaterialGroupName: string
    MaterialGroupCreatedOn: string
    MaterialGroupModifiedOn: string
    MaterialGroupModifiedByName: string
    MaterialGroupCreatedByName: string
    MaterialGroupStatus: number
    MaterialGroupVersion: number
  }
  