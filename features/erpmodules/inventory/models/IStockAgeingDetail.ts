export interface IStockAgeingDetail{
    map(arg0: (row: any, index: any) => void): unknown
    Type: string
    Id: number
    Code: string
    Name: string
    ItemId: number
    ItemCode: string
    ItemName: string
    UOMId: number
    UOMCode: string
    UOMName: string
    Age1GoodQuantity: number
    Age2GoodQuantity: number
    Age3GoodQuantity: number
    Age4GoodQuantity: number
    Age5GoodQuantity: number
    Age6GoodQuantity: number
    Age7GoodQuantity: number
    Age1Value: number
    Age2Value: number
    Age3Value: number
    Age4Value: number
    Age5Value: number
    Age6Value: number
    Age7Value: number
    Age1ReportValue: string
    Age2ReportValue: string
    Age3ReportValue: string
    Age4ReportValue: string
    Age5ReportValue: string
    Age6ReportValue: string
    Age7ReportValue: string
    NumberOfAge: number
    FirstInDate: string
    LastOutDate: string
    NoOfDays: number
    TotalAge1Value: number
    TotalAge2Value: number
    TotalAge3Value: number
    TotalAge4Value: number
    TotalAge5Value: number
    TotalAge6Value: number
    TotalAge7Value: number
    TotalAgeValue: number
    NumberOfRecords: number
    Label: string
  }

export interface IStockAgeingDetailTable{
    sno: number
    code: string
    name: string
    icode: string
    iname: string
    agevalue: number
    age1value: number
    age2value: number
    age3value: number
    age4value: number
    age5value: number
    age6value: number
}