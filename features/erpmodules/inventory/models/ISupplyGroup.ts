export interface ISupplyGroup {
    SupplyGroupId: number
    SupplyGroupCode: string
    SupplyGroupName: string
    SupplyGroupRemarks: string
    SupplyGroupSortOrder: number
    SupplyGroupStatus: number
    SupplyGroupVersion: number
    SupplyGroupCreatedById: number
    SupplyGroupCreatedOn: string
    SupplyGroupModifiedById: number
    SupplyGroupModifiedOn: string
    SupplyGroupCreatedByName: string
    SupplyGroupModifiedByName: string
    SupplyGroupDetailArray: SupplyGroupDetailArray[]
    SupplyGroupSourceType: number
  }
  
  export interface SupplyGroupDetailArray {
    SupplyGroupDetailId: number
    SupplyGroupId: number
    SupplyGroupDetailSlNo: number
    SupplyGroupDetailParticulars: string
    SupplyGroupDetailSharePercent: number
    OuId: number
    OuCode: string
    OuName: string
    PartyBranchId: number
    PartyBranchCode: string
    PartyBranchName: string
  }
  