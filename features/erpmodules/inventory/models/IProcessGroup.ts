export interface IProcessGroup
 {
    MProcessGroupId : number;
    MProcessGroupCode : string;
    MProcessGroupName : string;
    MProcessGroupProductType : number;
    MProcessGroupProcessType : number;
    MProcessGroupResourceType : number;
    MProcessGroupPatternType : number;
    ParameterSetId : number;
    ParameterSetCode : string;
    ParameterSetName : string;
    MProcessGroupSortOrder : number;
    MProcessGroupStatus : number;
    MProcessGroupVersion : number;
    MProcessGroupCreatedById : number;
    MProcessGroupCreatedByName : string;
    MProcessGroupCreatedOn : string;
    MProcessGroupModifiedById : number;
    MProcessGroupModifiedByName : string;
    MProcessGroupModifiedOn : string;
  }
  