
export interface IUOM {
    UOMId: number;
    UOMCode: string;
    UOMName: string;
    UOMType: string;
    UOMNoOfDecimals: string;
  
    UOMStatus: number;
    UOMVersion: number;
    sortorder: number;
    sourceType: string;
  }
  export interface GridSetting{
    headerName : string;
    field : string;
    sortable : boolean;
    filter : boolean;
    checkboxSelection : boolean;
    resizable : boolean;
    editable : boolean;
    pinned : string;
    cellEditor : string;
    cellEditorParams : string;
  }
  export interface GridProperties{
    paginationAutoPageSize: boolean;
    paginationPageSize : number;
    pagination : boolean;
    undoRedoCellEditing : boolean;
    undoRedoCellEditingLimit : number;
    enableCellChangeFlash : boolean;
    enterMovesDown : boolean;
    enterMovesDownAfterEdit : boolean;
    rowSelection : string;
    enableRangeSelection : boolean;
    paginateChildRows : boolean;
  }

  export interface picklist {
    Id:   number;
    Code: string;
    Name: string;
}
  