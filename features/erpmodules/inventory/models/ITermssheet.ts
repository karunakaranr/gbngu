export interface ITermssheet {
    TermsSetId: number;
    TermsSetName: string;
    TermsSetType: number;
    TermsSetTypeName: string;
    TermsSetIsSalesApplicable: number;
    TermsSetIsPurchaseApplicable: number;
    TermsSetSortOrder: number;
    TermsSetStatus: number;
    TermsSetVersion: number;
    TermsSetCreatedById: number;
    TermsSetCreatedOn: string;
    TermsSetCreatedByName?: any;
    TermsSetModifiedById: number;
    TermsSetModifiedOn: string;
    TermsSetModifiedByName?: any;
    TermsSetDetailArray: TermsSetDetailArray[];
  }
  export interface TermsSetDetailArray {
    TermsSetDetailId: number;
    TermsSetId: number;
    TermsSetName: string;
    TermsSetDetailSlNo: number;
    TermsId: number;
    TermsSection: string;
    TermsDescription: string;
    TermsType: number;
    TermsTypeDetailTypeName: string;
    TermsSetDetailReferenceNo: string;
  }