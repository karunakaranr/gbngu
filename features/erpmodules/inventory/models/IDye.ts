export interface IDye {
    DiecutId: number
    DiecutNumber: string
    ShapeId: number
    ShapeCode: string
    ShapeName: string
    DiecutUPS: number
    DiecutFinishedSize: string
    DiecutDiecutSize: string
    DiecutPaperSize: string
    DiecutWaste: number
    MakerId: number
    MakerCode: string
    MakerName: string
    DiecutSizeForRate: number
    DiecutRatePerSquare: number
    DiecutRate: number
    DiecutSourceType: number
    DiecutSortOrder: number
    DiecutStatus: number
    DiecutVersion: number
    DiecutCreatedById: number
    DiecutCreatedOn: string
    DiecutModifiedById: number
    DiecutModifiedOn: string
  }
  