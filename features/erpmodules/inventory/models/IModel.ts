export interface IModel {
  ModelId: number
  ModelCode: string
  ModelName: string
  ModelShortName: string
  ItemCategoryId: number
  ItemCategoryCode: string
  ItemCategoryName: string
  ItemBrandId: number
  ItemBrandCode: string
  ItemBrandName: string
  ModelIsSelected: number
  ModelIsAsset: number
  ModelIsItem: number
  ModelDefaultCmsId: string
  ModelCreatedById: number
  ModelCreatedOn: string
  ModelCreatedByName: string
  ModelModifiedById: number
  ModelModifiedOn: string
  ModelModifiedByName: string
  ModelSortOrder: number
  ModelStatus: number
  ModelVersion: number
}
