export interface ILovType {
    LovTypeId: number
    LovTypeCode: string
    LovTypeName: string
    LovTypeScoreRequired: number
    LovTypeCreatedById: number
    LovTypeCreatedOn: string
    LovTypeCreatedByName: string
    LovTypeModifiedById: number
    LovTypeModifiedOn: string
    LovTypeModifiedByName: string
    LovTypeSortOrder: number
    LovTypeStatus: number
    LovTypeVersion: number
    LovTypeSourceType: number
    LovTypeLovNature: number
    LovTypeGenerationType: number
    ParentLovTypeId: number
    ParentLovTypeCode: string
    ParentLovTypeName: string

}