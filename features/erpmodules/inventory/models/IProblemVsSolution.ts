export interface ProblemVsSolution {
    ProblemSolutionId: number
    ProblemId: number
    ProblemCode: string
    ProblemName: string
    ProblemSolutionSlNo: number
    SolutionId: number
    SolutionSolutionCode: string
    SolutionSolutionName: string
    ProblemSolutionRemarks: string
  }
  