import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InventoryComponent } from './inventory.component';
import { BrandComponent } from './screens/brand/brand/brand.component';
import { BrandlistComponent } from './screens/brand/brandlist/brandlist.component';
// import { ItemcategoryComponent } from './screens/itemcategory/itemcategory/itemcategory.component';
import { ItemCategorylistComponent } from './screens/itemcategory/itemcategorylist/itemcategorylist.component';
import { ItemSubCategorylistComponent } from './screens/itemsubcategory/itemsubcategorylist/itemsubcategorylist.component';
import { PackComponent } from './screens/Pack/Pack/pack.component';
import { PacklistComponent } from './screens/Pack/Packlist/packlist.component';
import { PacksetComponent } from './screens/packset/packset/packset.component';
import { PacksetlistComponent } from './screens/packset/packsetlist/packsetlist.component';
// import { UOMComponent } from './screens/UOM/UOM/uom.component';
import { UOMlistComponent } from './screens/UOM/UOMList/uomlist.component';
import { PartyListViewComponent } from './screens/PartyList/PartyListView/PartyListView.component';
import { ItemOUListViewComponent } from './screens/ItemOUList/ItemOUListView/ItemOUList.component';
import { ItemPackComponent } from './screens/ItemPack/ItemPack.component'
import { PartyBranchListComponent } from './screens/PartyBranch/PartyBranchList/PartyBranchList.component';
import { UserListComponent } from './screens/User/UserList/UserList.component';
import { ItemStockListComponent } from './screens/ItemStock/ItemStockList/ItemStockList.component';
import { ItemReportComponent } from './screens/Item/ItemReport/ItemReportList.component';
import { ItemBrandReportComponent } from './screens/brand/BrandListReport/BrandListReport.component';
import { ItemGroupListComponent } from './screens/ItemGroup/ItemGroupList/ItemGroupList.component';
import { CostcenterlistComponent } from './screens/costcenter/costcenterlist/costcenterlist.component';
import { TermssetlistComponent } from './screens/TermsSet/TermsSetList/TermsSetList.component';
import { AccountSchedulelistComponent } from './screens/AccountScheduleList/AccountScheduleList/AccountScheduleList.component';
import { BankbranchlistComponent } from './screens/bankbranch/bankbranchlist/bankbranchlist.component';
import { JobdefinelistComponent } from './screens/Jobdefine/jobdefinelist/jobdefinelist.component';
import { MaterialAccountComponent } from './screens/MaterialAccount/MaterialAccount.component';

import { LovComponent } from './screens/Lov/Lov.component';
import { ProcessOperationComponent } from './screens/ProcessOperation/ProcessOperation.component';
import { MaterialGroupComponent } from './screens/MaterialGroup/MaterialGroup.component';
import { CycleClassComponent } from './screens/CycleClass/CycleClass.component';
import { LotTypeComponent } from './screens/LotType/LotType.component';
import { LovTypeListComponent } from './screens/LovType/LovTypeList/LovTypeList.component';
import { partyitemlistComponent } from './screens/PartyItem/PartyItemList/partyitemlist.component';
import { itemwiseComponent } from './screens/ItemWise/itemWise/itemwise.component';
import { ParameterComponent } from './screens/Parameter/parameter.component';
import { StockPositionDetailComponent } from './screens/StockPositionDetail/StockPositionDetail/StockPositionDetail.component';
import { WOReportComponent } from './screens/RegisterWithoutItem/WOItemReports/WOReport.component';
import { StockLedgerDetailComponent } from './screens/Stock Ledger/Stock Ledger detail/StockLedgerDetail.component';
import { pricecategoryComponent } from './screens/PriceCategory/pricecategory.component';
import { StocklegersummaryComponent } from './screens/Stock Ledger/Stock Ledger Summary/stocklegersummary.component';
import { DatewiseminComponent } from './screens/Register/DateWise/Datewise Min and Inward/Datewisemin.component';
import { StockageingsummaryComponent } from './screens/Stock Ageing/Stock Ageing Summary/Stockageingsummary.component';
import { StockAgeingDetailComponent } from './screens/Stock Ageing/Stock Ageing Detail/StockAgeingDetail.component';
// import { LovTypeComponent } from './screens/LovType/LovType/LovType.component';
import { UOMComponent } from './screens/UOM/UOM/uom.component';
import { LovTypeComponent } from './screens/LovType/LovType/LovType.component';
import { ItemCategoryComponent } from './screens/itemcategory/itemcategory/itemcategory.component';
import { ProcessGroupComponent } from './screens/ProcessGroup/ProcessGroup.component';
import { ParametermapComponent } from './screens/Parametermap/parametermap.component';
import { SKUSettingComponent } from './screens/SKUSetting/skusetting.component';
import { ItemSubCategoryComponent } from './screens/itemsubcategory/itemsubcategory/itemsubcategory.component';
import { UOMSetComponent } from './screens/UOMSet/uomset.component';
import { SupplyGroupComponent } from './screens/SupplyGroup/supplygroup.component';
import { ItemMasterComponent } from './screens/ItemMaster/ItemMaster.component';
import { stockpositionComponent } from './screens/StockPositionSummary/StockPositionSummary/StockPositionSummary.component';
import { DyeComponent } from './screens/Dye/Dye.component';
import { TermComponent } from './screens/Terms/term.component';
import { MachinesComponent } from './screens/Machine/Machine.Component';
import { AssemblyBOMComponent } from './screens/AssemblyBOM/assemblybom.component';
import { ItemOUComponent } from './screens/ItemOU/itemou.component';
import { TermssheetComponent } from './screens/Tremssheet/Termssheet.component';
import { MRPPartyTypeComponent } from './screens/MRPPartyType/mrppartytype.component';
import { StoreComponent } from './screens/Store/StoreList/Store/Store.component';
import { ItemTemplatePartComponent } from './screens/ItemTemplatePart/ItemTemplatePart.component';
import { SimpleItemComponent } from './screens/SimpleItem/simpleitem.component';
import { ModelComponent } from './screens/Model/Model.component';
import { PrintedLabelComponent } from './screens/PrintedLabel/printedlabel.component';
import { MasterScheduleComponent } from './screens/MasterSchedule/masterschedule.component';
import { BomTreeComponent } from './screens/BomTree/BomTree.component';
const routes: Routes = [
  {
    path : 'MasterSchedule',
    component: MasterScheduleComponent
    },
  {
    path : 'PrintedLabel',
    component: PrintedLabelComponent
    },
  {
    path:'model',
    component: ModelComponent
  },
  {
    path: 'storecomponent',
    component: StoreComponent
  },
  {
    path: 'simpleitem',
    component: SimpleItemComponent
  },
  {
    path: 'ItemTemplatePart',
    component: ItemTemplatePartComponent
  },
  {
    path: 'cycleclass',
    component: CycleClassComponent
  },
  {
    path: 'mrppartytype',
    component: MRPPartyTypeComponent
  },
  {
    path: 'itemou',
    component: ItemOUComponent
  },
  {
    path: 'dye',
    component: DyeComponent
  },
  {
    path: 'assemblyboom',
    component: AssemblyBOMComponent
  },


  {
    path: 'termssheet',
    component: TermssheetComponent
  },
  {
    path: 'Machines',
    component: MachinesComponent
  },
  {
    path: '',
    component: InventoryComponent
  },
  {
    path: 'term',
    component: TermComponent
  },
  {
    path: 'itemmaster',
    component: ItemMasterComponent
  },
  {
    path: 'supplygroup',
    component: SupplyGroupComponent
  },
  {
    path: 'UOMSet',
    component: UOMSetComponent
  },
  {
    path: 'SKUSetting',
    component: SKUSettingComponent
  },
  {
    path: 'parametermap',
    component: ParametermapComponent
  },
  {
    path: 'materialgroup',
    component: MaterialGroupComponent
  },

  {
    path: 'processgroup',
    component: ProcessGroupComponent
  },
  {
    path: 'parameter',
    component: ParameterComponent
  },
  {
    path: 'Pack',
    children: [
      {
        path: 'list',
        component: PacklistComponent
      },
      {
        path: ':id',
        component: PackComponent,
      },
      {
        path: '',
        component: PackComponent,
      }
    ]
  }, {
    path: 'Stocklegersummary',
    children: [
      {
        path: 'list',
        component: StocklegersummaryComponent
      }
    ]
  },

  {
    path: 'Datewisemin',
    children: [
      {
        path: 'list',
        component: DatewiseminComponent
      }
    ]
  },

  {
    path: 'Stockageingsummary',
    children: [
      {
        path: 'list',
        component: StockageingsummaryComponent
      }
    ]
  },
  {
    path: 'pricecategory',
    children: [
      {
        path: 'list',
        component: pricecategoryComponent
      },
    ]
  },
  {
    path: 'Parameter',
    children: [
      {
        path: ':id',
        component: ParameterComponent
      },
      {
        path: '',
        component: ParameterComponent
      }
    ]
  },
  {
    path: 'packset',
    children: [
      {
        path: 'list',
        component: PacksetlistComponent
      },
      {
        path: ':id',
        component: PacksetComponent,
      },
      {
        path: '',
        component: PacksetComponent,
      }
    ]
  },
  {
    path: 'UOM',
    children: [
      {
        path: 'list',
        component: UOMlistComponent
      },
      {
        path: ':id',
        component: UOMComponent
      },
      {
        path: '',
        component: UOMComponent
      },
    ]
  },
  {
    path: 'brand',
    children: [
      {
        path: 'list',
        component: BrandlistComponent
      },
      {
        path: ':id',
        component: BrandComponent
      },
      {
        path: '',
        component: BrandComponent
      },
    ]
  },
  {
    path: 'itemcategory',
    children: [
      {
        path: 'list',
        component: ItemCategorylistComponent
      },
      {
        path: ':id',
        component: ItemCategoryComponent
      },
      {
        path: '',
        component: ItemCategoryComponent
      },
    ]
  },
  {
    path: 'itemsubcategory',
    children: [
      {
        path: 'list',
        component: ItemSubCategorylistComponent
      },
      {
        path: ':id',
        component: ItemSubCategoryComponent
      },
      {
        path: '',
        component: ItemSubCategoryComponent
      },
    ]
  },
  // {
  //   path: 'model',
  //   children: [
  //     {
  //       path: 'list',
  //       component: ModellistComponent
  //     },
  //     // {
  //     //   path: ':id',
  //     //   component: ModelComponent
  //     // },
  //     // {
  //     //   path: '',
  //     //   component: ModelComponent
  //     // },
  //   ]
  // },
  {
    path: 'partylistview',
    children: [
      {
        path: 'list',
        component: PartyListViewComponent
      },
    ]
  },
  {
    path: 'partyitem',
    children: [
      {
        path: 'list',
        component: partyitemlistComponent
      },
    ]
  },
  {
    path: 'itemoulistview',
    children: [
      {
        path: 'list',
        component: ItemOUListViewComponent
      },
    ]
  },
  {
    path: 'materialaccount',
    children: [
      {
        path: 'list',
        component: MaterialAccountComponent
      },
    ]
  },
  {
    path: 'itempack',
    children: [
      {
        path: 'list',
        component: ItemPackComponent
      }
    ]
  },

  {
    path: 'partybranch',
    children: [
      {
        path: 'list',
        component: PartyBranchListComponent
      },]
  },
  {
    path: 'userlist',
    children: [
      {
        path: 'list',
        component: UserListComponent
      },
    ]
  },
  {
    path: 'itemstocklist',
    children: [
      {
        path: 'list',
        component: ItemStockListComponent
      },
    ]
  },

  {
    path: 'itemreportlist',
    children: [
      {
        path: 'list',
        component: ItemReportComponent
      },
    ]
  },
  {
    path: 'itembrandlist',
    children: [
      {
        path: 'list',
        component: ItemBrandReportComponent
      },
    ]
  },
  {
    path: 'itemgrouplist',
    children: [
      {
        path: 'list',
        component: ItemGroupListComponent
      },
    ]
  },
  {
    path: 'lovtype',
    children: [
      {
        path: ':id',
        component: LovTypeComponent
      },
      {
        path: '',
        component: LovTypeComponent
      },
    ]
  },
  {
    path: 'lovtypelist',
    children: [
      {
        path: 'list',
        component: LovTypeListComponent
      },
    ]
  },
  {
    path: 'lottype',
    children: [
      {
        path: ':id',
        component: LotTypeComponent
      },
      {
        path: '',
        component: LotTypeComponent
      }
    ]
  },
  {
    path: 'materialgroup',
    children: [
      {
        path: ':id',
        component: MaterialGroupComponent
      },
      {
        path: '',
        component: MaterialGroupComponent
      }
    ]
  },
  // {
  //   path: 'cycleclass',
  //   children: [
  //     {
  //       path: ':id',
  //       component: CycleClassComponent
  //     },
  //     {
  //       path: '',
  //       component: CycleClassComponent
  //     }
  //   ]
  // },
  {
    path: 'processoperation',
    children: [
      {
        path: ':id',
        component: ProcessOperationComponent
      },
      {
        path: '',
        component: ProcessOperationComponent
      }
    ]
  },
  {
    path: 'lov',
    children: [
      {
        path: ':id',
        component: LovComponent
      },
      {
        path: '',
        component: LovComponent
      }
    ]
  },
  {
    path: 'stockposition',
    children: [
      {
        path: 'list',
        component: stockpositionComponent
      },
    ]
  },
  {
    path: 'bankbranch',
    children: [
      {
        path: 'list',
        component: BankbranchlistComponent
      }
    ]
  },
  {
    path: 'jobdefine',
    children: [
      {
        path: 'list',
        component: JobdefinelistComponent
      }
    ]
  },
  {
    path: 'costcenter',
    children: [
      {
        path: 'list',
        component: CostcenterlistComponent
      }
    ]
  },
  {
    path: 'termsset',
    children: [
      {
        path: 'list',
        component: TermssetlistComponent
      }
    ]
  },
  {
    path: 'accountschedulelist',
    children: [
      {
        path: 'list',
        component: AccountSchedulelistComponent
      }
    ]
  },
  {
    path: 'itemwise',
    children: [
      {
        path: 'list',
        component: itemwiseComponent
      }
    ]
  },
  {
    path: 'stockpositiondetail',
    children: [
      {
        path: 'list',
        component: StockPositionDetailComponent
      }
    ]
  },
  {
    path: 'RegisterWOItem',
    children: [
      {
        path: 'list',
        component: WOReportComponent
      }
    ]
  },
  {
    path: 'stockledgerdetail',
    children: [
      {
        path: 'list',
        component: StockLedgerDetailComponent
      }
    ]
  },
  {
    path: 'stockageingdetail',
    children: [
      {
        path: 'list',
        component: StockAgeingDetailComponent
      }
    ]
  },
  { path: 'themestest', loadChildren: () => import(`../../themes/themes.module`).then(m => m.ThemesModule) },
  {
    path: 'BomTree',
    children: [
      {
        path: 'list',
        component: BomTreeComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InventoryRoutingModule { }
