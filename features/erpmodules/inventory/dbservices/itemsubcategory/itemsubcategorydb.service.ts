import { Injectable } from '@angular/core';

import { GBHttpService } from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { ISelectListCriteria } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
import { GBBaseDBDataService } from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service';
import { Observable } from 'rxjs';

import { IItemSubCategory } from '../../models/Iitemsubcategory';
const urls = require('../../URLS/urls.json');

@Injectable({
  providedIn: 'root',
})
export class ItemsubcategoryDBService {
  endPoint: string = urls.ITEMSUBCATEGORY;
  idField: string = "?ItemSubCategoryId="

  constructor(public http: GBHttpService) {
  }


  getall(id) {
    const url = this.endPoint + 'SelectList';
    let criteria = {
      "SectionCriteriaList": [
        {
          "SectionId": 0,
          "AttributesCriteriaList": [
            {
              "FieldName": "ItemSubCategoryItemCategory.Id",
              "OperationType": 5,
              "FieldValue": id,
              "InArray": null,
              "JoinType": 2
            },
            {
              "FieldName": "Code",
              "OperationType": 2,
              "FieldValue": "",
              "InArray": null,
              "JoinType": 0
            }
          ],
          "OperationType": 0
        }
      ]
    }
    return this.http.httppost(url, criteria);
  }


  getid(id) {
    const url = this.endPoint + this.idField + id;
    return this.http.httpget(url);
  }

  // putData(data: IItemSubCategory) {
  //   return super.putData(data);
  // }

  // postData(data: IItemSubCategory) {
  //   return super.postData(data);
  // }

  // getData(idField: string, id: string): Observable<IItemSubCategory> {
  //   return super.getData(idField, id);
  // }

  // deleteData(idField: string, id: string) {
  //   return super.deleteData(idField, id)
  // }

  // getList(scl: ISelectListCriteria): Observable<any> {
  //   return super.getList(scl);
  // }
}
