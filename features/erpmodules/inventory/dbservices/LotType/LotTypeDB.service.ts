import { Injectable } from '@angular/core';
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {ISelectListCriteria } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
import {GBBaseDBDataService} from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service';
import { Observable } from 'rxjs';

import { LotType} from '../../models/ILotType';
const urls = require('../../URLS/urls.json');

@Injectable({
  providedIn: 'root',
})
export class LotTypeDBService{
  endPoint: string = urls.LotType;
  idField:string = "?LotTypeId="
  constructor(public http: GBHttpService) {
  }

  getall(){
    const url = this.endPoint + 'SelectList';
    let criteria = {"SectionCriteriaList":[{"SectionId":0,"AttributesCriteriaList":[{"FieldName":"Name","OperationType":2,"FieldValue":"","InArray":null,"JoinType":0}],"OperationType":0}]}
    return this.http.httppost(url,criteria);
  }


  getid(id){
    const url = this.endPoint + this.idField + id;
    return this.http.httpget(url);
  }
  // putData(data: IUOM) {
  //   return super.putData(data);
  // }

  // postData(data: IUOM) {
  //   return super.postData(data);
  // }

  // getData(idField: string, id: string): Observable<IUOM> {
  //   return super.getData(idField, id);
  // }

  // deleteData(idField: string, id: string) {
  //   return super.deleteData(idField, id)
  // }

  // getList(scl: ISelectListCriteria): Observable<any> {
  //   return super.getList(scl);
  // }
}
