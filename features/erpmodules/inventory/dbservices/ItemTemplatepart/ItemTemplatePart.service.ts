import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IItemTemplatePart } from '../../models/IItemTempleatePart';





 
@Injectable({
  providedIn: 'root'
})
export class IItemTemplatePartService extends GBBaseDataServiceWN<IItemTemplatePart> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IItemTemplatePart>) {
    super(dbDataService, 'ItemTemplatePartId');
  }
}
