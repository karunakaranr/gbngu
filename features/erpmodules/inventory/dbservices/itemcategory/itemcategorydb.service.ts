import { Injectable } from '@angular/core';
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {ISelectListCriteria } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
import {GBBaseDBDataService} from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service';

import { Observable } from 'rxjs';

import { IItemCategory} from '../../models/Iitemcategory';
const urls = require('../../URLS/urls.json');

@Injectable({
  providedIn: 'root',
})
export class ItemcategoryDBService{
  endPoint: string = urls.ITEMCATEGORY;
  idField:string = "?ItemCategoryId="

  constructor(public http: GBHttpService) {
  }

  getall(){
    const url = this.endPoint + 'SelectList';
    let criteria = {"SectionCriteriaList":[{"SectionId":0,"AttributesCriteriaList":[{"FieldName":"Code","OperationType":2,"FieldValue":"","InArray":null,"JoinType":1},{"FieldName":"Name","OperationType":2,"FieldValue":"","InArray":null,"JoinType":0}],"OperationType":0}]}
    return this.http.httppost(url,criteria);
  }


  getid(id){
    const url = this.endPoint + this.idField + id;
    return this.http.httpget(url);
  }

  // putData(data: IItemCategory) {
  //   return super.putData(data);
  // }

  // postData(data: IItemCategory) {
  //   return super.postData(data);
  // }

  // getData(idField: string, id: string): Observable<IItemCategory> {
  //   return super.getData(idField, id);
  // }

  // deleteData(idField: string, id: string) {
  //   return super.deleteData(idField, id)
  // }

  // getList(scl: ISelectListCriteria): Observable<any> {
  //   return super.getList(scl);
  // }
}
