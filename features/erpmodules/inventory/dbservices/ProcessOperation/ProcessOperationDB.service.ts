import { Injectable } from '@angular/core';
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {ISelectListCriteria } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
import {GBBaseDBDataService} from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ProcessOperation} from '../../models/IProcessOperation';
const urls = require('../../URLS/urls.json');

@Injectable({
  providedIn: 'root',
})
export class ProcessOperationDBService extends GBBaseDBDataService<ProcessOperation> {
  endPoint: string = urls.ProcessOperation;
  
  constructor(http: GBHttpService) {
    
    super(http);
  }

  putData(data: ProcessOperation) {
    return super.putData(data);
  }

  postData(data: ProcessOperation) {
    return super.postData(data);
  }

  getData(idField: string, id: string): Observable<ProcessOperation> {
    
    return super.getData(idField, id);
  }

  deleteData(idField: string, id: string) {
    return super.deleteData(idField, id)
  }

  getList(scl: ISelectListCriteria): Observable<any> {
    return super.getList(scl);
  }
}
