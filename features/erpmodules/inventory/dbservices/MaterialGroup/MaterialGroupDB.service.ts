import { Injectable } from '@angular/core';
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';

const urls = require('../../URLS/urls.json');
@Injectable({
  providedIn: 'root',
})
export class MaterialGroupDBService{
  endPoint: string = urls.MaterialGroup;
  idField:string = "?MaterialGroupId="
  constructor(public http: GBHttpService) {
  }

  getall(){
    const url = this.endPoint + 'SelectList';
    let criteria = {"SectionCriteriaList":[{"SectionId":0,"AttributesCriteriaList":[{"FieldName":"Code","OperationType":2,"FieldValue":"","InArray":null,"JoinType":1},{"FieldName":"Name","OperationType":2,"FieldValue":"","InArray":null,"JoinType":0}],"OperationType":0}]}
    return this.http.httppost(url,criteria);
  }

  getid(id){
    const url = this.endPoint + this.idField + id;
    return this.http.httpget(url);
  }

}
