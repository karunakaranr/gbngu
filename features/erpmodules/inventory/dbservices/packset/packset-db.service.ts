import { Injectable } from '@angular/core';

import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {ISelectListCriteria } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
import {GBBaseDBDataService} from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service';
import { Observable } from 'rxjs';

import { Ipackset } from '../../models/Ipackset';
const urls = require('../../URLS/urls.json');

@Injectable({
  providedIn: 'root',
})
export class PacksetDBService extends GBBaseDBDataService<Ipackset> {
  endPoint: string = urls.Packset;


  constructor(http: GBHttpService) {
    super(http);
  }

  putData(data: Ipackset) {
    return super.putData(data);
  }

  postData(data: Ipackset) {
    return super.postData(data);
  }

  getData(idField: string, id: string): Observable<Ipackset> {
    return super.getData(idField, id);
  }

  deleteData(idField: string, id: string) {
    return super.deleteData(idField, id)
  }

  getList(scl: ISelectListCriteria): Observable<any> {
    return super.getList(scl);
  }
}
