import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UicoreModule } from '@goodbooks/uicore';
import { UifwkUifwkmaterialModule } from '@goodbooks/uifwk/material';
import { NgxsModule } from '@ngxs/store';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { TranslocoRootModule } from '../../transloco/transloco-root.module';

import { NgBreModule } from '@unisoft/ng-bre';
import { NgSelectModule } from '@ng-select/ng-select';
import { EntitylookupModule } from '../../entitylookup/entitylookup.module';
import { AgGridModule } from 'ag-grid-angular';


import { GbgridModule } from 'features/gbgrid/components/gbgrid/gbgrid.module';
import { InventoryComponent } from './inventory.component';
import { PackComponent } from './screens/Pack/Pack/pack.component';
import { PacklistComponent } from './screens/Pack/Packlist/packlist.component';
import { InventoryRoutingModule } from './inventory-routing.module';
import { PackState } from './stores/Pack/pack.state';
// import { BrandComponent } from './screens/brand/brand/brand.component';
import { BrandComponent } from './screens/brand/brand/brand.component';
import { BrandlistComponent } from './screens/brand/brandlist/brandlist.component';
// import { ItemcategoryComponent } from './screens/itemcategory/itemcategory/itemcategory.component';
import { ItemCategorylistComponent } from './screens/itemcategory/itemcategorylist/itemcategorylist.component';
import { ItemSubCategoryComponent } from './screens/itemsubcategory/itemsubcategory/itemsubcategory.component';
import { ItemSubCategorylistComponent } from './screens/itemsubcategory/itemsubcategorylist/itemsubcategorylist.component';
import { PacksetComponent } from './screens/packset/packset/packset.component';
import { PacksetlistComponent } from './screens/packset/packsetlist/packsetlist.component';
import { UOMComponent } from './screens/UOM/UOM/uom.component';
import { UOMlistComponent } from './screens/UOM/UOMList/uomlist.component';
import { UOMState } from './stores/UOM/UOM.state';
import { PartyListViewComponent } from './screens/PartyList/PartyListView/PartyListView.component';
import { PartyBranchListComponent } from  './screens/PartyBranch/PartyBranchList/PartyBranchList.component';
import { UserListComponent } from  './screens/User/UserList/UserList.component';
import { ItemStockListComponent } from './screens/ItemStock/ItemStockList/ItemStockList.component';
import { ItemReportComponent } from './screens/Item/ItemReport/ItemReportList.component';
import { ItemBrandReportComponent } from './screens/brand/BrandListReport/BrandListReport.component';
import { ItemGroupListComponent } from './screens/ItemGroup/ItemGroupList/ItemGroupList.component';
import { stockpositionComponent } from './screens/StockPositionSummary/StockPositionSummary/StockPositionSummary.component';
import { ItemOUListViewComponent} from './screens/ItemOUList/ItemOUListView/ItemOUList.component';
import { ItemPackComponent } from './screens/ItemPack/ItemPack.component'
import { BankbranchlistComponent } from './screens/bankbranch/bankbranchlist/bankbranchlist.component';
import { JobdefinelistComponent } from './screens/Jobdefine/jobdefinelist/jobdefinelist.component';
import { CostcenterlistComponent } from './screens/costcenter/costcenterlist/costcenterlist.component';
import { TermssetlistComponent } from './screens/TermsSet/TermsSetList/TermsSetList.component';
import { AccountSchedulelistComponent } from './screens/AccountScheduleList/AccountScheduleList/AccountScheduleList.component';
import { MaterialAccountComponent } from './screens/MaterialAccount/MaterialAccount.component';
import { LovComponent } from './screens/Lov/Lov.component';
import { ProcessOperationComponent } from './screens/ProcessOperation/ProcessOperation.component';
import { MaterialGroupComponent } from './screens/MaterialGroup/MaterialGroup.component';
import { CycleClassComponent } from './screens/CycleClass/CycleClass.component';
import { LotTypeComponent } from './screens/LotType/LotType.component';
import { LovTypeListComponent } from './screens/LovType/LovTypeList/LovTypeList.component';
import { itempartylistComponent } from './screens/ItemParty/ItemPartyList/itempartylist.component';
import { partyitemlistComponent } from './screens/PartyItem/PartyItemList/partyitemlist.component';
import { SamplescreensModule } from '../samplescreens/samplescreens.module';
import { itemwiseComponent } from './screens/ItemWise/itemWise/itemwise.component';
import { ParameterComponent } from './screens/Parameter/parameter.component';
import { StockPositionDetailComponent } from './screens/StockPositionDetail/StockPositionDetail/StockPositionDetail.component';
import { WOReportComponent } from './screens/RegisterWithoutItem/WOItemReports/WOReport.component';
import { StockLedgerDetailComponent } from './screens/Stock Ledger/Stock Ledger detail/StockLedgerDetail.component';
import { pricecategoryComponent } from './screens/PriceCategory/pricecategory.component';
import { StocklegersummaryComponent } from './screens/Stock Ledger/Stock Ledger Summary/stocklegersummary.component';
import { DatewiseminComponent } from './screens/Register/DateWise/Datewise Min and Inward/Datewisemin.component';
import { StockageingsummaryComponent } from './screens/Stock Ageing/Stock Ageing Summary/Stockageingsummary.component';
import { StockAgeingDetailComponent } from './screens/Stock Ageing/Stock Ageing Detail/StockAgeingDetail.component';
// import { LovTypeComponent } from './screens/LovType/LovType/LovType.component';
import { LovTypeComponent } from './screens/LovType/LovType/LovType.component';
import { ItemCategoryComponent } from './screens/itemcategory/itemcategory/itemcategory.component';
import { ProcessGroupComponent } from './screens/ProcessGroup/ProcessGroup.component';
import { ParametermapComponent } from './screens/Parametermap/parametermap.component';
import { SKUSettingComponent } from './screens/SKUSetting/skusetting.component';
import { UOMSetComponent } from './screens/UOMSet/uomset.component';
import { SupplyGroupComponent } from './screens/SupplyGroup/supplygroup.component';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { ItemMasterComponent } from './screens/ItemMaster/ItemMaster.component';
import { DyeComponent } from './screens/Dye/Dye.component';
import { TermComponent } from './screens/Terms/term.component';
import { MachinesComponent } from './screens/Machine/Machine.Component';
import { AssemblyBOMComponent } from './screens/AssemblyBOM/assemblybom.component';
import { ItemOUComponent } from './screens/ItemOU/itemou.component';
import { TermssheetComponent } from './screens/Tremssheet/Termssheet.component';
import { MRPPartyTypeComponent } from './screens/MRPPartyType/mrppartytype.component';
import { StoreComponent } from './screens/Store/StoreList/Store/Store.component';
import { ItemTemplatePartComponent } from './screens/ItemTemplatePart/ItemTemplatePart.component';
import { SimpleItemComponent } from './screens/SimpleItem/simpleitem.component';
import { ModelComponent } from './screens/Model/Model.component';
import { PrintedLabelComponent } from './screens/PrintedLabel/printedlabel.component';
import { MasterScheduleComponent } from './screens/MasterSchedule/masterschedule.component';
import { BomTreeComponent } from './screens/BomTree/BomTree.component';
@NgModule({
  declarations: [
    ItemMasterComponent,
    PrintedLabelComponent,
    SimpleItemComponent,
    ItemTemplatePartComponent,
    MRPPartyTypeComponent,
    AssemblyBOMComponent,
    StoreComponent,
    TermssheetComponent,
    TermComponent,
    DyeComponent,
    ParameterComponent,
    UOMSetComponent,
    SupplyGroupComponent,
    ItemOUComponent,
    ParametermapComponent,
    MaterialGroupComponent,
    SKUSettingComponent,
    MasterScheduleComponent,
    ProcessGroupComponent,
    StocklegersummaryComponent,
    DatewiseminComponent,
    StockageingsummaryComponent,
    pricecategoryComponent,
    partyitemlistComponent,
    InventoryComponent,
    PackComponent,
    PacklistComponent,
    PacksetComponent,
    PacksetlistComponent,
    UOMComponent,
    UOMlistComponent,
    LovTypeComponent,

    // LovTypeComponent,
    CycleClassComponent,
    LotTypeComponent,
    ProcessOperationComponent,
    LovComponent,
    BrandComponent,
    BrandlistComponent,
    // ItemcategoryComponent,
    ItemCategoryComponent,
    ItemCategorylistComponent,
    MaterialAccountComponent,
    ItemSubCategoryComponent,
    ItemSubCategorylistComponent,
    ModelComponent,
    // ModellistComponent,
    PartyListViewComponent,
    ItemOUListViewComponent,
    ItemPackComponent,
    PartyBranchListComponent,
    UserListComponent,
    ItemStockListComponent,
    ItemReportComponent,
    ItemBrandReportComponent,
    ItemGroupListComponent,
    stockpositionComponent,
    BankbranchlistComponent,
    JobdefinelistComponent,
    CostcenterlistComponent,
    TermssetlistComponent,
    AccountSchedulelistComponent,
    LovTypeListComponent,
    itempartylistComponent,
    itemwiseComponent,
    StockPositionDetailComponent,
    WOReportComponent,
    StockLedgerDetailComponent,
    StockAgeingDetailComponent,
    UOMComponent,
    MachinesComponent,
    BomTreeComponent
  ],
  imports: [
    SamplescreensModule,
    CommonModule,
    InventoryRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    UicoreModule,
    NgBreModule,
    UifwkUifwkmaterialModule,
    TranslocoRootModule,
    NgxsModule.forFeature([]),
    NgxsFormPluginModule.forRoot(),
    AgGridModule,
    GbgridModule,
    EntitylookupModule,
    FormlyModule,
    FormlyMaterialModule,
    MatIconModule,
    MatButtonModule,
    NgSelectModule

  ],
  exports: [
    InventoryComponent,PacklistComponent,LovTypeComponent
  ],
})
export class InventoryModule {
  constructor() {

  }


}
