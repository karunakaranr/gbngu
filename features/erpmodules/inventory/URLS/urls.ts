export class InventoryURLS {
  public static savepack = '/api/mms/Pack.svc';
  public static deletepack = '/api/mms/Pack.svc/?PackId=';
  public static packlist = '/api/mms/Pack.svc/SelectList';
  public static packfill = '/api/mms/Pack.svc/?PackId=';
  public static savemodel = '/prox/mms/ItemModel.svc';
  public static deletemodel = '/prox/mms/ItemModel.svc/?ModelId=';
  public static modellist  = '/prox/mms/ItemModel.svc/SelectList';
  public static modelfill  = '/prox/mms/ItemModel.svc/?ModelId=';
  public static brandlist = '/prox/mms/ItemBrand.svc/SelectList';
  public static MarginSummary  = '/api/mms/Register.svc/MarginSummary/?Type=4&FirstNumber=1&MaxResult=10';
  public static savepermission = 'api/ads/OfflineSync.svc/OffLineSynchRequest/';
  public static packsetlist = '/api/mms/PackSet.svc/SelectList';
  public static packsetfill = '/api/mms/PackSet.svc/?PackSetId=';
  public static packsetsave = '/api/mms/PackSet.svc/';
  public static packsetdelete='api/mms/PackSet.svc/?PackSetId='
  public static saveemployee = '/api/prs/Employee.svc';
  public static Packset = '/mms/PackSet.svc/';
  public static UOMSet = '/fws/UOMSet.svc/';
  public static LovType = '/fws/LovType.svc/';
  public static ITEMCATEGORY = '/mms/ItemCategory.svc/';
  public static LotType = '/mms/LotType.svc/';
  public static ProcessOperation = "/mms/ProcessOperation.svc/";
  public static ProcessGroup ='/mms/MProcessGroup.svc/';
  public static MaterialGroup = '/mms/MaterialGroup.svc/';
  public static Parameter ='/fws/Parameter.svc/';
  public static Brand = '/mms/ItemBrand.svc/';
  public static UOM = '/fws/UOM.svc/';
  public static ItemMaster = '/mms/Item.svc/';
  public static CycleClass = "/mms/CycleClass.svc/";
  public static ItemSubCategory = "/mms/ItemSubCategory.svc/";
  public static Diecut = "/mms/Diecut.svc/";
  public static GCM = "/fws/GCM.svc/SelectList/";
  public static DieMasterParty = '/as/Party.svc/';
  public static Term = '/mms/Terms.svc/';
  public static ItemOU = '/cs/Criteria.svc/List/?ObjectCode=ITEMOU';
  public static ItemOUsave = '/mms/ItemOU.svc/';
  public static SKUSetting = '/mms/SKUSetting.svc/';
  public static SupplyGroup = '/mms/SupplyGroup.svc/';
  public static Lov = '/fws/Lov.svc/';
  public static Machines = '/mms/Machine.svc/';
  public static AssemblyBOM = '/mms/BOM.svc/';
  public static Termssheet ="/mms/TermsSet.svc/";
  public static MRPPartyType = '/mms/MRPPartyType.svc/';
  public static Store = "/mms/Store.svc/";
  public static ItemTemplatepart ="/mms/ItemTemplatePart.svc/";
  public static SimpleItem = '/mms/Item.svc/'
  public static MODEL = '/mms/ItemModel.svc/'
  public static Label = ' /mms/Label.svc/';
  public static MasterSchedule = '/mms/MasterSchedule.svc/';
  
} 
