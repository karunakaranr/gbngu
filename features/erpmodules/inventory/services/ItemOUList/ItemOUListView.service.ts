import { Injectable } from '@angular/core';
import { ItemOUListdbservice } from './../../dbservices/ItemOUList/itemoulistviewdb.service';
@Injectable({
  providedIn: 'root'
})
export class ItemOUListView {
  constructor(public dbservice: ItemOUListdbservice) { }

  public itemoulistview(){                      
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                    
    return this.dbservice.reportData(obj);
  }
}

