import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { ISimpleItem } from '../../models/ISimpleItem';



@Injectable({
  providedIn: 'root'
})
export class SimpleItemService extends GBBaseDataServiceWN<ISimpleItem> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ISimpleItem>) {
    super(dbDataService, 'ItemId');
  }
}
