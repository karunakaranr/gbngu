import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { ILov } from '../../models/ILov';


@Injectable({
  providedIn: 'root'
})
export class LovService extends GBBaseDataServiceWN<ILov> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ILov>) {
    super(dbDataService, 'LovId');
  }
}