import { Injectable } from '@angular/core';
import { ItemStockdbservice } from './../../dbservices/ItemStock/ItemStockdb.service';

@Injectable({
  providedIn: 'root'
})
export class ItemStockService {
  constructor(public dbservice: ItemStockdbservice) { }

  public GetitemstockList(){                      
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                    
    return this.dbservice.reportData(obj);
  }
}