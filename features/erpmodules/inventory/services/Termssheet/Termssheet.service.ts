import { Inject, Injectable } from '@angular/core';
// import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { ITermssheet } from '../../models/ITermssheet';
 
@Injectable({
  providedIn: 'root'
})
export class TermssheetService extends GBBaseDataServiceWN<ITermssheet> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ITermssheet>) {
    super(dbDataService, 'TermsSetId');
  }
}
