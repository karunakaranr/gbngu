import { Injectable } from "@angular/core";
import { BankbranchdbService } from "../../dbservices/bankbranch/bankbranchdb.service";

@Injectable({
    providedIn: 'root'
})
export class BankbranchService {

    constructor(
        private bankbranchdbService: BankbranchdbService
    ){ }

    public getBankBranchList() {                      
        return this.bankbranchdbService.picklist();
    }

    public rowService(obj){                   
        return this.bankbranchdbService.reportData(obj);
    }
}