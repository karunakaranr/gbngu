import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IParametermap } from '../../models/IParametermap'; 


 
@Injectable({
  providedIn: 'root'
})
export class ParametermapService extends GBBaseDataServiceWN<IParametermap> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IParametermap>) {
    super(dbDataService, 'ParameterMapId');
  }
}
