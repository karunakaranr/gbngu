import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IMachine } from '../../models/IMachine';





 
@Injectable({
  providedIn: 'root'
})
export class MachinesService extends GBBaseDataServiceWN<IMachine> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IMachine>) {
    super(dbDataService, 'MachineId');
  }
}
