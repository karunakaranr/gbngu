import { Injectable } from '@angular/core';
import { PartyListviewdbservice } from './../../dbservices/PartyList/partylistviewdb.service';
@Injectable({
  providedIn: 'root'
})
export class PartyListService {
  constructor(public dbservice: PartyListviewdbservice) { }

  public Getpartylistview(){                     
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                    
    return this.dbservice.reportData(obj);
  }
}