import { Injectable } from "@angular/core";
import { AccountScheduledbService } from "../../dbservices/AccountScheduleList/AccountScheduleListdb.service";

@Injectable({
    providedIn: 'root'
})
export class AccountScheduleService {

    constructor(
        private accountScheduledbService: AccountScheduledbService
    ){ }

    public getAccountScheduleList() {                      
        return this.accountScheduledbService.picklist();
    }

    public rowService(obj){                   
        return this.accountScheduledbService.reportData(obj);
    }
}
