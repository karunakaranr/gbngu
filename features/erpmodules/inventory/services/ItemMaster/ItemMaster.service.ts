import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IItemMaster } from '../../models/IItemMaster';


 
@Injectable({
  providedIn: 'root'
})
export class ItemMasterService extends GBBaseDataServiceWN<IItemMaster> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IItemMaster>) {
    super(dbDataService, 'ItemId');
  }
}
