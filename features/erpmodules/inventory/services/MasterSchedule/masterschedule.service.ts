import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IMasterSchedule } from 'features/erpmodules/planning/models/IMasterSchedule';

@Injectable({
  providedIn: 'root'
})
export class MasterScheduleService extends GBBaseDataServiceWN<IMasterSchedule> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IMasterSchedule>) {
    super(dbDataService, 'MasterScheduleId');
  }
}