import { Inject, Injectable } from "@angular/core";
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';


import { IProcessOperation } from '../../models/IProcessOperation'


@Injectable({
    providedIn: 'root'
  })
  export class ProcessOperationService extends GBBaseDataServiceWN<IProcessOperation> {
    constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IProcessOperation>) {
      super(dbDataService, 'ProcessOperationId');
    }
  }