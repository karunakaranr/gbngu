import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';
// import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IUOM } from '../../models/IUOM';


 
@Injectable({
  providedIn: 'root'
})
export class UOMService extends GBBaseDataServiceWN<IUOM> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IUOM>) {
    super(dbDataService, 'UOMId');
  }
}
