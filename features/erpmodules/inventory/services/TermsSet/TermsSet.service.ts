import { Injectable } from "@angular/core";
import { TermsSetdbService } from "../../dbservices/TermsSet/TermsSetdb.service";

@Injectable({
    providedIn: 'root'
})
export class  TermsSetService {

    constructor(
        private termsSetdbService: TermsSetdbService
    ){ }

    public getTermsSetList() {                      
        return this.termsSetdbService.picklist();
    }

    public rowService(obj){                    
        return this.termsSetdbService.reportData(obj);
    }
}
