import { Injectable } from '@angular/core';
import { UserListdbservice } from './../../dbservices/User/UserListdb.service';
@Injectable({
  providedIn: 'root'
})
export class UserListService {
  constructor(public dbservice: UserListdbservice) { }

  public Reportdetailservice(){                     
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                    
    return this.dbservice.reportData(obj);
  }
}``