import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

// import{IPrintedLabel} from './../models/IPrintedLabel'
import { IPrintedLabel } from '../../models/IPrintedLabel';
@Injectable({
  providedIn: 'root'
})
export class PrintedLabelService extends GBBaseDataServiceWN<IPrintedLabel> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPrintedLabel>) {
    super(dbDataService, 'LabelId');
  }
}