import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IAssemblyBOM } from '../../models/IAssemblyBOM';




 
@Injectable({
  providedIn: 'root'
})
export class AssemblyBOMService extends GBBaseDataServiceWN<IAssemblyBOM> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IAssemblyBOM>) {
    super(dbDataService, 'BOMId');
  }
}
