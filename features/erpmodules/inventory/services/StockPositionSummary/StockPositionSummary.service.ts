import { Injectable } from '@angular/core';
import { StockPositionSummarydbservice } from './../../dbservices/StockPositionSummary/StockPositionSummarydb.service';
@Injectable({
  providedIn: 'root'
})
export class StockPositionSummaryService {
  constructor(public dbservice: StockPositionSummarydbservice) { }

  public Reportdetailservice(){                    
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                 
    return this.dbservice.reportData(obj);
  }
}``