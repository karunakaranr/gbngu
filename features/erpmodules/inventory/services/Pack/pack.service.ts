import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GBBaseDataServiceWN} from './../../../../../libs/gbdata/src/lib/services/gbbasedata.service';

import { IPack } from './../../models/IPack';
import { PackDBService } from '../../dbservices/Pack/packdb.service';


@Injectable({
  providedIn: 'root'
})
export class PackService extends GBBaseDataServiceWN<IPack> {
  constructor(public dbDataService: PackDBService) {
    super(dbDataService, 'PackId');
  }

  saveData(data: IPack): Observable<any> {
    return super.saveData(data);
  }

  getData(id: string): Observable<IPack> {
    return super.getData(id);
  }
  deleteData(id: any): Observable<any> {
    return super.deleteData(id);
  }
  getAll(): Observable<any> {
    return super.getAll();
  }

  moveFirst(): Observable<IPack> {
    return super.moveFirst();
  }

  movePrev(): Observable<IPack> {
    return super.movePrev();
  }

  moveNext(): Observable<IPack> {
    return super.moveNext();
  }

  moveLast(): Observable<IPack> {
    return super.moveLast();
  }
}
