import { Injectable } from "@angular/core";
import { LovTypeListDbService } from "../../../dbservices/LovType/LovTypeList/LovTypeListDB.service";

@Injectable({
    providedIn: 'root'
})
export class LovTypeList {

    constructor(
        private lovtypelistdbService: LovTypeListDbService
    ){ }

    public lovtypelistview() {                      
        return this.lovtypelistdbService.picklist();
    }

    public Rowservice(obj){                    
        return this.lovtypelistdbService.reportData(obj);
    }
}
