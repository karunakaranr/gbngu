import { Injectable } from '@angular/core';
import { ItemBrandListdbservice } from './../../dbservices/brand/brandlistreportdb.service';
@Injectable({
  providedIn: 'root'
})
export class ItemBrandListService {
  constructor(public dbservice: ItemBrandListdbservice) { }

  public Reportdetailservice(){                     
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                    
    return this.dbservice.reportData(obj);
  }
}``