import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


// import { ModelName } from ‘ModelPath’;
import { IBrand } from '../../models/IBrand';

 
@Injectable({
  providedIn: 'root'
})
export class BrandService extends GBBaseDataServiceWN<IBrand> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IBrand>) {
    super(dbDataService, 'ItemBrandId');
  }
}
