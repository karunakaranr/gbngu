import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IModel } from '../../models/IModel';


 
@Injectable({
  providedIn: 'root'
})
export class ModelService extends GBBaseDataServiceWN<IModel> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IModel>) {
    super(dbDataService, 'ModelId');
  }
}
