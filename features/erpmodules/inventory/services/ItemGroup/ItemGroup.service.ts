import { Injectable } from '@angular/core';
import { ItemGroupdbservice } from './../../dbservices/ItemGroup/ItemGroupdb.service';

@Injectable({
  providedIn: 'root'
})
export class ItemGroupService {
  constructor(public dbservice: ItemGroupdbservice) { }

  public Getitemgrouplist(){                      
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                   
    return this.dbservice.reportData(obj);
  }
}