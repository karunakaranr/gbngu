import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IDye } from '../../models/IDye';


 
@Injectable({
  providedIn: 'root'
})
export class DyeService extends GBBaseDataServiceWN<IDye> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IDye>) {
    super(dbDataService, 'DiecutId');
  }
}
