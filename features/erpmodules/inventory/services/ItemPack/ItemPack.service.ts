import { Injectable } from '@angular/core';
import { ItemPackdbservice } from './../../dbservices/ItemPack/ItemPackdb.service';
@Injectable({
  providedIn: 'root'
})
export class ItemPackView {
  constructor(public dbservice: ItemPackdbservice) { }

  public itempackview(){                     
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                    
    return this.dbservice.reportData(obj);
  }
}

