import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IMaterialGroup } from '../../models/IMaterialGroup'; 

 
@Injectable({
  providedIn: 'root'
})
export class MaterialGroupService extends GBBaseDataServiceWN<IMaterialGroup> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IMaterialGroup>) {
    super(dbDataService, 'MaterialGroupId');
  }
}

