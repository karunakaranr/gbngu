import { Injectable } from "@angular/core";
import { CostcenterdbService } from "../../dbservices/costcenter/costcenterdb.service";

@Injectable({
    providedIn: 'root'
})
export class  CostcenterService {

    constructor(
        private costcenterdbService: CostcenterdbService
    ){ }

    public getCostcenterList() {                      
        return this.costcenterdbService.picklist();
    }

    public rowService(obj){                   
        return this.costcenterdbService.reportData(obj);
    }
}
