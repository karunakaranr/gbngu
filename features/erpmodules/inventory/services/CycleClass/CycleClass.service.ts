import { Inject, Injectable } from '@angular/core';

import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { ICycleClass } from '../../models/ICycleClass';



@Injectable({
  providedIn: 'root'
})
export class CycleClassService extends GBBaseDataServiceWN<ICycleClass> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICycleClass>) {
    super(dbDataService, 'CycleClassId');
  }
}
