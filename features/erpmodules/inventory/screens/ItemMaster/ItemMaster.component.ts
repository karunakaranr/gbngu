import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ItemMasterService } from '../../services/ItemMaster/ItemMaster.service';
import { IItemMaster } from '../../models/IItemMaster';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as ItemMasterJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ItemMaster.json'
import { InventoryURLS } from '../../URLS/urls';

@Component({
  selector: "app-ItemMaster",
  templateUrl: "./ItemMaster.component.html",
  styleUrls: ["./ItemMaster.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'FormId'},
    { provide: 'url', useValue: InventoryURLS.ItemMaster },
    { provide: 'DataService', useClass: ItemMasterService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ItemMasterComponent extends GBBaseDataPageComponentWN<IItemMaster> {
  title : string = "ItemMaster"
  ItemMasterJSON =  ItemMasterJSON
  form: GBDataFormGroupWN<IItemMaster> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "ItemMaster", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
      this.ItemFillData(arrayOfValues.FormId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }

  public ItemFillData(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(FormVariable => {
          this.form.patchValue(FormVariable);
    })
    }
  }

  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }

  public ChangeTab(event, TabName:string) :void{
    var i:number, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("FormTabLinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(TabName).style.display = "block";
    console.log("Event:",event)
    event.currentTarget.className += " active";
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IItemMaster> {
  const dbds: GBBaseDBDataService<IItemMaster> = new GBBaseDBDataService<IItemMaster>(http);
  dbds.endPoint = url;
  dbds.CopyFrom = ['ItemName','ItemCode','ItemShortName'];
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IItemMaster>, dbDataService: GBBaseDBDataService<IItemMaster>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IItemMaster> {
  return new GBDataPageService<IItemMaster>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
