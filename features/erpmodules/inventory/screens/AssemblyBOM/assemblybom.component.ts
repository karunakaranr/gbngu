import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { InventoryURLS } from '../../URLS/urls';
import { AssemblyBOMService } from '../../services/AssemblyBOM/assemblybom.service';
import { IAssemblyBOM } from '../../models/IAssemblyBOM';
import * as AssemblyBOMJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/AssemblyBOM.json'
@Component({
  selector: 'app-AssemblyBOM',
  templateUrl: './assemblybom.component.html',
  styleUrls: ['./assemblybom.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'BOMId'},
    { provide: 'url', useValue: InventoryURLS.AssemblyBOM },
    { provide: 'DataService', useClass: AssemblyBOMService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class AssemblyBOMComponent extends GBBaseDataPageComponentWN<IAssemblyBOM > {
  title: string = 'AssemblyBOM'
  AssemblyBOMJSON = AssemblyBOMJSON;


  form: GBDataFormGroupWN<IAssemblyBOM > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'AssemblyBOM', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.AssemblyBOMRetrivalValue(arrayOfValues.BOMId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public AssemblyBOMRetrivalValue (SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(AssemblyBOM => {
        this.form.patchValue(AssemblyBOM);
      })
    }
  }


  public AssemblyBOMPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IAssemblyBOM > {
  const dbds: GBBaseDBDataService<IAssemblyBOM > = new GBBaseDBDataService<IAssemblyBOM >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IAssemblyBOM >, dbDataService: GBBaseDBDataService<IAssemblyBOM >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IAssemblyBOM > {
  return new GBDataPageService<IAssemblyBOM >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
