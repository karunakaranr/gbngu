import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';


@Component({
  selector: 'app-pricecategory',
  templateUrl: './pricecategory.component.html',
  styleUrls: ['./pricecategory.component.scss'],
})
export class pricecategoryComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];  
  
  constructor(public store: Store) { }
  ngOnInit(): void {
    this.pricecategoryfun();
  }
  
  public pricecategoryfun() {
    this.rowdatacommon$.subscribe(data => {
      this.rowData = data;
      console.log("Price Category :", this.rowData[0].PartyPriceCategoryDetailArray)
    });
  }

}
