import { Component, OnInit } from "@angular/core";
import { TermsSetService } from "features/erpmodules/inventory/services/TermsSet/TermsSet.service";

@Component({
  selector: 'app-TermsSetList',
  templateUrl: './TermsSetList.component.html',
  styleUrls: ['./TermsSetList.component.scss']
})
export class TermssetlistComponent implements OnInit {

    termsList: any[] = [];

    constructor(
        private termssetService: TermsSetService
    ) { }

    ngOnInit() {
        this.getTermsSetList();
    }

    getTermsSetList(): void{
        this.termssetService.getTermsSetList().subscribe(menuDetails => {
            const menuDet = menuDetails;
            this.termssetService.rowService(menuDet).subscribe(res => {
                this.termsList = res.ReportDetail;
            });
        });
    }
    
}