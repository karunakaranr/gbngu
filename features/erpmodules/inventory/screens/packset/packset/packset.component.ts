import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { PacksetService } from 'features/erpmodules/inventory/services/packset/packset.service';
import { Ipackset } from 'features/erpmodules/inventory/models/Ipackset';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as PackSetJson from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
const urls = require('./../../../URLS/urls.json');

@Component({
  selector: 'app-packset',
  templateUrl: './packset.component.html',
  styleUrls: ['./packset.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'PackSetId' },
    { provide: 'url', useValue: urls.Packset },
    { provide: 'DataService', useClass: PacksetService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PacksetComponent extends GBBaseDataPageComponentWN<Ipackset> {
  title: string = 'PackSet'
  PackSetJSON = PackSetJson;

  form: GBDataFormGroupWN<Ipackset> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'PackSet', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.Getselectedid(arrayOfValues.PackSetId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

  public Getselectedid(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }

  public Getselectedids(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}

export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<Ipackset> {
  const dbds: GBBaseDBDataService<Ipackset> = new GBBaseDBDataService<Ipackset>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<Ipackset>, dbDataService: GBBaseDBDataService<Ipackset>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<Ipackset> {
  return new GBDataPageService<Ipackset>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


// import { Component, OnInit } from '@angular/core';
// import { FormBuilder } from '@angular/forms';
// import {GBBaseDataPageStoreComponentWN} from './../../../../../../libs/uicore/src/lib/components/Base/GBBasePage';
// import { Ipackset } from './../../../models/Ipackset';
// import { getThisGBPageService} from './packset.factory';
// import { PackFormgroupStore,} from './packset.formgroup';
// import { PacksetService } from './../../../services/packset/packset.service';
// import { PacksetDBService } from './../../../dbservices/packset/packset-db.service';
// import { Select, Store } from '@ngxs/store';

// import { GBHttpService } from './../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
// import { PacksetState } from './../../../stores/packset/packset.state';
// import { GetAll, GetData,SaveData } from '../../../stores/packset/packset.action';
// import { ActivatedRoute, ParamMap, Router } from '@angular/router';

// @Component({
//   selector: 'app-pack',
//   templateUrl: './packset.component.html',
//   styleUrls: ['./packset.component.scss'],
//   providers: [
//     { provide: 'IdField', useValue: 'idfield' },
//     { provide: 'url', useValue: 'url' },
//     { provide: 'Store', useClass: Store },
//     {provide: 'DBDataService',useClass: PacksetDBService,deps: [GBHttpService],},
//     { provide: 'DataService', useClass: PacksetService, deps: ['DBDataService'] },
//     {provide: 'PageService',useFactory: getThisGBPageService,deps: [Store,
//         'DataService',
//         'DBDataService',
//         FormBuilder,
//         GBHttpService,
//         ActivatedRoute,
//         Router,
//       ],
//     },
//   ],
// })
// export class PacksetComponent extends GBBaseDataPageStoreComponentWN<Ipackset>
//   implements OnInit {
//   title = 'PackSet';
//   rowData:Ipackset;
//   selection;
//   dataLoading;
//   columndata = [
//         { field: 'PackName',editable:true },
//         { field: 'PackSetDetailConversionType',editable:true },
//         { field: 'PackSetDetailConversionFactor',editable:true }];

//   form: PackFormgroupStore = new PackFormgroupStore(
//     this.gbps.gbhttp.http,
//     this.gbps.store
//   );
//   @Select(PacksetState) currentData$;
//   Picklistvalues
//   thisConstructor() {
//     this.currentData$.subscribe(res =>
//       this.rowData = res.currentData.model.PackSetDetailArray
//     );
//    }

//   loadScreen(recid?: string) {
//     if (recid) {
//       const id = parseInt(recid);
//       this.gbps.store.dispatch(new GetData(id));

//     }
//     else {
//       this.gbps.store.dispatch(new GetAll(null)).subscribe((res) => {
//         this.Picklistvalues = res.PackSet.data
//         console.log(" this.Picklistvalues:"+ JSON.stringify(this.Picklistvalues))
//         this.gbps.store.dispatch(new SaveData(null));
//       });
//     }
//   }

//   ngOnInit(): void {
//     let id = '';
//     this.gbps.activeroute.params.subscribe((params: any) => {
//       id = params['id'];
//     });
//     this.gbps.activeroute.paramMap.subscribe((params: ParamMap) => {
//       id = params.get('id')
//     });
//     if (this.gbps.activeroute.firstChild) {
//       this.gbps.activeroute.firstChild.params.subscribe((params: ParamMap) => {
//         id = params['id'];
//       });
//     }
//     this.loadScreen(id);
//   }
//   public changeFn(val) {
//     console.log("Dropdown selection:", val);
//     for(let data of this.Picklistvalues){
//       if(data.Code == val){
//         console.log(data.Id)
//         this.loadScreen(data.Id);
//       }
//     }
// }
// }


