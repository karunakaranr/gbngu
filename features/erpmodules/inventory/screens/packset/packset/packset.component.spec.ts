import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PacksetComponent } from './packset.component';

describe('PacksetComponent', () => {
  let component: PacksetComponent;
  let fixture: ComponentFixture<PacksetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PacksetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PacksetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
