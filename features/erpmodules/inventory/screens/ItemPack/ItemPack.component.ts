import { Component, OnInit } from '@angular/core';
import { ItemPackView } from '../../services/ItemPack/ItemPack.service';
const col = require('./../../../../../apps/Goodbooks/Goodbooks/src/assets/data/itempack.json')


@Component({
  selector: 'app-ItemPack',
  templateUrl: './ItemPack.component.html',
  styleUrls: ['./ItemPack.component.scss']
})

export class ItemPackComponent implements OnInit {
  title = 'Item Pack'
  public rowData = []
  public columnData = []

  constructor(public service: ItemPackView) { }
  ngOnInit(): void {
    this.accountlist();
  }
  public accountlist() {
    this.service.itempackview().subscribe(menudetails => {
      this.columnData = col;
      let servicejsondetail = menudetails
      this.service.Rowservice(servicejsondetail).subscribe(res => {
    this.rowData = res.ReportDetail;
    for (let data of this.rowData){
        if(data.PackConversionType == 0)
        {
        data.PartyIsServiceProvider = "No Conversion";
        }
        else if(data.PartyIsServiceProvider == 1)
        {
        data.PartyIsServiceProvider = "Fixed";
        }
        else{
            data.PartyIsServiceProvider = "Varying";
        }
        data.PackConversionFactor= data.PackConversionFactor.toLocaleString(undefined, { maximumFractionDigits: 2, minimumFractionDigits: 2 })
    }
  });
})

}

}