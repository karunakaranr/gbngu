import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ILotType } from '../../models/ILotType';
import { LotTypeService } from '../../services/LotType/LotType.service';
import { ButtonVisibility, Title } from 'features/layout/store/layout.actions';

import * as LotTypejson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/LotType.json'


import { InventoryURLS } from '../../URLS/urls';


@Component({
  selector: "app-LotType",
  templateUrl: "./LotType.component.html",
  styleUrls: ["./LotType.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'LotTypeId' },
    { provide: 'url', useValue: InventoryURLS.LotType },
    { provide: 'DataService', useClass: LotTypeService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class LotTypeComponent extends GBBaseDataPageComponentWN<ILotType> {
  title: string = "Lot Type"
  LotTypejson = LotTypejson;


  public setPatchValueToZero(value): void {
    console.log("this.form.get('LotTypeType').value:", this.form.get('LotTypeType').value)
    
    if(value == '0') {
      console.log("this.form.get('LotTypeType').value:", this.form.get('LotTypeType').value)
    this.form.get('SKUId').patchValue('-1');
    this.form.get('SKUName').patchValue('NONE');
    this.form.get('ItemId').patchValue('-1');
    this.form.get('ItemName').patchValue('NONE');
  }
  else  {
  
    this.form.get('ProcessName').patchValue('NONE');
    this.form.get('ProcessCode').patchValue('NONE');
    this.form.get('ProcessId').patchValue('-1');
  
  }
  console.log("this.form.get('LotTypeType').value:", this.form.get('LotTypeType').value)
  console.log("Form Detail:", this.form)
  }


  public itemClick(value): void {
    this.form.get('SKUId').patchValue('-1');
    this.form.get('SKUName').patchValue('');
  }

  form: GBDataFormGroupWN<ILotType> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'LotType', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.LottypeFillFunction(arrayOfValues.LotTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }




  public LottypeFillFunction(SelectedPicklistData: string): void {

   console.log("filller",SelectedPicklistData)
    if (SelectedPicklistData) {

      console.log("filllerall",SelectedPicklistData)
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Lottype => {
        this.form.patchValue(Lottype);
      })
      
      console.log("filllerallckkkj",SelectedPicklistData)
    }
  }


  public LotTypePatchValue(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}




export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ILotType> {
  const dbds: GBBaseDBDataService<ILotType> = new GBBaseDBDataService<ILotType>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ILotType>, dbDataService: GBBaseDBDataService<ILotType>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ILotType> {
  return new GBDataPageService<ILotType>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
