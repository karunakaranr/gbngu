import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';

import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { InventoryURLS } from '../../URLS/urls';
import { IItemTemplatePartService } from '../../dbservices/ItemTemplatepart/ItemTemplatePart.service';
import { IItemTemplatePart } from '../../models/IItemTempleatePart';
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
import * as ItemTemplatePartJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ItemTemplatePart.json'


@Component({
  selector: 'app-ItemTemplatePart',
  templateUrl: './ItemTemplatePart.component.html',
  styleUrls: ['./ItemTemplatePart.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'ItemTemplatePartId'},
    { provide: 'url', useValue: InventoryURLS.ItemTemplatepart },
    { provide: 'DataService', useClass: IItemTemplatePartService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ItemTemplatePartComponent extends GBBaseDataPageComponentWN<IItemTemplatePart> {
  title: string ='ItemTemplatePart'
  ItemTemplatePartJSON = ItemTemplatePartJSON;


  form: GBDataFormGroupWN<IItemTemplatePart> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'ItemTemplatePart', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.ItemTemplatePartId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IItemTemplatePart> {
  const dbds: GBBaseDBDataService<IItemTemplatePart> = new GBBaseDBDataService<IItemTemplatePart>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IItemTemplatePart>, dbDataService: GBBaseDBDataService<IItemTemplatePart>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IItemTemplatePart> {
  return new GBDataPageService<IItemTemplatePart>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
