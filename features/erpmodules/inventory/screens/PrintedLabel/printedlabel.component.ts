import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IPrintedLabel } from '../../models/IPrintedLabel';
import { PrintedLabelService } from '../../services/PrintedLabel/PrintedLabel.Service';
import { InventoryURLS } from '../../URLS/urls';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as PrintedLabelJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PrintedLabel.json'

@Component({
  selector: 'app-PrintedLabel',
  templateUrl:'printedlabel.component.html',
  styleUrls:['printedlabel.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'LabelId' },
    { provide: 'url', useValue: InventoryURLS.Label},
    { provide: 'DataService', useClass: PrintedLabelService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PrintedLabelComponent extends GBBaseDataPageComponentWN<IPrintedLabel> {
  title: string = "PrintedLabel"
  PrintedLabelJSON = PrintedLabelJSON;
  FirstNumber: number;
  SecondNumber: number;
  Balance:number;

  form: GBDataFormGroupWN<IPrintedLabel> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "PrintedLabel", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PrintedLabelFormVal(arrayOfValues.LabelId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public InputInitialValue(FirstNumberInput:number){
    this.FirstNumber = FirstNumberInput
    console.log("ExpectedAmount:",this.FirstNumber)
 }
 public InputMinusValue(SecondNumberInput:number){
   this.SecondNumber = SecondNumberInput
   console.log("OpeningAmount:",this.SecondNumber)
   this.Balance = this.FirstNumber - this.FirstNumber
   console.log("Balance:",this.Balance)
   var ans = this.FirstNumber - this.SecondNumber
   console.log("ans",ans)
this.form.get('LabelBackColor').patchValue(ans)
 }
  public PrintedLabelFormVal(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PrintedLabel => {
          this.form.patchValue(PrintedLabel);
      })
    }
  }
  public PrintedLabelpatch(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPrintedLabel> {
  const dbds: GBBaseDBDataService<IPrintedLabel> = new GBBaseDBDataService<IPrintedLabel>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPrintedLabel>, dbDataService: GBBaseDBDataService<IPrintedLabel>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPrintedLabel> {
  return new GBDataPageService<IPrintedLabel>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
