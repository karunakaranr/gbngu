import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CostcenterlistComponent } from './costcenterlist.component';

describe('CostcenterlistComponent', () => {
  let component: CostcenterlistComponent;
  let fixture: ComponentFixture<CostcenterlistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CostcenterlistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CostcenterlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
