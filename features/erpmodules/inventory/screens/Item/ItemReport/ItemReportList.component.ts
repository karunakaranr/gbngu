import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { ItemReportService } from './../../../services/Item/ItemReport.service'
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-ItemReportList.component',
  templateUrl: './ItemReportList.component.html',
  styleUrls: ['./ItemReportList.component.scss']
})

export class ItemReportComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;

  public rowData = [];

  constructor(public store: Store, @Inject(LOCALE_ID) public locale: string) { }
  ngOnInit(): void {
    this.GetitemreportList();
  }

  public GetitemreportList() {


    this.rowdatacommon$.subscribe(data => {
      this.rowData = data;
      console.log("Item List :", this.rowData)


      let json = this.rowData;
      var finalizedArray = [];


      json.map((row) => {
        finalizedArray.push({
          code: row['Code'],
          name: row['Name'],
          tname: row['Type.Name'],
          catname: row['Category.Name'],
          subcatname: row['SubCategory.Name'],

          igname: row['ItemGroup.Name'],
          ibname: row['ItemBrand.Name'],
          mname: row['Model.Name'],
          uomcode: row['StockUOM.Code'],
          gsttype: row['GSTTypeName'],
        });
      });


      const final = {};
      finalizedArray.forEach((detail) => {
        final[detail.catname] = {
          accountGroups: {},
          code: detail.catname,
          name: "",
          tname: "",
          catname: "",
          subcatname: "",

          igname: "",
          ibname: "",
          mname: "",
          uomcode: "",
          gsttype: "",
          ...final[detail.catname],
        };


        const grpcodes = Object.keys(final);

        const tableData = [];
        grpcodes.forEach((code) => {
          const accountGroups = Object.keys(final[code].accountGroups);

          // tableData.push({
          //   strcode: final[strcode].strcode,
          //   strname: "",
          //   bold: true,
          // });

          accountGroups.forEach((ag) => {
            tableData.push({
              code: final[code].accountGroups[ag].code,
              // igname: final[igcode].accountGroups[ag].igname,
              bold: true,
            });

            const accounts = Object.keys(final[code].accountGroups[ag].accounts);
            accounts.forEach((account) => {
              tableData.push({
                code: final[code].accountGroups[ag].accounts[account].code,
                name: final[code].accountGroups[ag].accounts[account].name,
                tname: final[code].accountGroups[ag].accounts[account].tname,
                catname: final[code].accountGroups[ag].accounts[account].catname,
                subcatname: final[code].accountGroups[ag].accounts[account].subcatname,

                igname: final[code].accountGroups[ag].accounts[account].igname,
                ibname: final[code].accountGroups[ag].accounts[account].ibname,
                mname: final[code].accountGroups[ag].accounts[account].mname,
                uomcode: final[code].accountGroups[ag].accounts[account].uomcode,
                gsttype: final[code].accountGroups[ag].accounts[account].gsttype,

              });
            });
          });
        });
      });

    });
  }
}