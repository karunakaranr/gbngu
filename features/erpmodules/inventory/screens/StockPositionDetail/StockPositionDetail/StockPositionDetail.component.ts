import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { GridProperties, GridSetting } from '../../../models/Iitemcategory';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-StockPositionDetail',
    templateUrl: './StockPositionDetail.component.html',
    styleUrls: ['./StockPositionDetail.component.scss'],
})
export class StockPositionDetailComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData = [];
    public sumquantity;
    public sumgoodquantity;
    totalamount: any[];
    constructor(public sharedService: SharedService,public store: Store) { }

    ngOnInit(): void {
        this.StockPositionDetail();
    }

    public StockPositionDetail() {
        this.sharedService.getRowInfo().subscribe(data => {
            if (data.length > 0) {
            this.rowData = data;
            console.log("Stock Position Detail :", this.rowData)


            this.sumquantity = 0;
            this.sumgoodquantity = 0;
            let json = this.rowData;
            var finalizedArray = [];
            for (let data of this.rowData) {
                this.sumquantity = this.sumquantity + data.Quantity;
                this.sumgoodquantity = this.sumgoodquantity + data.GoodQuantity;
            }
            json.map((row,index) => {
                finalizedArray.push({
                    sno:index,
                    code: row['Code'],
                    name: row['Name'],

                    itemcode: row['ItemCode'],
                    itemname: row['ItemName'],
                    uom: row['UOM'],
                    quantity: row['Quantity'],
                    goodquantity: row['GoodQuantity'],
                    rejectquantity: row['RejectedQuantity'],
                    reworkquantity: row['ReworkQuantity'],
                    other: row['OtherQuantity'],
                });
            });


            const final = {};
            finalizedArray.forEach((detail) => {
                final[detail.code] = {
                    accountGroups: {},

                    itemcode: detail.code,
                    itemname: detail.name,
                    uom: "",
                    quantity: 0,
                    goodquantity: 0,
                    rejectquantity: "",
                    reworkquantity: "",
                    other: "",
                    ...final[detail.code],
                };


                final[detail.code].accountGroups[detail.itemcode] = {
                    itemcode: detail.itemcode,
                    itemname: detail.itemname,
                    uom: detail.uom,
                    quantity: detail.quantity,
                    goodquantity: detail.goodquantity,
                    rejectquantity: detail.rejectquantity,
                    reworkquantity: detail.reworkquantity,
                    other: detail.other,
                };
            });
            
            const grpcodes = Object.keys(final);

            const tableData = [];
            grpcodes.forEach((itemcode) => {
                const accountGroups = Object.keys(final[itemcode].accountGroups);
                let sumquantity = 0;
                let sumgoodquantity = 0;

                accountGroups.forEach((ag) => {
                    sumquantity = sumquantity + parseFloat(final[itemcode].accountGroups[ag].quantity);
                    sumgoodquantity = sumgoodquantity + parseFloat(final[itemcode].accountGroups[ag].goodquantity);
                    })
                final[itemcode].quantity = sumquantity;
                final[itemcode].goodquantity = sumgoodquantity;

                    tableData.push({
                        itemcode: final[itemcode].itemcode,
                        itemname: final[itemcode].itemname,
                        uom: "",
                        quantity: final[itemcode].quantity,
                        goodquantity: final[itemcode].goodquantity,
                        rejectquantity: "",
                        reworkquantity: "",
                        other: "",
                        bold: true,
                    });
               
                    const accounts = Object.keys(final[itemcode].accountGroups);
                    accounts.forEach((account) => {
                        tableData.push({
                            itemcode: final[itemcode].accountGroups[account].itemcode,
                            itemname: final[itemcode].accountGroups[account].itemname,
                            uom: final[itemcode].accountGroups[account].uom,
                            quantity: final[itemcode].accountGroups[account].quantity,
                            goodquantity: final[itemcode].accountGroups[account].goodquantity,
                            rejectquantity: final[itemcode].accountGroups[account].rejectquantity,
                            reworkquantity: final[itemcode].accountGroups[account].reworkquantity,
                            other: final[itemcode].accountGroups[account].other,
                       
                    });
                });
            });
            this.rowData = tableData;
            console.log("Summary Register Data:", this.rowData)
        }
        });

    }
}


