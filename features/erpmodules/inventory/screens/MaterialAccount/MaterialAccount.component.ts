import { Component, OnInit } from '@angular/core';
import { MaterialAccount } from '../../services/MaterialAccount/MaterialAccount.service';


@Component({
  selector: 'app-MaterialAccount',
  templateUrl: './MaterialAccount.component.html',
  styleUrls: ['./MaterialAccount.component.scss']
})

export class MaterialAccountComponent implements OnInit {
  title = 'Material Account'
  public rowData = []
  public columnData = []

  constructor(public service: MaterialAccount) { }
  ngOnInit(): void {
    this.materialaccount();
  }
  public materialaccount() {
    this.service.materialaccountview().subscribe(menudetails => {
      let servicejsondetail = menudetails
      this.service.Rowservice(servicejsondetail).subscribe(res => {
    this.rowData = res.ReportDetail;
    for (let item of this.rowData){
      for(let data of item.MaterialAccountDetailArray){
        if(data.TransactionTypeType == 0)
        {
        data.TransactionTypeType = "Transfer";
        }
        else if(data.TransactionTypeType == 1)
        {
        data.TransactionTypeType = "Sales";
        }
        else if(data.TransactionTypeType == 2)
        {
        data.TransactionTypeType = "Purchase";
        }
        else if(data.TransactionTypeType == 3)
        {
        data.TransactionTypeType = "Sales Return";
        }
        else if(data.TransactionTypeType == 4)
        {
        data.TransactionTypeType = "Purchase Return";
        }
        else if(data.TransactionTypeType == 5)
        {
        data.TransactionTypeType = "Others";
        }
        else if(data.TransactionTypeType == 6)
        {
        data.TransactionTypeType = "Jobwork";
        }
        else if(data.TransactionTypeType == 7)
        {
        data.TransactionTypeType = "Jobwork Return";
        }
        else if(data.TransactionTypeType == 8)
        {
        data.TransactionTypeType = "Subcontract";
        }
        else if(data.TransactionTypeType == 5)
        {
        data.TransactionTypeType = "Subcontract Return";
        }
        
       }
  }
  });
})

}

}