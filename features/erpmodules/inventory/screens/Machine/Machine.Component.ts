import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
// import { ServiceName } from ‘ServicePath’;
// import { IMachine } from ‘ModelPath’;
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { InventoryURLS } from '../../URLS/urls';
import { MachinesService } from '../../services/Machine/Machine.Service';
import { IMachine } from '../../models/IMachine';
import * as MachineJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Machine.json'
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
// import { url} from ‘UrlPath’;


@Component({
    selector: 'app-Machine',
    templateUrl: './Machine.component.html',
    styleUrls: ['./Machine.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'MachineId' },
        { provide: 'url', useValue: InventoryURLS.Machines },
        { provide: 'DataService', useClass: MachinesService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class MachinesComponent extends GBBaseDataPageComponentWN<IMachine> {
    title: string = 'Machine'
    MachineJSON = MachineJSON;


    form: GBDataFormGroupWN<IMachine> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Machine', {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.FormFillFunction(arrayOfValues.MachineId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }


    public FormFillFunction(SelectedPicklistData: string): void {
        if (SelectedPicklistData) {
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
                this.form.patchValue(PackSet);
            })
        }
    }


    public PicklistPatchValue(SelectedPicklistDatas: any): void {
        console.log("SelectedPicklistDatas",SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }

    public ChangeTab(event, TabName: string): void {
        var i: number, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("FormTabLinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(TabName).style.display = "block";
        console.log("Event:", event)
        event.currentTarget.className += " active";
    }


}



export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IMachine> {
    const dbds: GBBaseDBDataService<IMachine> = new GBBaseDBDataService<IMachine>(http);
    dbds.endPoint = url;
    return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IMachine>, dbDataService: GBBaseDBDataService<IMachine>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IMachine> {
    return new GBDataPageService<IMachine>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
