import { IItemSubCategory } from '../../../models/Iitemsubcategory';
import { GBDataFormGroupStoreWN, GBDataFormGroupWN } from './../../../../../../libs/uicore/src/lib/classes/GBFormGroup';
import { HttpClient } from '@angular/common/http';
import { ItemsubcategoryService } from '../../../services/itemsubcategory/itemsubcategory.service';
import { Select, Store } from '@ngxs/store';
import { ItemsubcategoryStateActionFactory } from '../../../stores/itemsubcategory/itemsubcategory.actionfactory';

export class ItemsubcategoryFormgroup extends GBDataFormGroupWN<IItemSubCategory> {
  constructor(http: HttpClient, dataService: ItemsubcategoryService) {
    super(http, 'SC006', {}, dataService)
  }

  public clear() {
    super.clear();
  }

  public delete() {
    super.delete();
  }

  public save() {
    super.save();
  }

  public moveFirst() {
    super.moveFirst();
  }

  public movePrev() {
    super.movePrev();
  }

  public moveNext() {
    super.moveNext();
  }

  public moveLast() {
    super.moveLast();
  }
}
export class ItemsubcategoryFormgroupStore extends GBDataFormGroupStoreWN<IItemSubCategory> {
  constructor(http: HttpClient, store: Store) {
    super(http, 'SC006', {}, store, new ItemsubcategoryStateActionFactory())
  }

  public clear() {
    super.clear();
  }

  public delete() {
    super.delete();
  }

  public save() {
    super.save();
  }

  public moveFirst() {
    super.moveFirst();
  }

  public movePrev() {
    super.movePrev();
  }

  public moveNext() {
    super.moveNext();
  }

  public moveLast() {
    super.moveLast();
  }
}

