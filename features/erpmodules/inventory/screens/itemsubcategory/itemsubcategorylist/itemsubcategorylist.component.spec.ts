import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ItemSubCategorylistComponent } from './itemsubcategorylist.component';


describe('ItemSubCategorylistComponent', () => {
  let component: ItemSubCategorylistComponent;
  let fixture: ComponentFixture<ItemSubCategorylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemSubCategorylistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemSubCategorylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
