
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-Stocklegersummary',
    templateUrl: './Stocklegersummary.component.html',
    styleUrls: ['./Stocklegersummary.component.scss'],
})
export class StocklegersummaryComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;

    public TotalBill;
    public TotalTds;
    public TotalCess;
    public Totalmon;

    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getStocklegersummary();
    }

    public getStocklegersummary() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;

            if (data.length > 0) {

            console.log("Stock Leger Summary :", this.rowData)

            this.TotalBill = 0;
            this.TotalTds = 0;
            this.TotalCess = 0;
            this.Totalmon=0;

            for (let data of this.rowData) {
                this.TotalBill = (parseFloat(this.TotalBill) + parseFloat(data.OpeningValue)).toFixed(2);
                this.TotalTds = (parseFloat(this.TotalTds) + parseFloat(data.InwardPostedValue)).toFixed(2);
                this.TotalCess = (parseFloat(this.TotalCess) + parseFloat(data.OutwardPostedValue)).toFixed(2);
                this.Totalmon = (parseFloat(this.Totalmon) + parseFloat(data.ClosingValue)).toFixed(2);
            }

        }
        });
    }
}
