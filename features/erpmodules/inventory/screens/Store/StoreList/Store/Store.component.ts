import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { StoreService } from 'features/erpmodules/inventory/services/Store/Store.Service';
import { IStore } from 'features/erpmodules/inventory/models/IStore';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
import * as StoreJSON from './../../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Store.json'
// import { url} from ‘UrlPath’;
import { InventoryURLS } from 'features/erpmodules/inventory/URLS/urls';


@Component({
  selector: 'app-Store',
  templateUrl: './Store.component.html',
  styleUrls: ['./Store.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'StoreId'},
    { provide: 'url', useValue: InventoryURLS.Store },
    { provide: 'DataService', useClass: StoreService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class StoreComponent extends GBBaseDataPageComponentWN<IStore > {
  title: string = 'Store'
  StoreJSON = StoreJSON;


  form: GBDataFormGroupWN<IStore > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Store', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.StoreFillFunction(arrayOfValues.StoreId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public StoreFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public StorePatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IStore > {
  const dbds: GBBaseDBDataService<IStore > = new GBBaseDBDataService<IStore >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IStore >, dbDataService: GBBaseDBDataService<IStore >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IStore > {
  return new GBDataPageService<IStore >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
