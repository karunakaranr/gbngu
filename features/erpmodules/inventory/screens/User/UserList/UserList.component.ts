import { Component, OnInit } from '@angular/core';
import { UserListService } from './../../../services/User/UserList.service'
import { DatePipe } from '@angular/common';

const col = require('./../../../../../../apps/Goodbooks/Goodbooks/src/assets/data/UserList.json')

@Component({
  selector: 'app-UserList.component',
  templateUrl: './UserList.component.html',
  styleUrls: ['./UserList.component.scss']
})

export class UserListComponent implements OnInit {
  title = 'User List'
  public rowData = []
  public columnData = []

  constructor(public serivce: UserListService) { }
  ngOnInit(): void {
    this.userlist();
  }
  public userlist() {
    this.serivce.Reportdetailservice().subscribe(menudetails => {
      this.columnData = col;
      let servicejsondetail = menudetails
      this.serivce.Rowservice(servicejsondetail).subscribe(res => {
        this.rowData = res.ReportDetail;
        for (let data of this.rowData) {
          if (data) {
            var datePipe = new DatePipe("en-US");
            console.log(data.UserStatus);
            data.UserDOB = datePipe.transform(JSON.parse(data.UserDOB.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy');
            data.UserValidFrom = datePipe.transform(JSON.parse(data.UserValidFrom.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy');
            data.UserValidTo = datePipe.transform(JSON.parse(data.UserValidTo.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy');
            data.RoleName = data.RoleName.replace("NONE","");
            if(data.UserStatus == 0)
            {
            data.UserStatus = "Pen";
            }
            else if(data.UserStatus == 1)
            {
            data.UserStatus = "Act";
            }
            else if(data.UserStatus == 2)
            {
            data.UserStatus = "Del";
            }
            else if(data.UserStatus == 3)
            {
            data.UserStatus = "Amend";
            }
            else if(data.UserStatus == 4)
            {
            data.UserStatus = "InA";
            }
            else if(data.UserStatus == 5)
            {
            data.UserStatus = "Arc";
            }
           
          }
        }
      });
    })

  }

}
