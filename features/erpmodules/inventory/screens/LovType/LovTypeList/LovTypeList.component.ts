import { Component, OnInit } from '@angular/core';
import { LovTypeList } from '../../../services/LovType/LovTypeList/LovTypeList.service';
import { NavigationExtras, Router } from '@angular/router';
import {Inject, LOCALE_ID } from '@angular/core';


@Component({
  selector: 'app-LovTypeList',
  templateUrl: './LovTypeList.component.html',
  styleUrls: ['./LovTypeList.component.scss']
})

export class LovTypeListComponent implements OnInit {
  title = 'Lov Type List'
  public rowData = []
  public columnData = []

  constructor(public service: LovTypeList, private router: Router, @Inject(LOCALE_ID) public locale: string) { }
  ngOnInit(): void {
    this.LovTypeList();
  }
  selectedcell(data) {
    const queryParams: any = {};
    queryParams.myArray = JSON.stringify(data);
    const navigationExtras: NavigationExtras = {
      queryParams
    };
    this.router.navigate(["inventory/lovtype"], navigationExtras);

  }
  public LovTypeList() {
    this.service.lovtypelistview().subscribe(menudetails => {
      let servicejsondetail = menudetails
      this.service.Rowservice(servicejsondetail).subscribe(res => {
    this.rowData = res.ReportDetail;
    for (let data of this.rowData){
        if(data.LovTypeScoreRequired == 0)
        {
        data.LovTypeScoreRequired = "Yes";
        }
        else if(data.LovTypeScoreRequired == 1)
        {
        data.LovTypeScoreRequired = "No";
        }
        if(data.LovTypeLovNature == 0)
        {
        data.LovTypeLovNature = "Simple";
        }
        else if(data.LovTypeLovNature == 1)
        {
        data.LovTypeLovNature = "Dependent";
        }
        if(data.LovTypeGenerationType == 0)
        {
        data.LovTypeGenerationType = "Manual";
        }
        else if(data.LovTypeGenerationType == 1)
        {
        data.LovTypeGenerationType = "Auto";
        }
  }
  });
})

}

}