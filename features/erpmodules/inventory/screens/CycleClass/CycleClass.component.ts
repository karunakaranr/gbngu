import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ICycleClass } from '../../models/ICycleClass';
import { CycleClassService } from '../../services/CycleClass/CycleClass.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { InventoryURLS } from 'features/erpmodules/inventory/URLS/urls';
import * as CycleClassJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CycleClass.json'

@Component({
  selector: 'app-CycleCountClass',
  templateUrl: 'CycleClass.component.html',
  styleUrls:['CycleClass.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'CycleClassId' },
    { provide: 'url', useValue: InventoryURLS.CycleClass},
    { provide: 'DataService', useClass: CycleClassService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class CycleClassComponent extends GBBaseDataPageComponentWN<ICycleClass> {
  title: string = "CycleClass"
  CycleClassJSON = CycleClassJSON;

  form: GBDataFormGroupWN<ICycleClass> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "CycleClass", {}, this.gbps.dataService);
  thisConstructor() {
    console.log("hi");
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.CycleClassFormValue(arrayOfValues.CycleClassId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public CycleClassFormValue(SelectedPicklistData: string): void {
    console.log("hi i m here");
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(CycleClass => {
          this.form.patchValue(CycleClass);
      })
    }
  }
  public CycleClassPatchNot(SelectedPicklistDatas: any): void {
    console.log("YesSave")
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICycleClass> {
  const dbds: GBBaseDBDataService<ICycleClass> = new GBBaseDBDataService<ICycleClass>(http);
  console.log("Save")
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ICycleClass>, dbDataService: GBBaseDBDataService<ICycleClass>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICycleClass> {
  return new GBDataPageService<ICycleClass>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
  console.log("Save22")
}
