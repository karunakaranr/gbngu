import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
// import { ServiceName } from ‘ServicePath’;
// import { ModelName } from ‘ModelPath’;
import { BrandService } from 'features/erpmodules/inventory/services/brand/brand.service';
import { IBrand } from 'features/erpmodules/inventory/models/IBrand';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
// import { url} from ‘UrlPath’;
import * as BrandJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Brand.json'
import { InventoryURLS } from 'features/erpmodules/inventory/URLS/urls';


@Component({
  selector: 'app-Brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ItemBrandId'},
    { provide: 'url', useValue: InventoryURLS.Brand },
    { provide: 'DataService', useClass: BrandService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class BrandComponent extends GBBaseDataPageComponentWN<IBrand > {
  title: string = 'Brand'
  BrandJSON = BrandJSON;


  form: GBDataFormGroupWN<IBrand > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Brand', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.BrandFillValue(arrayOfValues.ItemBrandId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public BrandFillValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public BrandPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IBrand > {
  const dbds: GBBaseDBDataService<IBrand > = new GBBaseDBDataService<IBrand >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IBrand >, dbDataService: GBBaseDBDataService<IBrand >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IBrand > {
  return new GBDataPageService<IBrand >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


