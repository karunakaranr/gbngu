// import { BrandComponent } from './brand.component';
// import { FormBuilder } from '@angular/forms';
// import {GBDataPageServiceWN} from './../../../../../../libs/uicore/src/lib/services/gbpage.service';
// import { Store } from '@ngxs/store';
// import {GBBaseDataServiceWN} from './../../../../../../libs/gbdata/src/lib/services/gbbasedata.service';
// import {GBHttpService} from './../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
// import {GBBaseDBDataService} from './../../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service'
// import { IBrand } from './../../../models/IBrand';
// import { ActivatedRoute, Router, ParamMap } from '@angular/router';

// const datajson = require('./../brandapiresponse.json'); // for Testing
// const testjson = require('./../brandtest.json'); // for Testing

// describe('BrandComponent', () => {
//   let currentdatatest = testjson[0];                                //Testing
//   let withoutid = testjson[1];                                      //Testing
//   let withoutidcode = testjson[2];                                  //Testing
//   let nodata = testjson[3];                                          //Testing
                                          
//   let fixture: BrandComponent;
//   let formBuilderMock: GBDataPageServiceWN<IBrand>;
// 	let store:Store;
// 	let dataService:GBBaseDataServiceWN<IBrand>;
//   let dbDataService:GBBaseDBDataService<IBrand>;
// 	let fb = new FormBuilder();
// 	let gbhttp:GBHttpService;
// 	let activeroute=new ActivatedRoute();
// 	let router: Router;
//   beforeEach(() => {
//     formBuilderMock = new GBDataPageServiceWN(store,dataService,dbDataService,fb,gbhttp,activeroute,router);
//     fixture = new BrandComponent(
//       formBuilderMock,
//     );
//     // fixture.ngOnInit();
//   });

//   describe('Test: load', () => {
//     it('check service reponse', () => {
//       expect(datajson).toEqual(datajson);
//     });
//   });

//   describe('Test: currentdata', () => {
//     it('check service currentdata', () => {
//       expect(currentdatatest).toEqual(datajson[0]);
//     });
//   });

//   describe('Save: without mandatory id', () => {
//     it('check  mandatory id', () => {
//       expect(withoutid).toEqual(datajson[0]);
//     });
//   });

//   describe('Save: without mandatory id & code', () => {
//     it('check mandatory id & code', () => {
//       expect(withoutidcode).toEqual(datajson[0]);
//     });
//   });

//   describe('Save: with null data', () => {
//     it('check null datas', () => {
//       expect(nodata).toEqual(datajson[0]);
//     });
//   });

// });



// // import { HttpClient } from '@angular/common/http';
// // import { BrandComponent } from './brand.component';
// // import { FormBuilder } from '@angular/forms';
// // import {GBDataPageServiceWN} from './../../../../../../libs/uicore/src/lib/services/gbpage.service';
// // import { Store } from '@ngxs/store';
// // import {GBBaseDataServiceWN} from './../../../../../../libs/gbdata/src/lib/services/gbbasedata.service';
// // import {GBHttpService} from './../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
// // import {GBBaseDBDataService} from './../../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service'
// // import { IBrand } from './../../../models/IBrand';
// // import { ActivatedRoute, Router, ParamMap } from '@angular/router';
// // import {GBBaseDataPageStoreComponentWN} from './../../../../../../libs/uicore/src/lib/components/Base/GBBasePage';
// // import { BrandFormgroupStore } from './brand.formgroup';
// // describe('BrandComponent', () => {
// //   let fixture: BrandComponent;
// //   let formBuilderMock: GBBaseDataPageStoreComponentWN<IBrand>;
// // 	// let store:Store;z
// // 	// let dataService:GBBaseDataServiceWN<IBrand>;
// //   // let dbDataService:GBBaseDBDataService<IBrand>;
// // 	// let fb = new FormBuilder();
// //   // let http : HttpClient;
// // 	// let gbhttp:GBHttpService;
// // 	// let activeroute=new ActivatedRoute();
// // 	// let router: Router;
// //   // let gbps : GBDataPageServiceWN<IBrand>
// //   beforeEach(() => {
// //     formBuilderMock = new GBBaseDataPageStoreComponentWN();
// //     fixture = new BrandComponent(
// //       formBuilderMock,
// //     );
// //     fixture.ngOnInit();
// //   });

// //   describe('Test: ngOnInit', () => { 
// //     it('should initialize brandform', () => {
// //       const brandform = {
// //         ItemBrandCode: "ITBRAND",
// //         ItemBrandId: -1500000000 ,      ​
// //         ItemBrandIsAsset: 0,      ​
// //         ItemBrandIsItem: 0,     ​
// //         ItemBrandIsSelected: 1,        ​
// //         ItemBrandName: "ITEM BRAND",        ​
// //         ItemBrandRemarks: "ITEM BRAND",        ​
// //         ItemBrandStatus: 1,        
// //         ItemBrandVersion: 7,
// //       };
// //       expect(fixture.value).toEqual(brandform);
// //     });
// //   });

// // });

