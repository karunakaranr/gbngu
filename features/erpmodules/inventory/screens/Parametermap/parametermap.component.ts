import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ParametermapService } from '../../services/Parametermap/Parametermap'; 
import { IParametermap } from '../../models/IParametermap'; 
import { ButtonVisibility } from 'features/layout/store/layout.actions';
const urls = require('./../../URLS/urls.json');


@Component({
  selector: 'app-parametermap',
  templateUrl: './parametermap.component.html',
  styleUrls: ['./parametermap.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ParameterMapId'},
    { provide: 'url', useValue: urls.Parametermap },
    { provide: 'saveurl', useValue: urls.ParametermapSave },
    { provide: 'deleteurl', useValue: urls.ParametermapDelete },
    { provide: 'DataService', useClass:ParametermapService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url', 'saveurl', 'deleteurl'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})


export class ParametermapComponent extends GBBaseDataPageComponentWN<IParametermap> {
  title : string = "Parametermap"
  form: GBDataFormGroupWN<IParametermap> = new GBDataFormGroupWN(this.gbps.gbhttp.http,  "Parametermap", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
      this.Getselectedid(arrayOfValues.FormId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }

 
  public Getselectedid(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Parametermap => {
        console.log("Parametermap:",Parametermap[0])
          this.form.patchValue(Parametermap[0]);
          this.form.get('ParameterMapCreatedByName').patchValue('EMP00060')
          this.form.get('ParameterMapModifiedByName').patchValue('ADMIN')
          this.form.get('ParameterSetName').patchValue(-1499999995)
        })
    }
  }


  public Getselectedids(SelectedPicklistDatas: any): void {
    console.log("SelectedPicklistDatas:",SelectedPicklistDatas)
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("Form:",this.form.value)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string, saveurl: string, deleteurl: string): GBBaseDBDataService<IParametermap> {
  const dbds: GBBaseDBDataService<IParametermap> = new GBBaseDBDataService<IParametermap>(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl;
  dbds.deleteurl = deleteurl;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IParametermap>, dbDataService: GBBaseDBDataService<IParametermap>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IParametermap> {
  return new GBDataPageService<IParametermap>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
