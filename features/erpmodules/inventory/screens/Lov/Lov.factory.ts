
import { GBBaseDataService, GBBaseDataServiceWN} from './../../../../../libs/gbdata/src/lib/services/gbbasedata.service';
import {GBBaseDBDataService} from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service'
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {GbToasterService} from './../../../../../libs/gbcommon/src/lib/services/toaster/gbtoaster.service';
import { Lov } from '../../models/ILov';
import { LovService } from '../../services/Lov/Lov.service';
import { LovDBService } from '../../dbservices/Lov/LovDB.service';
import { Store } from '@ngxs/store';
import { FormBuilder } from '@angular/forms';
import {GBDataPageService, GBDataPageServiceWN} from './../../../../../libs/uicore/src/lib/services/gbpage.service';
import { ActivatedRoute, Router } from '@angular/router';

// export function getThisDBDataService(http: GBHttpService): GBBaseDBDataService<Lov> {
//   return new LovDBService(http);
// }
// export function getThisDataServiceWN(dbDataService: GBBaseDBDataService<Lov>): GBBaseDataServiceWN<Lov> {
//   return new LovService(dbDataService);
// }
// export function getThisDataService(dbDataService: GBBaseDBDataService<Lov>): GBBaseDataService<Lov> {
//   return new LovService(dbDataService);
// }
export function getThisGBPageService(store: Store, dataService: GBBaseDataService<Lov>, dbDataService: GBBaseDBDataService<Lov>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster:GbToasterService): GBDataPageService<Lov> {
  return new GBDataPageService<Lov>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
export function getThisGBPageServiceWN(store: Store, dataService: GBBaseDataServiceWN<Lov>, dbDataService: GBBaseDBDataService<Lov>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router, toaster:GbToasterService): GBDataPageServiceWN<Lov> {
  return new GBDataPageServiceWN<Lov>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
