import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { SKUSettingService } from '../../services/SKUSetting/skusetting.service';
import { ISKUSetting } from '../../models/ISKUSetting';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as SKUSettingJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/SKUSetting.json';
import { InventoryURLS } from '../../URLS/urls';


@Component({
  selector: 'app-skusetting',
  templateUrl: './skusetting.component.html',
  styleUrls: ['./skusetting.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'SKUSettingId'},
    { provide: 'url', useValue: InventoryURLS.SKUSetting },
    { provide: 'DataService', useClass: SKUSettingService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class SKUSettingComponent extends GBBaseDataPageComponentWN<ISKUSetting> {
  title: string = 'SKUSetting'
  SKUSettingJson = SKUSettingJson;


  form: GBDataFormGroupWN<ISKUSetting> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "SKUSetting", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.SKUSettingRetrivalValue(arrayOfValues.SKUSettingId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public SKUSettingRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public SKUSettingPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ISKUSetting> {
  const dbds: GBBaseDBDataService<ISKUSetting> = new GBBaseDBDataService<ISKUSetting>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ISKUSetting>, dbDataService: GBBaseDBDataService<ISKUSetting>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ISKUSetting> {
  return new GBDataPageService<ISKUSetting>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
