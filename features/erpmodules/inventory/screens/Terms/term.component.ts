import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as TermJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Term.json'
import { InventoryURLS } from '../../URLS/urls';
import { TermService } from '../../services/Term/term.service';
import { ITerm } from '../../models/ITerm';
@Component({
    selector: 'app-Term',
    templateUrl:'./term.component.html',
    styleUrls: ['./term.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'TermId' },
        { provide: 'url', useValue: InventoryURLS.Term },
        { provide: 'DataService', useClass: TermService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class TermComponent extends GBBaseDataPageComponentWN<ITerm> {
    title: string = "Term"
    TermJSON = TermJSON

    form: GBDataFormGroupWN<ITerm> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Term", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.TermPicklistFillValue(arrayOfValues.TermId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }
  
    public TermPicklistFillValue(SelectedPicklistData: string): void {
        // console.log("id",SelectedPicklistData)
        if (SelectedPicklistData) {
            // console.log("id",SelectedPicklistData)

            this.gbps.dataService.getData(SelectedPicklistData).subscribe(Term => {
                // console.log("Term",Term)
                    this.form.patchValue(Term);                
               
            })
        }
    }

    public TermPicklistPatchValue(SelectedPicklistDatas: any): void {
        // console.log("id",SelectedPicklistDatas)
        console.log("SelectedPicklistDatasRetrival",SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ITerm> {
    const dbds: GBBaseDBDataService<ITerm> = new GBBaseDBDataService<ITerm>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<ITerm>, dbDataService: GBBaseDBDataService<ITerm>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ITerm> {
    return new GBDataPageService<ITerm>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
