import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import { IProcessOperation } from '../../models/IProcessOperation';
// import { LotTypeService } from '../../services/LotType/LotType.service';
import { ProcessOperationService } from '../../services/ProcessOperation/ProcessOperation.service';
import { IProcessOperation } from '../../models/IProcessOperation';


import * as ProcessOperationJson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ProcessOperation.json'

import { InventoryURLS } from '../../URLS/urls';




@Component({
    selector: 'app-ProcessOperation',
    templateUrl: './ProcessOperation.component.html',
    styleUrls: ["./ProcessOperation.component.scss"],
    providers: [
        { provide: 'IdField', useValue: 'ProcessOperationId' },
        { provide: 'url', useValue: InventoryURLS.ProcessOperation },
        { provide: 'DataService', useClass: ProcessOperationService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class ProcessOperationComponent extends GBBaseDataPageComponentWN<IProcessOperation> {
  title: string = "ProcessOperation"
  ProcessOperationJson = ProcessOperationJson;





  form: GBDataFormGroupWN<IProcessOperation> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'ProcessOperation', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ProcessOperationPicklist(arrayOfValues.LotTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }




  public ProcessOperationPicklist(SelectedPicklistData: string): void {

   
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Lottype => {
        this.form.patchValue(Lottype);
      })
    }
  }


  public ProcessOperationPicklists(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}




export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IProcessOperation> {
  const dbds: GBBaseDBDataService<IProcessOperation> = new GBBaseDBDataService<IProcessOperation>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IProcessOperation>, dbDataService: GBBaseDBDataService<IProcessOperation>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IProcessOperation> {
  return new GBDataPageService<IProcessOperation>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
