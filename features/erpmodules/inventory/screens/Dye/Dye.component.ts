import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { DyeService } from '../../services/Dye/Dye.service';
import { IDye } from '../../models/IDye';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as DyeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Dye.json'
import { InventoryURLS } from '../../URLS/urls';

@Component({
  selector: 'app-Dye',
  templateUrl: './Dye.component.html',
  styleUrls: ['./Dye.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'DiecutId'},
    { provide: 'url', useValue: InventoryURLS.Diecut },
    { provide: 'DataService', useClass: DyeService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class DyeComponent extends GBBaseDataPageComponentWN<IDye > {
  title: string = 'Dye'
  DyeJSON = DyeJSON;


  form: GBDataFormGroupWN<IDye > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Dye', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.DyeRetrivalValue(arrayOfValues.DiecutId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public DyeRetrivalValue (SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public DyePatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IDye > {
  const dbds: GBBaseDBDataService<IDye > = new GBBaseDBDataService<IDye >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IDye >, dbDataService: GBBaseDBDataService<IDye >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IDye > {
  return new GBDataPageService<IDye >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
