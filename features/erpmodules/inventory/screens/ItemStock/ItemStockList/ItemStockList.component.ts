import { Component, OnInit } from '@angular/core';
import { ItemStockService } from './../../../services/ItemStock/ItemStock.service'
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-ItemStockList.component',
  templateUrl: './ItemStockList.component.html',
  styleUrls: ['./ItemStockList.component.scss']
})

export class ItemStockListComponent implements OnInit {
  title = 'Item Stock'

  itemstocklist: any[] = [];

  constructor(
    private itemstockservice: ItemStockService, public router: Router) { }

  ngOnInit() {
    this.GetitemstockList();
  }


  GetitemstockList(): void {
    this.itemstockservice.GetitemstockList().subscribe(menuDetails => {
      const menuDet = menuDetails;
      this.itemstockservice.Rowservice(menuDet).subscribe(res => {
        const final = {};
        const tableData = [];
        let STgroupCodes;
        let data = res.ReportDetail;
        data.forEach((detail) => {
          final[detail.StoreCode] = {
            STGroups: {},
            name: detail.StoreName,
            skucode: '',
            skuname: '',
            uom: '',
            binno: '',
            soh: '',
            cycleclass: '',
            ...final[detail.StoreCode]
          };

          final[detail.StoreCode].STGroups[detail.ItemCode] = {
            name: detail.ItemName,
            skucode: detail.SKUCode,
            skuname: detail.SKUName,
            uom: detail.UOMCode,
            binno: detail.ItemStockBinNumber,
            soh: detail.ItemStockStockOnHand,
            cycleclass: detail.CycleClassCycleClasso
          };
        });
        STgroupCodes = Object.keys(final);
        STgroupCodes.forEach((STgroupCode) => {
          tableData.push({
            code: STgroupCode,
            name: final[STgroupCode].name,
            skucode: final[STgroupCode].SKUCode,
            skuname: final[STgroupCode].SKUName,
            uom: final[STgroupCode].UOMCode,
            binno: final[STgroupCode].ItemStockBinNumber,
            soh: final[STgroupCode].ItemStockStockOnHand,
            cycleclass: final[STgroupCode].CycleClassCycleClasso,
            bold: true
          });

          let check = Object.keys(final[STgroupCode].STGroups);
          check.forEach((STGroup) => {
            tableData.push({
              code: STGroup,
              name: final[STgroupCode].STGroups[STGroup].name,
              skucode: final[STgroupCode].STGroups[STGroup].skucode,
              skuname: final[STgroupCode].STGroups[STGroup].skuname,
              uom: final[STgroupCode].STGroups[STGroup].uom,
              binno: final[STgroupCode].STGroups[STGroup].binno,
              soh: final[STgroupCode].STGroups[STGroup].soh,
              cycleclass: final[STgroupCode].STGroups[STGroup].cycleclass,

            });
          });
        });

        for (let data of tableData) {
          if (data.skucode == "NONE") {
            data.skucode = ""
          }
          if (data.skuname == "NONE") {
            data.skuname = ""
          }
          if (data.uom == "NONE") {
            data.uom = ""
          }
          if (data.cycleclass == "NONE") {
            data.cycleclass = ""
          }
          if (data.binno == "0") {
            data.binno = ""
          }
          if (data.binno == "null") {
            data.binno = ""
          }
          if (data.soh == "0") {
            data.soh = ""
          }
        }
        this.itemstocklist = tableData;
      });
    });
  }
  selectedcell(data) {
    const queryParams: any = {};
    queryParams.myArray = JSON.stringify(data);
    const navigationExtras: NavigationExtras = {
      queryParams
    };
    this.router.navigate(["HTMLVIEW"], navigationExtras);
  }

}

