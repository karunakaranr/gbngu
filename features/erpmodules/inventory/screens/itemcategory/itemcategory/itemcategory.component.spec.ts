import { FormBuilder } from '@angular/forms';
import {GBDataPageServiceWN} from './../../../../../../libs/uicore/src/lib/services/gbpage.service';
import { Store } from '@ngxs/store';
import {GBBaseDataServiceWN} from './../../../../../../libs/gbdata/src/lib/services/gbbasedata.service';
import {GBHttpService} from './../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {GBBaseDBDataService} from './../../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service'
import { IItemCategory } from './../../../models/Iitemcategory';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { ItemcategoryComponent } from './itemcategory.component';

const datajson = require('./itemcategory.json'); // for Testing


describe('ItemcategoryComponent', () => {
  let fixture: ItemcategoryComponent;
  let formBuilderMock: GBDataPageServiceWN<IItemCategory>;
  let store:Store;
  let dataService:GBBaseDataServiceWN<IItemCategory>;
  let dbDataService:GBBaseDBDataService<IItemCategory>;
  let fb = new FormBuilder();
  let gbhttp:GBHttpService;
  let activeroute=new ActivatedRoute();
  let router: Router;
  beforeEach(() => {
    formBuilderMock = new GBDataPageServiceWN(store,dataService,dbDataService,fb,gbhttp,activeroute,router);
    fixture = new ItemcategoryComponent(
      formBuilderMock,
    );
    // fixture.ngOnInit();
  });

  describe('Test: ngOnInit', () => {
    it('check service reponse', () => {
      expect(fixture.loadScreen()).toEqual(datajson);
    });
  });

});