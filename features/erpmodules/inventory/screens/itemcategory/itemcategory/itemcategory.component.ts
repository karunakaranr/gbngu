import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
// import { ServiceComponentName } from ‘ServiceComponentPath’ ;
import { ItemCategoryService } from 'features/erpmodules/inventory/services/itemcategory/itemcategory.service';
// import { ModelName } from ‘ModelPath’;
import { IItemCategory } from 'features/erpmodules/inventory/models/Iitemcategory';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

// import {} from('./../../../URLS/urls.json')
// const urls = require('./../../../URLS/urls.json');
import { InventoryURLS } from 'features/erpmodules/inventory/URLS/urls';
import * as ItemCategoryJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ItemCategory.json'


@Component({
  selector: 'app-ItemCategory',
  templateUrl: './itemcategory.component.html',
  styleUrls: ['./itemcategory.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ItemCategoryId'},
    { provide: 'url', useValue: InventoryURLS.ITEMCATEGORY },
    { provide: 'DataService', useClass: ItemCategoryService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ItemCategoryComponent extends GBBaseDataPageComponentWN<IItemCategory > {
  title: string = 'ItemCategory'
  ItemCategoryJSON = ItemCategoryJSON;


  form: GBDataFormGroupWN<IItemCategory > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'ItemCategory', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ItemFormFill(arrayOfValues.ItemCategoryId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public ItemFormFill(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IItemCategory > {
  const dbds: GBBaseDBDataService<IItemCategory > = new GBBaseDBDataService<IItemCategory >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IItemCategory >, dbDataService: GBBaseDBDataService<IItemCategory >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IItemCategory > {
  return new GBDataPageService<IItemCategory >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
