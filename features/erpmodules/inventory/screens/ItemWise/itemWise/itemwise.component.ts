import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
// import { ItemcategoryService } from 'features/erpmodules/inventory/services/itemcategory/itemcategory.service';
import { Observable } from 'rxjs';
import { GridProperties, GridSetting } from '../../../models/Iitemcategory';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';


@Component({
  selector: 'app-itemwise',
  templateUrl: './itemwise.component.html',
  styleUrls: ['./itemwise.component.scss'],
})
export class itemwiseComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];
  public TotalInQty;
  public TotalCost;
  public TotalValue;

  constructor(public store: Store, @Inject(LOCALE_ID) public locale: string) { }
  ngOnInit(): void {
    this.getitemwiselist();
  }

  public getitemwiselist() {
    this.rowdatacommon$.subscribe(data => {
      this.rowData = data;
      console.log("Item Wise :", this.rowData)

      this.TotalInQty = 0;
      this.TotalCost = 0;
      this.TotalValue = 0;
      let json = this.rowData;
      var finalizedArray = []
      for (let data of this.rowData) {
        this.TotalInQty = this.TotalInQty + data.inqty;
        this.TotalCost = this.TotalCost + data.cost;
        this.TotalValue = this.TotalValue + data.value;
      }
      json.map(row => {
        finalizedArray.push({
          icode: row['ItemCode'],
          iname: row['ItemName'],
          sldate: row['StockLedgerDate'],
          bizcode: row['BIZtransactionTypeCode'],
          slname: row['StockLedgerNumber'],
          refno: row['ReferenceNumber'],
          refdate: row['ReferenceDate'],

          stcode: row['StoreCode'],
          inqty: row['InwardGoodQuantity'],
          outqty: row['OutwardGoodQuantity'],
          cost: row['PostedCost'],
          value: row['PostedValue'],
          rate: row['Rate'],
        })
      })
      const final = {};
      finalizedArray.forEach(detail => {
        final[detail.icode] = {
          accounts: {},
          sldate: detail.icode,
          bizcode: detail.iname,
          slname: "",
          refno: "",
          refdate: "",

          stcode: "",
          inqty: "",
          outqty: "",
          cost: "",
          value: "",
          rate: "",
          ...final[detail.empcode],
        }
        final[detail.icode].accounts[detail.revno] = {
          sldate: detail.sldate,
          bizcode: detail.bizcode,
          slname: detail.slname,
          refno: detail.refno,
          refdate: detail.refdate,

          stcode: detail.stcode,
          inqty: 0,
          outqty: detail.outqty,
          cost: 0,
          value: 0,
          rate: detail.rate,
        }
      })
      const empcodes = Object.keys(final)
      const tableData = [];
      empcodes.forEach(code => {
        const account = Object.keys(final[code].accounts)
        let suminqty = 0;
        let sumcost = 0;
        let sumvalue = 0;
        account.forEach(account => {
          suminqty = suminqty + parseFloat(final[code].accounts[account].inqty);
          sumcost = sumcost + parseFloat(final[code].accounts[account].cost);
          sumvalue = sumvalue + parseFloat(final[code].accounts[account].value);
        })
        final[code].inqty = suminqty;
        final[code].cost = sumcost;
        final[code].value = sumvalue;

        tableData.push({
          sldate: final[code].sldate,
          bizcode: final[code].bizcode,
          bold: true,
        })
        account.forEach(ag => {
          tableData.push({
            sldate: final[code].accounts[ag].sldate,
            bizcode: final[code].accounts[ag].bizcode,
            slname: final[code].accounts[ag].slname,
            refno: final[code].accounts[ag].refno,
            refdate: final[code].accounts[ag].refdate,

            stcode: final[code].accounts[ag].stcode,
            inqty: final[code].accounts[ag].inqty,
            outqty: final[code].accounts[ag].outqty,
            cost: final[code].accounts[ag].cost,
            value: final[code].accounts[ag].value,
            rate: final[code].accounts[ag].rate,
          })
        })
      })
      this.rowData = tableData;
    });
  }
}
