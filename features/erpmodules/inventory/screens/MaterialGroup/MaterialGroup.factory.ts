
import { GBBaseDataService, GBBaseDataServiceWN} from './../../../../../libs/gbdata/src/lib/services/gbbasedata.service';
import {GBBaseDBDataService} from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service'
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {GbToasterService} from './../../../../../libs/gbcommon/src/lib/services/toaster/gbtoaster.service';
import { MaterialGroup } from '../../models/IMaterialGroup';
import { MaterialGroupService } from '../../services/MaterialGroup/MaterialGroup.service';
import { MaterialGroupDBService } from '../../dbservices/MaterialGroup/MaterialGroupDB.service';
import { Store } from '@ngxs/store';
import { FormBuilder } from '@angular/forms';
import {GBDataPageService, GBDataPageServiceWN} from './../../../../../libs/uicore/src/lib/services/gbpage.service';
import { ActivatedRoute, Router } from '@angular/router';

export function getThisDBDataService(http: GBHttpService): GBBaseDBDataService<MaterialGroup> {
  return new MaterialGroupDBService(http);
}
export function getThisDataServiceWN(dbDataService: GBBaseDBDataService<MaterialGroup>): GBBaseDataServiceWN<MaterialGroup> {
  return new MaterialGroupService(dbDataService);
}
export function getThisDataService(dbDataService: GBBaseDBDataService<MaterialGroup>): GBBaseDataService<MaterialGroup> {
  return new MaterialGroupService(dbDataService);
}
export function getThisGBPageService(store: Store, dataService: GBBaseDataService<MaterialGroup>, dbDataService: GBBaseDBDataService<MaterialGroup>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster:GbToasterService): GBDataPageService<MaterialGroup> {
  return new GBDataPageService<MaterialGroup>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
export function getThisGBPageServiceWN(store: Store, dataService: GBBaseDataServiceWN<MaterialGroup>, dbDataService: GBBaseDBDataService<MaterialGroup>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router, toaster:GbToasterService): GBDataPageServiceWN<MaterialGroup> {
  return new GBDataPageServiceWN<MaterialGroup>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
