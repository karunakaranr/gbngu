import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IMasterSchedule } from 'features/erpmodules/planning/models/IMasterSchedule';
import { MasterScheduleService } from '../../services/MasterSchedule/masterschedule.service';
import { InventoryURLS } from '../../URLS/urls';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as MasterScheduleJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/MasterSchedule.json'

@Component({
  selector: 'app-MasterSchedule',
  templateUrl:'masterschedule.component.html',
  styleUrls:['masterschedule.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'MasterScheduleId' },
    { provide: 'url', useValue: InventoryURLS.MasterSchedule},
    { provide: 'DataService', useClass: MasterScheduleService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})

export class MasterScheduleComponent extends GBBaseDataPageComponentWN<IMasterSchedule> {
  title: string = "MasterSchedule"
  MasterScheduleJSON = MasterScheduleJSON;
  form: GBDataFormGroupWN<IMasterSchedule> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "MRPMaster", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.MasterScheduleFormVal(arrayOfValues.MasterScheduleId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public MasterScheduleFormVal(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(MasterSchedule => {
          this.form.patchValue(MasterSchedule);
      })
    }
  }
  
  public MasterSchedulepatch(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IMasterSchedule> {
  const dbds: GBBaseDBDataService<IMasterSchedule> = new GBBaseDBDataService<IMasterSchedule>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IMasterSchedule>, dbDataService: GBBaseDBDataService<IMasterSchedule>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IMasterSchedule> {
  return new GBDataPageService<IMasterSchedule>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
