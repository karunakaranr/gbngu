import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { UOMService } from 'features/erpmodules/inventory/services/UOM/uom.service';
import { Observable } from 'rxjs';
const col = require('./../../../../../../apps/Goodbooks/Goodbooks/src/assets/data/uomlist.json')

@Component({
  selector: 'app-uomlist',
  templateUrl: './uomlist.component.html',
  styleUrls: ['./uomlist.component.scss'],
})
export class UOMlistComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];
  public sharedData: string;

  constructor(public sharedService: SharedService, public store: Store, @Inject(LOCALE_ID) public locale: string) { }

  ngOnInit(): void {
    this.getUOMlist();
  }

  public getUOMlist() {

    this.sharedService.getRowInfo().subscribe(rowselection => {
      if (rowselection.length > 0) {
        console.log("viewseletionsetting.......", rowselection)
      }
    })
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
        this.rowData = data;
        console.log("UOM :", this.rowData)
        const updatedRowData = [];

        for (let data of this.rowData) {
          let updatedData = Object.assign({}, data); // Create a copy of the original object

          if (data.UOMType === 0) {
            updatedData.UOMType = "length";
          } else if (data.UOMType === 1) {
            updatedData.UOMType = "Weight";
          } else if (data.UOMType === 2) {
            updatedData.UOMType = "Time";
          } else if (data.UOMType === 3) {
            updatedData.UOMType = "Yarn Count";
          } else if (data.UOMType === 4) {
            updatedData.UOMType = "Capacity";
          } else if (data.UOMType === 5) {
            updatedData.UOMType = "Production";
          } else if (data.UOMType === 6) {
            updatedData.UOMType = "Others";
          }

          updatedRowData.push(updatedData);
        }

        // Replace the original rowData with the updated array
        this.rowData = updatedRowData;

        console.log("UOM :", this.rowData);
      }
    });
  }
}


