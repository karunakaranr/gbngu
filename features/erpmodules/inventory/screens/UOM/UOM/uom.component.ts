import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
// import { ServiceName } from ‘ServicePath’;
// import { ModelName } from ‘ModelPath’;
import { UOMService } from 'features/erpmodules/inventory/services/UOM/uom.service';
import { IUOM } from 'features/erpmodules/inventory/models/IUOM';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
import * as UomJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Uom.json'
// import { url} from ‘UrlPath’;
import { InventoryURLS } from 'features/erpmodules/inventory/URLS/urls';


@Component({
  selector: 'app-UOM',
  templateUrl: './uom.component.html',
  styleUrls: ['./uom.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'UOMId'},
    { provide: 'url', useValue: InventoryURLS.UOM },
    { provide: 'DataService', useClass: UOMService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class UOMComponent extends GBBaseDataPageComponentWN<IUOM > implements OnInit{
  title: string = 'Uom'
  UomJSON = UomJSON;  
  @Output() Onselectdata: EventEmitter<any> = new EventEmitter<string>();


  form: GBDataFormGroupWN<IUOM > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Uom', {}, this.gbps.dataService);
  ngOnInit(): void {
    this.thisConstructor();
    this.subscribeToFormChanges();
  }

  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.UOMRetrivalValue(arrayOfValues.UOMId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

  private subscribeToFormChanges(): void {
    this.form.valueChanges.subscribe(() => {
      console.log("UOMMM",this.form.value)
      this.Onselectdata.emit(this.form.value)
      // alert('Form value changed!');
    });
  }


  public UOMRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public UOMPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IUOM > {
  const dbds: GBBaseDBDataService<IUOM > = new GBBaseDBDataService<IUOM >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IUOM >, dbDataService: GBBaseDBDataService<IUOM >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IUOM > {
  return new GBDataPageService<IUOM >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
