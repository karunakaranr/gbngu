import { Component, OnInit } from "@angular/core";
import { AccountScheduleService } from "features/erpmodules/inventory/services/AccountScheduleList/AccountScheduleList.service";

@Component({
  selector: 'app-account-schedulelist',
  templateUrl: './AccountScheduleList.component.html',
  styleUrls: ['./AccountScheduleList.component.scss']
})
export class AccountSchedulelistComponent implements OnInit {

    accScheduleList: any[] = [];

    constructor(
        private accountScheduleService: AccountScheduleService
    ) { }

    ngOnInit() {
        this.getAccountScheduleList();
    }

    getAccountScheduleList(): void{
        this.accountScheduleService.getAccountScheduleList().subscribe(menuDetails => {
            const menuDet = menuDetails;
            this.accountScheduleService.rowService(menuDet).subscribe(res => {
                this.prepareTableData(res.ReportDetail);
            });
        });
    }

    prepareTableData(data: any[]): void {
        const final = {};
        const tableData = [];
        let accScheduleCodes = [];

        data.forEach((detail) =>{
            final[detail.AccountScheduleNatureName] = {
                accScheduleGroups: [],
                code: detail.AccountScheduleNatureName,
                name: '',
                ...final[detail.AccountScheduleNatureName]
            };
        
            final[detail.AccountScheduleNatureName].accScheduleGroups.push({
                code: detail.AccountScheduleCode,
                name: detail.AccountScheduleName
            });
        });

        accScheduleCodes = Object.keys(final);

        accScheduleCodes.forEach((accScheduleCode) => {
            tableData.push({
                code: final[accScheduleCode].code,
                name: final[accScheduleCode].name,
                bold: true
            });
            final[accScheduleCode].accScheduleGroups.forEach((accScheduleGroup) => {
                tableData.push({
                    code: accScheduleGroup.code,
                    name: accScheduleGroup.name,
                    bold: false
                });
            });  
        });

        this.accScheduleList = tableData;
    }
    
}





  