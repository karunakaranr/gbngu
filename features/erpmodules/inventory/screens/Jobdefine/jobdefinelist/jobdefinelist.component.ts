import { Component, OnInit } from "@angular/core";
import { DatePipe } from '@angular/common';
import { JobdefineService } from "features/erpmodules/inventory/services/jobdefine/jobdefine.service";

const col = require('./../../../../../../apps/Goodbooks/Goodbooks/src/assets/data/jobdefinelist.json')

@Component({
  selector: 'app-jobdefinelist',
  templateUrl: './jobdefinelist.component.html',
  styleUrls: ['./jobdefinelist.component.scss']
})
export class JobdefinelistComponent implements OnInit {
  public rowData = [];
  public columnData = [];

  constructor(private jobdefineService: JobdefineService){  }

  ngOnInit(): void {
    this.getJobdefinelist();
  }

  getJobdefinelist() {
    this.jobdefineService.getJobDefineList().subscribe(menuDetails => {
      const menuDet = menuDetails;
      this.jobdefineService.rowService(menuDet).subscribe(res => {
        this.columnData = col;
        const reportDetails = res.ReportDetail;
        reportDetails.forEach(reportDetail => {
          if(reportDetail.CriteriaConfigName === 'NONE'){
            reportDetail.CriteriaConfigName = '';
          }
          reportDetail.JobdefineLastRunTime = this.getFormattedDate(reportDetail.JobdefineLastRunTime);
        });
        this.rowData = reportDetails;
      });
      
    });
  }

  getFormattedDate(dateValue) {
    let datePipe = new DatePipe("en-US");
    return datePipe.transform(JSON.parse(dateValue.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy');
  }

}