import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { JobdefinelistComponent } from './jobdefinelist.component';

describe('BankbranchlistComponent', () => {
  let component: JobdefinelistComponent;
  let fixture: ComponentFixture<JobdefinelistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobdefinelistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobdefinelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
