import {
  GBDataStateAction,
  GBDataStateActionFactoryWN,
 
} from './../../../../../libs/gbdata/src/lib/store/gbdata.actions';
import {  ISelectListCriteria, } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
import { IUOM } from '../../models/IUOM';
import { GetAll, GetData, MoveFirst, MoveLast, MoveNext, MovePrev, SaveData, ClearData, DeleteData, AddData } from './UOM.actions';

export class UomStateActionFactory extends GBDataStateActionFactoryWN<IUOM> {
    GetData(id: number): GBDataStateAction<IUOM> {
      return new GetData(id);
    }
    GetAll(scl: ISelectListCriteria): GBDataStateAction<IUOM> {
      return new GetAll(scl);
    }
    MoveFirst(): GBDataStateAction<IUOM> {
      return new MoveFirst();
    }
    MoveNext(): GBDataStateAction<IUOM> {
      return new MoveNext();
    }
    MovePrev(): GBDataStateAction<IUOM> {
      return new MovePrev();
    }
    MoveLast(): GBDataStateAction<IUOM> {
      return new MoveLast();
    }
    AddData(payload: IUOM): GBDataStateAction<IUOM> {
      return new AddData(payload);
    }
    SaveData(payload: IUOM): GBDataStateAction<IUOM> {
      return new SaveData(payload);
    }
    ClearData(): GBDataStateAction<IUOM> {
      return new ClearData();
    }
    DeleteData(): GBDataStateAction<IUOM> {
      return new DeleteData();
    }
  }
