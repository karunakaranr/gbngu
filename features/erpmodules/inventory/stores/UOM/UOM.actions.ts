import { IUOM } from '../../models/IUOM';
import {
  GBDataStateAction,
 
} from './../../../../../libs/gbdata/src/lib/store/gbdata.actions';
import {  ISelectListCriteria, } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';

  const UOMEntityName = 'UOM';
  export class GetData extends GBDataStateAction<IUOM> {
    static readonly type = '[' + UOMEntityName + '] getData';
    constructor(public id: number) {
      super();
    }
  }
  export class SaveData extends GBDataStateAction<IUOM> {
    static readonly type = '[' + UOMEntityName + '] saveData';
    constructor(
      public payload: IUOM
    ) {
      super();
    }
  }
  export class AddData extends GBDataStateAction<IUOM> {
    static readonly type = '[' + UOMEntityName + '] addData';
    constructor(
      public payload: IUOM
    ) {
      super();
    }
  }
  export class GetAll extends GBDataStateAction<IUOM> {
    static readonly type = '[' + UOMEntityName + '] getAll';
    constructor(public scl: ISelectListCriteria) {
      super();
    }
  }
  export class GetAllAndMoveFirst extends GBDataStateAction<IUOM> {
    static readonly type = '[' + UOMEntityName + '] getAllAndMoveFirst';
    constructor(public scl: ISelectListCriteria) {
      super();
    }
  }
  export class MoveFirst extends GBDataStateAction<IUOM> {
    static readonly type = '[' + UOMEntityName + '] MoveFirst';
    constructor() {
      super();
    }
  }
  export class MovePrev extends GBDataStateAction<IUOM> {
    static readonly type = '[' + UOMEntityName + '] MovePrev';
    constructor() {
      super();
    }
  }
  export class MoveNext extends GBDataStateAction<IUOM> {
    static readonly type = '[' + UOMEntityName + '] MoveNext';
    constructor() {
      super();
    }
  }
  export class MoveLast extends GBDataStateAction<IUOM> {
    static readonly type = '[' + UOMEntityName + '] MoveLast';
    constructor() {
      super();
    }
  }
  export class ClearData extends GBDataStateAction<IUOM> {
    static readonly type = '[' + UOMEntityName + '] ClearData';
    constructor() {
      super();
    }
  }
  export class DeleteData extends GBDataStateAction<IUOM> {
    static readonly type = '[' + UOMEntityName + '] DeleteData';
    constructor() {
      super();
    }
  }

  export class FormEditOption {
    static readonly type = '[' + UOMEntityName + '] FormEditOption';
    constructor(public FormEdit) {
    }
  }


