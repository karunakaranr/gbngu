import { IModel } from '../../models/IModel';
import {
  GBDataStateAction,
 
} from './../../../../../libs/gbdata/src/lib/store/gbdata.actions';
import {  ISelectListCriteria, } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';


  const ModelEntityName = 'MODEL';
  export class GetData extends GBDataStateAction<IModel> {
    static readonly type = '[' + ModelEntityName + '] getData';
    constructor(public id: number) {
      super();
    }
  }
  export class SaveData extends GBDataStateAction<IModel> {
    static readonly type = '[' + ModelEntityName + '] saveData';
    constructor(
      public payload: IModel
    ) {
      super();
    }
  }
  export class AddData extends GBDataStateAction<IModel> {
    static readonly type = '[' + ModelEntityName + '] addData';
    constructor(
      public payload: IModel
    ) {
      super();
    }
  }
  export class GetAll extends GBDataStateAction<IModel> {
    static readonly type = '[' + ModelEntityName + '] getAll';
    constructor(public scl: ISelectListCriteria) {
      super();
    }
  }
  export class GetAllAndMoveFirst extends GBDataStateAction<IModel> {
    static readonly type = '[' + ModelEntityName + '] getAllAndMoveFirst';
    constructor(public scl: ISelectListCriteria) {
      super();
    }
  }
  export class MoveFirst extends GBDataStateAction<IModel> {
    static readonly type = '[' + ModelEntityName + '] MoveFirst';
    constructor() {
      super();
    }
  }
  export class MovePrev extends GBDataStateAction<IModel> {
    static readonly type = '[' + ModelEntityName + '] MovePrev';
    constructor() {
      super();
    }
  }
  export class MoveNext extends GBDataStateAction<IModel> {
    static readonly type = '[' + ModelEntityName + '] MoveNext';
    constructor() {
      super();
    }
  }
  export class MoveLast extends GBDataStateAction<IModel> {
    static readonly type = '[' + ModelEntityName + '] MoveLast';
    constructor() {
      super();
    }
  }
  export class ClearData extends GBDataStateAction<IModel> {
    static readonly type = '[' + ModelEntityName + '] ClearData';
    constructor() {
      super();
    }
  }
  export class DeleteData extends GBDataStateAction<IModel> {
    static readonly type = '[' + ModelEntityName + '] DeleteData';
    constructor() {
      super();
    }
  }


