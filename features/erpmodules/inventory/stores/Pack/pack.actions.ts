import { IPack } from '../../models/IPack';
import {
  GBDataStateAction,
 
} from './../../../../../libs/gbdata/src/lib/store/gbdata.actions';
import {  ISelectListCriteria, } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
const packEntityName = 'Pack';
export class GetData extends GBDataStateAction<IPack> {
  static readonly type = '[' + packEntityName + '] getData';
  constructor(public id: number) {
    super();
  }
}
export class SaveData extends GBDataStateAction<IPack> {
  static readonly type = '[' + packEntityName + '] saveData';
  constructor(
    public payload: IPack
  ) {
    super();
  }
}
export class AddData extends GBDataStateAction<IPack> {
  static readonly type = '[' + packEntityName + '] addData';
  constructor(
    public payload: IPack
  ) {
    super();
  }
}
export class GetAll extends GBDataStateAction<IPack> {
  static readonly type = '[' + packEntityName + '] getAll';
  constructor(public scl: ISelectListCriteria) {
    super();
  }
}
export class GetAllAndMoveFirst extends GBDataStateAction<IPack> {
  static readonly type = '[' + packEntityName + '] getAllAndMoveFirst';
  constructor(public scl: ISelectListCriteria) {
    super();
  }
}
export class MoveFirst extends GBDataStateAction<IPack> {
  static readonly type = '[' + packEntityName + '] MoveFirst';
  constructor() {
    super();
  }
}
export class MovePrev extends GBDataStateAction<IPack> {
  static readonly type = '[' + packEntityName + '] MovePrev';
  constructor() {
    super();
  }
}
export class MoveNext extends GBDataStateAction<IPack> {
  static readonly type = '[' + packEntityName + '] MoveNext';
  constructor() {
    super();
  }
}
export class MoveLast extends GBDataStateAction<IPack> {
  static readonly type = '[' + packEntityName + '] MoveLast';
  constructor() {
    super();
  }
}
export class ClearData extends GBDataStateAction<IPack> {
  static readonly type = '[' + packEntityName + '] ClearData';
  constructor() {
    super();
  }
}
export class DeleteData extends GBDataStateAction<IPack> {
  static readonly type = '[' + packEntityName + '] DeleteData';
  constructor() {
    super();
  }
}
