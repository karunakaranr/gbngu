import {
  GBDataStateAction,
  GBDataStateActionFactoryWN,
 
} from './../../../../../libs/gbdata/src/lib/store/gbdata.actions';
import {  ISelectListCriteria, } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
import { IPack } from '../../models/IPack';
import { GetAll, GetData, MoveFirst, MoveLast, MoveNext, MovePrev, SaveData, ClearData, DeleteData, AddData } from './pack.actions';

export class PackStateActionFactory extends GBDataStateActionFactoryWN<IPack> {
    GetData(id: number): GBDataStateAction<IPack> {
      return new GetData(id);
    }
    GetAll(scl: ISelectListCriteria): GBDataStateAction<IPack> {
      return new GetAll(scl);
    }
    MoveFirst(): GBDataStateAction<IPack> {
      return new MoveFirst();
    }
    MoveNext(): GBDataStateAction<IPack> {
      return new MoveNext();
    }
    MovePrev(): GBDataStateAction<IPack> {
      return new MovePrev();
    }
    MoveLast(): GBDataStateAction<IPack> {
      return new MoveLast();
    }
    AddData(payload: IPack): GBDataStateAction<IPack> {
      return new AddData(payload);
    }
    SaveData(payload: IPack): GBDataStateAction<IPack> {
      return new SaveData(payload);
    }
    ClearData(): GBDataStateAction<IPack> {
      return new ClearData();
    }
    DeleteData(): GBDataStateAction<IPack> {
      return new DeleteData();
    }
  }
