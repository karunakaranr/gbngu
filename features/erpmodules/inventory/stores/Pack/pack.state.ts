import { State, Action, StateContext } from '@ngxs/store';
import { IPack } from '../../models/IPack';
import { GBDataState, GBDataStateModel } from './../../../../../libs/gbdata/src/lib/store/gbdata.state';
import { PackService } from '../../services/Pack/pack.service';
import { Inject, Injectable } from '@angular/core';
import { GetAll, MoveFirst, MoveNext, MovePrev, MoveLast, GetData, SaveData, GetAllAndMoveFirst, ClearData, DeleteData, AddData } from './pack.actions';

  export class PackStateModel extends GBDataStateModel<IPack> {
  }

  @State<PackStateModel>({
    name: 'Packs',
    defaults: {
      data: [],
      areDataLoaded: false,
      currentData: {
        model: undefined,
        dirty: false,
        errors: '',
        status: ''
      },
      currentIndex: 0,
    },
  })
  @Injectable()
  export class PackState extends GBDataState<IPack> {
    constructor(@Inject(PackService) dataService: PackService) {
      super(dataService, null);
    }

    @Action(GetData)
    GetData(context: StateContext<GBDataStateModel<IPack>>,
      { id }: GetData
    ) {
      return super.Super_GetData(context, id.toString());
    }
    @Action(ClearData)
    ClearData(context: StateContext<GBDataStateModel<IPack>>) {
      return super.Super_ClearData(context);
    }
    @Action(GetAll)
    GetAll(context: StateContext<GBDataStateModel<IPack>>, {scl}: GetAll) {
      return super.Super_GetAll(context, {scl: scl});
    }
    @Action(GetAllAndMoveFirst)
    GetAllAndMoveFirst(context: StateContext<GBDataStateModel<IPack>>, {scl}: GetAll) {

    }
    @Action(MoveFirst)
    MoveFirst(context: StateContext<GBDataStateModel<IPack>>) {
      return super.Super_MoveFirst(context);
    }
    @Action(MovePrev)
    MovePrev(context: StateContext<GBDataStateModel<IPack>>) {
      return super.Super_MovePrev(context);
    }
    @Action(MoveNext)
    MoveNext(context: StateContext<GBDataStateModel<IPack>>) {
      return super.Super_MoveNext(context);
    }
    @Action(MoveLast)
    MoveLast(context: StateContext<GBDataStateModel<IPack>>) {
      return super.Super_MoveLast(context);
    }
    @Action(DeleteData)
    DeleteData(context: StateContext<GBDataStateModel<IPack>>) {
      return super.Super_DeleteData(context);
    }
    @Action(SaveData)
    SaveData(context: StateContext<GBDataStateModel<IPack>>,
      { payload, id }: { payload: any; id: any }
    ) {
      return super.Super_SaveData(context, {payload: payload, id: id});
    }
    @Action(AddData)
    AddData(context: StateContext<GBDataStateModel<IPack>>,
      { payload }: { payload: any }
    ) {
      return super.Super_AddData(context, {payload: payload});
    }
  }
