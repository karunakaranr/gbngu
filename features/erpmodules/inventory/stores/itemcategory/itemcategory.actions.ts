import { IItemCategory } from '../../models/Iitemcategory';
import {
  GBDataStateAction,
 
} from './../../../../../libs/gbdata/src/lib/store/gbdata.actions';
import {  ISelectListCriteria, } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
  const ItemCategoryEntityName = 'ItemCategory';
  export class GetData extends GBDataStateAction<IItemCategory> {
    static readonly type = '[' + ItemCategoryEntityName + '] getData';
    constructor(public id: number) {
      super();
    }
  }
  export class SaveData extends GBDataStateAction<IItemCategory> {
    static readonly type = '[' + ItemCategoryEntityName + '] saveData';
    constructor(
      public payload: IItemCategory
    ) {
      super();
    }
  }
  export class AddData extends GBDataStateAction<IItemCategory> {
    static readonly type = '[' + ItemCategoryEntityName + '] addData';
    constructor(
      public payload: IItemCategory
    ) {
      super();
    }
  }
  export class GetAll extends GBDataStateAction<IItemCategory> {
    static readonly type = '[' + ItemCategoryEntityName + '] getAll';
    constructor(public scl: ISelectListCriteria) {
      super();
    }
  }
  export class GetAllAndMoveFirst extends GBDataStateAction<IItemCategory> {
    static readonly type = '[' + ItemCategoryEntityName + '] getAllAndMoveFirst';
    constructor(public scl: ISelectListCriteria) {
      super();
    }
  }
  export class MoveFirst extends GBDataStateAction<IItemCategory> {
    static readonly type = '[' + ItemCategoryEntityName + '] MoveFirst';
    constructor() {
      super();
    }
  }
  export class MovePrev extends GBDataStateAction<IItemCategory> {
    static readonly type = '[' + ItemCategoryEntityName + '] MovePrev';
    constructor() {
      super();
    }
  }
  export class MoveNext extends GBDataStateAction<IItemCategory> {
    static readonly type = '[' + ItemCategoryEntityName + '] MoveNext';
    constructor() {
      super();
    }
  }
  export class MoveLast extends GBDataStateAction<IItemCategory> {
    static readonly type = '[' + ItemCategoryEntityName + '] MoveLast';
    constructor() {
      super();
    }
  }
  export class ClearData extends GBDataStateAction<IItemCategory> {
    static readonly type = '[' + ItemCategoryEntityName + '] ClearData';
    constructor() {
      super();
    }
  }
  export class DeleteData extends GBDataStateAction<IItemCategory> {
    static readonly type = '[' + ItemCategoryEntityName + '] DeleteData';
    constructor() {
      super();
    }
  }


