import {
    GBDataStateAction,
    GBDataStateActionFactoryWN,
   
  } from './../../../../../libs/gbdata/src/lib/store/gbdata.actions';
  import {  ISelectListCriteria, } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
  import { CycleClass } from '../../models/ICycleClass';
  import { GetAll, GetData, MoveFirst, MoveLast, MoveNext, MovePrev, SaveData, ClearData, DeleteData, AddData } from './CycleClass.action';
  
  export class CycleClassStateActionFactory extends GBDataStateActionFactoryWN<CycleClass> {
      GetData(id: number): GBDataStateAction<CycleClass> {
        return new GetData(id);
      }
      GetAll(scl: ISelectListCriteria): GBDataStateAction<CycleClass> {
        return new GetAll(scl);
      }
      MoveFirst(): GBDataStateAction<CycleClass> {
        return new MoveFirst();
      }
      MoveNext(): GBDataStateAction<CycleClass> {
        return new MoveNext();
      }
      MovePrev(): GBDataStateAction<CycleClass> {
        return new MovePrev();
      }
      MoveLast(): GBDataStateAction<CycleClass> {
        return new MoveLast();
      }
      AddData(payload: CycleClass): GBDataStateAction<CycleClass> {
        return new AddData(payload);
      }
      SaveData(payload: CycleClass): GBDataStateAction<CycleClass> {
        return new SaveData(payload);
      }
      ClearData(): GBDataStateAction<CycleClass> {
        return new ClearData();
      }
      DeleteData(): GBDataStateAction<CycleClass> {
        return new DeleteData();
      }
    }
  