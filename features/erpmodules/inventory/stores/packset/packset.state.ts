import { State, Action, StateContext } from '@ngxs/store';
import { Ipackset } from '../../models/Ipackset';
import { GBDataState, GBDataStateModel } from '@goodbooks/gbdata';
import { PacksetService } from '../../services/packset/packset.service';
import { Inject, Injectable } from '@angular/core';
import { GetAll, MoveFirst, MoveNext, MovePrev, MoveLast, GetData, SaveData, GetAllAndMoveFirst, ClearData, DeleteData, AddData } from './packset.action';

  export class PacksetStateModel extends GBDataStateModel<Ipackset> {
  }

  @State<PacksetStateModel>({
    name: 'PackSet',
    defaults: {
      data: [],
      areDataLoaded: false,
      currentData: {
        model: undefined,
        dirty: false,
        errors: '',
        status: ''
      },
      currentIndex: 0,
    },
  })
  @Injectable()
  export class PacksetState extends GBDataState<Ipackset> {
    constructor(@Inject(PacksetService) dataService: PacksetService) {
      super(dataService, null);
    }

    @Action(GetData)
    GetData(context: StateContext<GBDataStateModel<Ipackset>>,
      { id }: GetData
    ) {
      return super.Super_GetData(context, id.toString());
    }
    @Action(ClearData)
    ClearData(context: StateContext<GBDataStateModel<Ipackset>>) {
      return super.Super_ClearData(context);
    }
    @Action(GetAll)
    GetAll(context: StateContext<GBDataStateModel<Ipackset>>, {scl}: GetAll) {
      return super.Super_GetAll(context, {scl: scl});
    }
    @Action(GetAllAndMoveFirst)
    GetAllAndMoveFirst(context: StateContext<GBDataStateModel<Ipackset>>, {scl}: GetAll) {

    }
    @Action(MoveFirst)
    MoveFirst(context: StateContext<GBDataStateModel<Ipackset>>) {
      return super.Super_MoveFirst(context);
    }
    @Action(MovePrev)
    MovePrev(context: StateContext<GBDataStateModel<Ipackset>>) {
      return super.Super_MovePrev(context);
    }
    @Action(MoveNext)
    MoveNext(context: StateContext<GBDataStateModel<Ipackset>>) {
      return super.Super_MoveNext(context);
    }
    @Action(MoveLast)
    MoveLast(context: StateContext<GBDataStateModel<Ipackset>>) {
      return super.Super_MoveLast(context);
    }
    @Action(DeleteData)
    DeleteData(context: StateContext<GBDataStateModel<Ipackset>>) {
      return super.Super_DeleteData(context);
    }
    @Action(SaveData)
    SaveData(context: StateContext<GBDataStateModel<Ipackset>>,
      { payload, id }: { payload: any; id: any }
    ) {
      return super.Super_SaveData(context, {payload: payload, id: id});
    }
    @Action(AddData)
    AddData(context: StateContext<GBDataStateModel<Ipackset>>,
      { payload }: { payload: any }
    ) {
      return super.Super_AddData(context, {payload: payload});
    }
  }
