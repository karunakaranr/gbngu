import { Ipackset } from '../../models/Ipackset';
import { GBDataStateAction, ISelectListCriteria} from '@goodbooks/gbdata';

const packsetEntityName = 'PackSet';
export class GetData extends GBDataStateAction<Ipackset> {
  static readonly type = '[' + packsetEntityName + '] getData';
  constructor(public id: number) {
    super();
  }
}
export class SaveData extends GBDataStateAction<Ipackset> {
  static readonly type = '[' + packsetEntityName + '] saveData';
  constructor(
    public payload: Ipackset
  ) {
    super();
  }
}
export class AddData extends GBDataStateAction<Ipackset> {
  static readonly type = '[' + packsetEntityName + '] addData';
  constructor(
    public payload: Ipackset
  ) {
    super();
  }
}
export class GetAll extends GBDataStateAction<Ipackset> {
  static readonly type = '[' + packsetEntityName + '] getAll';
  constructor(public scl: ISelectListCriteria) {
    super();
  }
}
export class GetAllAndMoveFirst extends GBDataStateAction<Ipackset> {
  static readonly type = '[' + packsetEntityName + '] getAllAndMoveFirst';
  constructor(public scl: ISelectListCriteria) {
    super();
  }
}
export class MoveFirst extends GBDataStateAction<Ipackset> {
  static readonly type = '[' + packsetEntityName + '] MoveFirst';
  constructor() {
    super();
  }
}
export class MovePrev extends GBDataStateAction<Ipackset> {
  static readonly type = '[' + packsetEntityName + '] MovePrev';
  constructor() {
    super();
  }
}
export class MoveNext extends GBDataStateAction<Ipackset> {
  static readonly type = '[' + packsetEntityName + '] MoveNext';
  constructor() {
    super();
  }
}
export class MoveLast extends GBDataStateAction<Ipackset> {
  static readonly type = '[' + packsetEntityName + '] MoveLast';
  constructor() {
    super();
  }
}
export class ClearData extends GBDataStateAction<Ipackset> {
  static readonly type = '[' + packsetEntityName + '] ClearData';
  constructor() {
    super();
  }
}
export class DeleteData extends GBDataStateAction<Ipackset> {
  static readonly type = '[' + packsetEntityName + '] DeleteData';
  constructor() {
    super();
  }
}
