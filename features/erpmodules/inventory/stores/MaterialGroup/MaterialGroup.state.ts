// import { State, Action, StateContext, Selector } from '@ngxs/store';
// import { MaterialGroup } from '../../models/IMaterialGroup';
// import { GBDataState, GBDataStateModel } from './../../../../../libs/gbdata/src/lib/store/gbdata.state';
// import { MaterialGroupService } from '../../services/MaterialGroup/MaterialGroup.service';
// import { Inject, Injectable } from '@angular/core';
// import { GetAll, MoveFirst, MoveNext, MovePrev, MoveLast, GetData, SaveData, GetAllAndMoveFirst,ClearData, DeleteData, AddData  } from './MaterialGroup.action';


//   export class MaterialGroupStateModel extends GBDataStateModel<MaterialGroup> {
//   }

//   @State<MaterialGroupStateModel>({
//     name: 'MaterialGroup',
//     defaults: {
//       data: [],
//       areDataLoaded: false,
//       currentData: {
//         model: undefined,
//         dirty: false,
//         errors: '',
//         status: ''
//       },
//       currentIndex: 0,
//     },
//   })
//   @Injectable()
//   export class MaterialGroupState extends GBDataState<MaterialGroup> {
//     constructor(@Inject(MaterialGroupService) dataService: MaterialGroupService) {
//       super(dataService, null);
//     }

//     @Action(GetData)
//     GetData(context: StateContext<GBDataStateModel<MaterialGroup>>,
//       { id }: GetData
//     ) {
//       return super.Super_GetData(context, id.toString());
//     }
//     @Action(ClearData)
//     ClearData(context: StateContext<GBDataStateModel<MaterialGroup>>) {
//       return super.Super_ClearData(context);
//     }
//     @Action(GetAll)
//     GetAll(context: StateContext<GBDataStateModel<MaterialGroup>>, {scl}: GetAll) {
//       return super.Super_GetAll(context, {scl: scl});

//     }
//     @Action(GetAllAndMoveFirst)
//     GetAllAndMoveFirst(context: StateContext<GBDataStateModel<MaterialGroup>>, {scl}: GetAll) {

//     }
//     @Action(MoveFirst)
//     MoveFirst(context: StateContext<GBDataStateModel<MaterialGroup>>) {
//       return super.Super_MoveFirst(context);
//     }
//     @Action(MovePrev)
//     MovePrev(context: StateContext<GBDataStateModel<MaterialGroup>>) {
//       return super.Super_MovePrev(context);
//     }
//     @Action(MoveNext)
//     MoveNext(context: StateContext<GBDataStateModel<MaterialGroup>>) {
//       return super.Super_MoveNext(context);
//     }
//     @Action(MoveLast)
//     MoveLast(context: StateContext<GBDataStateModel<MaterialGroup>>) {
//       return super.Super_MoveLast(context);
//     }
//     @Action(DeleteData)
//     DeleteData(context: StateContext<GBDataStateModel<MaterialGroup>>) {
//       return super.Super_DeleteData(context);
//     }
//     @Action(SaveData)
//     SaveData(context: StateContext<GBDataStateModel<MaterialGroup>>,
//       { payload, id }: { payload: any; id: any }
//     ) {
//       return super.Super_SaveData(context, {payload: payload, id: id});
//     }
//     @Action(AddData)
//     AddData(context: StateContext<GBDataStateModel<MaterialGroup>>,
//       { payload }: { payload: any }
//     ) {
//       return super.Super_AddData(context, {payload: payload});
//     }
//   }
