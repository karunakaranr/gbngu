export interface IAssetSchedule {
    AssetScheduleId: number
    AssetScheduleCode: string
    AssetScheduleName: string
    AssetScheduleMainHead: string
    AssetScheduleSubHead: string
    AssetScheduleSection: string
    AssetScheduleIsExtraShift: number
    AssetScheduleStandardUsefullLife: string
    AssetScheduleActualUsefullLife: string
    AssetScheduleWDVPercentage: string
    AssetScheduleSLPercentage: string
    AssetScheduleRemarks: string
    AssetScheduleCreatedOn: string
    AssetScheduleModifiedOn: string
    AssetScheduleModifiedByName: string
    AssetScheduleCreatedByName: string
    AssetScheduleStatus: number
    AssetScheduleVersion: number
  }
  