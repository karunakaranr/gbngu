import { Inject, Injectable } from '@angular/core';
// import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { GBBaseDataServiceWN,GBBaseDBDataService } from 'libs/gbdata/src';

import { IDepreciation } from '../../models/IDepreciation';



 
@Injectable({
  providedIn: 'root'
})
export class DepreciationService extends GBBaseDataServiceWN<IDepreciation> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IDepreciation>) {
    super(dbDataService, 'DepreciationId');
  }
}
