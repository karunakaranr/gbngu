export class SalesURLS {
    public static rate = '/mms/Rate.svc/';
    public static PartyPriceCategory = '/mms/PartyPriceCategory.svc/';
    public static PriceCategory = '/mms/PriceCategory.svc/';
    public static PriceListType = "/mms/PriceListType.svc/";
    public static PaymentTerm = "/mms/PaymentTerm.svc/";
    public static TargetMaster = '/mms/Target.svc/';
    public static Counter = '/mms/Counter.svc/';
    public static CashDisCount = '/mms/CashDiscount.svc/';
    public static TargetShare = '/mms/TargetShare.svc/';
    public static TargetShareSave = '/mms/TargetShare.svc/SaveTargetShares/'
    public static ExpiryItemReturning = '/mms/MMHead.svc/GenerateProformaInvoice'
    public static Period = '/ads/Period.svc/'
    public static SalesInvoiceGeneration = '/as/PartyBranch.svc/'
    public static BizTransactionType = '/ads/BizTransactionType.svc/'

}