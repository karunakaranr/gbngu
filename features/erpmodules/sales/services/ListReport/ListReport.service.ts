import { Injectable } from '@angular/core';
import { ListReportDbService } from './../../dbservices/ListReport/ListReportDb.service';
import { ICriteriaDTODetail } from '../../models/IFilterDetail';
@Injectable({
  providedIn: 'root'
})
export class ListReport {
  constructor(public dbservice: ListReportDbService) { }

  public ListReportview(BiztransactionClassId : number,FilterData :ICriteriaDTODetail,ReportType : number){               
    return this.dbservice.picklist(BiztransactionClassId,FilterData,ReportType);
  }
}