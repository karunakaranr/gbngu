import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { ITargetMaster } from '../../models/ITargetMaster';


 
@Injectable({
  providedIn: 'root'
})
export class TargetMasterService extends GBBaseDataServiceWN<ITargetMaster> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ITargetMaster>) {
    super(dbDataService, 'TargetId');
  }
}
