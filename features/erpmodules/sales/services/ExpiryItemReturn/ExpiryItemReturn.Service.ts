import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IExpiryItemReturn } from '../../models/ExpiryItemReturn';

@Injectable({
  providedIn: 'root'
})
export class ExpiryItemReturnService extends GBBaseDataServiceWN<IExpiryItemReturn> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IExpiryItemReturn>) {
    super(dbDataService, 'PartyExpiryNameId');
  }
}
