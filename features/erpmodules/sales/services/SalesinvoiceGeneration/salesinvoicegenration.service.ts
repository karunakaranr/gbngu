import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { ISalesInvoiceGeneration } from '../../models/ISalesInvoiceGeneration';
 
@Injectable({
  providedIn: 'root'
})
export class SalesInvoiceGenerationService extends GBBaseDataServiceWN<ISalesInvoiceGeneration> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ISalesInvoiceGeneration>) {
    super(dbDataService, 'PartyBranchId');
  }
}
