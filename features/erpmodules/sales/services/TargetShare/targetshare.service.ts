import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { ITargetShare } from '../../models/ITargetShare';




 
@Injectable({
  providedIn: 'root'
})
export class TargetShareService extends GBBaseDataServiceWN<ITargetShare> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ITargetShare>) {
    super(dbDataService, 'TargetId');
  }
}
