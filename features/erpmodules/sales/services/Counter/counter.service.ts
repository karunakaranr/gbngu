import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { ICounter } from '../../models/ICounter';


 
@Injectable({
  providedIn: 'root'
})
export class CounterService extends GBBaseDataServiceWN<ICounter> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICounter>) {
    super(dbDataService, 'CounterId');
  }
}
