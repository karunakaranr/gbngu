import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';
import { ICriteriaDTODetail } from '../../models/IFilterDetail';
@Injectable({
    providedIn: 'root',
})
export class ListReportDbService {
    constructor(public http: GBHttpService) { }

    public picklist(BiztransactionClassId: number, FilterData: ICriteriaDTODetail, ReportType: number): Observable<any> {
        let criteria = {

            "SectionCriteriaList": [
                {
                    "SectionId": 0,
                    "AttributesCriteriaList": [{
                        "FieldName": "OUId",
                        "OperationType": 5,
                        "FieldValue": FilterData.SectionCriteriaList[0].AttributesCriteriaList[0].FieldValue,
                        "JoinType": 2,
                        "CriteriaAttributeName": FilterData.SectionCriteriaList[0].AttributesCriteriaList[0].CriteriaAttributeName,
                        "CriteriaAttributeValue": FilterData.SectionCriteriaList[0].AttributesCriteriaList[0].CriteriaAttributeValue,
                        "IsHeader": 0,
                        "IsCompulsory": 1,
                        "CriteriaAttributeId": FilterData.SectionCriteriaList[0].AttributesCriteriaList[0].CriteriaAttributeId,
                        "CriteriaAttributeType": 0,
                        "FilterType": 1
                    }, {
                        "FieldName": "BiztransactionClassId",
                        "OperationType": 5,
                        "FieldValue": BiztransactionClassId,
                        "JoinType": 2,
                        "CriteriaAttributeName": "BiztransactionClassName",
                        "IsHeader": 0,
                        "IsCompulsory": 1,
                        "CriteriaAttributeId": -1899999970,
                        "CriteriaAttributeType": 0,
                        "FilterType": 1
                    }, {
                        "FieldName": "PeriodFromDate",
                        "OperationType": 10,
                        "FieldValue": FilterData.SectionCriteriaList[0].AttributesCriteriaList[1].FieldValue,
                        "JoinType": 0,
                        "CriteriaAttributeName": "Period From",
                        "CriteriaAttributeValue": FilterData.SectionCriteriaList[0].AttributesCriteriaList[1].CriteriaAttributeValue,
                        "IsHeader": 0,
                        "IsCompulsory": 0,
                        "CriteriaAttributeId": FilterData.SectionCriteriaList[0].AttributesCriteriaList[1].CriteriaAttributeId,
                        "CriteriaAttributeType": 4,
                        "FilterType": 1
                    }, {
                        "FieldName": "PeriodToDate",
                        "OperationType": 11,
                        "FieldValue": FilterData.SectionCriteriaList[0].AttributesCriteriaList[2].FieldValue,
                        "JoinType": 0,
                        "CriteriaAttributeName": "Period To",
                        "CriteriaAttributeValue": FilterData.SectionCriteriaList[0].AttributesCriteriaList[2].CriteriaAttributeValue,
                        "IsHeader": 0,
                        "IsCompulsory": 0,
                        "CriteriaAttributeId": FilterData.SectionCriteriaList[0].AttributesCriteriaList[2].CriteriaAttributeId,
                        "CriteriaAttributeType": 4,
                        "FilterType": 1
                    }
                    ],
                    "OperationType": 0
                }
            ]

        }

        const url = '/mms/MMReports.svc/MMPendingRegisterListReport/?BizTransactionClassId=' + BiztransactionClassId + '&Type=' + ReportType;
        return this.http.httppost(url, criteria);
    }
}