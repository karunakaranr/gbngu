import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ShortNumberPipe } from 'libs/gbcommon/src/lib/short-number.pipe';

import { UicoreModule } from '@goodbooks/uicore';
import { UifwkUifwkmaterialModule } from '@goodbooks/uifwk/material';
import { NgxsModule } from '@ngxs/store';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { NgxsFormPluginModule } from '@ngxs/form-plugin';

import { TranslocoRootModule } from './../../transloco/transloco-root.module';
import { GbdataModule } from '@goodbooks/gbdata';
import { environment } from '../../environments/environment';
import { NgBreModule } from '@unisoft/ng-bre';

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GbchartModule } from '../../common/shared/screens/gbchart/gbchart.module';

import { EntitylookupModule } from './../../entitylookup/entitylookup.module';
import { geolocationModule } from './../../geolocation/geolocation.module'
import { NgSelectModule } from '@ng-select/ng-select';

import { AgGridModule } from 'ag-grid-angular';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list'
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';

import { MatTabsModule } from '@angular/material/tabs'
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';

import { GbgridModule } from 'features/gbgrid/components/gbgrid/gbgrid.module';
import { CommonReportModule } from './../../commonreport/commonreport.module';
import { SalesRoutingModule } from './sales.routing.module';
import { SalesComponent } from './sales.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MaterialModule } from 'features/common/material.module';
import { MatInputModule } from '@angular/material/input';
import { MMStatusComponent } from './screens/MMStatus/MMStatus.component';
import { SummaryListComponent } from './screens/SummaryList/SummaryList.component';
import { ItemWiseSummaryComponent } from './screens/ItemWiseSummary/ItemWiseSummary.component';
import { ListReportComponent } from './screens/ListReport/ListReport.component';
import { SamplescreensModule } from '../samplescreens/samplescreens.module';
import { sopackedsummaryComponent } from './screens/SalesOrderPacked/SOPackedSummary/sopackedsummary.component';
import { sopackeditemlevelComponent } from './screens/SalesOrderPackedItemLevel/SalesOrderPackedItemLevelSummary/sopackeditemlevel.component';
import { SalesEnquiryComponent } from './screens/PendingRegister/ItemWise/Enquiry/SalesEnquiry.component';
import { MarginsummarylistComponent } from './screens/Margin/Margin Summary/Marginsummarylist.component';
import { SalesQuotationWOItemComponent } from './screens/Register/Enquiry and Quotation WO Item/SalesQuotationWOItem.component';
import { PendingRegDetComponent } from './screens/PendingRegister/Details/PendingRegDet.component';
import { pendingPOStatusComponent } from './screens/Status Register/Pending/PendingPOStatus.component';
import { SalesComSummaryComponent } from './screens/Sales Comparison/Summary/SalesComSummary.component';
import { ScVendorStockComponent } from './screens/Status Register/SC Vendor Stock/ScVendorStock.component';
import { SalesComparisonComponent } from './screens/Sales Comparison/Sales Comparison Details/SalesComparison.component';
import { InvoiceComponent } from './screens/Register/Invoice With Instrument/Invoice.component';
import { margindetailComponent } from './screens/Margin/Margin Detail/margindetail.component';
import { PriceListTypeComponent } from './screens/PriceListType/pricelisttype.component';
import { TargetMasterComponent } from './screens/TargetMaster/targetmaster.component';
import { PriceCategoryComponent } from './screens/Pricecategory/Pricecategory.component';
 import { CounterComponent } from './screens/Counter/counter.component';
 import { CashDiscountComponent } from '../sales/screens/CashDiscount/cashdiscount.component';
import { RateComponent } from './screens/Rate/rate.component';
import { PartyPriceCategoryComponent } from './screens/PartyPriceCategory/PartyPriceCategory.component';
import { TargetShareComponent } from './screens/TargetShare/targetshare.component';
import { ExpiryItemReturnComponent } from './screens/ExpiryItemReturn/expiryitemreturn.component';
import { SalesInvoiceGenerationComponent } from './screens/salesinvoicegeneration/salesinvoicegeneration.component';
//  import { GstDetailsComponent } from './screens/Duty & Tax/GST/Details/GstDetails.component';
//  import { GstAbstractComponent } from './screens/Duty & Tax/GST/Abstract/GstAbstract.component';
//  import { GstDocItemComponent } from './screens/Duty & Tax/GST/Document Item/GstDocItem.Component';
//  import { HsnCodeWiseComponent } from './screens/Duty & Tax/GST/HSN Code Wise/HsnCodeWise.component';
//  import { OuwiseDetailComponent } from './screens/Duty & Tax/Ou wise Detail/OuwiseDetail.component';
//  import { OuwiseSummaryComponent } from './screens/Duty & Tax/Ou Wise Summary/OuwiseSummary.co

@NgModule({
  declarations: [
    SalesInvoiceGenerationComponent,
    ExpiryItemReturnComponent,
    SalesComparisonComponent,
    TargetShareComponent,
    PartyPriceCategoryComponent,
    RateComponent,
    CashDiscountComponent,
    CounterComponent,
    PriceCategoryComponent,
    TargetMasterComponent,
    PriceListTypeComponent,
    InvoiceComponent,
    margindetailComponent,
    SalesComponent,
    MMStatusComponent,
    SummaryListComponent,
    ItemWiseSummaryComponent,
    ListReportComponent,
    sopackedsummaryComponent,
    sopackeditemlevelComponent,
    SalesEnquiryComponent,
    MarginsummarylistComponent,
    SalesQuotationWOItemComponent,
    PendingRegDetComponent,
    pendingPOStatusComponent,
    SalesComSummaryComponent,
    ScVendorStockComponent,
    // GstDetailsComponent,
    // GstAbstractComponent,
    // GstDocItemComponent,
    // HsnCodeWiseComponent,
    // OuwiseDetailComponent,
    // OuwiseSummaryComponent,
    ],
  imports: [
    SamplescreensModule,
    CommonModule,
    SalesRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    UicoreModule,
    NgBreModule,
    UifwkUifwkmaterialModule,
    TranslocoRootModule,
    // NgxsModule.forFeature([
    //  EmployeeState]),
    // NgxsFormPluginModule.forRoot(),
    AgGridModule,
    GbgridModule,
    GbdataModule.forRoot(environment),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDvFSDxJXXR1nrdRDIzvp6DRZB9v649My8',
      libraries: ['places', 'geometry', 'drawing']
    }),
    AgmDirectionModule,
    NgxChartsModule,
    GbchartModule,
    EntitylookupModule,
    geolocationModule,
    NgSelectModule,
    MaterialModule,
    MatSortModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatListModule,
    MatTabsModule,
    MatCheckboxModule,
    MatTableModule,
    MatIconModule,
    MatExpansionModule,
    MatDividerModule,
    MatPaginatorModule,
    CommonReportModule,
    MatFormFieldModule,
    MatInputModule,

  ],
  exports: [
    SalesComponent,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
  ],
})
export class SalesModule {


}
