export interface IPriceListType {
    "PriceListTypeId ": number;
    " PriceListTypeCode ": string;
    " PriceListTypeName ": string;
    " ChargeId ": number;
    " ChargeName ": string;
    " PriceListTypeRemarks ": string;
    " PriceListTypeIsPurchase ": number;
    " PriceListTypeIsSales ": number;
    " PriceListTypeIsTransfer ": number;
    " CurrencyId ": number;
    " CurrencyCode ": string;
    " CurrencyName ": string;
    " PriceListTypeCreatedById ": number;
    " PriceListTypeCreatedByName ": string;
    " PriceListTypeCreatedOn ": string;
    " PriceListTypeModifiedById ": number;
    " PriceListTypeModifiedByName ": string;
    " PriceListTypeModifiedOn ": string;
    " PriceListTypeSortOrder ": number;
    " PriceListTypeStatus ": number;
    " PriceListTypeVersion ": number;
    " PriceListTypeSourceType ": number;
    " PriceListTypeTransferCostType ": number;
  }
  