export interface ITargetMaster {
    TargetId: number
    ItemLevelId: string
    TargetCode: string
    TargetName: string
    CostCategoryId: string
    AllocationTypeId: string
    TargetPeriodType: number
    TargetFromDate: string
    TargetToDate: string
    TargetCompareFromDate: string
    TargetCompareToDate: string
    TargetIsQuantity: number
    TargetIsCost: number
    TargetIsValue: number
    GBPeriodId: string
    CompareGBPeriodId: string
    SplitTypeId: string
    TargetVersion: number
    TargetCreatedOn: string
    TargetModifiedOn: string
    TargetModifiedByName: string
    TargetCreatedByName: string
    TargetStatus: number
    TargetRemarks: string
  }
  