  export interface IExpiryItemReturn {
    PartyId: number
    PartyName: string
    PartyCode: string
    BiztransactionId: number
    BiztransactionName: string
    BiztransactionCode: string
    Expirydays: number
    DocummentDate: string
  }
  