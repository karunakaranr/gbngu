
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-margindetail',
    templateUrl: './margindetail.component.html',
    styleUrls: ['./margindetail.component.scss'],
})
export class margindetailComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;


    public rowData: any;
    public ToatlVal; // genad total
    public ToatlCost;
    public ToatlRealisation;
    public ToatlMargin;
    public ToatlMarginCost;
    public ToatlMargiVal;


    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getmargindetail();
    }

    public getmargindetail() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;
            if (data.length > 0) {
            console.log("Margin Type Details :", this.rowData)

               this.ToatlVal=0; // genad total
               this.ToatlCost=0; 
               this.ToatlRealisation=0; 
               this.ToatlMargin=0; 
               this.ToatlMarginCost=0; 
               this.ToatlMargiVal=0; 

               for (let data of this.rowData) {
                this.ToatlVal = this.ToatlVal + data.Value;  // grand total field declared here BasicValue is field name in json
                this.ToatlCost = this.ToatlCost + data.Cost; 
                this.ToatlRealisation = this.ToatlRealisation + data.Realisation; 
                this.ToatlMargin = this.ToatlMargin + data.Margin; 
                this.ToatlMarginCost = this.ToatlMarginCost + data.MarginPercentCost; 
                this.ToatlMargiVal = this.ToatlMargiVal + data.MarginPercentSale; 
            }

            let grouping = this.rowData;

            var finalizedGrp = [];

            grouping.map((inputdata, index) => {


                finalizedGrp.push({
                    sno: index,
                    code: inputdata['ItemCode'],
                    Particulars: inputdata['ItemName'],
                    Qty: inputdata['Quantity'],
                    UOM:inputdata['UomCode'],
                    Value:inputdata['Value'],
                    Cost:inputdata['Cost'],
                    Realisation:inputdata['Realisation'],
                    Margin: inputdata['Margin'],
                    Margincos:inputdata['MarginPercentCost'],
                    Marginval:inputdata['MarginPercentSale'],

                    ItemCode: inputdata['Code'],//grouping heading
                    ItemName:inputdata['Name'],
                });
            });

            const final = {};
            finalizedGrp.forEach((detailData) => {
                final[detailData.ItemCode] = {
                    supplyGroups: {},       // create a variable name for group which we can store data
                    code: detailData.ItemCode,
                    Particulars: detailData.ItemName,
                    Qty: 0,
                    UOM:"",
                    Value:0,
                    Cost:0,
                    Realisation:0,
                    Margin:0,
                    Margincos:0,
                    Marginval:0,
                    ...final[detailData.ItemCode]

                };

                final[detailData.ItemCode].supplyGroups[detailData.sno] = {
                    //sno:detailData.sno,
                    code: detailData.code,
                    Particulars: detailData.Particulars,
                    Qty: detailData.Qty,
                    UOM:detailData.UOM,
                    Value:detailData.Value,
                    Cost:detailData.Cost,
                    Realisation:detailData.Realisation,
                    Margin:detailData.Margin,
                    Margincos:detailData.Margincos,
                    Marginval:detailData.Marginval,

                }
            });
            const finalgrouping = Object.keys(final);
            const tableData = [];

            finalgrouping.forEach((codeData) => {

                const accounts = Object.keys(final[codeData].supplyGroups); 
                let sumnetvalue = 0;
                let sumValue = 0;
                let sumcost = 0;
                let sumrealise = 0;
                let margin = 0;
                let margincos = 0;
                let marginval = 0;


                accounts.forEach(ag => {
                    sumnetvalue = sumnetvalue + parseFloat(final[codeData].supplyGroups[ag].Qty);
                    sumValue = sumValue + parseFloat(final[codeData].supplyGroups[ag].Value);
                    sumcost = sumcost + parseFloat(final[codeData].supplyGroups[ag].Cost);
                    sumrealise = sumrealise + parseFloat(final[codeData].supplyGroups[ag].Realisation);
                    margin = margin + parseFloat(final[codeData].supplyGroups[ag].Margin);
                    margincos = margincos + parseFloat(final[codeData].supplyGroups[ag].Margincos);
                    marginval = marginval + parseFloat(final[codeData].supplyGroups[ag].Marginval);
                })
                final[codeData].Qty = sumnetvalue;
                final[codeData].Value = sumValue;
                final[codeData].Cost = sumcost;
                final[codeData].Realisation = sumrealise;
                final[codeData].Margin = margin;
                final[codeData].Margincos = margincos;
                final[codeData].Marginval = marginval;

                tableData.push({
                    code: final[codeData].code,
                    Particulars: final[codeData].Particulars,
                    Qty: final[codeData].Qty,
                    UOM:final[codeData].UOM,
                    Value:final[codeData].Value,
                    Cost:final[codeData].Cost,
                    Realisation:final[codeData].Realisation,
                    Margin:final[codeData].Margin,
                    Margincos:final[codeData].Margincos,
                    Marginval:final[codeData].Marginval,
                    
                    bold: true,
                });

                accounts.forEach((account) => {
                    tableData.push({
                        code: final[codeData].supplyGroups[account].code,
                        Particulars: final[codeData].supplyGroups[account].Particulars,
                        Qty: final[codeData].supplyGroups[account].Qty,
                        UOM: final[codeData].supplyGroups[account].UOM,
                        Value: final[codeData].supplyGroups[account].Value,
                        Cost: final[codeData].supplyGroups[account].Cost,
                        Realisation: final[codeData].supplyGroups[account].Realisation,
                        Margin: final[codeData].supplyGroups[account].Margin,
                        Margincos: final[codeData].supplyGroups[account].Margincos,
                        Marginval: final[codeData].supplyGroups[account].Marginval,
                       

                    })
                })

            })

            this.rowData = tableData;
            console.log("Final Data:", this.rowData[0])
        }

        });


    }
}
