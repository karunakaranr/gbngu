
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';

@Component({
    selector: 'app-Marginsummarylistlist',
    templateUrl: './Marginsummarylist.component.html',
    styleUrls: ['./Marginsummarylist.component.scss'],
})
export class MarginsummarylistComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    
    public Value;
    public Cost;
    public Realisation;
    public Margin;
    public MarginCost;
    public MarginVal;


    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getMarginsummarylist();
    }

    public getMarginsummarylist() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;
            if (data.length > 0) {
            console.log("Margin Summary Details :", this.rowData)

            this.Value = 0;
            this.Cost = 0;
            this.Realisation = 0;
            this.Margin=0;
            this.MarginCost=0;
            this.MarginVal=0;

            for (let data of this.rowData) {
                this.Value = (parseFloat(this.Value) + parseFloat(data.FinalNetValue)).toFixed(2);
                this.Cost = (parseFloat(this.Cost) + parseFloat(data.FinalCost)).toFixed(2);
                this.Realisation = (parseFloat(this.Realisation) + parseFloat(data.FinalRealisation)).toFixed(2);
                this.Margin = (parseFloat(this.Margin) + parseFloat(data.FinalMargin)).toFixed(2);
                this.MarginCost = (parseFloat(this.MarginCost) + parseFloat(data.FinalMarginPercentCost)).toFixed(2);
                this.MarginVal = (parseFloat(this.MarginVal) + parseFloat(data.FinalMarginPercentSale)).toFixed(2);
            }


        }
        });
    }
}
