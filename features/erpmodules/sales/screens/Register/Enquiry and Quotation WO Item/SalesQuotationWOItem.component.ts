
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-SalesQuotationWOItem',
    templateUrl: './SalesQuotationWOItem.component.html',
    styleUrls: ['./SalesQuotationWOItem.component.scss'],
})
export class SalesQuotationWOItemComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getSalesQuotationWOItem();
    }

    public getSalesQuotationWOItem() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;
            console.log("Machine Type Details :", this.rowData)
            

            // let grouping = this.rowData;

            // var finalizedGrp = [];

            // grouping.map((inputdata, index) =>{

              

            //     finalizedGrp.push({
            //         sno:index,
            //         DocNo:inputdata['DocumentNumber'],
            //         DocDate:inputdata['DocumentDate'],
            //         Type:inputdata['BIZTransactionType'],
            //         RefNo:inputdata['ReferenceNumber'],
            //         RefDate:inputdata['ReferenceDate'],
            //         Department:inputdata['Department'],
            //         Person:inputdata['Incharge'],
            //         Payment:inputdata['PaymentTerms'],

            //         nature:inputdata['Party'] ,
            //         name:inputdata['Party'] ,
            //     });
            // });

            // const final = {};
            // finalizedGrp.forEach((detailData) => {
            //     final[detailData.nature.Code] ={
            //         supplyGroups:{},     
            //         DocNo:detailData.nature.Code,
            //         DocDate:detailData.name.Name,
            //         Type:"",
            //         RefNo:"",
            //         RefDate:"",
            //         Department:"",
            //         Person:"",
            //         Payment:"",
                    
            //         ...final[detailData.nature.Code]

            //     };

            //     final[detailData.nature.Code].supplyGroups[detailData.sno] = {
            //         //sno:detailData.sno,
            //         DocNo: detailData.DocNo,
            //         DocDate:detailData.DocDate,
            //         Type: detailData.Type,
            //         RefNo: detailData.RefNo,
            //         RefDate: detailData.RefDate,
            //         Department: detailData.Department,
            //         Person: detailData.Person,
            //         Payment: detailData.Payment,

                
            //     }
            // });
            // const finalgrouping = Object.keys(final);
            // const tableData = [];

            // finalgrouping.forEach((codeData) => {
            //     tableData.push({
            //         DocNo:final[codeData].DocNo,
            //         DocDate:final[codeData].DocDate,
            //         Type:"",
            //         RefNo:"",
            //         RefDate:"",
            //         Department:"",
            //         Person:"",
            //         Payment:"",
            //         bold: true,
            //     });

            //     const accounts = Object.keys(final[codeData].supplyGroups); 
            //     accounts.forEach((account) => {
            //         tableData.push({
            //             DocNo:final[codeData].supplyGroups[account].DocNo,
            //             DocDate:final[codeData].supplyGroups[account].DocDate,
            //             Type:final[codeData].supplyGroups[account].Type.Name,
            //             RefNo:final[codeData].supplyGroups[account].RefNo,
            //             RefDate:final[codeData].supplyGroups[account].RefDate,
            //             Department:final[codeData].supplyGroups[account].Department.Name,
            //             Person:final[codeData].supplyGroups[account].Person.Name,
            //             Payment:final[codeData].supplyGroups[account].Payment.Name,

            //         })
            //     })

            // })

            //  this.rowData=tableData;
            //  console.log("Final Data:",this.rowData[0])

        });

       
    }
}
