import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { RateService } from '../../services/Rate/rate.service';
import { IRate } from '../../models/IRate';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { IBaseField, IPickListDetail } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { SalesURLS } from '../../URLS/urls';
import * as RateJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/rate.json'



@Component({
  selector: 'app-rate',
  templateUrl: './rate.component.html',
  styleUrls: ['./rate.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'RateId' },
    { provide: 'url', useValue: SalesURLS.rate },
    { provide: 'DataService', useClass: RateService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class RateComponent extends GBBaseDataPageComponentWN<IRate> {
  title: string = "rate"
  RateJSON = RateJSON

  form: GBDataFormGroupWN<IRate> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "rate", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.RateRetrivalValue(arrayOfValues.RateId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }





  public RateRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(rate => {
        if (SelectedPicklistData != '0') {
          this.form.patchValue(rate);
        }
      })
    }
  }


  public RatePatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IRate> {
  const dbds: GBBaseDBDataService<IRate> = new GBBaseDBDataService<IRate>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IRate>, dbDataService: GBBaseDBDataService<IRate>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IRate> {
  return new GBDataPageService<IRate>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
