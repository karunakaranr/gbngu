import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
  selector: 'app-sopackeditemlevel',
  templateUrl: './sopackeditemlevel.component.html',
  styleUrls: ['./sopackeditemlevel.component.scss'],
})
export class sopackeditemlevelComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData:any;  
  
  constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string) { }
  ngOnInit(): void {
    this.getsopackeditemlevel();
  }
  
  public getsopackeditemlevel() {
    this.sharedService.getRowInfo().subscribe(data => {
      this.rowData = data;
      console.log("Sales Order Packed & Not Invoice - Item Level:", this.rowData)


      let json = this.rowData;
      var finalizedArray = []
      json.map((row,index)=> {
        finalizedArray.push({
          sno: index,
          dno: row['DocumentNumber'],
          ddate: row['DocumentDate'],
          parname: row['PartyName'],
          pcgno: row['PackingNumber'],
          packdate: row['PackingDate'],

          slno: row['SlNo'],
          icode: row['ItemCode'],
          iname: row['ItemName'],
          skuname: row['SKUName'],
          netwgt: row['NetWeight'],
          packno: row['PackNumber'],
          goodqty: row['GoodQty'],
          packnostatus: row['PackNumberStatus'],

        });
      });
      const final = {};
      finalizedArray.forEach(detail => {
        final[detail.dno] = {
          accounts: {},
          slno: detail.dno,
          icode: detail.ddate,
          iname: detail.parname,
          skuname: detail.pcgno,
          netwgt: detail.packdate,
          packno: "",
          goodqty: "",
          packnostatus: "",

          ...final[detail.dno],
        }
        final[detail.dno].accounts[detail.sno] = {
          slno: detail.slno,
          icode: detail.icode,
          iname: detail.iname,
          skuname: detail.skuname,
          netwgt: detail.netwgt,
          packno: detail.packno,
          goodqty: detail.goodqty,
          packnostatus: detail.packnostatus,
        }
      })
      const empcodes = Object.keys(final)
      const tableData = [];
      empcodes.forEach(code => {
        const account = Object.keys(final[code].accounts)

        tableData.push({
          slno: final[code].slno,
          icode: final[code].icode,
          iname: final[code].iname,
          skuname: final[code].skuname,
          netwgt: final[code].netwgt,
          bold: true,
        })
        account.forEach(ag => {
          tableData.push({
            slno: final[code].accounts[ag].slno,
            icode: final[code].accounts[ag].icode,
            iname: final[code].accounts[ag].iname,
            skuname: final[code].accounts[ag].skuname,
            netwgt: final[code].accounts[ag].netwgt,
            packno: final[code].accounts[ag].packno,
            goodqty: final[code].accounts[ag].goodqty,
            packnostatus: final[code].accounts[ag].packnostatus
          })
        })
      })
      this.rowData = tableData;
      console.log("tableData",tableData)
    });
  }
}
