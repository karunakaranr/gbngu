import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { SalesURLS } from '../../URLS/urls';
import { TargetShareService } from '../../services/TargetShare/targetshare.service';
import { ITargetShare } from '../../models/ITargetShare';
import * as TargetShareJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/TargetShare.json'
@Component({
    selector: 'app-TargetShare',
    templateUrl: './targetshare.component.html',
    styleUrls: ['./targetshare.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'TargetId' },
        { provide: 'url', useValue: SalesURLS.TargetShare },
        { provide: 'saveurl', useValue: SalesURLS.TargetShareSave },
        { provide: 'DataService', useClass: TargetShareService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class TargetShareComponent extends GBBaseDataPageComponentWN<ITargetShare[]> {
    title: string = "TargetShare"
    TargetShareJSON = TargetShareJSON

    form: GBDataFormGroupWN<ITargetShare[]> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "TargetShare", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.TargetSharePicklistFillValue(arrayOfValues.TargetId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }


    public TargetSharePicklistFillValue(SelectedPicklistData: string): void {
        console.log("FillFungtion", this.form.value)
        console.log("id1", SelectedPicklistData)
        if (SelectedPicklistData) {
            console.log("id2", SelectedPicklistData)

            let PostingParameterFieldName = "OUId";
            let PostingParameterFieldValue = "OUId"
            this.gbps.dataService.multiParameterGetData(SelectedPicklistData, PostingParameterFieldName, PostingParameterFieldValue).subscribe(TargetShare => {
                console.log("id3", SelectedPicklistData)
                // this.form.patchValue(TargetShare);
                this.form.get('TargetId').patchValue(TargetShare[0].TargetId)
                // this.form.get('OUName').patchValue(TargetShare[0].OUName)
                // this.form.get('TargetName').patchValue(TargetShare[0].TargetName)
                this.form.get('TargetShareDetailArray').patchValue(TargetShare)
            })
        }
    }



    public TargetSharePicklistPatchValue(SelectedPicklistDatas: any): void {
        console.log("id", SelectedPicklistDatas)
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
        console.log("Form Value:", this.form.value)
    }

}


export function getThisDBDataService(http: GBHttpService, url: string,saveurl: string,): GBBaseDBDataService<ITargetShare[]> {
    const dbds: GBBaseDBDataService<ITargetShare[]> = new GBBaseDBDataService<ITargetShare[]>(http);
    dbds.endPoint = url;
    dbds.saveurl = saveurl;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<ITargetShare[]>, dbDataService: GBBaseDBDataService<ITargetShare[]>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ITargetShare[]> {
    return new GBDataPageService<ITargetShare[]>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
