import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Observable } from 'rxjs';
import { FilterState } from 'features/gbfilter/store/gbfilter.state';
import { CommonReportdbservice } from 'features/commonreport/dbservice/commonreportdbservice';
import { DashboardFunction } from 'libs/gbcommon/src/lib/services/DashboardFunction/dashboard.service';
import { IStatusDetail } from '../../models/IStatusDetail';
import { ICriteriaDTODetail } from '../../models/IFilterDetail';
import { SharedService } from 'features/commonreport/service/datapassing.service';
@Component({
  selector: 'app-MMStatus',
  templateUrl: './MMStatus.component.html',
  styleUrls: ['./MMStatus.component.scss']
})

export class MMStatusComponent implements OnInit {
  @Select(ReportState.Sourcecriteriadrilldown) filtercriteria$: Observable<any>;
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  title = 'MMStatus '
  MMStatusData : IStatusDetail[]=[];
  BiztransactionClassName : string;
  BiztransactionClassId : number;
  filterData : ICriteriaDTODetail;
  ReportType : number;
  DocumentType: string[] = ['Documents', 'Quantity', 'Value'];
  Type: string[] = ['Party', 'Item'];
  Level: string[] = ['Summary', 'List'];
  dashboardview: boolean = true;
  DocumentTypemodel = { option: 'Documents' }
  Typemodel = { option: 'Party' };
  Levelmodel = { option: 'Summary' };
  RandomNumber: number[] = []
  RandomBackground: number[] = []
  constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string,public filterservice : CommonReportdbservice, public commonfunction :DashboardFunction) {
  }
  ngOnInit(): void {
    this.DataReceiver();
  }

  public DataReceiver() {
    // this.rowdatacommon$.subscribe(data => {
    //   this.MMStatusData = data
    //   console.log("MM Status Data:", this.MMStatusData)
    //   this.RandomNumber=this.commonfunction.Random(data.length)
    // });
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
        this.MMStatusData = data
        console.log("MM Status Data:", this.MMStatusData)
        this.RandomNumber=this.commonfunction.Random(data.length)
        this.RandomBackground=this.commonfunction.RandomBackground(data.length)
      }
    })
  }

  public SummaryAndListReportServiceCall(data:IStatusDetail, reportType:number) {
    this.BiztransactionClassName = data.BiztransactionClassName;
    this.BiztransactionClassId = data.BiztransactionClassId;
    this.ReportType = reportType;
    this.filtercriteria$.subscribe(filterdata => {
      this.filterData = filterdata.CriteriaDTO;
      console.log("Filter Data:",this.filterData);
      this.dashboardview=false;
    })
  }
}