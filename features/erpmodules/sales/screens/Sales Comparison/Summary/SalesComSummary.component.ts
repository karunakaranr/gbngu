
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-SalesComSummary',
    templateUrl: './SalesComSummary.component.html',
    styleUrls: ['./SalesComSummary.component.scss'],
})
export class SalesComSummaryComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;

    public TotalQty;
    public TotalRealisation;
    public TotalNetValue;
    public TotalCompQty;
    public TotalCompRealisation;
    public TotalCompnetValue;

    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getSalesComSummary();
    }

    public getSalesComSummary() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;
            if (data.length > 0) {
            console.log("Sales Comparison Summary Details :", this.rowData);

            this.TotalQty = 0;
            this.TotalRealisation = 0;
            this.TotalNetValue = 0;
            this.TotalCompQty = 0;
            this.TotalCompRealisation = 0;
            this.TotalCompnetValue = 0;

            for (let data of this.rowData){
               this.TotalQty = (parseFloat(this.TotalQty)+parseFloat(data.SalesQty)).toFixed(2);
               this.TotalRealisation = (parseFloat(this.TotalRealisation)+parseFloat(data.SalesRealisation)).toFixed(2);
               this.TotalNetValue = (parseFloat(this.TotalNetValue)+parseFloat(data.NetSalesValue)).toFixed(2);
               this.TotalCompQty = (parseFloat(this.TotalCompQty)+parseFloat(data.CompSalesQty)).toFixed(2);
               this.TotalCompRealisation = (parseFloat(this.TotalCompRealisation)+parseFloat(data.CompSalesRealisation)).toFixed(2);
               this.TotalCompnetValue = (parseFloat(this.TotalCompnetValue)+parseFloat(data.CompNetSalesValue)).toFixed(2);

            }

        }
        });
    }
}
