
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-SalesComparison',
    templateUrl: './SalesComparison.component.html',
    styleUrls: ['./SalesComparison.component.scss'],
})
export class SalesComparisonComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;


    public rowData: any;
    public ToatlVal; // genad total
    public ToatlCost;
    public ToatlRealisation;
    public Toatlpadding;
    public ToatlpaddingCost;
    public ToatlMargiVal;


    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getSalesComparison();
    }

    public getSalesComparison() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;
            if (data.length > 0) {
            console.log("Sales Comparison Details :", this.rowData)

               this.ToatlVal=0; // genad total
               this.ToatlCost=0; 
               this.ToatlRealisation=0; 
               this.Toatlpadding=0; 
               this.ToatlpaddingCost=0; 
               this.ToatlMargiVal=0; 

               for (let data of this.rowData) {
                this.ToatlVal = this.ToatlVal + data.SalesQty;  
                this.ToatlCost = this.ToatlCost + data.SalesRealisation; 
                this.ToatlRealisation = this.ToatlRealisation + data.NetSalesValue; 
                this.Toatlpadding = this.Toatlpadding + data.CompSalesQty; 
                this.ToatlpaddingCost = this.ToatlpaddingCost + data.CompSalesRealisation; 
                this.ToatlMargiVal = this.ToatlMargiVal + data.CompNetSalesValue; 
            }

            let grouping = this.rowData;

            var finalizedGrp = [];

            grouping.map((inputdata, index) => {

              

                finalizedGrp.push({
                    sno: index,
                    code: inputdata['ItemCode'],
                    Particulars: inputdata['ItemName'],
                    Qty:inputdata['SalesQty'],
                    Realisation:inputdata['SalesRealisation'],
                    NetValue:inputdata['NetSalesValue'],
                    CompQty: inputdata['CompSalesQty'],
                    CompRel:inputdata['CompSalesRealisation'],
                    ComNet:inputdata['CompNetSalesValue'],

                    ItemCode: inputdata['Code'],
                    ItemName:inputdata['Name'],
                });
            });

            const final = {};
            finalizedGrp.forEach((detailData) => {
                final[detailData.ItemCode] = {
                    supplyGroups: {},       
                    code: detailData.ItemCode,
                    Particulars: detailData.ItemName,
                   
                    Qty:0,
                    Realisation:0,
                    NetValue:0,
                    CompQty:0,
                    CompRel:0,
                    ComNet:0,
                    ...final[detailData.ItemCode]

                };

                final[detailData.ItemCode].supplyGroups[detailData.sno] = {
                    //sno:detailData.sno,
                    code: detailData.code,
                    Particulars: detailData.Particulars,
                  
                    Qty:detailData.Qty,
                    Realisation:detailData.Realisation,
                    NetValue:detailData.NetValue,
                    CompQty:detailData.CompQty,
                    CompRel:detailData.CompRel,
                    ComNet:detailData.ComNet,

                }
            });
            const finalgrouping = Object.keys(final);
            const tableData = [];

            finalgrouping.forEach((codeData) => {

                const accounts = Object.keys(final[codeData].supplyGroups); 
                
                let sumValue = 0;
                let sumcost = 0;
                let sumrealise = 0;
                let CompQty = 0;
                let CompRel = 0;
                let ComNet = 0;


                accounts.forEach(ag => {
                    
                    sumValue = sumValue + parseFloat(final[codeData].supplyGroups[ag].Qty);
                    sumcost = sumcost + parseFloat(final[codeData].supplyGroups[ag].Realisation);
                    sumrealise = sumrealise + parseFloat(final[codeData].supplyGroups[ag].NetValue);
                    CompQty = CompQty + parseFloat(final[codeData].supplyGroups[ag].CompQty);
                    CompRel = CompRel + parseFloat(final[codeData].supplyGroups[ag].CompRel);
                    ComNet = ComNet + parseFloat(final[codeData].supplyGroups[ag].ComNet);
                })
               
                final[codeData].Qty = sumValue;
                final[codeData].Realisation = sumcost;
                final[codeData].NetValue = sumrealise;
                final[codeData].CompQty = CompQty;
                final[codeData].CompRel = CompRel;
                final[codeData].ComNet = ComNet;

                tableData.push({
                    code: final[codeData].code,
                    Particulars: final[codeData].Particulars,
                    Qty:final[codeData].Qty,
                    Realisation:final[codeData].Realisation,
                    NetValue:final[codeData].NetValue,
                    CompQty:final[codeData].CompQty,
                    CompRel:final[codeData].CompRel,
                    ComNet:final[codeData].ComNet,
                    
                    bold: true,
                });

                accounts.forEach((account) => {
                    tableData.push({
                        code: final[codeData].supplyGroups[account].code,
                        Particulars: final[codeData].supplyGroups[account].Particulars,
                        Qty: final[codeData].supplyGroups[account].Qty,
                        Realisation: final[codeData].supplyGroups[account].Realisation,
                        NetValue: final[codeData].supplyGroups[account].NetValue,
                        CompQty: final[codeData].supplyGroups[account].CompQty,
                        CompRel: final[codeData].supplyGroups[account].CompRel,
                        ComNet: final[codeData].supplyGroups[account].ComNet,
                       

                    })
                })

            })

            this.rowData = tableData;
            console.log("Final Data:", this.rowData[0])
        }
        });


    }
}
