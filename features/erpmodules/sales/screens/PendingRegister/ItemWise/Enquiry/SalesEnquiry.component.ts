
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';


@Component({
    selector: 'app-SalesEnquiry',
    templateUrl: './SalesEnquiry.component.html',
    styleUrls: ['./SalesEnquiry.component.scss'],
})
export class SalesEnquiryComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;
    public TotalQuality;
    public PendingQuality;
    public Totalcost;
    public TotalValue;

    constructor() { }
    ngOnInit(): void {
        this.getSalesEnquiry();
    }

    public getSalesEnquiry() {
        this.rowdatacommon$.subscribe(data => {
            this.rowData = data;
            console.log("Deducted Category Details :", this.rowData);

            this.TotalQuality = 0;
            this.PendingQuality = 0;
            this.Totalcost = 0;
            this.TotalValue = 0;

            for (let data of this.rowData) {
                this.TotalQuality = this.TotalQuality + data.TransactionActualQuantity;
                this.PendingQuality = this.PendingQuality + data.PendingOrderFC;
                this.Totalcost = this.Totalcost + data.ItemPostedCost;
                this.TotalValue = this.TotalValue + data.PendingValue;
            }

            let groupingData = this.rowData;
            var finalizedGroup = [];

            groupingData.map((inputData, index) => {
                finalizedGroup.push({
                    sno: index,
                    docnum: inputData['DocumentNumber'],
                    docdate: inputData['DocumentDate'],
                    refnum: inputData['ReferenceNumber'],
                    refdate: inputData['ReferenceDate'],
                    actualquan: inputData['TransactionActualQuantity'],
                    pendorder: inputData['PendingOrderFC'],
                    uomcode: inputData['UOMCode'],
                    rate: inputData['Rate'],
                    postedcost: inputData['ItemPostedCost'],
                    penvalue: inputData['PendingValue'],
                    itemcode:inputData['ItemCode'],
                    itemname:inputData['ItemName'],
                    acccode:inputData['AccountCode'],
                    accname:inputData['AccountName'],

                });
            });

            const final = {};
            finalizedGroup.forEach((detailData) => {
                final[detailData.itemcode] = {
                    salesenqgroup: {},
                    sno: detailData.itemcode,
                    docnum: detailData.itemname,
                    docdate: "",
                    refnum: "",
                    refdate: "",
                    actualquan: 0,
                    pendorder: 0,
                    uomcode: "",
                    rate: 0,
                    postedcost: "",
                    penvalue:"",
                    

                    ...final[detailData.itemcode]

                };

                final[detailData.itemcode].salesenqgroup[detailData.acccode] = {
                    salesenqmultigrup: {},
                    sno: detailData.acccode,
                    docnum: detailData.accname,
                    docdate: "",
                    refnum: "",
                    refdate: "",
                    actualquan: 0,
                    pendorder: 0,
                    uomcode: "",
                    rate: 0,
                    postedcost: "",
                    penvalue:"",

                    ...final[detailData.itemcode].salesenqgroup[detailData.acccode],
                }

                final[detailData.itemcode].salesenqgroup[detailData.acccode].salesenqmultigrup[detailData.sno] = {
                    sno: detailData.sno,
                    docnum: detailData.docnum,
                    docdate: detailData.docdate,
                    refnum: detailData.refnum,
                    refdate: detailData.refdate,
                    actualquan: detailData.actualquan,
                    pendorder: detailData.pendorder,
                    uomcode: detailData.uomcode,
                    rate: detailData.rate,
                    postedcost: detailData.postedcost,
                    penvalue:detailData.penvalue,

                }
            });

            const dedGroups = Object.keys(final);

            const tableData = [];

            dedGroups.forEach((Details) => {
                const salesenqgroup = Object.keys(final[Details].salesenqgroup);
                let actualquan = 0;
                let pendorder = 0;
                let rate = 0;
                salesenqgroup.forEach((ag) => {
                    const salesenqmultigrup = Object.keys(final[Details].salesenqgroup[ag].salesenqmultigrup)
                    salesenqmultigrup.forEach((subtotal)=>{
                        actualquan = actualquan + parseFloat(final[Details].salesenqgroup[ag].salesenqmultigrup[subtotal].actualquan);
                        pendorder = pendorder + parseFloat(final[Details].salesenqgroup[ag].salesenqmultigrup[subtotal].pendorder);
                        rate = rate + parseFloat(final[Details].salesenqgroup[ag].salesenqmultigrup[subtotal].rate);
                    })
                })

                final[Details].actualquan = actualquan;
                final[Details].pendorder = pendorder;
                final[Details].rate = rate;

                tableData.push({
                    sno: final[Details].sno,
                    docnum: final[Details].docnum,
                    docdate: "",
                    refnum: "",
                    refdate: "",
                    actualquan: final[Details].actualquan,
                    pendorder: final[Details].pendorder,
                    uomcode: "",
                    rate: final[Details].rate,
                    postedcost: "",
                    penvalue:"",
                    bold: true,
                });



                salesenqgroup.forEach(ag => {
                    const salesenqmultigrup = Object.keys(final[Details].salesenqgroup[ag].salesenqmultigrup)
                    let actualquan = 0;
                    let pendorder = 0;
                    let rate = 0;
                    salesenqmultigrup.forEach(account => {
                        actualquan = actualquan + parseFloat(final[Details].salesenqgroup[ag].salesenqmultigrup[account].actualquan);
                        pendorder = pendorder + parseFloat(final[Details].salesenqgroup[ag].salesenqmultigrup[account].pendorder);
                        rate = rate + parseFloat(final[Details].salesenqgroup[ag].salesenqmultigrup[account].rate);
                    })
                    final[Details].actualquan = actualquan;
                    final[Details].pendorder = pendorder;
                    final[Details].uomcode = rate;

                    tableData.push({
                        sno: final[Details].salesenqgroup[ag].sno,
                        docnum: final[Details].salesenqgroup[ag].docnum,
                        docdate: "",
                        refnum: "",
                        refdate: "",
                        actualquan: final[Details].actualquan,
                        pendorder: final[Details].pendorder,
                        uomcode: "",
                        rate: final[Details].rate,
                        postedcost: "",
                        penvalue:"",
                        bold: true,
                    })


                    salesenqmultigrup.forEach(account => {
                        tableData.push({
                            sno: final[Details].salesenqgroup[ag].salesenqmultigrup[account].sno,
                            docnum: final[Details].salesenqgroup[ag].salesenqmultigrup[account].docnum,
                            docdate: final[Details].salesenqgroup[ag].salesenqmultigrup[account].docdate,
                            refnum: final[Details].salesenqgroup[ag].salesenqmultigrup[account].refnum,
                            refdate: final[Details].salesenqgroup[ag].salesenqmultigrup[account].refdate,
                            actualquan: final[Details].salesenqgroup[ag].salesenqmultigrup[account].actualquan,
                            pendorder: final[Details].salesenqgroup[ag].salesenqmultigrup[account].pendorder,
                            uomcode: final[Details].salesenqgroup[ag].salesenqmultigrup[account].uomcode,
                            rate: final[Details].salesenqgroup[ag].salesenqmultigrup[account].rate,
                            postedcost: final[Details].salesenqgroup[ag].salesenqmultigrup[account].postedcost,
                            penvalue: final[Details].salesenqgroup[ag].salesenqmultigrup[account].penvalue,

                        })
                    })
                })

            });
            this.rowData = tableData;

            console.log("Final Data:", this.rowData[0])



        });
    }
}
