
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { Select } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';

@Component({
    selector: 'app-PendingRegDet',
    templateUrl: './PendingRegDet.component.html',
    styleUrls: ['./PendingRegDet.component.scss'],
})
export class PendingRegDetComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: any;

    public PenQty;
    public Basic;
    public Realisation;
    public IP;
    public OP;
   



    constructor(public sharedService: SharedService) { }
    ngOnInit(): void {
        this.getPendingRegDet();
    }

    public getPendingRegDet() {
        this.sharedService.getRowInfo().subscribe(data => {
            this.rowData = data;
            if (data.length > 0) {
            console.log("Pending Register Details :", this.rowData); 

            
            this.PenQty = 0;
            this.Basic = 0;
            this.Realisation = 0;
            this.IP=0;
            this.OP=0;
           
            for (let data of this.rowData) {
                this.PenQty = (parseFloat(this.PenQty) + parseFloat(data.PendingQuantity)).toFixed(2);
                this.Basic = (parseFloat(this.Basic) + parseFloat(data.ItemPostedCost)).toFixed(2);
                this.Realisation = (parseFloat(this.Realisation) + parseFloat(data.ItemRealisationValueFC)).toFixed(2);
                this.IP = (parseFloat(this.IP) + parseFloat(data.InputValue)).toFixed(2);
                this.OP = (parseFloat(this.OP) + parseFloat(data.OutPutValue)).toFixed(2);
               
            }





            let groupingData = this.rowData;
            var finalizedGroup = [];


            groupingData.map((inputData, index) => {
                finalizedGroup.push({
                  sno: index,
                  PreDoc : inputData['PreDocumentNumber'],
                  T : inputData['StockPostType'],
                  Code : inputData['ItemCode'],
                  Name : inputData['ProductDescription'],
                  Qty : inputData['TransactionActualQuantity'],
                  PendQty : inputData['PendingQuantity'],
                  UOMCode : inputData['UOMCode'],
                  Rate : inputData['Rate'],
                  Basic : inputData['ItemPostedCost'],
                  Realisation : inputData['ItemRealisationValueFC'],
                  Ipval : inputData['InputValue'],
                  Opval : inputData['OutPutValue'],
                  NODays : inputData['NoOfDaysPending'],
                  Remarks : inputData['Remarks'],


                  Date: inputData['DocumentDate'],
                  DocumentNumber:inputData['DocumentNumber'],
                  PartyReferenceNumber:inputData['PartyReferenceNumber'],
                  PartyReferenceDate:inputData['PartyReferenceDate'],
                  PartyCode:inputData['PartyCode'],
                  PartyName:inputData['PartyName'],
                  CreatedByName:inputData['CreatedByName'],
                  ModifiedByName:inputData['ModifiedByName'],
                });
            });


            const final ={};
            finalizedGroup.forEach(detailData =>{
               final[detailData.Date] ={
                firstgrup:{},
                PreDoc : detailData.Date,
                T : "",
                Code :"",
                Name : "",
                Qty : "",
                PendQty : "",
                UOMCode : "",
                Rate : "",
                Basic : "",
                Realisation : "",
                Ipval : "",
                Opval : "",
                NODays : "",
                Remarks : "",


                ...final[detailData.Date]
               }; 

                final[detailData.Date].firstgrup[detailData.DocumentNumber] = {
                      Secondgrup: {},
                      PreDoc : detailData.DocumentNumber,
                      T : detailData.PartyReferenceNumber,
                      Code :detailData.PartyReferenceDate,
                      Name :detailData.PartyCode + " " +detailData.PartyName,
                      Qty : detailData.CreatedByName,
                      PendQty : detailData.ModifiedByName,
                      UOMCode : "",
                      Rate : "",
                      Basic : 0,
                      Realisation : 0,
                      Ipval : 0,
                      Opval : 0,
                      NODays : "",
                      Remarks : "",

                      ...final[detailData.Date].firstgrup[detailData.DocumentNumber],

                };
                final[detailData.Date].firstgrup[detailData.DocumentNumber].Secondgrup[detailData.sno] = {
                      PreDoc : detailData.PreDoc,
                      T : detailData.T,
                      Code :detailData.Code,
                      Name : detailData.Name,
                      Qty : detailData.Qty,
                      PendQty : detailData.PendQty,
                      UOMCode : detailData.UOMCode,
                      Rate : detailData.Rate,
                      Basic : detailData.Basic,
                      Realisation : detailData.Realisation,
                      Ipval : detailData.Ipval,
                      Opval : detailData.Opval,
                      NODays : detailData.NODays,
                      Remarks : detailData.Remarks,

                }


            }); 

            const dedGroups = Object.keys(final);

            const tableData = [];

            dedGroups.forEach((Details) => {
                const finalfirstgrup = Object.keys(final[Details].firstgrup);


                
                tableData.push({
                    PreDoc:final[Details].PreDoc,
                    T : "",
                    Code: "",
                    Name: "",
                    Qty: "",
                    PendQty: "",
                    UOMCode: "",
                    Rate: "",
                    Basic:"",
                    Realisation: "",
                    Ipval: "",
                    Opval:"",
                    NODays: "",
                    Remarks: "",
                    bold: true,
                });

                finalfirstgrup.forEach(ag => {
                    const finalsecondgrup = Object.keys(final[Details].firstgrup[ag].Secondgrup)
                    let actualquan = 0; 
                    let sumRealisation =0;
                    let sumIpval =0;
                    let sumOpval =0;
                    finalsecondgrup.forEach(account =>{
                        actualquan = actualquan + parseFloat(final[Details].firstgrup[ag].Secondgrup[account].Basic);
                        sumRealisation =sumRealisation +  parseFloat(final[Details].firstgrup[ag].Secondgrup[account].Realisation);
                        sumIpval =sumIpval +  parseFloat(final[Details].firstgrup[ag].Secondgrup[account].Ipval);
                        sumOpval =sumOpval +  parseFloat(final[Details].firstgrup[ag].Secondgrup[account].Opval);


                    });
                    final[Details].Basic = actualquan;
                    final[Details].Realisation = sumRealisation;
                    final[Details].Ipval = sumIpval;
                    final[Details].Opval = sumOpval;

                    tableData.push({
                        PreDoc:final[Details].firstgrup[ag].PreDoc,
                        T : final[Details].firstgrup[ag].T,
                        Code: final[Details].firstgrup[ag].Code,
                        Name: final[Details].firstgrup[ag].Name,
                        Qty: final[Details].firstgrup[ag].Qty,
                        PendQty: final[Details].firstgrup[ag].PendQty,
                        UOMCode: final[Details].firstgrup[ag].UOMCode,
                        Rate: "",
                        Basic: final[Details].Basic,
                        Realisation:final[Details].Realisation,
                        Ipval: final[Details].Ipval,
                        Opval:final[Details].Opval,
                        NODays: "",
                        Remarks: "",
                        bold: true,
                    });

                    finalsecondgrup.forEach(account =>{
                        tableData.push({
                            PreDoc:final[Details].firstgrup[ag].Secondgrup[account].PreDoc,
                            T:final[Details].firstgrup[ag].Secondgrup[account].T,
                            Code:final[Details].firstgrup[ag].Secondgrup[account].Code,
                            Name:final[Details].firstgrup[ag].Secondgrup[account].Name,
                            Qty:final[Details].firstgrup[ag].Secondgrup[account].Qty,
                            PendQty:final[Details].firstgrup[ag].Secondgrup[account].PendQty,
                            UOMCode:final[Details].firstgrup[ag].Secondgrup[account].UOMCode,
                            Rate:final[Details].firstgrup[ag].Secondgrup[account].Rate,
                            Basic:final[Details].firstgrup[ag].Secondgrup[account].Basic,
                            Realisation:final[Details].firstgrup[ag].Secondgrup[account].Realisation,
                            Ipval:final[Details].firstgrup[ag].Secondgrup[account].Ipval,
                            Opval:final[Details].firstgrup[ag].Secondgrup[account].Opval,
                            NODays:final[Details].firstgrup[ag].Secondgrup[account].NODays,
                            Remarks:final[Details].firstgrup[ag].Secondgrup[account].Remarks

                        })
                    })
                })

            });

            this.rowData = tableData;

            console.log("Final Data:", this.rowData[0])
        }
        });
    }
}
