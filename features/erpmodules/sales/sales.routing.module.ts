import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SalesComponent } from './sales.component';
import { MMStatusComponent } from './screens/MMStatus/MMStatus.component';
import { SummaryListComponent } from './screens/SummaryList/SummaryList.component';
import { ListReportComponent } from './screens/ListReport/ListReport.component';
import { ItemWiseSummaryComponent } from './screens/ItemWiseSummary/ItemWiseSummary.component';
import { sopackedsummaryComponent } from './screens/SalesOrderPacked/SOPackedSummary/sopackedsummary.component';
import { sopackeditemlevelComponent } from './screens/SalesOrderPackedItemLevel/SalesOrderPackedItemLevelSummary/sopackeditemlevel.component';
import { SalesEnquiryComponent } from './screens/PendingRegister/ItemWise/Enquiry/SalesEnquiry.component';
import { MarginsummarylistComponent } from './screens/Margin/Margin Summary/Marginsummarylist.component';
import { SalesQuotationWOItemComponent } from './screens/Register/Enquiry and Quotation WO Item/SalesQuotationWOItem.component';
import { PendingRegDetComponent } from './screens/PendingRegister/Details/PendingRegDet.component';
import { pendingPOStatusComponent } from './screens/Status Register/Pending/PendingPOStatus.component';
import { SalesComSummaryComponent } from './screens/Sales Comparison/Summary/SalesComSummary.component';
import { ScVendorStockComponent } from './screens/Status Register/SC Vendor Stock/ScVendorStock.component';
// import { GstDetailsComponent } from './screens/Duty & Tax/GST/Details/GstDetails.component';
// import { GstAbstractComponent } from './screens/Duty & Tax/GST/Abstract/GstAbstract.component';
// import { GstDocItemComponent } from './screens/Duty & Tax/GST/Document Item/GstDocItem.Component';
// import { HsnCodeWiseComponent } from './screens/Duty & Tax/GST/HSN Code Wise/HsnCodeWise.component';
// import { InvoiceComponent } from './screens/Register/Invoice With Instrument/Invoice.component';
// import { margindetailComponent } from './screens/Margin/Margin Detail/margindetail.component';
// import { SalesComparisonComponent } from './screens/Sales Comparison/Sales Comparison Details/SalesComparison.component';
// import { OuwiseSummaryComponent } from './screens/Duty & Tax/Ou Wise Summary/OuwiseSummary.component';import { PriceListTypeComponent } from './screens/PriceListType/pricelisttype.component';
import { PriceListTypeComponent } from './screens/PriceListType/pricelisttype.component';
import { TargetMasterComponent } from './screens/TargetMaster/targetmaster.component';
import { PriceCategoryComponent } from './screens/Pricecategory/Pricecategory.component';
 import { CounterComponent } from './screens/Counter/counter.component';
 import { CashDiscountComponent } from './screens/CashDiscount/cashdiscount.component';
 import { RateComponent } from './screens/Rate/rate.component';
import { PartyPriceCategoryComponent } from './screens/PartyPriceCategory/PartyPriceCategory.component';
import { TargetShareComponent } from './screens/TargetShare/targetshare.component';
import { ExpiryItemReturnComponent } from './screens/ExpiryItemReturn/expiryitemreturn.component';
import { SalesInvoiceGenerationComponent } from './screens/salesinvoicegeneration/salesinvoicegeneration.component';
const routes: Routes = [
  {
    path:'salesinvoicegeneration',
    component: SalesInvoiceGenerationComponent

  },
  {
    path:'expiryitemreturn',
    component: ExpiryItemReturnComponent

  },
  {
    path: '',
    component: SalesComponent
  },
  {
    path: 'targetshare',
    component: TargetShareComponent
  },
  {
    path: 'partypricecategory',
    component: PartyPriceCategoryComponent
  },
  {
    path: 'CashDiscount',
    component: CashDiscountComponent
  },
  {
    path: 'rate',
    component: RateComponent
  },
  {
    path: 'Pricecategory',
    component: PriceCategoryComponent
  },
  {
    path:'Counter',
    component:CounterComponent
  },
  {
    path: 'Targetmaster',
    component: TargetMasterComponent
  },
  {
    path: 'Pricelisttype',
    component: PriceListTypeComponent
  },
  {
    path: 'mmstatus',
    children: [
      {
        path: 'list',
        component: MMStatusComponent
      },
    ]
  },
  {
    path:'summarylist',
    component: SummaryListComponent
  },
  {
    path:'itemwisesummary',
    component: ItemWiseSummaryComponent
  },
  {
    path:'listreport',
    component: ListReportComponent
  },
  {
    path:'listreport',
    component: sopackedsummaryComponent
  },
  {
    path:'listreport',
    component: sopackeditemlevelComponent
  },
  {
    path:'SalesEnquiry',
    component: SalesEnquiryComponent
  },
  {
    path:'Marginsummarylist',
    component: MarginsummarylistComponent
  },
  {
    path:'SalesQuotationWOItem',
    component: SalesQuotationWOItemComponent
  },

  {
    path:'PendingRegDet',
    component: PendingRegDetComponent
  },

  {
    path:'pendingPOStatus',
    component: pendingPOStatusComponent
  },

  {
    path:'ScVendorStock',
    component: ScVendorStockComponent
  },
  {
    path:'SalesComSummary',
    component: SalesComSummaryComponent
  },
  
  
  // {
  //   path:'GstDetails',
  //   component: GstDetailsComponent
  // },

  // {
  //   path:'GstAbstract',
  //   component: GstAbstractComponent
  // },

  // {
  //   path:'GstDocItem',
  //   component: GstDocItemComponent
  // },

  // {
  //   path:'HsnCodeWise',
  //   component: HsnCodeWiseComponent
  // },


  // {
  //   path:'summarylist',
  //   component: SummaryListComponent
  // },
  // {
  //   path:'SalesComparison',
  //   component: SalesComparisonComponent
  // },
  // {
  //   path:'margindetail',
  //   component: margindetailComponent
  // },
  // {
  //   path:'OuwiseSummary',
  //   component: OuwiseSummaryComponent
  // },
  // {
  //   path:'Invoice',
  //   component: InvoiceComponent
  // },
  
  
  
  { path: 'themestest', loadChildren: () => import(`./../../themes/themes.module`).then(m => m.ThemesModule) },

 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesRoutingModule { }
