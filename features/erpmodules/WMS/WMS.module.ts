import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { UicoreModule } from '@goodbooks/uicore';
import { UifwkUifwkmaterialModule } from '@goodbooks/uifwk/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslocoRootModule } from './../../transloco/transloco-root.module';
import { GbdataModule } from '@goodbooks/gbdata';
import { environment } from '../../environments/environment';
import { NgBreModule } from '@unisoft/ng-bre';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GbchartModule } from '../../common/shared/screens/gbchart/gbchart.module';
import { EntitylookupModule } from './../../entitylookup/entitylookup.module';
import { geolocationModule } from './../../geolocation/geolocation.module';
import { NgSelectModule } from '@ng-select/ng-select';
import { AgGridModule } from 'ag-grid-angular';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import { GbgridModule } from 'features/gbgrid/components/gbgrid/gbgrid.module';
import { CommonReportModule } from './../../commonreport/commonreport.module';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MaterialModule } from 'features/common/material.module';
import { MatInputModule } from '@angular/material/input';
import { SamplescreensModule } from '../samplescreens/samplescreens.module';
import { BinTypeComponent } from './screens/Bintype/bintype.component';
import { WarehouseComponent } from './screens/Warehouse/warehouse.component';
import { BinComponent } from './screens/Bin/bin.component';

//Screens
import { WMSComponent } from './WMS.component';
import { WMSroutingModule } from './WMS.routing.module';
import { CaptureimagevideoComponent } from 'features/gbaudiovideo/component/audiovideo/audiovideo.component';
import { AudioRecordingService } from 'features/gbaudiovideo/service/audio-recording.service';
import { VideoRecordingService } from 'features/gbaudiovideo/service/video-recording.service';


@NgModule({
    declarations: [
      WMSComponent,
      BinTypeComponent,
      CaptureimagevideoComponent,
      WarehouseComponent,
      BinComponent
    ],
    imports: [
      SamplescreensModule,
      CommonModule,
      WMSroutingModule, 
      ReactiveFormsModule,
      FormsModule,
      UicoreModule,
      NgBreModule,
      UifwkUifwkmaterialModule,
      TranslocoRootModule,
      AgGridModule,
      GbgridModule,
      GbdataModule.forRoot(environment),
      AgmCoreModule.forRoot({
        apiKey: 'AIzaSyDvFSDxJXXR1nrdRDIzvp6DRZB9v649My8',
        libraries: ['places', 'geometry', 'drawing'],
      }),
      AgmDirectionModule,
      NgxChartsModule,
      GbchartModule,
      EntitylookupModule,
      geolocationModule,
      NgSelectModule,
      MaterialModule,
      MatSortModule,
      MatFormFieldModule,
      MatSelectModule,
      MatButtonModule,
      MatListModule,
      MatTabsModule,
      MatCheckboxModule,
      MatTableModule,
      MatIconModule,
      MatExpansionModule,
      MatDividerModule,
      MatPaginatorModule,
      CommonReportModule,
      MatFormFieldModule,
      MatInputModule,
    ],
    exports: [
      WMSComponent,
      MatSortModule,
      MatFormFieldModule,
      MatInputModule,
    ],
    providers: [AudioRecordingService, VideoRecordingService],

  })
  export class WMSModule {
    constructor() {}
  }