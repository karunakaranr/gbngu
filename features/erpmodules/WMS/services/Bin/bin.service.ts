import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IBin } from '../../models/IBin';


 
@Injectable({
  providedIn: 'root'
})
export class BinService extends GBBaseDataServiceWN<IBin> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IBin>) {
    super(dbDataService, 'BinId');
  }
}

