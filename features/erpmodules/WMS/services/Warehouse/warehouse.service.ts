import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IWarehouse } from '../../models/IWarehouse';

 
@Injectable({
  providedIn: 'root'
})
export class WarehouseService extends GBBaseDataServiceWN<IWarehouse> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IWarehouse>) {
    super(dbDataService, 'WareHouseId');
  }
}
