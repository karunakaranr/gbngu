export interface IBin {
    BinId: number
    BinNumber: string
    BinWareHouseAreaId: number
    BinStorageSectionId: number
    BinCubage: number
    BinBinStatus: number
    BinRemarks: string
    WareHouseId: number
    WareHouseCode: string
    WareHouseName: string
    BinTypeId: number
    BinTypeCode: string
    BinTypeName: string
    BinSortOrder: number
    BinStatus: number
    BinVersion: number
    BinCreatedById: number
    BinCreatedOn: string
    BinCreatedByName: string
    BinModifiedById: number
    BinModifiedByName: string
    BinModifiedOn: string
  }