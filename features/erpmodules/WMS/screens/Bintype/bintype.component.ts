import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { BintypeService } from '../../services/Bintype/bintype.service';
import { IBintype } from '../../models/IBintype';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as BintypeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Bintype.json';
import { WMSURLS } from 'features/erpmodules/WMS/urls/url';


@Component({
  selector: 'app-bintype', 
  templateUrl: './bintype.component.html',
  styleUrls: ['./bintype.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'BinTypeId'},
    { provide: 'url', useValue: WMSURLS.bintype },
    { provide: 'DataService', useClass: BintypeService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class BinTypeComponent extends GBBaseDataPageComponentWN<IBintype> {
  title: string = "Bintype"
  BintypeJSON = BintypeJSON; 


  form: GBDataFormGroupWN<IBintype > = new GBDataFormGroupWN(this.gbps.gbhttp.http, "Bintype", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.BinTypeRetrivalValue(arrayOfValues.BinTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public BinTypeMaxWeightDeciVal(theVal:any){
    console.log("theVal**",theVal)
    this.form.get('BinTypeMaxWeight').patchValue(theVal); 
  }
  public BinTypeTotalCapacitytDeciVal(theValue:any){
    console.log("theValue",theValue);
    this.form.get('BinTypeTotalCapacity').patchValue(theValue);
  }

  public BinTypeRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(Bintype => {
        this.form.patchValue(Bintype);
      })
    }
  }


  public BinTypePatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IBintype > {
  const dbds: GBBaseDBDataService<IBintype > = new GBBaseDBDataService<IBintype >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IBintype >, dbDataService: GBBaseDBDataService<IBintype >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IBintype > {
  return new GBDataPageService<IBintype >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


