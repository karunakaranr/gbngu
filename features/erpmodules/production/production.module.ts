import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ShortNumberPipe } from 'libs/gbcommon/src/lib/short-number.pipe';

import { UicoreModule } from '@goodbooks/uicore';
import { UifwkUifwkmaterialModule } from '@goodbooks/uifwk/material';
import { NgxsModule } from '@ngxs/store';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { NgxsFormPluginModule } from '@ngxs/form-plugin';

import { TranslocoRootModule } from './../../transloco/transloco-root.module';
import { GbdataModule } from '@goodbooks/gbdata';
import { environment } from '../../environments/environment';
import { NgBreModule } from '@unisoft/ng-bre';

import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GbchartModule } from '../../common/shared/screens/gbchart/gbchart.module';

import { EntitylookupModule } from './../../entitylookup/entitylookup.module';
import { geolocationModule } from './../../geolocation/geolocation.module'
import { NgSelectModule } from '@ng-select/ng-select';

import { AgGridModule } from 'ag-grid-angular';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list'
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatExpansionModule } from '@angular/material/expansion';

import { MatTabsModule } from '@angular/material/tabs'
import { MatCheckboxModule } from '@angular/material/checkbox';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';

import { GbgridModule } from 'features/gbgrid/components/gbgrid/gbgrid.module';
import { CommonReportModule } from './../../commonreport/commonreport.module';
import { ProductionRoutingModule } from './production.routing.module';
import { ProductionComponent } from './production.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MaterialModule } from 'features/common/material.module';
import { MatInputModule } from '@angular/material/input';
import { SamplescreensModule } from '../samplescreens/samplescreens.module';
import { WorkOrderDashboardComponent } from './screens/WorkOrderDashboard/WorkOrderDashboard.component';
import { WorkOrderSummaryComponent } from './screens/WorkOrderSummary/WorkOrderSummary.component';
import { WorkOrderListComponent } from './screens/WorkOrderList/WorkOrderList.component';
import { ProductionSummaryComponent } from './screens/Production/Summary/ProductionSummary.component';
import { StoppageDetailComponent } from './screens/Stoppage/StoppageDetail.component';
// import { ConsolidatedShortageComponent } from './screens/Shortage/ConsolidatedShortage.component';
import { ProdRegisterReportComponent } from './screens/Register/ProdRegisterReport.component';
import { WIPStockViewComponent } from './screens/Production/WIP Stock/WIPStockView.component';
import { WorkCenterComponent } from './screens/WorkCenter/WorkCenter.component';
import { MachineWiseProdComponent } from './screens/Production/Machine Wise Production/MachineWiseProd.component';
import { ResourceTypeComponent } from './screens/ResourceType/ResourceType.component';
import { ProcessMasterComponent } from './screens/ProcessMaster/ProcessMaster.component';
import { DefectComponent } from './screens/Defect/Defect.component';
import { CountMasterComponent } from './screens/CountMaster/CountMaster.component';
import { MachineTypeComponent } from './screens/MachineType/MachineType.component';
import { ProcessTypeComponent } from './screens/ProcessType/ProcessType.component';
import { ParameterSetComponent } from './screens/ParameterSet/ParameterSet.component';
import { DieMasterComponent } from './screens/DieMaster/DieMaster.component';
import { FloorPlanComponent } from './screens/FloorPlan/FloorPlan.component';

// import { PatternComponent } from './screens/Pattern/Pattern.component';
@NgModule({
  declarations: [
    StoppageDetailComponent,
    // PatternComponent,
    DieMasterComponent,
    WorkCenterComponent,
    ProductionSummaryComponent,
    WorkOrderListComponent,
    WorkOrderSummaryComponent,
    ProductionComponent,
    WorkOrderDashboardComponent,
    // ConsolidatedShortageComponent
    ProdRegisterReportComponent,
    WIPStockViewComponent,
    MachineWiseProdComponent,
    ResourceTypeComponent,
    ProcessMasterComponent,
    DefectComponent,
    CountMasterComponent,
    MachineTypeComponent,
    ProcessTypeComponent,
    ParameterSetComponent,
    FloorPlanComponent
 
  
  ],
  imports: [
    SamplescreensModule,
    CommonModule,
    ProductionRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    UicoreModule,
    NgBreModule,
    UifwkUifwkmaterialModule,
    TranslocoRootModule,
    // NgxsModule.forFeature([
    //  EmployeeState]),
    // NgxsFormPluginModule.forRoot(),
    AgGridModule,
    GbgridModule,
    GbdataModule.forRoot(environment),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDvFSDxJXXR1nrdRDIzvp6DRZB9v649My8',
      libraries: ['places', 'geometry', 'drawing']
    }),
    AgmDirectionModule,
    NgxChartsModule,
    GbchartModule,
    EntitylookupModule,
    geolocationModule,
    NgSelectModule,
    MaterialModule,
    MatSortModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatListModule,
    MatTabsModule,
    MatCheckboxModule,
    MatTableModule,
    MatIconModule,
    MatExpansionModule,
    MatDividerModule,
    MatPaginatorModule,
    CommonReportModule,
    MatFormFieldModule,
    MatInputModule,

  ],
  exports: [ 
    ProductionComponent,
    MatSortModule,
    MatFormFieldModule,
    MatInputModule,
  ],
})
export class ProductionModule {


}
