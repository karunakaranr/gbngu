export interface IProcessType {
    ProcessTypeId: number
    ProcessTypeCode: string
    ProcessTypeName: string
    ProcessTypeCharges: string
    ProcessTypeVersion: number
    ProcessTypeCreatedOn: string
    ProcessTypeModifiedOn: string
    ProcessTypeModifiedByName: string
    ProcessTypeCreatedByName: string
    ProcessTypeStatus: number
    ProcessTypeDetailArray: ProcessTypeDetailArray[]
  }
  
  export interface ProcessTypeDetailArray {
    ProcessTypeDetailSlNo: number
    undefined: string
    ProcessId: string
    ProcessName: string
  }