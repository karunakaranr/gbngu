export class IMachineType {
    MachineTypeId: number
    MachineTypeCode: string
    MachineTypeName: string
    MachineTypeNature: number
    ParameterSetId: number
    ParameterSetCode: string
    ParameterSetName: string
    ResourceTypeId: number
    ResourceTypeCode: string
    ResourceTypeName: string
    MachineTypeStandardProductionFactor: number
    MachineTypeCreatedById: number
    MachineTypeCreatedOn: string
    MachineTypeModifiedById: number
    MachineTypeModifiedOn: string
    MachineTypeCreatedByName: string
    MachineTypeModifiedByName: string
    MachineTypeSortOrder: number
    MachineTypeStatus: number
    MachineTypeVersion: number
    }