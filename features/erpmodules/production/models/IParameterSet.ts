export interface IParameterSet {
    ParameterSetId: number
    ParameterSetCode: string
    ParameterSetName: string
    ParameterSetRemarks: string
    ParameterSetCreatedOn: string
    ParameterSetModifiedOn: string
    ParameterSetModifiedByName: string
    ParameterSetCreatedByName: string
    ParameterSetStatus: number
    ParameterSetVersion: number
    ParameterSetDetailArray: ParameterSetDetailArray[]
  }
  
  export interface ParameterSetDetailArray {
    ParameterSetDetailSlno: number
    undefined: string
    ParameterId: string
    ForItemDuplicate: string
    ParameterName: string
    ParameterSetDetailSection: string
    ParameterSetDetailRejectionType: string
  }
  