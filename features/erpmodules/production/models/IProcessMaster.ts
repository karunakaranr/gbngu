export class IProcessMaster {
    ProcessId: number
  ProcessCode: string
  ProcessName: string
  ProcessCharges: string
  UomId: number
  ProcessType: number
  ProcessCalcType: number
  ParentProcessId: number
  ProcessRateType: string
  PayAccountId: number
  ReceiveAccountId: number
  ProcessRemarks: string
  ProcessStatus: number
  GSTTariffId: number
  GSTTableId: string
  ProcessVersion: number
  ProcessIsDie: number
}