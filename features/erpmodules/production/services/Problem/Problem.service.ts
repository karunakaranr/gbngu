import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IModel } from 'features/erpmodules/inventory/models/IModel';


// import { ModelName } from ‘ModelPath’;
import { IProblem } from '../../models/IProblem';

 
@Injectable({
  providedIn: 'root'
})
export class ProblemService extends GBBaseDataServiceWN<IProblem> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IProblem>) {
    super(dbDataService, 'ProblemId');
  }
}
