import { Injectable } from '@angular/core';
import { WorkOrderSummaryDbService } from './../../dbservices/WorkOrderSummary/WorkOrderSummaryDb.service';
import { ICriteriaDTODetail } from '../../models/IFilterDetail';
@Injectable({
  providedIn: 'root'
})
export class WorkOrderSummary {
  constructor(public dbservice: WorkOrderSummaryDbService) { }

  public WorkOrderSummaryview(FilterData : ICriteriaDTODetail,ReportType: number){               
    return this.dbservice.picklist(FilterData,ReportType);
  }
}