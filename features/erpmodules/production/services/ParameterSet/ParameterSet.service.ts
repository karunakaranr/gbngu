import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


// import { ModelName } from ‘ModelPath’;
import { IParameterSet } from '../../models/IParameterSet';


 
@Injectable({
  providedIn: 'root'
})
export class ParameterSetService extends GBBaseDataServiceWN<IParameterSet> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IParameterSet>) {
    super(dbDataService, 'ParameterSetId');
  }
}
