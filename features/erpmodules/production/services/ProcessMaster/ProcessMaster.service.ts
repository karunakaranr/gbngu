import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


// import { ModelName } from ‘ModelPath’;
import { IProcessMaster } from '../../models/IProcessMaster';


 
@Injectable({
  providedIn: 'root'
})
export class ProcessMasterService extends GBBaseDataServiceWN<IProcessMaster> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IProcessMaster>) {
    super(dbDataService, 'ProcessId');
  }
}
