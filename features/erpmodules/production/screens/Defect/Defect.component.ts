import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { DefectService } from '../../services/Defect/Defect.service';
import { IDefect } from '../../models/IDefect';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as DefectJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Defect.json'
import {URLS} from 'features/erpmodules/production/URLS/urls'


@Component({
  selector: 'app-Defect',
  templateUrl: './Defect.component.html',
  styleUrls: ['./Defect.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'DefectId'},
    { provide: 'url', useValue: URLS.Defect },
    { provide: 'DataService', useClass: DefectService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class DefectComponent extends GBBaseDataPageComponentWN<IDefect > {
  title: string = 'Defect'
  DefectJSON = DefectJSON;


  form: GBDataFormGroupWN<IDefect > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Defect', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.DefectFillFunction(arrayOfValues.DefectId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

public DefectValue(defect:any){
  console.log("defect",defect);
  this.form.get('DefectPointPerDefect').patchValue(defect)

}

// public GetTotalWeightDesimal(TotalWeight:any){
//   console.log("TotalWeight",TotalWeight)
//   this.form.get('PatternTotalWeight').patchValue(TotalWeight)
// }

  public DefectFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public DefectPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IDefect > {
  const dbds: GBBaseDBDataService<IDefect > = new GBBaseDBDataService<IDefect >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IDefect >, dbDataService: GBBaseDBDataService<IDefect >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IDefect > {
  return new GBDataPageService<IDefect >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
