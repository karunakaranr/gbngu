import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ProcessMasterService } from '../../services/ProcessMaster/ProcessMaster.service';
import { IProcessMaster } from '../../models/IProcessMaster';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as ProcessMasterJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ProcessMaster.json'
import {URLS} from 'features/erpmodules/production/URLS/urls'
import { IProcessGroup } from 'features/erpmodules/inventory/models/IProcessGroup';


@Component({
  selector: 'app-ProcessMaster',
  templateUrl: './ProcessMaster.component.html',
  styleUrls: ['./ProcessMaster.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ProcessId'},
    { provide: 'url', useValue: URLS.Process },
    { provide: 'DataService', useClass: ProcessMasterService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ProcessMasterComponent extends GBBaseDataPageComponentWN<IProcessMaster > {
  title: string = 'ProcessMaster'
  ProcessMasterJSON = ProcessMasterJSON;


  form: GBDataFormGroupWN<IProcessMaster > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'ProcessMaster', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ProcessMasterFillFunction(arrayOfValues.ProcessId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public ProcessMasterFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public ProcessMasterPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IProcessMaster > {
  const dbds: GBBaseDBDataService<IProcessMaster > = new GBBaseDBDataService<IProcessMaster >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IProcessMaster >, dbDataService: GBBaseDBDataService<IProcessMaster >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IProcessMaster > {
  return new GBDataPageService<IProcessMaster >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
