import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';

import { DieMasterService } from '../../services/DieMaster/DieMaster.service';
import { IDieMaster } from '../../models/IDieMaster';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as DieMasterJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/DieMaster.json'
import { URLS } from 'features/erpmodules/production/URLS/urls'


@Component({
  selector: 'app-DieMaster',
  templateUrl: './DieMaster.component.html',
  styleUrls: ['./DieMaster.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'PatternId' },
    { provide: 'url', useValue: URLS.Pattern },
    { provide: 'DataService', useClass: DieMasterService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class DieMasterComponent extends GBBaseDataPageComponentWN<IDieMaster> {
  title: string = 'DieMaster'
  DieMasterJSON = DieMasterJSON;
  ExpectedAmount: number;
  OpeningAmount: number;
  Balance: number;
  isdissable: boolean = true

  form: GBDataFormGroupWN<IDieMaster> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'DieMaster', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.DieMasterFillValue(arrayOfValues.PatternId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  if
  public GetTotalWeightDesimal(TotalWeight: any) {
    console.log("TotalWeight", TotalWeight)
    this.form.get('PatternTotalWeight').patchValue(TotalWeight)
  }

  public InputPatternLifeCount(LifeCountValue: number) {
    this.form.get('PatternLifeCount').patchValue(LifeCountValue)
    this.ExpectedAmount = LifeCountValue
    console.log("ExpectedAmount:", this.ExpectedAmount)
  }
  public InputPatternOpeningCount(OpeningCountValue: number) {
    console.log("OpeningCountValue", OpeningCountValue)
    this.form.get('PatternOpeningCount').patchValue(OpeningCountValue)
    this.OpeningAmount = OpeningCountValue
    this.Balance = this.ExpectedAmount - this.ExpectedAmount
    var ans = this.ExpectedAmount - this.OpeningAmount
    this.form.get('PatternBalanceCount').patchValue(ans)
     this.isdissable = true
  }
  public GetBalanceCountDesimal(BalanceCount: any) {
    console.log("BalanceCount", BalanceCount)
    this.form.get('PatternBalanceCount').patchValue(BalanceCount)
  }
  public GetTransactionCountDecimal(TransactionCount: any) {
    console.log("TransactionCount", TransactionCount)
    this.form.get('PatternTransactionCount').patchValue(TransactionCount)
  }
  public DieMasterFillValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
        this.isdissable = false
      })
    }
  }


  public DieMasterPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IDieMaster> {
  const dbds: GBBaseDBDataService<IDieMaster> = new GBBaseDBDataService<IDieMaster>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IDieMaster>, dbDataService: GBBaseDBDataService<IDieMaster>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IDieMaster> {
  return new GBDataPageService<IDieMaster>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
