import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { PatternService } from '../../services/Pattern/Pattern.service';
import { IPattern } from '../../models/IPattern';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as PatternJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Pattern.json'
import {URLS} from 'features/erpmodules/production/URLS/urls'


@Component({
  selector: 'app-Pattern',
  templateUrl: './Pattern.component.html',
  styleUrls: ['./Pattern.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ItemId'},
    { provide: 'url', useValue: URLS.DieMasterItem },
    { provide: 'DataService', useClass: PatternService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PatternComponent extends GBBaseDataPageComponentWN<IPattern > {
  title: string = 'Pattern'
  PatternJSON = PatternJSON;


  form: GBDataFormGroupWN<IPattern > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Pattern', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PatternRetrivalValue(arrayOfValues.PackSetId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public PatternRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PatternPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPattern > {
  const dbds: GBBaseDBDataService<IPattern > = new GBBaseDBDataService<IPattern >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPattern >, dbDataService: GBBaseDBDataService<IPattern >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPattern > {
  return new GBDataPageService<IPattern >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
