import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ProcessTypeService } from '../../services/ProcessType/ProcessType.service';
import { IProcessType } from '../../models/IProcessType';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as ProcessTypeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ProcessType.json'

import {URLS} from 'features/erpmodules/production/URLS/urls'



@Component({
  selector: 'app-ProcessType',
  templateUrl: './ProcessType.component.html',
  styleUrls: ['./ProcessType.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ProcessTypeId'},
    { provide: 'url', useValue: URLS.ProcessType },
    { provide: 'DataService', useClass: ProcessTypeService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ProcessTypeComponent extends GBBaseDataPageComponentWN<IProcessType > {
  title: string = 'ProcessType'
  ProcessTypeJSON = ProcessTypeJSON;


  form: GBDataFormGroupWN<IProcessType > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'ProcessType', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ProcessTypeFillFunction(arrayOfValues.ProcessTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
public GetDecimal(GetValue:any){
  console.log("GetValue: ",GetValue);
  this.form.get('ProcessTypeCharges').patchValue(GetValue)
}

  public ProcessTypeFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public ProcessTypePatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IProcessType > {
  const dbds: GBBaseDBDataService<IProcessType > = new GBBaseDBDataService<IProcessType >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IProcessType >, dbDataService: GBBaseDBDataService<IProcessType >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IProcessType > {
  return new GBDataPageService<IProcessType >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
