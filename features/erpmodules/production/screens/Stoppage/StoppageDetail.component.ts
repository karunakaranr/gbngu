import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { IStoppageDetail } from '../../models/IStoppageDetail';



@Component({
  selector: 'app-StoppageDetail',
  templateUrl: './StoppageDetail.component.html',
  styleUrls: ['./StoppageDetail.component.scss'],
})
export class StoppageDetailComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData: IStoppageDetail;
  public tabledata;
  
  constructor(public store: Store) { }
  ngOnInit(): void {
    this.getStoppageDetail();
  }
  
  public getStoppageDetail() {
    this.rowdatacommon$.subscribe(data => {
      this.rowData = data;
      console.log("Stoppage Detail :", this.rowData)


    

      let json = this.rowData;
      var finalizedArray = [];
      json.map((row,index) => {
          finalizedArray.push({
              sno: index,
              produdate:row['ProductionDate'],
              empname: row['EmployeeName'],
              indentno: row['IndentNumber'],
              productionno: row['ProductionNumber'],
              fromtime: row['StoppageFromDateTime'],
              totime: row['StoppageToDateTime'],
              stopduration: row['StoppageDuration'],
              reasonname: row['ReasonName'],
              remarks: row['Remarks']
             
          });
      });


      const final = {};
      finalizedArray.forEach((detail) => {
          final[detail.produdate] = {
              accountGroups: {},
              indentno: detail.produdate,
              productionno: "",
              fromtime: "",
              totime: "",
              stopduration: "",
              reasonname: "",
              remarks: "",
              ...final[detail.produdate],
          };
          final[detail.produdate].accountGroups[detail.empname] = {
            accountGroup2: {},
            indentno: detail.empname,
            productionno: "",
            fromtime: "",
            totime: "",
            stopduration: "",
            reasonname: "",
            remarks: "",
            ...final[detail.produdate].accountGroups[detail.empname],
        }

        final[detail.produdate].accountGroups[detail.empname].accountGroup2[detail.sno] = {
           
            indentno: detail.indentno,
            productionno: detail.productionno,
            fromtime: detail.fromtime,
            totime: detail.totime,
            stopduration: detail.stopduration,
            reasonname: detail.reasonname,
            remarks:detail.remarks,
          };
      });

      const stoppagedetail = Object.keys(final);

      const tableData = [];
      stoppagedetail.forEach((stopdetail) => {
          tableData.push({
            indentno: final[stopdetail].indentno,
              productionno: "",
              fromtime: "",
              totime: "",
              stopduration: "",
              reasonname: "",
              remarks: "",
              bold: true,
          });

          const accountGroups = Object.keys(final[stopdetail].accountGroups);
          accountGroups.forEach(stdetail => {
                    
                    
                    tableData.push({
                        indentno: final[stopdetail].accountGroups[stdetail].indentno,
                        productionno: "",
                        fromtime: "",
                        totime: "",
                        stopduration: "",
                        reasonname: "",
                        remarks: "",
                        bold: true,
                    })

          const accountGroup2 = Object.keys(final[stopdetail].accountGroups[stdetail].accountGroup2);
          accountGroup2.forEach((sdetail) => {
              tableData.push({
                indentno: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].indentno,
                productionno: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].productionno,
                fromtime: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].fromtime,
                totime: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].totime,
                stopduration: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].stopduration,
                reasonname: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].reasonname,
                remarks: final[stopdetail].accountGroups[stdetail].accountGroup2[sdetail].remarks,
              
                

              })
          })
        })
      });
  
      this.tabledata = tableData;

      console.log("Final Data:", this.rowData)
      
    });
  }
}