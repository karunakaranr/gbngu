import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
// import { INestingPlan } from '../../models/INestingPlan';
// import { NestingPlanService } from '../../services/NestingPlan/NestingPlan.service';
import { INestingPlan } from '../../models/INestingPlan';
import { NestingPlanService } from '../../services/NestingPlan/NestingPlan.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

import * as NestingPlanjson from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/NestingPlan.json'


import{URLS} from '../../URLS/urls'


@Component({
  selector: "app-NestingPlan",
  templateUrl: "./NestingPlan.component.html",
  styleUrls: ["./NestingPlan.component.scss"],
  providers: [
    { provide: 'IdField', useValue: 'NestingPlanId' },
    { provide: 'url', useValue: URLS.NestingPlan },
    { provide: 'DataService', useClass: NestingPlanService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class NestingPlanComponent extends GBBaseDataPageComponentWN<INestingPlan> {
  title: string = "NestingPlan"
  NestingPlanjson = NestingPlanjson;


 

  form: GBDataFormGroupWN<INestingPlan> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'NestingPlan', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.NestingPlanFillFunction(arrayOfValues.NestingPlanId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }




  public NestingPlanFillFunction(SelectedPicklistData: string): void {

   
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(NestingPlan => {
        this.form.patchValue(NestingPlan);
      })
    }
  }


  public NestingPlanPatchValue(SelectedPicklistDatas: any): void {
    
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    console.log("iteem==>",SelectedPicklistDatas)
  }
}




export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<INestingPlan> {
  const dbds: GBBaseDBDataService<INestingPlan> = new GBBaseDBDataService<INestingPlan>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<INestingPlan>, dbDataService: GBBaseDBDataService<INestingPlan>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<INestingPlan> {
  return new GBDataPageService<INestingPlan>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
