import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Observable } from 'rxjs';
import { FilterState } from 'features/gbfilter/store/gbfilter.state';
import { CommonReportdbservice } from 'features/commonreport/dbservice/commonreportdbservice';
import { DashboardFunction } from 'libs/gbcommon/src/lib/services/DashboardFunction/dashboard.service';
import { ICriteriaDTODetail } from '../../models/IFilterDetail';
import { IWorkOrderDetail } from '../../models/IWorkOrderDetail';
@Component({
  selector: 'app-WorkOrderDashboard',
  templateUrl: './WorkOrderDashboard.component.html',
  styleUrls: ['./WorkOrderDashboard.component.scss']
})

export class WorkOrderDashboardComponent implements OnInit {
  @Select(FilterState.Filter) filtercriteria$: Observable<any>;
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  Summary: string[] = ['Item', 'Process', 'Work Center','Sub Contract'];
  ReportType : number=0;
  SummaryModel = { option: 0 }
  WorkOrderDetail : IWorkOrderDetail;
  filterData : ICriteriaDTODetail;
  SelectSummary : boolean =false;
  constructor(public store: Store, @Inject(LOCALE_ID) public locale: string,public filterservice : CommonReportdbservice, public commonfunction :DashboardFunction) {
  }
  ngOnInit(): void {
    this.DataReceiver();
  }

  public DataReceiver() {
    this.rowdatacommon$.subscribe(data => {
      this.WorkOrderDetail = data[0];
      console.log("Work Order Detail:", this.WorkOrderDetail)
    });
  }

  public SummaryAndListReportServiceCall() {
    this.filtercriteria$.subscribe(filterdata => {
      this.filterData = filterdata;
      console.log("Filter Data:",this.filterData);
    })
    console.log("Model:", this.SummaryModel)
    this.SelectSummary=true;
    this.ReportType=this.SummaryModel.option;
  }

}