import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
// import { ItemcategoryService } from 'features/erpmodules/inventory/services/itemcategory/itemcategory.service';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { IMachineWiseProd } from 'features/erpmodules/production/models/IMachineWiseProd';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
    selector: 'app-MachineWiseProd',
    templateUrl: './MachineWiseProd.component.html',
    styleUrls: ['./MachineWiseProd.component.scss'],
})
export class MachineWiseProdComponent implements OnInit {
    @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
    public rowData: IMachineWiseProd[];

    constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string) { }
    ngOnInit(): void {
        this.getMachineWiseProd();
    }

    public getMachineWiseProd() {
        this.sharedService.getRowInfo().subscribe(data => {
            if (data.length > 0) {
            this.rowData = data;
            console.log("Machine Wise Production Data :", this.rowData)


            let json = this.rowData;
            var finalizedArray = []
            json.map((row, index) => {
                finalizedArray.push({
                    sno: index,
                    Code: row['Code'],
                    Name: row['Name'],

                    SKUId: row['SKUId'],

                    ItemCode: row['SKUId'] == -1 ? row.ItemCode : row.SKUCode,
                    ItemName: row['SKUId'] == -1 ? row.ItemName : row.SKUName,
                    UOMCode: row['UOMCode'],
                    Produced: row['Produced'],
                    Reprocess: row['Reprocess'],
                    Accepeted: row['Accepeted'],
                    Rejected: row['Rejected'],
                    Rework: row['Rework'],
                });
            });
            const final = {};
            finalizedArray.forEach(detail => {
                final[detail.Code] = {
                    accounts: {},
                    ItemCode: detail.Code,
                    ItemName: detail.Name,
                    UOMCode: "",
                    Produced: "",
                    Reprocess: "",
                    Accepeted: "",
                    Rejected: "",
                    Rework: "",

                    ...final[detail.Code],
                }
                final[detail.Code].accounts[detail.ItemCode] = {
                    ItemCode: detail.ItemCode,
                    ItemName: detail.ItemName,
                    UOMCode: detail.UOMCode,
                    Produced: detail.Produced,
                    Reprocess: detail.Reprocess,
                    Accepeted: detail.Accepeted,
                    Rejected: detail.Rejected,
                    Rework: detail.Rework,
                }
            })
            const empcodes = Object.keys(final)
            const tableData = [];
            empcodes.forEach(code => {
                const account = Object.keys(final[code].accounts)

                tableData.push({
                    ItemCode: final[code].ItemCode,
                    ItemName: final[code].ItemName,
                    UOMCode: "",
                    Produced: "",
                    Reprocess: "",
                    Accepeted: "",
                    Rejected: "",
                    Rework: "",
                    bold: true,
                })
                account.forEach(ag => {
                    tableData.push({
                        ItemCode: final[code].accounts[ag].ItemCode,
                        ItemName: final[code].accounts[ag].ItemName,
                        UOMCode: final[code].accounts[ag].UOMCode,
                        Produced: final[code].accounts[ag].Produced,
                        Reprocess: final[code].accounts[ag].Reprocess,
                        Accepeted: final[code].accounts[ag].Accepeted,
                        Rejected: final[code].accounts[ag].Rejected,
                        Rework: final[code].accounts[ag].Rework,
                    })
                })
            })
            this.rowData = tableData;
            console.log("tableData", tableData)
        }
        });
    }
}
