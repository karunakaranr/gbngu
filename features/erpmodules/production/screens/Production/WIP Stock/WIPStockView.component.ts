import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
// import { ItemcategoryService } from 'features/erpmodules/inventory/services/itemcategory/itemcategory.service';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { IWIPStock } from 'features/erpmodules/production/models/IWIPStock';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
  selector: 'app-WIPStockView',
  templateUrl: './WIPStockView.component.html',
  styleUrls: ['./WIPStockView.component.scss'],
})
export class WIPStockViewComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData: IWIPStock[];  
  public TotalDuration;
  public TotalRate;
  public TotalValue;
  constructor(public sharedService: SharedService,public store: Store, @Inject(LOCALE_ID) public locale: string) { }
  ngOnInit(): void {
    this.getWIPStockView();
  }
  
  public getWIPStockView() {
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("WIP STock Data :", this.rowData)


      this.TotalDuration = 0;
      this.TotalRate = 0;
      this.TotalValue = 0;

      for (let data of this.rowData) {
          this.TotalDuration = this.TotalDuration + data.Duration;
          this.TotalRate = this.TotalRate + data.TotalCost;
          this.TotalValue = this.TotalValue + data.Totalvalue;
      }

      let json = this.rowData;
      var finalizedArray = []
      json.map((row,index)=> {
        finalizedArray.push({
          sno: index,
          ItemCode: row['SKUId'] == -1 ? row.ItemCode : row.SKUCode,
          ItemName: row['SKUId'] == -1 ? row.ItemName : row.SKUName,

          SKUId: row['SKUId'],

          IndentDate: row['IndentDate'],
          IndentNumber: row['IndentNumber'],
          Name: row['SKUId'] == -1 ? row.ItemName : row.SKUName,
          WorkCenterName: row['WorkCenterName'],
          Planned: row['Planned'],
          Produced: row['Produced'],
          Pending: row['Pending'],
          AvailableToWork: row['AvailableToWork'],
          WIPStock: row['WIPStock'],
          NextProcessName: row['NextProcessName'],
          NextProcessPending: row['NextProcessPending'],
          Duration: row['Duration'],
          TotalCost: row['TotalCost'],
          Totalvalue: row['Totalvalue'],
        });
      });
      const final = {};
      finalizedArray.forEach(detail => {
        final[detail.ItemCode] = {
          accounts: {},
          IndentDate: detail.ItemCode,
          IndentNumber: "",
          Name: detail.ItemName,
          WorkCenterName: "",
          Planned: "",
          Produced: "",
          Pending: "",
          AvailableToWork: "",
          WIPStock: "",
          NextProcessName: "",
          NextProcessPending: "",
          Duration: "",
          TotalCost: 0,
          Totalvalue: "",

          ...final[detail.ItemCode],
        }
        final[detail.ItemCode].accounts[detail.sno] = {
            IndentDate: detail.IndentDate,
            IndentNumber: detail.IndentNumber,
            Name: detail.Name,
            WorkCenterName: detail.WorkCenterName,
            Planned: detail.Planned,
            Produced: detail.Produced,
            Pending: detail.Pending,
            AvailableToWork: detail.AvailableToWork,
            WIPStock: detail.WIPStock,
            NextProcessName: detail.NextProcessName,
            NextProcessPending: detail.NextProcessPending,
            Duration: detail.Duration,
            TotalCost: detail.TotalCost,
            Totalvalue: detail.Totalvalue,
        }
      })
      const empcodes = Object.keys(final)
      const tableData = [];
      empcodes.forEach(code => {
        const account = Object.keys(final[code].accounts)

        tableData.push({
            IndentDate: final[code].IndentDate,
            IndentNumber: "",
            Name: final[code].Name,
            WorkCenterName: "",
            Planned: "",
            Produced: "",
            Pending: "",
            AvailableToWork: "",
            WIPStock: "",
            NextProcessName: "",
            NextProcessPending: "",
            Duration: "Total:",
            TotalCost: final[code].TotalCost,
            Totalvalue: "",
          bold: true,
        })
        account.forEach(ag => {
          tableData.push({
            IndentDate: final[code].accounts[ag].IndentDate,
            IndentNumber: final[code].accounts[ag].IndentNumber,
            Name: final[code].accounts[ag].Name,
            WorkCenterName: final[code].accounts[ag].WorkCenterName,
            Planned: final[code].accounts[ag].Planned,
            Produced: final[code].accounts[ag].Produced,
            Pending: final[code].accounts[ag].Pending,
            AvailableToWork: final[code].accounts[ag].AvailableToWork,
            WIPStock: final[code].accounts[ag].WIPStock,
            NextProcessName: final[code].accounts[ag].NextProcessName,
            NextProcessPending: final[code].accounts[ag].NextProcessPending,
            Duration: final[code].accounts[ag].Duration,
            TotalCost: final[code].accounts[ag].TotalCost,
            Totalvalue: final[code].accounts[ag].Totalvalue,
          })
        })
      })
      this.rowData = tableData;
      console.log("tableData",tableData)
    }
    });
  }
}
