import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { IProductionSummary } from 'features/erpmodules/production/models/IProductionSummary';


@Component({
  selector: 'app-ProductionSummary',
  templateUrl: './ProductionSummary.component.html',
  styleUrls: ['./ProductionSummary.component.scss'],
})
export class ProductionSummaryComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData: IProductionSummary;
  public tabledata;
  
  constructor(public store: Store) { }
  ngOnInit(): void {
    this.getProductionSummary();
  }
  
  public getProductionSummary() {
    this.rowdatacommon$.subscribe(data => {
      this.rowData = data;
      console.log("Production Summary :", this.rowData)


    

      let json = this.rowData;
      var finalizedArray = [];
      json.map((row,index) => {
          finalizedArray.push({
              sno: index,
              workname:row['WorkCenterName'],
              iname: row['ItemName'],
              icode: row['ItemCode'],
              skuname: row['SKUName'],
              proname: row['ProcessName'],
              uomname: row['UOMName'],
              produce: row['Produced'],
              reprocess: row['Reprocess'],
              accepet: row['Accepeted'],
              reject: row['Rejected'],
              rework: row['Rework']
          });
      });


      const final = {};
      finalizedArray.forEach((detail) => {
          final[detail.workname] = {
              accountGroups: {},
              iname: detail.workname,
              icode: "",
              skuname: "",
              proname: "",
              uomname: "",
              produce: "",
              reprocess: "",
              accepet: "",
              reject: "",
              rework: "",
              ...final[detail.workname],
          };

          final[detail.workname].accountGroups[detail.sno] = {
            iname: detail.iname,
            icode: detail.icode,
            skuname: detail.skuname,
            proname: detail.proname,
            uomname: detail.uomname,
            produce: detail.produce,
            reprocess: detail.reprocess,
            accepet: detail.accepet,
            reject: detail.reject,
            rework: detail.rework,
          };
      });

      const productionsummary = Object.keys(final);

      const tableData = [];
      productionsummary.forEach((prodsum) => {
          tableData.push({
              iname: final[prodsum].iname,
              icode: "",
              skuname: "",
              proname: "",
              uomname: "",
              produce: "",
              reprocess: "",
              accepet: "",
              reject: "",
              rework: "",
              bold: true,
          });

          const summary = Object.keys(final[prodsum].accountGroups);
          summary.forEach((psummary) => {
              tableData.push({
                iname: final[prodsum].accountGroups[psummary].iname,
                icode: final[prodsum].accountGroups[psummary].icode,
                skuname: final[prodsum].accountGroups[psummary].skuname,
                proname: final[prodsum].accountGroups[psummary].proname,
                uomname: final[prodsum].accountGroups[psummary].uomname,
                produce: final[prodsum].accountGroups[psummary].produce,
                reprocess: final[prodsum].accountGroups[psummary].reprocess,
                accepet: final[prodsum].accountGroups[psummary].accepet,
                reject: final[prodsum].accountGroups[psummary].reject,
                rework: final[prodsum].accountGroups[psummary].rework,

              });
          });
      });
      this.tabledata = tableData;

      console.log("Final Data:", this.rowData)
      
    });
  }

}