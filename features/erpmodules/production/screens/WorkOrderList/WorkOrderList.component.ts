import { Component, Input, OnInit } from '@angular/core';
import { WorkOrderList } from './../../services/WorkOrderList/WorkOrderList.service';
import { Inject, LOCALE_ID } from '@angular/core';
import { ICriteriaDTODetail } from '../../models/IFilterDetail';
import { IWorkOrderList } from '../../models/IWorkOrderList';
@Component({
  selector: 'app-WorkOrderList',
  templateUrl: './WorkOrderList.component.html',
  styleUrls: ['./WorkOrderList.component.scss']
})

export class WorkOrderListComponent implements OnInit {
  @Input() FilterData : ICriteriaDTODetail;
  RowData : IWorkOrderList[]=[];
  constructor(public service: WorkOrderList, @Inject(LOCALE_ID) public locale: string ) { }
  ngOnInit(): void {
      this.service.WorkOrderListview(this.FilterData).subscribe(menudetails => {
        this.RowData=menudetails;
        console.log("Work Order List Data:",this.RowData)
      })
  }
}