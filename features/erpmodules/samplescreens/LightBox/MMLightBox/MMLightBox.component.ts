import { Component, Input, OnInit } from '@angular/core';
import { MMLightBox } from './../../services/LightBox/MMLightBox.service';
import  {Inject, LOCALE_ID } from '@angular/core';
import { DatePipe } from '@angular/common';
import { IMMLightBoxDetail } from '../../models/IMMLightBoxDetail';
@Component({
  selector: 'app-MMLightBox',
  templateUrl: './MMLightBox.component.html',
  styleUrls: ['./MMLightBox.component.scss']
})

export class MMLightBoxComponent implements OnInit {
  @Input() DocumentId;
  LightBoxData:IMMLightBoxDetail;
  constructor(public service: MMLightBox, @Inject(LOCALE_ID) public locale: string ) { }
  ngOnInit(): void {
    var datePipe = new DatePipe("en-US");
    this.service.MMLightBoxView(this.DocumentId).subscribe(menudetails => {
      this.LightBoxData=menudetails[0];
      this.LightBoxData.MMHeadDate = datePipe.transform(JSON.parse(this.LightBoxData.MMHeadDate.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy');
      this.LightBoxData.MMHeadReferenceDate = datePipe.transform(JSON.parse(this.LightBoxData.MMHeadReferenceDate.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy'); 
      console.log("MM LightBox Data:", this.LightBoxData);
    }) 
  }
}