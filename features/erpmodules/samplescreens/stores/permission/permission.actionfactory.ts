import {
    GBDataStateAction,
    GBDataStateActionFactoryWN,
    ISelectListCriteria,
  } from '@goodbooks/gbdata';
  import { permissiondetail } from './../../models/permission/ipermission.model';
import { GetAll, GetData, MoveFirst, MoveLast, MoveNext, MovePrev, GetAllAndMoveFirst, SaveData, ClearData, DeleteData, AddData } from './permission.action';

export class PermissionStateActionFactory extends GBDataStateActionFactoryWN<permissiondetail> {
    GetData(id: number): GBDataStateAction<permissiondetail> {
      return new GetData(id);
    }
    GetAll(scl: ISelectListCriteria): GBDataStateAction<permissiondetail> {
      return new GetAll(scl);
    }
    MoveFirst(): GBDataStateAction<permissiondetail> {
      return new MoveFirst();
    }
    MoveNext(): GBDataStateAction<permissiondetail> {
      return new MoveNext();
    }
    MovePrev(): GBDataStateAction<permissiondetail> {
      return new MovePrev();
    }
    MoveLast(): GBDataStateAction<permissiondetail> {
      return new MoveLast();
    }
    AddData(payload: permissiondetail): GBDataStateAction<permissiondetail> {
      return new AddData(payload);
    }
    SaveData(payload: permissiondetail): GBDataStateAction<permissiondetail> {
      return new SaveData(payload);
    }
    ClearData(): GBDataStateAction<permissiondetail> {
      return new ClearData();
    }
    DeleteData(): GBDataStateAction<permissiondetail> {
      return new DeleteData();
    }
  }
