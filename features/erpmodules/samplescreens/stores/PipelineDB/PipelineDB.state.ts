import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { GbPipelineDB } from './PipelineDB.action';
export class PipelineDBStateModel {
  PipelineDBdata;
}
@State<PipelineDBStateModel>({
  name: 'PipelineDB',
  defaults: {
    PipelineDBdata: null,
  },
})
@Injectable()
export class PipelineDBState {
  @Selector() static PipelineDB(state: PipelineDBStateModel) {
    return state.PipelineDBdata;
  }

  @Action(GbPipelineDB)
  GbPipelineDB(context: StateContext<PipelineDBStateModel>, IPipelineDB: GbPipelineDB) {
    const state = context.getState();
    context.setState({ ...state, PipelineDBdata: IPipelineDB.DGbPipelineDB });
  }
}