import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { UicoreModule } from '@goodbooks/uicore';
import { UifwkUifwkmaterialModule } from '@goodbooks/uifwk/material';
import { NgxsModule } from '@ngxs/store';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { FormlyMaterialModule } from '@ngx-formly/material';

import { RulesFormComponent } from './packs/rulesform/rulesform.component';
import { PackbothComponent } from './packs/Packboth/packboth.component';
import { PackdbservComponent } from './packs/Packdbserv/packdbserv.component';

import { PackState } from '../inventory/stores/Pack/pack.state';
import {PermissionState} from './stores/permission/permission.state'

import { attachmentComponent } from './attachment/attachment.component'
import {geolocationtestComponent} from './screens/geolocationtest/geolocationtest.component'
import { AngularFileUploaderComponent } from './fileuploader/angular-file-uploader.component';
import {permissionformComponent} from './screens/permissionform/permissionform.component'
import {permissionlistComponent} from './screens/permissionlist/permissionlist.component'
import {approvallistComponent} from './screens/approvallist/approvallist.component' 
import { PacknoneComponent } from './packs/Packnone/packnone.component';
import { PackservComponent } from './packs/Packserv/packserv.component';
import { PackstoreComponent } from './packs/Packstore/Components/packstore.component';
import { ProfileComponent } from './screens/profile/profile.component';

import { SamplescreensRoutingModule } from './samplescreens-routing.module';
import { SamplescreensComponent } from './samplescreens.component';
import { NgxsFormPluginModule } from '@ngxs/form-plugin';
import { PacklistComponent } from '../inventory/screens/Pack/Packlist/packlist.component';
import { BrandlistComponent } from '../inventory/screens/brand/brandlist/brandlist.component';
import { ItemCategorylistComponent } from '../inventory/screens/itemcategory/itemcategorylist/itemcategorylist.component';
import { PacksetlistComponent } from '../inventory/screens/packset/packsetlist/packsetlist.component';
import { EmployeemasterlistComponent } from '../hrms/screens/employee/employeelist/employeemasterlist.component';
import { EmployeeSkillListComponent } from '../hrms/screens/employee/EmployeeSkill/EmployeeSkillList/EmployeeSkillList.component';
import { ItemOUListViewComponent } from '../inventory/screens/ItemOUList/ItemOUListView/ItemOUList.component';
import { LOCALE_ID } from "@angular/core";
import "@angular/common/locales/global/en-IN";

import { ChartviewComponent } from './screens/chart/chartview/chartview.component';
import { MarginsummaryComponent } from './screens/sales/marginsummary/marginsummary.component';

import { TranslocoRootModule } from './../../transloco/transloco-root.module';
import { GbdataModule } from '@goodbooks/gbdata';
import { environment } from '../../environments/environment';
import { NgBreModule } from '@unisoft/ng-bre';

import { DashboardItems } from '../../common/shared/services/gridster/dashboarditems';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { GbchartModule } from '../../common/shared/screens/gbchart/gbchart.module';
import { EntitylookuptestComponent } from './screens/entitylookuptest/entitylookuptest.component';
import { EntitylookupModule } from './../../entitylookup/entitylookup.module';
import { geolocationModule} from './../../geolocation/geolocation.module'
import { NgSelectModule } from '@ng-select/ng-select';


import { GridViewComponent} from './screens/gridview/gridview.component';
import { AgGridModule } from 'ag-grid-angular';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule} from '@angular/material/button';
import { MatListModule } from '@angular/material/list'
import { MatIconModule} from '@angular/material/icon';
import { MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { NgApexchartsModule } from 'ng-apexcharts';

import { MatTabsModule} from '@angular/material/tabs'
import { MatCheckboxModule} from '@angular/material/checkbox';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction'
import { GblogModule } from 'features/gblog/gblog.module';

import { GbgridModule } from 'features/gbgrid/components/gbgrid/gbgrid.module';
import { GbfilterModule } from './../../gbfilter/gbfilter.module';
import { CommonReportModule } from './../../commonreport/commonreport.module';
import { ApexChartComponent } from './screens/chart/ApexChart/ApexChart.component';
import { StatementOfAccountComponent } from './screens/chart/StatementOfAccount/StatementOfAccount.component';
import { ApexChartState } from './stores/apexchart/apexchart.state';
import { PipelineDBComponent } from './Dashboard/PipelineDB/PipelineDB.component';
import { ProductionMonitorComponent } from './Dashboard/ProductionMonitor/ProductionMonitor.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { GbmodulelistMainComponent } from './screens/ModuleList/ModuleList.component';
import { MatMenuModule } from '@angular/material/menu';
import { MMStatusComponent } from '../sales/screens/MMStatus/MMStatus.component';
import { MMLightBoxComponent } from './LightBox/MMLightBox/MMLightBox.component';
import { VoucherLightBoxComponent } from './LightBox/VoucherLightBox/VoucherLightBox.component';
import { ShortNumberPipe } from 'libs/gbcommon/src/lib/short-number.pipe';
import { DateFormaterPipe } from 'libs/gbcommon/src/lib/Date-Formater.pipe';
import { SubTotalPipe } from 'libs/gbcommon/src/lib/subtotal.pipe'; 
import { WorkOrderDashboardComponent } from '../production/screens/WorkOrderDashboard/WorkOrderDashboard.component';
import { CashBookComponent } from '../finance/screens/Books/CashBook/CashBook.component';
import { MenuListMainComponent } from './screens/MenuList/MenuList.component';
import { TruncatePipe } from 'libs/gbcommon/src/lib/TruncateText.pipe';
import { MainMenuComponent } from './screens/MainMenu/MainMenu.component';
import { NotificationComponent } from './screens/Notification/Notification.component';
import { AppraisalDashBoardComponent } from '../hrms/screens/AppraisalDashBoard/AppraisalDashBoard.component';
import { MapComponent } from './screens/Map/map.component';
import { SitePunchComponent } from '../hrms/screens/SitePunch/SitePunch.component';
import { UOMlistComponent } from '../inventory/screens/UOM/UOMList/uomlist.component';
import { ItemBrandReportComponent } from '../inventory/screens/brand/BrandListReport/BrandListReport.component';
import { StorelistComponent } from '../inventory/screens/Store/StoreList/storelist.component';
import { ItemGroupListComponent } from '../inventory/screens/ItemGroup/ItemGroupList/ItemGroupList.component';
import { itempartylistComponent } from '../inventory/screens/ItemParty/ItemPartyList/itempartylist.component';
import { partyitemlistComponent } from '../inventory/screens/PartyItem/PartyItemList/partyitemlist.component';
import { ItemReportComponent } from '../inventory/screens/Item/ItemReport/ItemReportList.component';
import { schedulelistComponent } from '../finance/screens/Schedule/ScheduleList/schedulelist.component';
import { AccountGroupComponent } from '../finance/screens/AccountList/AccountGroup/AccountGroup.component';
import { AccountListComponent } from '../finance/screens/AccountList/AccountList/AccountList.component';
import { leaverequestComponent } from '../hrms/screens/LeaveRequest/LeaveRequest/leaverequest.component';
import { partybranchlistComponent } from '../finance/screens/PartyBranch/PartyBranchList/partybranchlist.component';
import { shiftlistComponent } from '../hrms/screens/Shift/ShiftList/shiftlist.component';
import { paymentsummaryComponent } from '../finance/screens/Payment/PaymentSummary/paymentsummary.component';
import { sopackedsummaryComponent } from '../sales/screens/SalesOrderPacked/SOPackedSummary/sopackedsummary.component';
import { sopackeditemlevelComponent } from '../sales/screens/SalesOrderPackedItemLevel/SalesOrderPackedItemLevelSummary/sopackeditemlevel.component';
import { itemwiseComponent } from '../inventory/screens/ItemWise/itemWise/itemwise.component';
import { tbscheduleComponent } from '../finance/screens/TrialBalanceSchedule/tbschedule.component';
import { tbgroupComponent } from '../finance/screens/TrialBalanceGroup/tbgroup.component';
import { tbaccountComponent } from '../finance/screens/TrialBalanceAccount/tbaccount.component';
import { tbscheduledetailComponent } from '../finance/screens/TrialBalance(Schedule)Detail/tbscheduledetail.component';
import { tbgroupdetailComponent } from '../finance/screens/TrialBalance(Group)Detail/tbgroupdetail.component';
import { tbaccountdetailComponent } from '../finance/screens/TrialBalance(Account)Detail/tbaccountdetail.component';
import { receivablebillwiseComponent } from '../finance/screens/ReceivablesBillwise/receivablebillwise.component';
import { receivablesdetailComponent } from '../finance/screens/ReceivablesDetail/receivablesdetail.component';
import { receivablessummaryComponent } from '../finance/screens/ReceivablesSummary/receivablessummary.component';
import { NumberFormatPipe } from 'libs/gbcommon/src/lib/Number-Formatter.pipe';
import { bankbookComponent } from '../finance/screens/BankBook/bankbook.component';
import { supplygroupComponent } from '../finance/screens/CheckList/SupplyGroup/supplygroup.component';
import { deductedcategoryComponent } from '../finance/screens/TDS/DeductedCategory/deductedcategory.component';
import { SalesEnquiryComponent } from '../sales/screens/PendingRegister/ItemWise/Enquiry/SalesEnquiry.component';
import { DailyAttendanceComponent } from '../hrms/screens/Attendance/Daily Attendance List/DailyAttendance.component';
import { MonthlyAttendanceComponent } from '../hrms/screens/Attendance/Monthly Attendance/MonthlyAttendance.component';
import { daybookComponent } from '../finance/screens/DayBook/DayBook.component';
import { soasingleComponent } from '../finance/screens/StatementofAccount(Single)/soasingle.component';
import { ItemSubCategorylistComponent } from '../inventory/screens/itemsubcategory/itemsubcategorylist/itemsubcategorylist.component';
import { AllControlFormComponent } from './screens/AllControlForm/AllControlForm.component';
import { MatNativeDateModule } from '@angular/material/core';
// import { brsdetailComponent } from '../finance/screens/BRSDetail/BRSDetailView/brsdetail.component';
import { brssummaryComponent } from '../finance/screens/BRSSummary/BRSSummaryView/brssummary.component';
import { registerdetailComponent } from '../finance/screens/RegisterDetail/RegisterDetailSummary/registerdetail.component';
import { WOReportComponent } from '../inventory/screens/RegisterWithoutItem/WOItemReports/WOReport.component';
import { StockPositionDetailComponent } from '../inventory/screens/StockPositionDetail/StockPositionDetail/StockPositionDetail.component';
import { stockpositionComponent } from '../inventory/screens/StockPositionSummary/StockPositionSummary/StockPositionSummary.component';
import { TimeslipComponent } from '../hrms/screens/Attendance/Timeslip/Timeslip.component';
import { MachinelistComponent } from '../maintenance/screens/Checklist/Machine List/Machinelist.component';
import { AssettypeComponent } from '../maintenance/screens/Checklist/Asset Type List/Assettype.component';
import { AssetactivityComponent } from '../maintenance/screens/Checklist/Asset Activity List/Assetactivity.component';
import { MachineTypeComponent } from '../maintenance/screens/Checklist/Machine Type List/MachineType.component';
import { EquipmentkeyComponent } from '../maintenance/screens/Checklist/Equipment Key Component List/Equipmentkey.component';
import { MatDialogModule } from '@angular/material/dialog';
import { DialogBoxComponent } from './screens/DialogBox/DialogBox.component';
import { brsdetailComponent } from '../finance/screens/BRSDetail/BRSDetailView/brsdetail.component';
import { brsregisterComponent } from '../finance/screens/BRSRegister/BRSRegisterView/brsregister.component';
import { EmployeeVsShiftComponent } from '../hrms/screens/EmployeeVsShift/EmployeeVsShiftView/EmployeeVsShift.component';
import { StockLedgerDetailComponent } from '../inventory/screens/Stock Ledger/Stock Ledger detail/StockLedgerDetail.component';
import { LateArrivalComponent } from '../hrms/screens/Attendance/Late Arrival HTML View/LateArrivalView.component';
import { pricecategoryComponent } from '../inventory/screens/PriceCategory/pricecategory.component';
import { attendancepunchComponent } from '../hrms/screens/Punches/AttendancePunch/attendancepunch.component';
import { EmployeedetailComponent } from '../hrms/screens/employee/EmployeeDetail/employeedetail.component';
import { ProductionSummaryComponent } from '../production/screens/Production/Summary/ProductionSummary.component';
import { StoppageDetailComponent } from '../production/screens/Stoppage/StoppageDetail.component';
import { AssettypeactivityComponent } from '../maintenance/screens/Checklist/Asset Type Activity/Assettypeactivity.component';
import { keycomponentComponent } from '../maintenance/screens/Checklist/KeyComponents/keycomponent.component';
import { StocklegersummaryComponent } from '../inventory/screens/Stock Ledger/Stock Ledger Summary/stocklegersummary.component';
import { WorkTypeComponent } from '../projects/Screen/Checklist/WorkType List/WorkType.component';
import { EarlyDepatureComponent } from '../hrms/screens/Attendance/Early Depature/EarlyDepature.component';
import { DatewiseminComponent } from '../inventory/screens/Register/DateWise/Datewise Min and Inward/Datewisemin.component';
import { MarginsummarylistComponent } from '../sales/screens/Margin/Margin Summary/Marginsummarylist.component';
import { StockageingsummaryComponent } from '../inventory/screens/Stock Ageing/Stock Ageing Summary/Stockageingsummary.component';
import { SalesQuotationWOItemComponent } from '../sales/screens/Register/Enquiry and Quotation WO Item/SalesQuotationWOItem.component';
import { RegisterDetailoneComponent } from '../purchase/screens/Register/RDetailone/registerdetailone.component';
import { RegisterDetailTwoComponent } from '../purchase/screens/Register/RDetailtwo/registerdetailtwo.component';
import { RegisterPartyWiseComponent } from '../purchase/screens/Register/RPartyWise/registerpartywise.component';
import { RegisterItemWiseComponent } from '../purchase/screens/Register/RItemWise/registeritemwise.component';
import { PendingRegisterDetailComponent } from '../purchase/screens/PendingRegister/PRDetail/pendingregisterdetail.component';
import { PendingRegisterWOItemComponent } from '../purchase/screens/PendingRegister/PRWOItem/pendingregisterwoitem.component';
import { PendingRegisterPartyWiseComponent } from '../purchase/screens/PendingRegister/PRPartyWise/pendingrPartyWise.component'; 
import { PendingRegDetComponent } from '../sales/screens/PendingRegister/Details/PendingRegDet.component';
import { pendingPOStatusComponent } from '../sales/screens/Status Register/Pending/PendingPOStatus.component';
import { SalesComSummaryComponent } from '../sales/screens/Sales Comparison/Summary/SalesComSummary.component';
import { ScVendorStockComponent } from '../sales/screens/Status Register/SC Vendor Stock/ScVendorStock.component';
import { SalesComparisonComponent } from '../sales/screens/Sales Comparison/Sales Comparison Details/SalesComparison.component'; 
import { InvoiceComponent } from '../sales/screens/Register/Invoice With Instrument/Invoice.component'; 
import { margindetailComponent } from '../sales/screens/Margin/Margin Detail/margindetail.component'; 
// import { ConsolidatedShortageComponent } from '../production/screens/Shortage/ConsolidatedShortage.component';
import { StockAgeingDetailComponent } from '../inventory/screens/Stock Ageing/Stock Ageing Detail/StockAgeingDetail.component';
import { ActivityRegisterViewComponent } from '../maintenance/screens/Register/Activity Register/ActivityRegisterView.component';
import { ScheduleDatewiseComponent } from '../maintenance/screens/Checklist/Schedule Datewise/ScheduleDatewise.component';
import { ProdRegisterReportComponent } from '../production/screens/Register/ProdRegisterReport.component';
import { WIPStockViewComponent } from '../production/screens/Production/WIP Stock/WIPStockView.component';
import { MachineWiseProdComponent } from '../production/screens/Production/Machine Wise Production/MachineWiseProd.component';
// import { GstDetailsComponent } from '../sales/screens/Duty & Tax/GST/Details/GstDetails.component';
// import { GstAbstractComponent } from '../sales/screens/Duty & Tax/GST/Abstract/GstAbstract.component';
// import { GstDocItemComponent } from '../sales/screens/Duty & Tax/GST/Document Item/GstDocItem.Component';
// import { HsnCodeWiseComponent } from '../sales/screens/Duty & Tax/GST/HSN Code Wise/HsnCodeWise.component';
// import { OuwiseDetailComponent } from '../sales/screens/Duty & Tax/Ou wise Detail/OuwiseDetail.component';
// import { OuwiseSummaryComponent } from '../sales/screens/Duty & Tax/Ou Wise Summary/OuwiseSummary.component';
import { WFApprovalComponent } from './screens/WorkFlow Approval/WFApproval.component';
import { LeadStageComponent } from '../crmsales/screens/leadstage/leadstage.component';
import { ContactListComponent } from '../crmsales/screens/checklist/contactlist.component';
import { SpecialCharacterPipe } from 'libs/gbcommon/src/lib/SpecialCharacterFormater.pipe';
import { PunchDetailsComponent } from '../hrms/screens/PunchDetails/PunchDetails.component';
// import { ContactComponent } from '../crmsales/screens/ContactlistInformation/contact.component';
// import { addressComponent } from '../crmsales/screens/Address/address.component';
// import { AddressListComponent } from '../crmsales/screens/AddressList/addresslist.component';
import { EmployeeHierarchyComponent } from '../hrms/screens/EmployeeHierarchy/employeehierarchy.component';
import { BomTreeComponent } from '../inventory/screens/BomTree/BomTree.component';

@NgModule({
  declarations: [
    WFApprovalComponent,
    DialogBoxComponent,
    DialogBoxComponent,
    AllControlFormComponent,
    NotificationComponent,
    MainMenuComponent,
    TruncatePipe,
    MenuListMainComponent,
    DateFormaterPipe,
    ShortNumberPipe,
    SubTotalPipe,
    SpecialCharacterPipe,
    VoucherLightBoxComponent,
    MMLightBoxComponent,
    SamplescreensComponent,
    RulesFormComponent,
    PackbothComponent,
    PackdbservComponent,
    PacknoneComponent,
    PackservComponent,
    PackstoreComponent,
    StorelistComponent,
    ProfileComponent,
    MapComponent,
    attachmentComponent,
    geolocationtestComponent,
    AngularFileUploaderComponent,
  
    permissionformComponent,
  
    permissionlistComponent,
    approvallistComponent,
    ChartviewComponent,
    MarginsummaryComponent,
    EntitylookuptestComponent,
    ApexChartComponent,
    GridViewComponent,
    StatementOfAccountComponent,
    ProductionMonitorComponent,
    PipelineDBComponent,
    GbmodulelistMainComponent,   
    NumberFormatPipe,
    
  ],
  imports: [
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatMenuModule,
    NgbModule,
    CommonModule,
    GblogModule,
    GbfilterModule,
    SamplescreensRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    UicoreModule,
    NgBreModule,
    UifwkUifwkmaterialModule,
    NgApexchartsModule,
    TranslocoRootModule,
    NgxsModule.forFeature([PackState,  PermissionState, ApexChartState]),
    NgxsFormPluginModule.forRoot(),
    AgGridModule,
    GbgridModule,
    GbdataModule.forRoot(environment),
    AgmCoreModule.forRoot({
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en
      apiKey: 'AIzaSyDvFSDxJXXR1nrdRDIzvp6DRZB9v649My8' ,
      libraries: ['places' ,'geometry','drawing']
    }),
    AgmDirectionModule,
    NgxChartsModule,
    GbchartModule,
    EntitylookupModule,
    geolocationModule,
    NgSelectModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatListModule,
    MatTabsModule,
    MatCheckboxModule,
    MatIconModule,
    MatExpansionModule,
    MatDividerModule,
    CommonReportModule,
    FormlyModule,
    FormlyMaterialModule


  ],
  exports: [
    SamplescreensComponent,
    GridViewComponent, 
    MMLightBoxComponent, 
    VoucherLightBoxComponent,
    ShortNumberPipe,
    DateFormaterPipe,
    SubTotalPipe,
    SpecialCharacterPipe,
    ApexChartComponent,
    MapComponent,
    TruncatePipe,
    NumberFormatPipe,
    DialogBoxComponent
  ],
  providers: [{ provide: LOCALE_ID, useValue: "en-IN" }],
})
export class SamplescreensModule {
  constructor() {
    this.addDashboardItems();
  }

  addDashboardItems() {
    DashboardItems.push({ portletName: "PacklistComponent", component: PacklistComponent });
    DashboardItems.push({ portletName: "PackSetlistComponent", component: PacksetlistComponent });
    DashboardItems.push({ portletName: "BrandListComponent", component: BrandlistComponent });
    // DashboardItems.push({ portletName: "ModelListComponent", component: ModellistComponent });
    DashboardItems.push({ portletName: "ItemCategorylistComponent", component: ItemCategorylistComponent });
    DashboardItems.push({ portletName: "ChartviewComponent", component: ChartviewComponent });
    DashboardItems.push({ portletName: "Employeemasterlist", component: EmployeemasterlistComponent });
    DashboardItems.push({ portletName: "EmployeeSkillList", component: EmployeeSkillListComponent });
    DashboardItems.push({ portletName: "ItemBrand", component: EmployeeSkillListComponent });
    DashboardItems.push({ portletName: "ItemOUList", component: ItemOUListViewComponent });
    DashboardItems.push({ portletName: "ApexChartComponent", component: ApexChartComponent });
    DashboardItems.push({ portletName: "StatementOfAccountComponent", component: StatementOfAccountComponent });
    DashboardItems.push({ portletName: "PipelineDBComponent", component: PipelineDBComponent });
    DashboardItems.push({ portletName: "ProductionMonitorComponent", component: ProductionMonitorComponent });
    DashboardItems.push({ portletName: "MMStatusComponent", component: MMStatusComponent });
    DashboardItems.push({ portletName: "WorkOrderDashboardComponent", component: WorkOrderDashboardComponent });
    DashboardItems.push({ portletName: "CashBookComponent", component: CashBookComponent });
    DashboardItems.push({ portletName: "AppraisalDashBoardComponent", component: AppraisalDashBoardComponent });
    DashboardItems.push({ portletName:"SitePunchComponent", component: SitePunchComponent});
    DashboardItems.push({ portletName:"UOMlistComponent", component: UOMlistComponent});
    // DashboardItems.push({ portletName:"ModellistComponent", component: ModellistComponent});
    DashboardItems.push({ portletName:"ItemBrandReportComponent", component: ItemBrandReportComponent});
    DashboardItems.push({ portletName:"StorelistComponent", component: StorelistComponent});
    DashboardItems.push({ portletName:"ItemGroupListComponent", component: ItemGroupListComponent});
    DashboardItems.push({ portletName:"itempartylistComponent", component: itempartylistComponent});
    DashboardItems.push({ portletName:"partyitemlistComponent", component: partyitemlistComponent});
    DashboardItems.push({ portletName:"ItemReportComponent", component: ItemReportComponent});
    DashboardItems.push({ portletName:"schedulelistComponent", component: schedulelistComponent});
    DashboardItems.push({ portletName:"AccountGroupComponent", component: AccountGroupComponent});
    DashboardItems.push({ portletName:"AccountListComponent", component: AccountListComponent});
    DashboardItems.push({ portletName:"leaverequestComponent", component: leaverequestComponent});
    DashboardItems.push({ portletName:"partybranchlistComponent", component: partybranchlistComponent});
    DashboardItems.push({ portletName:"shiftlistComponent", component: shiftlistComponent});
    DashboardItems.push({ portletName:"paymentsummaryComponent", component: paymentsummaryComponent});
    DashboardItems.push({ portletName:"sopackedsummaryComponent", component: sopackedsummaryComponent});
    DashboardItems.push({ portletName:"sopackeditemlevelComponent", component: sopackeditemlevelComponent});
    DashboardItems.push({ portletName:"itemwiseComponent", component: itemwiseComponent});
    DashboardItems.push({ portletName:"tbscheduleComponent", component: tbscheduleComponent});
    DashboardItems.push({ portletName:"tbgroupComponent", component: tbgroupComponent});
    DashboardItems.push({ portletName:"tbaccountComponent", component: tbaccountComponent});
    DashboardItems.push({ portletName:"tbscheduledetailComponent", component: tbscheduledetailComponent});
    DashboardItems.push({ portletName:"tbgroupdetailComponent", component: tbgroupdetailComponent});
    DashboardItems.push({ portletName:"tbaccountdetailComponent", component: tbaccountdetailComponent});
    DashboardItems.push({ portletName:"receivablebillwiseComponent", component: receivablebillwiseComponent});
    DashboardItems.push({ portletName:"receivablesdetailComponent", component: receivablesdetailComponent});
    DashboardItems.push({ portletName:"receivablesummaryComponent", component: receivablessummaryComponent});
    DashboardItems.push({ portletName:"bankbookComponent", component: bankbookComponent});
    DashboardItems.push({ portletName:"supplygroupComponent", component: supplygroupComponent});
    DashboardItems.push({portletName:"deductedcategoryComponent", component: deductedcategoryComponent});
    DashboardItems.push({portletName:"SalesEnquiryComponent", component:SalesEnquiryComponent});
    DashboardItems.push({portletName:"DailyAttendanceComponent", component:DailyAttendanceComponent});
    DashboardItems.push({portletName:"MonthlyAttendanceComponent", component:MonthlyAttendanceComponent});
    DashboardItems.push({ portletName:"daybookComponent", component: daybookComponent});
    DashboardItems.push({ portletName:"soasingleComponent", component: soasingleComponent});
    DashboardItems.push({ portletName:"ItemSubCategorylistComponent", component: ItemSubCategorylistComponent});
    DashboardItems.push({ portletName:"stockpositionComponent", component: stockpositionComponent});
    DashboardItems.push({ portletName:"StockPositionDetailComponent", component: StockPositionDetailComponent});
    DashboardItems.push({ portletName:"WOReportComponent", component: WOReportComponent});
    DashboardItems.push({ portletName:"registerdetailComponent", component: registerdetailComponent});
    DashboardItems.push({ portletName:"brssummaryComponent", component: brssummaryComponent});
    // DashboardItems.push({ portletName:"brsdetailComponent", component: brsdetailComponent});
    DashboardItems.push({portletName:"TimeslipComponent", component:TimeslipComponent});
    DashboardItems.push({portletName:"MachinelistComponent", component:MachinelistComponent});
    DashboardItems.push({portletName:"AssettypeComponent", component:AssettypeComponent});
    DashboardItems.push({portletName:"AssetactivityComponent", component:AssetactivityComponent});

    DashboardItems.push({portletName:"MachineTypeComponent", component:MachineTypeComponent});
    DashboardItems.push({portletName:"EquipmentkeyComponent", component:EquipmentkeyComponent});
    DashboardItems.push({ portletName:"brsdetailComponent", component: brsdetailComponent});
    DashboardItems.push({ portletName:"brsregisterComponent", component: brsregisterComponent});
    DashboardItems.push({ portletName:"EmployeeVsShiftComponent", component: EmployeeVsShiftComponent});
    DashboardItems.push({ portletName:"StockLedgerDetailComponent", component: StockLedgerDetailComponent});
    DashboardItems.push({ portletName:"LateArrivalComponent", component: LateArrivalComponent});
    DashboardItems.push({portletName:"attendancepunchComponent", component:attendancepunchComponent});
    DashboardItems.push({portletName:"pricecategoryComponent", component:pricecategoryComponent});
    DashboardItems.push({portletName:"EmployeedetailComponent", component:EmployeedetailComponent});
    DashboardItems.push({portletName:"ProductionSummaryComponent", component:ProductionSummaryComponent});
    DashboardItems.push({portletName:"StoppageDetailComponent", component:StoppageDetailComponent});
    DashboardItems.push({portletName:"AssettypeactivityComponent", component:AssettypeactivityComponent});
    DashboardItems.push({portletName:"keycomponentComponent", component:keycomponentComponent});
    DashboardItems.push({portletName:"StocklegersummaryComponent", component:StocklegersummaryComponent});
    DashboardItems.push({portletName:"WorkTypeComponent", component:WorkTypeComponent});
    DashboardItems.push({portletName:"EarlyDepatureComponent", component:EarlyDepatureComponent});
    DashboardItems.push({portletName:"DatewiseminComponent", component:DatewiseminComponent});
    DashboardItems.push({portletName:"MarginsummarylistComponent", component:MarginsummarylistComponent});
    DashboardItems.push({portletName:"StockageingsummaryComponent", component:StockageingsummaryComponent});
    DashboardItems.push({portletName:"SalesQuotationWOItemComponent", component:SalesQuotationWOItemComponent});
    DashboardItems.push({portletName:"RegisterDetailoneComponent", component:RegisterDetailoneComponent});
    DashboardItems.push({portletName:"RegisterDetailTwoComponent", component:RegisterDetailTwoComponent});
    DashboardItems.push({portletName:"RegisterPartyWiseComponent", component:RegisterPartyWiseComponent});
    DashboardItems.push({portletName:"RegisterItemWiseComponent", component:RegisterItemWiseComponent});
    DashboardItems.push({portletName:"PendingRegisterDetailComponent", component:PendingRegisterDetailComponent});
    DashboardItems.push({portletName:"PendingRegisterWOItemComponent", component:PendingRegisterWOItemComponent});
    DashboardItems.push({portletName:"PendingRegisterPartyWiseComponent", component:PendingRegisterPartyWiseComponent});
    DashboardItems.push({portletName:"PendingRegDetComponent", component:PendingRegDetComponent});
    DashboardItems.push({portletName:"pendingPOStatusComponent", component:pendingPOStatusComponent});
    DashboardItems.push({portletName:"SalesComSummaryComponent", component:SalesComSummaryComponent});
    DashboardItems.push({portletName:"ScVendorStockComponent", component:ScVendorStockComponent});
    DashboardItems.push({portletName:"SalesComparisonComponent", component:SalesComparisonComponent});
    DashboardItems.push({portletName:"InvoiceComponent", component:InvoiceComponent});
    DashboardItems.push({portletName:"margindetailComponent", component:margindetailComponent});
    // DashboardItems.push({portletName:"ConsolidatedShortageComponent", component:ConsolidatedShortageComponent});
    DashboardItems.push({portletName:"StockAgeingDetailComponent", component:StockAgeingDetailComponent});
    DashboardItems.push({portletName:"ActivityRegisterViewComponent", component:ActivityRegisterViewComponent});
    DashboardItems.push({portletName:"ScheduleDatewiseComponent", component:ScheduleDatewiseComponent});
    DashboardItems.push({portletName:"ProdRegisterReportComponent", component:ProdRegisterReportComponent});
    DashboardItems.push({portletName:"WIPStockViewComponent", component:WIPStockViewComponent});
    DashboardItems.push({portletName:"MachineWiseProdComponent", component:MachineWiseProdComponent});
    // DashboardItems.push({portletName:"GstDetailsComponent", component:GstDetailsComponent});
    // DashboardItems.push({portletName:"GstAbstractComponent", component:GstAbstractComponent});
    // DashboardItems.push({portletName:"GstDocItemComponent", component:GstDocItemComponent});
    // DashboardItems.push({portletName:"HsnCodeWiseComponent", component:HsnCodeWiseComponent});
    // DashboardItems.push({portletName:"OuwiseDetailComponent", component:OuwiseDetailComponent});
    // DashboardItems.push({portletName:"OuwiseSummaryComponent", component:OuwiseSummaryComponent});    DashboardItems.push({portletName:"LeadStageComponent", component:LeadStageComponent});
    DashboardItems.push({portletName:"LeadStageComponent", component:LeadStageComponent});
    DashboardItems.push({portletName:"ContactListComponent", component:ContactListComponent});
    // DashboardItems.push({portletName:"ContactComponent", component:ContactComponent});
    // DashboardItems.push({portletName:"addressComponent", component:addressComponent});
    // DashboardItems.push({portletName:"AddressListComponent", component:AddressListComponent});
    DashboardItems.push({portletName:"PunchDetailsComponent", component:PunchDetailsComponent});
    DashboardItems.push({portletName:"EmployeeHierarchyComponent", component:EmployeeHierarchyComponent});
    DashboardItems.push({portletName:"BomTreeComponent", component:BomTreeComponent});


  }
}













// [
// 	{
// 		"UserVsPortletId": -1499999607,
// 		"UserId": -1499998941,
// 		"UserCode": "ADMIN",
// 		"UserName": "ADMIN",
// 		"PageId": -1499999983,
// 		"PageCode": "sampledp",
// 		"PageName": "sampledp",
// 		"PortletId": -1899999893,
// 		"PortletCode": "PacklistComponent",
// 		"PortletName": "PacklistComponent",
// 		"CriteriaConfigId": -1,
// 		"CriteriaConfigName": "NONE",
// 		"UserVsPortletHeight": 300,
// 		"UserVsPortletWidth": 50,
// 		"UserVsPortletSlNo": 0,
// 		"UserVsPortletPortletRow": 5,
// 		"UserVsPortletPortletColumn": 3,
// 		"X": 0,
// 		"Y": 0,
// 		"Component": "PacklistComponent",
// 		"TypeId": "PacklistComponent"
// 	},
// 	{
// 		"UserVsPortletId": -1499999606,
// 		"UserId": -1499998941,
// 		"UserCode": "ADMIN",
// 		"UserName": "ADMIN",
// 		"PageId": -1499999983,
// 		"PageCode": "sampledp",
// 		"PageName": "sampledp",
// 		"PortletId": -1899999906,
// 		"PortletCode": "PackSetlistComponent",
// 		"PortletName": "PackSetlistComponent",
// 		"CriteriaConfigId": -1,
// 		"CriteriaConfigName": "NONE",
// 		"UserVsPortletHeight": 300,
// 		"UserVsPortletWidth": 50,
// 		"UserVsPortletSlNo": 0,
// 		"UserVsPortletPortletRow": 3,
// 		"UserVsPortletPortletColumn": 4,
// 		"X": 4,
// 		"Y": 0
// 	},
// 	{
// 		"UserVsPortletId": -1499999606,
// 		"UserId": -1499998941,
// 		"UserCode": "ADMIN",
// 		"UserName": "ADMIN",
// 		"PageId": -1499999983,
// 		"PageCode": "sampledp",
// 		"PageName": "sampledp",
// 		"PortletId": -1899999906,
// 		"PortletCode": "BrandListComponent",
// 		"PortletName": "BrandListComponent",
// 		"CriteriaConfigId": -1,
// 		"CriteriaConfigName": "NONE",
// 		"UserVsPortletHeight": 300,
// 		"UserVsPortletWidth": 50,
// 		"UserVsPortletSlNo": 0,
// 		"UserVsPortletPortletRow": 2,
// 		"UserVsPortletPortletColumn": 2,
// 		"X": 4,
// 		"Y": 5
// 	},
// 	{
// 		"UserVsPortletId": -1499999607,
// 		"UserId": -1499998942,
// 		"UserCode": "ADMIN",
// 		"UserName": "ADMIN",
// 		"PageId": -1499999984,
// 		"PageCode": "sampledp",
// 		"PageName": "sampledp",
// 		"PortletId": -1899999907,
// 		"PortletCode": "ModelListComponent",
// 		"PortletName": "ModelListComponent",
// 		"CriteriaConfigId": -1,
// 		"CriteriaConfigName": "NONE",
// 		"UserVsPortletHeight": 300,
// 		"UserVsPortletWidth": 50,
// 		"UserVsPortletSlNo": 0,
// 		"UserVsPortletPortletRow": 2,
// 		"UserVsPortletPortletColumn": 2,
// 		"X": 4,
// 		"Y": 5
// 	},
// 	{
// 		"UserVsPortletId": -1499999608,
// 		"UserId": -1499998943,
// 		"UserCode": "ADMIN",
// 		"UserName": "ADMIN",
// 		"PageId": -1499999985,
// 		"PageCode": "sampledp",
// 		"PageName": "sampledp",
// 		"PortletId": -1899999908,
// 		"PortletCode": "UOMListComponent",
// 		"PortletName": "UOMListComponent",
// 		"CriteriaConfigId": -1,
// 		"CriteriaConfigName": "NONE",
// 		"UserVsPortletHeight": 300,
// 		"UserVsPortletWidth": 50,
// 		"UserVsPortletSlNo": 0,
// 		"UserVsPortletPortletRow": 2,
// 		"UserVsPortletPortletColumn": 2,
// 		"X": 4,
// 		"Y": 5
// 	},
// 	{
// 		"UserVsPortletId": -1499999609,
// 		"UserId": -1499998944,
// 		"UserCode": "ADMIN",
// 		"UserName": "ADMIN",
// 		"PageId": -1499999986,
// 		"PageCode": "sampledp",
// 		"PageName": "sampledp",
// 		"PortletId": -1899999909,
// 		"PortletCode": "ItemCategoryComponent",
// 		"PortletName": "ItemCategoryComponent",
// 		"CriteriaConfigId": -1,
// 		"CriteriaConfigName": "NONE",
// 		"UserVsPortletHeight": 300,
// 		"UserVsPortletWidth": 50,
// 		"UserVsPortletSlNo": 0,
// 		"UserVsPortletPortletRow": 2,
// 		"UserVsPortletPortletColumn": 2,
// 		"X": 4,
// 		"Y": 5
// 	},
// 	{
// 		"UserVsPortletId": -1499999610,
// 		"UserId": -1499998945,
// 		"UserCode": "ADMIN",
// 		"UserName": "ADMIN",
// 		"PageId": -1499999987,
// 		"PageCode": "sampledp",
// 		"PageName": "sampledp",
// 		"PortletId": -1899999910,
// 		"PortletCode": "ItemSubCategoryComponent",
// 		"PortletName": "ItemSubCategoryComponent",
// 		"CriteriaConfigId": -1,
// 		"CriteriaConfigName": "NONE",
// 		"UserVsPortletHeight": 300,
// 		"UserVsPortletWidth": 50,
// 		"UserVsPortletSlNo": 0,
// 		"UserVsPortletPortletRow": 2,
// 		"UserVsPortletPortletColumn": 2,
// 		"X": 4,
// 		"Y": 5
// 	}
// ]