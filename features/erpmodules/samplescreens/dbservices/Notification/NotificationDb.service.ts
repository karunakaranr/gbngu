import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';
import { AuthState } from 'features/common/shared/stores/auth.state';
import { Select } from '@ngxs/store';
@Injectable({
    providedIn: 'root',
})
export class NotificationDbService {
    @Select(AuthState.AddDTO) LOGINDTO$: Observable<any>;
    constructor(public http: GBHttpService) { }
    
    public notification(): Observable<any> {
        let userid;
        this.LOGINDTO$.subscribe(dto => {
            userid = dto.UserId;
        })
        let criteria = {

            "SectionCriteriaList": [
                {
                    "SectionId": 0,
                    "AttributesCriteriaList": [{
                        "FieldName": "ToUserId",
                        "OperationType": 5,
                        "FieldValue": userid,
                        "JoinType": 2,
                        "InArray":null
                    }
                    ],
                    "OperationType": 0
                }
            ]

        }
        const url = 'fws/Notification.svc/Notification/?FirstNumber=0&MaxResult=5000';
        return this.http.httppost(url, criteria);
    }
}