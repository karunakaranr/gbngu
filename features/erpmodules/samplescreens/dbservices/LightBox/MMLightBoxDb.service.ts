import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';
@Injectable({
    providedIn: 'root',
})
export class MMLightBoxDbService {
    rowcriteria ;
    constructor(public http: GBHttpService) { }

    public picklist(data): Observable<any> {
        const url = '/mms/MMHead.svc/?DocumentId='+data;
        return this.http.httpget(url);
    }
}