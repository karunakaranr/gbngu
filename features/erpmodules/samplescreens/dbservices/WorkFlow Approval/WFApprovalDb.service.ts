
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';
import { AuthState } from 'features/common/shared/stores/auth.state';
import { Select } from '@ngxs/store';
import { approvallist, approval } from './../../models/iapproval.model'
import { Store } from '@ngxs/store';

const urls = require('../../URLS/urls.json');


@Injectable({
    providedIn: 'root',
})

export class WFApprovalDbService {
    @Select(AuthState.AddDTO) LOGINDTO$: Observable<any>;
    UserCode: string
    constructor(public http: GBHttpService) {
       
     }

    public Requestbyme(): Observable<any> {
        this.LOGINDTO$.subscribe(dto => {
            console.log("LOGINDTO:",dto)
            this.UserCode = dto.UserCode;
        })
        let criteria = "";
        const url = "/fws/Workflow.svc/WorkFlowList/?Type=1&WorkFlowUserId="+this.UserCode+"&EntityCode=NONE&FirstNumber=1&MaxResult=20";
        return this.http.httppost(url, criteria);
    }

    public Requesttome(): Observable<any> {
        this.LOGINDTO$.subscribe(dto => {
            console.log("LOGINDTO:",dto)
            this.UserCode = dto.UserCode;
        })
        let criteria = "";
        const url = "/fws/Workflow.svc/WorkFlowList/?Type=0&WorkFlowUserId="+this.UserCode+"&EntityCode=NONE&FirstNumber=1&MaxResult=20";
        return this.http.httppost(url, criteria);
    }
    public Requesttouser(picklistUser,DocumentNumber): Observable<any> {
        this.LOGINDTO$.subscribe(dto => {
            console.log("LOGINDTO:",dto)
            this.UserCode = dto.UserCode;
        })
        let criteria = {
            "SectionCriteriaList": [
                {
                    "SectionId": 0,
                    "AttributesCriteriaList": [
                        {
                            "FieldName": "FromUserId",
                            "OperationType": 1,
                            "FieldValue": picklistUser,
                            "InArray": null,
                            "JoinType": 2
                        },
                        {
                            "FieldName": "DocumentNumber",
                            "OperationType": 2,
                            "FieldValue": DocumentNumber,
                            "InArray": null,
                            "JoinType": 2
                        },
                        {
                            "FieldName": "Filter",
                            "OperationType": 5,
                            "FieldValue": 1,
                            "InArray": null,
                            "JoinType": 2
                        },
                        {
                            "FieldName": "Type",
                            "OperationType": 5,
                            "FieldValue": 1,
                            "InArray": null,
                            "JoinType": 0
                        }
                    ]
                }
            ]
        };
        const url = "/fws/Workflow.svc/WorkFlowList/?Type=-2&WorkFlowUserId="+this.UserCode+"&EntityCode=NONE&FirstNumber=1&MaxResult=20";
        return this.http.httppost(url, criteria);
    }
    public SearchItem(SearchItem): Observable<any> {
        let criteria = {
            "SectionCriteriaList": [{
                "SectionId": 0,
                "AttributesCriteriaList": [{
                    "FieldName": "SearchField",
                    "OperationType": 1,
                    "FieldValue": SearchItem,
                    "InArray": null,
                    "JoinType": 0
                }, {
                    "FieldName": "Type",
                    "OperationType": 1,
                    "FieldValue": "1",
                    "InArray": null,
                    "JoinType": 0
                }],
                "OperationType": 0
            }]
        };
        const url = "/fws/Workflow.svc/WorkFlowList/?Type=-1&WorkFlowUserId=DEMO&EntityCode=NONE&FirstNumber=1&MaxResult=20";
        return this.http.httppost(url, criteria);
    }

    public AproveItem(formattedItems: any[]): Observable<approval> {
        const url = "/fws/Workflow.svc/Approve/";
        return this.http.httppost(url, formattedItems);
    }

    public RejectedItem(formattedItems: any[]): Observable<approval> {
        const url = "/fws/Workflow.svc/Reject/";
        return this.http.httppost(url, formattedItems);
    }

    public ResendItem(formattedItems: any[]): Observable<approval> {
        const url = "fws/Workflow.svc/Resend/";
        return this.http.httppost(url, formattedItems);
    }

    public lighboxsend(mailData): Observable<any> {
      
        
        const url =  "/fws/Mail.svc/SendMail";
        return this.http.httppost(url, mailData);
    }

    public forwardData(): Observable<approval> {
        const url = "fws/Workflow.svc/WorkFlowList/?Type=0&WorkFlowUserId=DEMO&EntityCode=NONE&FirstNumber=81&MaxResult=20";
        let criteria = "";

        return this.http.httppost(url, criteria);
    }

    public backwardData(): Observable<approval> {
        const url = "fws/Workflow.svc/WorkFlowList/?Type=0&WorkFlowUserId=DEMO&EntityCode=NONE&FirstNumber=1&MaxResult=20";
        let criteria = "";

        return this.http.httppost(url, criteria);
    }
    
    
}     