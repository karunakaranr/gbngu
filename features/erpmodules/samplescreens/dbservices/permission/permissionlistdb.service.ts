import { Injectable } from '@angular/core';

import { GBHttpService } from '@goodbooks/gbcommon';
import { GBBaseDBDataService, ISelectListCriteria } from '@goodbooks/gbdata';
import { Observable } from 'rxjs';

import { permissionlist } from '../../models/permission/ipermission.model';
const urls = require('../../URLS/urls.json');

@Injectable({
  providedIn: 'root',
})
export class PermissionlistDBService extends GBBaseDBDataService<permissionlist> {
  endPoint: string = urls.permissionlist;
  firstNumner=1;
  maxResult=50;
  useAPIPagination=true;

  constructor(http: GBHttpService) {
    super(http);
  }

  putData(data: permissionlist) {
    return super.putData(data);
  }

  postData(data: permissionlist) {
    return super.postData(data);
  }

  getData(idField: string, id: string): Observable<permissionlist> {
    return super.getData(idField, id);
  }

  deleteData(idField: string, id: string) {
    return super.deleteData(idField, id)
  }

  getList(scl: ISelectListCriteria): Observable<any> {
    scl= {
      "SectionCriteriaList": [
        {
          "SectionId": 0,
          "AttributesCriteriaList": [
            {
              "FieldName": "BizTransactionTypeId",
              "OperationType": 5,
              "FieldValue": "-1499996142",
              "InArray": null,
              "JoinType": 2
            }
          ],
          "OperationType": 0
        }
      ]
    }
    return super.getList(scl);
  }
}
