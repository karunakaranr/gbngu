import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';
import { approvallist,approval } from './../../models/iapproval.model';
const urls = require('../../URLS/urls.json');
@Injectable({
  providedIn: 'root',
})
export class approvaldbservice {
   approvalitems:approval
  constructor(private http: GBHttpService) { }
  public approvaltomeservice(): Observable<approvallist> {
    return this.http.httppost(urls.requestedtome,'');

  } 
  public approvalbymeservice(): Observable<approvallist> {
    return this.http.httppost(urls.requestedbyme,'');

  } 
  public approvalservice(approvaljson) : Observable<approval>{
   
  
  return this.http.httppost(urls.approve,approvaljson);
 
  }

  public rejectservice(approvaljson): Observable<approval> {
    
    
  
    return  this.http.httppost(urls.reject,approvaljson);
 
   }

   public resendservice(approvaljson): Observable<approval> {
 
 
    return  this.http.httppost(urls.resend,approvaljson);
 
   }
 
}
