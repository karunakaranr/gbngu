import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { PipelineDB } from '../../services/Dashboard/PipelineDB/PipelineDB.service';
import { Observable } from 'rxjs';
import { LayoutState } from './../.././../../layout/store/layout.state'
import { Select,Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { GbPipelineDB } from '../../stores/PipelineDB/PipelineDB.action';
import { LOCALE_ID } from '@angular/core';
import {
  ApexAxisChartSeries,
  ApexChart,
  ChartComponent,
  ApexDataLabels,
  ApexPlotOptions,
  ApexResponsive,
  ApexXAxis,
  ApexLegend,
  ApexFill
} from "ng-apexcharts";
import { FormBuilder, FormGroup } from '@angular/forms';
export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  dataLabels: ApexDataLabels;
  plotOptions: ApexPlotOptions;
  responsive: ApexResponsive[];
  xaxis: ApexXAxis;
  legend: ApexLegend;
  fill: ApexFill;
};
@Component({
  selector: 'app-PipelineDB',
  templateUrl: './PipelineDB.component.html',
  styleUrls: ['./PipelineDB.component.scss']
})

export class PipelineDBComponent implements OnInit {
  @ViewChild("chart") chart: ChartComponent;
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  title = 'Pipe Line DB';
  TypeList = ['Line','Bar'];
  lightBoxForm: FormGroup;
  rowData = []
  PipelineDTOs;
  PipeLineInchargeDTOs;
  PipelineAgeDTOs;
  PipelineDTOsAmt;
  PipeLineInchargeDTOsAmt;
  PipelineAgeDTOsAmt;
  chartOptions1;
  chartOptions2;
  chartOptions3;
  count = 0;
  M1;
  M2;
  M3;
  M4;
  M5;
  chartdata1 = [
    {
      series: [],
      chart: {
        type: "bar",
        height: 350,
        stacked: true,
        toolbar: {
          show: true
        }
      },
      plotOptions: {
        bar: {
          horizontal: true
        }
      },
      title: {
        text: "Pipeline DTOs",
        align: "center"
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              position: "bottom",
              offsetX: -10,
              offsetY: 0
            }
          }
        }
      ],
      xaxis: {
        categories: []
      },
      fill: {
        opacity: 1
      },
      legend: {
        position: "top",
        horizontalAlign: "left",
        offsetX: 40
      }
    }];
  chartdata2 = [
    {
      series: [],
      chart: {
        type: "bar",
        height: 350,
        stacked: true,
        toolbar: {
          show: true
        }
      },
      plotOptions: {
        bar: {
          horizontal: true
        }
      },
      title: {
        text: "PipeLine Incharge DTOs",
        align: "center"
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              position: "bottom",
              offsetX: -10,
              offsetY: 0
            }
          }
        }
      ],
      xaxis: {
        categories: []
      },
      fill: {
        opacity: 1
      },
      legend: {
        position: "top",
        horizontalAlign: "left",
        offsetX: 40
      }
    }];
    chartdata3=[
      {
          series: [],
          chart: {
              type: "bar",
              height: 350,
              toolbar: {
                  show: true
              }
          },
          plotOptions: {
            bar: {
              horizontal: false
            }
          },
          title: {
              text: "PipeLine Age DTOs",
              align: "center"
          },
          responsive: [
            {
              breakpoint: 480,
              options: {
                legend: {
                  position: "bottom",
                  offsetX: -10,
                  offsetY: 0
                }
              }
            }
          ],
          xaxis: {
            categories: []
          },
          fill: {
            opacity: 1
          },
          legend: {
            position: "top",
            horizontalAlign: "left",
            offsetX: 40
          }
        }];
  @Select(LayoutState.ThemeClass) themeClass$: Observable<string>;
  constructor(public service: PipelineDB, public store: Store, private fb: FormBuilder,@Inject(LOCALE_ID) localeId) { }
  ngOnInit(): void {
    this.lightBoxForm = this.fb.group({
      label: '',
      series: '',
      charttype: '',
      charttype1:'bar'
    });
    this.PipelineDB();
  }
  public PipelineDB() {
    this.rowdatacommon$.subscribe(id => {
      this.rowData=id
      if(this.rowData!=null){
        this.M1=decodeURIComponent(JSON.parse('"' + this.rowData[0].PipelineDTOs[0].M1.replace(/\"/g, '\\"') + '"'))
        this.M2=decodeURIComponent(JSON.parse('"' + this.rowData[0].PipelineDTOs[0].M2.replace(/\"/g, '\\"') + '"'))
        this.M3=decodeURIComponent(JSON.parse('"' + this.rowData[0].PipelineDTOs[0].M3.replace(/\"/g, '\\"') + '"'))
        this.M4=decodeURIComponent(JSON.parse('"' + this.rowData[0].PipelineDTOs[0].M4.replace(/\"/g, '\\"') + '"'))
        this.M5=decodeURIComponent(JSON.parse('"' + this.rowData[0].PipelineDTOs[0].M5.replace(/\"/g, '\\"') + '"'))
        this.PipelineDTOsAmt=this.rowData[0].PipelineDTOs[0].AmountIn;
        this.PipeLineInchargeDTOsAmt=this.rowData[0].PipeLineInchargeDTOs[0].AmountIn;
        this.PipelineAgeDTOsAmt=this.rowData[0].PipelineAgeDTOs[0].AmountIn;
        if(this.PipelineDTOsAmt==""){
          this.PipelineDTOsAmt="Rupees"
          this.PipeLineInchargeDTOsAmt="Rupees"
          this.PipelineAgeDTOsAmt="Rupees"
        }
        console.log("Pipe Line DTO:",this.PipelineDTOsAmt,this.PipeLineInchargeDTOsAmt,this.PipelineAgeDTOsAmt)
        this.Chartdata();
      }
    })
  }

  public Chartdata(){
    this.PipelineDTOs = this.rowData[0].PipelineDTOs;
        this.PipeLineInchargeDTOs = this.rowData[0].PipeLineInchargeDTOs;
        this.PipelineAgeDTOs = this.rowData[0].PipelineAgeDTOs;
        this.chartdata1[0].series = []
        for (let value of this.rowData[0].PipelineDTOs) {
          let a = value.StageName
          this.chartdata1[0].series.push({ name: a, data: [] })
        }
        this.chartdata1[0].series.pop()
        for (let item of this.chartdata1[0].series) {
          item.data = []
          let data = this.rowData[0].PipelineDTOs[this.count];
          item.data.push(data['Eno'])
          item.data.push(data['Mno'])
          item.data.push(data['M1No'])
          item.data.push(data['M2No'])
          item.data.push(data['Lno'])
          this.count = this.count + 1;
        }
        this.chartdata1[0].xaxis.categories = []
        let data = this.rowData[0].PipelineDTOs[0];
        this.chartdata1[0].xaxis.categories.push(decodeURIComponent(JSON.parse('"' + data['M1'].replace(/\"/g, '\\"') + '"')))
        this.chartdata1[0].xaxis.categories.push(decodeURIComponent(JSON.parse('"' + data['M2'].replace(/\"/g, '\\"') + '"')))
        this.chartdata1[0].xaxis.categories.push(decodeURIComponent(JSON.parse('"' + data['M3'].replace(/\"/g, '\\"') + '"')))
        this.chartdata1[0].xaxis.categories.push(decodeURIComponent(JSON.parse('"' + data['M4'].replace(/\"/g, '\\"') + '"')))
        this.chartdata1[0].xaxis.categories.push(decodeURIComponent(JSON.parse('"' + data['M5'].replace(/\"/g, '\\"') + '"')))
        this.chartlist(this.chartdata1)

        this.count = 0
        this.chartdata2[0].series = []
        for (let value of this.rowData[0].PipeLineInchargeDTOs) {
          let a = value.Name
          this.chartdata2[0].series.push({ name: a, data: [] })
        }
        this.chartdata2[0].series.pop()
        for (let item of this.chartdata2[0].series) {
          item.data = []
          let data = this.rowData[0].PipeLineInchargeDTOs[this.count];
          item.data.push(data['Eno'])
          item.data.push(data['Mno'])
          item.data.push(data['M1No'])
          item.data.push(data['M2No'])
          item.data.push(data['Lno'])
          this.count = this.count + 1;
        }
        this.chartdata2[0].xaxis.categories = []
        data = this.rowData[0].PipelineDTOs[0];
        this.chartdata2[0].xaxis.categories.push(decodeURIComponent(JSON.parse('"' + data['M1'].replace(/\"/g, '\\"') + '"')))
        this.chartdata2[0].xaxis.categories.push(decodeURIComponent(JSON.parse('"' + data['M2'].replace(/\"/g, '\\"') + '"')))
        this.chartdata2[0].xaxis.categories.push(decodeURIComponent(JSON.parse('"' + data['M3'].replace(/\"/g, '\\"') + '"')))
        this.chartdata2[0].xaxis.categories.push(decodeURIComponent(JSON.parse('"' + data['M4'].replace(/\"/g, '\\"') + '"')))
        this.chartdata2[0].xaxis.categories.push(decodeURIComponent(JSON.parse('"' + data['M5'].replace(/\"/g, '\\"') + '"')))
        this.chartlist1(this.chartdata2)

        this.count = 0
        this.chartdata3[0].series = []
        this.chartdata3[0].series.push({ name: 'Total', data: [] })
        for (let item of this.chartdata3[0].series) {
          item.data = []
          item.data.push(this.rowData[0].PipelineAgeDTOs[0].TNo)
          for(let data of this.rowData[0].PipelineAgeDTOs){
          item.data.push(data['TNo'])
          }
          item.data.pop();
        }
        this.chartdata3[0].xaxis.categories = []
        this.chartlist2(this.chartdata3)
  }

  public chartlist(chartdata) {

    this.chartOptions1 = {
      series: chartdata[0].series,

      chart: chartdata[0].chart,

      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: 'straight',
        width: 3,
      },
      grid: {
        padding: {
          right: 30,
          left: 20,
        },
      },
      title: chartdata[0].title,
      plotOptions: {
        bar: {
          horizontal: true
        }
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              position: "bottom",
              offsetX: -10,
              offsetY: 0
            }
          }
        }
      ],
      xaxis: chartdata[0].xaxis,
      fill: {
        opacity: 1
      },
      legend: {
        position: "top",
        horizontalAlign: "left",
        offsetX: 40
      }

    };
    console.log("Chart1:" + JSON.stringify(this.chartOptions1));
  }

  public chartlist1(chartdata) {

    this.chartOptions2 = {
      series: chartdata[0].series,

      chart: chartdata[0].chart,

      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: 'straight',
        width: 3,
      },
      grid: {
        padding: {
          right: 30,
          left: 20,
        },
      },
      title: chartdata[0].title,
      plotOptions: {
        bar: {
          horizontal: true
        }
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              position: "bottom",
              offsetX: -10,
              offsetY: 0
            }
          }
        }
      ],
      xaxis: chartdata[0].xaxis,
      fill: {
        opacity: 1
      },
      legend: {
        position: "top",
        horizontalAlign: "left",
        offsetX: 40
      }

    };
    console.log("Chart:" + JSON.stringify(this.chartOptions2));
  }

  public chartlist2(chartdata) {

    this.chartOptions3 = {
      series: chartdata[0].series,

      chart: chartdata[0].chart,

      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: 'straight',
        width: 3,
      },
      grid: {
        padding: {
          right: 30,
          left: 20,
        },
      },
      plotOptions: {
        bar: {
          horizontal: true
        }
      },
      title: chartdata[0].title,
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              position: "bottom",
              offsetX: -10,
              offsetY: 0
            }
          }
        }
      ],
      fill: {
        opacity: 1
      },
      legend: {
        position: "top",
        horizontalAlign: "left",
        offsetX: 40
      }

    };
    console.log("Chart:" + JSON.stringify(this.chartOptions3));
  }
  changeChart(value) {
    // if (value == "Line") {
    //   this.chartdata[0].chart.type = "line"
    //   this.chartlist(this.chartdata)
    // }
    // if (value == "Pie") {
    //   this.chartdata[0].chart.type = "pie"
    //   this.chartlist1(this.chartdata)
    // }
    // if (value == "Bar") {
    //   this.chartdata[0].chart.type = "bar"
    //   this.chartlist(this.chartdata)
    // }
  }

  public btnfilter() {
    document.getElementById("myModal1").style.display = "block";
  }

  public close() {
    document.getElementById("myModal1").style.display = "none";
  }

  public btnfilter2() {
    document.getElementById("myModal2").style.display = "block";
  }
  
  public close2() {
    document.getElementById("myModal2").style.display = "none";
  }

  public btnfilter3() {
    document.getElementById("myModal3").style.display = "block";
  }
  
  public close3() {
    document.getElementById("myModal3").style.display = "none";
  }

  public applybutton() {
    
    let chartdata=this.chartdata1
    // let a1 = this.lightBoxForm.get('series').value
    // chartdata1[0].series = []
    // for( let item of a1){
    //  let temp=[]
    //  for (let data of this.rowData){
    //   temp.push(data[item])
    //  }
    //  chartdata1[0].series.push({name:item,data:temp})
    // }
    // let b1 = this.lightBoxForm.get("label").value
    // chartdata1[0].labels = []
    // for (let data of this.rowData) {
    //   chartdata1[0].labels.push(data[b1])
    // }
    let value=this.lightBoxForm.get("charttype").value
    if (value == "Line") {
      this.chartdata1[0].chart.type = "line"
      this.chartlist(this.chartdata1)
    }
    if (value == "Bar") {
      this.chartdata1[0].chart.type = "bar"
      this.chartlist(this.chartdata1)
    }
    document.getElementById("myModal1").style.display = "none";
  }

  get charttype() {
    return this.lightBoxForm.get('charttype1')
  };

  public clearbutton(){
    this.lightBoxForm.reset();
    this.charttype.setValue(this.chartdata1[0].chart.type)
  }
  public applybutton1() {
    console.log("Apply1");
    let chartdata=this.chartdata2
    // let a1 = this.lightBoxForm.get('series').value
    // chartdata1[0].series = []
    // for( let item of a1){
    //  let temp=[]
    //  for (let data of this.rowData){
    //   temp.push(data[item])
    //  }
    //  chartdata1[0].series.push({name:item,data:temp})
    // }
    // let b1 = this.lightBoxForm.get("label").value
    // chartdata1[0].labels = []
    // for (let data of this.rowData) {
    //   chartdata1[0].labels.push(data[b1])
    // }
    let value=this.lightBoxForm.get("charttype").value
    if (value == "Line") {
      this.chartdata2[0].chart.type = "line"
      this.chartlist1(this.chartdata2)
    }
    if (value == "Bar") {
      this.chartdata2[0].chart.type = "bar"
      this.chartlist1(this.chartdata2)
    }
    document.getElementById("myModal1").style.display = "none";
  }
  public applybutton2() {
    console.log("Apply1");
    let chartdata=this.chartdata1
    // let a1 = this.lightBoxForm.get('series').value
    // chartdata1[0].series = []
    // for( let item of a1){
    //  let temp=[]
    //  for (let data of this.rowData){
    //   temp.push(data[item])
    //  }
    //  chartdata1[0].series.push({name:item,data:temp})
    // }
    // let b1 = this.lightBoxForm.get("label").value
    // chartdata1[0].labels = []
    // for (let data of this.rowData) {
    //   chartdata1[0].labels.push(data[b1])
    // }
    let value=this.lightBoxForm.get("charttype").value
    if (value == "Line") {
      this.chartdata3[0].chart.type = "line"
      this.chartlist2(this.chartdata3)
    }
    if (value == "Bar") {
      this.chartdata3[0].chart.type = "bar"
      this.chartlist2(this.chartdata3)
    }
    document.getElementById("myModal1").style.display = "none";
  }
}