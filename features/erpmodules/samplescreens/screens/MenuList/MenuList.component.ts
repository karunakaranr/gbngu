import { Component, OnInit } from '@angular/core';
import { modulestate } from 'libs/gbui/src/lib/store/modulelist.state';
import { Select, Store } from '@ngxs/store';
import { ButtonVisibility, OpenMenuBar, Path, Title, RolesAndRights, BizTransactionClassId, FormEditable, FormUnEditable, GCMTypeId, Reporttypebtn } from 'features/layout/store/layout.actions';
import { LayoutState } from 'features/layout/store/layout.state';
import { CloseMenuBar } from 'features/layout/store/layout.actions';
import { MenuListservice } from 'features/layout/services/MenuList/MenuList.service';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { IMenulist } from '../../models/Imenu.model';
import { Reportid } from 'features/commonreport/datastore/commonreport.action';
import { MenuService } from '../../services/menulist/menulist.service';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
// const frequent:IMenulist = require('./FrequentlyUsed.json')
@Component({
  selector: 'app-MenuList',
  templateUrl: './MenuList.component.html',
  styleUrls: ['./MenuList.component.scss']
})
export class MenuListMainComponent implements OnInit {
  @Select(modulestate.getmoduleid) selectedId: any;
  @Select(LayoutState.CurrentTheme) currenttheme: any;
  @Select(LayoutState.Title) titlepath$: any;
  @Select(LayoutState.FormEdit) edit$: Observable<boolean>;
  @Select(LayoutState.MenuList) menulist$:any;
  MenuList: IMenulist;
  theme: any='Default'
  MenuPath:string;
  MenuListData:any = []
  MenuParamData:string;
  // frequentlyused: IMenulist=frequent;
  constructor(public store: Store, private menulistservice: MenuListservice,private route: ActivatedRoute,public router: Router, public service: MenuService) {
  }

  ngOnInit(): void {
    this.store.dispatch(new FormUnEditable);
    // this.store.dispatch(new CloseMenuBar)
    this.menulist$.subscribe(menudata =>{
      this.MenuListData = menudata
      this.MenuList=this.MenuListData;
      this.route.queryParams.subscribe(queryParams => {
        const Data = queryParams['data'];
        this.MenuParamData = Data;
        console.log("this.MenuParamData:",this.MenuParamData)
        if(Data == undefined){
          this.MenuList=this.MenuListData;
        } else {
          let menudata = this.MenuParamData.split(' > ')
          console.log("this.MenuListData:",this.MenuListData)
          for(let data of this.MenuListData){
            if(data.Id == parseInt(menudata[0])){
              if(menudata.length == 1){
                this.MenuList=data;
              } else {
                this.MenuListSearch(data,menudata,1)
              }
              break;
            }
          }
        }
        
      });
      
    })
    this.currenttheme.subscribe( (theme:String) => {
      if (theme =='Blue') {
        this.theme=theme;
      } else {
        this.theme='Default'
      }
    })
    this.route.params.subscribe(params => {
        const id = params['id'];
        // this.MenuList=this.menulistservice.MenuListData()
        this.MenuPath=id;
        this.store.dispatch(new Title(this.MenuPath))
        // Now 'id' holds the value of the 'id' parameter from the route
        // Use it for your component logic
      });
    // this.store.dispatch(new CloseMenuBar)
    this.store.dispatch(new ButtonVisibility("MenuList"))
    // this.MenuPath=this.MenuList.ModuleName+">"+this.MenuList.DisplayName
    // this.store.dispatch(new Title(this.MenuPath))
    this.currenttheme.subscribe( (theme:String) => {
      if (theme =='Blue') {
        this.theme=theme;
      } else {
        this.theme='Default'
      }
    })

    this.edit$.pipe(take(1)).subscribe(newValue => {
      console.log("Menu",newValue)
      if(newValue == true){
        this.store.dispatch(new FormUnEditable);
      }
    });
  }

  public MenuListSearch(menudata,idlist,index){
    console.log("menudata:",menudata)
    for( let data of menudata.children){
      if(data.Id == parseInt(idlist[index])){
        if(idlist.length == index+1){
          this.MenuList=data;
        } else {
          this.MenuListSearch(data,idlist,index+1)
        }
        break;
      }
    }
  }

  public MenuData(data:IMenulist) {
    console.log("Menu Data:",data)
    this.store.dispatch(new BizTransactionClassId(data.BizTransactionClassId));
    this.service.RolesAndRightsView(data.Id,data.BizTransactionClassId).subscribe(menudetails => {
      console.log("Roles and Rights:",menudetails)
      this.store.dispatch(new RolesAndRights(menudetails));
    })
    if(data.MenuType==2){
    let reportid={Id:data.Id,View:data.DisplayName}
    this.store.dispatch(new ButtonVisibility("Reports"))
    this.store.dispatch(new Reporttypebtn("NO_Addnewbtn"))
    let id = "Reportid:"+reportid.Id;
    let name = reportid.View;
    this.store.dispatch(new Title(name))
    const queryParams: any = {};
    queryParams.Reportid = JSON.stringify(id);
    const navigationExtras: NavigationExtras = {
      queryParams
    };
    this.router.navigate([`commonreport/${id}`]);
    // this.store.dispatch(new OpenMenuBar)
    this.store.dispatch(new Reportid(reportid.Id))
    }
    else{
      let FormData={Path:data.WebFormSecondURL , Id:data.ListMenuId , View: data.DisplayName , GCM:data.GcmTypeId}
      if (FormData.Id == -1) {
        if(data.WebFormName.toLowerCase() == "gcm" && FormData.GCM == -1){
          let path = FormData.Path
          let view = FormData.View
          let gcmid = FormData.GCM
          console.log("GCM:",gcmid)
          this.store.dispatch(new GCMTypeId(gcmid));
          this.router.navigate([`${path}/${gcmid}`]);
          this.store.dispatch(new Title(view))
          this.store.dispatch(new ButtonVisibility("Forms"))
        }
        else if(FormData.GCM == -1){
          let path = FormData.Path
          let view = "-"+FormData.View
          this.router.navigate([path]);
          let fullpath ='';
          this.titlepath$.subscribe( res =>{
            fullpath = res + view
          })
          this.store.dispatch(new Title(fullpath))
          this.store.dispatch(new ButtonVisibility("Forms"))
        }
        else{
          let path = FormData.Path
          let view = FormData.View
          let gcmid = FormData.GCM
          console.log("GCM:",gcmid)
          this.store.dispatch(new GCMTypeId(gcmid));
          this.router.navigate([`${path}/${gcmid}`]);
          this.store.dispatch(new Title(view))
          this.store.dispatch(new ButtonVisibility("Forms"))
        }
      }
      else {
        let view = FormData.View
        let path = FormData.Path
        let id = "Reportid:"+ FormData.Id
        const queryParams: any = {};
        queryParams.Reportname = JSON.stringify(view);
        const navigationExtras: NavigationExtras = {
          queryParams
        };
        this.router.navigate([`commonreport/${id}`]);
        // this.router.navigate(["/commonreport"], navigationExtras);
        // this.store.dispatch(new Title(view))
        this.store.dispatch(new Path(path))
        this.store.dispatch(new Reporttypebtn("Addnewbtn"))
        this.store.dispatch(new ButtonVisibility("Report&forms"))
        this.store.dispatch(new Reportid(FormData.Id))


        // Code for Full Menu Path
        // let path = FormData.Path
          let fullview = "-"+FormData.View
          this.router.navigate([path]);
          let fullpath ='';
          this.titlepath$.subscribe( res =>{
            fullpath = res + fullview
          })
          this.store.dispatch(new Title(fullpath))
      }
      // this.store.dispatch(new OpenMenuBar)
    }
  }
  public MenuFolder(data:IMenulist){
    this.menulistservice.MenuList=data;
    this.MenuPath=this.MenuPath+" > "+data.DisplayName
    this.store.dispatch(new Title(this.MenuPath))
    let id=data.Id
    if(this.MenuParamData == undefined){
      this.MenuParamData = data.Id.toString()
    } else {
      this.MenuParamData = this.MenuParamData + ' > ' + data.Id
    }
    this.router.navigate(["GoodBooks/Menu",this.MenuPath],{ queryParams: { data: this.MenuParamData }});
  }

}
