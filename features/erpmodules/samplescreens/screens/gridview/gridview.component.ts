import { Component, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Igridlist } from 'features/erpmodules/inventory/models/Ipackset';


@Component({
  selector: 'app-gridview',
  templateUrl: './gridview.component.html',
  styleUrls: ['./gridview.component.scss']
})
export class GridViewComponent implements OnInit {
  @Input() rowData: Igridlist;
  @Input() columnDefs;
  @Output() Griddata;
  private gridApi;
  private gridColumnApi;

  get data(){
    let allRowData = [];
    this.gridApi.forEachNode(node => allRowData.push(node.data));
    this.Griddata = allRowData;
    return this.Griddata
  }

  constructor() { }

  ngOnInit(): void {

  }
  public onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.rowData = this.rowData;
  }

  AddRow() {
    let newRow = {};
    this.gridApi.updateRowData({ add: [newRow] });
  }
  deleteRow() {
    let selectedNodes = this.gridApi.getSelectedNodes();
    let selectedData = selectedNodes.map(node => node.data);
    this.gridApi.updateRowData({ remove: selectedData });
  }

 

}
