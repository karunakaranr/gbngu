import { permissiondetail } from '../../../samplescreens/models/permission/ipermission.model';
import { permissionservice } from './../../services/permission/permission.service'
import { GBDataFormGroupStoreWN, GBDataFormGroupWN } from '@goodbooks/uicore';
import { HttpClient } from '@angular/common/http';
import { Select, Store } from '@ngxs/store';
import { GBDataStateActionFactoryWN } from '@goodbooks/gbdata';
import { PermissionStateActionFactory } from '../../../samplescreens/stores/permission/permission.actionfactory';
import { Observable } from 'rxjs';
import { PermissionState } from '../../stores/permission/permission.state';

export class PermissionFormgroup extends GBDataFormGroupWN<permissiondetail> {
  constructor(http: HttpClient, dataService: permissionservice) {
    super(http, 'SC005', {}, dataService)
  }

  public clear() {
    super.clear();
  }

  public delete() {
    super.delete();
  }

  public save() {
    super.save();
  }

  public moveFirst() {
    super.moveFirst();
  }

  public movePrev() {
    super.movePrev();
  }

  public moveNext() {
    super.moveNext();
  }

  public moveLast() {
    super.moveLast();
  }
}

export class PermissionFormgroupStore extends GBDataFormGroupStoreWN<permissiondetail> {
  constructor(http: HttpClient, store: Store) {
    super(http, 'SC005', {}, store, new PermissionStateActionFactory())
  }

  public clear() {
    super.clear();
  }

  public delete() {
    super.delete();
  }

  public save() {
    super.save();
  }

  public moveFirst() {
    super.moveFirst();
  }

  public movePrev() {
    super.movePrev();
  }

  public moveNext() {
    super.moveNext();
  }

  public moveLast() {
    super.moveLast();
  }
}

