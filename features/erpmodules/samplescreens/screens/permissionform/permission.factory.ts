import { GBBaseDataService, GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { GBHttpService,GbToasterService } from '@goodbooks/gbcommon';
import { permissiondetail } from '../../../samplescreens/models/permission/ipermission.model';
import { permissionservice } from './../../services/permission/permission.service'
import {permissiondbservice} from './../../dbservices/permission/permissiondb.service'
import { Store } from '@ngxs/store';
import { FormBuilder } from '@angular/forms';
import { GBDataPageService, GBDataPageServiceWN } from '@goodbooks/uicore';
import { ActivatedRoute, Router } from '@angular/router';

export function getThisDBDataService(http: GBHttpService): GBBaseDBDataService<permissiondetail> {
  return new permissiondbservice(http);
}
export function getThisDataServiceWN(dbDataService: GBBaseDBDataService<permissiondetail>): GBBaseDataServiceWN<permissiondetail> {
  return new permissionservice(dbDataService);
}
export function getThisDataService(dbDataService: GBBaseDBDataService<permissiondetail>): GBBaseDataService<permissiondetail> {
  return new permissionservice(dbDataService);
}
export function getThisGBPageService(store: Store, dataService: GBBaseDataService<permissiondetail>, dbDataService: GBBaseDBDataService<permissiondetail>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster:GbToasterService): GBDataPageService<permissiondetail> {
  return new GBDataPageService<permissiondetail>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
export function getThisGBPageServiceWN(store: Store, dataService: GBBaseDataServiceWN<permissiondetail>, dbDataService: GBBaseDBDataService<permissiondetail>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster:GbToasterService): GBDataPageServiceWN<permissiondetail> {
  return new GBDataPageServiceWN<permissiondetail>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
