import { Component, OnInit } from '@angular/core';
import { modulestate } from 'libs/gbui/src/lib/store/modulelist.state';
import { Select, Store } from '@ngxs/store';
import { ButtonVisibility, OpenMenuBar, Path, Title } from 'features/layout/store/layout.actions';
import { LayoutState } from 'features/layout/store/layout.state';
import { CloseMenuBar } from 'features/layout/store/layout.actions';
import { MenuListservice } from 'features/layout/services/MenuList/MenuList.service';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
import { IMenulist } from '../../models/Imenu.model';
import { Reportid } from 'features/commonreport/datastore/commonreport.action';
const frequent:IMenulist = require('./FrequentlyUsed.json')
@Component({
  selector: 'app-MainMenu',
  templateUrl: './MainMenu.component.html',
  styleUrls: ['./MainMenu.component.scss']
})
export class MainMenuComponent implements OnInit {
  @Select(modulestate.getmoduleid) selectedId: any;
  @Select(LayoutState.CurrentTheme) currenttheme: any;
  FrequentList: IMenulist=frequent;
  theme: any='Default'
  constructor(public store: Store,private route: ActivatedRoute,public router: Router) {
  }

  ngOnInit(): void {
    this.store.dispatch(new CloseMenuBar)
    this.currenttheme.subscribe( (theme:String) => {
        if (theme =='Blue') {
          this.theme=theme;
        } else {
          this.theme='Default'
        }
      })
    // this.store.dispatch(new Title(this.FrequentList[0].ModuleName))
    this.store.dispatch(new ButtonVisibility("MainMenu"))
  }
  public MenuData(data:IMenulist) {
    if(data.MenuType==2){
    let reportid={Id:data.Id,View:data.DisplayName}
    this.store.dispatch(new ButtonVisibility("Reports"))
    let id = reportid.Id;
    let name = reportid.View;
    this.store.dispatch(new Title(name))
    const queryParams: any = {};
    queryParams.Reportid = JSON.stringify(id);
    const navigationExtras: NavigationExtras = {
      queryParams
    };
    this.router.navigate([`commonreport/${id},`],{skipLocationChange: true, replaceUrl: false});
    this.store.dispatch(new OpenMenuBar)
    this.store.dispatch(new Reportid(id))
    }
    else{
      let FormData={Path:data.WebFormSecondURL , Id:data.ListMenuId , View: data.DisplayName}
      if (FormData.Id == -1) {
        let path = FormData.Path
        let view = FormData.View
        this.router.navigate([path]);
        this.store.dispatch(new Title(view))
        this.store.dispatch(new ButtonVisibility("Forms"))
      }
      else {
        let view = FormData.View
        let path = FormData.Path
        const queryParams: any = {};
        queryParams.Reportname = JSON.stringify(view);
        const navigationExtras: NavigationExtras = {
          queryParams
        };
        this.router.navigate(["/commonreport"], navigationExtras);
        this.store.dispatch(new Title(view))
        this.store.dispatch(new Path(path))
        this.store.dispatch(new ButtonVisibility("Report&forms"))
        this.store.dispatch(new Reportid(FormData.Id))
      }
    }
  }
}