import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { GridProperties, GridSetting } from './../../models/permission/ipermission.model';
import { PermissionlistService } from './../../services/permission/permissionlist.service';
import { permissionservice } from './../../services/permission/permission.service';
import { Store } from '@ngxs/store';

@Component({
  selector: 'app-permissionlist',
  templateUrl: './permissionlist.component.html',
  styleUrls: ['./permissionlist.component.scss'],
})
export class permissionlistComponent implements OnInit {
  title = 'Permissionlist'
  public rowData = [];
  public columnData = [];
  public defaultColDef = [];
  constructor(public service: permissionservice, private http: HttpClient ,public store: Store) { }

  ngOnInit(): void {
    this.Modellist();
  }

  public Modellist() { 
    this.service.getAll().subscribe((res) => {
      this.rowData = res;
      this.getgridsetting();
    });
  }
  public getgridsetting() {
    this.gridSetting().subscribe((res) => {
      this.columnData = res;
      this.getgridproperties();
    });
  }
  public getgridproperties() {
    this.gridproperties().subscribe((res) => {
      this.defaultColDef = res;
    });
  }

  private grid_setting = "assets/data/permissionlistsetting.json";
  private grid_properties = "assets/data/grid-properties.json";
  gridSetting(): Observable<GridSetting[]> {
    return this.http.get<GridSetting[]>(this.grid_setting);
    // return this.http.get<GridSetting[]>(this.grid_setting);
  }
  gridproperties(): Observable<GridProperties[]> {
    return this.http.get<GridProperties[]>(this.grid_properties);
    // return this.http.get<GridProperties[]>(this.grid_properties);
  }

}
