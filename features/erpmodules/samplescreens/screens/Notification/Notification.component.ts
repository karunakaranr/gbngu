import { Component, OnInit } from '@angular/core';
import { Notification } from '../../services/Notification/Notification.service';
@Component({
  selector: 'app-Notification',
  templateUrl: './Notification.component.html',
  styleUrls: ['./Notification.component.scss']
})
export class NotificationComponent implements OnInit {
  NotificationData:any;
  constructor(public service: Notification) {
  }

  ngOnInit(): void {
    this.service.NotificationView().subscribe(data => {
        this.NotificationData=data
        console.log("Notification value:",this.NotificationData);
    })
  }
}