import { Component, OnInit } from '@angular/core';
import { Moduleservice } from './../../services/modulelist/module.service';
import { modulestate } from 'libs/gbui/src/lib/store/modulelist.state';
import { ModuleID } from './../../stores/modulelist/modulelist.action';//DTO not come name shoud change , file moduleaction
import { Select, Store } from '@ngxs/store';
import { ButtonVisibility, OpenMenuBar, Title, FormUnEditable, OpenMainBar, CloseMainBar, MenuList } from 'features/layout/store/layout.actions';
import { LayoutState } from 'features/layout/store/layout.state';
import { CloseMenuBar } from 'features/layout/store/layout.actions';
import { MenuListservice } from 'features/layout/services/MenuList/MenuList.service';
import { NavigationExtras, Router } from '@angular/router';
@Component({
  selector: 'app-ModuleList',
  templateUrl: './ModuleList.component.html',
  styleUrls: ['./ModuleList.component.scss']
})
export class GbmodulelistMainComponent implements OnInit {
  @Select(modulestate.getmoduleid) selectedId: any;
  @Select(LayoutState.CurrentTheme) currenttheme: any;
  modulelist;
  count: number;
  theme: any='Default'
  constructor(public moduleservice: Moduleservice, public store: Store, private menulistservice: MenuListservice, public router: Router) {
  }

  ngOnInit(): void {
    this.store.dispatch(new OpenMainBar);
    this.store.dispatch(new OpenMenuBar);
    this.store.dispatch(new FormUnEditable);
    // this.store.dispatch(new CloseMenuBar)
    this.viewmodulelist();
    this.store.dispatch(new ButtonVisibility("Empty"))
  
    this.currenttheme.subscribe( (theme:String) => {
      if (theme =='Blue') {
        this.theme=theme;
      } else {
        this.theme='Default'
      }
    })
  }
  private viewmodulelist() {

    this.moduleservice.modulelistservice().subscribe((data) => {

      this.modulelist = data;
      console.log("Module List", this.modulelist[0])
      this.count = this.modulelist.length;
    });

  }
  public SelectedModuleId(selectedModuleId: any) {
    this.store.dispatch(new ModuleID(selectedModuleId.ModuleId));
    this.store.dispatch(new Title(selectedModuleId.ModuleName))
    // this.store.dispatch(new MenuList(this.modulelist))
    this.store.dispatch(new CloseMainBar);
    this.router.navigate(['GoodBooks/MainMenu']);
  }
}
