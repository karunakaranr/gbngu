import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MarginsummaryComponent } from '../marginsummary/marginsummary.component';

describe('MarginsummaryComponent', () => {
  let component: MarginsummaryComponent;
  let fixture: ComponentFixture<MarginsummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarginsummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarginsummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
