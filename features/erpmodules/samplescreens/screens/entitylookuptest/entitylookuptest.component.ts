import { Component, OnInit, ViewChild } from '@angular/core';
import { IEntityConfig } from '../../../../entitylookup/model/Ientitylookup.modal';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';



@Component({
  selector: 'app-entitylookuptest',
  templateUrl: './entitylookuptest.component.html',
  styleUrls: ['./entitylookuptest.component.scss']
})
export class EntitylookuptestComponent implements OnInit {
  public packentity;
  entitytest: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    this.packentity = '-1899997696';
    this.entitytest = this.fb.group({
      lookupdata:new FormControl('',[Validators.required])
    });
  }
  check(){
    console.log(this.entitytest.value);
  }
}




  // entitytest1: FormGroup;
  // entitytest2: FormGroup;
  // entitytest3: FormGroup;

  // datavalue;
  // entityconfig: IEntityConfig = {
  //   usePicklistService: true,
  //   placeholder:"PackList",
  //   picklistid: "-1899999792",
  //   bindfield: "Code",
  //   bindlabel: "Code",
  //   expandable: true,
  //   multiselect:true,
  //   highlightterm: true,
  //   AllowAdvanceFinder: true,
  //   AllowCreateNew: false,
  //   AllowRecent: true,
  //   AllowSwitchViews: false,
  //   fieldwidth: '570px',
  //   displaycolumns: [
  //     { label: "Code", field: "Code" },
  //     { label: "Factor", field: "ConversionFactor" }],
  //   dialogconfig: {
  //     disableClose: true,
  //     autoFocus: true,
  //     hasBackdrop: false,
  //     backdropClass: 'backdropBackground',
  //     width: '100%',
  //     height: '100%',
  //     position: {
  //       top: '20vh',
  //       left: '70vw'
  //     },
  //   },
  //   griddata: {
  //     columnDefs: [
  //       { field: 'Code', filter: 'agTextColumnFilter', sortable: true, checkboxSelection: true },
  //       { field: 'Name', filter: 'false', sortable: false, editable: true },
  //       { field: 'ConversionFactor', filter: 'agNumberColumnFilter', sortable: true }],
  //   },
  // }
  // entityconfig3: IEntityConfig = {
  //   usePicklistService: true,
  //   picklistid: "-1899999205",
  //   bindfield: "Name",
  //   bindlabel: "Name",
  //   placeholder:"UOMList",
  //   multiselect:false,
  //   expandable: true,
  //   highlightterm: true,
  //   AllowAdvanceFinder: true,
  //   AllowCreateNew: false,
  //   AllowRecent: true,
  //   AllowSwitchViews: false,
  //   displaycolumns: [
  //     { label: "Code", field: "Code" },
  //     { label: "Name", field: "Name" }],
  //   dialogconfig: {
  //     disableClose: true,
  //     autoFocus: true,
  //     hasBackdrop: false,
  //     backdropClass: 'backdropBackground',
  //     width: '100%',
  //     height: '100%',
  //     position: {
  //       top: '20vh',
  //       left: '70vw'
  //     },
  //   },
  //   griddata: {
  //     columnDefs: [
  //       { field: 'Code', filter: 'agTextColumnFilter', sortable: true, checkboxSelection: true },
  //       { field: 'Name', filter: 'false', sortable: false, editable: false },],
  //   },
  // }
  // entityconfig5: IEntityConfig = {
  //   usePicklistService: true,
  //   picklistid: "-1899999849",
  //   bindfield: "Name",
  //   bindlabel: "Name",
  //   placeholder:"ItemList",
  //   multiselect:false,
  //   expandable: true,
  //   highlightterm: true,
  //   AllowAdvanceFinder: true,
  //   AllowCreateNew: false,
  //   AllowRecent: true,
  //   AllowSwitchViews: false,
  //   displaycolumns: [
  //     { label: "Code", field: "Code" },
  //     { label: "Name", field: "Name" }],
  //   dialogconfig: {
  //     disableClose: true,
  //     autoFocus: true,
  //     hasBackdrop: false,
  //     backdropClass: 'backdropBackground',
  //     width: '100%',
  //     height: '100%',
  //     position: {
  //       top: '20vh',
  //       left: '70vw'
  //     },
  //   },
  //   griddata: {
  //     columnDefs: [
  //       { field: 'Code', filter: 'agTextColumnFilter', sortable: true, checkboxSelection: false },
  //       { field: 'Name', filter: 'false', sortable: false,editable: false },],
  //   },
  // }
  // entityconfig4: IEntityConfig = {
  //   usePicklistService: true,
  //   picklistid: "-1899997651",
  //   bindfield: "Name",
  //   bindlabel: "Name",
  //   placeholder:"ItemMasterList",
  //   multiselect:true,
  //   expandable: true,
  //   highlightterm: true,
  //   AllowAdvanceFinder: true,
  //   AllowCreateNew: false,
  //   AllowRecent: true,
  //   AllowSwitchViews: false,
  //   displaycolumns: [
  //     { label: "Code", field: "Code" },
  //     { label: "Name", field: "Name" }],
  //   dialogconfig: {
  //     disableClose: true,
  //     autoFocus: true,
  //     hasBackdrop: false,
  //     backdropClass: 'backdropBackground',
  //     width: '100%',
  //     height: '100%',
  //     position: {
  //       top: '20vh',
  //       left: '70vw'
  //     },
  //   },
  //   griddata: {
  //     columnDefs: [
  //       { field: 'Code', filter: 'agTextColumnFilter', sortable: true, checkboxSelection: true },
  //       { field: 'Name', filter: 'false', sortable: false, editable: true },],
  //   },
  // }
  // entityconfig2: IEntityConfig = {
  //   usePicklistService: false,
  //   bindfield: "Name",
  //   bindlabel: "Name",
  //   placeholder:"PackList",
  //   expandable: true,
  //   highlightterm: true,
  //   AllowAdvanceFinder: false,
  //   AllowCreateNew: true,
  //   AllowRecent: false,
  //   AllowSwitchViews: true,
  //   displaycolumns: [
  //     { label: "Code", field: "Code" },
  //     { label: "Factor", field: "ConversionFactor" }],
  //   API: {
  //     "PicklistCode": "PACKCODPL",
  //     "PicklistName": "Pack Picklist",
  //     "PicklistTitle": "Code",
  //     "PicklistIsMultiselect": 0,
  //     "PicklistUri": "http://169.56.148.10:82/gb4/fws/gb4/mms/Pack.svc/SelectList",
  //     "PicklistDisplayFieldTextBox": "Code,Name",
  //     "PicklistWidth": 304,
  //     "PicklistHeight": 178,
  //     "PicklistNumberOfSelection": 4,
  //     "PicklistMinimunNumber": 1,
  //     "PicklistSearchableFields": "Name",
  //     "PicklistSearchFields": "Code,Name",
  //     "PicklistSortOrder": 0,
  //     "PicklistStatus": 1,
  //     "PicklistVersion": 12,
  //     "PicklistSourceType": 2,
  //     "PicklistScreenOperationMode": 1,
  //     "PicklistRemarks": null,
  //     "PicklistDisplayFieldName": "Code,Name",
  //     "PicklistSelectionFieldName": "Id",
  //     "PicklistOutputDtos": [
  //       {
  //         "PicklistOutputId": -1899998553,
  //         "PicklistId": -1899999792,
  //         "PicklistCode": "PACKCODPL",
  //         "PicklistName": "Pack Picklist",
  //         "PicklistOutputSlNo": 1,
  //         "PicklistOutputFieldName": "PackPicklistCode",
  //         "PicklistOutputIsVisible": 0,
  //         "PicklistOutputHeaderText": "Pack Code",
  //         "PicklistOutputFieldSize": 200,
  //         "PicklistOutputIsFreezingReq": 1,
  //         "PicklistOutputIsHeaderWrappingReq": 1,
  //         "PicklistOutputIsDataWrappingReq": 1
  //       },
  //       {
  //         "PicklistOutputId": -1899998552,
  //         "PicklistId": -1899999792,
  //         "PicklistCode": "PACKCODPL",
  //         "PicklistName": "Pack Picklist",
  //         "PicklistOutputSlNo": 2,
  //         "PicklistOutputFieldName": "PackPicklistName",
  //         "PicklistOutputIsVisible": 0,
  //         "PicklistOutputHeaderText": "Pack Name",
  //         "PicklistOutputFieldSize": 200,
  //         "PicklistOutputIsFreezingReq": 1,
  //         "PicklistOutputIsHeaderWrappingReq": 1,
  //         "PicklistOutputIsDataWrappingReq": 1
  //       }
  //     ]
  //   }
  // };

  // entityconfig= {
  //     placeholder:"PackList",
  //     picklistid: "-1899999792",
  //     bindfield: "Code",
  //     bindlabel: "Code",
  //     displaycolumns: [
  //       { label: "Code", field: "Code" },
  //       { label: "Factor", field: "ConversionFactor" }],
  //     griddata: {
  //       columnDefs: [
  //         { field: 'Code', filter: 'agTextColumnFilter', sortable: true, checkboxSelection: true },
  //         { field: 'Name', filter: 'false', sortable: false, editable: true },
  //         { field: 'ConversionFactor', filter: 'agNumberColumnFilter', sortable: true }],
  //     },
  //   }


  // public GetChildData(data) {
  //   let datavalue=this.entitytest.get('lookupdata').value;
  //   this.entitytest.get('lookupdata').patchValue([...datavalue,data]);
  // }

