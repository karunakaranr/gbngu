import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EntitylookuptestComponent } from './entitylookuptest.component';

describe('EntitylookuptestComponent', () => {
  let component: EntitylookuptestComponent;
  let fixture: ComponentFixture<EntitylookuptestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EntitylookuptestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EntitylookuptestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
