import { Component, OnInit, ViewChild, Input} from '@angular/core';
import { StatementOfAccount } from './../../../services/chart/StatementOfAccount/StatementOfAccount.service';
import { FormBuilder, FormGroup } from '@angular/forms';

import {
  ChartComponent,
  ApexAxisChartSeries,
  ApexChart,
  ApexXAxis,
  ApexDataLabels,
  ApexTitleSubtitle,
  ApexStroke,
  ApexGrid,
  ApexAnnotations,
} from 'ng-apexcharts';

export type ChartOptions = {
  series: ApexAxisChartSeries;
  annotations: ApexAnnotations;
  chart: ApexChart;
  xaxis: ApexXAxis;
  dataLabels: ApexDataLabels;
  grid: ApexGrid;
  labels: string[];
  stroke: ApexStroke;
  title: ApexTitleSubtitle;
};
const jsonvalue = require('./../StatementOfAccount/StatementOfAccount.json')
@Component({
  selector: 'app-ChartSetting',
  templateUrl: './ChartSetting.component.html',
  styleUrls: ['./ChartSetting.component.scss']
})

export class ChartSettingComponent implements OnInit {
  @Input() rowData;
  @Input() chartdata;
  @ViewChild('chart') chart: ChartComponent;
  public chartOptions: Partial<ChartOptions>;
  public chartOptions1: Partial<ChartOptions>;
  public chartOptions2: Partial<ChartOptions>;
  title = 'Apex Chart '
  TypeList = ['line','bar','pie']
  lightBoxForm: FormGroup;
  value;
  chartseries=[];
  keyValue=[];
  constructor(public service: StatementOfAccount, private fb: FormBuilder) { }
  ngOnInit(): void {
    this.lightBoxForm = this.fb.group({
      label: '',
      series: '',
      charttype: '',
      charttype1:'line'
    });
    this.insurancelist();
  }
  public insurancelist() {
    console.log("Report Detail:", this.rowData);
    console.log("Chart Detail:", JSON.stringify(this.chartdata))
    let date: Date
    this.service.StatementOfAccountview().subscribe(menudetails => {
      let servicejsondetail = menudetails
      this.service.Rowservice(servicejsondetail, '').subscribe(res => {
        this.rowData = res.ReportDetail;
        this.keyValue=Object.keys(this.rowData[0])
        for (let item of this.chartdata[0].series) {
          let a1 = item.name
          item.data = []
          for (let data of this.rowData) {
            item.data.push(data[a1])
          }
        }
        let b1 = this.chartdata[0].label
        this.chartdata[0].labels = []
        for (let data of this.rowData) {
          this.chartdata[0].labels.push(data[b1])
        }

        this.chartdata[0].chart.type = "line"
        this.chartlist(this.chartdata)
      });

    })
  }
  public chartlist(chartdata) {

    this.chartOptions = {
      series: chartdata[0].series,

      chart: chartdata[0].chart,

      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: 'straight',
        width: 3,
      },
      grid: {
        padding: {
          right: 30,
          left: 20,
        },
      },
      title: chartdata[0].title,

      labels: chartdata[0].labels,
    };
    console.log("Chart:" + JSON.stringify(this.chartOptions));
  }
  public chartlist1(chartdata) {

    this.chartOptions = {
      series: chartdata[0].series[0].data,

      chart: chartdata[0].chart,
      dataLabels: {
        enabled: false,
      },
      stroke: {
        curve: 'straight',
        width: 3,
      },
      grid: {
        padding: {
          right: 30,
          left: 20,
        },
      },
      title: chartdata[0].title,

      labels: chartdata[0].labels,
    };
    console.log("Chart:" + JSON.stringify(this.chartOptions));
  }
  changeChart(value) {
    if (value == "line") {
      this.chartdata[0].chart.type = "line"
      this.chartlist(this.chartdata)
    }
    if (value == "pie") {
      this.chartdata[0].chart.type = "pie"
      this.chartlist1(this.chartdata)
    }
    if (value == "bar") {
      this.chartdata[0].chart.type = "bar"
      this.chartlist(this.chartdata)
    }
  }

  public btnfilter() {
    document.getElementById("myModal1").style.display = "block";
  }

  public close() {
    document.getElementById("myModal1").style.display = "none";
  }

  public applybutton() {
    console.log("Label:" + this.lightBoxForm.get('label').value)
    console.log("Series1:" + this.lightBoxForm.get('series').value)
    console.log("Type:" + this.lightBoxForm.get("charttype").value)

    let a1 = this.lightBoxForm.get('series').value
    this.chartdata[0].series = []
    console.log("A1"+a1)
    for( let item of a1){
     let temp=[]
     for (let data of this.rowData){
      temp.push(data[item])
     }
     this.chartdata[0].series.push({name:item,data:temp})
    }
    let b1 = this.lightBoxForm.get("label").value
    this.chartdata[0].labels = []
    for (let data of this.rowData) {
      this.chartdata[0].labels.push(data[b1])
    }
    this.chartdata[0].chart.type = this.lightBoxForm.get("charttype").value
    if (this.lightBoxForm.get("charttype").value == 'pie') {
      this.chartlist1(this.chartdata)
    } else {
      this.chartlist(this.chartdata)
    }
    this.charttype.setValue(this.lightBoxForm.get("charttype").value);
    document.getElementById("myModal1").style.display = "none";
  }

  get charttype() {
    return this.lightBoxForm.get('charttype1')
  };

  public clearbutton(){
    this.lightBoxForm.reset();
    this.charttype.setValue(this.chartdata[0].chart.type)
  } 
}