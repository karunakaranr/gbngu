import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Chartsetting } from '../../../models/chart.model';
import { Chartsettingservice } from '../../../services/chart/chartsetting.service';
import { ChartdataService } from '../../../services/chart/chartdata.service';

@Component({
  selector: 'goodbooks-chartview',
  templateUrl: './chartview.component.html',
  styleUrls: ['./chartview.component.scss']
})
export class ChartviewComponent implements OnInit {

  title = "Charts";
  public viewChart = [];
  public chartdata = [];
  public bubblechart = [];
  public chartsetting: Chartsetting[];
  chart: string;
  type;
  constructor(private http: HttpClient, private _chartsettingservice: Chartsettingservice, private _chartdataService: ChartdataService) { }

  ngOnInit() {
    this.showchart();
  }

  onNotified(chartType: string) {
    this.chart = chartType
    this.type = this.chart
  }
  public showchart() {
    this._chartsettingservice.chartsetting()
      .subscribe((data: Chartsetting[]) => {
        this.chartsetting = data;
        console.log(this.chartsetting)
        this.getChart();
      });
  }
  public getChart() {
    this._chartdataService.chartdata()
      .subscribe(data => {
        this.chartdata = data;
        console.log(this.chartdata);
        this.getChartdata(); 
      });
      
  }
  public getChartdata() {
    this._chartdataService.getchart()
      .subscribe(data => {
        this.viewChart = data;
        console.log(this.viewChart);
        this.getbubblechart();
      })
  }
  public getbubblechart() {
    this._chartdataService.getbubble()
      .subscribe(data => {
        this.bubblechart = data;
        console.log(this.bubblechart)
      })
  }

}


