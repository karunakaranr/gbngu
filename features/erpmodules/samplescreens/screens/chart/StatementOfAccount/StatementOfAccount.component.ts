import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewChild, Renderer2, ElementRef} from '@angular/core';
import { StatementOfAccount } from './../../../services/chart/StatementOfAccount/StatementOfAccount.service';
import { DatePipe } from '@angular/common';
import { NavigationExtras, Router, RouterModule } from '@angular/router';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { GbApexChartDTO} from 'features/erpmodules/samplescreens/stores/apexchart/apexchart.action';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Observable } from 'rxjs';
const jsonvalue = require('./StatementOfAccount.json')

@Component({
  selector: 'app-StatementOfAccount',
  templateUrl: './StatementOfAccount.component.html',
  styleUrls: ['./StatementOfAccount.component.scss']
})

export class StatementOfAccountComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  title = 'Statement Of Account '
  rowData;
  chartdata=jsonvalue;
  constructor(public store: Store ,public service: StatementOfAccount, private router: Router, private fb: FormBuilder,private renderer: Renderer2) {
   }
  ngOnInit(): void {
    this.insurancelist();
  }
  public insurancelist() {
    this.rowdatacommon$.subscribe(id => {
      this.rowData=id
      console.log("ROWDATA:"+this.rowData)
      this.store.dispatch(new GbApexChartDTO(this.rowData));
    });
  }
}