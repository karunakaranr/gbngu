import { Component, OnInit } from '@angular/core';
import { WFApproval } from '../../services/WorkFlowApproval/WFApproval.service';
import { approval } from './../../models/iapproval.model';
import { Select, Store } from '@ngxs/store';
import { FormEditable } from 'features/layout/store/layout.actions';
import { ILoginDTO } from 'features/common/shared/stores/auth.model';
import { AuthState } from 'features/common/shared/stores/auth.state';
import { Observable } from 'rxjs';
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
import { MatDialog } from '@angular/material/dialog';
@Component({
  selector: 'app-WFApproval',
  templateUrl: './WFApproval.component.html',
  styleUrls: ['./WFApproval.component.scss']
})
export class WFApprovalComponent implements OnInit {
  @Select(AuthState.AddDTO) loginDTOs$: Observable<ILoginDTO>;
  public rowData: any;
  public Total: number;
  public isRequestByMeTabActive: boolean = false;
  public isRequestOnMeTabActive: boolean = false;
  public approvaljson: approval[] = [];
  public approvallistdata: Array<object> = [];
  public index: any;
  public searchText: any;
  public doc: any;
  showLightbox: boolean = false;
  showLightbox2: boolean = false;
  public selectAllChecked: boolean = false;
  isDropdownOpen = false;
  sendMailService: any;
  MainId: string;
  picklistUser: string;
  public isHovered: boolean = false;
  public totalValueFromSelectedItems: number = 0;
  public sendBoxImageDefault: string = 'assets/icons/Mail.png';
  public sendBoxImageHover: string = 'assets/icons/mingcute_mail-fill.png';
  public sendBoxImageSource: string = this.sendBoxImageDefault;
  DocumentNumber: any;


  constructor(public service: WFApproval, public store: Store, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.loginDTOs$.subscribe(dto => {
      console.log("LoginDTO:", dto)
      this.MainId = dto.UserPrimaryMailId;
      console.log("LoginmailID:", this.MainId)
    })
    this.isRequestOnMeTabActive = true;
    this.RequestOnMeTabClick();
    this.RequestByMeTabClick();

  }
  // calculateTotal() {
  //   this.Total = this.rowData.reduce((total, data) => {
  //     return data.selected ? total + data.TotalValue : total;
  //   }, 0);
  // }

  RequestByMeTabClick() {
    if (this.isRequestByMeTabActive) {
      this.service.Requestbyme().subscribe((data) => {
        this.rowData = data;
        console.log('Requestbyme value:', this.rowData);
        this.calculateTotal();
      });
    }
  }

  RequestOnMeTabClick() {
    if (this.isRequestOnMeTabActive) {
      this.service.Requesttome().subscribe((data) => {
        this.rowData = data;
        console.log('Requesttome value:', this.rowData);
  
        // Debug: Log the DisplayValues
        this.rowData.forEach(item => {
          console.log('DisplayValue:', item.DisplayValue.split(',')[2] );
        });
  
        this.calculateTotal();
      });
    }
  }
   
  RequesttouserTabClick() {
    this.service.Requesttouser(this.Getselectedid, this.DocumentNumber).subscribe(
      (data) => {
        if (Array.isArray(data) || (typeof data === 'object' && data !== null)) {
          this.rowData = data;
          console.log('picklistUser value:', this.rowData);
        } else {
          const dialogRef = this.dialog.open(DialogBoxComponent, {
            width: '400px',
            data: {
              message: 'You dont have privilege to approve on behalf approvals...',
              heading: 'Warning',
            }
          });          
          this.showLightbox2 = false;
        }
      },
      (error) => {
        alert('Error: ' + error.message);
        this.showLightbox2 = false;
      }
    );
  }
  forwardDataOnClick(){
    this.service.forwardData().subscribe((data) => {
      this.rowData = data;
      console.log('forwardData value:', this.rowData);

    });
  }
  backwardDataOnClick(){
    this.service.backwardData().subscribe((data) =>{
      this.rowData = data;
      console.log('backwardData value:', this.rowData);
    })
  }

  SearchItemOnClick() {
    this.service.SearchItem(this.searchText).subscribe((data) => {
      this.rowData = data;
      console.log('SearchItem value:', this.rowData);

      if (!this.rowData || this.rowData.length === 0) {
        const dialogRef = this.dialog.open(DialogBoxComponent, {
          width: '400px',
          data: {
            message: 'No details found for the search value.',
            heading: 'Info',
          }
        });

      }
    });
  }

  approve() {
    console.log("approve");

    // Filter selected items
    const selectedItems = this.rowData.filter((item: { selected: any; }) => item.selected);

    if (selectedItems.length > 0) {
      // Convert the selected items to the desired format
      const formattedItems = selectedItems.map((item: { DocumentNumber: any; BizTransactionTypeId: any; ObjectId: any; ProcessInstanceId: any; EntityCode: any; DisplayValue: any; FromUserCode: any; InchargeMail: any; Version: any; PartyBranchId: any; }) => ({
        "DocumentNumber": item.DocumentNumber,
        "BizTransactionTypeId": item.BizTransactionTypeId,
        "ObjectId": item.ObjectId,
        "ProcessInstanceId": item.ProcessInstanceId,
        "EntityCode": item.EntityCode,
        "DisplayValue": item.DisplayValue,
        "FromUserCode": item.FromUserCode,
        "InchargeMail": item.InchargeMail,
        "Version": item.Version,
        "PartyBranchId": item.PartyBranchId,
        "Remarks": "",
      }));

      console.log('Formatted Items:', formattedItems);

      this.service.AproveItem(formattedItems).subscribe(
        (data) => {
          this.rowData = this.rowData.filter((item: { selected: any; }) => !item.selected);
          console.log('Remaining items:', this.rowData);

          const dialogRef = this.dialog.open(DialogBoxComponent, {
            width: '400px',
            data: {
              message: 'Approval successful',
              heading: 'Success',
            }
          });
        },
        (error) => {
          console.error('Error during approval:', error);
          const dialogRef = this.dialog.open(DialogBoxComponent, {
            width: '400px',
            data: {
              message: 'Error during approval.',
              heading: 'Warning',
            }
          });
        }
      );
    } else {
      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: 'Please select items before approving',
          heading: 'Info',
        }
      });

    }
  }
  forwardData(){
    console.log("forwardData");

  }

  ResendItem() {
    console.log("ResendItem");
    const selectedItems = this.rowData.filter((item: { selected: any; }) => item.selected);

    if (selectedItems.length > 0) {
      const formattedItems = selectedItems.map((item: { DocumentNumber: any; BizTransactionTypeId: any; ObjectId: any; ProcessInstanceId: any; EntityCode: any; DisplayValue: any; FromUserCode: any; InchargeMail: any; Version: any; PartyBranchId: any; }) => ({
        "DocumentNumber": item.DocumentNumber,
        "BizTransactionTypeId": item.BizTransactionTypeId,
        "ObjectId": item.ObjectId,
        "ProcessInstanceId": item.ProcessInstanceId,
        "EntityCode": item.EntityCode,
        "DisplayValue": item.DisplayValue,
        "FromUserCode": item.FromUserCode,
        "InchargeMail": item.InchargeMail,
        "Version": item.Version,
        "PartyBranchId": item.PartyBranchId,
        "Remarks": "",
      }));

      console.log('Formatted Items:', formattedItems);

      this.service.ResendItem(formattedItems).subscribe(
        (data) => {
          this.rowData = this.rowData.filter((item: { selected: any; }) => !item.selected);
          console.log('Remaining items:', this.rowData);
          const dialogRef = this.dialog.open(DialogBoxComponent, {
            width: '400px',
            data: {
              message: 'Resend successful',
              heading: 'Success',
            }
          });
         
        },
        (error) => {

          console.error('Error during Resend:', error);
          const dialogRef = this.dialog.open(DialogBoxComponent, {
            width: '400px',
            data: {
              message: 'Error during Resend.',
              heading: 'Warning',
            }
          });
        }
      );
    } else {
      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: 'Please select items before Resending.',
          heading: 'Info',
        }
      });

    }
  }

  RejectedItem() {
    console.log("RejectedItem");

    const selectedItems = this.rowData.filter((item: { selected: any; }) => item.selected);

    if (selectedItems.length > 0) {
      const formattedItems = selectedItems.map((item: { DocumentNumber: any; BizTransactionTypeId: any; ObjectId: any; ProcessInstanceId: any; EntityCode: any; DisplayValue: any; FromUserCode: any; InchargeMail: any; Version: any; PartyBranchId: any; }) => ({
        "DocumentNumber": item.DocumentNumber,
        "BizTransactionTypeId": item.BizTransactionTypeId,
        "ObjectId": item.ObjectId,
        "ProcessInstanceId": item.ProcessInstanceId,
        "EntityCode": item.EntityCode,
        "DisplayValue": item.DisplayValue,
        "FromUserCode": item.FromUserCode,
        "InchargeMail": item.InchargeMail,
        "Version": item.Version,
        "PartyBranchId": item.PartyBranchId,
        "Remarks": "",
      }));

      console.log('Formatted Items:', formattedItems);

      this.service.RejectedItem(formattedItems).subscribe(
        (data) => {
          this.rowData = this.rowData.filter((item: { selected: any; }) => !item.selected);
          console.log('Remaining items:', this.rowData);

          const dialogRef = this.dialog.open(DialogBoxComponent, {
            width: '400px',
            data: {
              message: 'Reject successful.',
              heading: 'Success',
            }
          });

        },
        (error) => {
          console.error('Error during Reject:', error);
          const dialogRef = this.dialog.open(DialogBoxComponent, {
            width: '400px',
            data: {
              message: 'Error during Reject.',
              heading: 'Warning',
            }
          });

        }
      );
    } else {

      const dialogRef = this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: 'Please select items before Rejecting.',
          heading: 'Info',
        }
      });

    }
  }


  lighboxsend(): void {
    const comments = (document.getElementById('Comments') as HTMLInputElement)?.value;
    const displayValue = (document.getElementById('documentval') as HTMLParagraphElement)?.innerText;

    const mailData = {
      MailTo: this.Getselectedid,
      Subject: displayValue,
      Message: comments,
      MailToSendType: 1,
      ReplyTo: this.MainId,
    };

    this.service.lighboxsend(mailData).subscribe(
      (response) => {
        console.log('Mail sent successfully', response);
      },
      (error) => {
        console.error('Error sending mail', error);
      }
    );

    this.showLightbox = false;
  }


  Getselectedid(Selectedid: any) {
    console.log("Selectedid:", Selectedid);
    this.Getselectedid = Selectedid;
}


  Getselectedidbehalf(Selectedid: any) {
    console.log("Selectedid:", Selectedid)
  }
  onRequestByMeTabClick() {
    this.isRequestByMeTabActive = true;
    this.isRequestOnMeTabActive = false;
    this.RequestByMeTabClick();
  }

  onRequestOnMeTabClick() {
    this.isRequestOnMeTabActive = true;
    this.isRequestByMeTabActive = false;
    this.RequestOnMeTabClick();
  }

  selectAllItems() {
    if (this.rowData) {
        this.rowData.forEach((item: { selected: boolean; }) => (item.selected = true));
        this.calculateTotal(); // Calculate total after selecting all items
    }
}
deselectAllItems() {
  if (this.rowData) {
      this.rowData.forEach((item: { selected: boolean; }) => (item.selected = false));
      this.calculateTotal(); // Recalculate total after deselecting all items
  }
}

calculateTotal() {
  this.Total = this.rowData.reduce((total, data) => {
      return data.selected ? total + data.TotalValue : total;
  }, 0);

  if (this.Total === 0) {
      // Reset the total value when it becomes 0
      this.Total = 0;
  }
}
  public showlightbox() {
    this.store.dispatch(new FormEditable);
    this.showLightbox = true;
  }
  public showlightbox2() {
    this.store.dispatch(new FormEditable);
    this.showLightbox2 = true;
  }

  closeLightBox() {
    this.showLightbox = false;
    this.showLightbox2 = false;
  }


  toggleDropdown() {
    this.isDropdownOpen = !this.isDropdownOpen;
  }

  public approveIconDefault: string = 'assets/icons/accept.png';
  public approveIconHover: string = 'assets/icons/acceptimg.png';

  public rejectIconDefault: string = 'assets/icons/reject.png';
  public rejectIconHover: string = 'assets/icons/rejectimg.png';

  public resendIconDefault: string = 'assets/icons/resend.png';
  public resendIconHover: string = 'assets/icons/resendimg.png';

  public previewIconDefault: string = 'assets/icons/preview.png';
  public previewIconHover: string = 'assets/icons/previewing.png';

  public approveIconSource: string = this.approveIconDefault;
  public rejectIconSource: string = this.rejectIconDefault;
  public resendIconSource: string = this.resendIconDefault;
  public previewIconSource: string = this.previewIconDefault;

  onApproveIconMouseEnter(): void {
    this.approveIconSource = this.approveIconHover;
  }

  onApproveIconMouseLeave(): void {
    this.approveIconSource = this.approveIconDefault;
  }

  onRejectIconMouseEnter(): void {
    this.rejectIconSource = this.rejectIconHover;
  }

  onRejectIconMouseLeave(): void {
    this.rejectIconSource = this.rejectIconDefault;
  }

  onResendIconMouseEnter(): void {
    this.resendIconSource = this.resendIconHover;
  }

  onResendIconMouseLeave(): void {
    this.resendIconSource = this.resendIconDefault;
  }

  onpreviewIconMouseEnter(): void {
    this.previewIconSource = this.previewIconHover;
  }

  onpreviewIconMouseleave(): void {
    this.previewIconSource = this.previewIconDefault;
  }


  onIconMouseLeave(): void {
    this.approveIconSource = this.approveIconDefault;
    this.rejectIconSource = this.rejectIconDefault;
    this.resendIconSource = this.resendIconDefault;
    this.previewIconSource = this.previewIconDefault;
  }

  clearSearch(event: Event): void {
    event.preventDefault();

    this.searchText = '';

    console.log('Clear icon clicked');

    this.RequestOnMeTabClick();
  }


  imageHovered: number | null = null;

  onItemHover(index: number): void {
    this.imageHovered = index;
  }

  onItemLeave(index: number): void {
    if (this.imageHovered === index) {
      this.imageHovered = null;
    }
  }
  onSendBoxHover(): void {
    this.sendBoxImageSource = this.sendBoxImageHover;
  }

  onSendBoxLeave(): void {
    this.sendBoxImageSource = this.sendBoxImageDefault;
  }

}
