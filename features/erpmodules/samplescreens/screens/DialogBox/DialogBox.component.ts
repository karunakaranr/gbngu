import { Component, Inject, Input } from '@angular/core';
import { MAT_DIALOG_DATA,MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-my-modal',
  templateUrl: './DialogBox.component.html',
  styleUrls: ['./DialogBox.component.scss']
})
export class DialogBoxComponent {
  typeofmessage : string;
  Errormsg : string;
  MoreDisplay:boolean = false;
  constructor(public dialogRef: MatDialogRef<DialogBoxComponent>,@Inject(MAT_DIALOG_DATA) public data: any) {
    console.log('Injected Data:', data);
    this.typeofmessage = typeof data.message
    this.Errormsg = data.errorinfo
  }

  moretoggle(){
    this.MoreDisplay = !this.MoreDisplay
  }
  closeModal(CloseValue: string): void {
    this.dialogRef.close(CloseValue);
  }
}