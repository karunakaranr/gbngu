import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IPackserv } from './IPackserv';


@Injectable({
  providedIn: 'root'
})
export class PackservService extends GBBaseDataServiceWN<IPackserv> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPackserv>) {
    super(dbDataService, 'PackId');
  }
}
