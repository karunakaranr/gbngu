export interface IPackserv {
  PackId: number;
  PackCode: string;
  PackName: string;
  PackConversionType: string;
  PackConversionFactor: number;
  PackStatus: number;
  PackVersion: number;
  sortorder: number;
  sourceType: string;
}

export interface ISelectedDate{
  lookupdata: IPackPicklist[]
}

export interface IPackPicklist{
    Id:   string;
    Code: string;
    ConversionFactor: string;
    Name: string;
}





