import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackservComponent } from './packserv.component';

describe('PackservComponent', () => {
  let component: PackservComponent;
  let fixture: ComponentFixture<PackservComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackservComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackservComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
