import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
// import { GBHttpService, GbToasterService } from '@goodbooks/gbcommon';
import { GBHttpService, GbToasterService } from 'libs/gbcommon/src';
import { AbstractDS, GBBaseDataService, GBBaseDataServiceWN, GBBaseDBDataService } from 'libs/gbdata/src';
// import { AbstractDS, GBBaseDataService, GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService, GBFormGroup } from 'libs/uicore/src';
// import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService, GBFormGroup } from '@goodbooks/uicore';
import { Store } from '@ngxs/store';

import { IPackserv, ISelectedDate } from './IPackserv';
import { PackservService } from './packserv.service';
const urls = require('./../../URLS/urls.json');

@Component({
  selector: 'app-packserv',
  templateUrl: './packserv.component.html',
  styleUrls: ['./packserv.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'PackId' },
    { provide: 'url', useValue: urls.Pack },
    { provide: 'DataService', useClass: PackservService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]

})
export class PackservComponent extends GBBaseDataPageComponentWN<IPackserv> {
  title : string = 'Packserv';
  form: GBDataFormGroupWN<IPackserv> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'SC002', {}, this.gbps.dataService);
  thisConstructor() { }
  
  public Getselectedid(SelectedPicklistData: any): void {
    console.log("SelectedPicklistData:",SelectedPicklistData)
    if (SelectedPicklistData[0].Id) {
      // console.log("SelectedPicklistData:", typeof SelectedPicklistData.lookupdata[0].Id)
      this.gbps.dataService.getData(SelectedPicklistData[0].Id).subscribe(res => {
        console.log("Pack Form Value", res)
        this.form.patchValue(res);
        
      })
    }
  }


    // Callservice(){  //F  irst 20 data calling
    //   this.gbps.dataService.getAll().subscribe(res => {
    //     let data = res
    //     data = data.map(({ Id, Code, Name }) => ({ PackId: Id, PackCode: Code, PackName: Name }));
    //     this.Picklistvalues = data;
    //     console.log("Picklistvalues",this.Picklistvalues)
    //   })
    // }

    // changeFn(data){ //Getting Selected picklist value and getting its respective data
    //   console.log("Data:",this.form.get('PackCode').value.PackId)
    // if(this.form.get('PackCode').value.PackId != undefined){
    //   this.gbps.dataService.getData(this.form.get('PackCode').value.PackId).subscribe(res => {
    //     console.log("res",res)
    //     this.form.patchValue(res);
    //   })
    //   }
    // }


    // onSearch(searchedvalue) { //search in api
    //   this.searchTerm = searchedvalue;
    //   let searchvaluetyped =searchedvalue.term
    //   console.log("this.searchOnEnter",this.searchOnEnter)
    //   if(this.searchOnEnter) {
    //   this.gbps.dataService.Searchpicklistvalue(searchvaluetyped).subscribe(res => {
    //     console.log("SEARCHVALUEREPONSE",res)
    //     let data = res
    //     data = data.map(({ Id, Code, Name }) => ({ PackId: Id, PackCode: Code, PackName: Name }));
    //     this.Picklistvalues = data
    //   })
    // }
    // }

    // onKeyDown(event: KeyboardEvent): void { //Enter buton trigger for search
    //   if (event.key === 'Enter') {
    //     this.searchOnEnter = true;
    //     this.onSearch(this.searchTerm);
    //     this.searchOnEnter = false;
    //   }
    // }
  }



export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPackserv> {
  const dbds: GBBaseDBDataService<IPackserv> = new GBBaseDBDataService<IPackserv>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPackserv>, dbDataService: GBBaseDBDataService<IPackserv>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router, toaster: GbToasterService): GBDataPageService<IPackserv> {
  return new GBDataPageService<IPackserv>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
