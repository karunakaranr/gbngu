import { Injectable } from '@angular/core';
import { Ipack } from './../Model/packstore.modal';
import { PackstoredatabaseService } from './../Dbservice/packstoreDB.service';



@Injectable({
  providedIn: 'root'
})
export class PackstoreService {
  constructor(public packdb: PackstoredatabaseService) { }

  public getlist(id) {
    return this.packdb.getlistDB(id);
  }
  public savepack(packdata: Ipack) {
    return this.packdb.savapack(packdata);
  }

  public deletepack(PackId) {
    return this.packdb.deletepack(PackId);
  }

  public updatepack(packdata) {
    return this.packdb.savapack(packdata);
  }
}

