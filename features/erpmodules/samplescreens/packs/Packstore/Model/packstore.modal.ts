export class Ipack {
    PackId: number;
    PackCode: string;
    PackName: string;
    PackConversionType: number;
    PackConversionFactor: string;
    PackCreatedOn: Date;
    PackModifiedOn: Date;
    PackModifiedByName: string;
    PackCreatedByName: string;
    PackStatus: number;
    PackVersion: number;
    
    constructor(){
      this.PackStatus=1,
      this.PackVersion=1
    }
}
