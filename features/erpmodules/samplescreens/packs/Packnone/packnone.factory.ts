// import { HttpClient } from '@angular/common/http';

import { GBBaseDataService, GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { GBHttpService,GbToasterService } from '@goodbooks/gbcommon';

import { IPacknone } from './IPacknone';
import { Store } from '@ngxs/store';
import { FormBuilder } from '@angular/forms';
import { GBDataPageService } from '@goodbooks/uicore';
import { ActivatedRoute, Router } from '@angular/router';

export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPacknone> {
  const dbds: GBBaseDBDataService<IPacknone> = new GBBaseDBDataService<IPacknone>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisDataServiceWN(dbds: GBBaseDBDataService<IPacknone>, idField: string): GBBaseDataServiceWN<IPacknone> {
  return new GBBaseDataServiceWN<IPacknone>(dbds, idField);
}
export function getThisDataService(dbds: GBBaseDBDataService<IPacknone>, idField: string): GBBaseDataService<IPacknone> {
  return new GBBaseDataService<IPacknone>(dbds, idField);
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPacknone>, dbDataService: GBBaseDBDataService<IPacknone>, fb: FormBuilder, gbhttp:GBHttpService, activeroute:ActivatedRoute, router:Router,toaster:GbToasterService): GBDataPageService<IPacknone> {
  return new GBDataPageService<IPacknone>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
