import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PacknoneComponent } from './packnone.component';

describe('PacknoneComponent', () => {
  let component: PacknoneComponent;
  let fixture: ComponentFixture<PacknoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PacknoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PacknoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
