import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PackbothComponent } from './packs/Packboth/packboth.component';
import { PackdbservComponent } from './packs/Packdbserv/packdbserv.component';

import { PacknoneComponent } from './packs/Packnone/packnone.component'; 
import { PackservComponent } from './packs/Packserv/packserv.component';
import { ProfileComponent } from './screens/profile/profile.component';

import { attachmentComponent } from './attachment/attachment.component'
import { AngularFileUploaderComponent } from './fileuploader/angular-file-uploader.component'
import { SamplescreensComponent } from './samplescreens.component';
import {permissionformComponent} from './screens/permissionform/permissionform.component'

import {permissionlistComponent} from './screens/permissionlist/permissionlist.component'
import {approvallistComponent} from './screens/approvallist/approvallist.component'
import {geolocationtestComponent} from './screens/geolocationtest/geolocationtest.component'


import { RulesFormComponent } from './packs/rulesform/rulesform.component';
import { DashboardComponent } from './../../common/shared/screens/dashboard/dashboard.component';
import { MarginsummaryComponent } from './screens/sales/marginsummary/marginsummary.component';
import { ChartviewComponent } from './screens/chart/chartview/chartview.component';
import { EntitylookuptestComponent } from './screens/entitylookuptest/entitylookuptest.component';
import { GblogComponent } from 'features/gblog/components/gblog/gblog.component';
import { ApexChartComponent } from './screens/chart/ApexChart/ApexChart.component';
import { StatementOfAccountComponent } from './screens/chart/StatementOfAccount/StatementOfAccount.component';
import { PipelineDBComponent } from './Dashboard/PipelineDB/PipelineDB.component';
import { ProductionMonitorComponent } from './Dashboard/ProductionMonitor/ProductionMonitor.component';
import { GbmodulelistMainComponent } from './screens/ModuleList/ModuleList.component';
import { MMLightBoxComponent } from './LightBox/MMLightBox/MMLightBox.component';
import { VoucherLightBoxComponent } from './LightBox/VoucherLightBox/VoucherLightBox.component';
import { MenuListMainComponent } from './screens/MenuList/MenuList.component';
import { MainMenuComponent } from './screens/MainMenu/MainMenu.component';
import { NotificationComponent } from './screens/Notification/Notification.component';
import { MapComponent } from './screens/Map/map.component';
import { AllControlFormComponent } from './screens/AllControlForm/AllControlForm.component';
import { WFApprovalComponent } from './screens/WorkFlow Approval/WFApproval.component';
const routes: Routes = [
  {
    path: 'allcontrolform',
    component: AllControlFormComponent
  },
  {
    path: 'WFApproval',
    component: WFApprovalComponent
  },
  {
    path: 'Notification',
    component: NotificationComponent
  },
  {
    path: 'voucherlightbox',
    component: VoucherLightBoxComponent
  },
  {
    path: 'mmlightbox',
    component: MMLightBoxComponent
  },
  {
    path: 'Module',
    component: GbmodulelistMainComponent
  },
  {
    path: 'MainMenu',
    component: MainMenuComponent
  },
  {
    path: 'Menu/:id',
    component: MenuListMainComponent
  },
  {
    path: '',
    component: SamplescreensComponent
  },
  {
    path: 'rulesform',
    component: RulesFormComponent
  },
  {
    path: 'attach',
    component: attachmentComponent
  },
  {
    path:'geolocationtest',
    component:geolocationtestComponent
  },
  {
    path: 'fileupload',
    component: AngularFileUploaderComponent
  },
  {
    path: 'Dashboard',
    component: DashboardComponent
  },
  {
    path: 'MapAllocation',
    component: MapComponent
  },
  {
    path: 'PackBoth',
    children: [
      {
        path: ':id',
        component: PackbothComponent
      },
      {
        path: '',
        component: PackbothComponent
      },
    ]
  },
  {
    path: 'Packdbserv',
    children: [
      {
        path: ':id',
        component: PackdbservComponent
      },
      {
        path: '',
        component: PackdbservComponent
      },
    ]
  },
  {
    path: 'PackNone',
    children: [
      {
        path: ':id',
        component: PacknoneComponent
      },
      {
        path: '',
        component: PacknoneComponent
      },
    ]
  },
  {
    path: 'PackServ',
    children: [
      {
        path: ':id',
        component: PackservComponent
      },
      {
        path: '',
        component: PackservComponent
      },
    ]
  },
  {
    path: 'chart',
    children: [
      {
        path: 'statementofaccount',
        component: StatementOfAccountComponent
      },
    ]
  },
  {
    path: 'pipelinedb',
    component: PipelineDBComponent
  },
  {
    path: 'productionmonitor',
    component: ProductionMonitorComponent
  },
  {
    path: 'apexchart',
    component: ApexChartComponent
  },
  {
    path: 'Profile',
    component: ProfileComponent
  },
  {
    path: 'chart',
    component: ChartviewComponent
  },
  
  {
    path: 'permissionform',
    component: permissionformComponent
  },
 
  {
    path: 'permissionlist',
    component: permissionlistComponent
  },
  {
    path : 'approvallist',
    component: approvallistComponent
  },

  {
    path: 'gblog',
    component: GblogComponent
  }, 
  {
    path: 'sales',
    component: MarginsummaryComponent
  },
  {
    path: 'entity',
    component: EntitylookuptestComponent
  },
  { path: 'themestest', loadChildren: () => import(`./../../themes/themes.module`).then(m => m.ThemesModule) },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SamplescreensRoutingModule { }
