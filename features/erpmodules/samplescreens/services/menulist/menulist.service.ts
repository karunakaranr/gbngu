import { Injectable } from '@angular/core';
import { MenuListDBService } from '../../dbservices/menulist/menulistDb.service'; 
@Injectable({
  providedIn: 'root'
})
export class MenuService {
  constructor(public dbservice: MenuListDBService) { }

  public RolesAndRightsView(menuid: number,BizTransactionClassId:number){               
    return this.dbservice.RolesAndRights(menuid,BizTransactionClassId);
  }
}