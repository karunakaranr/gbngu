import { Injectable } from '@angular/core';
import { PipelineDBDbService } from './../../../dbservices/Dashboard/PipelineDB/PipelineDBdb.service';
@Injectable({
  providedIn: 'root'
})
export class PipelineDB {
  constructor(public dbservice: PipelineDBDbService) { }

  public pipelinedb(){        
    console.log("aaaa",this.dbservice.picklist())               //col
    return this.dbservice.picklist();
  }

  public Rowservice(obj){ 
    return this.dbservice.reportData(obj);
  }
}