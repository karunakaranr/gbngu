import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BrandRulesService {
  private rulesdata   = "assets/data/rulesengine.json";
 
  constructor(private http : HttpClient) { }
  ruledata(){
  return this.http.get(this.rulesdata);
   }

  
}
