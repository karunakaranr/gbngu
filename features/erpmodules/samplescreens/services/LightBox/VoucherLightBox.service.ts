import { Injectable } from '@angular/core';
import { VoucherLightBoxDbService } from '../../dbservices/LightBox/VoucherLightBoxDb.service';
@Injectable({
  providedIn: 'root'
})
export class VoucherLightBox {
  constructor(public dbservice: VoucherLightBoxDbService) { }

  public VoucherLightBoxView(data){               
    return this.dbservice.picklist(data);
  }
}