import { Injectable } from '@angular/core';
import { MMLightBoxDbService } from '../../dbservices/LightBox/MMLightBoxDb.service';
@Injectable({
  providedIn: 'root'
})
export class MMLightBox {
  constructor(public dbservice: MMLightBoxDbService) { }

  public MMLightBoxView(data){               
    return this.dbservice.picklist(data);
  }
}