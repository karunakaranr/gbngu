import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Chartsetting } from '../../models/chart.model';


@Injectable({
  providedIn: 'root'
})
export class Chartsettingservice {
    private _chartsetting  = "assets/data/chartsetting.json";
    
    constructor(private http : HttpClient) { }
    chartsetting(): Observable<Chartsetting[]>{
         return this.http.get<Chartsetting[]>(this._chartsetting);
      }
   
}
