import { Injectable } from '@angular/core';
import { StatementOfAccountDbService } from './../../../dbservices/Chart/StatementOfAccount/StatementOfAccountdb.service';
@Injectable({
  providedIn: 'root'
})
export class StatementOfAccount {
  constructor(public dbservice: StatementOfAccountDbService) { }

  public StatementOfAccountview(){
    return this.dbservice.picklist();
  }

  public Rowservice(obj){
    return this.dbservice.reportData(obj);
  }
}