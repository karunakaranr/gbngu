// wf-approval.service.ts

import { Injectable } from '@angular/core';
import { WFApprovalDbService } from '../../dbservices/WorkFlow Approval/WFApprovalDb.service';
import { approvallist, approval } from './../../models/iapproval.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WFApproval {
  // ApproveItem(formattedItems: any) {
  //   throw new Error('Method not implemented.');
  // }
  // removeSelectedItems(selectedItems: any) {
  //   throw new Error('Method not implemented.');
  // }
  constructor(public dbservice: WFApprovalDbService) { }

  public Requestbyme(): Observable<approvallist> {
    return this.dbservice.Requestbyme();
  }

  public Requesttome(): Observable<approvallist> {
    return this.dbservice.Requesttome();
  }

  public Requesttouser(picklistUser:any, DocumentNumber:any): Observable<approvallist> {
    return this.dbservice.Requesttouser(picklistUser,DocumentNumber);
  }


  public SearchItem(SearchItem: any): Observable<approvallist> {
    return this.dbservice.SearchItem(SearchItem);
  }

    public AproveItem(formattedItems: any[]): Observable<approval> {
      return this.dbservice.AproveItem(formattedItems);
    }

    public RejectedItem(formattedItems: any[]): Observable<approval> {
      return this.dbservice.RejectedItem(formattedItems);
    }
    public ResendItem(formattedItems: any[]): Observable<approval> {
      return this.dbservice.ResendItem(formattedItems);
    }

    public lighboxsend(mailData) {
      return this.dbservice.lighboxsend(mailData);
    }
    public forwardData(): Observable<approval> {
      return this.dbservice.forwardData();
    }
    public backwardData(): Observable<approval> {
      return this.dbservice.backwardData();
    }

 
}
