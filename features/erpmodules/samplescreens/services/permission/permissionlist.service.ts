import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { GBBaseDataServiceWN } from '@goodbooks/gbdata';

import { permissionlist } from './../../models/permission/ipermission.model';
import { PermissionlistDBService } from './../../dbservices/permission/permissionlistdb.service';


@Injectable({
  providedIn: 'root'
})
export class PermissionlistService extends GBBaseDataServiceWN<permissionlist> {
  constructor(public dbDataService: PermissionlistDBService) {
    super(dbDataService, 'TimeSlipId');
  }

  saveData(data: permissionlist): Observable<any> {
    return super.saveData(data);
  }

  getData(id: string): Observable<permissionlist> {
    return super.getData(id);
  }
  deleteData(id: any): Observable<any> {
    return super.deleteData(id);
  }
  getAll(): Observable<any> {
    return super.getAll();
  }

  moveFirst(): Observable<permissionlist> {
    return super.moveFirst();
  }

  movePrev(): Observable<permissionlist> {
    return super.movePrev();
  }

  moveNext(): Observable<permissionlist> {
    return super.moveNext();
  }

  moveLast(): Observable<permissionlist> {
    return super.moveLast();
  }
}
