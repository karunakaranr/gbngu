export interface ISelectionCycle {
    SelectionCycleId: number
    SelectionCycleCode: string
    SelectionCycleName: string
    SelectionCycleRemarks: string
    SelectionCycleDetailArray: SelectionCycleDetailArray[]
    SelectionCycleCreatedById: number
    SelectionCycleCreatedOn: string
    SelectionCycleModifiedById: number
    SelectionCycleModifiedOn: string
    SelectionCycleBaseClinetId: number
    SelectionCycleSortOrder: number
    SelectionCycleStatus: number
    SelectionCycleVersion: number
    SelectionCycleSourceType: number
  }
  
  export interface SelectionCycleDetailArray {
    SelectionCycleDetailId: number
    SelectionCycleId: number
    SelectionCycleCode: string
    SelectionCycleName: string
    SelectionCycleRemarks: string
    SelectionRoundId: number
    SelectionRoundCode: string
    SelectionRoundName: string
    SelectionRoundRemarks: string
    SelectionCycleDetailSlNo: number
    SelectionCycleDetailWeightage: number
    SelectionCycleDetailCreatedById: number
    SelectionCycleDetailCreatedOn: string
    SelectionCycleDetailModifiedById: number
    SelectionCycleDetailModifiedOn: string
    SelectionCycleDetailBaseClinetId: number
    SelectionCycleDetailSortOrder: number
    SelectionCycleDetailStatus: number
    SelectionCycleDetailVersion: number
    SelectionCycleDetailSourceType: number
  }
  