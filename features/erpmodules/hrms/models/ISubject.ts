export interface ISubject {
    
    SubjectId: number
    SubjectCode: string
    SubjectName: string
    SubjectParticulars: string
    SubjectSourceType: number
    SubjectSortOrder: number
    SubjectStatus: number
    SubjectVersion: number
    SubjectCreatedById: number
    SubjectCreatedOn: string
    SubjectModifiedById: number
    SubjectModifiedOn: string
}