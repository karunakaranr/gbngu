export interface IEmployeeHierarchy {
  Elevel: number
  Id: number
  Code: string
  Name: string
  ParentId: number
  ParentCode: string
  ParentName: string
  Designation: string
  Department: string
  EmployeePhoto: string
  Parent: any
  children: IEmployeeHierarchy[]
  Attr: any
}