export interface IEmployeeSalary {
    EmployeeSalaryId: number
    EmployeeId: number
    EmployeeCode: string
    EmployeeName: string
    EmployeeSalarySlNo: number
    EmployeeSalaryType: number
    EmployeeSalaryMonth: number
    EmployeeSalaryYear: number
    EmployeeSalaryFromDate: string
    EmployeeSalaryToDate: string
    EmployeeSalaryTotalEarnings: number
    EmployeeSalaryTotalDeduction: number
    EmployeeSalaryNetPay: number
    EmployeeSalaryC2C: number
    EmployeeSalaryGratuityDays: number
    EmployeeSalaryBonusOnAmount: number
  }
  