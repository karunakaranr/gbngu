export interface IShiftMaster{

    ShiftId: number
  ShiftCode: string
  ShiftDescription: string
  ShiftStartTime: number
  ShiftEndTime: number
  ShiftSecondHalfStartTime: number
  ShiftFirstHalfEndTime: number
  ShiftGraceMinutes: number
  ShiftBreakMinutes: number
  ShiftShiftMinutes: number
  ShiftIsProductionShift: number
  ShiftRemarks: string
  ShiftIsBreakApplicable: number
  ShiftWorkingMinutes: number
  ShiftFullDayMinutes: number
  ShiftHalfDayMinutes: number
  ShiftCreatedById: number
  ShiftCreatedByName: any
  ShiftCreatedOn: string
  ShiftModifiedById: number
  ShiftModifiedByName: any
  ShiftModifiedOn: string
  ShiftSortOrder: number
  ShiftStatus: number
  ShiftVersion: number
  ShiftSourceType: number
  ShiftCutOffMinutes: number
  ShiftCutOffMinutesOut: number
  ShiftWorkTimeMethod: number




}