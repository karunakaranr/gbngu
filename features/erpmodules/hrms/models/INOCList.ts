export interface INOCList {
    NOCListId: number
    NOCListCode: string
    NOCListName: string
    NOCListSortOrder: number
    NOCListStatus: number
    NOCListVersion: number
    NOCListCreatedById: number
    NOCListCreatedOn: string
    NOCListModifiedById: number
    NOCListModifiedOn: string
    NOCListCreatedByName: any
    NOCListModifiedByName: any
    NOCListDetailArray: NoclistDetailArray[]
  }
  
  export interface NoclistDetailArray {
    NOCListDetailId: number
    NOCListId: number
    NOCListDetailSlNo: number
    NOCListDetailParticulars: string
    DepartmentId: number
    DepartmentCode: string
    DepartmentName: string
    NOCListDetailRemarks: string
  }