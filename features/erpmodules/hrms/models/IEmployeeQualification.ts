export interface IEmployeeQualification {
    EmployeeQualificationId: number
    EmployeeId: number
    EmployeeCode: string
    EmployeeName: string
    EmployeeQualificationSlNo: number
    EmployeeQualificationCourse: string
    EmployeeQualificationInstitute: string
    EmployeeQualificationDuration: string
    EmployeeQualificationFromYear: string
    EmployeeQualificationToYear: number
    EmployeeQualificationGrade: string
    EmployeeQualificationRemarks: string
  }
  