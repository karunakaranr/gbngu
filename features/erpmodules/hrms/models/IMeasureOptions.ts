export interface IMeasureOptions{
    LovId: number
    LovCode: string
    LovName: string
    LovTypeId: number
    LovTypeCode: string
    LovTypeName: string
    LovTypeScoreRequired: number
    LovScore: number
    LovCreatedById: number
    LovCreatedOn: string
    LovCreatedByName: any
    LovModifiedById: number
    LovModifiedOn: string
    LovModifiedByName: any
    LovSortOrder: number
    LovStatus: number
    LovVersion: number
    LovSourceType: number
    ParentId: number
    ParentCode: string
    ParentName: string
    CodeDefineId: number
}