export interface IEmployeeSkill {
    EmployeeSkillId: number
    EmployeeId: number
    EmployeeCode: string
    EmployeeName: string
    EmployeeSkillSlno: number
    SkillId: number
    SkillCode: string
    SkillName: string
    EmployeeSkillSkillLevel: number
    EmployeeSkillParticulars: string
  }
  