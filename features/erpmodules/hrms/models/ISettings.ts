export interface ISettings 
{
    AdditionDeductionId: number
    AdditionDeductionCode: string
    AdditionDeductionName: string
    AdditionDeductionDisplayName: string
    AdditionDeductionFieldDataTypeName: string
    AdditionDeductionFieldDataType: number
    AdditionDeductionFieldDataSize: number
    AdditionDeductionApplicableTypeName: string
    AdditionDeductionApplicableType: number
    AdditionDeductionNatureName: string
    AdditionDeductionNature: number
    AdditionDeductionDefaultValue: string
    AdditionDeductionRemarks: string
    AdditionDeductionArrearApplicable: number
    LovTypeId: number
    LovTypeCode: string
    LovTypeName: string
    AdditionDeductionOperationType: number
    AdditionDeductionSplitType: number
    AdditionDeductionSortOrder: number
    AdditionDeductionStatus: number
    AdditionDeductionVersion: number
    AdditionDeductionCreatedById: number
    AdditionDeductionCreatedOn: string
    AdditionDeductionModifiedById: number
    AdditionDeductionModifiedOn: string
    AdditionDeductionCreatedByName: any
    AdditionDeductionModifiedByName: any
    AdditionDeductionIsPartialAllowed: number
    AdditionDeductionPayConfigurationIds: string
    AdditionDeductionPayConfigurationNames: string
    FormulaId: number
    FormulaCode: string
    FormulaName: string
    AdditionDeductionDisplayCode: string
}