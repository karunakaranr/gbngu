export interface IJobProfileLibrary {
    JobProfileId: number
    JobProfileName: string
    JobProfileType: number
    JobProfileRemarks: string
    JobProfileSummary: string
    DepartmentId: string
    DesignationId: number
    ReportingToDesignationId: number
    JobId: string
    JobNumber: string
    JobTitle: string
    JobDescription: string
    EmployeeId: number
    ReportingToId: number
    JobProfileSortOrder: number
    JobProfileStatus: number
    JobProfileVersion: number
    JobProfileDetailArray: any[]
  }