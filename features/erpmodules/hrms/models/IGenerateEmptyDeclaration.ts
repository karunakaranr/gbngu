export interface IGenerateEmptyDeclaration {
    PeriodName: string
    EmployeeCode: string
    SingleEmployee: number
    MultipleEmployee: number
    GenerateId: number
    PeriodId: number
    EmployeeId: number
  }