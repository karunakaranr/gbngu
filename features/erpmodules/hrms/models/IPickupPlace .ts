export interface IPickupPlace{

    VillageId: number
    VillageCode: string
    VillageName: string
    VillageDistance: number
    VillageEmployerAmount: number
    VillageEmployeeAmount: number
    VehicleId: number
    VehicleNumber: string
    VillageSortOrder: number
    VillageStatus: number
    VillageVersion: number
    VillageCreatedById: number
    VillageCreatedOn: string
    VillageModifiedById: number
    VillageModifiedOn: string


}