export interface IActivityRecruitmentProcess {
  
        ActivityId: number
        ActivityCode: string
        ActivityName: string
        ActivityNature: any
        ActivityTypeId: number
        ActivityTypeCode: string
        ActivityTypeName: string
        ActivityTypeGenerateType: any
        ActivityTypeAutoType: any
        ActivityTypeAutoTypeProcess: any
        ActivityTypeClassId: number
        ActivityTypeSubClassId: number
        ActivityTypeIsActive: number
        ActivityTypeRemarks: string
        ActivityPrefix: string
        ActivityIsTakePending: number
        ActivityIsAllocReq: number
        ActivityIsAllocDirect: number
        ActivityIsProductive: number
        ActivityIsRepeatable: number
        ActivityIsMultiResReq: number
        ActivityIsAllowRework: number
        ActivityIsOutputBased: number
        ActivityIsHavePrecon: number
        ActivityPreconChkList: string
        ActivityIsHavePostcon: number
        ActivityPostconChkList: string
        ActivityIsReviewReq: number
        ActivityReviewChkList: string
        ActivityIsAllocBased: number
        ActivityAllocType: string
        ActivityActGenType: string
        ActivityAutoType: string
        ActivityAutoTypeProcess: string
        ActivityClassId: number
        ActivitySubClassId: number
        ActivityIsActive: number
        ActivityRemarks: string
        ActivityIsNoBased: number
        ActivityMaxDays: number
        ActivityMaxHrs: number
        ActivityMinDays: number
        ActivityMinHrs: number
        ActivityAvgDays: number
        ActivityAvgHrs: number
        ActivityIsTravel: number
        ActivityIsF2F: number
        ActivityIsExpense: number
        ActivityFundCode: string
        ActivityIsReminder: number
        ActivityRMDNoOfDays: number
        ActivityRMDNoOfHrs: number
        ActivityRMDMode: any
        ActivityIsHour: number
        ActivityIsCharge: number
        ActivityLaboutAmt: number
        ActivityOHAmt: number
        ActivityChargeTypeCode: any
        ActivityCollAccCode: any
        ActivityPaidAccCode: any
        ActivityActivityRef: any
        ActivityActCostType: any
        ActivityTimeUOM: any
        ActivityItemCode: any
        ActivityCreatedById: number
        ActivityCreatedOn: string
        ActivityModifiedById: number
        ActivityModifiedOn: string
        ActivitySortOrder: number
        ActivityStatus: number
        ActivityVersion: number
        UOMId: number
        UOMCode: string
        UOMName: string
        ActivityRecoveryType: number
        ActivityRecoveryPercentage: number
        ActivityIsClaim: number
        ExpenseAccountId: number
        ExpenseAccountCode: string
        ExpenseAccountName: string
        BillAccountId: number
        BillAccountCode: string
        BillAccountName: string
        ItemId: number
        ItemCode: any
        ItemName: string
        SKUId: number
        SKUCode: string
        SKUName: string
        ParameterSetId: number
        ParameterSetCode: string
        ParameterSetName: string
        PreCheckListId: number
        PostCheckListId: number
        ReviewCheckListId: number
        ForTemplateId: number
        ForTemplateCode: string
        ForTemplateName: string
        BOMId: number
        BOMCode: string
        BOMName: string
        BORId: number
        ActivityApplicableResults: string
        ActivityApplicableResultsValue: string
        ActivityApplicableStages: string
        ActivityApplicableStagesValue: string
        ActivityIsScore: number
        ActivityIsLetter: number
        ActivityIsCityClass: number
        ActivityIsGrade: number
        ActivityIsMode: number
        ActivityIsVehicleType: number
        ActivityIsDistance: number
      }
      










