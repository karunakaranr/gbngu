
import {
  GBDataStateAction,
  ISelectListCriteria,
} from '@goodbooks/gbdata';
import { IEmployee } from '../../models/IEmployee';

    const EmployeeEntityName = 'EmployeeMaster';
  export class GetData extends GBDataStateAction<IEmployee> {
    static readonly type = '[' + EmployeeEntityName + '] getData';
    constructor(public id: number) {
      super();
    }
  }
  export class SaveData extends GBDataStateAction<IEmployee> {
    static readonly type = '[' + EmployeeEntityName + '] saveData';
    constructor(
      public payload: IEmployee
    ) {
      super();
    }
  }
  export class AddData extends GBDataStateAction<IEmployee> {
    static readonly type = '[' + EmployeeEntityName + '] addData';
    constructor(
      public payload: IEmployee
    ) {
      super();
    }
  }
  export class GetAll extends GBDataStateAction<IEmployee> {
    static readonly type = '[' + EmployeeEntityName + '] getAll';
    constructor(public scl: ISelectListCriteria) {
      super();
    }
  }
  export class GetAllAndMoveFirst extends GBDataStateAction<IEmployee> {
    static readonly type = '[' + EmployeeEntityName + '] getAllAndMoveFirst';
    constructor(public scl: ISelectListCriteria) {
      super();
    }
  }
  export class MoveFirst extends GBDataStateAction<IEmployee> {
    static readonly type = '[' + EmployeeEntityName + '] MoveFirst';
    constructor() {
      super();
    }
  }
  export class MovePrev extends GBDataStateAction<IEmployee> {
    static readonly type = '[' + EmployeeEntityName + '] MovePrev';
    constructor() {
      super();
    }
  }
  export class MoveNext extends GBDataStateAction<IEmployee> {
    static readonly type = '[' + EmployeeEntityName + '] MoveNext';
    constructor() {
      super();
    }
  }
  export class MoveLast extends GBDataStateAction<IEmployee> {
    static readonly type = '[' + EmployeeEntityName + '] MoveLast';
    constructor() {
      super();
    }
  }
  export class ClearData extends GBDataStateAction<IEmployee> {
    static readonly type = '[' + EmployeeEntityName + '] ClearData';
    constructor() {
      super();
    }
  }
  export class DeleteData extends GBDataStateAction<IEmployee> {
    static readonly type = '[' + EmployeeEntityName + '] DeleteData';
    constructor() {
      super();
    }
  }


