import {
    GBDataStateAction,
    GBDataStateActionFactoryWN,
    ISelectListCriteria,
  } from '@goodbooks/gbdata';
  import { IEmployee } from '../../models/IEmployee';
  import { GetAll, GetData, MoveFirst, MoveLast, MoveNext, MovePrev, SaveData, ClearData, DeleteData, AddData } from './employeemaster.action';

export class EmployeeStateActionFactory extends GBDataStateActionFactoryWN<IEmployee> {
    GetData(id: number): GBDataStateAction<IEmployee> {
      return new GetData(id);
    }
    GetAll(scl: ISelectListCriteria): GBDataStateAction<IEmployee> {
      return new GetAll(scl);
    }
    MoveFirst(): GBDataStateAction<IEmployee> {
      return new MoveFirst();
    }
    MoveNext(): GBDataStateAction<IEmployee> {
      return new MoveNext();
    }
    MovePrev(): GBDataStateAction<IEmployee> {
      return new MovePrev();
    }
    MoveLast(): GBDataStateAction<IEmployee> {
      return new MoveLast();
    }
    AddData(payload: IEmployee): GBDataStateAction<IEmployee> {
      return new AddData(payload);
    }
    SaveData(payload: IEmployee): GBDataStateAction<IEmployee> {
      return new SaveData(payload);
    }
    ClearData(): GBDataStateAction<IEmployee> {
      return new ClearData();
    }
    DeleteData(): GBDataStateAction<IEmployee> {
      return new DeleteData();
    }
  }
