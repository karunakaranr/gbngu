
import { State, Action, StateContext } from '@ngxs/store';
import { IEmployee } from '../../models/IEmployee';
import { GBDataState, GBDataStateModel } from '@goodbooks/gbdata';
import { EmployeeService } from '../../services/employeemaster/employeemaster.service';
import { Inject, Injectable } from '@angular/core';
import { GetAll, MoveFirst, MoveNext, MovePrev, MoveLast, GetData, SaveData, GetAllAndMoveFirst,ClearData, DeleteData, AddData  } from './employeemaster.action';


  export class EmployeeStateModel extends GBDataStateModel<IEmployee> {
  }

  @State<EmployeeStateModel>({
    name: 'EmployeeMaster',
    defaults: {
      data: [],
      areDataLoaded: false,
      currentData: {
        model: undefined,
        dirty: false,
        errors: '',
        status: ''
      },
      currentIndex: 0,
    },
  })
  @Injectable()
  export class EmployeeState extends GBDataState<IEmployee> {
    constructor(@Inject(EmployeeService) dataService: EmployeeService) {
      super(dataService, null);
    }

    @Action(GetData)
    GetData(context: StateContext<GBDataStateModel<IEmployee>>,
      { id }: GetData
    ) {
      return super.Super_GetData(context, id.toString());
    }
    @Action(ClearData)
    ClearData(context: StateContext<GBDataStateModel<IEmployee>>) {
      return super.Super_ClearData(context);
    }
    @Action(GetAll)
    GetAll(context: StateContext<GBDataStateModel<IEmployee>>, {scl}: GetAll) {
      return super.Super_GetAll(context, {scl: scl});
    }
    @Action(GetAllAndMoveFirst)
    GetAllAndMoveFirst(context: StateContext<GBDataStateModel<IEmployee>>, {scl}: GetAll) {

    }
    @Action(MoveFirst)
    MoveFirst(context: StateContext<GBDataStateModel<IEmployee>>) {
      return super.Super_MoveFirst(context);
    }
    @Action(MovePrev)
    MovePrev(context: StateContext<GBDataStateModel<IEmployee>>) {
      return super.Super_MovePrev(context);
    }
    @Action(MoveNext)
    MoveNext(context: StateContext<GBDataStateModel<IEmployee>>) {
      return super.Super_MoveNext(context);
    }
    @Action(MoveLast)
    MoveLast(context: StateContext<GBDataStateModel<IEmployee>>) {
      return super.Super_MoveLast(context);
    }
    @Action(DeleteData)
    DeleteData(context: StateContext<GBDataStateModel<IEmployee>>) {
      return super.Super_DeleteData(context);
    }
    @Action(SaveData)
    SaveData(context: StateContext<GBDataStateModel<IEmployee>>,
      { payload, id }: { payload: any; id: any }
    ) {
      return super.Super_SaveData(context, {payload: payload, id: id});
    }
    @Action(AddData)
    AddData(context: StateContext<GBDataStateModel<IEmployee>>,
      { payload }: { payload: any }
    ) {
      return super.Super_AddData(context, {payload: payload});
    }
  }
