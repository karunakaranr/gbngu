import { Injectable } from '@angular/core';
import {GBHttpService} from '../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';

const urls = require('./../../../URLS/urls.json');

@Injectable({
  providedIn: 'root',
})
export class MeasureOptionsDBService{
  // LovTypeCode='';
  endPoint: string = urls.MeasureOptions;  //lovtype
  endPoint1: string = urls.MeasureOptions1; //lov

  useAPIPagination=true;
  idField:string = "?LovId="
  constructor(public http: GBHttpService) {
  }

  getLovType(){
    const LovTypeurl = this.endPoint + 'SelectList/?firstNumber=1&maxResult=50';
    console.log("URL:",LovTypeurl);
    let criteria = {"SectionCriteriaList":[{"SectionId":0,"AttributesCriteriaList":[{"FieldName":"Code","OperationType":2,"FieldValue":"","InArray":null,"JoinType":1},{"FieldName":"Name","OperationType":2,"FieldValue":"","InArray":null,"JoinType":0}]}]}


    return this.http.httppost(LovTypeurl,criteria);
  }
  getCode(id){


    const Lovurl = this.endPoint1 + 'SelectList/?firstNumber=1&maxResult=50';
    console.log("URL:",Lovurl);
    if (id=='') {
      let criteria1 = {"SectionCriteriaList":[{"SectionId":0,"AttributesCriteriaList":[{"FieldName":"Code","OperationType":2,"FieldValue":"","InArray":null,"JoinType":0}]}]}
      return this.http.httppost(Lovurl,criteria1);
    }
    else{
      let criteria = {"SectionCriteriaList":[{"SectionId":0,"AttributesCriteriaList":[{
        "FieldName": "LovType.Id",
        "OperationType": 5,
        "FieldValue": id,
        "InArray": null,
        "JoinType": 2
    },{"FieldName":"Code","OperationType":2,"FieldValue":"","InArray":null,"JoinType":1}]}]}
      return this.http.httppost(Lovurl,criteria);
    }

  }
  getName(id){

    const Lovurl = this.endPoint1 + 'SelectList/?firstNumber=1&maxResult=50';
    console.log("URL:",Lovurl);
    if (id=='') {
      let criteria1 = {"SectionCriteriaList":[{"SectionId":0,"AttributesCriteriaList":[{"FieldName":"Name","OperationType":2,"FieldValue":"","InArray":null,"JoinType":0}]}]}
      return this.http.httppost(Lovurl,criteria1);
    }
    else{
    let criteria = {"SectionCriteriaList":[{"SectionId":0,"AttributesCriteriaList":[{
      "FieldName": "LovType.Id",
      "OperationType": 5,
      "FieldValue": id,
      "InArray": null,
      "JoinType": 2
  },{"FieldName":"Name","OperationType":2,"FieldValue":"","InArray":null,"JoinType":0}]}]}
    return this.http.httppost(Lovurl,criteria);
  }
  }

  getid(id){
    const url = this.endPoint1 + this.idField + id;
    return this.http.httpget(url);

  }
  
}
