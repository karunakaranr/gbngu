
import { Injectable } from '@angular/core';
import {GBHttpService} from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import {ISelectListCriteria } from './../../../../../libs/gbdata/src/lib/interfaces/ISectionCriteriaList';
import {GBBaseDBDataService } from './../../../../../libs/gbdata/src/lib/services/gbbasedbdata.service';
import { Observable } from 'rxjs';
import { IEmployee } from '../../models/IEmployee';
const urls = require('../../URLS/urls.json');

@Injectable({
  providedIn: 'root',
})
export class EmployeeDBService extends GBBaseDBDataService<IEmployee> {
  endPoint: string = urls.EmployeeMaster;
  endPointList: string = 'GetEmployeePicklistSelect2';
  firstNumber = -1;
  maxResult = 50;
  useAPIPagination = true;
  previous:boolean=false;

  constructor(http: GBHttpService) {
    super(http);
  }

  putData(data: IEmployee) {
    return super.putData(data);;
  }

  postData(data: IEmployee) {
    return super.postData(data);
  }

  getData(idField: string, id: string): Observable<IEmployee> {
    return super.getData(idField, id);
  }

  deleteData(idField: string, id: string) {
    return super.deleteData(idField, id)
  }

  getList(scl: ISelectListCriteria): Observable<any> {
   if(this.firstNumber > this.maxResult){
    if(this.previous == true){
    this.firstNumber = this.firstNumber - this.maxResult;
    this.previous=false;
    }
    else{
    if (this.firstNumber === -1) {
      this.firstNumber = 1;
    }
    else {
      this.firstNumber = this.maxResult + this.firstNumber;
    }
  }
}
else{
  if (this.firstNumber === -1) {
    this.firstNumber = 1;
  }
  else {
    this.firstNumber = this.maxResult + this.firstNumber;
  } 
}
    return super.getList(scl);
  }

}






























