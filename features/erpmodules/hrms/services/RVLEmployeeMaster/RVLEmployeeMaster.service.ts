import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IRVLEmployeeMaster } from '../../models/IRVLEmployeeMaster';


 
@Injectable({
  providedIn: 'root'
})
export class RVLEmployeeMasterService extends GBBaseDataServiceWN<IRVLEmployeeMaster> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IRVLEmployeeMaster>) {
    super(dbDataService, 'EmployeeId');
  }
}
