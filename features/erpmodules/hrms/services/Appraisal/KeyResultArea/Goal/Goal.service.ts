import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IGoal } from 'features/erpmodules/hrms/models/IGoal';




 
@Injectable({
  providedIn: 'root'
})
export class GoalService extends GBBaseDataServiceWN<IGoal> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IGoal>) {
    super(dbDataService, 'GoalId');
  }
}
