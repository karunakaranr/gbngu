import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IPerformanceMeasure } from 'features/erpmodules/hrms/models/IPerformanceMeasure';


// import { ModelName } from ‘ModelPath’;


 
@Injectable({
  providedIn: 'root'
})
export class PerformanceMeasureService extends GBBaseDataServiceWN<IPerformanceMeasure> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPerformanceMeasure>) {
    super(dbDataService, 'MeasureId');
  }
}
