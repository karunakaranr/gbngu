import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { IMeasureOptionsType } from 'features/erpmodules/hrms/models/IMeasureOptionsType';

``

 
@Injectable({
  providedIn: 'root'
})
export class MeasureOptionsTypeService extends GBBaseDataServiceWN<IMeasureOptionsType> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IMeasureOptionsType>) {
    super(dbDataService, 'LovTypeId');
  }
}
