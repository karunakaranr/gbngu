import { Injectable } from '@angular/core';
import { EmployeeSkillListdbservice } from './../../dbservices/EmployeeSkillList/EmployeeSkillListdb.service';
@Injectable({
  providedIn: 'root'
})
export class EmployeeSkillListService {
  constructor(public dbservice: EmployeeSkillListdbservice) { }

  public Reportdetailservice(){                      //col
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                    //row
    return this.dbservice.reportData(obj);
  }
}``