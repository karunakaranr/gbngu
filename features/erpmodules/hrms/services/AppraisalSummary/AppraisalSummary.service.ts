import { Injectable } from '@angular/core';
import { AppraisalSummaryDbService } from '../../dbservices/AppraisalSummary/AppraisalSummaryDb.service';
import { ICriteriaDTODetail } from 'features/erpmodules/sales/models/IFilterDetail';
@Injectable({
  providedIn: 'root'
})
export class AppraisalSummary {
  constructor(public dbservice: AppraisalSummaryDbService) { }

  public AppraisalSummaryview(FilterData : ICriteriaDTODetail,ReportType: number){               
    return this.dbservice.picklist(FilterData,ReportType);
  }
}