import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IPayTaxSetting } from '../../models/IPayTaxSetting';



 
@Injectable({
  providedIn: 'root'
})
export class PayTaxSettingService extends GBBaseDataServiceWN<IPayTaxSetting> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPayTaxSetting>) {
    super(dbDataService, 'PayTaxTypeDetailId');
  }
}
