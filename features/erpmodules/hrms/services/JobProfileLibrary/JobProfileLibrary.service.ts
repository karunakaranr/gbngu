import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IJobProfileLibrary } from '../../models/IJobProfileLibrary';


 
@Injectable({
  providedIn: 'root'
})
export class JobProfileLibraryService extends GBBaseDataServiceWN<IJobProfileLibrary> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IJobProfileLibrary>) {
    super(dbDataService, 'JobProfileId');
  }
}
