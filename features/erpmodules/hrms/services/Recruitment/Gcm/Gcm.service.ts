import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IGcm } from 'features/erpmodules/hrms/models/IGcm';



 
@Injectable({
  providedIn: 'root'
})
export class GcmService extends GBBaseDataServiceWN<IGcm> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IGcm>) {
    super(dbDataService, 'GcmId');
  }
}
