import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IRecruitment } from '../../models/IRecruitment';



 
@Injectable({
  providedIn: 'root'
})
export class RecruitmentService extends GBBaseDataServiceWN<IRecruitment> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IRecruitment>) {
    super(dbDataService, 'WorkTypeId');
  }
}
