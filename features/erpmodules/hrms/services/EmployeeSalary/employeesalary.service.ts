import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IEmployeeSalary } from '../../models/IEmployeeSalary';


 
@Injectable({
  providedIn: 'root'
})
export class EmployeeSalaryService extends GBBaseDataServiceWN<IEmployeeSalary> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IEmployeeSalary>) {
    super(dbDataService, 'Employee.Id');
  }
}
