import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IEmployeeWorking } from 'features/erpmodules/hrms/models/IEmployeeWorking';
 
@Injectable({
  providedIn: 'root'
})
export class EmployeeWorkingService extends GBBaseDataServiceWN<IEmployeeWorking> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IEmployeeWorking>) {
    super(dbDataService, 'Employee.Id');
  }
}
