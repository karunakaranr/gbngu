import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { ISkillMaster } from '../../models/ISkillMaster';



 
@Injectable({
  providedIn: 'root'
})
export class SkillMasterService extends GBBaseDataServiceWN<ISkillMaster> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ISkillMaster>) {
    super(dbDataService, 'SkillId');
  }
}
