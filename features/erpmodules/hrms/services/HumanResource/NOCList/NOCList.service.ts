import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { INOCList } from 'features/erpmodules/hrms/models/INOCList';



 
@Injectable({
  providedIn: 'root'
})
export class NOCListService extends GBBaseDataServiceWN<INOCList> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<INOCList>) {
    super(dbDataService, 'NOCListId');
  }
}
