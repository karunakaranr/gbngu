import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IOULevelSetting } from '../../models/IOULevelSetting';




 
@Injectable({
  providedIn: 'root'
})
export class OULevelSettingService extends GBBaseDataServiceWN<IOULevelSetting> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IOULevelSetting>) {
    super(dbDataService, 'OUId');
  }
}
