import { Injectable } from '@angular/core'
import { PayRevisionListDbService } from './../../dbservices/PayRevisionList/PayRevisionDb.service';
@Injectable({
  providedIn: 'root'
})
export class PayRevisionList {
  constructor(public dbservice: PayRevisionListDbService) { }

  public payrevisionlistview(){                      
    return this.dbservice.picklist();
  }

  public Rowservice(obj){                    
    return this.dbservice.reportData(obj);
  }
}