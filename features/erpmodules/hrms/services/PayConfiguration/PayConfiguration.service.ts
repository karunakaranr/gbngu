import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


// import { ModelName } from ‘ModelPath’;
import { IPayConfiguration } from '../../models/IPayConfiguration';

 
@Injectable({
  providedIn: 'root'
})
export class PayConfigurationService extends GBBaseDataServiceWN<IPayConfiguration> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IPayConfiguration>) {
    super(dbDataService, 'PayConfigurationId');
  }
}
