import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';
import { IGenerateEmptyDeclaration } from '../../models/IGenerateEmptyDeclaration';


@Injectable({
  providedIn: 'root'
})
export class GenerateEmptyDeclarationService extends GBBaseDataServiceWN<IGenerateEmptyDeclaration> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IGenerateEmptyDeclaration>) {
    super(dbDataService, 'GenerateId');
  }
}