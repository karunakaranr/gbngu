import { Injectable } from '@angular/core';
import { VillageListdbservice } from './../../dbservices/VillageList/VillageListdb.service';
@Injectable({
  providedIn: 'root'
})
export class VillageListService {
  constructor(public dbservice: VillageListdbservice) { }

  public Reportdetailservice(){                     
    return this.dbservice.picklist();
  }

  public Rowservice(obj){             
    return this.dbservice.reportData(obj);
  }
}``