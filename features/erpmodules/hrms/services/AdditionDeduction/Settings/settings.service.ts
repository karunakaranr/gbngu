import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';

import { ISettings } from 'features/erpmodules/hrms/models/ISettings';



 
@Injectable({
  providedIn: 'root'
})
export class SettingsService extends GBBaseDataServiceWN<ISettings> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ISettings>) {
    super(dbDataService, 'AdditionDeductionId');
  }
}
