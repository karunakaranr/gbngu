import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { ICasteCategory } from '../../models/ICasteCategory';


 
@Injectable({
  providedIn: 'root'
})
export class CasteCategoryService extends GBBaseDataServiceWN<ICasteCategory> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<ICasteCategory>) {
    super(dbDataService, 'CasteCategoryId');
  }
}
