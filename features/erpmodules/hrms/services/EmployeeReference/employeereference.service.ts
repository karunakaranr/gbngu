import { Inject, Injectable } from '@angular/core';
import { GBBaseDataServiceWN, GBBaseDBDataService } from '@goodbooks/gbdata';


import { IEmployeeReference } from '../../models/IEmployeeReference';


 
@Injectable({
  providedIn: 'root'
})
export class EmployeeReferenceService extends GBBaseDataServiceWN<IEmployeeReference> {
  constructor(@Inject('DBDataService') public dbDataService: GBBaseDBDataService<IEmployeeReference>) {
    super(dbDataService, 'Employee.Id');
  }
}
