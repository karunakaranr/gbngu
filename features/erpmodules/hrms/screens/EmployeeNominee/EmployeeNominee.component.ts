import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
// import { ServiceName } from ‘ServicePath’;
// import { IEmployeeNominee[] } from ‘ModelPath’;
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { HRMSURLS } from '../../URLS/urls';
import { EmployeeNomineeService } from '../../services/EmployeeNominee/EmployeeNominee.service';
import { IEmployeeNominee } from '../../models/IEmployeeNominee';
import * as EmployeeNomineeJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/EmployeeNominee.json'
// import * as JsonName from ‘JsonPath’ './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PackSet.json';
// import { url} from ‘UrlPath’;


@Component({
  selector: 'app-EmployeeNominee',
  templateUrl: './EmployeeNominee.component.html',
  styleUrls: ['./EmployeeNominee.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'EmployeeNomineeId'},
    { provide: 'url', useValue: HRMSURLS.EmployeeNominee },
    { provide: 'saveurl', useValue: HRMSURLS.EmployeeNomineesave },
    { provide: 'DataService', useClass: EmployeeNomineeService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class EmployeeNomineeComponent extends GBBaseDataPageComponentWN<IEmployeeNominee[] > {
  title: string ='EmployeeNominee'
  EmployeeNomineeJSON = EmployeeNomineeJSON;


  form: GBDataFormGroupWN<IEmployeeNominee[] > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'EmployeeNominee', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      // this.FormFillFunction(arrayOfValues.EmployeeNomineeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  // public FormFillFunction(SelectedPicklistData: string): void {
  //   if (SelectedPicklistData) {
  //     this.gbps.dataService.postData(SelectedPicklistData).subscribe(EmployeeNominee => {
  //       // this.form.get('EmployeeId').patchValue(EmployeeNominee[0] EmployeeId)
  //       // this.form.get('EmployeeCode').patchValue(EmployeeNominee[0].EmployeeCode)
  //       // this.form.get('EmployeeName').patchValue(EmployeeNominee[0].EmployeeName)
  //       this.form.get('EmployeeNomineeArray').patchValue(EmployeeNominee)
  //     })
  //   }
  // }

  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }

  public EmployeeNominee(EmployeeNominee) {

    this.gbps.dataService.postData(EmployeeNominee).subscribe(res =>{
        // console.log("rrr",res)
        this.form.patchValue(res[0])
        this.form.get('EmployeeId').patchValue(res[0].EmployeeId)
        this.form.get('EmployeeCode').patchValue(res[0].EmployeeCode)
        this.form.get('EmployeeName').patchValue(res[0].EmployeeName)

                this.form.get('EmployeeNomineeArray').patchValue(res)
        // console.log("EmployeeNomineeArray",this.form.value)
    })
}
}



export function getThisDBDataService(http: GBHttpService, url: string,saveurl:string): GBBaseDBDataService<IEmployeeNominee[]> {
  const dbds: GBBaseDBDataService<IEmployeeNominee[]> = new GBBaseDBDataService<IEmployeeNominee[]>(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IEmployeeNominee[] >, dbDataService: GBBaseDBDataService<IEmployeeNominee[] >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IEmployeeNominee[] > {
  return new GBDataPageService<IEmployeeNominee[] >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


