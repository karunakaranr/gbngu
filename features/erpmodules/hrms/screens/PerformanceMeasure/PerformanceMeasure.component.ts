import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { HRMSURLS } from '../../URLS/urls';
import { PerformanceMeasureService } from '../../services/Appraisal/PerformanceMeasure/PerformanceMeasure.service';
import { IPerformanceMeasure } from '../../models/IPerformanceMeasure';
import * as PerformanceMeasureJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PerformanceMeasure.json'
import { InventoryURLS } from 'features/erpmodules/inventory/URLS/urls';



@Component({
  selector: 'app-PerformanceMeasure',
  templateUrl: './PerformanceMeasure.component.html',
  styleUrls: ['./PerformanceMeasure.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'MeasureId'},
    { provide: 'url', useValue: HRMSURLS.PerformanceMeasur },
    { provide: 'DataService', useClass: PerformanceMeasureService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PerformanceMeasureComponent extends GBBaseDataPageComponentWN< IPerformanceMeasure > {
  title: string = 'PerformanceMeasure'
  PerformanceMeasureJSON = PerformanceMeasureJSON;


  form: GBDataFormGroupWN< IPerformanceMeasure > = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'PerformanceMeasure', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.MeasureId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }

  public addnew(templateRef) {
    let dialogRef = this.dialog.open(templateRef, {
      width: '600px',
      height:"500PX"
    });
  }
  savedata

  public BankFillFunction(formdata): void {
    if (formdata) {
      this.savedata = formdata
      console.log("PARAMTERUOM",this.savedata)
    }
  }

  public newsave(){
    console.log("SAVEEE",this.savedata)
    let title = 'Bank';
    let url = InventoryURLS.LovType
    this.formaction.savemodel(this.savedata,title,url)
    this.dialog.closeAll()
  }  
}



export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService< IPerformanceMeasure > {
  const dbds: GBBaseDBDataService< IPerformanceMeasure > = new GBBaseDBDataService< IPerformanceMeasure >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService< IPerformanceMeasure >, dbDataService: GBBaseDBDataService< IPerformanceMeasure >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService< IPerformanceMeasure > {
  return new GBDataPageService< IPerformanceMeasure >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


