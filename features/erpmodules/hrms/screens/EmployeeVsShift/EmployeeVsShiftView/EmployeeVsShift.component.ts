import { HttpClient } from '@angular/common/http';
import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
// import { ItemsubcategoryService } from 'features/erpmodules/inventory/services/itemsubcategory/itemsubcategory.service';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
  selector: 'app-EmployeeVsShift',
  templateUrl: './EmployeeVsShift.component.html',
  styleUrls: ['./EmployeeVsShift.component.scss'],
})
export class EmployeeVsShiftComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];
  public TotalValue;
  constructor(public sharedService: SharedService,public store: Store) { }

  ngOnInit(): void {
    this.getEmployeeVsShiftView();
  }
  
  public getEmployeeVsShiftView() {
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("EmployeeVsShift Data :", this.rowData)


      let json = this.rowData;
      var finalizedArray = [];
      json.map((row,index) => {
        finalizedArray.push({
          sno: index,
          shiftname: row['ShiftPatternName'],


          empcode: row['EmployeeCode'],
          empname: row['EmployeeName'],
          patdate: row['PatternDate'],
          depname: row['DepartmentName'],
        });
      });


      const final = {};
      finalizedArray.forEach((detail) => {
        final[detail.shiftname] = {
          accountGroups: {},
          empcode: detail.shiftname,
          empname: "",
          patdate: "",
          depname: "",
          ...final[detail.shiftname],
        };

      
        final[detail.shiftname].accountGroups[detail.sno] = {
          empcode: detail.empcode,
          empname: detail.empname,
          patdate: detail.patdate,
          depname: detail.depname,
        };
      });

      const grpcodes = Object.keys(final);

      const tableData = [];
      grpcodes.forEach((sno) => {
        const accountGroups = Object.keys(final[sno].accountGroups);

        tableData.push({
          empcode: final[sno].empcode,
          empname: "",
          patdate: "",
          depname: "",
          bold: true,
        });

        const accounts = Object.keys(final[sno].accountGroups);
        accounts.forEach((account) => {
          tableData.push({
            empcode: final[sno].accountGroups[account].empcode,
            empname: final[sno].accountGroups[account].empname,
            patdate: final[sno].accountGroups[account].patdate,
            depname: final[sno].accountGroups[account].depname,
          });
        });
      });
      this.rowData = tableData;

      console.log("Final Data:", this.rowData)
    }
    });
  }

}
