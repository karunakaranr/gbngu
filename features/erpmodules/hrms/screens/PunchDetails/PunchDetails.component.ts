import { Component, Inject, LOCALE_ID, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { Observable } from 'rxjs';
import { SharedService } from 'features/commonreport/service/datapassing.service';
import { IPunchDetail } from '../../models/IPunchDetail';
// import { MapComponent } from 'features/erpmodules/samplescreens/screens/Map/map.component';
// const mapdata= require("./../../../samplescreens/screens/Map/map.json")
@Component({
  selector: 'app-PunchDetails',
  templateUrl: './PunchDetails.component.html',
  styleUrls: ['./PunchDetails.component.scss']
})

export class PunchDetailsComponent implements OnInit {
  RowData: IPunchDetail[];
  constructor(public sharedService: SharedService,@Inject(LOCALE_ID) public locale: string ) { }
  ngOnInit(): void {
      this.DataReceiver();
  }

  public DataReceiver() {
    // console.log("mapdata:",mapdata)
    // this.RowData = mapdata.tabledata;
    this.sharedService.getRowInfo().subscribe(data => {
      this.RowData = data
      console.log("Punch Details Data:", data)
    });
  }

  countInOccurrencesUpToIndex(index: number): number {
    let count = 0;
    for (let i = 0; i <= index; i++) {
        if (this.RowData[i].Type === 'In') {
            count++;
        }
    }
    return count;
}

 parseTimeString(timeString: string): Date | null {
  const currentDate = new Date(); // Use the current date as a placeholder
  const [hours, minutes] = timeString.split(':').map(part => parseInt(part));

  // Set the time components to the current date
  currentDate.setHours(hours);
  currentDate.setMinutes(minutes);

  return currentDate;
}


calculateDuration(fromTime: string, toTime: string): string {
  try {
      if (!fromTime || !toTime) {
          return '';
      }

      const from = this.parseTimeString(fromTime);
      const to = this.parseTimeString(toTime);
      
      if (!from || !to) {
          throw new Error('Invalid time format');
      }

      const durationMs = to.getTime() - from.getTime();
      const hours = Math.floor(durationMs / (1000 * 60 * 60));
      const minutes = Math.floor((durationMs % (1000 * 60 * 60)) / (1000 * 60));

      const formattedHours = String(hours).padStart(2, '0');
      const formattedMinutes = String(minutes).padStart(2, '0');

      return `${formattedHours}:${formattedMinutes}`;
  } catch (error) {
      console.error('Error calculating duration:', error);
      return 'Invalid time';
  }
}



}
  
