import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Select, Store } from '@ngxs/store';

import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { HRMSURLS } from '../../URLS/urls';
import { OULevelSettingService } from '../../services/OULevelSetting/OULevelSetting.service';
import { IOULevelSetting } from '../../models/IOULevelSetting';
import * as OULevelSettingJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/OULevelSetting.json'
import { AuthState } from 'features/common/shared/stores/auth.state';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-OULevelSetting',
  templateUrl:'oulevelsetting.component.html',
  styleUrls: ['oulevelsetting.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'Id'},
    { provide: 'url', useValue: HRMSURLS.OULevelSetting },
    { provide: 'DataService', useClass: OULevelSettingService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class OULevelSettingComponent extends GBBaseDataPageComponentWN<IOULevelSetting> {
  @Select(AuthState.AddDTO) LOGINDTO$: Observable<any>;
  title : string ="OULevelSetting"
  OULevelSettingJSON = OULevelSettingJSON
  LoginDTO
  form: GBDataFormGroupWN<IOULevelSetting> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "OULevelSetting", {}, this.gbps.dataService);
  thisConstructor() {
    this.LOGINDTO$.subscribe(dto => {
      console.log("LoginDTO:",dto)
      this.LoginDTO = dto;
    })
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if(arrayOfValues != null){
      this.OULevelSettingPicklistFillValue(arrayOfValues.Id)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.OULevelSettingPicklistFillValue(this.LoginDTO.WorkOUId)
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
   }
 
  public OULevelSettingPicklistFillValue(SelectedPicklistData: string): void {
    console.log("SelectedPicklistData:",SelectedPicklistData)
    if (SelectedPicklistData) {
        this.gbps.dataService.getData(SelectedPicklistData).subscribe(OULevelSetting => {
          console.log("OULevelSetting:",OULevelSetting)
            this.form.patchValue(OULevelSetting[0]);
          })
      }
    }
  
  
    public OULevelSettingPicklistPatchValue(SelectedPicklistDatas: any): void {
      this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
    
    public ChangeTab(event, TabName:string) :void{
        var i:number, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
          tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("FormTabLinks");
        for (i = 0; i < tablinks.length; i++) {
          tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(TabName).style.display = "block";
        console.log("Event:",event)
        event.currentTarget.className += " active";
      }
  }
  
  
  export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IOULevelSetting> {
    const dbds: GBBaseDBDataService<IOULevelSetting> = new GBBaseDBDataService<IOULevelSetting>(http);
    dbds.endPoint = url;
    return dbds;
  }
  export function getThisPageService(store: Store, dataService: GBBaseDataService<IOULevelSetting>, dbDataService: GBBaseDBDataService<IOULevelSetting>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IOULevelSetting> {
    return new GBDataPageService<IOULevelSetting>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
  }
  