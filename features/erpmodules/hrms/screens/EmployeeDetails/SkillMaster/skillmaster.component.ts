// import { Component } from '@angular/core';
// import { FormBuilder } from '@angular/forms';
// import { ActivatedRoute, Router } from '@angular/router';
// import { GBHttpService } from 'libs/gbcommon/src';
// import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
// import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
// import { Store } from '@ngxs/store';
// import { SkillMasterService } from 'features/erpmodules/hrms/services/SkillMaster/SkillMaster.service';
// import { ISkillMaster } from 'features/erpmodules/hrms/models/ISkillMaster';

// import { ButtonVisibility } from 'features/layout/store/layout.actions';

// const urls = require('./../../../URLS/urls.json');


// @Component({
//   selector: 'app-SkillMaster',
//   templateUrl: 'skillmaster.component.html',
//   styleUrls: ['skillmaster.component.scss'],
//   providers: [
//     { provide: 'IdField', useValue: 'SkillId'},
//     { provide: 'url', useValue: urls.SkillMaster },
//     { provide: 'DataService', useClass: SkillMasterService },
//     { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
//     { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
//   ]
// })
// export class SkillMasterComponent extends GBBaseDataPageComponentWN<ISkillMaster> {
//   title : string = "SkillMaster"
//   form: GBDataFormGroupWN<ISkillMaster> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "SkillMaster", {}, this.gbps.dataService);
//   thisConstructor() {
//     let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
//     if(arrayOfValues != null){
//       this.Getselectedid(arrayOfValues.SkillId)
//       this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
//     } else {
//       this.store.dispatch(new ButtonVisibility("Forms"))
//     }
//    }
 
//   public Getselectedid(SelectedPicklistData: string): void {
//     if (SelectedPicklistData) {
//         this.gbps.dataService.getData(SelectedPicklistData).subscribe(SkillMaster => {
//             this.form.patchValue(SkillMaster);
//           })
//       }
//     }
  
  
//     public Getselectedids(SelectedPicklistDatas: any): void {
//       this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
//     }
//   }
  
  
//   export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ISkillMaster> {
//     const dbds: GBBaseDBDataService<ISkillMaster> = new GBBaseDBDataService<ISkillMaster>(http);
//     dbds.endPoint = url;
//     return dbds;
//   }
//   export function getThisPageService(store: Store, dataService: GBBaseDataService<ISkillMaster>, dbDataService: GBBaseDBDataService<ISkillMaster>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ISkillMaster> {
//     return new GBDataPageService<ISkillMaster>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
//   }
  

import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ISkillMaster } from 'features/erpmodules/hrms/models/ISkillMaster';
import { SkillMasterService } from 'features/erpmodules/hrms/services/SkillMaster/SkillMaster.service';
import { HRMSURLS } from './../../../URLS/urls'

import * as SkillMasterJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/SkillMaster.json'



import { ButtonVisibility } from 'features/layout/store/layout.actions';

@Component({
  selector: 'app-SkillMaster',
  templateUrl:'./skillmaster.component.html',
  styleUrls: ['./skillmaster.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'SkillId' },
    { provide: 'url', useValue:HRMSURLS.SkillMaster },
    { provide: 'DataService', useClass: SkillMasterService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class SkillMasterComponent extends GBBaseDataPageComponentWN<ISkillMaster> {
  title: string = "SkillMaster"
  SkillMasterJSON = SkillMasterJSON;

  form: GBDataFormGroupWN<ISkillMaster> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "SkillMaster", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.SkillMasterValue(arrayOfValues.SkillId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public SkillMasterValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(SkillMaster => {
        this.form.patchValue(SkillMaster);
      })
    }
  }
  public SkillMasterNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ISkillMaster> {
  const dbds: GBBaseDBDataService<ISkillMaster> = new GBBaseDBDataService<ISkillMaster>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ISkillMaster>, dbDataService: GBBaseDBDataService<ISkillMaster>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ISkillMaster> {
  return new GBDataPageService<ISkillMaster>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
