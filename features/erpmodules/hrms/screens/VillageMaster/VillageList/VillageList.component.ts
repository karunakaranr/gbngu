import { Component, OnInit } from '@angular/core';
import { VillageListService } from './../../../services/VillageList/VillageList.service';

const col = require('./../../../../../../apps/Goodbooks/Goodbooks/src/assets/data/VillageList.json')

@Component({
  selector: 'app-VillageList.component',
  templateUrl: './VillageList.component.html',
  styleUrls: ['./VillageList.component.scss']
})

export class VillageListComponent implements OnInit {
  title = 'Village List'
  public rowData = []
  public columnData = []

  constructor(public serivce: VillageListService) { }
  ngOnInit(): void {
    this.VillageList();
  }
  public VillageList() {
    this.serivce.Reportdetailservice().subscribe(menudetails => {
      this.columnData = col;
      let servicejsondetail = menudetails
      this.serivce.Rowservice(servicejsondetail).subscribe(res => {
        this.rowData = res.ReportDetail;
      });
    })

  }

}
