import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { ICriteriaDTODetail } from 'features/erpmodules/sales/models/IFilterDetail';
import { Inject, LOCALE_ID } from '@angular/core';
import { AppraisalSummary } from '../../services/AppraisalSummary/AppraisalSummary.service';
import { IAppraisalDetail } from '../../models/IAppraisalDetail';

@Component({
  selector: 'app-AppraisalSummary',
  templateUrl: './AppraisalSummary.component.html',
  styleUrls: ['./AppraisalSummary.component.scss']
})

export class AppraisalSummarycomponent implements  OnChanges {
  @Input() ReportType : number;
  @Input() FilterData : ICriteriaDTODetail;
  RowData : IAppraisalDetail[]=[];
  constructor(public service: AppraisalSummary, @Inject(LOCALE_ID) public locale: string) { }
  ngOnChanges(changes: SimpleChanges): void {
    if(changes.ReportType){
      this.service.AppraisalSummaryview(this.FilterData,this.ReportType).subscribe(menudetails => {
        this.RowData=menudetails;
        console.log("Appraisal Summary Data:",this.RowData)
      }) 
    }
    console.log("Report Type: " ,this.ReportType);
  }
  

}