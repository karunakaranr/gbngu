import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { ISelectionCycle } from 'features/erpmodules/hrms/models/ISelectionCycle';
import { HRMSURLS } from 'features/erpmodules/hrms/URLS/urls';
import { SelectionCycleService } from 'features/erpmodules/hrms/services/Recruitment/SelectionCycle.service';
import * as SelectionCycleJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/SelectionCycle.json'

@Component({
    selector: 'app-SelectionCycle',
    templateUrl:'./selectioncycle.component.html',
    styleUrls: ['./selectioncycle.component.scss'],
    providers: [
        { provide: 'IdField', useValue: 'SelectionCycleId' },
        { provide: 'url', useValue: HRMSURLS.SelectionCycle },
        { provide: 'DataService', useClass: SelectionCycleService },
        { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
        { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
    ]
})
export class SelectionCycleComponent extends GBBaseDataPageComponentWN<ISelectionCycle> {
    title: string = "SelectionCycle"
    SelectionCycleJSON = SelectionCycleJSON

    form: GBDataFormGroupWN<ISelectionCycle> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "SelectionCycle", {}, this.gbps.dataService);
    thisConstructor() {
        let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
        if (arrayOfValues != null) {
            this.SelectionCyclePicklistFillValue(arrayOfValues.SelectionCycleId)
            this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
        } else {
            this.store.dispatch(new ButtonVisibility("Forms"))
        }
    }

    public SelectionCyclePicklistFillValue(SelectedPicklistData: string): void {
        console.log("SelectedPicklistData",SelectedPicklistData)
        if (SelectedPicklistData) {
            this.gbps.dataService.getData(SelectedPicklistData).subscribe(SelectionCycle => {
                console.log("SelectionCycle",SelectionCycle)
                    this.form.patchValue(SelectionCycle);
                 
            })
        }
    }



    public SelectionCyclePicklistPatchValue(SelectedPicklistDatas: any): void {
        this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ISelectionCycle> {
    const dbds: GBBaseDBDataService<ISelectionCycle> = new GBBaseDBDataService<ISelectionCycle>(http);
    dbds.endPoint = url;
    return dbds;
}


export function getThisPageService(store: Store, dataService: GBBaseDataService<ISelectionCycle>, dbDataService: GBBaseDBDataService<ISelectionCycle>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ISelectionCycle> {
    return new GBDataPageService<ISelectionCycle>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}