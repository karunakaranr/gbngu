import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { HRMSURLS } from 'features/erpmodules/hrms/URLS/urls';
import { RecruitmentService } from 'features/erpmodules/hrms/services/Recruitment/Recruitment.service';
import { IRecruitment } from 'features/erpmodules/hrms/models/IRecruitment';
import * as RecruitmentJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/Recruitment.json'



@Component({
  selector: 'app-Recruitment',
  templateUrl: './Recruitment.component.html',
  styleUrls: ['./Recruitment.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'WorkTypeId'},
    { provide: 'url', useValue: HRMSURLS.RecruitmentCode},
    { provide: 'DataService', useClass: RecruitmentService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class RecruitmentComponent extends GBBaseDataPageComponentWN<IRecruitment > {
  title: string = 'Recruitment'
  RecruitmentJSON = RecruitmentJSON;


  form: GBDataFormGroupWN<IRecruitment> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'Recruitment', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.FormFillFunction(arrayOfValues.WorkTypeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public FormFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
      })
    }
  }


  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IRecruitment> {
  const dbds: GBBaseDBDataService<IRecruitment> = new GBBaseDBDataService<IRecruitment>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IRecruitment>, dbDataService: GBBaseDBDataService<IRecruitment>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IRecruitment> {
  return new GBDataPageService<IRecruitment>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
