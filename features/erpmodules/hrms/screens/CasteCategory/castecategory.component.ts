import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { CasteCategoryService } from '../../services/CasteCategory/castecategory.service';
import { ICasteCategory } from '../../models/ICasteCategory';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as CasteCategoryjson from'./../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CasteCategory.json';
import { HRMSURLS } from '../../URLS/urls';


@Component({
  selector: 'app-castecategory',
  templateUrl: './castecategory.component.html',
  styleUrls: ['./castecategory.component.scss'], 
  providers: [
    { provide: 'IdField', useValue: 'CasteCategoryId'},
    { provide: 'url', useValue: HRMSURLS.CasteCategory},
    { provide: 'DataService', useClass: CasteCategoryService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class CasteCategoryComponent extends GBBaseDataPageComponentWN<ICasteCategory> {
  title: string = 'CasteCategory'
  CasteCategoryjson = CasteCategoryjson;


  form: GBDataFormGroupWN<ICasteCategory> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'CasteCategory', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.CastecategoryRetrivalValue(arrayOfValues.CasteCategoryId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

public GetDecimalValue(GetValue:any){
  console.log("GetValue**",GetValue)
  this.form.get('CasteCategoryReservationPercentage').patchValue(GetValue);
}

  public CastecategoryRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(CasteCategory => {
        this.form.patchValue(CasteCategory);
      })
    }
  }
  public CastecategoryPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<ICasteCategory> {
  const dbds: GBBaseDBDataService<ICasteCategory> = new GBBaseDBDataService<ICasteCategory>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<ICasteCategory>, dbDataService: GBBaseDBDataService<ICasteCategory>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<ICasteCategory> {
  return new GBDataPageService<ICasteCategory>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
