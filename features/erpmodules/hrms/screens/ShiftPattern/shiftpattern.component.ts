import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IShiftPattern } from '../../models/IShiftPattern';
import { ShiftPatternService } from '../../services/ShiftPattern/shiftpattern.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { HRMSURLS } from 'features/erpmodules/hrms/URLS/urls';
import * as ShiftPatternJSON  from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ShiftPattern.json'

@Component({
  
  selector: 'app-ShiftPattern',
  templateUrl: 'shiftpattern.component.html',
  styleUrls: ['shiftpattern.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'ShiftPatternId' },
    { provide: 'url', useValue:HRMSURLS.ShiftPattern },
    { provide: 'DataService', useClass: ShiftPatternService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ShiftPatterncomponent extends GBBaseDataPageComponentWN<IShiftPattern> {
  title: string = "ShiftPattern"
  ShiftPatternJSON = ShiftPatternJSON;

  form: GBDataFormGroupWN<IShiftPattern> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "ShiftPattern", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ShiftPatternValue(arrayOfValues.ShiftPatternId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public ShiftPatternValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(ShiftPattern => {
        this.form.patchValue(ShiftPattern);
      })
    }
  }
  public ShiftPatternNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IShiftPattern> {
  const dbds: GBBaseDBDataService<IShiftPattern> = new GBBaseDBDataService<IShiftPattern>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IShiftPattern>, dbDataService: GBBaseDBDataService<IShiftPattern>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IShiftPattern> {
  return new GBDataPageService<IShiftPattern>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
