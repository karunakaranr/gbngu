import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { EmployeeSalaryService } from '../../services/EmployeeSalary/employeesalary.service';
import { IEmployeeSalary } from '../../models/IEmployeeSalary';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as EmployeeSalaryJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/EmployeeSalary.json';
import { HRMSURLS } from '../../URLS/urls';


@Component({
  selector: 'app-employeesalary',
  templateUrl: './employeesalary.component.html',
  styleUrls: ['./employeesalary.component.scss'], 
  providers: [
    { provide: 'IdField', useValue: 'EmployeeSalaryId'},
    { provide: 'url', useValue: HRMSURLS.EmployeeSalary},
    { provide: 'saveurl', useValue: HRMSURLS.EmployeeSalarySave},
    { provide: 'deleteurl', useValue: HRMSURLS.EmployeeSalarySave},
    { provide: 'DataService', useClass: EmployeeSalaryService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl','deleteurl'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class EmployeeSalaryComponent extends GBBaseDataPageComponentWN<IEmployeeSalary[]> {
  title: string = 'EmployeeSalary'
  EmployeeSalaryJSON = EmployeeSalaryJSON;

  form: GBDataFormGroupWN<IEmployeeSalary[]> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'EmployeeSalary', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.EmployeeSalaryRetrivalValue(arrayOfValues.EmployeeSalaryId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

  public EmployeeSalaryRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {

        console.log("SelectedPicklistData",SelectedPicklistData);

      this.gbps.dataService.postData(SelectedPicklistData).subscribe(EmployeeSalary => {
        if (EmployeeSalary.length == 0) {
            console.log("EmployeeSalary:",EmployeeSalary)
            console.log("Form Patch:",this.form.value)
            this.form.get('EmployeeSalaryId').patchValue(SelectedPicklistData)
          } else {
              console.log("EmployeeSalary:", EmployeeSalary);
              this.form.get('EmployeeSalaryId').patchValue(EmployeeSalary[0].EmployeeSalaryId);
              this.form.get('EmployeeName').patchValue(EmployeeSalary[0].EmployeeName);
              this.form.get('EmployeeSalaryArray').patchValue(EmployeeSalary);
              console.log("Form Patch:", this.form.value);
          }
      })
    }
  }

  public EmployeeSalaryPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}

export function getThisDBDataService(http: GBHttpService, url: string, saveurl: string, deleteurl:string): GBBaseDBDataService<IEmployeeSalary[]> {
  const dbds: GBBaseDBDataService<IEmployeeSalary[]> = new GBBaseDBDataService<IEmployeeSalary[]>(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl;
  dbds.deleteurl = deleteurl;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IEmployeeSalary[]>, dbDataService: GBBaseDBDataService<IEmployeeSalary[]>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IEmployeeSalary[]> {
  return new GBDataPageService<IEmployeeSalary[]>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
