

import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as EmployeeQualificationJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/EmployeeQualification.json';
import { HRMSURLS } from '../../URLS/urls';
import { IEmployeeQualification } from '../../models/IEmployeeQualification';
import { EmployeeQualificationService } from '../../services/EmployeeQualifiacation/EmployeeQualification.Service';

@Component({
  selector: 'app-EmployeeQualification',
  templateUrl: './EmployeeQualification.component.html',
  styleUrls: ['./EmployeeQualification.component.scss'],
  providers: [
    { provide: 'IdField', useValue: "EmployeeQualificationId"},
    { provide: 'url', useValue: HRMSURLS.EmployeeQualification },
    { provide: 'saveurl', useValue: HRMSURLS.EmployeeQualificationSave },
    { provide: 'deleteurl', useValue: HRMSURLS.EmployeeQualificationDelete },
    { provide: 'DataService', useClass: EmployeeQualificationService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url', 'saveurl', 'deleteurl','IdField'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class EmployeeQualificationComponent extends GBBaseDataPageComponentWN<IEmployeeQualification> {
  title: string = 'EmployeeQualification'
  EmployeeQualificationJSON = EmployeeQualificationJSON;


  form: GBDataFormGroupWN<IEmployeeQualification> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "EmployeeQualification", {}, this.gbps.dataService);
  thisConstructor() {
    console.log("HRMSURLS.EmployeeQualification:",HRMSURLS)
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.EmployeeQualificationFillFunction(arrayOfValues.EmployeeQualificationId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }


  public EmployeeQualificationFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.postData(SelectedPicklistData).subscribe(EmployeeQualification => {
        this.form.get('EmployeeId').patchValue(EmployeeQualification[0].EmployeeId)
        this.form.get('EmployeeCode').patchValue(EmployeeQualification[0].EmployeeCode)
        this.form.get('EmployeeName').patchValue(EmployeeQualification[0].EmployeeName)
        this.form.get('EmployeeQualificationArray').patchValue(EmployeeQualification)
      })
    }
  }

  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string, saveurl: string, deleteurl:string,IdField:string): GBBaseDBDataService<IEmployeeQualification> {
  const dbds: GBBaseDBDataService<IEmployeeQualification> = new GBBaseDBDataService<IEmployeeQualification>(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl;
  dbds.deleteurl = deleteurl;
  dbds.IdField = IdField
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IEmployeeQualification>, dbDataService: GBBaseDBDataService<IEmployeeQualification>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IEmployeeQualification> {
  return new GBDataPageService<IEmployeeQualification>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
