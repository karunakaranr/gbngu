import { Component, OnInit } from '@angular/core';
import { EmployeeSkillListService } from './../../../../services/EmployeeSkillList/EmployeeSkillList.service'
import { NavigationExtras, Router } from '@angular/router';
 const col = require('apps/Goodbooks/Goodbooks/src/assets/data/brandlist.json');


@Component({
  selector: 'app-EmployeeSkillList.component',
  templateUrl: './EmployeeSkillList.component.html',
  styleUrls: ['./EmployeeSkillList.component.scss']
})

export class EmployeeSkillListComponent implements OnInit {
  title = 'Employee Skill List'
  public rowData = []
  public columnData = []

  constructor(public serivce: EmployeeSkillListService ,private router: Router) { }
  ngOnInit(): void {
    this.employeeskilllist();
  }
  public employeeskilllist() {
    this.serivce.Reportdetailservice().subscribe(menudetails => {
      this.columnData = col;
      let servicejsondetail = menudetails
      this.serivce.Rowservice(servicejsondetail).subscribe(res => {
        this.rowData = res.ReportDetail;
        
        for (let data of this.rowData) {
          if (data.EmployeeSkillParticulars.length>20) {
            data.EmployeeSkillParticulars=data.EmployeeSkillParticulars.substring(0,20)+"...";
          }
        }
      });
    })

  }

  selectedcell(data) {
    const queryParams: any = {};
    queryParams.myArray = JSON.stringify(data);
    queryParams.Id = '-1399999025';
    const navigationExtras: NavigationExtras = {
      queryParams
    };
    this.router.navigate(["HTMLVIEW"],navigationExtras);

  }

}
