
import { IEmployee } from '../../../../models/IEmployee';
import { GBDataFormGroupStoreWN, GBDataFormGroupWN } from './../../../../../../../libs/uicore/src/lib/classes/GBFormGroup';
import { HttpClient } from '@angular/common/http';
import { EmployeeService } from '../../../../services/employeemaster/employeemaster.service';
import { Store } from '@ngxs/store';
import { EmployeeStateActionFactory } from '../../../../stores/employeemaster/employeemaster.actionfactory'

export class EmployeemasterFormgroup extends GBDataFormGroupWN<IEmployee> {
  constructor(http: HttpClient, dataService: EmployeeService) {
    super(http, 'SC0010', {}, dataService)
  }

  public clear() {
    super.clear();
  }

  public delete() {
    super.delete();
  }

  public save() {
    super.save();
  }

  public moveFirst() {
    super.moveFirst();
  }

  public movePrev() {
    super.movePrev();
  }

  public moveNext() {
    super.moveNext();
  }

  public moveLast() {
    super.moveLast();
  }
}
export class EmployeemasterFormgroupStore extends GBDataFormGroupStoreWN<IEmployee> {
  constructor(http: HttpClient, store: Store) {
    super(http, 'SC0010', {}, store, new EmployeeStateActionFactory())
  }

  public clear() {
    super.clear();
  }

  public delete() {
    super.delete();
  }

  public save() {
    super.save()
  }

  public moveFirst() {
    super.moveFirst();
  }

  public movePrev() {
    super.movePrev();
  }

  public moveNext() {
    super.moveNext();
  }

  public moveLast() {
    super.moveLast();
  }
  
}

