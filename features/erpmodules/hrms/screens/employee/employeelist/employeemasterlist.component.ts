import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Select } from '@ngxs/store';
import { GridProperties } from 'features/common/shared/models/gbgrid/gridview.model';
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';
import { GridSetting } from '../../../../inventory/models/IBrand';
import { EmployeeService } from '../../../services/employeemaster/employeemaster.service';
import {GBHttpService} from "./../../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service"


@Component({
  selector: 'app-employeemasterlist',
  templateUrl: './employeemasterlist.component.html',
  styleUrls: ['./employeemasterlist.component.scss'],
})

export class EmployeemasterlistComponent implements OnInit {
  title = 'Employeemaster list'
  public rowData = [];
  public columnData = [];
  public defaultColDef = [];
  sortDir = 1;
  constructor(public service: EmployeeService, public router: Router, public route: ActivatedRoute, private GBHttpService:GBHttpService) { 
  }
 
  ngOnInit(): void {
    this.employeelist();
  }

  @Select(LayoutState.ThemeClass) themeClass$: Observable<string>;

  public employeelist() {
    this.service.getAll().subscribe((res) => {
      this.rowData = res;
      console.log(this.rowData);
      this.getgridsetting();
    });
  }
  public getgridsetting() {
    this.gridSetting().subscribe((res) => {
      this.columnData = res;
      console.log(this.columnData);
      this.getgridproperties();
    });
  }
  public getgridproperties() {
    this.gridproperties().subscribe((res) => {
      this.defaultColDef = res;
    });
  }

  private grid_setting = "assets/data/employeelist.json";
  private grid_properties = "assets/data/grid-properties.json";
  gridSetting(): Observable<GridSetting[]> {
    return this.GBHttpService.httpgetasset(this.grid_setting);
  }
  gridproperties(): Observable<GridProperties[]> {
    return this.GBHttpService.httpgetasset(this.grid_properties);
  }

  public nextclick() {
    this.service.getAll().subscribe(data => {
      if (data == 'server Error') {
        this.rowData = this.rowData;
      }
      else {
        this.rowData = data;
      }
    });
  }
  public previousclick() {
    this.service.previous = true;
    this.service.getAll().subscribe(data => {
      this.rowData = data;
    });
  }

  public onCell(event) {
    const urlpath = this.defaultColDef[0].routingpath;
    const params = event.Id;
    this.router.navigate([urlpath, params], { relativeTo: this.route });
  }

  onSortClick(event) {
    let target = event.currentTarget,
      classList = target.classList;

    if (classList.contains('fa-chevron-up')) {
      classList.remove('fa-chevron-up');
      classList.add('fa-chevron-down');
      this.sortDir = -1;
    } else {
      classList.add('fa-chevron-up');
      classList.remove('fa-chevron-down');
      this.sortDir = 1;
    }
    this.sortArr(this.columnData[0].headerName);
  }
  sortArr(colName: any) {
    this.rowData.sort((a, b) => {
      a = a[colName].toLowerCase();
      b = b[colName].toLowerCase();
      return a.localeCompare(b) * this.sortDir;
    });
  }
  deleteRow(x) {
    var delBtn = confirm(" Do you want to delete ?");
    if (delBtn == true) {
      this.rowData.splice(x, 1);
    }
  }


}

