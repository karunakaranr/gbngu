import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { INOCList } from 'features/erpmodules/hrms/models/INOCList';
import { NOCListService } from 'features/erpmodules/hrms/services/HumanResource/NOCList/NOCList.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { HRMSURLS } from 'features/erpmodules/hrms/URLS/urls';
import * as NOCListJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/NOCList.json'


@Component({
  selector: 'app-NOCList',
  templateUrl: 'noclist.component.html',
  styleUrls: ['noclist.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'NOCListId' },
    { provide: 'url', useValue: HRMSURLS.NOCList },
    { provide: 'DataService', useClass: NOCListService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class NOCListComponent extends GBBaseDataPageComponentWN<INOCList> {
  title: string = "NOCList"
  NOCListJSON = NOCListJSON;

  form: GBDataFormGroupWN<INOCList> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "NOCList", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.NOCListValue(arrayOfValues.NOCListId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public NOCListValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(NOCList => {
        this.form.patchValue(NOCList);
      })
    }
  }
  public NOCListNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<INOCList> {
  const dbds: GBBaseDBDataService<INOCList> = new GBBaseDBDataService<INOCList>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<INOCList>, dbDataService: GBBaseDBDataService<INOCList>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<INOCList> {
  return new GBDataPageService<INOCList>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
