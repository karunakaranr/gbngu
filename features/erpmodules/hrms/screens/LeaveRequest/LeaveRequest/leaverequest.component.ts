import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { ReportState } from 'features/commonreport/datastore/commonreport.state';
import { SharedService } from 'features/commonreport/service/datapassing.service';


@Component({
  selector: 'app-leaverequest',
  templateUrl: './leaverequest.component.html',
  styleUrls: ['./leaverequest.component.scss'],
})
export class leaverequestComponent implements OnInit {
  @Select(ReportState.RowDataPassing) rowdatacommon$: Observable<any>;
  public rowData = [];  
  
  constructor(public sharedService: SharedService,public store: Store) { }
  ngOnInit(): void {
    this.LeaveRequest();
  }
  
  public LeaveRequest() {
    this.sharedService.getRowInfo().subscribe(data => {
      if (data.length > 0) {
      this.rowData = data;
      console.log("Leave Request :", this.rowData)
    }
    });
  }

}
