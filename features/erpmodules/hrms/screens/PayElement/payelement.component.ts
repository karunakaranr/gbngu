import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { PayElementService } from '../../services/PayElement/payelement.service';
import { IPayElement } from '../../models/IPayElement';
import { HRMSURLS } from 'features/erpmodules/hrms/URLS/urls';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as PayElementJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PayElement.json'


@Component({
  selector: 'app-PayElement',
  templateUrl: 'payelement.component.html',
  styleUrls: ['payelement.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'PayelementId' },
    { provide: 'url', useValue:HRMSURLS.PayElement },
    { provide: 'DataService', useClass: PayElementService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PayElementComponent extends GBBaseDataPageComponentWN<IPayElement> {
  title: string = "PayElement"
  PayElementJSON = PayElementJSON;

  form: GBDataFormGroupWN<IPayElement> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "PayElement", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PayElementValue(arrayOfValues.PayelementId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

  public PayElementValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PayElement => {
        this.form.patchValue(PayElement);
      })
    }
  }
  public PayElementNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPayElement> {
  const dbds: GBBaseDBDataService<IPayElement> = new GBBaseDBDataService<IPayElement>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPayElement>, dbDataService: GBBaseDBDataService<IPayElement>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPayElement> {
  return new GBDataPageService<IPayElement>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
