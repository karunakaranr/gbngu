import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { EmployeeReferenceService } from '../../services/EmployeeReference/employeereference.service';
import { IEmployeeReference } from '../../models/IEmployeeReference';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as EmployeeReferenceJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/EmployeeReference.json';
import { HRMSURLS } from '../../URLS/urls';


@Component({
  selector: 'app-employeereference',
  templateUrl: './employeereference.component.html',
  styleUrls: ['./employeereference.component.scss'], 
  providers: [
    { provide: 'IdField', useValue: 'EmployeeReferenceId'},
    { provide: 'url', useValue: HRMSURLS.EmployeeReference},
    { provide: 'saveurl', useValue: HRMSURLS.EmployeeReferenceSave},
    { provide: 'deleteurl', useValue: HRMSURLS.EmployeeRefrenceDelete},
    { provide: 'DataService', useClass: EmployeeReferenceService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url','saveurl','deleteurl'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class EmployeeReferenceComponent extends GBBaseDataPageComponentWN<IEmployeeReference[]> {
  title: string = 'EmployeeReference'
  EmployeeReferenceJSON = EmployeeReferenceJSON;

  form: GBDataFormGroupWN<IEmployeeReference[]> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'EmployeeReference', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.EmployeeReferenceRetrivalValue(arrayOfValues.EmployeeReferenceId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

  public EmployeeReferenceRetrivalValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {

        console.log("SelectedPicklistData",SelectedPicklistData);

        this.gbps.dataService.postData(SelectedPicklistData).subscribe(EmployeeReference => {
          console.log("EmployeeReference*:",EmployeeReference)
          if (EmployeeReference.length == 0) {
              console.log("EmployeeReference:",EmployeeReference)
              console.log("Form Patch:",this.form.value)
              this.form.get('EmployeeReferenceId').patchValue(SelectedPicklistData)
            } else {
                console.log("EmployeeReference:", EmployeeReference);
                this.form.get('EmployeeReferenceId').patchValue(EmployeeReference[0].EmployeeReferenceId);
                this.form.get('EmployeeName').patchValue(EmployeeReference[0].EmployeeName);
                this.form.get('EmployeeReferenceArray').patchValue(EmployeeReference);
                console.log("Form Patch:", this.form.value);
            }
        })
     
    }
  }

  public EmployeeReferencePatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}

export function getThisDBDataService(http: GBHttpService, url: string, saveurl: string, deleteurl:string): GBBaseDBDataService<IEmployeeReference[]> {
  const dbds: GBBaseDBDataService<IEmployeeReference[]> = new GBBaseDBDataService<IEmployeeReference[]>(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl;
  dbds.deleteurl = deleteurl;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IEmployeeReference[]>, dbDataService: GBBaseDBDataService<IEmployeeReference[]>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IEmployeeReference[]> {
  return new GBDataPageService<IEmployeeReference[]>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
