import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IPickupPlace } from 'features/erpmodules/hrms/models/IPickupPlace ';
import { PickupPlaceService } from 'features/erpmodules/hrms/services/TypeMaster/PickupPlace/pickupplace.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { HRMSURLS } from 'features/erpmodules/hrms/URLS/urls';
import * as PickupPlaceJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PickupPlace.json'
@Component({
  selector: 'app-PickupPlace',
  templateUrl: 'pickupplace.component.html',
  styleUrls:['pickupplace.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'VillageId' },
    { provide: 'url', useValue: HRMSURLS.PickupPlace},
    { provide: 'DataService', useClass: PickupPlaceService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class PickupPlaceComponent extends GBBaseDataPageComponentWN<IPickupPlace> {
  title: string = "PickupPlace"
  PickupPlaceJSON = PickupPlaceJSON;

  form: GBDataFormGroupWN<IPickupPlace> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "PickupPlace", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PickupPlaceFormv(arrayOfValues.VillageId)
   
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public PickupPlaceFormv(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PickupPlace => {
        console.log('PickupPlace',PickupPlace)
          this.form.patchValue(PickupPlace);
      })
    }
  }

  public PickupPlaceFormnew(SelectedPicklistDatas: any){
    console.log("SelectedPicklistDatas**",SelectedPicklistDatas)
    let linkids = SelectedPicklistDatas.id.split(',')
    let linkvalues = SelectedPicklistDatas.value.split(',')
    console.log("linkids",linkids)
    console.log("linkvalues",linkvalues)
    for(let i = 0;i<linkids.length;i++){
      this.form.get(linkids[i])?.setValue(SelectedPicklistDatas.Selected[linkvalues[i]])
      console.log(linkids[i],":",this.form.get(linkids[i])?.value)
    }
    console.log("Form Value:",this.form.value)
  }
}

export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPickupPlace> {
  const dbds: GBBaseDBDataService<IPickupPlace> = new GBBaseDBDataService<IPickupPlace>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IPickupPlace>, dbDataService: GBBaseDBDataService<IPickupPlace>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPickupPlace> {
  return new GBDataPageService<IPickupPlace>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
