import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { ManPowerRequirementService } from '../../services/ManPowerRequirement/ManPowerRequirement.Service';
import { IManPowerRequirement } from '../../models/IManPowerRequirement';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { HRMSURLS } from '../../URLS/urls';

import * as ManPowerRequirementJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/ManPowerRequirement.json'



@Component({
  selector: 'app-ManPowerRequirement',
  templateUrl: 'manpowerrequirement.component.html',
  styleUrls:['manpowerrequirement.component.scss'],
  providers: [
    { provide: 'IdField', useValue:'ManPowerId' },
    { provide: 'url', useValue:HRMSURLS.ManPowerRequirement},
    { provide: 'DataService', useClass: ManPowerRequirementService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class ManPowerRequirementComponent extends GBBaseDataPageComponentWN<IManPowerRequirement> {
  title: string = "ManPowerRequirement"
  ManPowerRequirementJSON= ManPowerRequirementJSON;

  form: GBDataFormGroupWN<IManPowerRequirement> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "ManPowerRequirement", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.ManPowerRequirementValue(arrayOfValues.ManPowerId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public ManPowerRequirementValue(SelectedPicklistData: string): void {
    console.log("SelectedPicklistData",SelectedPicklistData)
    if (SelectedPicklistData) {
      console.log("SelectedPicklistData1",SelectedPicklistData)
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(ManPowerRequirement => {
          this.form.patchValue(ManPowerRequirement);
      })
    }
  }
  public ManPowerRequirementNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IManPowerRequirement> {
  const dbds: GBBaseDBDataService<IManPowerRequirement> = new GBBaseDBDataService<IManPowerRequirement>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IManPowerRequirement>, dbDataService: GBBaseDBDataService<IManPowerRequirement>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IManPowerRequirement> {
  return new GBDataPageService<IManPowerRequirement>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
