import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IEmployeeSkill } from '../../models/IEmployeeSkill';
import { EmployeeSkillService } from '../../services/EmployeeSkill/EmployeeSkill.Service';
import { HRMSURLS } from 'features/erpmodules/hrms/URLS/urls';
// import * as EmployeeSkillJSON  from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/EmployeeSkill.json'
import * as EmployeeSkillJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/EmployeeSkill.json'
import { ButtonVisibility } from 'features/layout/store/layout.actions';

@Component({
  selector: 'app-EmployeeSkill',
  templateUrl: 'employeeskill.component.html',
  styleUrls: ['employeeskill.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'EmployeeId' },
    { provide: 'url', useValue: HRMSURLS.EmployeeSkill },
    { provide: 'saveurl', useValue: HRMSURLS.EmployeeSkillSave },
    { provide: 'deleteurl', useValue: HRMSURLS.EmployeeSkillDelete },
    { provide: 'DataService', useClass: EmployeeSkillService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class EmployeeSkillComponent extends GBBaseDataPageComponentWN<IEmployeeSkill> {
  title: string = "EmployeeSkill"
  EmployeeSkillJSON = EmployeeSkillJSON;

  form: GBDataFormGroupWN<IEmployeeSkill> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "EmployeeSkill", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.EmployeeSkillValue(arrayOfValues.EmployeeSkillId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public EmployeeSkillValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.postData(SelectedPicklistData).subscribe(EmployeeSkill => {
        this.form.get('EmployeeId').patchValue(EmployeeSkill[0].EmployeeId)
        this.form.get('EmployeeCode').patchValue(EmployeeSkill[0].EmployeeCode)
        this.form.get('EmployeeName').patchValue(EmployeeSkill[0].EmployeeName)
        this.form.get('EmployeeSkillArray').patchValue(EmployeeSkill)
        this.form.patchValue(EmployeeSkill);
      })
    }
  }
  public EmployeeSkillRetrival(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IEmployeeSkill> {
  const dbds: GBBaseDBDataService<IEmployeeSkill> = new GBBaseDBDataService<IEmployeeSkill>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IEmployeeSkill>, dbDataService: GBBaseDBDataService<IEmployeeSkill>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IEmployeeSkill> {
  return new GBDataPageService<IEmployeeSkill>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
