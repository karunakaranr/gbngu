import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { PayPeriodService } from 'features/erpmodules/hrms/services/PayRollSettings/PayPeriod.service';
import { IPayPeriod } from 'features/erpmodules/hrms/models/IPayPeriod';
import * as PayPeriodJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/PayPeriod.json'
import { HRMSURLS } from 'features/erpmodules/hrms/URLS/urls';
import { ButtonVisibility } from 'features/layout/store/layout.actions';

@Component({
  selector: 'app-PayPeriod',
  templateUrl: 'payperiod.component.html',
  styleUrls: ['payperiod.component.scss'],
  providers: [

    { provide: 'IdField', useValue: 'PayPeriodId' },
    { provide: 'url', useValue: HRMSURLS.PayPeriod },
    { provide: 'DataService', useClass: PayPeriodService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})

export class PayPeriodComponent extends GBBaseDataPageComponentWN<IPayPeriod> {
  title: string = "PayPeriod"
  lockdate:string ;
  PayPeriodJSON = PayPeriodJSON;
  Startdate: string;
  Enddate: string;
  diffdate: number;
  PayPeriodFromDate: [''];
      PayPeriodLockDate: [''];
  form: GBDataFormGroupWN<IPayPeriod> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "PayPeriod", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.PayPeriodFormVal(arrayOfValues.PayPeriodId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public PayPeriodFormVal(SelectedPicklistData: string): void {

    if (SelectedPicklistData) {
      
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PayPeriod => {
        this.form.patchValue(PayPeriod);
      })
    }
  } 
  lockdatefunction(){

    let tommorow= new Date(this.form.get('PayPeriodFromDate').value);
    tommorow.setDate(tommorow.getDate()-1);
let lockdate =this.form.get('PayPeriodLockDate').setValue(tommorow)
console.log ("lockdate",lockdate)
  }

  // updateLockDate() {
  //   console.log ("hi",this.updateLockDate)
  //   const fromDate = new Date(this.form.get('PayPeriodFromDate').value);
  //   console.log ("fromDate",fromDate)
  //   fromDate.setDate(fromDate.getDate() - 1);

  //   this.form.get('PayPeriodLockDate').setValue(fromDate.toISOString().split('T')[0]);
    
  // }

  public InputPatch(payperiods){
console.log("payperiod",payperiods)
    this.form.get('PayPeriodDisplayName').patchValue(payperiods)
  }
  public PayPeriodpatch(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
  public ToDate(Input) {
    console.log("enttime:", Input)
    this.form.get('PayPeriodPayDate').patchValue(Input)
    this.form.get('PayPeriodAccountDate').patchValue(Input)
  }
  public getDateFromEpochString(epochString: string): Date {
    console.log("getDateFromEpochStringFunction")
    const epochMilliseconds = Number(epochString.match(/\d+/)[0]);
    return new Date(epochMilliseconds);
  }
  public calculateDateDifference(Startdate: string, Enddate: string): number {
    console.log("calculateDateDifferenceFunction")
    const dateObject1 = this.getDateFromEpochString(Startdate);
    const dateObject2 = this.getDateFromEpochString(Enddate);
    const differenceMilliseconds = Math.abs(dateObject2.getTime() - dateObject1.getTime());
    const differenceDays = Math.ceil(differenceMilliseconds / (1000 * 3600 * 24));
    return differenceDays;
  }
  public CalculateDays(Input) {
    this.Startdate = this.form.get('PayPeriodFromDate').value;
    this.Enddate = this.form.get('PayPeriodToDate').value;
    const differenceInDays = this.calculateDateDifference(this.Startdate, this.Enddate);
    console.log("differenceInDays", differenceInDays)
    this.form.get('PayPeriodTotalDays').patchValue(differenceInDays)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IPayPeriod> {
  const dbds: GBBaseDBDataService<IPayPeriod> = new GBBaseDBDataService<IPayPeriod>(http);
  dbds.endPoint = url;
  return dbds;
}

export function getThisPageService(store: Store, dataService: GBBaseDataService<IPayPeriod>, dbDataService: GBBaseDBDataService<IPayPeriod>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IPayPeriod> {
  return new GBDataPageService<IPayPeriod>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
