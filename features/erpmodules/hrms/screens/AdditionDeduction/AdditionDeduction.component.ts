import { Component, OnInit } from '@angular/core';
import { AdditionDeduction } from '../../services/AdditionDeduction/AdditionDeduction.service';
import {Inject, LOCALE_ID } from '@angular/core';

@Component({
  selector: 'app-AdditionDeduction',
  templateUrl: './AdditionDeduction.component.html',
  styleUrls: ['./AdditionDeduction.component.scss']
})

export class AdditionDeductionComponent implements OnInit {
  title = 'Addition Deduction'
  public rowData = []
  public columnData = []

  constructor(public serivce: AdditionDeduction, @Inject(LOCALE_ID) public locale: string) { }
  ngOnInit(): void {
    this.payrevisionlist();
  }
  public payrevisionlist() {
    this.serivce.additiondeductionview().subscribe(menudetails => {
      let servicejsondetail = menudetails
      this.serivce.Rowservice(servicejsondetail).subscribe(res => {
        this.rowData = res.ReportDetail;
        console.log("dkd", res);

        let json = this.rowData;
        var finalizedArray = []
        json.map(row => {
          finalizedArray.push({
            nature: row['AdditionDeductionNatureName'],
            code: row['AdditionDeductionCode'],
            name: row['AdditionDeductionName'],
            disname: row['AdditionDeductionDisplayName'],
            type: row['AdditionDeductionFieldDataTypeName'],
            size: row['AdditionDeductionFieldDataSize'],
            value: row['AdditionDeductionDefaultValue'],
            apptype: row['AdditionDeductionApplicableTypeName'],
            remark: row['AdditionDeductionRemarks']
          })
        })
        const final = {};
        finalizedArray.forEach(detail => {
          final[detail.nature] = {
            accounts :{},
            code: detail.nature,
            name: "",
            disname:"",
            type: "",
            size: "",
            value:"",
            apptype:"",
            remark:"",
            ...final[detail.nature], 
          }
          final[detail.nature].accounts[detail.code] = {
            code: detail.code,
            name: detail.name,
            disname: detail.disname,
            type:detail.type,
            size:detail.size,
            value:detail.value,
            apptype:detail.apptype,
            remark:detail.remark
          }
        })
        const empcodes = Object.keys(final)
        const tableData = [];
        empcodes.forEach(cd => {
          const account = Object.keys(final[cd].accounts)
            tableData.push({
              code: final[cd].code,
              name: "",
              disname:"",
              type:"",
              size:"",
              value:"",
              apptype: "",
              remark: "",
              bold: true,
            })
            account.forEach(ag => {
              tableData.push({
                code: final[cd].accounts[ag].code,
                name: final[cd].accounts[ag].name,
                disname: final[cd].accounts[ag].disname,
                type: final[cd].accounts[ag].type,
                size:  final[cd].accounts[ag].size,
                value:final[cd].accounts[ag].value,
                apptype:final[cd].accounts[ag].apptype,
                remark:final[cd].accounts[ag].remark,
              })
            })
        })
        this.rowData=tableData;
      })
    })

  }

}  