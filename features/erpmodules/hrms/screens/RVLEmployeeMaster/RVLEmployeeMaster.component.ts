import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { RVLEmployeeMasterService } from '../../services/RVLEmployeeMaster/RVLEmployeeMaster.service';
import { IRVLEmployeeMaster } from '../../models/IRVLEmployeeMaster';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as RVLEmployeeMasterJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/RVLEmployeeMaster.json'
import { HRMSURLS } from '../../URLS/urls';
import { today } from 'libs/ng-bre/src/lib/functions/customfunctions';



@Component({
  selector: 'app-RVLEmployeeMaster',
  templateUrl: './RVLEmployeeMaster.component.html',
  styleUrls: ['./RVLEmployeeMaster.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'EmployeeId'},
    { provide: 'url', useValue: HRMSURLS.TimSlipEmployee },
    { provide: 'DataService', useClass: RVLEmployeeMasterService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class RVLEmployeeMasterComponent extends GBBaseDataPageComponentWN<IRVLEmployeeMaster > {
  title: string = 'RVLEmployeeMaster'
  RVLEmployeeMasterJSON = RVLEmployeeMasterJSON;
FName:string="";
MName:string="";
LName:string="";

  form: GBDataFormGroupWN<IRVLEmployeeMaster> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'RVLEmployeeMaster', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.RVLEmployeeMasterFillFunction(arrayOfValues.EmployeeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }

public GetEmployeeAge(GetValue:string){
  console.log("Conversion:",GetValue.replace("/Date(", "").replace(")/", ""));
  let EmployeeAgeMilSec:any = GetValue.replace("/Date(", "").replace(")/", "")
console.log("EmployeeAgeMilSec",EmployeeAgeMilSec)
let today:any = new Date().getTime();
console.log("today",today)
let diff = today-EmployeeAgeMilSec
console.log("diff",diff)
  let millisecondsInSecond = 1000;
  let secondsInMinute = 60;
  let minutesInHour = 60;
  let hoursInDay = 24;
  let daysInYear = 365;
  const seconds = Math.floor(diff / millisecondsInSecond);
  console.log("seconds",seconds)
  const minutes = Math.floor(seconds / secondsInMinute);
  console.log("minutes",minutes)
  const hours = Math.floor(minutes / minutesInHour);
  console.log("hours",hours)
  const days = Math.floor(hours / hoursInDay);
  console.log("days",days)
  const years = Math.floor(days / daysInYear);
  console.log("years",years)
   this.form.get('EmployeeAge').patchValue(years)
}

public FindValue(value:number){
  console.log("value",value)
}
public FirstNamePatch(FirstName:string){
  console.log("FirstName",FirstName)
  this.FName=FirstName
  this.form.get('EmployeeName').patchValue(this.FName+" "+this.MName+" "+this.LName)
}

public MiddleNamePatch(MiddleName:string){
  this.MName = MiddleName
  console.log("MiddleName",MiddleName)
  this.form.get('EmployeeName').patchValue(this.FName+" "+this.MName+" "+this.LName)
}

public LastNamePatch(LastName:string){
  console.log("LastName",LastName)
  this.LName = LastName
  this.form.get('EmployeeName').patchValue(this.FName+" "+this.MName+" "+this.LName)
}


  public RVLEmployeeMasterFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(PackSet => {
        this.form.patchValue(PackSet);
        console.log("value*: ",this.form.value)
      })
    }
  }

  public getPresentAddress(addressvalue:any){
    console.log("addressvalue: ",addressvalue);
    if(addressvalue==0){
      console.log("Working")
      this.form.get('PermanentAddressDTO').patchValue(this.form.get('PresentAddressDTO').value);
      // this.form.get('PermanentAddressDTO').patchValue(this.form.get('PartyZipCodePresent').value);
      // this.form.get('PermanentAddressDTO').patchValue(this.form.get('CountryName').value)
    }
  }

  public RVLEmployeeMasterPatchValue(SelectedPicklistDatas: any): void {
    let idsplit= SelectedPicklistDatas.id.split(',')
    if(idsplit.length==1){
      this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
    } else {
      console.log("this.form.value:",this.form.value)
      let form =  JSON.parse(JSON.stringify(this.form.value))
      form[idsplit[0]][idsplit[1]] = SelectedPicklistDatas.SelectedData
      form[idsplit[0]][idsplit[2]] = SelectedPicklistDatas.Selected.Name
      this.form.patchValue(form)
    }
  }

  public RVLEmployeeMasterRetrievalFillFunction(SelectedPicklistDatas: any){
    console.log("SelectedPicklistDatas**",SelectedPicklistDatas)
    let linkids = SelectedPicklistDatas.id.split(',')
    let linkvalues = SelectedPicklistDatas.value.split(',')
    console.log("linkids",linkids)
    console.log("linkvalues",linkvalues)
    for(let i = 0;i<linkids.length;i++){
      this.form.get(linkids[i])?.setValue(SelectedPicklistDatas.Selected[linkvalues[i]])
    }
  }

  public addnew(templateRef) {
    let dialogRef = this.dialog.open(templateRef, {
      width: '600px'
    });
  }

  savedata
  Banksavedata

  public BankFillFunction(formdata): void {
    if (formdata) {
      this.savedata = formdata
      console.log("RVLSAVE",this.savedata)
    }
  }

  public newsave(){
    console.log("SAVEEE",this.savedata)
    let title = 'BankBranch';
    let url = HRMSURLS.BankBranch
    this.formaction.savemodel(this.savedata,title,url)
    this.dialog.closeAll()
  }

  public newsave2(){
    console.log("SAVEEE",this.Banksavedata)
    let title = 'Bank';
    let url = HRMSURLS.Bank
    this.formaction.savemodel(this.Banksavedata,title,url)
    this.dialog.closeAll()
  }

  public ChangeTab(event, TabName:string) :void{
    console.log("TabStyle")
    var i:number, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("FormTabLinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(TabName).style.display = "block";
    console.log("Event:",event)
    event.currentTarget.className += " active";
  }

}


export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IRVLEmployeeMaster > {
  const dbds: GBBaseDBDataService<IRVLEmployeeMaster > = new GBBaseDBDataService<IRVLEmployeeMaster >(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IRVLEmployeeMaster >, dbDataService: GBBaseDBDataService<IRVLEmployeeMaster >, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IRVLEmployeeMaster > {
  return new GBDataPageService<IRVLEmployeeMaster >(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
