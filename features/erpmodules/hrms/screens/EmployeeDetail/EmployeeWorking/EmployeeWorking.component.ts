import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { EmployeeWorkingService } from 'features/erpmodules/hrms/services/EmployeeDetail/EmployeeWorking/EmployeeWorking.service';
import { IEmployeeWorking } from 'features/erpmodules/hrms/models/IEmployeeWorking';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as EmployeeWorkingJSON from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/EmployeeWorking.json'
import { HRMSURLS } from './../../../URLS/urls';

@Component({
  selector: 'app-EmployeeWorking',
  templateUrl: './EmployeeWorking.component.html',
  styleUrls: ['./EmployeeWorking.component.scss'],
  providers: [
    { provide: 'IdField', useValue: "EmployeeId"},
    { provide: 'url', useValue: HRMSURLS.EmployeeWorking },
    { provide: 'saveurl', useValue: HRMSURLS.EmployeeWorkingSave },
    { provide: 'deleteurl', useValue: HRMSURLS.EmployeeWorkingDelete },
    { provide: 'DataService', useClass: EmployeeWorkingService},
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url', 'saveurl', 'deleteurl','IdField'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class EmployeeWorkingComponent extends GBBaseDataPageComponentWN<IEmployeeWorking> {
  title: string = 'EmployeeWorking'
  EmployeeWorkingJSON = EmployeeWorkingJSON;


  form: GBDataFormGroupWN<IEmployeeWorking> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "EmployeeWorking", {}, this.gbps.dataService);
  thisConstructor() {
    console.log("HRMSURLS.EmployeeWorking:",HRMSURLS)
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.EmployeeWorkingFillFunction(arrayOfValues.EmployeeId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
public getName(event:any){
  console.log("event",event)
}

  public EmployeeWorkingFillFunction(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.postData(SelectedPicklistData).subscribe(EmployeeWorking => {
        console.log("EmployeeWorking",EmployeeWorking)
        this.form.get('EmployeeId').patchValue(EmployeeWorking[0].EmployeeId)
        this.form.get('EmployeeCode').patchValue(EmployeeWorking[0].EmployeeCode)
        this.form.get('EmployeeName').patchValue(EmployeeWorking[0].EmployeeName)
        this.form.get('EmployeeWorkingArray').patchValue(EmployeeWorking)
      })
    }
  }

  public PicklistPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string, saveurl: string, deleteurl:string,IdField:string): GBBaseDBDataService<IEmployeeWorking> {
  const dbds: GBBaseDBDataService<IEmployeeWorking> = new GBBaseDBDataService<IEmployeeWorking>(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl;
  dbds.deleteurl = deleteurl;
  dbds.IdField = IdField
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IEmployeeWorking>, dbDataService: GBBaseDBDataService<IEmployeeWorking>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IEmployeeWorking> {
  return new GBDataPageService<IEmployeeWorking>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
