import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { GenerateEmptyDeclarationService } from '../../services/GenerateEmptyDeclaration/GenerateEmptyDelcaration.Service';
import { IGenerateEmptyDeclaration } from '../../models/IGenerateEmptyDeclaration';
import { HRMSURLS } from '../../URLS/urls';
import * as GenerateEmptyDeclarationJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/GenerateEmptyDeclaration.json'


import { ButtonVisibility } from 'features/layout/store/layout.actions';

@Component({
  selector: 'app-GenerateEmptyDeclaration',
  templateUrl:'./generateemptydeclaration.component.html',
  styleUrls: ['./generateemptydeclaration.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'GenerateId' },
    { provide: 'url', useValue:HRMSURLS.GenerateEmptyDeclaration }, 
    { provide: 'DataService', useClass: GenerateEmptyDeclarationService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class GenerateEmptyDeclarationComponent extends GBBaseDataPageComponentWN<IGenerateEmptyDeclaration> {
  title: string = "GenerateEmptyDeclaration"
  GenerateEmptyDeclarationJSON = GenerateEmptyDeclarationJSON;   

  form: GBDataFormGroupWN<IGenerateEmptyDeclaration> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "GenerateEmptyDeclaration", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.GenerateEmptyDeclarationValue(arrayOfValues.GenerateId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  
  public GenerateEmptyDeclarationValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(GenerateEmptyDeclaration => {
        this.form.patchValue(GenerateEmptyDeclaration);
      })
    }
  }
  public GenerateEmptyDeclarationNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IGenerateEmptyDeclaration> {
  const dbds: GBBaseDBDataService<IGenerateEmptyDeclaration> = new GBBaseDBDataService<IGenerateEmptyDeclaration>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IGenerateEmptyDeclaration>, dbDataService: GBBaseDBDataService<IGenerateEmptyDeclaration>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IGenerateEmptyDeclaration> {
  return new GBDataPageService<IGenerateEmptyDeclaration>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
