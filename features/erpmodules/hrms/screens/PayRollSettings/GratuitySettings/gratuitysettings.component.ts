import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { IGratuitySettings } from 'features/erpmodules/hrms/models/IGratuitySettings';
import { GratuitySettingsService } from 'features/erpmodules/hrms/services/PayRollSettings/GratuitySettings/gratuitysettings.service';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import { HRMSURLS } from 'features/erpmodules/hrms/URLS/urls';
import * as GratuitySettingsJSon from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/GratuitySettings.json'

@Component({
  selector: 'app-GratuitySettings',
  templateUrl: 'GratuitySettings.component.html',
  styleUrls: ['GratuitySettings.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'GratuityGroupId' },
    { provide: 'url', useValue: HRMSURLS.GratuitySettings },
    { provide: 'DataService', useClass: GratuitySettingsService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class GratuitySettingsComponent extends GBBaseDataPageComponentWN<IGratuitySettings> {
  title: string = "GratuitySettings"
  GratuitySettingsJSon = GratuitySettingsJSon;

  form: GBDataFormGroupWN<IGratuitySettings> = new GBDataFormGroupWN(this.gbps.gbhttp.http, "GratuitySettings", {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.GratuitySettingsValue(arrayOfValues.GratuityGroupId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public GratuitySettingsValue(SelectedPicklistData: string): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(GratuitySettings => {
        this.form.patchValue(GratuitySettings);
      })
    }
  }
  public GratuitySettingsNot(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}
export function getThisDBDataService(http: GBHttpService, url: string): GBBaseDBDataService<IGratuitySettings> {
  const dbds: GBBaseDBDataService<IGratuitySettings> = new GBBaseDBDataService<IGratuitySettings>(http);
  dbds.endPoint = url;
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IGratuitySettings>, dbDataService: GBBaseDBDataService<IGratuitySettings>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IGratuitySettings> {
  return new GBDataPageService<IGratuitySettings>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}
