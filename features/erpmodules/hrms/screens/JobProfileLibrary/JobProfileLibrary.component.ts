import { Component } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { GBHttpService } from 'libs/gbcommon/src';
import { GBBaseDataService, GBBaseDBDataService } from 'libs/gbdata/src';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService } from 'libs/uicore/src';
import { Store } from '@ngxs/store';
import { JobProfileLibraryService } from '../../services/JobProfileLibrary/JobProfileLibrary.service';
import { IJobProfileLibrary } from '../../models/IJobProfileLibrary';
import { ButtonVisibility } from 'features/layout/store/layout.actions';
import * as JobProfileLibraryJSON from './../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/JobProfileLibrary.json';
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
import { HRMSURLS } from '../../URLS/urls';
import { info } from 'console';
@Component({
  selector: 'app-JobProfileLibrary',
  templateUrl: './JobProfileLibrary.component.html',
  styleUrls: ['./JobProfileLibrary.component.scss'],
  providers: [
    { provide: 'IdField', useValue: 'JobProfileId' },
    { provide: 'url', useValue: HRMSURLS.JobProfileLibrary },
    { provide: 'saveurl', useValue: HRMSURLS.JobProfileSave },
    { provide: 'DataService', useClass: JobProfileLibraryService },
    { provide: 'DBDataService', useFactory: getThisDBDataService, deps: [GBHttpService, 'url', 'saveurl', 'IdField'] },
    { provide: 'PageService', useFactory: getThisPageService, deps: [Store, 'DataService', 'DBDataService', FormBuilder, GBHttpService] },
  ]
})
export class JobProfileLibraryComponent extends GBBaseDataPageComponentWN<IJobProfileLibrary> {
  title: string = 'JobProfileLibrary'
  JobProfileLibraryJSON = JobProfileLibraryJSON;
  JobProfileDetailId: number = 0;
  JobProfileDetailDetailType: any = 0
  domainId: number = -1
  domainName: String = ""
  SectionId: number = -1
  SectionName: String = ""
  ParticularsId: number = -1
  ParticularsName: string = ""
  KRAId: number = -1
  KRAName: String = ""
  SkillId: number = -1
  SkillName: String = ""
  ObjectiveId: number = -1
  ObjectiveName: String = ""
  JobProfileId: number = -1
  JobProfileName: string = "";
  isButtonDisabled: Boolean = true
  GridIndex : number = -1
  update : number = 1;
  EditArray: boolean = true

  ngOnInit(): void {
    console.log("Ready")
    var tabcontent
    tabcontent = document.getElementsByClassName("tabcontent");
    tabcontent[0].style.display = "block";
    console.log("Tabcontent", tabcontent[0])
    document.getElementById("ResponsibilitiesAndDuties").style.display = "block"
  } 
  form: GBDataFormGroupWN<IJobProfileLibrary> = new GBDataFormGroupWN(this.gbps.gbhttp.http, 'JobProfileLibrary', {}, this.gbps.dataService);
  thisConstructor() {
    let arrayOfValues = JSON.parse(this.activeroute.snapshot.queryParamMap.get('SelectedId'))
    if (arrayOfValues != null) {
      this.JobProfileLibraryFillFunction(arrayOfValues.JobProfileId)
      this.store.dispatch(new ButtonVisibility("FormsWithDataStoreValue"))
    } else {
      this.store.dispatch(new ButtonVisibility("Forms"))
    }
  }
  public ChangeTab(event, TabName: string): void {
    var i: number, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("FormTabLinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(TabName).style.display = "block";
    console.log("Event:", event)
    event.currentTarget.className += " active";
  }

  public RADLightBox(update) {
    console.log("update",update)
    console.log("LightboxWorking")
    document.getElementById("myModal").style.display = "block";
    if(update == 0){
      document.getElementById("lightBoxSaveRAD").style.display = "none";
      document.getElementById("lightboxUpdateRAD").style.display = "inline-block";
    }
    else{
      document.getElementById("lightBoxSaveRAD").style.display = "inline-block";
      document.getElementById("lightboxUpdateRAD").style.display = "none";
      this.form.get('DomainName').patchValue("");
    this.form.get('DomainId').patchValue(-1)
    this.form.get('SectionId').patchValue(-1)
    this.form.get('SectionName').patchValue("")
    this.form.get('JobProfileDetailParticulars').patchValue("")
    this.form.get('ParticularId').patchValue(-1)
    this.form.get('KRAName').patchValue("")
    this.form.get('KRAId').patchValue(-1)
    this.form.get('SkillName').patchValue("")
    this.form.get('SkillId').patchValue(-1)
    this.form.get('ObjectiveName').patchValue("")
    this.form.get('ObjectiveId').patchValue(-1)
    }
    
    this.domainName = ""
    this.SectionName = ""
    this.ParticularsName = ""
    this.KRAName = ""
    this.SkillName = ""
    this.ObjectiveName = ""
    
    
  }
  public KRALightBox(update) {
    document.getElementById("myModal1").style.display = "block";
    if (update == 0) {
      document.getElementById("lightboxSaveKRA").style.display = "none";
      document.getElementById("lightboxUpdateKRA").style.display = "inline-block";
    }
    else{
      document.getElementById("lightboxSaveKRA").style.display = "inline-block";
      document.getElementById("lightboxUpdateKRA").style.display = "none";
      this.form.get('DomainName').patchValue("");
      this.form.get('DomainId').patchValue(-1)
      this.form.get('SectionId').patchValue(-1)
      this.form.get('SectionName').patchValue("")
      this.form.get('JobProfileDetailParticulars').patchValue("")
      this.form.get('ParticularId').patchValue(-1)
      this.form.get('KRAName').patchValue("")
      this.form.get('KRAId').patchValue(-1)
      this.form.get('SkillName').patchValue("")
      this.form.get('SkillId').patchValue(-1)
      this.form.get('ObjectiveName').patchValue("")
      this.form.get('ObjectiveId').patchValue(-1)
    }
    this.domainName = ""
    this.SectionName = ""
    this.ParticularsName = ""
    this.KRAName = ""
    this.SkillName = ""
    this.ObjectiveName = ""
  }
  public RequirementsLightBox(update) {
    document.getElementById("myModal2").style.display = "block";
    if(update==0){
      document.getElementById("lightboxSaveRequirement").style.display = "none";
      document.getElementById("lightboxUpdateRequirement").style.display = "inline-block";
    }
    else{
      document.getElementById("lightboxSaveRequirement").style.display = "inline-block";
      document.getElementById("lightboxUpdateRequirement").style.display = "none";
      this.form.get('DomainName').patchValue("");
      this.form.get('DomainId').patchValue(-1)
      this.form.get('SectionId').patchValue(-1)
      this.form.get('SectionName').patchValue("")
      this.form.get('JobProfileDetailParticulars').patchValue("")
      this.form.get('ParticularId').patchValue(-1)
      this.form.get('KRAName').patchValue("")
      this.form.get('KRAId').patchValue(-1)
      this.form.get('SkillName').patchValue("")
      this.form.get('SkillId').patchValue(-1)
      this.form.get('ObjectiveName').patchValue("")
      this.form.get('ObjectiveId').patchValue(-1)
    }
    this.domainName = ""
    this.SectionName = ""
    this.ParticularsName = ""
    this.KRAName = ""
    this.SkillName = ""
    this.ObjectiveName = ""
  }
  public SkillLightBox(update) {
    document.getElementById("myModal3").style.display = "block";
    if(update==0){
      document.getElementById("lightboxSaveSkill").style.display = "none";
      document.getElementById("lightboxUpdateSkill").style.display = "inline-block";
    }
    else{
      document.getElementById("lightboxSaveSkill").style.display = "inline-block";
      document.getElementById("lightboxUpdateSkill").style.display = "none";
      this.form.get('DomainName').patchValue("");
      this.form.get('DomainId').patchValue(-1)
      this.form.get('SectionId').patchValue(-1)
      this.form.get('SectionName').patchValue("")
      this.form.get('JobProfileDetailParticulars').patchValue("")
      this.form.get('ParticularId').patchValue(-1)
      this.form.get('KRAName').patchValue("")
      this.form.get('KRAId').patchValue(-1)
      this.form.get('SkillName').patchValue("")
      this.form.get('SkillId').patchValue(-1)
      this.form.get('ObjectiveName').patchValue("")
      this.form.get('ObjectiveId').patchValue(-1)
    }
    this.domainName = ""
    this.SectionName = ""
    this.ParticularsName = ""
    this.KRAName = ""
    this.SkillName = ""
    this.ObjectiveName = ""
  }


  public RADclose() {
    document.getElementById("myModal").style.display = "none";
    this.domainName = ""
    this.SectionName = ""
    this.ParticularsName = ""
    this.KRAName = ""
    this.SkillName = ""
    this.ObjectiveName = ""
  }
  public KRAClose() {
    document.getElementById("myModal1").style.display = "none";
    this.domainName = ""
    this.SectionName = ""
    this.ParticularsName = ""
    this.KRAName = ""
    this.SkillName = ""
    this.ObjectiveName = ""
  }
  public Requirementsclose() {
    document.getElementById("myModal2").style.display = "none";
    this.domainName = ""
    this.SectionName = ""
    this.ParticularsName = ""
    this.KRAName = ""
    this.SkillName = ""
    this.ObjectiveName = ""
  }
  public Skillclose() {
    document.getElementById("myModal3").style.display = "none";
    this.domainName = ""
    this.SectionName = ""
    this.ParticularsName = ""
    this.KRAName = ""
    this.SkillName = ""
    this.ObjectiveName = ""
  }

 
  public GetDomainValue(event: any) {
    console.log("Domainevent", event)
    this.domainId = event.Selected.Id
    this.domainName = event.Selected.Name;
    console.log("DomainName", this.domainName = event.Selected.Name)
  }
  public GetSectionValue(event: any) {
    this.SectionId = event.Selected.Id
    this.SectionName = event.Selected.Name
  }
  public GetParticularsValue(event: any) {
    console.log("GetParticularsValue",event)
    this.ParticularsId = event.Selected.Particulars
    this.ParticularsName = event.Selected.Name
  }
  public GetKRAValue(event: any) {
    this.KRAId = event.Selected.Id
    this.KRAName = event.Selected.Name
  }
  public GetSkillValue(event: any) {
    this.SkillId = event.Selected.Id
    this.SkillName = event.Selected.Name
  }
  public GetObjectiveValue(event: any) {
    this.ObjectiveId = event.Selected.Id
    this.ObjectiveName = event.Selected.Name
  }


  public ClearValue() {
    this.form.get('DomainName').patchValue("");
    this.form.get('DomainId').patchValue(-1)
    this.form.get('SectionId').patchValue(-1)
    this.form.get('SectionName').patchValue("")
    this.form.get('JobProfileDetailParticulars').patchValue("")
    this.form.get('ParticularId').patchValue(-1)
    this.form.get('KRAName').patchValue("")
    this.form.get('KRAId').patchValue(-1)
    this.form.get('SkillName').patchValue("")
    this.form.get('SkillId').patchValue(-1)
    this.form.get('ObjectiveName').patchValue("")
    this.form.get('ObjectiveId').patchValue(-1)
    this.domainName = ""
    this.SectionName = ""
    this.ParticularsName = ""
    this.KRAName = ""
    this.SkillName = ""
    this.ObjectiveName = ""
  }
  JPLightBox = HRMSURLS.JobProfileLightBoxSave
  public RADSave() {
    console.log("RADSave")
    var criteria = {
      "JobProfileDetailId": 0,
      "JobProfileId": this.JobProfileId,
      "JobProfileName": this.JobProfileName,
      "JobProfileDetailDetailType": 0,
      "JobProfileDetailSlNo": 1,
      "DomainId": this.domainId,
      "SectionId": this.SectionId,
      "ObjectiveId": this.ObjectiveId,
      "KRAId": this.KRAId,
      "SkillId": this.SkillId,
      "JobProfileDetailParticulars": this.ParticularsId
    }

    console.log("criteria", criteria)

    if (this.domainName == "") {
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Select The Domain",
          heading: 'Info',
        }
      });
    }
    else if (this.SectionName == "") {
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Select The Section",
          heading: 'Info',
        }
      });
    }
    else if (this.ParticularsName == "") {
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Select The Particulars",
          heading: 'Info',
        }
      });
    }
    else {
      console.log("Value Given")
      this.http.httppost(this.JPLightBox, criteria).subscribe((response: any) => {
        console.log("response", response)
        this.dialog.open(DialogBoxComponent, {
          width: '400px',
          data: {
            message: "Detail Saved Successfully",
            heading: 'Info',
          }
        });
        this.ClearValue()
      })
    }


  }
  public KRASave() {
    var criteria = {
      "JobProfileDetailId": 0,
      "JobProfileId": this.JobProfileId,
      "JobProfileName": this.JobProfileName,
      "JobProfileDetailDetailType": 1,
      "JobProfileDetailSlNo": 1,
      "DomainId": this.domainId,
      "SectionId": this.SectionId,
      "ObjectiveId": this.ObjectiveId,
      "KRAId": this.KRAId,
      "SkillId": this.SkillId,
      "JobProfileDetailParticulars": this.ParticularsId
    }
    console.log("criteria", criteria)

    if (this.domainName == "") {
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Select The Domain",
          heading: 'Info',
        }
      });
    }
    else if (this.SectionName == "") {
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Select The Section",
          heading: 'Info',
        }
      });
    }
    else if (this.ParticularsName == "") {
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Select The Particulars",
          heading: 'Info',
        }
      });
    }
    else if (this.ObjectiveName == "") {
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Select The Objective",
          heading: 'Info',
        }
      });
    }
    else if (this.KRAName == "") {
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Select The KRA",
          heading: 'Info',
        }
      });
    }
    else {
      console.log("Value Given")
      this.http.httppost(this.JPLightBox, criteria).subscribe((response: any) => {
        console.log("response", response)
        this.dialog.open(DialogBoxComponent, {
          width: '400px',
          data: {
            message: "Detail Saved Successfully",
            heading: 'Info',
          }
        });
        this.ClearValue()
      })
    }

  }

  public RequirementSave() {
    console.log("requirement")
    var criteria = {
      "JobProfileDetailId": 0,
      "JobProfileId": this.JobProfileId,
      "JobProfileName": this.JobProfileName,
      "JobProfileDetailDetailType": 2,
      "JobProfileDetailSlNo": 1,
      "DomainId": this.domainId,
      "SectionId": this.SectionId,
      "ObjectiveId": this.ObjectiveId,
      "KRAId": this.KRAId,
      "SkillId": this.SkillId,
      "JobProfileDetailParticulars": this.ParticularsId
    }
    console.log("criteria", criteria)
    if (this.domainName == "") {
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Select The Domain",
          heading: 'Info',
        }
      });
    }
    else if (this.SectionName == "") {
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Select The Section",
          heading: 'Info',
        }
      });
    }
    else if (this.ParticularsName == "") {
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Select The Particulars",
          heading: 'Info',
        }
      });
    }
    else {
      console.log("Value Given")
      this.http.httppost(this.JPLightBox, criteria).subscribe((response: any) => {
        console.log("response", response)
        this.dialog.open(DialogBoxComponent, {
          width: '400px',
          data: {
            message: "Detail Saved Successfully",
            heading: 'Info',
          }
        });
        this.ClearValue()
      })
    }

  }

  public SkillsSave() {
    console.log("SkillsSave")
    var criteria = {
      "JobProfileDetailId": 0,
      "JobProfileId": this.JobProfileId,
      "JobProfileName": this.JobProfileName,
      "JobProfileDetailDetailType": 3,
      "JobProfileDetailSlNo": 1,
      "DomainId": this.domainId,
      "SectionId": this.SectionId,
      "ObjectiveId": this.ObjectiveId,
      "KRAId": this.KRAId,
      "SkillId": this.SkillId,
      "JobProfileDetailParticulars": this.ParticularsId
    }
    console.log("criteria", criteria)
    if (this.domainName == "") {
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Select The Domain",
          heading: 'Info',
        }
      });
    }
    else if (this.SectionName == "") {
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Select The Section",
          heading: 'Info',
        }
      });
    }
    else if (this.SkillName == "") {
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Select The Skill",
          heading: 'Info',
        }
      });
    }
    else if (this.ParticularsName == "") {
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Please Select The Particulars",
          heading: 'Info',
        }
      });
    }
    else {
      this.http.httppost(this.JPLightBox, criteria).subscribe((response: any) => {
        console.log("response", response)
        this.dialog.open(DialogBoxComponent, {
          width: '400px',
          data: {
            message: "Detail Saved Successfully",
            heading: 'Info',
          }
        });
        this.ClearValue()
      })
    }

  }

  public EditRAD(event: any) {
    console.log("GridEdidOption", event)
    console.log("event.DomainName", event.value.DomainName)
    this.form.get('DomainName').patchValue(event.value.DomainName);
    this.form.get('SectionName').patchValue(event.value.SectionName);
    this.form.get('JobProfileDetailParticulars').patchValue(event.value.JobProfileDetailParticulars);
    this.ParticularsId = event.value.JobProfileDetailParticulars
    // this.form.get('JobProfileDetailDetailType').patchValue(event.value.JobProfileDetailDetailType)
    this.JobProfileDetailDetailType = event.value.JobProfileDetailDetailType
    this.JobProfileDetailId = event.value.JobProfileDetailId
    let ans = 0
    this.RADLightBox(ans);
    this.GridIndex = event.index
    console.log("GridIndex",this.GridIndex)

  }

  public EditKRA(event: any){
    console.log("ObjectiveKRA",event)
    let ans = 0
    this.KRALightBox(ans);
    this.form.get('DomainName').patchValue(event.value.DomainName);
    this.form.get('DomainId').patchValue(event.value.DomainId)
    this.form.get('SectionName').patchValue(event.value.SectionName);
    this.form.get('SectionId').patchValue(event.value.SectionId)
    this.form.get('JobProfileDetailParticulars').patchValue(event.value.JobProfileDetailParticulars);
    this.form.get('KRAName').patchValue(event.value.KRAName)
    this.form.get('KRAId').patchValue(event.value.KRAId)
    this.form.get('ObjectiveName').patchValue(event.value.ObjectiveName)
    this.form.get('ObjectiveId').patchValue(event.value.ObjectiveId)
    this.ParticularsId = event.value.JobProfileDetailParticulars
    this.JobProfileDetailDetailType = event.value.JobProfileDetailDetailType
    this.JobProfileDetailId = event.value.JobProfileDetailId
    this.GridIndex = event.index
  }
  public EditRequiremants(event: any){
    let ans = 0
    this.RequirementsLightBox(ans);
    this.form.get('DomainName').patchValue(event.value.DomainName);
    this.form.get('DomainId').patchValue(event.value.DomainId)
    this.form.get('SectionName').patchValue(event.value.SectionName);
    this.form.get('SectionId').patchValue(event.value.SectionId)
    this.form.get('JobProfileDetailParticulars').patchValue(event.value.JobProfileDetailParticulars);
    this.ParticularsId = event.value.JobProfileDetailParticulars
    this.JobProfileDetailDetailType = event.value.JobProfileDetailDetailType
    this.JobProfileDetailId = event.value.JobProfileDetailId
    this.GridIndex = event.index
  }
  public EditSkill(event: any){
    let ans = 0
    this.SkillLightBox(ans);
    this.form.get('DomainName').patchValue(event.value.DomainName);
    this.form.get('DomainId').patchValue(event.value.DomainId)
    this.form.get('SectionName').patchValue(event.value.SectionName);
    this.form.get('SectionId').patchValue(event.value.SectionId)
    this.form.get('JobProfileDetailParticulars').patchValue(event.value.JobProfileDetailParticulars);
    this.form.get('SkillName').patchValue(event.value.SkillName);
    this.form.get('SkillId').patchValue(event.value.SkillId)

    this.ParticularsId = event.value.JobProfileDetailParticulars
    this.JobProfileDetailDetailType = event.value.JobProfileDetailDetailType
    this.JobProfileDetailId = event.value.JobProfileDetailId
    this.GridIndex = event.index
  }

  public RADUpdateValue() {
    console.log("Updating");
    let DublicateCriteria = {
      "JobProfileDetailId": this.JobProfileDetailId,
      "JobProfileId": this.JobProfileId,
      "JobProfileName": this.JobProfileName,
      "JobProfileDetailDetailType":  this.JobProfileDetailDetailType,
      "JobProfileDetailSlNo": 1,
      "DomainId": this.domainId,
      "SectionId": this.SectionId,
      "ObjectiveId": this.ObjectiveId,
      "KRAId": this.KRAId,
      "SkillId": this.SkillId,
      "JobProfileDetailParticulars": this.ParticularsId
    }

    console.log("DublicateCriteria",DublicateCriteria);
    this.http.httppost(this.JPLightBox, DublicateCriteria).subscribe((response: any) => {
      console.log("response", response)
      this.dialog.open(DialogBoxComponent, {
        width: '400px',
        data: {
          message: "Detail Updated Successfully",
          heading: 'Info',
        }
      });
      this.EditArray = !this.EditArray
      console.log("this.EditArray = !this.EditArray",this.EditArray = !this.EditArray)
      this.ClearValue()
      this.RADclose()
      this.JobProfileLibraryFillFunction(DublicateCriteria.JobProfileId)
    })
  }

 

  public JobProfileLibraryFillFunction(SelectedPicklistData: any): void {
    if (SelectedPicklistData) {
      this.gbps.dataService.getData(SelectedPicklistData).subscribe(JobProfile => {
        console.log("JobProfile", JobProfile)
        this.EditArray = !this.EditArray
        this.JobProfileId = JobProfile.JobProfileId
        this.JobProfileName = JobProfile.JobProfileName
        let TabItemArray = JobProfile.JobProfileDetailArray
        console.log("TabItemArray", TabItemArray)
        let DetailArrayvalue =  []
        let DetailArrayvalue1 = []
        let DetailArrayvalue2 = []
        let DetailArrayvalue3 = []
        for (let item of TabItemArray) {
          console.log("item", item)
          if (item.JobProfileDetailDetailType == 0) {
            DetailArrayvalue.push(item)
          }
          else if (item.JobProfileDetailDetailType == 1) {
            DetailArrayvalue1.push(item)
          }
          else if (item.JobProfileDetailDetailType == 2) {
            DetailArrayvalue2.push(item)
          }
          else if (item.JobProfileDetailDetailType == 3) {
            DetailArrayvalue3.push(item)
          }
        }
        console.log("DetailArrayvalue", DetailArrayvalue)
        console.log("DetailArrayvalue1", DetailArrayvalue1)
        console.log("DetailArrayvalue2", DetailArrayvalue2)
        console.log("DetailArrayvalue3", DetailArrayvalue3)

        this.form.get('JobProfileDetailArray1').patchValue(DetailArrayvalue)
        console.log("firstArray",this.form.get('JobProfileDetailArray1'))
        this.form.get('JobProfileDetailArray2').patchValue(DetailArrayvalue1)
        console.log("SecondArray",this.form.get('JobProfileDetailArray2'))
        this.form.get('JobProfileArray3').patchValue(DetailArrayvalue2)
        this.form.get('JobProfileArray4').patchValue(DetailArrayvalue3)
        this.form.patchValue(JobProfile);
        this.isButtonDisabled = false
        this.form.get('JobProfileDetailArray').patchValue([]);
      })
    }
  }

  public GetJobTittle(event: any) {
    console.log("GetJobTittleevent", event);
    let linkids = event.id.split(',')
    let linkvalues = event.value.split(',')
    console.log("linkids", linkids)
    console.log("linkvalues", linkvalues)
    for (let i = 0; i < linkids.length; i++) {
      this.form.get(linkids[i])?.setValue(event.Selected[linkvalues[i]])
    }
  }

  public JobProfileLibraryPatchValue(SelectedPicklistDatas: any): void {
    this.form.get(SelectedPicklistDatas.id).patchValue(SelectedPicklistDatas.SelectedData)
  }
}


export function getThisDBDataService(http: GBHttpService, url: string, saveurl: string, IdField: string): GBBaseDBDataService<IJobProfileLibrary> {
  const dbds: GBBaseDBDataService<IJobProfileLibrary> = new GBBaseDBDataService<IJobProfileLibrary>(http);
  dbds.endPoint = url;
  dbds.saveurl = saveurl
  return dbds;
}
export function getThisPageService(store: Store, dataService: GBBaseDataService<IJobProfileLibrary>, dbDataService: GBBaseDBDataService<IJobProfileLibrary>, fb: FormBuilder, gbhttp: GBHttpService, activeroute: ActivatedRoute, router: Router): GBDataPageService<IJobProfileLibrary> {
  return new GBDataPageService<IJobProfileLibrary>(store, dataService, dbDataService, fb, gbhttp, activeroute, router);
}


