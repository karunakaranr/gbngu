import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Entitylookupservice } from 'features/entitylookup/services/entitylookup/entitylookup.service';
import { EntityDateFormatteromponent } from '../entityDateFormatter/entityDateFormatter.component';
import { GridOptions } from 'ag-grid-community';
import { IPicklistObj } from 'features/entitylookup/model/picklist.model';
@Component({
  selector: 'app-entitygrid',
  templateUrl: './entitygrid.component.html',
  styleUrls: ['./entitygrid.component.scss']
})
export class EntitygridComponent implements OnInit {
  rowData;
  columnDefs;
  title;
  showbutton;
  color;
  icons;
  dependpicklist;
  picklistsearchfields: string;
  Initailgridlength: any;
  picklisturl: string;
  searchText: string;
  multiples: string;
  maxlength: number;
  gridindex: number;
  gridOptions: GridOptions;
  PicklistTitle: string;
  SelectionId:number | string
  picklistobj: IPicklistObj;
  private gridApi;
  private gridColumnApi;
  @ViewChild("myInput") myInputField: ElementRef;
  constructor(public service: Entitylookupservice, private dialogRef: MatDialogRef<EntitygridComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
    this.rowData = this.data.rowData;
    this.columnDefs = this.data.columnDefs;
    this.title = this.data.title;
    this.Initailgridlength = this.data.rowData.length
    this.showbutton = this.data.button;
    this.picklistsearchfields = this.data.picklistsearchfields;
    this.dependpicklist = this.data.dependpicklist;
    this.picklisturl = this.data.picklisturl,
    this.maxlength = this.data.maxlength
    this.gridindex = this.data.gridindex
    this.PicklistTitle = this.data.displaytitle
    this.SelectionId = this.data.selectionid
    console.log("Picklist Data:", this.rowData)
    if (this.data.multiple == false) {
      this.multiples = 'single'
      if(!this.data.IsFilter){
        this.searchText = this.data.inputvalue
      }
    }
    else {
      this.multiples = 'multiple'
    }
    const inputField = document.getElementById('myInput');
    inputField.addEventListener('keyup', (event: KeyboardEvent) => {
      if (event.key === 'Enter') {
        this.OnServiceCall();
      }
    })
    this.icons = {
      sortAscending: '<i class="fa fa-arrow-down "/>',
      sortDescending: '<i class="fa fa-arrow-up"/>',
    };
  }

  public OnServiceCall(): void {
    this.picklistobj = {
      'picklistName': this.title,
      'searcheithercoloumn': this.picklistsearchfields,
      'url': this.picklisturl,
      'searcheither': true,
      'dependpicklistvalue': this.dependpicklist,
      'isfilter': this.data.IsFilter,
      'ismultiple':this.data.multiple
    }
    let objvalue = this.picklistobj;
    this.service.Dataservice(objvalue, this.gridindex, this.searchText).subscribe(response => {
      if (response.length > 0) {
        this.rowData = response;
      }
      else {
        if (this.searchText != '' && (this.data.addnew || this.data.addnew == undefined)) {
          this.dialogRef.close(this.searchText);
        }
      }
    })
  }

  public onClose(): void {
    if(this.gridApi == undefined){
      this.dialogRef.close()
    } else {
      if(this.gridApi.getSelectedNodes().length > 0){
        let selectedNodes = this.gridApi.getSelectedNodes();
        let selectedData = selectedNodes.map(node => node.data);
        this.dialogRef.close(selectedData);
      } else {
        if(this.searchText == null || this.searchText == ""){
          this.dialogRef.close("");
        } else {
          if(this.data.addnew){
            this.dialogRef.close(this.searchText);
          } else {
            this.dialogRef.close()
          }
        }
      }
    }
  }

  public onGridReady(params): void {
    this.gridOptions = params;
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.rowData = this.data.rowData;
    console.log("RowData:",this.data.inputid)
    if (this.data.multiple == false) {
      if(!this.data.IsFilter){
        this.searchText = this.data.inputvalue
        this.quicksearch(this.data.inputvalue)
      }
    } else {
      this.gridOptions.api.forEachNode((node) => {
        if(this.data.IsFilter){
          if (this.data.filtervalue.includes(node.data[this.SelectionId])) {
            node.setSelected(true);
          }
        }else {
          if (this.data.inputid.includes(node.data[this.SelectionId])) {
            node.setSelected(true);
          }
        }
      });
    }
  }

  public quicksearch(event: any): void {
    this.gridApi.setQuickFilter(event);
    if(this.data.addnew == true && event != null){
      let data = event + "(NEW)"
      if (event != null) {
        if (event.length == 0) {
          this.data.rowData.splice(0, 1)
          this.gridApi.setRowData(this.data.rowData)
        }
      }
      if (!event.trim()) {
        return;
      }
      const exists = this.data.rowData.some(row => row[this.PicklistTitle].toString() === event);
  
      if (this.data.rowData.length > this.Initailgridlength) {
        if (!exists) {
          let emptyObject: any = {};
          this.columnDefs.forEach(item => {
            emptyObject[item.field] = data;
          });
          this.data.rowData[0] = emptyObject;
          this.gridOptions.api.setRowData(this.data.rowData);
        } else {
          this.data.rowData.splice(0, 1);
          this.gridOptions.api.setRowData(this.data.rowData);
        }
      }
      else {
        if (!exists) {
          let emptyObject: any = {};
          this.columnDefs.forEach(item => {
            emptyObject[item.field] = data;
          });
          // this.gridOptions.rowData[0] = emptyObject;
          // const addNewData = { id: event, value: event };
          this.data.rowData.unshift(emptyObject);
          this.gridOptions.api.setRowData(this.data.rowData);
        } else {
        }
      }  
    }

  }

  public selecteddata(): void {
    let selectedNodes = this.gridApi.getSelectedNodes();
    let selectedData = selectedNodes.map(node => node.data);
    console.log("selectedData:",selectedData)
    if(selectedData[0]!=undefined && (selectedData[0][this.PicklistTitle]).toString().includes("(NEW)")){
      this.dialogRef.close(selectedData[0][this.PicklistTitle].replace("(NEW)",""));
    } else {
      this.dialogRef.close(selectedData)
    }
  }

  public select(): void {

  }

  public DeselectSelectedCells(): void {
    this.searchText = "";
    this.quicksearch("")
    this.myInputField.nativeElement.focus();
    this.gridApi.deselectAll();
  }
}

