export interface Ientitylookup {
    picklistid: string;
    bindfield: string;
    bindlabel: string;
    showcard: boolean;
    highlightterm: string;
    displaycolumns: IDisplaycolumns;
}
export interface IDisplaycolumns {
    label: string;
    field: string;
}

export interface IEntityConfig {
    usePicklistService: boolean;
    picklistid?: string;
    bindfield: string;
    bindlabel: string;
    placeholder:string;
    fieldwidth?:string;
    expandable?: boolean;
    multiselect?: boolean;
    highlightterm?: boolean;
    AllowAdvanceFinder?: boolean;
    AllowCreateNew?: boolean;
    AllowRecent?: boolean;
    AllowSwitchViews?: boolean;
    API?: IEntityAPIModel;
    displaycolumns: IDisplaycolumns[];
    dialogconfig?: Idialogconfig;
    griddata?: Igriddata;
}
export interface IEntityAPIModel {
    PicklistCode: string;
    PicklistDisplayFieldName: string;
    PicklistDisplayFieldTextBox: string;
    PicklistHeight: number;
    PicklistIsMultiselect: number;
    PicklistMinimunNumber: number;
    PicklistName: string;
    PicklistNumberOfSelection: number;
    PicklistRemarks: string;
    PicklistScreenOperationMode: number;
    PicklistSearchFields: string;
    PicklistSearchableFields: string;
    PicklistSelectionFieldName: string;
    PicklistSortOrder: number;
    PicklistSourceType: number;
    PicklistStatus: number;
    PicklistTitle: string;
    PicklistUri: string;
    PicklistVersion: number;
    PicklistWidth: number;
    PicklistOutputDtos: IPicklistOutputDtos[];
}
export interface IPicklistOutputDtos {
    PicklistOutputId: number;
    PicklistId: number;
    PicklistCode: string;
    PicklistName: string;
    PicklistOutputSlNo: number;
    PicklistOutputFieldName: string;
    PicklistOutputIsVisible: number;
    PicklistOutputHeaderText: string;
    PicklistOutputFieldSize: number;
    PicklistOutputIsFreezingReq: number;
    PicklistOutputIsHeaderWrappingReq: number;
    PicklistOutputIsDataWrappingReq: number;
}
export interface Idialogconfig {
    disableClose: boolean,
    hasBackdrop: boolean,
    autoFocus: boolean,
    backdropClass: string,
    width: string,
    height: string,
    position: {
        top: string,
        left: string
    }
}
export interface Igriddata {
    columnDefs: Igridcolumndef[],
    rowData?: [];
}
export interface Igridcolumndef{
    field:string;
    sortable?:boolean;
    filter?:string;
    checkboxSelection?:boolean;
    editable?:boolean;
}
export interface Igriddefaulcolms{
    filter?:string;
}
