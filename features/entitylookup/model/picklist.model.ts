export class IPicklist {
    PicklistId: string;
    PicklistCode: string;
    PicklistDisplayFieldName: string;
    PicklistDisplayFieldTextBox: string;
    PicklistHeight: number;
    PicklistIsMultiselect: number;
    PicklistMinimunNumber: number;
    PicklistName: string;
    PicklistNumberOfSelection: number;
    PicklistRemarks: string;
    PicklistScreenOperationMode: number;
    PicklistSearchFields: string;
    PicklistSearchableFields: string;
    PicklistSelectionFieldName: string;
    PicklistSortOrder: number;
    PicklistSourceType: number;
    PicklistStatus: number;
    PicklistTitle: string;
    PicklistUri: string;
    PicklistVersion: number;
    PicklistWidth: number;

}


export class IPicklistColumn {
    columnSlno: number;
    entitymemberid: number;
    ColumnName: string;
    ColumnDataType: number;   // enum value  eg. Number,Date,String etc
    DisplayTitle: string;
    Displayslno: number;
    DisplayWidth: number;
}

export class IcolumnDefs {
    headerName:string;
    field: string;
    sortable: boolean;
    checkboxSelection: boolean;
    width?:number;
    cellRendererFramework?:any;
}

export interface IPicklistObj {
    picklistName: string
    searcheithercoloumn: string
    url: string
    searcheither: boolean
    dependpicklistvalue: IDependentPicklist | string;
    isfilter?: boolean
    ismultiple?:boolean
  }

  export interface IDependentPicklist {
    DependPicklistId: string
    DependPicklistName: string
    Operationtpyevalue: number
    values: string | number
  }
  

  export interface IConfig {
    expandable: boolean
    displaycolumns: Displaycolumns[]
    bindlabel: string
    AllowAdvanceFinder: boolean
    griddata: Griddata
  }
  
  export interface Displaycolumns {
    label: string
    field: string
  }

  export interface Griddata {
    columnDefs: IcolumnDefs[]
  }
  
