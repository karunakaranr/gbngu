import { Injectable } from '@angular/core';
import { Entitylookupdbservice} from './../../dbservices/entitylookup/entitylookupdb.service';
import { IPicklistObj } from 'features/entitylookup/model/picklist.model'; 


@Injectable({
  providedIn: 'root'
})
export class Entitylookupservice {
  constructor( public DBservice : Entitylookupdbservice) { }

public PicklistService(id){
  return this.DBservice.picklist(id);
}

public defaultvalue(obj,GridIndex, criteria){
  return this.DBservice.PicklistData(obj,GridIndex, criteria);
}
 
public Dataservice(obj : IPicklistObj,GridIndex:number,criteria:string){
  return this.DBservice.PicklistData(obj,GridIndex, criteria);
}
  }




