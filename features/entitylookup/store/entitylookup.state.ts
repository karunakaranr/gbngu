import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { GbentityDTO , GblistdataDTO , Entitylookupdependpicklist} from './entitylookup.action';

export class EntityStateModel {
  entitydata: string;
  Listdata: string;
  sentitylookupdependpicklist : any
}

@State<EntityStateModel>({
  name: 'Entity',
  defaults: {
    entitydata: null,
    Listdata: null,
    sentitylookupdependpicklist:null
  },
})

@Injectable()
export class EntityState {
  @Selector() static Entity(state: EntityStateModel) {
    return state.entitydata;
  }


  @Action(GbentityDTO)
  GbEntityDTO(context: StateContext<EntityStateModel>, Ientity: GbentityDTO) {
    const state = context.getState();
    context.setState({ ...state, entitydata: Ientity.DEntity });
  }



  @Selector() static listdata(state: EntityStateModel) {
    return state.Listdata;
  }

  @Selector() static Entitylookupdependpicklistdata(state: EntityStateModel) {
    return state.sentitylookupdependpicklist;
  }



  @Action(GblistdataDTO)
  GbListdataDTO(context: StateContext<EntityStateModel>, IListdata: GblistdataDTO) {
    const state = context.getState();
    context.setState({ ...state, Listdata: IListdata.DListdata });
  }

  @Action(Entitylookupdependpicklist)
  Entitylookupdependpicklist(context: StateContext<EntityStateModel>, IEntitylookupdependpicklist: Entitylookupdependpicklist) {
    const state = context.getState();
    context.setState({ ...state, sentitylookupdependpicklist: IEntitylookupdependpicklist.entitylookupdependpicklist });
  }
}