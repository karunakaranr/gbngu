import { ChangeDetectorRef } from "@angular/core";

const storeSliceName = 'Entity';
const storelistName = 'Listdata';


export class GbentityDTO {
  static readonly type = '[' + storeSliceName + '] GbentityDTO';
  constructor(public DEntity: string, public ref: ChangeDetectorRef = null) {
  }
}


export class GblistdataDTO {
  static readonly type = '[' + storelistName + '] GblistdataDTO';
  constructor(public DListdata: string) {
  }
}



export class Entitylookupdependpicklist {
  static readonly type = '[' + storelistName + '] Entitylookupdependpicklist';
  constructor(public entitylookupdependpicklist: any) {
  }
}
