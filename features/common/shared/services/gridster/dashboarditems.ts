import { DatewiseminComponent } from "features/erpmodules/inventory/screens/Register/DateWise/Datewise Min and Inward/Datewisemin.component";
import { UOMlistComponent } from "features/erpmodules/inventory/screens/UOM/UOMList/uomlist.component";

export interface DashboardItemModel {
  portletName: string;
  component: unknown;
}
const dashboardItems = [];

export class DashboardItems {
  static push(di: DashboardItemModel) {
    dashboardItems.push(di);
  }

  static getComponent(name: string) {
    // console.log("Component Name:",name)
    dashboardItems.push({ portletName:"DatewiseminComponent", component: DatewiseminComponent});
    return dashboardItems.filter((x) => x.portletName === name)[0].component;
  }
}
