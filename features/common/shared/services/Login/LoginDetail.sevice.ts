import { Injectable } from '@angular/core';
import { LoginDetailDbService } from './../../dbservices/Login/LoginDetailDB.service';
@Injectable({
  providedIn: 'root'
})
export class LoginDetail {
  constructor(public dbservice: LoginDetailDbService) { }

  public LoginDetailview(UserId : string){               
    return this.dbservice.logindetailservice(UserId);
  }
}