import { Injectable } from '@angular/core';
import { GBBaseService } from '@goodbooks/gbdata';



import { ILoginDetail } from './../../models/Login/LoginDetail';
import { Authkeyservice } from './Authkey.service';


@Injectable({
  providedIn: 'root',
})
export class Loginservice extends GBBaseService {

  constructor(public Authkey: Authkeyservice) {
    super();
   }

  public LoginService(userdetails: ILoginDetail,uri) {
    return this.Authkey.Authkeyservice(userdetails,uri);
  }
}
