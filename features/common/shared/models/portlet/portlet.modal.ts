export class IPortlet
{
  UserVsPortletId: Number;
	UserId: Number;
  PageId: string;
	PageCode: String;
  PageName: string;
	PortletId: Number;
	PortletCode: String;
  PortletName: string;

}
