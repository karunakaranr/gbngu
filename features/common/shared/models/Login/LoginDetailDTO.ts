export interface ILoginDetailDTO{
	UserId: number,
	UserCode: string,
	UserFullName: string,
	UserLoginName: string,
	LastSuccessLoginOn: string,
	LastFailureAttemptOn: string,
	NoofNewTasks: number
	NoofNewInbox: number
	OUID: number
	OUName: string,
	OULogoURI: string,
	PartyBranchId: number,
	PartyBranchName: string,
	PartyBranchCityName: string,
	WorkRegion: string,
	WrokGeoCode: string,
	ImageURL: string,
	WorkPeriodId: number,
	WorkPeriodFromDate: string,
	WorkPeriodToDate: string,
	Logo1: string,
	Logo2: string,
	StoreName: string,
	StoreId: number,
	ApplicablePeriodFrom: string
}