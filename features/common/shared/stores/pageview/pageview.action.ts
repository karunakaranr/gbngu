import {PageViewModal} from '@ToolsModal/PageView/pageview.modal';

export class GetPortlet
{
  static readonly type = '[PageViewModal] Get';
}

export class saveuserpage
{
  static readonly type = '[page] Add';
  constructor(public saveuserportlet: PageViewModal) {
    }
}

export class RemoveItem
{
  static readonly type = '[Main Page] DeleteItem';
  userpagedata: any;
  constructor(public readonly ItemId: number) { }
}
