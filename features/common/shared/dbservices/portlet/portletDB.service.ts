import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {  IDashboard } from './../../models/dashboard/dashboard.model';

const saveuserpage ='apiu/prox/fws/UserVsPortlet.svc/"';


@Injectable({
  providedIn: 'root'
})
export class PortletdbService {
  public loginDTO;
  public userid;
  public roleid;
  sampledata   = 'assets/mockdata/_general/page.json';


  constructor(private httpclient: HttpClient)
  {}

  public ViewPortlet():Observable<IDashboard[]>
  {
    return this.httpclient.get<IDashboard[]>(this.sampledata);
  }
}
