import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GbchartComponent } from './gbchart.component';

describe('ChartViewComponent', () => {
  let component: GbchartComponent;
  let fixture: ComponentFixture<GbchartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GbchartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbchartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
