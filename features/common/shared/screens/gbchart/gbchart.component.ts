import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, Output, TemplateRef } from '@angular/core';
import { Chartdata, Chartsetting } from '../../models/gbchart/chart.model';


@Component({
  selector: 'goodbooks-gbchart',
  templateUrl: './gbchart.component.html',
  styleUrls: ['./gbchart.component.scss']
})
export class GbchartComponent {
  @Input() chartTemplate : TemplateRef<HTMLElement>;
  @Input() chartdata : Chartdata[];
  @Input() chartsetting : Chartsetting;
  clickedItem;
  @Output() typetoemit : EventEmitter<string>= new EventEmitter<string>();
  item: string;
  selectedObject;
  constructor( private http : HttpClient ) { }
  ngOnInit(){
    //alert(this.chartdata);
  }
onClicked(){
  this.typetoemit.emit(JSON.stringify(this.chartsetting[0].chartType) + " was clicked");
  this.item ="was clicked: "+ JSON.stringify(this.chartsetting[0].chartType);
 }
 onSelect(data): void {
  this.clickedItem ="Item clicked: "+ JSON.stringify(data);
 }
 
 onActivate(data): void {
  console.log('Activate', JSON.parse(JSON.stringify(data)));
 }
 
 onDeactivate(data): void {
  console.log('Deactivate', JSON.parse(JSON.stringify(data)));
 }
 
 handleChange(Type) {
  this.selectedObject = (this.ChartTypes[Type])
 }
 ChartTypes = [
  {Type: 'Choose Chart' },
  {Type: 'Area Chart' },
  {Type: 'Line Chart'  },
  {Type: 'Horizontal Barchart' },
  {Type: 'Vertical Barchart'},
  {Type: 'Tree Chart' },
  {Type: 'Advanced Pie Chart' },
  {Type: 'Gauge Chart' },
  {Type: 'Horizontal Chart' },
  {Type: 'Vertical Chart' },
  {Type: 'Stacked Vertical Chart' },
  {Type: 'Stacked Horizontal Chart' },
  {Type: 'Pie Chart' },
  {Type: 'Piegrid Chart' },
  {Type: 'Normalized Area Chart' },
  {Type: 'Stacked Area Chart' },
  {Type: 'Bubble Chart' },
  {Type: 'Numbercard Chart' },
  {Type: 'Polar Chart' }
 ];
 
}
