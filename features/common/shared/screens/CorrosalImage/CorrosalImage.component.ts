import { Component, OnInit } from "@angular/core";
const jsonvalue = require('./../../../../../apps/Goodbooks/Goodbooks/src/assets/Carousel_Image/Image.json')
@Component({
    selector: 'app-corosalimage',
    templateUrl: './CorrosalImage.component.html',
    styleUrls: ['./CorrosalImage.component.scss']
})

export class CorrosalImageComponent implements OnInit {

    slides;
    currentIndex = 0;
    async ngOnInit(): Promise<void> {
        let abc = [];
        for (let data of jsonvalue) {
            if (new Date(data.date) >= new Date()) {
                abc.push({ image: `assets/Carousel_Image/Images/${data.name}` })
            }
        }
        this.slides = abc;
        while (true) {

            await this.delay(10000);
            this.prevSlide()
        }
    }

    delay(milliseconds: number) {
        return new Promise(resolve => setTimeout(resolve, milliseconds));
    }

    setCurrentSlideIndex(index) {
        this.currentIndex = index;
    }

    isCurrentSlideIndex(index) {
        return this.currentIndex === index;
    }

    prevSlide() {
        this.currentIndex = (this.currentIndex < this.slides.length - 1) ? ++this.currentIndex : 0;
    }

    nextSlide() {
        this.currentIndex = (this.currentIndex > 0) ? --this.currentIndex : this.slides.length - 1;
    }
}