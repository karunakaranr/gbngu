import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import * as THREE from 'three';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { FontLoader } from 'three/examples/jsm/loaders/FontLoader.js';
import { TextGeometry } from 'three/examples/jsm/geometries/TextGeometry.js';
@Component({
    selector: 'app-textanimation',
    templateUrl: './TextAnimation.component.html',
    styleUrls: ['./TextAnimation.component.scss']
})

export class TextAnimationComponent implements OnInit, OnDestroy {
    @ViewChild('rendererContainer', { static: true }) rendererContainer!: ElementRef;

    constructor() { }

    ngOnInit(): void {
        this.initThreeJs();
    }

    ngOnDestroy(): void {
        window.removeEventListener('resize', this.onWindowResize);
    }

    private async initThreeJs(): Promise<void> {
        const mount = this.rendererContainer.nativeElement;

        const fontLoader = new FontLoader();
        const font = await fontLoader.loadAsync('assets/fonts/droid_sans_mono_regular.typeface.json');

        const maptexture = new THREE.TextureLoader().load('assets/textures/2294472375_24a3b8ef46_o.jpg');
        maptexture.mapping = THREE.EquirectangularReflectionMapping;

        let titleName = 'GoodBooks'
        let titleNameLength = titleName.length

        const textMesh = new THREE.Mesh(
            new TextGeometry(titleName, {
                font: font,
                size: 30,
                height: 10,
                bevelEnabled: true,
                bevelThickness: 2,
                bevelSize: 1,
                bevelSegments: 3
            }),
            new THREE.MeshBasicMaterial({
                color: '#6E3AB6',
                wireframe: false,
                envMap: maptexture,
                opacity: 10,
                transparent: true
            })
        );



        textMesh.position.set(-0.125 * titleNameLength, 0, 0);
        textMesh.scale.set(0.01, 0.01, 0.01);
        textMesh.castShadow = true;
        textMesh.receiveShadow = true;

        const scene = new THREE.Scene();
        scene.add(textMesh);

        // seat axis
        const axes = new THREE.AxesHelper();
        //  scene.add(axes);

        //   scene.background = new THREE.Color(0x000000);

        const ambientLight = new THREE.AmbientLight(0x050505);
        scene.add(ambientLight);

        const directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);
        directionalLight.position.set(0, 100, 100);
        directionalLight.castShadow = true;
        directionalLight.shadow.mapSize.width = 1024;
        directionalLight.shadow.mapSize.height = 1024;
        scene.add(directionalLight);

        const camera = new THREE.PerspectiveCamera(70, mount.offsetWidth / mount.offsetHeight, 0.01, 1000);
        camera.position.set(1, 1, 1);
        camera.lookAt(scene.position);


        const renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.setClearColor(0xffffff, 1.0);
        renderer.setSize(mount.offsetWidth, mount.offsetHeight);
        renderer.shadowMap.enabled = true;

        const orbitControls = new OrbitControls(camera, renderer.domElement);
        orbitControls.maxPolarAngle = Math.PI * 0.5;
        orbitControls.minDistance = 0.1;
        orbitControls.maxDistance = 100;
        orbitControls.autoRotate = true;
        orbitControls.autoRotateSpeed = 1.0;

        renderer.setAnimationLoop(() => {
            orbitControls.update();
            renderer.render(scene, camera);
        });

        mount.appendChild(renderer.domElement);

        window.addEventListener('resize', this.onWindowResize.bind(this));
    }

    private onWindowResize(): void {
        const mount = this.rendererContainer.nativeElement;
        const width = mount.offsetWidth;
        const height = mount.offsetHeight;

        const camera = new THREE.PerspectiveCamera(50, width / height, 0.01, 1000);
        camera.aspect = width / height;
        camera.updateProjectionMatrix();

        const renderer = new THREE.WebGLRenderer({ antialias: true });
        renderer.setSize(width, height);
    }
}