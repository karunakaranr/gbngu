import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { IDashboard } from './../../models/dashboard/dashboard.model';
import { IGridsterProperty } from './../../models/gridster/gridster.modal';
import { Store, Select } from '@ngxs/store';
import { DashboardService } from './../../services/dashboard/dashboard.service';
import { GridsterConfig, GridsterItem, DisplayGrid, GridsterItemComponentInterface } from 'angular-gridster2';
import { GridsterService } from './../../services/gridster/gridster.service';
import { PortletComponent } from '../portlet/portlet.component';
import { DashboardItems } from '../../services/gridster/dashboarditems';
import {LayoutState} from './../../../../layout/store/layout.state';
import { Observable } from 'rxjs';
import { Color } from 'ag-grid-community';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  pagemodal: any = [];
  pageviewmodal: IDashboard[];
  gridproperty: IGridsterProperty = new IGridsterProperty();
  public pagelayout: GridsterItem[] = [];
  private resizeEvent: EventEmitter<any> = new EventEmitter<any>();
  private configureEvent: EventEmitter<boolean> = new EventEmitter<boolean>();
  public pageType;
  public deleteconfig;
  public selectportlet;
  public NonEdit;
  public savepage;
  public edit = true;
  options: GridsterConfig;

  get editable(): GridsterConfig {
    return this.gridster.withEditable;
  }
  get pages(): GridsterItem[] {
    return this.pagelayout;
  }

  constructor(private router: Router, public store: Store,
    public pageservice: DashboardService, public gridster: GridsterService,
    public dialogRef: MatDialogRef<PortletComponent>, public dialog: MatDialog,) { }

  ngOnInit() {
    this.getuserpageportlet();
    this.options = {
      pushItems: true,
      draggable: {
        enabled: true
      },
      resizable: {
        enabled: true
      }
    };
  }

  public getuserpageportlet() {

    this.pageType = this.gridster.withNonEditable;

    this.pageservice.getuserpage().subscribe(Result => {
      console.log("pagemodel",Result)
      this.pagemodal = Result;
      for (let i = 0; i < this.pagemodal.length; i++) {
        let compType = this.pagemodal[i].PortletName;
        const compt = DashboardItems.getComponent(compType);
        this.pagelayout.push({
          cols: this.pagemodal[i].UserVsPortletPortletColumn,
          rows: this.pagemodal[i].UserVsPortletPortletRow,
          x: this.pagemodal[i].X,
          y: this.pagemodal[i].Y,
          componentType: compt,
          name: compType,
          id: this.pagemodal[i].UserVsPortletId

        });
      }

    });
  }

  public editpages() {
    this.edit = false;
    this.deleteconfig = true;
    this.selectportlet = true;
    this.NonEdit = true;
    this.savepage = true;
    this.pageType = this.gridster.withEditable;
  }
  public Noneditpages() {
    this.edit = true;
    this.deleteconfig = false;
    this.selectportlet = false;
    this.NonEdit = false;
    this.savepage = false;
    this.pageType = this.gridster.withNonEditable;
  }

  public changedOptions() {
    this.options.api.optionsChanged();
  }

  public onClick_remove(item): void {
    this.pages.splice(this.pages.indexOf(item), 1);
  }

  public portletList() {
    const dialogRef = this.dialog.open(PortletComponent,
      {
        width: '50vw',
        height: '30vw'
      });
    dialogRef.afterClosed().subscribe(result => {
      let compType = result;
      const compt = DashboardItems.getComponent(compType.PortletName);
      this.pagelayout.push({
        cols: compType.UserVsPortletPortletColumn,
        rows: compType.UserVsPortletPortletRow,
        x: compType.X,
        y: compType.Y,
        componentType: compt,
        name: compType.PortletName,
        id: compType.UserVsPortletId

      });
    });
  }

  SaveChange() {
    console.log(this.pagelayout);
    this.edit = true;
    this.deleteconfig = false;
    this.selectportlet = false;
    this.NonEdit = false;
    this.savepage = false;
    this.pageType = this.gridster.withNonEditable;
  }
  @Select(LayoutState.ThemeClass) themeClass$: Observable<string>;
}
