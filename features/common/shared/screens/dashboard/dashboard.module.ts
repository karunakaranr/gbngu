import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './dashboard.component';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { GridsterModule } from 'angular-gridster2';
import {GridsterDirective} from './../../services/gridster/gridster.directive';
import { DynamicModule } from 'ng-dynamic-component';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [
    DashboardComponent,GridsterDirective
  ],
  imports: [
    CommonModule,
    CommonModule,
    FormsModule,
    MatDialogModule,
    GridsterModule,
    DynamicModule,
    MatIconModule,
    MatButtonModule,
  ],
  exports: [
    DashboardComponent
  ],
  providers:[
    {
      provide: MatDialogRef,
      useValue: {}
    },
  ]
})
export class DashboardModule { }
