import { Routes } from '@angular/router';

import {LoginComponent} from './login.component';


export const LoginRoutingModule: Routes = [{
  path: '',
  component: LoginComponent
}];
