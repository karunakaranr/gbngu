import { Component, ViewEncapsulation } from '@angular/core';
import { Validators } from '@angular/forms';
import { GBBasePageComponentNG } from './../../../../../libs/uicore/src/lib/components/Base/GBBasePage';
import { GBDataPageServiceNG} from './../../../../../libs/uicore/src/lib/services/gbpage.service';
import { GBBaseFormGroup } from './../../../../../libs/uicore/src/lib//classes/GBFormGroup';
import { ILoginDetail } from './../../models/Login/LoginDetail';
import { Versionservice } from './../../services/Login/Version.service';
import { Select } from '@ngxs/store';
import { LayoutState } from 'features/layout/store/layout.state';
const themecolors = require('./../../../../../apps/Goodbooks/Goodbooks/src/assets/Theme/ThemeColor.json')
@Component({
  selector: 'app-logincommon',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  providers: [
    { provide: 'PageService', useClass: GBDataPageServiceNG},
    {provide: 'DataService', useClass: Versionservice}
  ]
})
export class LoginComponent extends GBBasePageComponentNG {
  @Select(LayoutState.CurrentTheme) currenttheme: any;   
  form: GBBaseFormGroup;
  title = 'login';
  label = 'Login';
  hide = true;
  isToggle: boolean = false;
  async thisConstructor() {
    this.form = this.gbps.fb.group({
      Server: ['GB4SC', Validators.required],
      UserName: ['ADMIN', Validators.required],
      Password: ['admin', Validators.required]
    }) as GBBaseFormGroup;
    this.currenttheme.subscribe( (theme) => {
        this.setStyle('--color', themecolors[theme]);
    })
    this.toggleCompenents();
  }

  setStyle(name,value) {
    document.documentElement.style.setProperty(name, value);
  } 

  public Login() {
    const logindata: ILoginDetail = this.form.value as ILoginDetail;
    (this.gbps.dataService as Versionservice).getversioninfo(logindata); 
  }
  public toggleCompenents() {
    // Automatically toggle components every 2 seconds
    setInterval(() => {
    this.isToggle = !this.isToggle;
     }, 60000);
  }
}

