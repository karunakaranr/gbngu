export class Gridsetting {
    headerName : string;
    field : string;
    sortable : boolean;
    filter : boolean;
    checkboxSelection : boolean;
    resizable : boolean;
    editable : boolean;
    pinned : string;
    cellEditor : string;
    cellEditorParams : string;
}

