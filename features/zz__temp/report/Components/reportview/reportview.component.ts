import { Component, Input, OnInit } from '@angular/core';
import { GridSetting } from '../../../Ag-grid/Model/gridview.model';
import { ReportDetail } from '../../models/report.model';

@Component({
  selector: 'app-reportview',
  templateUrl: './reportview.component.html',
  styleUrls: ['./reportview.component.scss']
})
export class ReportviewComponent implements OnInit {
  @Input() rowData: ReportDetail[];
  @Input() columnData: GridSetting[];
  constructor() { }
  ngOnInit(): void {
  }

}
