import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountopeningComponent } from './accountopening.component';

describe('AccountopeningComponent', () => {
  let component: AccountopeningComponent;
  let fixture: ComponentFixture<AccountopeningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountopeningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountopeningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
