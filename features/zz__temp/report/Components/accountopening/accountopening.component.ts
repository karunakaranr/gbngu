import { Component, OnInit } from '@angular/core';
import { Injectable } from '@angular/core';
import { Reportservice } from '../../services/report.service';
@Injectable({
  providedIn: 'root',
})
@Component({
  selector: 'app-accountopening',
  templateUrl: './accountopening.component.html',
  styleUrls: ['./accountopening.component.scss']
})
export class AccountopeningComponent implements OnInit {
  public rowData = [];
  public columnData = [];

  constructor(public reportservice: Reportservice) {
  }

  ngOnInit(): void {
    this.reportdata();
  }

  private reportdata() {
    this.reportservice.reportdataservice().subscribe((griddata) => {
      this.rowData = griddata;
      console.log(this.rowData);
    });
    this.reportcolumndata();
  }
  
  private reportcolumndata(){
    this.reportservice.preparecolumndata().subscribe((columnfields) => {
        this.columnData = columnfields;
      });
  }
 
}
