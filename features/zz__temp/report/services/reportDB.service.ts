import { Injectable } from '@angular/core';
import { GBHttpService } from '@goodbooks/gbcommon';
import { Observable } from 'rxjs';
import { URLS } from '../URL/url';
import { ReportObject } from '../models/grid';
import { CriteriaObject } from '../models/gridcriteria.model';

@Injectable({
  providedIn: 'root',
})

export class ReportDBservice {
 
  constructor(public HTTP: GBHttpService) { }

  public reportmetadataDb(): Observable<ReportObject> {
    return this.HTTP.httpget(URLS.AccountDetails);
  }
  
  public reportdataDb(): Observable<CriteriaObject> {
  
  const criteria=  {
      SectionCriteriaList: [
        {
          SectionId: -1499503059,
          AttributesCriteriaList: [
            {
              FieldName: 'OUId',
              OperationType: 5,
              FieldValue: '-1500000000',
              JoinType: 2,
              CriteriaAttributeName: 'OU',
              CriteriaAttributeValue: 'TESTING COMPANY',
              IsHeader: 1,
              IsCompulsory: 1,
              CriteriaAttributeId: -1899999999,
              CriteriaAttributeType: 0,
              FilterType: 1
            },
            {
              FieldName: 'Provision',
              OperationType: 1,
              FieldValue: '1',
              JoinType: 2,
              CriteriaAttributeName: 'Provision',
              CriteriaAttributeValue: 'No',
              IsHeader: 1,
              IsCompulsory: 1,
              CriteriaAttributeId: -1399999663,
              CriteriaAttributeType: 3,
              FilterType: 1
            },
            {
              FieldName: 'ReportType',
              OperationType: 1,
              FieldValue: '1',
              JoinType: 2,
              CriteriaAttributeName: 'Report Type',
              CriteriaAttributeValue: 'Lead Activity Register',
              IsHeader: 1,
              IsCompulsory: 1,
              CriteriaAttributeId: -1399999390,
              CriteriaAttributeType: 3,
              FilterType: 1
            },
            {
              FieldName: 'PeriodFromDate',
              OperationType: 10,
              FieldValue: '1585699200',
              JoinType: 0,
              CriteriaAttributeName: 'FromDate',
              CriteriaAttributeValue: '01/Apr/2020',
              IsHeader: 0,
              IsCompulsory: 0,
              CriteriaAttributeId: -2147483643,
              CriteriaAttributeType: 4,
              FilterType: 1
            },
            {
              FieldName: 'PeriodToDate',
              OperationType: 11,
              FieldValue: '1617148800',
              JoinType: 0,
              CriteriaAttributeName: 'ToDate',
              CriteriaAttributeValue: '31/Mar/2021',
              IsHeader: 0,
              IsCompulsory: 0,
              CriteriaAttributeId: -2147483642,
              CriteriaAttributeType: 4,
              FilterType: 1
            }
          ],
          OperationType: 0
        }
      ]
    };
    
    return this.HTTP.httppost(URLS.AccountSummary,criteria);
  }

}


