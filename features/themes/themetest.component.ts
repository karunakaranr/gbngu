import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Route, Router } from '@angular/router';

@Component({
  selector: 'app-ttform',
  template: `
    <div>Theme Test</div>

    <select
      #theme
      (change)="theme$ = theme.value"
      class="bg-default availableThemes"
      getThemeList
      [value]="theme$"
    >
      <option value="">--none--</option>
    </select>
    <select
      #darkmode
      (change)="darkmode$ = darkmode.value"
      class="bg-default availableThemes"
      [getThemeMode]="theme$"
      [value]="darkmode$"
    >
      <option value="">--none--</option>
    </select>

    <!-- <a [routerLink]="[{outlets: {themeoutlet: ['matform']}}]">Material Form</a> | -->

    <a [routerLink]="'matform'">Material Form</a> |
    <a [routerLink]="'htmlform'">HTML Form</a> |
    <a [routerLink]="'bsform'">Bootstrap Form</a> |

    <!-- <div>
      <router-outlet name="themeoutlet"></router-outlet>
    </div> -->
  `,
})
export class ThemeTestComponent implements OnInit {
  theme$: string = 'default';
  darkmode$: string = 'day';

  constructor(private router: Router, private activatedRoute: ActivatedRoute) {
    console.log('');
  }

  ngOnInit(): void {
  }

  // printpath(parent: String, config: Route[]) {
  //   for (let i = 0; i < config.length; i++) {
  //     const route = config[i];
  //     console.log(parent + '/' + route.path);
  //     if (route.children) {
  //       const currentPath = route.path ? parent + '/' + route.path : parent;
  //       this.printpath(currentPath, route.children);
  //     }
  //   }
  // }
}
