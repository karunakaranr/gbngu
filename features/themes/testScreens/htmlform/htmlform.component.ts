import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-htmlform',
  templateUrl: './htmlform.component.html',
  styleUrls: ['./htmlform.component.scss']
})
export class HtmlformComponent implements OnInit {
  testText = "The quick brown fox jumps over the lazy dog ";

  constructor() { }

  ngOnInit(): void {
  }

}
