import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BsformComponent } from './bsform.component';

describe('BsformComponent', () => {
  let component: BsformComponent;
  let fixture: ComponentFixture<BsformComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BsformComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BsformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
