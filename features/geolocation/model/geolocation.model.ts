export interface geoconfig {

		lat: string;
		long: string;
		isdirection: boolean;
		address: string;
	
}