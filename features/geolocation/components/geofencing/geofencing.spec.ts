import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { geofencingcomponentComponent } from './geofencing.component';

describe('MapcomponentComponent', () => {
  let component: geofencingcomponentComponent;
  let fixture: ComponentFixture<geofencingcomponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ geofencingcomponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(geofencingcomponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
