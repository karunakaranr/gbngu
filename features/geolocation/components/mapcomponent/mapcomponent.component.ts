import { Component, ViewChild, ElementRef, NgZone, Input, Output, EventEmitter } from '@angular/core';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { geoconfig } from './../../model/geolocation.model'

@Component({
  selector: 'app-mapcomponent',
  templateUrl: './mapcomponent.component.html',
  styleUrls: ['./mapcomponent.component.scss']
})
export class MapcomponentComponent {
  title: string = '';
  latitude;
  longitude;
  zoom: number;
  address: string;
  private geoCoder;
  isdirectionbutton
  getaddress = true
  direction = false
  @Input()
  config: geoconfig;
  @Output() geoinfo = new EventEmitter();

  @ViewChild('search')
  public searchElementRef: ElementRef;


  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone

  ) { }


  ngOnInit() {
    this.isdirectionbutton = this.config.isdirection
    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          console.log(place.geometry)
          if (place.geometry === undefined || place.geometry === null) {
            let placefromsearch = place.name
            let latlongsplit = placefromsearch.split(",")


            this.latitude = parseFloat(latlongsplit[0]);
            this.longitude = parseFloat(latlongsplit[1]);
            this.zoom = 12;
            this.geoCoder.geocode({ 'location': { lat: this.latitude, lng: this.longitude } }, (results, status) => {
              if (status === 'OK') {

                if (results[0]) {

                  this.zoom = 12;
                  this.address = results[0].formatted_address;

                } else {
                  window.alert('No results found');
                }
              } else {
                window.alert('Geocoder failed due to: ' + status);
              }

            });

          }
          else {
            this.latitude = place.geometry.location.lat();
            this.longitude = place.geometry.location.lng();
            this.zoom = 12;
          }


        });
      });
    });
  }

  directionenable() {
    this.direction = true
    this.getaddress = false
  }
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        console.log(this.latitude, this.longitude)
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }


  markerDragEnd($event: MouseEvent) {
    console.log($event);
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {

    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }
 

}
interface marker {
  lat: number;
  lng: number;
  label?: string;
  draggable: boolean;
}