import { Component , ViewChild, ElementRef, NgZone} from '@angular/core';

import { MapsAPILoader } from '@agm/core';
@Component({
  selector: 'app-mapdirectionComponent',
  templateUrl: './mapdirection.component.html',
  styleUrls: ['./mapdirection.component.scss']
})
export class mapdirectionComponent  {
  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  private geoCoder;

  public origin: any
  public destination: any
latfrom
longfrom
latto
longto


  @ViewChild('searchfrom')
  public searchElementReffrom: ElementRef;

  @ViewChild('searchto')
  public searchElementRefto: ElementRef;

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone
  ) { }
 
  
  
  ngOnInit() {

    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;

      let autocompletefrom = new google.maps.places.Autocomplete(this.searchElementReffrom.nativeElement);
      let autocompleteto = new google.maps.places.Autocomplete(this.searchElementRefto.nativeElement);

      autocompletefrom.addListener("place_changed", () => {
        this.ngZone.run(() => {
          console.log("ngzone")
          let place: google.maps.places.PlaceResult = autocompletefrom.getPlace();
console.log(place)
          
          this.latfrom=place.geometry.location.lat()
          this.longfrom=place.geometry.location.lng()
          console.log(place.geometry.location.lat())
          console.log(place.geometry.location.lng())
        });
      });

      
      autocompleteto.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocompleteto.getPlace();
console.log(place)
         
         this.latto=place.geometry.location.lat()
         this.longto=place.geometry.location.lng()
         console.log(place.geometry.location.lat())
          console.log(place.geometry.location.lng())
        });
      });

     
    });
 
  }
  
  getDirection() {
    this.origin = { lat: this.latfrom, lng: this.longfrom }
    this.destination = { lat: this.latto, lng: this.longto }
  
  } 

   initMap(): void {
    const map = new google.maps.Map(
      document.getElementById("map") as HTMLElement,
      {
        mapTypeControl: false,
        center: { lat: -33.8688, lng: 151.2195 },
        zoom: 13,
      }
    );
  
  }
  
  
  private setCurrentLocation() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 8;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }
  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      console.log(results);
      console.log(status);
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }

    });
  }
}
  