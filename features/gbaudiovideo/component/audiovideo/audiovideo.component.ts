import { Component, ChangeDetectionStrategy, ChangeDetectorRef, ViewChild, ElementRef } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { AudioRecordingService } from 'features/gbaudiovideo/service/audio-recording.service';
import { VideoRecordingService } from 'features/gbaudiovideo/service/video-recording.service';

@Component({
  selector: 'app-audiovideo',
  templateUrl: './audiovideo.component.html',
  styleUrls: ['./audiovideo.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class CaptureimagevideoComponent {
  
  video: any;
  isAudioRecording = false;
  audioRecordedTime: string;
  audioBlobUrl: SafeUrl | null;
  audioBlob: Blob;
  audioName: string;
  audioConf = { audio: true}

  isVideoRecording = false; // Add video-related variables
  videoRecordedTime: string;
  videoBlobUrl: SafeUrl | null;
  videoBlob: Blob;
  videoName: string;
  videoConf = { video: { facingMode:"user", width: 320 }, audio: true}

  constructor(
    private ref: ChangeDetectorRef,
    private audioRecordingService: AudioRecordingService,
    private videoRecordingService: VideoRecordingService, // Inject VideoRecordingService
    private sanitizer: DomSanitizer
  ) {

    // Subscribe to audio recording events
    this.audioRecordingService.recordingFailed().subscribe(() => {
      this.isAudioRecording = false;
      this.ref.detectChanges();
    });

    this.audioRecordingService.getRecordedTime().subscribe((time: string) => {
      this.audioRecordedTime = time;
      this.ref.detectChanges();
    });

    this.audioRecordingService.getRecordedBlob().subscribe((data: { blob: Blob; title: string }) => {
      this.audioBlob = data.blob;
      this.audioName = data.title;
      this.audioBlobUrl = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(data.blob));
      this.ref.detectChanges();
    });

    // Subscribe to video recording events
    this.videoRecordingService.recordingFailed().subscribe(() => {
      this.isVideoRecording = false;
      this.ref.detectChanges();
    });

    this.videoRecordingService.getRecordedTime().subscribe((time: string) => {
      this.videoRecordedTime = time;
      this.ref.detectChanges();
    });

    this.videoRecordingService.getRecordedBlob().subscribe((data: { blob: Blob; title: string }) => {
      this.videoBlob = data.blob;
      this.videoName = data.title;
      this.videoBlobUrl = this.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(data.blob));
      this.ref.detectChanges();
    });
  }

  startVideoRecording() {
    if (!this.isVideoRecording) {
      this.video = document.getElementById("videoElement");
      console.log("this.video",this.video)
      this.video.controls = false;
      this.isVideoRecording = true;
      this.videoRecordingService.startRecording(this.videoConf)
      .then(stream => {
        console.log("stream",stream)
        this.video.srcObject = stream;
        this.video.play();
      })
      .catch(function (err) {
        console.log(err.name + ": " + err.message);
      });
    }
  }

  stopVideoRecording() {
    if (this.isVideoRecording) {
      this.videoRecordingService.stopRecording();
      this.video.srcObject = this.videoBlobUrl;
      this.isVideoRecording = false;
      this.video.controls = true;
    }
  }
  startAudioRecording() {
    if (!this.isAudioRecording) {
      this.isAudioRecording = true;
      this.audioRecordingService.startRecording();
    }
  }

  stopAudioRecording() {
    if (this.isAudioRecording) {
      this.isAudioRecording = false;
      this.audioRecordingService.stopRecording();
    }
  }
  clearVideoRecordedData() {
    this.videoBlobUrl = null;
    this.video.srcObject = null;
    this.video.controls = false;
    this.ref.detectChanges();
  }
  clearAudioRecordedData() {
    this.audioBlobUrl = null;
    this.ref.detectChanges();
  }

  downloadVideoRecordedData() {
    this._downloadFile(this.videoBlob, 'video/mp4', this.videoName);
  }
  downloadAudioRecordedData() {
    this._downloadFile(this.audioBlob, 'audio/webm', this.audioName);
  }
  // private _downloadFile(data: any, type: string, filename: string): void {
  //   // Download file logic (same as before)
  // }
  _downloadFile(data: any, type: string, filename: string): any {
    const blob = new Blob([data], { type: type });
    const url = window.URL.createObjectURL(blob);
    //this.video.srcObject = stream;
    //const url = data;
    const anchor = document.createElement('a');
    anchor.download = filename;
    anchor.href = url;
    document.body.appendChild(anchor);
    anchor.click();
    document.body.removeChild(anchor);
  }
}
