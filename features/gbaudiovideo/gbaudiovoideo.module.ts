
import { CaptureimagevideoComponent } from './component/audiovideo/audiovideo.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AudioRecordingService } from './service/audio-recording.service';
import { VideoRecordingService } from './service/video-recording.service';
import { WMSModule } from 'features/erpmodules/WMS/WMS.module';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    CaptureimagevideoComponent,
    
  ],
  imports: [
    BrowserModule,
    CommonModule,
    WMSModule,
    NgModule
  ],
  exports: [
    CaptureimagevideoComponent,
    
  ],
  providers: [AudioRecordingService, VideoRecordingService],
  bootstrap: [CaptureimagevideoComponent]
})
export class CaptureimagevideoModule {
 
}
