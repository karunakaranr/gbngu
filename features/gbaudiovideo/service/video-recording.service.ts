import { Injectable } from '@angular/core';
import RecordRTC from 'recordrtc';
import * as moment from 'moment';
import { Observable, Subject } from 'rxjs';

interface RecordedVideoOutput {
  blob: Blob;
  title: string;
}

@Injectable()
export class VideoRecordingService {
  getStream() {
    throw new Error('Method not implemented.');
  }

  private stream: MediaStream;
  private recorder: RecordRTC | any;
  private interval: number | undefined; // Changed type to number

  private startTime: moment.MomentInput;
  private _recorded = new Subject<RecordedVideoOutput>();
  private _recordingTime = new Subject<string>();
  private _recordingFailed = new Subject<void>();

  getRecordedBlob(): Observable<RecordedVideoOutput> {
    return this._recorded.asObservable();
  }

  getRecordedTime(): Observable<string> {
    return this._recordingTime.asObservable();
  }

  recordingFailed(): Observable<void> {
    return this._recordingFailed.asObservable();
  }


  startRecording( conf: any ): Promise<any> {
    var browser = <any>navigator;

    this._recordingTime.next('00:00');
    return new Promise((resolve, reject) => {
      browser.mediaDevices.getUserMedia(conf).then((stream: MediaStream) => {
        this.stream = stream;
        this.record();
        resolve(this.stream);
      }).catch(() => {
        this._recordingFailed.next();
        reject;
      });
    });
  }

  abortRecording() {
    this.stopMedia();
  }

  private record() {
    this.recorder = new RecordRTC(this.stream, {
      type: 'video',
      mimeType: 'video/webm',
      bitsPerSecond: 44000
    });
    this.recorder.startRecording();
    this.startTime = moment();
    this.interval = setInterval(
      () => {
        const currentTime = moment();
        const diffTime = moment.duration(currentTime.diff(this.startTime));
        const time = this.toString(diffTime.minutes()) + ':' + this.toString(diffTime.seconds());
        this._recordingTime.next(time);
      },
      500
    ) as any; // Cast setInterval return value to any
  }

  private toString(value: number) {
    let val = JSON.stringify(value);
    if (!value) {
      val = '00';
    }
    if (value < 10) {
      val = '0' + value;
    }
    return val;
  }

  stopRecording() {
    if (this.recorder) {
      this.recorder.stopRecording(() => {
        const recordedBlob = this.recorder.getBlob();
        const recordedName = 'video_' + new Date().getTime() + '.webm';
        this._recorded.next({ blob: recordedBlob, title: recordedName });
        this.stopMedia();
      });
    }
  }

  private stopMedia() {
    if (this.recorder) {
      this.recorder = null;
      clearInterval(this.interval!);
      if (this.stream) {
        this.stream.getVideoTracks().forEach(track => track.stop());
        this.stream.getAudioTracks().forEach(track => track.stop());
      }
    }
  }
}
