import { ChangeDetectorRef } from "@angular/core";

const storeSliceName = 'Filter';

export class GbfilterDTO {
  static readonly type = '[' + storeSliceName + '] GbfilterDTO';
  constructor(public DFilter) {
  }
}

export class GbReportformatselection {
  static readonly type = '[' + storeSliceName + '] GbReportformatselection';
  constructor(public DReportformatselection) {
  }
}

