import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonreportComponent } from './commonreport.component';

describe('CommonreportComponent', () => {
  let component: CommonreportComponent;
  let fixture: ComponentFixture<CommonreportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonreportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonreportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
