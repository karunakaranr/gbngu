import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CommonReportdbservice } from './../dbservice/commonreportdbservice';
@Injectable({
  providedIn: 'root'
})
export class CommonReportService {
  constructor(public dbservice:CommonReportdbservice) { }

  public Reportdetailservice(id){                      //col
    return this.dbservice.picklist(id);
  }

  public Rowservice(obj, criteria){                    //row
    return this.dbservice.reportData(obj, criteria);
  }

  
  public CriteriaConfigWithFieldValue(id) {            //configselection
    return this.dbservice.CriteriaConfigWithFieldValue(id);
  }

  public CriteriaConfigId(id) {                        //
    return this.dbservice.CriteriaConfigId(id);
  }

  public reportDatacriteria(data, criterias){            //afterfilter
    return this.dbservice.reportDatacriteria(data, criterias);
  }

  public drilldownsettingservice(drillmenuid, selectedrowdata ,fieldname){  //drilldownsettings
    return this.dbservice.drilldownsettingdb(drillmenuid, selectedrowdata ,fieldname);
  }
}
