import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GblogComponent } from './components/gblog/gblog.component';

const routes: Routes = [
  {
    path: 'gblog',
    component: GblogComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GblogRoutingModule { }
