import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { GbLogService } from 'features/gblog/services/gblog/gblog.service';

@Component({
  selector: 'app-gblog',
  templateUrl: './gblog.component.html',
  styleUrls: ['./gblog.component.scss'],
})

export class GblogComponent implements OnInit {
  title = 'EventLog for Pack'
  logData;
  constructor(public service: GbLogService, private http: HttpClient) { }

  ngOnInit(): void {
    this.gblog();
  }

  public gblog() {
    this.service.getlogservice().subscribe((res) => {
      this.logData = res;
    });
  }


}
