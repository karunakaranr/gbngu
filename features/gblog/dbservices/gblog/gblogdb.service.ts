import { Injectable } from '@angular/core';
import { GbLogURL } from './../../URLS/urls';
import { HttpClient } from '@angular/common/http';
const urls = require('./../../URLS/urls.json');
@Injectable({
  providedIn: 'root',
})
export class Gblogdbservice {

  entityid = urls.EntityId;
  dataid = "EntityId=" + this.entityid + "&DataId" + urls.DataId;
  constructor(private http: HttpClient) { }
  public LogDbServices() {
    return this.http.get(GbLogURL.Gblog + this.dataid);
    
  }

}



