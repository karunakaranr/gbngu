import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxsModule } from '@ngxs/store';
import { NgSelectModule } from '@ng-select/ng-select';
import { AgGridModule } from 'ag-grid-angular';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule} from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction'
import { GblogComponent } from './components/gblog/gblog.component';
import { GblogRoutingModule } from './gblog-routing.module';
import { GbgridModule } from 'features/gbgrid/components/gbgrid/gbgrid.module';

@NgModule({
  declarations: [
    GblogComponent,
   
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AgGridModule,
    GbgridModule,
    AgmDirectionModule,
    NgSelectModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatListModule,
    GblogRoutingModule
  ],
  exports: [
    GblogComponent,
    
  ],
})
export class GblogModule {
 
}
