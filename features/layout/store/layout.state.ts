import { State, Action, StateContext, Selector } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ChangeDetectorRef, Inject, Injectable } from '@angular/core';
import {
  ToggleMenuBar,
  OpenMenuBar,
  CloseMenuBar,
  OpenMainBar,
  CloseMainBar,
  TogglePinMenuBar,
  PinMenuBar,
  UnpinMenuBar,
  ToggleContextBar,
  OpenContextBar,
  TotalReportPages,
  CloseContextBar,
  TogglePinContextBar,
  PinContextBar,
  UnpinContextBar,
  ChangeTheme,
  ChangeMode,
  ButtonVisibility,
  Title,
  Path, 
  FormEditOption,
  FormEditable,
  FormUnEditable,
  Movenextpreviousid,
  VersionServiceURL,
  ResetForm,
  FormValidationvalue,
  RolesAndRights,
  FormControlValue,
  MenuList,
  BizTransactionClassId,
  InnerExceptionError,
  GCMTypeId,
  Reporttypebtn
} from './layout.actions';
import { IBizTransaction } from '../models/IBizTransaction';

export class FormEditStateModel {
  formedit;
}

export class LayoutStateModel {
  MenuBarOpened: boolean;
  MenuBarPinned: boolean;
  OpenMainBar: boolean;
  CloseMainBar: boolean;
  FormEdit: boolean;
  ContextBarOpened: boolean;
  ContextBarPinned: boolean;
  Path: string;
  Theme: string;
  Mode: string;
  Visibilityname: string;
  Title : string;
  statemovenextpreviousid : string;
  versionServiceURL : string
  resetform : boolean
  formvalidations : any
  RolesandRights: IBizTransaction
  FormControlValue: any
  MenuList:any
  BizTransactionClassId:number | string
  InnerExceptionError:string | number
  TotalReportPages: number
  GCMTypeId:number | string
  Reporttypebtn:string
}

@State<LayoutStateModel>({
  name: 'Layout',
  defaults: {
    ContextBarOpened: false,
    ContextBarPinned: false,
    MenuBarOpened: false,
    MenuBarPinned: false,
    OpenMainBar:true,
    CloseMainBar:false,
    FormEdit:false,
    Theme: 'Default',
    Mode: 'light',
    Path: 'null',
    Visibilityname: 'null',
    Title:null,
    statemovenextpreviousid : null,
    versionServiceURL:null,
    resetform: false,
    formvalidations:null,
    RolesandRights: null,
    FormControlValue:null,
    MenuList:null,
    BizTransactionClassId:-1,
    InnerExceptionError:null,
    TotalReportPages: null,
    GCMTypeId:-1,
    Reporttypebtn:null
  },
})

@State<FormEditStateModel>({
  name: 'Form',
  defaults: {
    formedit: null,
  },
})

@Injectable()
export class LayoutState {
  @Selector() static Formeditabe(state: FormEditStateModel) {
    return state.formedit;
  }
  @Selector() static MenuBarOpened(state: LayoutStateModel) {
    return state.MenuBarOpened;
  }
  @Selector() static MenuBarPinned(state: LayoutStateModel) {
    return state.MenuBarPinned;
  }

  @Selector() static OpenMainBar(state: LayoutStateModel) {
    return state.OpenMainBar;
  }

  @Selector() static FormEdit(state: LayoutStateModel) {
    return state.FormEdit;
  }
  @Action(ToggleMenuBar)
  ToggleMenuBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, MenuBarOpened: !state.MenuBarOpened });
  }
  @Action(OpenMenuBar)
  OpenMenuBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, MenuBarOpened: true });
  }
  @Action(CloseMenuBar)
  CloseMenuBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, MenuBarOpened: false });
  }

  @Action(OpenMainBar)
  OpenMainBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, OpenMainBar: true });
  }
  @Action(CloseMainBar)
  CloseMainBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, OpenMainBar: false });
  }

  @Action(FormEditable)
  FormEditable(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, FormEdit: true });
  }
  @Action(FormUnEditable)
  FormUnEditable(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, FormEdit: false });
  }
  @Action(TogglePinMenuBar)
  TogglePinMenuBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, MenuBarPinned: !state.MenuBarPinned });
  }
  @Action(PinMenuBar)
  PinMenuBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, MenuBarPinned: true });
  }
  @Action(UnpinMenuBar)
  UnpinMenuBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, MenuBarPinned: false });
  }

  @Selector() static ContextBarOpened(state: LayoutStateModel) {
    return state.ContextBarOpened;
  }
  @Selector() static ContextBarPinned(state: LayoutStateModel) {
    return state.ContextBarPinned;
  }
  @Action(ToggleContextBar)
  ToggleContextBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, ContextBarOpened: !state.ContextBarOpened });
  }
  @Action(OpenContextBar)
  OpenContextBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, ContextBarOpened: true });
  }
  @Action(CloseContextBar)
  CloseContextBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, ContextBarOpened: false });
  }
  @Action(TogglePinContextBar)
  TogglePinContextBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, ContextBarPinned: !state.ContextBarPinned });
  }
  @Action(PinContextBar)
  PinContextBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, ContextBarPinned: true });
  }
  @Action(UnpinContextBar)
  UnpinContextBar(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, ContextBarPinned: false });
  }

  @Selector() static CurrentTheme(state: LayoutStateModel) {
    return state.Theme;
  }

  @Selector() static CurrentMode(state: LayoutStateModel) {
    return state.Mode;
  }

  @Selector() static ThemeClass(state: LayoutStateModel) {
    let retClass: String = 'theme--' + state.Theme + ' theme--' + state.Theme + '-' + state.Mode;
    return retClass;
  }

  @Selector() static Movenxtpreid(state: LayoutStateModel) {
    return state.statemovenextpreviousid;
  }

  @Selector() static Resetformvalue(state: LayoutStateModel) {
    return state.resetform;
  }

  @Selector() static FormValidationvalues(state: LayoutStateModel) {
    return state.formvalidations;
  }

  @Selector() static BizTransactionClassId(state: LayoutStateModel) {
    return state.BizTransactionClassId;
  }

  @Selector() static TotalReportPages(state: LayoutStateModel) {
    return state.TotalReportPages;
  }

  @Selector() static InnerExceptionError(state: LayoutStateModel) {
    return state.InnerExceptionError;
  }

  @Selector() static GCMTypeId(state: LayoutStateModel) {
    return state.GCMTypeId;
  }

  @Selector() static Versionservicurl(state: LayoutStateModel) {
    return state.versionServiceURL;
  }

  @Action(ChangeTheme)
  ChangeTheme(context: StateContext<LayoutStateModel>, newTheme: ChangeTheme) {
    const state = context.getState();
    context.setState({ ...state, Theme: newTheme.themeName });
    if (newTheme.ref != null) newTheme.ref.detectChanges();
  }

  @Action(VersionServiceURL)
  VersionServiceURL(context: StateContext<LayoutStateModel>, version: VersionServiceURL) {
    const state = context.getState();
    context.setState({ ...state, versionServiceURL:version.versionurl });
    if (version.ref != null) version.ref.detectChanges();
  }

  @Action(Movenextpreviousid)
  Movenextpreviousid(context: StateContext<LayoutStateModel>, moveid: Movenextpreviousid) {
    const state = context.getState();
    context.setState({ ...state, statemovenextpreviousid: moveid.movenextpreviousid });
  }
 
  @Action(ResetForm)
  ResetForm(context: StateContext<LayoutStateModel>) {
    const state = context.getState();
    context.setState({ ...state, resetform: !state.resetform });
  }

  @Action(FormValidationvalue)
  FormValidationvalue(context: StateContext<LayoutStateModel>, validation: FormValidationvalue) {
    const state = context.getState();
    context.setState({ ...state, formvalidations: validation.formvalidationvalues });
  }

  @Action(BizTransactionClassId)
  BizTransactionClassId(context: StateContext<LayoutStateModel>, validation: BizTransactionClassId) {
    const state = context.getState();
    context.setState({ ...state, BizTransactionClassId: validation.biztrannsactionclass });
  }

  @Action(TotalReportPages)
  TotalReportPages(context: StateContext<LayoutStateModel>, validation: TotalReportPages) {
    const state = context.getState();
    context.setState({ ...state, TotalReportPages: validation.TotalReportPages });
  }

  @Action(InnerExceptionError)
  InnerExceptionError(context: StateContext<LayoutStateModel>, validation: InnerExceptionError) {
    const state = context.getState();
    context.setState({ ...state, InnerExceptionError: validation.InnerExceptionError });
  }

  @Action(GCMTypeId)
  GCMTypeId(context: StateContext<LayoutStateModel>, validation: GCMTypeId) {
    const state = context.getState();
    context.setState({ ...state, GCMTypeId: validation.gcmtypeid });
  }

  @Action(ChangeMode)
  ChangeMode(context: StateContext<LayoutStateModel>, newMode: ChangeMode) {
    const state = context.getState();
    context.setState({ ...state, Mode: newMode.modeName });
    if (newMode.ref != null) newMode.ref.detectChanges();
  }

  @Selector() static NeedBackdrop(state: LayoutStateModel) {
    return !state.MenuBarPinned;
    return !state.ContextBarPinned || !state.MenuBarPinned;
  }


  @Selector() static Title(state: LayoutStateModel) {
    return state.Title;
  }

  @Action(Title)
  Title(context: StateContext<LayoutStateModel>, title: Title) {
    const state = context.getState();
    context.setState({ ...state, Title: title.title});
  }

  @Selector() static Visibilebuttons(state: LayoutStateModel) {
    return state.Visibilityname;
  }

  @Selector() static Reporttypebtn(state: LayoutStateModel) {
    return state.Reporttypebtn;
  }

  @Selector() static RolesAndRights(state: LayoutStateModel) {
    return state.RolesandRights;
  }

  @Selector() static FormControlValue(state: LayoutStateModel) {
    return state.FormControlValue;
  }

  @Selector() static MenuList(state: LayoutStateModel) {
    return state.MenuList;
  }

  @Action(ButtonVisibility)
  Visibilityname(context: StateContext<LayoutStateModel>, visiblebuttons: ButtonVisibility) {
    const state = context.getState();
    context.setState({ ...state, Visibilityname: visiblebuttons.visibilityname});
  }

  @Action(Reporttypebtn)
  Reporttypebtn(context: StateContext<LayoutStateModel>, report_typebtns: Reporttypebtn) {
    const state = context.getState();
    context.setState({ ...state, Reporttypebtn: report_typebtns.reporttypebtns});
  }

  @Action(RolesAndRights)
  RolesAndRights(context: StateContext<LayoutStateModel>, rolesandrights: RolesAndRights) {
    const state = context.getState();
    context.setState({ ...state, RolesandRights: rolesandrights.roles});
  }

  @Action(FormControlValue)
  FormControlValue(context: StateContext<LayoutStateModel>, Formcontrolvalue: FormControlValue) {
    const state = context.getState();
    context.setState({ ...state, FormControlValue: Formcontrolvalue.formcontrolvalue});
  }

  @Action(MenuList)
  MenuList(context: StateContext<LayoutStateModel>, MenuList: MenuList) {
    const state = context.getState();
    context.setState({ ...state, MenuList: MenuList.MenuList});
  }

  @Selector() static Path(state: LayoutStateModel) {
    return state.Path;
  }

  @Action(Path)
  Path(context: StateContext<LayoutStateModel>, Path: Path) {
    const state = context.getState();
    context.setState({ ...state, Path: Path.path});
  }

  @Action(FormEditOption)
    FormEditOption(context: StateContext<FormEditStateModel>, FormEdit1: FormEditOption) {
      const state = context.getState();
      context.setState({ ...state, formedit: FormEdit1.FormEdit});
    }
}
