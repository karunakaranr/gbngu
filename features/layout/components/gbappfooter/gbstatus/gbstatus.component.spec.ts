import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbstatusComponent } from './gbstatus.component';

describe('GbstatusComponent', () => {
  let component: GbstatusComponent;
  let fixture: ComponentFixture<GbstatusComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbstatusComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbstatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
