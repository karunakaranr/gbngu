import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbsaveComponent } from './gbsave.component';

describe('GbsaveComponent', () => {
  let component: GbsaveComponent;
  let fixture: ComponentFixture<GbsaveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbsaveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbsaveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
