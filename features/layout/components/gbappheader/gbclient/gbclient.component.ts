import { Component, OnInit } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { ILoginDTO } from './../.../../../../../common/shared/models/Login/Logindto';
import { AuthState } from './../../../../common/shared/stores/auth.state';
import { Observable, Subscription } from 'rxjs';
import { LoginDetail } from 'features/common/shared/services/Login/LoginDetail.sevice';
import { ILoginDetailDTO } from 'features/common/shared/models/Login/LoginDetailDTO';
import { DatePipe } from '@angular/common';
import { LoginDetails } from 'features/common/shared/stores/auth.action';
@Component({
  selector: 'app-gbclient',
  templateUrl: './gbclient.component.html',
  styleUrls: ['./gbclient.component.scss']
})
export class GbclientComponent implements OnInit {
  @Select(AuthState.AddDTO) loginDTOs$: Observable<ILoginDTO>;
  private formSubscription: Subscription = new Subscription();
  Companyname : string;
  LoginDetail : ILoginDetailDTO;
  FromDate: string;
  ToDate: string;
  constructor(public service : LoginDetail ,public store: Store) { }

  ngOnInit(): void {
    var datePipe = new DatePipe("en-US");
    this.formSubscription.add(
      this.loginDTOs$.subscribe(dto => {
        this.Companyname = dto.OuName;
        this.service.LoginDetailview(dto.UserId).subscribe(menudetails => {
          this.LoginDetail = menudetails
          this.store.dispatch( new LoginDetails(menudetails))
          this.FromDate=datePipe.transform(JSON.parse(this.LoginDetail.WorkPeriodFromDate.replace("/Date(", "").replace(")/", "")), 'yyyy');
          this.ToDate=datePipe.transform(JSON.parse(this.LoginDetail.WorkPeriodToDate.replace("/Date(", "").replace(")/", "")), 'yyyy');
          console.log("Login DTO:",this.LoginDetail)
        })
      })
    );
  }

}
