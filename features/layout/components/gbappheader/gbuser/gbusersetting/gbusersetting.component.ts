import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { GBHttpService } from '@goodbooks/gbcommon';
import { Router } from '@angular/router';
import { StateClear } from 'ngxs-reset-plugin';
import { Select, Store } from '@ngxs/store'
import { Observable, Subscription } from 'rxjs';
import { AuthState } from './../../../../../common/shared/stores/auth.state';
import { ILoginDTO } from './../.../../../../../../common/shared/models/Login/Logindto';
import { MatDialog } from '@angular/material/dialog';
import { GbSettingsComponent } from './../../gbsettings/gbsettings.component';
import { ChangeTheme, ChangeMode, FormEditable } from 'features/layout/store/layout.actions';
import { formactionservice } from 'features/layout/services/formaction/formaction.sevice';
import { FormBuilder, FormGroup } from '@angular/forms';
import { AddLoginDTO } from 'features/common/shared/stores/auth.action';
import { SharedService } from 'features/commonreport/service/datapassing.service';
@Component({
  selector: 'app-gbusersetting',
  templateUrl: './gbusersetting.component.html',
  styleUrls: ['./gbusersetting.component.scss']
})
export class GbusersettingComponent implements OnInit {
  public DB: string;
  public user: string;
  public change: null;
  MySettingform: FormGroup;
  LoginDTO:ILoginDTO;
  OuName:string;
  OuPeriod:string;
  public usercolor;
  public color = 'Black';
  public imgTraceSrc = 'assets/UserSetting/Black/Trace.png';
  public imgThemeSrc = 'assets/UserSetting/Black/Theme.png';
  public imgSettingSrc = 'assets/UserSetting/Black/Setting.png';
  public imgLoggedInSrc = 'assets/UserSetting/Black/Logged.png';
  public imgAboutSrc = 'assets/UserSetting/Black/About.png';
  public imgLogoutSrc = 'assets/UserSetting/Black/Logout.png';
  @Select(AuthState.AddDTO) loginDTOs$: Observable<ILoginDTO>;
  @Select(AuthState.GetTheme) theme$: Observable<any>;
  private formSubscription: Subscription = new Subscription();
  constructor(public fb: FormBuilder, public service: formactionservice, public HTTP: GBHttpService, public store: Store, public route: Router, public dialog: MatDialog, private ref: ChangeDetectorRef, public shareddataservice: SharedService) { }

  ngOnInit(): void {
    this.formSubscription.add(
      this.loginDTOs$.subscribe(dto => {
        this.LoginDTO = {
          UserCode: dto.UserCode,
          DatabaseName: dto.DatabaseName,
          SessionId: dto.SessionId,
          ValidToTime: dto.ValidToTime,
          ValidityOfSession: dto.ValidityOfSession,
          UserId: dto.UserId,
          UserPrimaryMailId: dto.UserPrimaryMailId,
          ServerId: dto.ServerId,
          RoleId: dto.RoleId,
          AppId: dto.AppId,
          DeviceId: dto.DeviceId,
          WorkOUId: dto.WorkOUId,
          WorkPeriodId: dto.WorkPeriodId,
          WorkPartyBranchId: dto.WorkPartyBranchId,
          WorkStoreId: dto.WorkStoreId,
          Realm: dto.Realm,
          ImgPath: dto.ImgPath,
          ModeOfOperation: dto.ModeOfOperation,
          MachineIP: dto.MachineIP,
          UserName: dto.UserName,
          TimeZone: dto.TimeZone,
          DatabaseOffset: dto.DatabaseOffset,
          DatabaseType: dto.DatabaseType,
          UserCriteriaConfigId: dto.UserCriteriaConfigId,
          ClientId: dto.ClientId,
          SourceType: dto.SourceType,
          UserCriteriaDTO: dto.UserCriteriaDTO,
          StartTime: dto.StartTime,
          DateFormat: dto.DateFormat,
          CurrencyFormat: dto.CurrencyFormat,
          TimeFormat: dto.TimeFormat,
          QuantityFormat: dto.QuantityFormat,
          FromMailMenuId: dto.FromMailMenuId,
          FromMailvalueId: dto.FromMailvalueId,
          FEUri: dto.FEUri,
          IsAdmin: dto.IsAdmin,
          ServerConfigId: dto.ServerConfigId,
          ModeOfWorking: dto.ModeOfWorking,
          BaseUri: dto.BaseUri,
          Geo: dto.Geo,
          LoginServerDate: dto.LoginServerDate,
          LoginEventLogId: dto.LoginEventLogId,
          ServerPopup: dto.ServerPopup,
          WorkFinanceBookId:dto.WorkFinanceBookId,
          IsValidationRequired: dto.IsValidationRequired,
          OuCode: dto.OuCode,
          OuName: dto.OuName,
          SelectlistOperationType:dto.SelectlistOperationType,
          LastLoginUsedTime:dto.LastLoginUsedTime,
          ExpiryTime:dto.ExpiryTime,
          GraceTime:dto.GraceTime,
          ServerConfigOffset:dto.ServerConfigOffset,
          ServerConfigMaxValue:dto.ServerConfigMaxValue,
          TimeZoneId:dto.TimeZoneId,
          TimeZoneDisplayName:dto.TimeZoneDisplayName,
          ServiceOffSet:dto.ServiceOffSet,
          FinalOffSetValue:dto.FinalOffSetValue,
          FETIMEZONEOFFSET:dto.FETIMEZONEOFFSET,
          IsIpBasedCheckingRequired:dto.IsIpBasedCheckingRequired,
          IsForcePasswordChange:dto.IsForcePasswordChange,
          Delimiter:dto.Delimiter,
          ReportBaseUri:dto.ReportBaseUri,
          FEVersion:dto.FEVersion,
          
        };
        this.user = dto.UserName;
      })
    );
  }


  public logout() {
    const UserName = this.user;
    const url = '/fws/User.svc/LogOut/?UserCode=' + UserName;
    this.HTTP.httppost(url, '').subscribe(Res => {
      if (Res) {
        this.shareddataservice.setRowInfo(undefined);
        this.route.navigate([''])
        this.store.dispatch(new StateClear());
        this.store.dispatch(new ChangeTheme('Default', this.ref));
        this.store.dispatch(new ChangeMode('light', this.ref));
      }
    });
  }
  openDialog() {
    const dialogRef = this.dialog.open(GbSettingsComponent, { position: { right: '0' } });
    dialogRef.afterClosed().subscribe(result => {
    });
  }

  changeimageTraceWhite() {
    this.imgTraceSrc = 'assets/UserSetting/White/Trace.png';

  }

  changeimageTraceBlack() {
    this.imgTraceSrc = 'assets/UserSetting/Black/Trace.png';

  }

  changeimageThemeWhite() {
    this.imgThemeSrc = 'assets/UserSetting/White/Theme.png';

  }

  changeimageThemeBlack() {
    this.imgThemeSrc = 'assets/UserSetting/Black/Theme.png';

  }

  changeimageSettingWhite() {
    this.imgSettingSrc = 'assets/UserSetting/White/Setting.png';

  }

  changeimageSettingBlack() {
    this.imgSettingSrc = 'assets/UserSetting/Black/Setting.png';

  }

  changeimageLoggedInWhite() {
    this.imgLoggedInSrc = 'assets/UserSetting/White/Logged.png';

  }

  changeimageLoggedInBlack() {
    this.imgLoggedInSrc = 'assets/UserSetting/Black/Logged.png';

  }

  changeimageAboutWhite() {
    this.imgAboutSrc = 'assets/UserSetting/White/About.png';

  }

  changeimageAboutBlack() {
    this.imgAboutSrc = 'assets/UserSetting/Black/About.png';

  }

  changeimageLogoutWhite() {
    this.imgLogoutSrc = 'assets/UserSetting/White/Logout.png';

  }

  changeimageLogoutBlack() {
    this.imgLogoutSrc = 'assets/UserSetting/Black/Logout.png';

  }

  public MySettings() {
    this.service.MysettingGet().subscribe(res => {
      console.log("ress",res)
      if(res){
        let data = JSON.parse(res.Body)
        this.OuName = data[0].OuName
        this.OuPeriod = data[0].PeriodName
        this.MySettingform = this.fb.group({
          UserId: [data[0].UserId],
          OuId: [data[0].OuId],
          PeriodId: [data[0].PeriodId],
          PartyBranchId: [data[0].PartyBranchId],
          StoreId: [data[0].StoreId],
          WorkDate: [data[0].WorkDate],
          RegionId: [data[0].RegionId],
          UserDateFormat: [data[0].DateFormat],
          UserTimeFormat: [data[0].TimeFormat],
          UserCurrencyFormat: [data[0].CurrencyFormat],
          UserQuantityFormat: [data[0].QuantityFormat],
          WorkFinanceBookId: [data[0].WorkFinanceBookId],
          WorkFinanceBookName: [data[0].WorkFinanceBookName],
          TimeZoneId: [data[0].TimeZoneId],
          DefaultMenuId: [data[0].DefaultMenuId],
          DeviceId: [data[0].DeviceId],
        })

        console.log(":::",this.MySettingform.value,this.OuName)
        document.getElementById("Mysettingscreenmodel").style.display = "block";
        this.store.dispatch(new FormEditable);
      }
    })
  }


  public GetselectedOuName(SelectedOuName:any){
    console.log("SelectedOuName",SelectedOuName.SelectedData)
    this.MySettingform.get('OuId').setValue(SelectedOuName.SelectedData)
    this.LoginDTO.WorkOUId = SelectedOuName.SelectedData
  }

  public GetselectedOuPeriod(SelectedOuperiod:any){
    console.log("SelectedOuperiod",SelectedOuperiod.SelectedData)
    this.MySettingform.get('PeriodId').setValue(SelectedOuperiod.SelectedData)
    this.LoginDTO.WorkPeriodId = SelectedOuperiod.SelectedData
  }

  public SaveSettings(){
    document.getElementById("Mysettingscreenmodel").style.display = "none";
    this.service.SettingSave(this.MySettingform.value).subscribe( res=> {
      console.log("Save",res)
    })

    this.store.dispatch(new AddLoginDTO(this.LoginDTO));
  }

  public settingclose(){
    document.getElementById("Mysettingscreenmodel").style.display = "none";
  }

}
