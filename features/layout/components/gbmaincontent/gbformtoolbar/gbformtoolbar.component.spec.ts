import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbformtoolbarComponent } from './gbformtoolbar.component';

describe('GbformtoolbarComponent', () => {
  let component: GbformtoolbarComponent;
  let fixture: ComponentFixture<GbformtoolbarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbformtoolbarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbformtoolbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
