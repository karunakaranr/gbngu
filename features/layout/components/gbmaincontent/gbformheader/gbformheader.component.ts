import { Component, OnInit, ViewChild } from '@angular/core';
import { GbformactionsComponent } from './gbformactions/gbformactions.component';
import { GbformtitleComponent } from './gbformtitle/gbformtitle.component';
import { LayoutState } from 'features/layout/store/layout.state';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-gbformheader',
  templateUrl: './gbformheader.component.html',
  styleUrls: ['./gbformheader.component.scss']
})
export class GbformheaderComponent implements OnInit {
  @ViewChild(GbformtitleComponent) title: GbformtitleComponent;
  @ViewChild(GbformactionsComponent) forms: GbformactionsComponent;
  @Select(LayoutState.FormEdit) edit$: Observable<boolean>;
  divBackgroundColor: string = 'white'; // Initial background color
  focusSet: boolean = false; // Track whether focus has been set
  borderStyle: string = 'none'; // Initial border style
  
  constructor() { }

  ngOnInit(): void {
    this.edit$.subscribe(res => {
      if (res) {
        this.divBackgroundColor = '#C4EEDA'; // Set background color to green
        this.borderStyle = '1px solid #32AE73';
        if (!this.focusSet) {
        }
      } else {
        this.divBackgroundColor = 'white'; // Initial background color
        this.borderStyle = 'none'; // Remove border
      }
    });
  }

 
}
