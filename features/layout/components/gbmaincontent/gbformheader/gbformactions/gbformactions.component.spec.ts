import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GbformactionsComponent } from './gbformactions.component';

describe('GbformactionsComponent', () => {
  let component: GbformactionsComponent;
  let fixture: ComponentFixture<GbformactionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GbformactionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GbformactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
