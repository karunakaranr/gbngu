import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { GBHttpService } from '@goodbooks/gbcommon';
import { Select } from '@ngxs/store';
import { AuthState } from 'features/common/shared/stores/auth.state';

@Injectable({
    providedIn: 'root',
  })

  export class formactiondbservice {
    constructor(private http: GBHttpService) { }
    @Select(AuthState.AddDTO) LOGINDTO$: Observable<any>;
    public formactionService(url,criteria) : Observable<any> {
      return this.http.httppost(url,criteria);
    }

    public MySettingdbget() : Observable<any> {
      let userid : any;
      this.LOGINDTO$.subscribe(dto => {
        let Logindto = dto;
        userid = dto.UserId;
    })
      let url = '/fws/User.svc/UserSettingDetail/?UserId='+userid
      return this.http.httpget(url);
    }

    public Settingdbsave(savedata) : Observable<any> {
      let url = '/fws/User.svc/UpdateUserSettings'
      return this.http.httppost(url,savedata)
    }
  
  }
  