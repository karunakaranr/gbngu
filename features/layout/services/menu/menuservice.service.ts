import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IMenulist } from './../../models/Imenu.model';
import { MenudbService } from './../../dbservices/modulelist/menu/menudb.service';
@Injectable({
  providedIn: 'root'
})
export class MenuserviceService {
  constructor(private menudbService: MenudbService) { }

  public savedata(url,criteria):Observable<any> {
    return this.menudbService.savedata(url,criteria)
  }

  public savedraft(criteria):Observable<any> {
    return this.menudbService.savedraft(criteria)
  }

  public deletedata(url,idfied, selectedid):Observable<any> {
    return this.menudbService.deletedata(url,idfied, selectedid)
  }
  
  menudetailsservice(moduleid:any): Observable<IMenulist> {

    return this.menudbService.MenulistService(moduleid)
  }

  excelservice(exceldata , menudata , type): Observable<any> {
    return this.menudbService.exceldbservice(exceldata , menudata , type)
  }

  mailgenerationservice(menudetails,pdfcsv,mailid): Observable<any> {
    return this.menudbService.mailgenrationdbservice(menudetails,pdfcsv,mailid)
  }

  reportviewaddnewformsetting(menudetails): Observable<any> {
    return this.menudbService.reportviewaddnewsetting(menudetails)
  }

  reportviewreportsetting(menudetails): Observable<any> {
    return this.menudbService.reportviewreportsettingdbservice(menudetails)
  }

  reportviewformsetting(menudetails,reportid): Observable<any> {
    return this.menudbService.reportviewformsettingdbservice(menudetails,reportid)
  }

  reportviewsettingsave(savedata): Observable<any> {
    return this.menudbService.reportviewsavesettinggdbservice(savedata)
  }

  reportschedularreport(menudetails): Observable<any> {
    return this.menudbService.reportschedularreportdbservice(menudetails)
  }

  reportschedularsave(savedata): Observable<any>{
    return this.menudbService.reportschedularsavedbservice(savedata)
  }

  commentlistservice(commentcriteria): Observable<any>{
    return this.menudbService.commentlistdbservice(commentcriteria)
  }

  commentnewsaveservice(adddnewcriteria): Observable<any>{
    return this.menudbService.commentnewsavedbservice(adddnewcriteria)
  }
}
