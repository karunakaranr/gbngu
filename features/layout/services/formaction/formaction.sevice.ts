import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { formactiondbservice } from './../../dbservices/modulelist/formaction/formactionDB.service';
@Injectable({
  providedIn: 'root',
})
export class formactionservice {

  constructor(public formactionDBservice: formactiondbservice) { }

  public formactionservice(url,criteria): Observable<any> {
    return this.formactionDBservice.formactionService(url,criteria);
  }

  public MysettingGet(): Observable<any> {
    return this.formactionDBservice.MySettingdbget();
  }

  public SettingSave(savedata){
    return this.formactionDBservice.Settingdbsave(savedata);
  }

}