import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoGbComponent } from './lo-gb.component';

describe('LoGbComponent', () => {
  let component: LoGbComponent;
  let fixture: ComponentFixture<LoGbComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoGbComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoGbComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
