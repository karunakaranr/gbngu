import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoHgComponent } from './lo-hg.component';

describe('LoHgComponent', () => {
  let component: LoHgComponent;
  let fixture: ComponentFixture<LoHgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoHgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoHgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
