export type reportscheduler = Root2[]

export interface Root2 {
  SchedulerId: number
  SchedulerName: string
  JobDefineId: number
  JobName: string
  MailTemplateId: number
  ActionId: number
  ReplyTo: string
  ToDeliveryType: number
  ToContentType: number
  To: string
  MailCCDeliveryType: number
  MailCCContentType: number
  MailCC: string
  MailBCCDeliveryType: number
  MailBCCContentType: number
  MailBCC: string
  ExportFormat: number
  ReportType: number
  Type: number
  ActionType: number
  ActionForType: number
  ReportId: number
  ReportName: string
  ReportFormatId: number
  ReportFormatName: string
  MailTemplateName: string
  FileId: number
  FileName: string
  CriteriaConfigId: number
  CriteriaConfigName: string
  EventTypeActionDTO: EventTypeActionDto
  EventTypeId: number
  EventTypeName: string
  GroupByFields: string
  ContentTo: string
  ContentCC: string
  ContentBcc: string
  ActionName: string
}

export interface EventTypeActionDto {
  EventTypeActionId: number
  EventTypeActionNature: number
  EventTypeId: number
  EventTypeCode: string
  EventTypeName: string
  TriggerReportId: number
  TriggerReportCode: string
  TriggerReportName: string
  EventTypeActionDetailArray: EventTypeActionDetailArray[]
  MailId: number
  MailDescription: any
  EventTypeActionCreatedById: number
  EventTypeActionCreatedOn: string
  EventTypeActionModifiedById: number
  EventTypeActionModifiedOn: string
  EventTypeActionSortOrder: number
  EventTypeActionStatus: number
  EventTypeActionVersion: number
  EventTypeActionSourceType: number
}

export interface EventTypeActionDetailArray {
  EventTypeActionDetailId: number
  EventTypeActionId: number
  EventTypeActionDetailSlNo: number
  ActionId: number
  ActionDescription: string
  EventTypeActionDetailDescription: string
  EventTypeActionDetailIsActive: number
  EventTypeActionDetailIsDirectCall: number
  EventTypeActionDetailIsSchedulerCall: number
  EventTypeActionDetailEvalCondition: string
  EventTypeActionDetailSourceType: number
}
