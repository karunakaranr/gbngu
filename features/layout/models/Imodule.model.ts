
export interface IModulelist {
	ModuleId: number;
	ModuleCode: string;
	ModuleName: string;
	DefaultMenuId: number;
	DefaultMenuCode: string;
	DefaultMenuName: string;
	RoleModuleDefaultMenuId: number;
	RoleModuleDefaultMenuCode: string;
	RoleModuleDefaultMenuName: string;
	RoleModuleDefaultMenuReportId: number;
	RoleModuleDefaultMenuWebFormId: number;
	RoleModuleDefaultMenuWebFormName: string;
	RoleModuleDefaultMenuWebFormUrl: string;
	RoleModuleDefaultMenuHelpUrl: string;
	RoleModuleDefaultMenuImageId: number;
	UserDefaultMenuId: number;
	UserDefaultMenuCode: string;
	UserDefaultMenuName: string;
	UserDefaultMenuReportId: number;
	UserDefaultMenuWebFormId: number;
	UserDefaultMenuWebFormName: string;
	UserDefaultMenuWebFormUrl: string;
	UserDefaultMenuHelpUrl: string;
	UserDefaultMenuImageId: number;
}

export interface Listofmodule {
	ModuleDetails:
	{
		ModuleId: number;
		ModuleCode: string;
		ModuleName: string;
		DefaultMenuId: number;
		DefaultMenuCode: string;
		DefaultMenuName: string;
		RoleModuleDefaultMenuId: number;
		RoleModuleDefaultMenuCode: string;
		RoleModuleDefaultMenuName: string;
	}[];
}

export interface Imoduleid {
	ModuleId: number;
}

export class IMenuDto{
	'MenuOption': number;
  }


