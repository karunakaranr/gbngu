import { Injectable } from '@angular/core';
import { Gbaboutdbservice } from '../../dbservices/gbabout/gbaboutdb.service';
@Injectable({
  providedIn: 'root',
})
export class GbaboutService {

  constructor(public Gbaboutdbservice: Gbaboutdbservice) { }

  public getaboutservice() {
    return this.Gbaboutdbservice.AboutDbServices();
  }

}
