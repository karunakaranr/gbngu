import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxsModule } from '@ngxs/store';
import { NgSelectModule } from '@ng-select/ng-select';
import { AgGridModule } from 'ag-grid-angular';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule} from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { AgmCoreModule } from '@agm/core';
import { AgmDirectionModule } from 'agm-direction'
import { GridModule } from 'features/common/shared/screens/gbgrid/gbgrid.module';
import { GbaboutRoutingModule } from './gbabout-routing.module';
import { GbaboutComponent } from './components/gbabout/gbabout.component';

@NgModule({
  declarations: [
    
    GbaboutComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AgGridModule,
    GridModule,
    AgmDirectionModule,
    NgSelectModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatListModule,
    GbaboutRoutingModule
  ],
  exports: [
    
    GbaboutComponent
  ],
})
export class GbaboutModule {
 
}
