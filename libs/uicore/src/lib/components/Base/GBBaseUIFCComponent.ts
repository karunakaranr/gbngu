import { Component, ElementRef, Input, Renderer2, Self } from '@angular/core';
import { ControlValueAccessor, FormBuilder, NgControl } from '@angular/forms';

import { GBFormControl } from '../../classes/GBFormControl';
import { IField } from '../../interfaces/ibasefield';
import { GBBaseUIComponent } from './GBBaseUIComponent';
import { MatDialog } from '@angular/material/dialog';
import { FormService } from '../../services/FormServiceCall';
@Component({
  template: ''
})
export abstract class GBBaseUIFCComponent extends GBBaseUIComponent implements ControlValueAccessor {
  @Input() gblabel: string | string[];
  @Input() gbplaceholder: string;
  @Input() gbtype: string;
  @Input() gbwidth:string;
  @Input() gbpicklist;
  constructor(public service: FormService,public renderer: Renderer2,public dialog: MatDialog,public formBuilder: FormBuilder,@Self() public ngControl?: NgControl,public el?: ElementRef) {
    super();
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  get gbControl(): GBFormControl {
    return this.ngControl?.control as GBFormControl;
  }

  get angControl(): NgControl {
    return this.ngControl as NgControl;
  }

  get gbControlName(): string {
    return 'addField';
  }

  get Field(): IField | null {
    if (!this.gbControl) {
      return null;
    }
    if (this.gblabel!= undefined || this .gbplaceholder!= undefined || this.gbwidth!=undefined) {
      // console.log("this.gbControl.field",this.gbControl.field)
      this.gbControl.field = {  Label: this.gblabel, Placeholder: this.gbplaceholder, width: this.gbwidth, Type: this.gbtype ,PickLists:this.gbpicklist } as IField;
    }
    return this.gbControl?.field as IField;
  }

  get value(): any {
    return this.gbControl?.value;
  }

  set value(val: any) {
    this.gbControl?.patchValue(val);
  }

  propagateChange = (_: any) => { };

  writeValue(obj: any): void {
    // if (obj !== undefined) {
    //   this.value = obj;
    // }
  }
  registerOnChange(fn: any): void {
    // this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    // throw new Error("Method not implemented.");
  }
  setDisabledState?(isDisabled: boolean): void {
    // throw new Error("Method not implemented.");
  }
}
