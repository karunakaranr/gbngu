import { Injectable } from "@angular/core";
import { GBHttpService } from "@goodbooks/gbcommon";

@Injectable({
    providedIn: 'root',
  })
  export class FormdbService{
    // accountbalance: any;

    constructor(public http: GBHttpService) {}
    

    public getcheckbalance() {
        let balance =
    
        {
          "SectionCriteriaList": [
            {
              "SectionId": 0,
              "AttributesCriteriaList": [
                {
                  "FieldName": "ClientId",
                  "OperationType": 5,
                  "FieldValue": "6082d0b3af748ca57406c6f1090d4e72",
                  "InArray": null,
                  "JoinType": 0
                },
                {
                  "FieldName": "SecretKey",
                  "OperationType": 5,
                  "FieldValue": "edac734fc7c636fa5c627a9003a20518",
                  "InArray": null,
                  "JoinType": 0
                },
                {
                  "FieldName": "TestId",
                  "OperationType": 5,
                  "FieldValue": "1",
                  "InArray": null,
                  "JoinType": 0
                },
                {
                  "FieldName": "requestUUID",
                  "OperationType": 5,
                  "FieldValue": "ABC123",
                  "InArray": null,
                  "JoinType": 0
                },
                {
                  "FieldName": "serviceRequestId",
                  "OperationType": 5,
                  "FieldValue": "OpenAPI",
                  "InArray": null,
                  "JoinType": 0
                },
                {
                  "FieldName": "serviceRequestVersion",
                  "OperationType": 5,
                  "FieldValue": "1.0",
                  "InArray": null,
                  "JoinType": 0
                },
                {
                  "FieldName": "channelId",
                  "OperationType": 5,
                  "FieldValue": "TXBAPI",
                  "InArray": null,
                  "JoinType": 0
                },
                {
                  "FieldName": "corpCode",
                  "OperationType": 5,
                  "FieldValue": "DEMOTEST1",
                  "InArray": null,
                  "JoinType": 0
                },
                {
                  "FieldName": "corpAccNum",
                  "OperationType": 5,
                  "FieldValue": "********9499978",
                  "InArray": null,
                  "JoinType": 0
                },
                {
                  "FieldName": "checksum",
                  "OperationType": 5,
                  "FieldValue": "lepvispegu",
                  "InArray": null,
                  "JoinType": 0
                }
              ],
              "OperationType": 0
            }
          ]
        }
        return this.http.httppost("/as/EBank.svc/BankAccountBalance", balance)
      }
    
  }