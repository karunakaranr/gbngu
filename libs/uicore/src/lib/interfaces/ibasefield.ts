import { AbstractControl, FormArray, FormGroup } from '@angular/forms';

export interface IUIField {
  field?: IField;
  readonly form?: FormGroup | FormArray;
  readonly formControl?: AbstractControl;
  readonly data?: any;
}

export interface IBaseField {
  ObjectFields: IField[]
}

export interface IField {
  Name: string;
  Description: string;
  IsSystem: boolean;
  FreezeColumns:number
  Mask:string
  // EntityID: IBaseEntity;
  Formula: string;
  EnableAnalysis: boolean;
  Length: number;
  Precision: number;
  Children:IField[]
  Type: string;
  width:string;
  Label: string | string[];
  Tooltip?: string;
  Placeholder?: string;
  Width:number; 
  Required: boolean;
  ValidRegEx: string;
  MinValue?: any;
  MaxValue?: any;
  DefaultValue: any;
  PostDefaultValue?:any;
  PickLists: string[] | number[] | IPickListValue[];
  Grid: IGrid[];
  Height:number;
  FormPicklist: IPickListDetail;
  IsFieldRequired: boolean;
  IsAddrowRequired: boolean;
  IsEditRequired: boolean;
  IsDeleterowRequired: boolean;
  DeleteUrl:string;
  DeleteParameterFieldName:string;
  DeleteParameterFieldValue:string;
  // Events
  BeforeChange: string;
  AfterChange: string;
  MaxLength:number;
  MinLength:number;
  LinkId:string;
  ReadOnly:boolean;
  PostData:boolean;
  isKeyField:boolean;
  LabelWidth:number;
  DependentField:string;
  SLNOField:string;
  Rules: IRule[];
}

export interface IGrid {
  Label: string | number;
  value: string;
  Width?: number;
  Type?: string;
  SNo: number;
  IsVisible:boolean;
  MaxLength?:number;
  Required:boolean;
  PostData:boolean;
  DefaultId:boolean;
  Disabled:true;
  PicklistDetail?: IPickListDetail;
  sticky?: boolean;
  left?: number;
  PickLists: string[] | number[] | IPickListValue[];
  DependentFieldRelation: string[];
  DependendChangeField: string[];
  DefaultValue: string | number;
  PicklistRetrivalURL?:string;
  PicklistRetrivalIdField?:string;
  DependendField:string;
  DependendValue:string | number;
  DependentDefaultValue:string | number;
  DependendFieldName:string;
  DependendFieldValue: string | number;
  DisableFieldBasedOnDependent: boolean;
  DependentChangeFields:string[];
  DependentChangeFieldName:string[],
  DependentChangeFieldValue:any,
  DependentChangeDefaultValue:any,
  Placeholder:string;
  Expansion:boolean;
  Mask:string;
}

export interface IPickListDetail {
  SLno: any;
  PicklistUrl: string;
  PlaceHolder:string
  PicklistDisplayText: string;
  PicklistSearchFields: string;
  Multiple: boolean;
  LinkId: string;
  LinkValue: string;
  PicklistSelection: string;
  PicklistDisplayLabel:string;
  PicklistTitle: string;
  Label:string;
  Width:number;
  AddNew:boolean;
  MaxLength:number;
  ColumnWidth:string;
  ColumnType:string;
  isKeyField:boolean;
  PicklistCriteria:string;
  ReadOnly:boolean;
  LabelWidth:number;
  PickListWithCommas:true;
}

export interface IPickListValue {
  key: string | number;
  value: string;
  PlaceHolder?:boolean;
}

export interface IScreen {
  ID: string;
  Name: string;
  Description: string;
  EntityID: IEntity;
  IsSystem: boolean;
  ScreenType: string;
  ViewID: IView;

  // Events
  AfterLoad: string;
  BeforeSave: string;
  AfterSave: string;
  BeforeDelete: string;
  AfterValidation: string;

  // Misc
  HelpURL: string;
  MultimediaURL: string;
  PrecompliledScreenPage: string;
  OverrideScreenPage: string;
  ObjectFields: IField[];

  Rules: IRule[];
}

export interface IDataType {
  Name: string;
  RendererClass: string;
}

export interface IModule {
  Name: string;
  Description: string;
  IsSystem: string;
  HelpURL: string;
  MultimediaURL: string;
  PartnerID: string;
}

export interface IEntity {
  ID: string;
  ModuleID: IModule;
  Name: string;
  Description: string;
  ImageField: string;
  IDField: IField;
  BIZTransactionTypes: string;
  IsSystem: boolean;
  RestEndpoint: string;
  HelpURL: string;
  MultimediaURL: string;
  Attachments: boolean;
  Activities: boolean;
  Communication: boolean;
  DefaultDocumentLayout: string;
  LookupFields: IField[];
}

export interface IRelationship {
  Name: string;
  FromEntityID: IEntity;
  ToEntityID: IEntity;
  RelationshipType: string;
  IsSystem: boolean;
}

export interface IView {
  Name: string;
  Description: string;
  IsSystem: boolean;
  EntityID: IEntity;
  ImageField: IField;
  Header1Field: IField;
  Header2Field: IField;
  Editable: boolean;
}

export interface IRule {
  ID: string;
  Name: string;
  Condition: ICondition;
}

export interface ICondition {
  Fact: string;
  Operator: EnumComparers;
  ValueType: EnumValueType;
  Value: unknown;
  Truthy: IAction | ICondition;
  Falsy: IAction | ICondition;
}

export interface IAction {
  Name: string;
}

export interface IActionShowError extends IAction {
  Field: string;
  ErrorMessage: string;
}

export interface IActionSetVisible extends IAction {
  Field: string;
  Visible: boolean;
}

export interface IActionSetValue extends IAction {
  Field: string;
  ValueType: EnumValueType;
  Value: unknown | IFormula;
}

export interface IActionSetRequired extends IAction {
  Field: string;
  Required: boolean;
}

export interface IActionSetLock extends IAction {
  Field: string;
  Locked: boolean;
}

export interface IFormula {
  Field: string;
  Operator: EnumOperators
}

export const enum EnumComparers {
  AreEqual = 0,
  AreNotEqual = 1,
  IsLessThan = 2,
  IsLessThanOrEqual  = 3,
  IsGreaterThan = 4,
  IsGreaterThanEqual = 5,
  ContainsData = 6,
  DoesNotContainData = 7,
  BeginsWith = 8,
  DoesNotBeginWith = 9,
  EndsWith = 10,
  DoesNotEndWith = 11
}

export const enum  EnumOperators {
  Add = 0,
  Subtract = 1,
  Multiply = 2,
  Divide = 3
}

export const enum EnumValueType {
  Constant = 0,
  Field = 1,
  Formula = 2
}

export interface IPicklistEmitValue{
  SelectedData: number
  id: string
  Selected: any
  value:string
}