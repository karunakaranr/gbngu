import { HttpClient } from '@angular/common/http';
import { AbstractControl, AbstractControlOptions, AsyncValidatorFn, FormGroup, ValidationErrors, ValidatorFn, Validators } from '@angular/forms';

// import { GBBaseDataService, GBBaseDataServiceWN, GBDataStateActionFactory, GBDataStateActionFactoryWN } from '@goodbooks/gbdata';
import { GBBaseDataService, GBBaseDataServiceWN } from './../../../../gbdata/src/lib/services/gbbasedata.service';
import { GBDataStateActionFactory, GBDataStateActionFactoryWN } from './../../../../gbdata/src/lib/store/gbdata.actions'
import { Select, Store } from '@ngxs/store';

import { Observable, of } from 'rxjs';
import { map } from 'rxjs/operators';
import { IField, IScreen } from '../interfaces/ibasefield';

import { GBFormControl } from './GBFormControl';
import { GBFormService } from './../services/gbform.service'
//import * as assetsdata from './../../../../../apps/Goodbooks/Goodbooks/src/assets/mockdata/_general/screens.json';

export class GBBaseFormGroup extends FormGroup {
  myvalidatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions;
  public ErrMessages: { [key: string]: string } = {};

  constructor(
    controls: { [key: string]: AbstractControl; },
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]
  ) {
    super(controls, validatorOrOpts, asyncValidator);
  }

  public addCustomvalidator(
    name: string,
    customValidatorOrOpts: ValidatorFn | ValidatorFn[] | AbstractControlOptions,
    errMessage?: string
  ) {
    let newValidFns: ValidatorFn[] = [];
    if (Array.isArray(this.myvalidatorOrOpts)) {
      newValidFns = newValidFns.concat(this.myvalidatorOrOpts);
    }
    if (Array.isArray(customValidatorOrOpts)) {
      newValidFns = newValidFns.concat(customValidatorOrOpts);
    }
    else if (typeof customValidatorOrOpts === 'function') {
      newValidFns.push(customValidatorOrOpts);
    }
    this.ErrMessages[name] = errMessage || '';
    this.setValidators(newValidFns);
    this.updateValueAndValidity();
  }

  public get hasCustomError(): boolean {
    let retValue = false;
    const controlErrors: ValidationErrors | null = this.errors;
    if (controlErrors != null) {
      Object.keys(controlErrors).forEach(element => {
        if (this.ErrMessages[element]) {
          retValue = true;
        }
      });
    }
    return retValue;
  }

  public get customErrorMessages(): string {
    let retMsg = '';
    const controlErrors: ValidationErrors | null = this.errors;
    if (controlErrors != null) {
      Object.keys(controlErrors).forEach(element => {
        if (this.ErrMessages[element]) {
          retMsg += this.ErrMessages[element] + '; ';
        }
      });
    }
    return retMsg;
  }

  public reset() {
    super.reset();
    Object.keys(this.controls).forEach(key => {
      const fc = this.controls[key] as GBFormControl;
      fc.setValue(fc.field.DefaultValue);
    });
  }
}

export class GBFormSubGroup extends GBBaseFormGroup {

  constructor(public field: IField, controls: { [key: string]: AbstractControl; }, validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions, asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]) {
    super(controls, validatorOrOpts, asyncValidator);
  }

}

export class GBFormGroup extends GBBaseFormGroup {
  // fs: GBFormService;
  public get bFormReady(): boolean {
    return (Object.keys(this.controls).length > 0);
  }
  constructor(
    private http: HttpClient,
    public formid: string,
    // public formservice: GBFormService,
    controls: { [key: number]: AbstractControl; },
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]
  ) {
    super(controls, validatorOrOpts, asyncValidator);
    // this.fs = new GBFormService(http);
    this.setFormGroup();
  }

  private _getFormData(id: string): Observable<IScreen> {
    const jsonFile = `assets/FormJSONS/` + id + `.json`;
    return this.http.get(jsonFile)
      .pipe(
        map(res => {
          console.log("JSON Detail:", res)
          return (res as IScreen);
        })
      );
  }

  setFormGroup() {
    this.Initialize().subscribe();
  }

  public Initialize(): Observable<boolean> {
    return this._getFormData(this.formid).pipe(map(res => {
      const form: IScreen = res;
      const formFields = form.ObjectFields;
      for (const itm of formFields) {
        const ibf = itm; // formFields[i] as IField;
        if (ibf.Type) {
          switch (ibf.Type.toLowerCase()) {
            case 'address': {
              this.addControl(ibf.Name, new GBFormSubGroup(ibf, {
                // PartyCode: new GBFormControl({ Name: 'PartyCode', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                // PartyName: new GBFormControl({ Name: 'PartyName', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                // PartyBranchCode: new GBFormControl({ Name: 'PartyBranchCode', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                // PartyBranchName: new GBFormControl({ Name: 'PartyBranchName', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                AddressId: new GBFormControl({ Name: 'AddressId', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: -1 } as IField, ''),
                AddressObjectTypeId: new GBFormControl({ Name: 'AddressObjectTypeId', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: -1 } as IField, ''),
                AddressObjectId: new GBFormControl({ Name: 'AddressObjectId', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: -1 } as IField, ''),
                AddressAddressType: new GBFormControl({ Name: 'AddressAddressType', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 0 } as IField, ''),
                AddressAddressNature: new GBFormControl({ Name: 'AddressAddressNature', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: "0" } as IField, ''),
                AddressLine1: new GBFormControl({ Name: 'AddressLine1', Type: 'string', Label: '', Required: false, Placeholder: "Enter Address Line1", DefaultValue: "NONE", Width: 250 } as IField, ''),
                AddressLine2: new GBFormControl({ Name: 'AddressLine2', Type: 'string', Label: '', Required: false, Placeholder: "Enter Address Line2", DefaultValue: "NONE", Width: 250 } as IField, ''),
                AddressLine3: new GBFormControl({ Name: 'AddressLine3', Type: 'string', Label: '', Required: false, Placeholder: "Enter Address Line3", DefaultValue: "NONE", Width: 250 } as IField, ''),
                AddressLine4: new GBFormControl({ Name: 'AddressLine4', Type: 'string', Label: '', Required: false, Placeholder: "Enter Address Line4", DefaultValue: "NONE", Width: 250 } as IField, ''),
                AddressLine5: new GBFormControl({ Name: 'AddressLine5', Type: 'string', Label: '', Required: false, Placeholder: "Enter Address Line5", DefaultValue: "NONE", Width: 250 } as IField, ''),
                AddressZipCode: new GBFormControl({ Name: 'AddressZipCode', Type: 'string', Label: '', Required: true, Placeholder: "Enter Zip Code", DefaultValue: "0" } as IField, ''),
                CityId: new GBFormControl({ Name: 'CityId', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: -1 } as IField, ''),
                // CityCode: new GBFormControl({ Name: 'CityCode', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                CityName: new GBFormControl({ Name: 'CityName', Type: 'string', Label: '', Required: true, Placeholder: "Select City", DefaultValue: "NONE" } as IField, ''),
                CountryId: new GBFormControl({ Name: 'CountryId', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: -1 } as IField, ''),
                // CountryCode: new GBFormControl({ Name: 'CountryCode', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                CountryName: new GBFormControl({ Name: 'CountryName', Type: 'string', Label: '', Required: true, Placeholder: "Select Country", DefaultValue: "NONE" } as IField, ''),
                // CountryISDCode: new GBFormControl({ Name: 'CountryISDCode', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "", Width: 80 } as IField, ''),
                AddressPhone: new GBFormControl({ Name: 'AddressPhone', Type: 'number', Label: '', Required: false, Placeholder: "Enter Phone Number", DefaultValue: 0 } as IField, ''),
                AddressMobile: new GBFormControl({ Name: 'AddressMobile', Type: 'number', Label: '', Required: false, Placeholder: "Enter Mobile Number", DefaultValue: 0 } as IField, ''),
                AddressFax: new GBFormControl({ Name: 'AddressFax', Type: 'number', Label: '', Required: false, Placeholder: "Enter Fax Number", DefaultValue: 0 } as IField, ''),
                AddressWeb: new GBFormControl({ Name: 'AddressWeb', Type: 'number', Label: '', Required: false, Placeholder: "Enter Web Address", DefaultValue: 0 } as IField, ''),
                AddressGeoCode: new GBFormControl({ Name: 'AddressGeoCode', Type: 'number', Label: '', Required: false, Placeholder: "Enter Geo Code", DefaultValue: 0 } as IField, ''),
                AddressMail: new GBFormControl({ Name: 'AddressMail', Type: 'number', Label: '', Required: false, Placeholder: "Enter Mail Id", DefaultValue: 0, Width: 250 } as IField, ''),
                AddressVATTINNumber: new GBFormControl({ Name: 'AddressVATTINNumber', Type: 'number', Label: '', Required: false, Placeholder: "Enter TIN Number", DefaultValue: 0 } as IField, ''),
                AddressVATCSTNumber: new GBFormControl({ Name: 'AddressVATCSTNumber', Type: 'number', Label: '', Required: false, Placeholder: "Enter CST Number", DefaultValue: 0 } as IField, ''),
                AddressPAN: new GBFormControl({ Name: 'AddressPAN', Type: 'number', Label: '', Required: false, Placeholder: "Enter PAN", DefaultValue: 0 } as IField, ''),
                AddressTAN: new GBFormControl({ Name: 'AddressTAN', Type: 'number', Label: '', Required: false, Placeholder: "Enter TAN", DefaultValue: 0 } as IField, ''),
                AddressServiceTaxNumber: new GBFormControl({ Name: 'AddressServiceTaxNumber', Type: 'number', Label: '', Required: false, Placeholder: "Enter Service Tax Number", DefaultValue: 0 } as IField, ''),
                AddressExciseDivision: new GBFormControl({ Name: 'AddressExciseDivision', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 0 } as IField, ''),
                AddressExciseRange: new GBFormControl({ Name: 'AddressExciseRange', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 0 } as IField, ''),
                AddressExciseCodeNumber: new GBFormControl({ Name: 'AddressExciseCodeNumber', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 0 } as IField, ''),
                AddressExciseNotification: new GBFormControl({ Name: 'AddressExciseNotification', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 0 } as IField, ''),
                AddressExciseCircle: new GBFormControl({ Name: 'AddressExciseCircle', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 0 } as IField, ''),
                AddressExciseCollectrate: new GBFormControl({ Name: 'AddressExciseCollectrate', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 0 } as IField, ''),
                AddressExciseRegisterNumber: new GBFormControl({ Name: 'AddressExciseRegisterNumber', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                AddressTDSCircle: new GBFormControl({ Name: 'AddressTDSCircle', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 1 } as IField, ''),
                StandardRegionId: new GBFormControl({ Name: 'StandardRegionId', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: -1 } as IField, ''),
                // StandardRegionCode: new GBFormControl({ Name: 'StandardRegionCode', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                // StandardRegionName: new GBFormControl({ Name: 'StandardRegionName', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                LocationId: new GBFormControl({ Name: 'LocationId', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: -1 } as IField, ''),
                // LocationCode: new GBFormControl({ Name: 'LocationCode', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                // LocationName: new GBFormControl({ Name: 'LocationName', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                AddressGSTIn: new GBFormControl({ Name: 'AddressGSTIn', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: 0 } as IField, ''),
                StateId: new GBFormControl({ Name: 'StateId', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: -1 } as IField, ''),
                // StateCode: new GBFormControl({ Name: 'StateCode', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                StateName: new GBFormControl({ Name: 'StateName', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                // StateGSTStateCode: new GBFormControl({ Name: 'StateGSTStateCode', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                // StateTINStart: new GBFormControl({ Name: 'StateTINStart', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                // UpdateShippingAddress: new GBFormControl({ Name: 'UpdateShippingAddress', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                // TempPartyId: new GBFormControl({ Name: 'TempPartyId', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "-1" } as IField, ''),
                // TempPartyProductId: new GBFormControl({ Name: 'TempPartyProductId', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "-1" } as IField, ''),
                // AddressCreatedById: new GBFormControl({ Name: 'AddressCreatedById', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "-1",PostData:false } as IField, ''),
                // AddressCreatedOn: new GBFormControl({ Name: 'AddressCreatedOn', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "",PostData:false } as IField, ''),
                // AddressModifiedById: new GBFormControl({ Name: 'AddressModifiedById', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "-1",PostData:false } as IField, ''),
                // AddressModifiedOn: new GBFormControl({ Name: 'AddressModifiedOn', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "",PostData:false } as IField, ''),
                // AddressSortOrder: new GBFormControl({ Name: 'AddressSortOrder', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                AddressVersion: new GBFormControl({ Name: 'AddressVersion', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 1 } as IField, ''),
                // AddressSourceType: new GBFormControl({ Name: 'AddressSourceType', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                AddressStatus: new GBFormControl({ Name: 'AddressStatus', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 1 } as IField, ''),
                // AddressGeolocation: new GBFormControl({ Name: 'AddressGeolocation', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                // AddressBorder: new GBFormControl({ Name: 'AddressBorder', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "",PostData:false } as IField, ''),
              }));
              break;
            }
            case 'contact':{
              this.addControl(ibf.Name, new GBFormSubGroup(ibf, {
                PartyCode: new GBFormControl({ Name: 'PartyCode', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                PartyName: new GBFormControl({ Name: 'PartyName', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                PartyBranchCode: new GBFormControl({ Name: 'PartyBranchCode', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                PartyBranchName: new GBFormControl({ Name: 'PartyBranchName', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "" } as IField, ''),
                DefaultImageId: new GBFormControl({Name: 'DefaultImageId', Type: 'number', Label: '', Required:false, Placeholder: "", DefaultValue:-1} as IField),
                RelationShip: new GBFormControl({Name: 'RelationShip', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "None" } as IField),
                ContactSexName: new GBFormControl({Name: 'ContactSexName', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "None"} as IField),
                ContactSex: new GBFormControl({Name: 'ContactSex', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: "None"} as IField),
                ContactId: new GBFormControl({Name: 'ContactId', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 0} as IField),
                ContactModifiedById: new GBFormControl({Name: 'ContactModifiedById', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: -1} as IField),
                EntityId: new GBFormControl({Name: 'EntityId', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: "-1"} as IField),
                ContactObjectId: new GBFormControl({Name: 'ContactObjectId', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: "-1"} as IField),
                ContactContactNature: new GBFormControl({Name: 'ContactContactNature', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 0} as IField),
                ContactName: new GBFormControl({Name: 'ContactName', Type: 'string', Label: '', Required: false, Placeholder: "Enter Contact Name", DefaultValue: ""} as IField),
                ContactFirstName: new GBFormControl({Name: 'ContactFirstName', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: ""} as IField),
                ContactMiddleName: new GBFormControl({Name: 'ContactMiddleName', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: ""} as IField),
                ContactLastName: new GBFormControl({Name: 'ContactLastName', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: ""} as IField),
                ContactSalutation: new GBFormControl({Name: 'ContactSalutation', Type: 'string', Label: '', Required: false, Placeholder: "Enter Salutation", DefaultValue: "Mr."} as IField),
                ContactDob: new GBFormControl({Name: 'ContactDob', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "/Date(1713398400000)/"} as IField),
                ContactMailId: new GBFormControl({Name: 'ContactMailId', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: "-1"} as IField),
                ContactPhoneNo: new GBFormControl({Name: 'ContactPhoneNo', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: ""} as IField),
                ContactMobileNo: new GBFormControl({Name: 'ContactMobileNo', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: ""} as IField),
                ContactRelationShip: new GBFormControl({Name: 'ContactRelationShip', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: ""} as IField),
                ContactDesignation: new GBFormControl({Name: 'ContactDesignation', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: ""} as IField),
                ContactCompany: new GBFormControl({Name: 'ContactCompany', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: ""} as IField),
                ContactDefaultImageId: new GBFormControl({Name: 'ContactDefaultImageId', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: "-1"} as IField),
                ContactLinkType: new GBFormControl({Name: 'ContactLinkType', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: ""} as IField),
                ReportingToId: new GBFormControl({Name: 'ReportingToId', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: "-1"} as IField),
                ContactImageViewUrl: new GBFormControl({Name: 'ContactImageViewUrl', Type: 'string', Label: '', Required: false, Placeholder: "", DefaultValue: ""} as IField),
                ContactCreatedById: new GBFormControl({Name: 'ContactCreatedById', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: -1} as IField),
                ContactSortOrder: new GBFormControl({Name: 'ContactSortOrder', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 9999} as IField),
                ContactStatus: new GBFormControl({Name: 'ContactStatus', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 1} as IField),
                ContactVersion: new GBFormControl({Name: 'ContactVersion', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 1} as IField),
                ContactSourceType: new GBFormControl({Name: 'ContactSourceType', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 5} as IField),
                ContactIsUser: new GBFormControl({Name: 'ContactIsUser', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: 1} as IField),
                UserLinkId: new GBFormControl({Name: 'UserLinkId', Type: 'number', Label: '', Required: false, Placeholder: "", DefaultValue: -1} as IField),
              }));
              break;
            }
            case 'datetime': {
              break;
            }
            case 'mobile': {
              this.addControl(ibf.Name, new GBFormSubGroup(ibf, {
                Code: new GBFormControl({
                  Name: 'Code', Type: 'string', Label: 'Title', Tooltip: '', Placeholder: '--Select--',
                  Required: false, ValidRegEx: '', PickLists: [
                    '+91', '+92', '+93']
                } as IField, ''),
                Mobileno: new GBFormControl({
                  Name: 'mobile', Type: 'string', Label: 'Mobile', Tooltip: '', Placeholder: 'MobileNo', Required: false,
                  ValidRegEx: ''
                } as IField, '')
              }));
              break;
            }
            case 'phone': {
              this.addControl(ibf.Name, new GBFormSubGroup(ibf, {
                Code: new GBFormControl({
                  Name: 'Code', Type: 'string', Label: 'Title', Tooltip: '', Placeholder: '--Select--',
                  Required: false, ValidRegEx: '', PickLists: [
                    '+91', '+92', '+93']
                } as IField, ''),
                DisCode: new GBFormControl({
                  Name: 'DisCode', Type: 'string', Label: 'Code', Tooltip: '', Placeholder: 'Code', Required: false,
                  ValidRegEx: ''
                } as IField, ''),
                Phoneno: new GBFormControl({
                  Name: 'Phoneno', Type: 'string', Label: 'phone', Tooltip: '', Placeholder: 'Phone', Required: false,
                  ValidRegEx: ''
                } as IField, '')
              }));
              break;
            }
            default: {
              this.addControl(ibf.Name, new GBFormControl(ibf, ibf.DefaultValue));
              break;
            }
          }
        }
      }
      return true;
    }));
  }
}

export class GBDataFormGroup<T> extends GBFormGroup {
  constructor(
    http: HttpClient,
    public formid: string,
    controls: { [key: string]: AbstractControl; },
    public ds: GBBaseDataService<T>,
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]
  ) {
    super(http, formid, controls, validatorOrOpts, asyncValidator);
    // this.fs = new GBFormService(http);
    // this.fs.setFormGroup(this);
  }
  public getData(id: string) {
    this.ds.getData(id);
  }
  public clear() {
    this.reset();
  }

  public delete() {
    const pdata: T = this.value as T;
    this.ds.deleteData(pdata[this.ds.IdField]);
    this.reset();
  }
  public save() {
    const pdata: T = this.value as T;
    this.ds.saveData(pdata);

  }
}

export class GBDataFormGroupWN<T> extends GBDataFormGroup<T> {
  constructor(
    http: HttpClient,
    public formid: string,
    controls: { [key: string]: AbstractControl; },
    public ds: GBBaseDataServiceWN<T>,
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]
  ) {
    super(http, formid, controls, ds, validatorOrOpts, asyncValidator);
    // this.fs = new GBFormService(http);
    // this.fs.setFormGroup(this);

    // this.ds.getAll().subscribe(res => {
    //   console.log("KLK",res)
    //   // this.ds.moveFirst().subscribe(r => {
    //     // const pk: T = r;
    //     // this.patchValue(pk);
    //   // });
    // });
  }
  public clear() {
    this.reset();
  }
  public moveFirst() {
    this.ds.moveFirst().subscribe(r => {
      const pk: T = r;
      this.patchValue(pk);
    });
  }

  public movePrev() {
    this.ds.movePrev().subscribe(r => {
      const pk: T = r;
      this.patchValue(pk);
    });
  }

  public moveNext() {
    this.ds.moveNext().subscribe(r => {
      const pk: T = r;
      this.patchValue(pk);
    });
  }

  public moveLast() {
    this.ds.moveLast().subscribe(r => {
      const pk: T = r;
      this.patchValue(pk);
    });
  }

}

export class GBDataFormGroupStore<T> extends GBFormGroup {
  // currentData$;//: Observable<T>;
  constructor(
    http: HttpClient,
    public formid: string,
    controls: { [key: string]: AbstractControl; },
    public store: Store,
    public af: GBDataStateActionFactory<T>,
    //public ds: GBBaseDataServiceWN<T>,
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]
  ) {
    super(http, formid, controls, validatorOrOpts, asyncValidator);
  }
  public getData(id: number) {
    this.store.dispatch(this.af.GetData(id));
  }
  public clear() {
    this.store.dispatch(this.af.ClearData())
    this.reset();
  }

  public delete() {
    const pdata: T = this.value as T;
    this.store.dispatch(this.af.DeleteData())
    // this.ds.deleteData(pdata[this.ds.IdField]);
    //this.reset();
  }
  public save() {
    const pdata: T = this.value as T;
    this.store.dispatch(this.af.SaveData(pdata));
  }
}

export class GBDataFormGroupStoreWN<T> extends GBDataFormGroupStore<T> {

  constructor(
    http: HttpClient,
    public formid: string,
    controls: { [key: string]: AbstractControl; },
    public store: Store,
    public af: GBDataStateActionFactoryWN<T>,
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]
  ) {
    super(http, formid, controls, store, af, validatorOrOpts, asyncValidator);
  }

  public getAll(scl) {
    this.store.dispatch(this.af.GetAll(scl));
  }

  public moveFirst() {
    this.store.dispatch(this.af.MoveFirst());
  }

  public movePrev() {
    this.store.dispatch(this.af.MovePrev());
  }

  public moveNext() {
    this.store.dispatch(this.af.MoveNext());
  }

  public moveLast() {
    this.store.dispatch(this.af.MoveLast());
  }

}

export class GBDataListFormGroupStore<T> extends GBDataFormGroupStore<T> {
  constructor(
    http: HttpClient,
    public formid: string,
    controls: { [key: string]: AbstractControl; },
    public store: Store,
    public af: GBDataStateActionFactoryWN<T>,
    validatorOrOpts?: ValidatorFn | ValidatorFn[] | AbstractControlOptions,
    asyncValidator?: AsyncValidatorFn | AsyncValidatorFn[]
  ) {
    super(http, formid, controls, store, af, validatorOrOpts, asyncValidator);
  }

  public getAll(scl) {
    this.store.dispatch(this.af.GetAll(scl));
  }
}