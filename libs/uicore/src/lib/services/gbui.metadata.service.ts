import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IScreen } from '../interfaces/ibasefield';
const screenjson = require('./../assets/FormJSONPath.json');
@Injectable()
export class GBUIMetadataService {
  constructor(
    private http: HttpClient) {

  }

  public getFormData(id: string): Observable<IScreen> {
    const jsonFile = `assets/FormJSONS/`+id+`.json`;
    return this.http.get(jsonFile)
      .pipe(
        map(res => {
          console.log("JSON Detail:", res)
          return (res as IScreen);
        })
      );
  }

  public getAccessControls() {
    //this is to avoid an empty function
  }
}
