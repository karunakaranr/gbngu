import { TestBed, waitForAsync } from '@angular/core/testing';
import { UifwkUifwkprimeNgModule } from './uifwk-uifwkprime-ng.module';

describe('UifwkUifwkprimeNgModule', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [UifwkUifwkprimeNgModule],
    }).compileComponents();
  }));

  // TODO: Add real tests here.
  //
  // NB: This particular test does not do anything useful.
  //     It does NOT check for correct instantiation of the module.
  it('should have a module definition', () => {
    expect(UifwkUifwkprimeNgModule).toBeDefined();
  });
});
