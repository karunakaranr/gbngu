import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { GbComboBox } from './gb-combobox.actions';

export class ComboBoxStateModel {
    comboboxdata;
}

@State<ComboBoxStateModel>({
    name: 'ComboBox',
    defaults: {
        comboboxdata: null,
    },
})
@Injectable()
export class GbComboBoxState {
  @Selector() static Combobox(state: ComboBoxStateModel) {
    return state.comboboxdata;
  }
  @Action(GbComboBox)
  GbComboBox(context: StateContext<ComboBoxStateModel>, IComboBox: GbComboBox) {
    const state = context.getState();
    context.setState({ ...state, comboboxdata: IComboBox.DComboBox });
  }
}