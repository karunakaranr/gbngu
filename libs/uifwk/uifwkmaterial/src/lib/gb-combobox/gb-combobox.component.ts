import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { GBBaseUIFCComponent } from '@goodbooks/uicore';
import { Select } from '@ngxs/store';
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-gb-combobox',
  templateUrl: './gb-combobox.component.html',
  styleUrls: ['./gb-combobox.component.scss']
})
export class GbComboboxComponent extends GBBaseUIFCComponent implements OnInit {
  @Output() ComboBoxOutPut: EventEmitter<any> = new EventEmitter<string>();
  @Select(LayoutState.FormEdit) edit$: Observable<any>;
  isreadable:boolean = false;
  isFocused: boolean = false;
  KeyPair;
  
  ngOnInit(): void {
    if(this.gbControl.field.Width){
      this.setStyle('--ComboBoxWidth', this.gbControl.field.Width + 'px');
    } else {
      this.gbControl.field.Width = 200;
      this.setStyle('--ComboBoxWidth', '240px');
    }
    this.edit$.subscribe(res => {   
      this.isreadable = res;
    })
  }

  public foundObject() {
    if(this.gbControl.value == undefined || this.gbControl.value === ""){
      return ""
    } else {
      for( let i =0 ; i<this.gbControl.field.PickLists.length;i++){
        if(this.gbControl.value == this.gbControl.field.PickLists[i]['key']){
          this.KeyPair = i
          this.KeyPair = this.gbControl.field.PickLists[this.KeyPair]
          break;
        }
      }
      return this.KeyPair.value
    }
  }

  setStyle(name,value) {
    document.documentElement.style.setProperty(name, value);
  }
  public isPicklistvalue(val) {
    return (val[0].key !== undefined);
  }

  onSelectionChange(event: any) {
    this.ComboBoxOutPut.emit(event.value);
  }
}


// import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
// import { GBBaseUIFCComponent } from '@goodbooks/uicore';
// import { Observable } from 'rxjs';
// import { Select } from '@ngxs/store';
// import { LayoutState } from 'features/layout/store/layout.state';
// @Component({
//   selector: 'app-gb-combobox',
//   templateUrl: './gb-combobox.component.html',
//   styleUrls: ['./gb-combobox.component.scss']
// })
// export class GbComboboxComponent extends GBBaseUIFCComponent implements OnChanges {
//   @Select(LayoutState.Formeditabe) formedit$: Observable<any>;
//   @Select(LayoutState.FormEdit) edit$: Observable<any>;
//   @Input() picklistid;
//   @Input() picklistdata;
//   @Output() childEvent : EventEmitter<any> = new EventEmitter();
//   check = 1
//   isreadable = false;
//   defaultvalue : any ={key:"",value:""};
//   ngOnChanges(changes: SimpleChanges): void {
//     if(changes.picklistid || changes.picklistdata){
//       if(this.picklistid || this.picklistid==0){
//       this.defaultvalue = this.picklistdata.find(c => c.key == this.picklistid )
//     }
//     if(this.picklistid == undefined){
//       this.picklistid = 0
//       this.defaultvalue = this.picklistdata.find(c => c.key == this.picklistid )
//     }
//     }
//     this.edit$.subscribe(res => {
//       this.isreadable = res;
//     })
//   }

//   change(value){
//     this.picklistid = value
//     this.defaultvalue = this.picklistdata.find(c => c.key == this.picklistid )
//     this.childEvent.emit(value);
//   }
// }
