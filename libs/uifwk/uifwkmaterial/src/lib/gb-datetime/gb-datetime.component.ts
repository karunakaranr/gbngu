import { DatePipe } from '@angular/common';
import { Component, Input, OnChanges, OnInit, SimpleChanges, ViewChild, ElementRef, EventEmitter, Output } from '@angular/core';
import { FormControl } from '@angular/forms';

import { GBBaseUIFCComponent } from '@goodbooks/uicore';
import { Select } from '@ngxs/store';
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-gb-datetime',
  templateUrl: './gb-datetime.component.html',
  styleUrls: ['./gb-datetime.component.scss']
})
export class GbDateTimeComponent extends GBBaseUIFCComponent implements OnChanges,OnInit {
  // @ViewChild('timeformat', { static: false }) timeformat: ElementRef;
  @Output() DateFormat: EventEmitter<any> = new EventEmitter<string>();
  @Output() Minute: EventEmitter<any> = new EventEmitter<string>();
  @Output() TimeOutput: EventEmitter<any> = new EventEmitter<string>();
  @Select(LayoutState.FormEdit) edit$: Observable<any>;
  @Input() floatLabel; // 'always' | 'never' | 'auto' = 'always';
  @Input() appearance; // 'legacy' | 'standard' | 'fill' | 'outline' = 'standard';
  @ViewChild('picker') picker: any;
  @Input() time: string| number;
  @Input() PrimaryId : number | string;
  @Input() default: string;
  isFocused: boolean = false;
  public disabled = false;
  public showSpinners = true;
  public showSeconds = false;
  public touchUi = false;
  public enableMeridian = false;
  public minDate: Date;
  public maxDate: Date;
  public stepHour = 1;
  public stepMinute = 1;
  public stepSecond = 1;
  isreadable:boolean = false;
  public dateControl = new FormControl(new Date());

  public options = [
    { value: true, label: 'True' },
    { value: false, label: 'False' }
  ];
  public stepHours = [1, 2, 3, 4, 5];
  public stepMinutes = [1, 5, 10, 15, 20, 25];
  public stepSeconds = [1, 5, 10, 15, 20, 25];

  ngOnInit(): void {
    this.edit$.subscribe(res => {   
      this.isreadable = res;
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.PrimaryId || changes.time){
      if(typeof this.time=='number' && this.time != 0){
        let hrstring : string = Math.floor(this.time / 60) < 10 ? "0" + Math.floor(this.time / 60).toString() : Math.floor(this.time / 60).toString();
        let minutesString: string = (this.time % 60) < 10 ? "0" + (this.time % 60).toString() : (this.time % 60).toString();
        this.value = hrstring+":"+minutesString
      } else if(typeof this.time=='string' && this.time.includes('Date')){
          const currentTime = new Date(JSON.parse(this.time.replace("/Date(", "").replace(")/", ""))+ 43200000)
          let hrstring = currentTime.getHours() < 10 ? `0${currentTime.getHours()}` : `${currentTime.getHours()}` 
          let minutesString = currentTime.getMinutes() < 10 ? `0${currentTime.getMinutes()}` : `${currentTime.getMinutes()}`
          this.value = hrstring+":"+minutesString
      } else if(this.default == 'currenttime'){
        const currentTime = new Date()
        let hrstring = currentTime.getHours() < 10 ? `0${currentTime.getHours()}` : `${currentTime.getHours()}` 
        let minutesString = currentTime.getMinutes() < 10 ? `0${currentTime.getMinutes()}` : `${currentTime.getMinutes()}`
        this.value = hrstring+":"+minutesString
      }
      console.log("Date Value:",this.value);
    }
  }

  public onTimeChange(event):void{
    const inputElement = event.target as HTMLInputElement;
    if(inputElement.value.length == 5){
      const timeString = inputElement.value;
      const [hours, minutes] = timeString.split(':').map(String);
      let hr = hours;
      let min = minutes;
      if(parseInt(hours) > 24){
        hr = "00";
        min = "00"
        const dialogRef = this.dialog.open(DialogBoxComponent, {
          width: '400px',
          data: {
            message: "Please enter Valid Time Format",
            heading: 'Error',
          }
        });
      }
      else if(parseInt(minutes) > 60){
        hr = "00"
        min = "00";
        const dialogRef = this.dialog.open(DialogBoxComponent, {
          width: '400px',
          data: {
            message: "Please enter Valid Time Format",
            heading: 'Error',
          }
        });
      }
      this.value = hr + ':' + min;
      const totalMinutes = parseInt(hr) * 60 + parseInt(min);
      let todaydate = new Date().setHours(0,0,0,0) 
      let todaytime = todaydate + totalMinutes*60000
      const dateFormat = "/Date("+todaytime+")/";
      let minuteemitvaue = { SelectedData : totalMinutes, id : this.gbControl.field.LinkId}
      let dateemitvalue = { SelectedData: dateFormat, id : this.gbControl.field.LinkId}
      this.Minute.emit(minuteemitvaue);
      this.DateFormat.emit(dateemitvalue);
      this.TimeOutput.emit(this.value);
    }
  }
}

