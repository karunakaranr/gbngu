import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges, ElementRef, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { GBBaseUIFCComponent } from '@goodbooks/uicore';
import { Select } from '@ngxs/store';
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
import { IGrid, IPickListDetail, IPicklistEmitValue } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-gb-formgrid',
  templateUrl: './gb-formgrid.component.html',
  styleUrls: ['./gb-formgrid.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class GbFormGridComponent extends GBBaseUIFCComponent implements OnInit, OnChanges {
  // @ViewChild('datepicker', { static: true }) datepicker: ElementRef;
  @Output() GridOutPut: EventEmitter<any> = new EventEmitter<string>();
  @Output() LightBoxOutPut: EventEmitter<any> = new EventEmitter<string>();
  @Output() EditOutPut: EventEmitter<any> = new EventEmitter<string>();
  @Select(LayoutState.FormEdit) edit$: Observable<any>;
  @Select(LayoutState.Resetformvalue) resetform$: Observable<any>;
  @ViewChild('tableRow') tableRows: ElementRef[];
  @Input() ArrayDetail: number | string;
  @Input() ArrayValues;
  isreadable: boolean = false;
  orderForm: FormGroup;
  items: FormArray;
  selectedRowIndex: number = 0;
  AddrowNeeded: boolean = true
  DeleteRowNeeded: boolean = true
  IsEditRequired: boolean = false;
  GridFlag = {}
  GridDecimalValue = {}
  GridMask = {}
  ngOnChanges(changes: SimpleChanges): void {
    if (this.gbControl.field.IsAddrowRequired != undefined) {
      this.AddrowNeeded = false
    }
    if (this.gbControl.field.IsDeleterowRequired != undefined) {
      this.DeleteRowNeeded = false
    }
    if (this.gbControl.field.IsEditRequired != undefined) {
      this.IsEditRequired = true
    }
    if (changes.ArrayDetail) {
      this.selectedRowIndex = 0
      this.orderForm = new FormGroup({
        items: new FormArray([])
      });
      this.items = this.orderForm.get('items') as FormArray
      if (this.gbControl.value != null) {
        for (let i = 0; i < this.gbControl.value.length; i++) {
          this.items.push(this.formBuilder.group(this.gbControl.value[i]));
          const itemFormGroup = this.items.at(i);
          if (this.gbControl.field.SLNOField != undefined) {
            itemFormGroup.get(this.gbControl.field.SLNOField)?.setValue(i + 1);
          }
        }
      }
    }
  }

  ngOnInit(): void {
    this.gbControl.field.Grid?.sort((a, b) => (a['SNo'] < b['SNo'] ? -1 : 1));
    this.edit$.subscribe((res: boolean) => {
      this.isreadable = res;
    })
    this.resetform$.subscribe((res: boolean) => {
      this.selectedRowIndex = 0
      this.orderForm = new FormGroup({
        items: new FormArray([])
      });
      this.items = this.orderForm.get('items') as FormArray;
      if (this.gbControl.value != null) {
        for (let i = 0; i < this.gbControl.value.length; i++) {
          this.items.push(this.formBuilder.group(this.gbControl.value[i]));
          const itemFormGroup = this.items.at(i);
          if (this.gbControl.field.SLNOField != undefined) {
            itemFormGroup.get(this.gbControl.field.SLNOField)?.setValue(i + 1);
          }
        }
      }
    })
  }

  public onArrowDown() {
    const items = this.orderForm.get('items') as FormArray;
    if (this.selectedRowIndex != items.length - 1) {
      this.selectedRowIndex++;
    }
  }

  public onArrowUp() {
    if (this.selectedRowIndex != 0) {
      this.selectedRowIndex--;
    }
  }

  public onRowClick(index: number): void {
    this.selectedRowIndex = index;
    if (this.tableRows && this.tableRows[index]) {
      this.renderer.selectRootElement(this.tableRows[index].nativeElement).focus();
    }
  }

  public PicklistPatchValue(PicklistData: IPicklistEmitValue, index: number, PicklistDetail: IPickListDetail): void {
    let field = PicklistData.id.split(',')
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      const itemFormGroup = items.at(index);
      if (PicklistData.value != undefined) {
        let linkids = PicklistData.id.split(',')
        let linkvalues = PicklistData.value.split(',')
        if (PicklistData.Selected != undefined) {
          for (let i = 0; i < linkids.length; i++) {
            itemFormGroup.get(linkids[i])?.setValue(PicklistData.Selected[linkvalues[i]])
          }
        } else {
          for (let i = 0; i < linkids.length; i++) {
            if (linkids[i].toLowerCase().includes('id')) {
              itemFormGroup.get(linkids[i])?.setValue(-1)
            } else {
              itemFormGroup.get(linkids[i])?.setValue("")
            }
          }
        }
      }
      else if (field.length == 2) {
        itemFormGroup.get(field[0])?.setValue(PicklistData.SelectedData);
        if (PicklistData.Selected != undefined) {
          let names = ""
          if(PicklistDetail.Multiple){
            for (let item of PicklistData.Selected) {
              names = names + item[PicklistDetail.PicklistTitle] + ","
            }
            names = names.slice(0, -1);
          } else {
            names = PicklistData.Selected[PicklistDetail.PicklistTitle]
          }
          itemFormGroup.get(field[1])?.setValue(names);
        } else {
          itemFormGroup.get(field[1])?.setValue("");
        }
      } else {
        itemFormGroup.get(field[0])?.setValue(PicklistData.SelectedData);
      }
    }
    this.value = this.items.value;
    this.GridOutPut.emit(this.value);
  }

  public PicklistLoadCurrentRow(PicklistId: number, index: number, PicklistRetrivalURL: string, PicklistRetrivalIdField: string) {
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      const itemFormGroup = items.at(index);
      this.service.getGridService(PicklistRetrivalURL, PicklistId, PicklistRetrivalIdField).subscribe(res => {
        itemFormGroup.patchValue(res)
        this.value = this.items.value;
        this.GridOutPut.emit(this.value);
      })

    }
  }

  public ComboBoxValue(ComboJSON, FieldName: number): void {
    let KeyPair
    for (let i = 0; i < ComboJSON.length; i++) {
      if (FieldName == ComboJSON[i]['key']) {
        KeyPair = i
        KeyPair = ComboJSON[KeyPair]
        break;
      }
    }
    return KeyPair
  }

  public getZerothIndexValue(index: number): any {
    const items = this.orderForm.get('items') as FormArray;
    if (items.length > 0) {
      const selctedItem = items.at(index);
      return selctedItem.value;
    }
    return null;
  }

  public getTimeValue(index: number, fieldvalue: string): any {
    const items = this.orderForm.get('items') as FormArray;
    if (items.length > 0) {
      const selctedItem = items.at(index)
      return selctedItem.value[fieldvalue];
    }
    return null;
  }

  public editField(index: number, fieldName: string, updatedValue: number, FieldData: IGrid): void {
    // console.log("FieldData:",FieldData)
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      const itemFormGroup = items.at(index);
      itemFormGroup.get(fieldName)?.setValue(updatedValue);
      if (FieldData.DependentChangeFields != undefined) {
        itemFormGroup.get(FieldData.DependentChangeFieldName[updatedValue])?.setValue(FieldData.DependentChangeFieldValue[updatedValue]);
        for (let i = 0; i < FieldData.DependentChangeFields.length; i++) {
          if (FieldData.DependentChangeFields[i] != FieldData.DependentChangeFieldName[updatedValue]) {
            itemFormGroup.get(FieldData.DependentChangeFields[i])?.setValue(FieldData.DependentChangeDefaultValue[i]);
          }
        }
      }
    }
    this.value = this.items.value;
    this.GridOutPut.emit(this.value);
  }

  // clearStartDate() {
  //   this.datepicker.nativeElement.value = null;
  // }

  public onDateChange(index: number, fieldName: string, event: any): void {
    // var datePipe = new DatePipe("en-US");
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      const itemFormGroup = items.at(index);
      itemFormGroup.get(fieldName)?.setValue("/Date(" + ((event).getTime() + 43200000).toString() + ")/");
    }
    this.value = this.items.value;
    this.GridOutPut.emit(this.value);
    // console.log("Date Picker:",this.datepicker)
    // this.datepicker.nativeElement.value = datePipe.transform(event,'dd/MMM/yyyy');
  }

  public getDateValue(DateValue: string) {
    var datePipe = new DatePipe("en-US");
    if (DateValue.includes('Date')) {
      return datePipe.transform(JSON.parse(DateValue.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy')
    } else {
      return ""
    }
  }

  public LightBox(index: number, FieldName: string): void {
    let emitvalue = { index: index, FieldValue: this.getZerothIndexValue(index)[FieldName] }
    this.LightBoxOutPut.emit(emitvalue)
  }

  public async ValueSave(index: number, FieldIndex: number, fieldName: string, event: KeyboardEvent, type: string): Promise<void> {
    const items = this.orderForm.get('items') as FormArray;
    const inputElement = event.target as HTMLInputElement;
    if (index >= 0 && index < items.length && type.toLowerCase() == 'number') {
      const itemFormGroup = items.at(index);
      if (Number.isNaN(parseInt(inputElement.value))) {
        itemFormGroup.get(fieldName)?.setValue("")
      } else {
        if (this.gbControl.field.Grid[FieldIndex].Mask != undefined) {
          if (this.GridFlag["GridFlag" + FieldIndex] == undefined) {
            this.GridMask["GridMask" + FieldIndex] = this.gbControl.field.Grid[FieldIndex].Mask
            this.GridDecimalValue["GridDecimal" + FieldIndex] = this.gbControl.field.Grid[FieldIndex].Mask.split('.')[0].length
            this.GridFlag["GridFlag" + FieldIndex] = false
          }
          this.gbControl.field.Grid[FieldIndex].Mask = ""
          if (inputElement.value[0] == '0' && (inputElement.value[1] && inputElement.value[1] == '0')) {
            await this.delay(100);
            itemFormGroup.get(fieldName)?.setValue(inputElement.value.slice(0, -1))
          }

          if (inputElement.value[0] == '0' && (inputElement.value[1] && inputElement.value[1] != '.')) {
            await this.delay(100);
            itemFormGroup.get(fieldName)?.setValue(inputElement.value.slice(1))
          }

          else if (inputElement.value.split('.').length - 1 > 1) {
            await this.delay(100);
            itemFormGroup.get(fieldName)?.setValue(inputElement.value.slice(0, -1))
          }
          else if (inputElement.value.length > 0 && ((inputElement.value[inputElement.value.length - 1] != '.') && (/[^0-9.]/g).test(inputElement.value[inputElement.value.length - 1]))) {
            itemFormGroup.get(fieldName)?.setValue(inputElement.value.slice(0, -1))
          }
          if (inputElement.value.length > this.GridDecimalValue["GridDecimal" + FieldIndex] && !inputElement.value.includes('.')) {
            this.gbControl.field.Grid[FieldIndex].Mask = this.GridMask["GridMask" + FieldIndex]
            await this.delay(100);
            itemFormGroup.get(fieldName)?.setValue(inputElement.value)
          }
        } else {
          itemFormGroup.get(fieldName)?.setValue(parseFloat(inputElement.value.replace(/[^0-9]/g, '')));
        }
      }
    } else if (index >= 0 && index < items.length && type.toLowerCase() == 'percentage') {
      const itemFormGroup = items.at(index);
      if (Number.isNaN(parseInt(inputElement.value))) {
        itemFormGroup.get(fieldName)?.setValue("")
      } else {
        if (inputElement.value[0] == '0' && (inputElement.value[1] && inputElement.value[1] == '0')) {
          await this.delay(100);
          inputElement.value = inputElement.value.slice(0, -1);
          itemFormGroup.get(fieldName)?.setValue(itemFormGroup.get(fieldName)?.value.slice(0, -1))
        }

        if (inputElement.value[0] == '0' && (inputElement.value[1] && inputElement.value[1] != '.')) {
          await this.delay(100);
          inputElement.value = inputElement.value.slice(1);
          itemFormGroup.get(fieldName)?.setValue(itemFormGroup.get(fieldName)?.value.slice(1))
        }

        if (inputElement.value[0] && parseInt(inputElement.value[0]) > 1) {
          if (inputElement.value[0] == '.') {
            this.gbControl.field.Grid[FieldIndex].Mask = ".00"
          }
          else if (inputElement.value[1] == '.') {
            this.gbControl.field.Grid[FieldIndex].Mask = "0.00"
          } else if (inputElement.value.length > 2) {
            this.gbControl.field.Grid[FieldIndex].Mask = "00.00"
          } else {
            this.gbControl.field.Grid[FieldIndex].Mask = ""
          }
        } else {
          if (inputElement.value[0] == '.') {
            this.gbControl.field.Grid[FieldIndex].Mask = ".00"
          }
          else if (inputElement.value[1] == '.') {
            this.gbControl.field.Grid[FieldIndex].Mask = "0.00"
          }
          else if (inputElement.value[2] == '.') {
            this.gbControl.field.Grid[FieldIndex].Mask = "00.00"
          } else if (inputElement.value.length > 2) {
            if (parseInt(inputElement.value) > 100) {
              this.gbControl.field.Grid[FieldIndex].Mask = "00.00"
            } else if (parseInt(inputElement.value) == 100) {
              this.gbControl.field.Grid[FieldIndex].Mask = "000"
            } else {
              this.gbControl.field.Grid[FieldIndex].Mask = "000.00"
            }
          } else {
            this.gbControl.field.Grid[FieldIndex].Mask = ""
          }
        }
        itemFormGroup.get(fieldName)?.setValue(inputElement.value)
      }
    }
    this.value = this.items.value;
    // console.log("Form Value:",this.value)
    this.GridOutPut.emit(this.value);
  }

  delay(milliseconds: number) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
  }

  private createItem(index: number): FormGroup {
    const formGroupConfig: { [key: string]: any } = {};
    this.gbControl.field.Grid?.forEach((field, fieldIndex) => {
      // console.log(field.value,":",field.DefaultValue)
      if (field.PostData != false) {
        if (field.DependendChangeField != undefined) {
          if (this.ArrayValues[field.DependendChangeField[0]] > this.ArrayValues[field.DependendChangeField[1]]) {
            formGroupConfig[field.value] = 1;
          } else {
            formGroupConfig[field.value] = 0;
          }
        }
        else if (field.Type?.toLowerCase() == 'sno' || field.Type?.toLowerCase() == 'defaultsno') {
          formGroupConfig[field.value] = index + 1;
        } else if (field.Type?.toLowerCase() == 'fieldbasedpicklist' || field.Type?.toLowerCase() == 'fieldbasedcombobox') {
          if (field.DefaultValue == undefined) {
            formGroupConfig[field.value[0]] = '';
          } else {
            formGroupConfig[field.value[0]] = field.DefaultValue;
          }
        } else {
          if (field.DefaultValue == undefined) {
            formGroupConfig[field.value] = '';
          } else if (field.DefaultId == true) {
            formGroupConfig[field.value] = this.ArrayDetail
          } else if (field.DefaultValue == 'today') {
            let todaydate = new Date();
            formGroupConfig[field.value] = "/Date(" + ((todaydate).getTime()).toString() + ")/";
          } else if (typeof field.DefaultValue == 'object') {
            if (this.ArrayValues[field.DefaultValue[0]] - this.ArrayValues[field.DefaultValue[1]] < 0) {
              formGroupConfig[field.value] = (this.ArrayValues[field.DefaultValue[0]] - this.ArrayValues[field.DefaultValue[1]]) * -1;
            } else {
              formGroupConfig[field.value] = this.ArrayValues[field.DefaultValue[0]] - this.ArrayValues[field.DefaultValue[1]];
            }
          } else {
            formGroupConfig[field.value] = field.DefaultValue;
          }
        }
      }
    });
    return this.formBuilder.group(formGroupConfig);
  }

  getRowColor(index: number): string {
    const colors = ['#FFFFFF', '#F9F6FE', '#FFFFFF', '#FDF5FB', '#FFFFFF', '#FFF0F2']; // Define your colors array
    return colors[index % colors.length]; // Get the color based on index modulo colors array length
  }


  public addrow(): void {
    if (this.isreadable) {
      let flag = 1;
      let alertdata: string[] = []
      const items = this.orderForm.get('items') as FormArray;
      const itemFormGroup = items.at(items.length - 1);
      if (itemFormGroup) {
        this.gbControl.field.Grid?.forEach((field, index) => {
          if(typeof field.value == 'object'){
            let a = itemFormGroup.get(field.DependendField)?.value === '' ? Number(field.DependentDefaultValue) -1 :itemFormGroup.get(field.DependendField)?.value
            if(field.Required != false && itemFormGroup.get(field.value[a])?.value === ''){
              console.log(field.value, ":", itemFormGroup.get(field.value)?.value)
              alertdata.push("Please Fill - " + items.length + " - " + field.Label)
              flag = 0;
            }
          }
          else if (itemFormGroup.get(field.value)?.value === '' && field.Required != false) {
            console.log(field.value, ":", itemFormGroup.get(field.value)?.value)
            alertdata.push("Please Fill - " + items.length + " - " + field.Label)
            flag = 0;
          } else if (field.Type?.toLowerCase() == 'mail' && itemFormGroup.get(field.value)?.value != '') {
            let emailPattern = /^[\w-]+(?:\.[\w-]+)*@[a-zA-Z\d-]+\.[a-zA-Z]{2,}$/
            // let emailPattern = /^[\w.-]+@[a-zA-Z\d-]+\.[a-zA-Z]{2,}$/
            if (!(emailPattern.test(itemFormGroup.get(field.value)?.value))) {
              alertdata.push("Please Provide Valid Email - " + items.length + " - " + field.Label)
              flag = 0;
            }
          }
        })
      }
      if (flag) {
        this.items = this.orderForm.get('items') as FormArray;
        this.items.push(this.createItem(this.items.value.length));
        this.selectedRowIndex = this.items.value.length - 1;
        this.value = this.items.value;
        this.GridOutPut.emit(this.value);
      } else {
        const dialogRef = this.dialog.open(DialogBoxComponent, {
          width: '400px',
          data: {
            message: alertdata,
            heading: 'info'
          }
        })
      }
    }
  }

  public insertItem(index: number): void {
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      items.insert(index, this.createItem(index));
      this.selectedRowIndex = index
    }
  }

  private removedata(index: number, SLNoField: string): void {
    const items = this.orderForm.get('items') as FormArray;
    if(this.gbControl.field.DeleteUrl != undefined){
      const items = this.orderForm.get('items') as FormArray;
      const itemFormGroup = items.at(index);
      let url = this.gbControl.field.DeleteUrl
      let DeleteParameterFieldName = this.gbControl.field.DeleteParameterFieldName.split(',')
      let DeleteParameterFieldValue = this.gbControl.field.DeleteParameterFieldValue.split(',')
      if (itemFormGroup) {
        for(let i=0;i<DeleteParameterFieldName.length;i++){
          if(i==0){
            url = url + "?" + DeleteParameterFieldName[i] + "=" +  itemFormGroup.get(DeleteParameterFieldValue[i])?.value
          } else {
            url = url + "&" + DeleteParameterFieldName[i] + "=" +  itemFormGroup.get(DeleteParameterFieldValue[i])?.value
          }
        }
      }
      this.service.DeleteRowData(url).subscribe(res=>{
        
      })
    }
    if (index >= 0 && index < items.length) {
      items.removeAt(index);
      if (SLNoField != undefined) {
        for (let i = index; i < items.length; i++) {
          const itemFormGroup = items.at(i);
          itemFormGroup.get(SLNoField)?.setValue(i + 1);
        }
      }
    }
    this.value = this.items.value;
    this.GridOutPut.emit(this.value);
  }

  public removeItem(index: number, SLNoField: string): void {
    let deletemsg = "Do You want to Delete this data?"
    if(this.gbControl.field.DeleteUrl != undefined){
      deletemsg = "This will remove the details from stored data. Do You want to Delete this data?"
    }
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '600px',
      data: {
        message: deletemsg,
        heading: 'Info',
        button1: 'Yes',
        button2: 'No'
      }
    })
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Yes') {
        this.removedata(index, SLNoField);
      }
    });
  }

  public EditItem(index: number, SLNoField: string): void {
    const items = this.orderForm.get('items') as FormArray;
    const itemFormGroup = items.at(index);
    let emitvalue = {index:index,value:itemFormGroup.value}
    this.EditOutPut.emit(emitvalue)
    // console.log("Edit Record:",itemFormGroup.value)
  }
}