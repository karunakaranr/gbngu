import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GbCheckboxComponent } from './gb-checkbox.component';

describe('GbCheckboxComponent', () => {
  let component: GbCheckboxComponent;
  let fixture: ComponentFixture<GbCheckboxComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GbCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
