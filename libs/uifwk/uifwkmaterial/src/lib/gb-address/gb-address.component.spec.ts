import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GbAddressComponent } from './gb-address.component';

describe('GbAddressComponent', () => {
  let component: GbAddressComponent;
  let fixture: ComponentFixture<GbAddressComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GbAddressComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbAddressComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
