import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { GBBaseUIFCComponent, IPickListValue } from '@goodbooks/uicore';
import { Select } from '@ngxs/store';
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-gb-radiobox',
  templateUrl: './gb-radiobox.component.html',
  styleUrls: ['./gb-radiobox.component.scss']
})
export class GbRadioboxComponent extends GBBaseUIFCComponent implements OnInit {
  @Output() RadioButtonOutPut: EventEmitter<any> = new EventEmitter<string>();
  @Select(LayoutState.FormEdit) edit$: Observable<any>;
  isreadable:boolean = false;
  isFocused: boolean = false;
  KeyPair ;
  ngOnInit(): void {
    this.edit$.subscribe(res => {
      this.isreadable = res;
    })
  }

  public foundObject() {
    if(this.gbControl.value == undefined || this.gbControl.value === ""){
      return ""
    } else {
      for( let i = 0 ; i<this.gbControl.field.PickLists.length;i++){
        if(this.gbControl.value == this.gbControl.field.PickLists[i]['key']){
          this.KeyPair = i
          this.KeyPair = this.gbControl.field.PickLists[this.KeyPair]
          break;
        }
      }
      return this.KeyPair.value
    }
  }

  onKeyDown(event: KeyboardEvent) {
    if (event.key === 'Tab') {
      this.isFocused = false
    }
  }

  onKeyUp(event: KeyboardEvent) {
    if (event.key === 'Tab') {
      this.isFocused = true
    }
  }

  onRadioButtonChange(event: any) {
    this.RadioButtonOutPut.emit(event.value)
  }
}


// import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
// import { LayoutState } from 'features/layout/store/layout.state';
// import { GBBaseUIFCComponent } from '@goodbooks/uicore';
// import { Select } from '@ngxs/store';
// import { Observable } from 'rxjs';

// @Component({
//   selector: 'app-gb-radiobox',
//   templateUrl: './gb-radiobox.component.html',
//   styleUrls: ['./gb-radiobox.component.scss']
// })
// export class GbRadioboxComponent extends GBBaseUIFCComponent implements  OnChanges {
//   @Select(LayoutState.Formeditabe) formedit$: Observable<any>;
//   @Select(LayoutState.FormEdit) edit$: Observable<any>;
//   @Input() checkedid;
//   @Input() picklistdata;
//   @Output() childEvent: EventEmitter<any> = new EventEmitter();
//   defaultvalue: any = {key:"",value:""};
//   isreadable: any;
//   onRadioButtonChange(event) { 
//     this.childEvent.emit(event.value);
//   }
//   ngOnChanges(changes: SimpleChanges): void {
//     if(changes.checkedid || changes.picklistdata){
//       this.defaultvalue = this.picklistdata.find(c => c.key == this.checkedid )
//       if(this.checkedid == undefined){
//         this.checkedid = 0;
//         this.defaultvalue = this.picklistdata.find(c => c.key == this.checkedid );
//       }
//     }
//     this.edit$.subscribe(res => {
//       this.isreadable = res;
//     })
//   }
// }
