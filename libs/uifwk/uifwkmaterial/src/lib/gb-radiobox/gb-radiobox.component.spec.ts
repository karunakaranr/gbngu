import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GbRadioboxComponent } from './gb-radiobox.component';

describe('GbRadioboxComponent', () => {
  let component: GbRadioboxComponent;
  let fixture: ComponentFixture<GbRadioboxComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GbRadioboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbRadioboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
