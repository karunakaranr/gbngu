import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { GbInputComponent } from './gb-input.component';

describe('GbInputComponent', () => {
  let component: GbInputComponent;
  let fixture: ComponentFixture<GbInputComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GbInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GbInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
