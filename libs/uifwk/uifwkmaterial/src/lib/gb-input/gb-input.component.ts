import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';

import { GBBaseUIFCComponent } from '@goodbooks/uicore';
import { Select } from '@ngxs/store';
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-gb-input',
  templateUrl: './gb-input.component.html',
  styleUrls: ['./gb-input.component.scss']
})
export class GbInputComponent extends GBBaseUIFCComponent implements OnInit{
  @Output() InputOutPut: EventEmitter<any> = new EventEmitter<string>();
  @Input() minLen: number;
  @Input() maxLen: number;
  @Input() floatLabel; // 'always' | 'never' | 'auto' = 'always';
  @Input() appearance; // 'legacy' | 'standard' | 'fill' | 'outline' = 'standard';
  @Input() HtmlType = 'text';
  @Input() HtmlPattern = '';
  @Input() Screen : string;
  @Select(LayoutState.FormEdit) edit$: Observable<any>;
  isreadable:boolean = false;
  @ViewChild('input') input: ElementRef;
  isFocused: boolean = false;
  isReadonly: boolean = false;
  flag = true;
  Mask : string;
  DecimalValue: number;
  inputElement;
  ngOnInit(): void {
    this.edit$.subscribe(res => {
      this.isreadable = res;
    })
    if(this.gbControl.field != undefined && this.gbControl.field.ReadOnly != undefined){
      this.isReadonly = this.gbControl.field.ReadOnly
    }
  }

  onKeyDown(e: any): boolean {
    if ([8, 9, 27, 13, 46].indexOf(e.keyCode) !== -1 ||
      // Allow: Ctrl+A
      (e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+C
      (e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+V
      (e.keyCode === 86 && (e.ctrlKey || e.metaKey)) ||
      // Allow: Ctrl+X
      (e.keyCode === 88 && (e.ctrlKey || e.metaKey)) ||
      // Allow: home, end, left, right
      (e.keyCode >= 35 && e.keyCode <= 39)) {
      // let it happen, don't do anything
      return true;
    } else {
      return false
    }
  }

  public async validateInput(event: KeyboardEvent): Promise<void> {
    this.inputElement = event.target as HTMLInputElement;
    if(this.gbControl.field.Type != undefined && this.gbControl.field.Type.toLowerCase() == 'number'){
      if(Number.isNaN(parseInt(this.inputElement.value.replace(/[^0-9]/g, '')))){
        this.value = ""
      } else{
        if(this.gbControl.field.Mask != undefined){
          if(this.flag){
            this.Mask = this.gbControl.field.Mask
            this.DecimalValue = this.gbControl.field.Mask.split('.')[0].length
            this.flag = false
          }
          this.gbControl.field.Mask = ""
          if(this.inputElement.value[0] == '0' && (this.inputElement.value[1] && this.inputElement.value[1] == '0')){
            await this.delay(100);
            this.value = this.inputElement.value.slice(1 ,)
            this.inputElement.value = this.inputElement.value.slice(1 ,)
          }
          this.inputElement.value = this.inputElement.value.replace(/[^0-9.]/g, '')
          await this.delay(100);
          this.value = this.inputElement.value.replace(/[^0-9.]/g, '');
          if(this.inputElement.value.split('.').length - 1 > 1){
            const lastIndex = this.inputElement.value.lastIndexOf(".");
            await this.delay(100);
            this.value = this.inputElement.value.substring(0, lastIndex) + this.inputElement.value.substring(lastIndex + 1);
            this.inputElement.value = this.inputElement.value.substring(0, lastIndex) + this.inputElement.value.substring(lastIndex + 1);
          }
          if(this.inputElement.value.length>this.DecimalValue && !this.inputElement.value.includes('.')){
            this.gbControl.field.Mask = this.Mask
            await this.delay(100);
            this.value = this.inputElement.value
          }
        }else {
          await this.delay(100);
          this.value = parseInt(this.inputElement.value.replace(/[^0-9]/g, ''));
        }
      }
    } else if(this.gbControl.field.Type != undefined && this.gbControl.field.Type.toLowerCase() == 'percentage'){
      this.inputElement.value = this.inputElement.value.replace(/[^0-9.]/g, '');
      if(this.inputElement.value[0] == '0' && (this.inputElement.value[1] && this.inputElement.value[1] == '0')){
        await this.delay(100);
        this.value = this.inputElement.value.slice(1 ,)
        this.inputElement.value = this.inputElement.value.slice(1 ,)
      }

      if(this.inputElement.value[0] == '0' && (this.inputElement.value[1] && this.inputElement.value[1] != '.')){
        await this.delay(100);
        this.value = this.inputElement.value.slice(1 ,)
        this.inputElement.value = this.inputElement.value.slice(1 ,)
      }

      if(this.inputElement.value[0] && parseInt(this.inputElement.value[0]) > 1){
        if(this.inputElement.value[0]=='.'){
          this.gbControl.field.Mask = ".00"
        }
        else if(this.inputElement.value[1]=='.'){
          this.gbControl.field.Mask = "0.00"
        } else if(this.inputElement.value.length > 2){
          this.gbControl.field.Mask = "00.00"
        } else {
          this.gbControl.field.Mask = ""
        }
      } else {
        if(this.inputElement.value[0]=='.'){
          this.gbControl.field.Mask = ".00"
        }
        else if(this.inputElement.value[1]=='.'){
          this.gbControl.field.Mask = "0.00"
        }
        else if(this.inputElement.value[2]=='.'){
          this.gbControl.field.Mask = "00.00"
        } else if(this.inputElement.value.length>2) {
          if(parseInt(this.inputElement.value) > 100){
            this.gbControl.field.Mask = "00.00"
          } else if(parseInt(this.inputElement.value) == 100){
            this.gbControl.field.Mask = "000"
          } else {
            this.gbControl.field.Mask = "000.00"
          } 
        } else {
          this.gbControl.field.Mask = ""
        }
      }
      await this.delay(100);
      this.value = this.inputElement.value
    }else {
      await this.delay(100);
      this.value = this.inputElement.value
    }
    this.InputOutPut.emit(this.value)
  } 

  delay(milliseconds: number) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
  }
}
