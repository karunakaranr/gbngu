import { DatePipe } from '@angular/common';
import { Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';
import { FormGroup } from '@angular/forms';

import { GBBaseUIFCComponent } from '@goodbooks/uicore';
import { Select } from '@ngxs/store';
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-gb-date',
  templateUrl: './gb-date.component.html',
  styleUrls: ['./gb-date.component.scss']
})
export class GbDateComponent extends GBBaseUIFCComponent implements OnInit,OnChanges{
  @Select(LayoutState.FormEdit) edit$: Observable<any>;
  @Select(LayoutState.Resetformvalue) resetform$: Observable<any>;
  @ViewChild('datepicker', { static: true }) datepicker: ElementRef;
  @Input() floatLabel:string; // 'always' | 'never' | 'auto' = 'always';
  @Input() appearance:string; // 'legacy' | 'standard' | 'fill' | 'outline' = 'standard';
  @Input() date:string;
  @Input() default:string = "";
  @Output() DateOutPut: EventEmitter<any> = new EventEmitter<string>();
  @Output() DateValueOutPut: EventEmitter<any> = new EventEmitter<string>();
  isFocused: boolean = false;
  isreadable:boolean = false;
  SelectedDate : any;
  ngOnInit(): void {
    this.edit$.subscribe(res => {
      this.isreadable = res;
    })
    this.resetform$.subscribe((res:boolean)=>{
      var datePipe = new DatePipe("en-US");
      if(this.value != "" && this.value.toLowerCase() != "today"){
        this.SelectedDate = datePipe.transform(JSON.parse(this.value.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy');
        this.datepicker.nativeElement.value = this.SelectedDate;
      } else {
          if(this.default.toLowerCase() == 'today' || this.value.toLowerCase() == "today"){
            let todaydate =new Date()
            var datePipe = new DatePipe("en-US");
            this.datepicker.nativeElement.value = datePipe.transform(todaydate,'dd/MMM/yyyy');
            this.value = "/Date(" + ((todaydate).getTime()).toString() + ")/";
            this.DateOutPut.emit(this.value);
          } else {
            this.datepicker.nativeElement.value = "";
          }
      }
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.date){
      var datePipe = new DatePipe("en-US");
      if(this.value != "" && this.value.toLowerCase() != "today"){
        this.SelectedDate = datePipe.transform(JSON.parse(this.value.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy');
        this.datepicker.nativeElement.value = this.SelectedDate;
      } else {
          if(this.default.toLowerCase() == 'today' || this.value.toLowerCase() == "today"){
            let todaydate =new Date()
            var datePipe = new DatePipe("en-US");
            this.datepicker.nativeElement.value = datePipe.transform(todaydate,'dd/MMM/yyyy');
            this.value = "/Date(" + ((todaydate).getTime()).toString() + ")/";
            this.DateOutPut.emit(this.value);
          } else {
            this.datepicker.nativeElement.value = "";
          }
      }
    }
  }

  clearStartDate() {
    this.datepicker.nativeElement.value = null;
    this.value = ""
    this.DateOutPut.emit("");
    this.DateValueOutPut.emit("")
  }

  onDateChange(event: any) {
    var datePipe = new DatePipe("en-US");
    this.datepicker.nativeElement.value = datePipe.transform(event,'dd/MMM/yyyy');
    this.value = "/Date(" + ((event).getTime() + 43200000).toString() + ")/";
    this.DateOutPut.emit(this.value);
    this.DateValueOutPut.emit(event)
  }
}

