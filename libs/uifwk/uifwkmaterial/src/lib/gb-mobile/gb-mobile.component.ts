import { Component, OnInit } from '@angular/core';

import { GBBaseUIFGComponent } from '@goodbooks/uicore';

@Component({
  selector: 'app-gb-mobile',
  templateUrl: './gb-mobile.component.html',
  styleUrls: ['./gb-mobile.component.scss']
})
export class GbMobileComponent extends GBBaseUIFGComponent implements OnInit {
  private regex: any;
  ngOnInit() {
    this.regex = this.gbFormGroup.controls;
    if (this.regex === '') {
      this.Field.ValidRegEx = '^((\\+91-?)|0)?[0-9]{10}$';
    }
  }
}
