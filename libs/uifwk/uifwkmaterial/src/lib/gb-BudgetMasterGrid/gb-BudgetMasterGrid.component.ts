import { ChangeDetectionStrategy, Component, Input, OnChanges, OnInit, SimpleChanges, ElementRef, ViewChild, EventEmitter, Output } from '@angular/core';
import { FormArray, FormGroup } from '@angular/forms';
import { GBBaseUIFCComponent } from '@goodbooks/uicore';
import { Select } from '@ngxs/store';
import { LayoutState } from 'features/layout/store/layout.state';
import { Observable } from 'rxjs';
import { DialogBoxComponent } from 'features/erpmodules/samplescreens/screens/DialogBox/DialogBox.component';
import { IPickListDetail, IPicklistEmitValue } from 'libs/uicore/src/lib/interfaces/ibasefield';
import { DatePipe } from '@angular/common';
import { CostingCharges } from './../../../../../../apps/Goodbooks/Goodbooks/src/assets/FormJSONS/CostingCharges.json'
import { ChargeCostDetailArray } from './ChargeCostDetailArray.json'
@Component({
  selector: 'app-gb-BudgetMasterGrid',
  templateUrl: './gb-BudgetMasterGrid.component.html',
  styleUrls: ['./gb-BudgetMasterGrid.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class GbBudgetMasterGridComponent extends GBBaseUIFCComponent implements OnInit, OnChanges {
  // @ViewChild('datepicker', { static: true }) datepicker: ElementRef;
  @Output() GridOutPut: EventEmitter<any> = new EventEmitter<string>();
  @Output() LightBoxOutPut: EventEmitter<any> = new EventEmitter<string>();
  @Output() FieldChange: EventEmitter<any> = new EventEmitter<string>();
  @Select(LayoutState.FormEdit) edit$: Observable<any>;
  @Select(LayoutState.Resetformvalue) resetform$: Observable<any>;
  @ViewChild('tableRow') tableRows: ElementRef[];
  @Input() ArrayDetail: number | string;
  @Input() ArrayValues;
  isreadable: boolean = false;
  CostCharge: FormGroup;
  CostChargeitems: FormArray;
  orderForm: FormGroup;
  items: FormArray;
  selectedRowIndex: number = 0;
  AddrowNeeded: boolean = true;
  DeleteRowNeeded: boolean = true;
  CostingChargeJSON = CostingCharges
  CostingChargeWidth: number = 0
  ChargeCostDetailArray = ChargeCostDetailArray
  ChargeCostSLNO = 0
  flag: boolean = true;
  LevelDefault='All';
  LevelArray :string[] = ['All'];
  ngOnChanges(changes: SimpleChanges) {
    if (this.gbControl.field.IsAddrowRequired != undefined) {
      this.AddrowNeeded = false
    }
    if (this.gbControl.field.IsDeleterowRequired != undefined) {
      this.DeleteRowNeeded = false
    }
    if (changes.ArrayDetail) {
      this.selectedRowIndex = 0
      this.orderForm = new FormGroup({
        items: new FormArray([])
      });
      this.items = this.orderForm.get('items') as FormArray;
      if (this.gbControl.value != null) {
        this.patchFormArray(this.gbControl.value);
        this.FormCalculationPatch();
        this.FormSubTotal();
        this.value = this.items.value;
        this.GridOutPut.emit(this.value);
        // for(let i=0; i<this.gbControl.value.length;i++){
        //   console.log("CostingChargesArray Detail:",this.orderForm.get('items'))
        //   this.items.push(this.formBuilder.group(this.gbControl.value[i]));
        //   const itemFormGroup = this.items.at(i);
        //   itemFormGroup.get('COSTINGCHARGESArray')?.setValue(ChargeCostDetailArray);
        //   let COSTINGCHARGESArray = itemFormGroup.get('COSTINGCHARGESArray') as FormArray
        //   // for(let j=0;j<ChargeCostDetailArray.length;j++){
        //   //   console.log("COSTINGCHARGESArray:",itemFormGroup.get('COSTINGCHARGESArray')?.value)
        //   //   COSTINGCHARGESArray.push(this.formBuilder.group(ChargeCostDetailArray[j]))
        //   // }
        //   console.log("COSTINGCHARGESArray:",itemFormGroup.get('COSTINGCHARGESArray'))
        //   if(this.gbControl.field.SLNOField != undefined){
        //     itemFormGroup.get(this.gbControl.field.SLNOField)?.setValue(i+1);
        //   }
        // }
      }
    }
  }
  ngOnInit(): void {
    this.gbControl.field.Grid?.sort((a, b) => (a['SNo'] < b['SNo'] ? -1 : 1));
    this.CostingChargeJSON.sort((a, b) => (a['SNo'] < b['SNo'] ? -1 : 1))
    this.CostingChargeWidth = this.CostingChargeJSON.reduce((accumulator, currentValue) => accumulator + currentValue.width, 0);
    this.edit$.subscribe((res: boolean) => {
      this.isreadable = res;
    })
    this.resetform$.subscribe((res: boolean) => {
      this.selectedRowIndex = 0
      this.orderForm = new FormGroup({
        items: new FormArray([])
      });
      this.items = this.orderForm.get('items') as FormArray;
      if (this.gbControl.value != null) {
        this.patchFormArray(this.gbControl.value);
        this.FormCalculationPatch();
        this.FormSubTotal();
        this.value = this.items.value;
        this.GridOutPut.emit(this.value);
        // for(let i=0; i<this.gbControl.value.length;i++){
        //   console.log("CostingChargesArray Detail:",this.orderForm.get('items')?.value)
        //   this.items.push(this.formBuilder.group(this.gbControl.value[i]));
        //   const itemFormGroup = this.items.at(i);
        //   // itemFormGroup.get('COSTINGCHARGESArray')?.setValue([]);
        //   let COSTINGCHARGESArray = itemFormGroup.get('COSTINGCHARGESArray') as FormArray
        //   for(let j=0;j<ChargeCostDetailArray.length;j++){
        //     COSTINGCHARGESArray.push(this.formBuilder.group(ChargeCostDetailArray[j]))
        //   }
        //   console.log("COSTINGCHARGESArray:",itemFormGroup.get('COSTINGCHARGESArray')?.value)
        //   if(this.gbControl.field.SLNOField != undefined){
        //     itemFormGroup.get(this.gbControl.field.SLNOField)?.setValue(i+1);
        //   }
        // }
      }
    })
  }

  delay(milliseconds: number) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
  }

  public UpdateJ() {
    this.ChargeCostSLNO = this.ChargeCostSLNO + 1;
  }

  public ResetJ() {
    this.ChargeCostSLNO = 0;
  }



  public initItem(itemData: any, index: number): FormGroup {
    let ItemData = JSON.parse(JSON.stringify(itemData))
    // const itemFormGroup = itemsFormArray.at(index);
    //   if(this.gbControl.field.SLNOField != undefined){
    //     itemFormGroup.get(this.gbControl.field.SLNOField)?.setValue(index+1);
    //     console.log(this.gbControl.field.SLNOField,":",itemFormGroup.get(this.gbControl.field.SLNOField)?.value)
    //   }
    ItemData[this.gbControl.field.SLNOField] = index + 1
    ItemData.COSTINGCHARGESArray = this.patchSubItems(itemData, index)
    return this.formBuilder.group(ItemData);
  }

  public patchSubItems(subItems: any[], index): FormArray {
    let subItemsFormArray = this.formBuilder.array([]);
    ChargeCostDetailArray.forEach((subItem, indexj) => {
      let SubItemData = JSON.parse(JSON.stringify(subItem))
      // SubItemData.ChargeDetailCalculatedValue = this.formulaExpression(subItems,ChargeCostDetailArray,indexj)
      subItemsFormArray.push(this.formBuilder.group(SubItemData));
    });
    return subItemsFormArray;
  }

  public patchFormArray(items: any[]): void {
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    items.forEach((item, index) => {
      if(!this.LevelArray.includes(item.Level)){
        this.LevelArray.push(item.Level)
      }
      // const itemFormGroup = itemsFormArray.at(index);
      // if(this.gbControl.field.SLNOField != undefined){
      //   itemFormGroup.get(this.gbControl.field.SLNOField)?.setValue(index+1);
      //   console.log(this.gbControl.field.SLNOField,":",itemFormGroup.get(this.gbControl.field.SLNOField)?.value)
      // }
      itemsFormArray.push(this.initItem(item, index));
    });
  }

  public FormCalculationPatch() {
    this.items = this.orderForm.get('items') as FormArray;
    for (let i = 0; i < this.items.value.length; i++) {
      const itemFormGroup = this.items.at(i) as FormGroup;
      itemFormGroup.get('TotalValue')?.setValue(itemFormGroup.get('PROJECTEDRATE')?.value * itemFormGroup.get('FINALQUANTITY')?.value)
      const subItemsFormArray = itemFormGroup.get('COSTINGCHARGESArray') as FormArray;
      for (let j = 0; j < subItemsFormArray?.value.length; j++) {
        // if(itemFormGroup.get('Level')?.value != 1){
          this.FormulaCalculation(i, j)
        // }
      }
    }
  }

  public FormSubTotal(){
    this.items = this.orderForm.get('items') as FormArray;
    let index = 0;
    let chargecostsum = 0;
    let chargevalsum = 0;    
    let prcscostsum = 0;
    let prcsvalsum = 0;
    let expcostsum = 0;
    let expvalsum = 0;
    let totalcost = 0;
    let totalval = 0;
    let itemlength = this.items.value.length
    for (let i = 0; i < this.items.value.length; i++) {
      const itemFormGroup = this.items.at(i) as FormGroup;
      if(itemFormGroup.get('Level')?.value == 1 ){
        if(i!=0){
          const itemFormGroup1 = this.items.at(index) as FormGroup;
          if(itemFormGroup1.get('ValueType')?.value == 1){
            itemFormGroup1.get('MATERIALCHARGESCOST')?.setValue(chargecostsum.toFixed(2))
            itemFormGroup1.get('MATERIALCHARGESVALUE')?.setValue(chargevalsum.toFixed(2))
            itemFormGroup1.get('MATERIALPROCESSCOST')?.setValue(prcscostsum.toFixed(2))
            itemFormGroup1.get('MATERIALPROCESSVALUE')?.setValue(prcsvalsum.toFixed(2))
            // itemFormGroup1.get('MATERIALEXPENSECOST')?.setValue(expcostsum.toFixed(2))
            // itemFormGroup1.get('MATERIALEXPENSEVALUE')?.setValue(expvalsum.toFixed(2))
            itemFormGroup1.get('TOTALCOST')?.setValue(totalcost.toFixed(2))
            itemFormGroup1.get('TOTALVALUE')?.setValue(totalval.toFixed(2))
          } else {
            // itemFormGroup1.get('MATERIALCHARGESCOST')?.setValue("0.00")
            // itemFormGroup1.get('MATERIALCHARGESVALUE')?.setValue("0.00")
            // itemFormGroup1.get('MATERIALPROCESSCOST')?.setValue("0.00")
            // itemFormGroup1.get('MATERIALPROCESSVALUE')?.setValue("0.00")
            // itemFormGroup1.get('MATERIALEXPENSECOST')?.setValue("0.00")
            // itemFormGroup1.get('MATERIALEXPENSEVALUE')?.setValue("0.00")
            // itemFormGroup1.get('TOTALCOST')?.setValue("0.00")
            // itemFormGroup1.get('TOTALVALUE')?.setValue("0.00")
          }
        }
        index = i
        chargecostsum = 0;
        chargevalsum = 0;    
        prcscostsum = 0;
        prcsvalsum = 0;
        expcostsum = 0;
        expvalsum = 0;
        totalcost = 0;
        totalval = 0;
      } else if(itemlength == i+1) {
        chargecostsum = chargecostsum + parseFloat(itemFormGroup.get('MATERIALCHARGESCOST')?.value)
        chargevalsum = chargevalsum + parseFloat(itemFormGroup.get('MATERIALCHARGESVALUE')?.value)
        prcscostsum = prcscostsum + parseFloat(itemFormGroup.get('MATERIALPROCESSCOST')?.value)
        prcsvalsum = prcsvalsum + parseFloat(itemFormGroup.get('MATERIALPROCESSVALUE')?.value)
        // expcostsum = expcostsum + parseFloat(itemFormGroup.get('MATERIALEXPENSECOST')?.value)
        // expvalsum = expvalsum + parseFloat(itemFormGroup.get('MATERIALEXPENSEVALUE')?.value)
        totalcost = totalcost + parseFloat(itemFormGroup.get('TOTALCOST')?.value)
        totalval = totalval + parseFloat(itemFormGroup.get('TOTALVALUE')?.value)
        const itemFormGroup1 = this.items.at(index) as FormGroup;
        if(itemFormGroup1.get('ValueType')?.value == 1){
          itemFormGroup1.get('MATERIALCHARGESCOST')?.setValue(chargecostsum.toFixed(2))
          itemFormGroup1.get('MATERIALCHARGESVALUE')?.setValue(chargevalsum.toFixed(2))
          itemFormGroup1.get('MATERIALPROCESSCOST')?.setValue(prcscostsum.toFixed(2))
          itemFormGroup1.get('MATERIALPROCESSVALUE')?.setValue(prcsvalsum.toFixed(2))
          // itemFormGroup1.get('MATERIALEXPENSECOST')?.setValue(expcostsum.toFixed(2))
          // itemFormGroup1.get('MATERIALEXPENSEVALUE')?.setValue(expvalsum.toFixed(2))
          itemFormGroup1.get('TOTALCOST')?.setValue(totalcost.toFixed(2))
          itemFormGroup1.get('TOTALVALUE')?.setValue(totalval.toFixed(2))
        } else {
          // itemFormGroup1.get('MATERIALCHARGESCOST')?.setValue("0.00")
          // itemFormGroup1.get('MATERIALCHARGESVALUE')?.setValue("0.00")
          // itemFormGroup1.get('MATERIALPROCESSCOST')?.setValue("0.00")
          // itemFormGroup1.get('MATERIALPROCESSVALUE')?.setValue("0.00")
          // itemFormGroup1.get('MATERIALEXPENSECOST')?.setValue("0.00")
          // itemFormGroup1.get('MATERIALEXPENSEVALUE')?.setValue("0.00")
          // itemFormGroup1.get('TOTALCOST')?.setValue("0.00")
          // itemFormGroup1.get('TOTALVALUE')?.setValue("0.00")
        }
      } else {
        chargecostsum = chargecostsum + parseFloat(itemFormGroup.get('MATERIALCHARGESCOST')?.value)
        chargevalsum = chargevalsum + parseFloat(itemFormGroup.get('MATERIALCHARGESVALUE')?.value)
        prcscostsum = prcscostsum + parseFloat(itemFormGroup.get('MATERIALPROCESSCOST')?.value)
        prcsvalsum = prcsvalsum + parseFloat(itemFormGroup.get('MATERIALPROCESSVALUE')?.value)
        // expcostsum = expcostsum + parseFloat(itemFormGroup.get('MATERIALEXPENSECOST')?.value)
        // expvalsum = expvalsum + parseFloat(itemFormGroup.get('MATERIALEXPENSEVALUE')?.value)
        totalcost = totalcost + parseFloat(itemFormGroup.get('TOTALCOST')?.value)
        totalval = totalval + parseFloat(itemFormGroup.get('TOTALVALUE')?.value)
      }
    }
  }

  LevelFilter(level){
    let items = this.gbControl.value
    this.orderForm = new FormGroup({
      items: new FormArray([])
    });
    this.items = this.orderForm.get('items') as FormArray;
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    items.forEach((item, index) => {
      if(item.Level == level || level == 'All'){
        itemsFormArray.push(this.initItem(item, index));
      }
    });
    this.FormCalculationPatch();
    this.FormSubTotal();
    this.value = this.items.value;
    this.GridOutPut.emit(this.value);
  }

  public FormCalclationSum(itemIndex: number) {
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    if (itemIndex >= 0 && itemIndex < itemsFormArray.length) {
      const itemFormGroup = itemsFormArray.at(itemIndex) as FormGroup;
      const subItemsFormArray = itemFormGroup.get('COSTINGCHARGESArray') as FormArray;
      let TotalCharge = 0;
      for (let j = 0; j < subItemsFormArray?.value.length; j++) {
        const subItemFormGroup = subItemsFormArray.at(j) as FormGroup;
        let Operation = subItemFormGroup.get('ChargeDetailOperationType')?.value == 1 ? -1 : 1
        TotalCharge = TotalCharge + parseFloat(subItemFormGroup.get('ChargeDetailCalculatedValue')?.value) * Operation
      }
      
      itemFormGroup.get('MATERIALPROCESSCOST')?.setValue(itemFormGroup.get('MATERIALPROCESSCOST')?.value)
      itemFormGroup.get('MATERIALPROCESSVALUE')?.setValue(itemFormGroup.get('MATERIALPROCESSCOST')?.value * itemFormGroup.get('FINALQUANTITY')?.value)
      // itemFormGroup.get('MATERIALEXPENSECOST')?.setValue((itemFormGroup.get('MATERIALEXPENSEVALUE')?.value / itemFormGroup.get('FINALQUANTITY')?.value).toFixed(2))
      // itemFormGroup.get('MATERIALEXPENSEVALUE')?.setValue(itemFormGroup.get('MATERIALEXPENSEVALUE')?.value)
      itemFormGroup.get('MATERIALCHARGESCOST')?.setValue((TotalCharge).toFixed(2))
      itemFormGroup.get('MATERIALCHARGESVALUE')?.setValue(TotalCharge * itemFormGroup.get('FINALQUANTITY')?.value.toFixed(2))
      let TotalValue = parseFloat(itemFormGroup.get('MATERIALCHARGESVALUE')?.value) + parseFloat(itemFormGroup.get('MATERIALPROCESSVALUE')?.value) + parseFloat(itemFormGroup.get('MATERIALEXPENSEVALUE')?.value) + parseFloat(itemFormGroup.get('TotalValue')?.value)
      itemFormGroup.get('TOTALCOST')?.setValue((TotalValue / itemFormGroup.get('FINALQUANTITY')?.value).toFixed(2))
      itemFormGroup.get('TOTALVALUE')?.setValue((TotalValue).toFixed(2))
    }
  }

  public FormArrayPatch(itemIndex: number, subItemIndex: number, fieldName: string, event: KeyboardEvent) {
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    if (itemIndex >= 0 && itemIndex < itemsFormArray.length) {
      const itemFormGroup = itemsFormArray.at(itemIndex) as FormGroup;
      const subItemsFormArray = itemFormGroup.get('COSTINGCHARGESArray') as FormArray;
      const subItemFormGroup = subItemsFormArray.at(subItemIndex) as FormGroup;
      const inputElement = event.target as HTMLInputElement;
      if (Number.isNaN(parseInt(inputElement.value.replace(/[^0-9]/g, '')))) {
        subItemFormGroup.get(fieldName)?.setValue("")
      } else {
        subItemFormGroup.get(fieldName)?.setValue(parseInt(inputElement.value.replace(/[^0-9]/g, '')));
      }
    }
    this.FormulaCalculation(itemIndex, subItemIndex)
    this.FormSubTotal();
    this.value = this.items.value;
    this.GridOutPut.emit(this.value);
  }

  public FormulaCalculation(itemIndex: number, subItemIndex: number): void {
    const itemsFormArray = this.orderForm.get('items') as FormArray;
    const itemFormGroup = itemsFormArray.at(itemIndex) as FormGroup;
    const subItemsFormArray = itemFormGroup.get('COSTINGCHARGESArray') as FormArray;
    const subItemFormGroup = subItemsFormArray.at(subItemIndex) as FormGroup;
    subItemFormGroup.patchValue({ ChargeDetailCalculatedValue: this.formulaExpression(itemFormGroup.value, subItemsFormArray.value, subItemIndex).toFixed(2) });
    this.FormCalclationSum(itemIndex);
  }

  public formulaExpression(SubItem: any, griddata: any, arrayIndex: number): number {
    let result = 0
    let Operation = griddata[arrayIndex].ChargeDetailOperationType == 1 ? -1 : 1
    let FormulaExpression = griddata[arrayIndex].FormulaExpression;
    let clearExpression = FormulaExpression.replace(/\{\{|\}\}/g, '');
    let round = false
    if(clearExpression.includes('Round')){
      round = true
    }
    let evaluatedExpression = clearExpression
      .replace(/SLBASIC_CalcFC/g, SubItem.PROJECTEDRATE)
      .replace(/OverHeads_Input/g, griddata[0].ChargeDetailDefaultInputValue)
      .replace(/Margin_Input/g, griddata[1].ChargeDetailDefaultInputValue)
      .replace(/Freight_Input/g, griddata[2].ChargeDetailDefaultInputValue)
      .replace(/Packing&Forwarding_Input/g, griddata[3].ChargeDetailDefaultInputValue)
      .replace(/ExpenseOnSite_Input/g, griddata[4].ChargeDetailDefaultInputValue)
      .replace(/Miscellanous_Input/g, griddata[5].ChargeDetailDefaultInputValue)
      .replace(/Discount_Input/g, griddata[6].ChargeDetailDefaultInputValue)
      .replace(/Round/g, '');
    result = eval(evaluatedExpression);
    if(round){
      result = this.Round(result)
    }
    return Math.floor(result * 100) / 100;
  }

  public Round(num){
    return Math.round(num);
  }

  public onArrowDown() {
    const items = this.orderForm.get('items') as FormArray;
    if (this.selectedRowIndex != items.length - 1) {
      this.selectedRowIndex++;
    }
  }

  public onArrowUp() {
    if (this.selectedRowIndex != 0) {
      this.selectedRowIndex--;
    }
  }

  public onRowClick(index: number): void {
    this.selectedRowIndex = index;
    if (this.tableRows && this.tableRows[index]) {
      this.renderer.selectRootElement(this.tableRows[index].nativeElement).focus();
    }
  }

  public ToggleExpansion(index: number): void {
    this.gbControl.field.Grid[index].Expansion = !this.gbControl.field.Grid[index].Expansion
    // this.gbControl.field.Grid[index-1].Expansion = !this.gbControl.field.Grid[index-1].Expansion
    // let emitvalue = { SelectedData: this.gbControl.field.Grid[index].Expansion, id: FieldName }
    // this.FieldChange.emit(emitvalue)
  }

  public PicklistPatchValue(PicklistData: IPicklistEmitValue, index: number, PicklistDetail: IPickListDetail): void {
    let field = PicklistData.id.split(',')
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      const itemFormGroup = items.at(index);
      if (PicklistData.value != undefined) {
        let linkids = PicklistData.id.split(',')
        let linkvalues = PicklistData.value.split(',')
        for (let i = 0; i < linkids.length; i++) {
          itemFormGroup.get(linkids[i])?.setValue(PicklistData.Selected[linkvalues[i]])
        }
      }
      else if (field.length == 2) {
        itemFormGroup.get(field[0])?.setValue(PicklistData.SelectedData);
        itemFormGroup.get(field[1])?.setValue(PicklistData.Selected[PicklistDetail.PicklistTitle]);
      } else {
        itemFormGroup.get(field[0])?.setValue(PicklistData.SelectedData);
      }
    }
    this.value = this.items.value;
    this.GridOutPut.emit(this.value);
    this.GridOutPut.emit(this.value);
  }

  public PicklistLoadCurrentRow(PicklistId: number, index: number, PicklistRetrivalURL: string, PicklistRetrivalIdField: string) {
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      const itemFormGroup = items.at(index);
      this.service.getGridService(PicklistRetrivalURL, PicklistId, PicklistRetrivalIdField).subscribe(res => {
        itemFormGroup.patchValue(res)
        this.value = this.items.value;
        this.GridOutPut.emit(this.value);
      })

    }
  }

  public ComboBoxValue(ComboJSON, FieldName: number): void {
    let KeyPair
    for (let i = 0; i < ComboJSON.length; i++) {
      if (FieldName == ComboJSON[i]['key']) {
        KeyPair = i
        KeyPair = ComboJSON[KeyPair]
        break;
      }
    }
    return KeyPair
  }

  public getZerothIndexValue(index: number): any {
    const items = this.orderForm.get('items') as FormArray;
    if (items.length > 0) {
      const selctedItem = items.at(index);
      return selctedItem.value;
    }
    return null;
  }

  public getTimeValue(index: number, fieldvalue: string): any {
    const items = this.orderForm.get('items') as FormArray;
    if (items.length > 0) {
      const selctedItem = items.at(index)
      return selctedItem.value[fieldvalue];
    }
    return null;
  }

  public editField(index: number, fieldName: string, updatedValue: number): void {
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      const itemFormGroup = items.at(index);
      itemFormGroup.get(fieldName)?.setValue(updatedValue);
    }
    this.value = this.items.value;
    if (fieldName == 'ValueType') {
      this.FormSubTotal();
    }
    this.GridOutPut.emit(this.value);
  }

  // clearStartDate() {
  //   this.datepicker.nativeElement.value = null;
  // }

  public onDateChange(index: number, fieldName: string, event: any): void {
    // var datePipe = new DatePipe("en-US");
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      const itemFormGroup = items.at(index);
      itemFormGroup.get(fieldName)?.setValue("/Date(" + ((event).getTime() + 43200000).toString() + ")/");
    }
    this.value = this.items.value;
    this.GridOutPut.emit(this.value);
    // console.log("Date Picker:",this.datepicker)
    // this.datepicker.nativeElement.value = datePipe.transform(event,'dd/MMM/yyyy');
  }

  public getDateValue(DateValue: string) {
    var datePipe = new DatePipe("en-US");
    if (DateValue.includes('Date')) {
      return datePipe.transform(JSON.parse(DateValue.replace("/Date(", "").replace(")/", "")), 'dd/MMM/yyyy')
    } else {
      return ""
    }
  }

  public LightBox(index: number, FieldName: string): void {
    let emitvalue = { index: index, FieldValue: this.getZerothIndexValue(index)[FieldName] }
    this.LightBoxOutPut.emit(emitvalue)
  }

  public ValueSave(index: number, fieldName: string, event: KeyboardEvent, type: string): void {
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length && type.toLowerCase() == 'number') {
      const itemFormGroup = items.at(index);
      const inputElement = event.target as HTMLInputElement;
      if (Number.isNaN(parseInt(inputElement.value.replace(/[^0-9]/g, '')))) {
        itemFormGroup.get(fieldName)?.setValue("")
      } else {
        itemFormGroup.get(fieldName)?.setValue(parseInt(inputElement.value.replace(/[^0-9]/g, '')));
      }
    }
    this.value = this.items.value;
    this.GridOutPut.emit(this.value);
    if (fieldName == 'RATEFC' || fieldName == 'FINALQUANTITY' || fieldName == 'PROJECTEDRATE') {
      this.FormCalculationPatch();
    }
  }

  private createItem(index: number): FormGroup {
    const formGroupConfig: { [key: string]: any } = {};
    this.gbControl.field.Grid?.forEach((field, fieldIndex) => {
      if (field.PostData != false) {
        if (field.DependendChangeField != undefined) {
          if (this.ArrayValues[field.DependendChangeField[0]] > this.ArrayValues[field.DependendChangeField[1]]) {
            formGroupConfig[field.value] = 1;
          } else {
            formGroupConfig[field.value] = 0;
          }
        }
        else if (field.Type?.toLowerCase() == 'sno' || field.Type?.toLowerCase() == 'defaultsno') {
          formGroupConfig[field.value] = index + 1;
        } else if (field.Type?.toLowerCase() == 'fieldbasedpicklist' || field.Type?.toLowerCase() == 'fieldbasedcombobox') {
          if (field.DefaultValue == undefined) {
            formGroupConfig[field.value[0]] = '';
          } else {
            formGroupConfig[field.value[0]] = field.DefaultValue;
          }
        } else if (field.Type?.toLowerCase() == 'array') {
          formGroupConfig[field.value[0]] = this.formBuilder.array([]);
        } else {
          if (field.DefaultValue == undefined) {
            formGroupConfig[field.value] = '';
          } else if (field.DefaultId == true) {
            formGroupConfig[field.value] = this.ArrayDetail
          } else if (field.DefaultValue == 'today') {
            let todaydate = new Date();
            formGroupConfig[field.value] = "/Date(" + ((todaydate).getTime()).toString() + ")/";
          } else if (typeof field.DefaultValue == 'object') {
            if (this.ArrayValues[field.DefaultValue[0]] - this.ArrayValues[field.DefaultValue[1]] < 0) {
              formGroupConfig[field.value] = (this.ArrayValues[field.DefaultValue[0]] - this.ArrayValues[field.DefaultValue[1]]) * -1;
            } else {
              formGroupConfig[field.value] = this.ArrayValues[field.DefaultValue[0]] - this.ArrayValues[field.DefaultValue[1]];
            }
          } else {
            formGroupConfig[field.value] = field.DefaultValue;
          }
        }
      }
    });
    return this.formBuilder.group(formGroupConfig);
  }

  getRowColor(index: number): string {
    const colors = ['#FFFFFF', '#F9F6FE', '#FFFFFF', '#FDF5FB', '#FFFFFF', '#FFF0F2']; // Define your colors array
    return colors[index % colors.length]; // Get the color based on index modulo colors array length
  }


  public addrow(): void {
    if (this.isreadable) {
      let flag = 1;
      let alertdata: string[] = []
      const items = this.orderForm.get('items') as FormArray;
      const itemFormGroup = items.at(items.length - 1);
      if (itemFormGroup) {
        this.gbControl.field.Grid?.forEach((field, index) => {
          if (itemFormGroup.get(field.value)?.value === '' && field.Required != false) {
            alertdata.push("Please Fill " + " - " + items.length + " - " + field.Label)
            flag = 0;
          }
        })
      }
      if (flag) {
        this.items = this.orderForm.get('items') as FormArray;
        this.items.push(this.createItem(this.items.value.length));
        this.selectedRowIndex = this.items.value.length - 1;
        this.value = this.items.value;
        this.GridOutPut.emit(this.value);
      } else {
        const dialogRef = this.dialog.open(DialogBoxComponent, {
          width: '400px',
          data: {
            message: alertdata,
            heading: 'Error'
          }
        })
      }
    }
  }

  public insertItem(index: number): void {
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      items.insert(index, this.createItem(index));
      this.selectedRowIndex = index
    }
  }

  private removedata(index: number): void {
    const items = this.orderForm.get('items') as FormArray;
    if (index >= 0 && index < items.length) {
      items.removeAt(index);
    }
    this.value = this.items.value;
    this.GridOutPut.emit(this.value);
  }

  public removeItem(index: number): void {
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '400px',
      data: {
        message: "Do You want to Delete this data?",
        heading: 'Info',
        button1: 'Yes',
        button2: 'No'
      }
    })
    dialogRef.afterClosed().subscribe(result => {
      if (result == 'Yes') {
        this.removedata(index);
      }
    });
  }
}