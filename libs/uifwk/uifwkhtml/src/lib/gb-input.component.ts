import { Component, OnInit } from '@angular/core';
import { GBBaseUIFCComponent } from '@goodbooks/uicore';



@Component({
  selector: 'app-gb-input',
  template: '{{Field?.Label}}: <input [formControl]="gbControl"><br/>'
})
export class GbInputComponent extends GBBaseUIFCComponent {
}
