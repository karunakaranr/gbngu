import { Component, OnInit } from '@angular/core';
import { GBBaseUIFCComponent } from '@goodbooks/uicore';



@Component({
  selector: 'app-gb-phone',
  template: '{{Field?.Label}}: <input [formControl]="gbControl" type="number"><br/>'
})
export class GbPhoneComponent extends GBBaseUIFCComponent {
}
