import { TestBed, waitForAsync } from '@angular/core/testing';
import { UifwkUifwkhtmlModule } from './uifwk-uifwkhtml.module';

describe('UifwkUifwkhtmlModule', () => {
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      imports: [UifwkUifwkhtmlModule],
    }).compileComponents();
  }));

  // TODO: Add real tests here.
  //
  // NB: This particular test does not do anything useful.
  //     It does NOT check for correct instantiation of the module.
  it('should have a module definition', () => {
    expect(UifwkUifwkhtmlModule).toBeDefined();
  });
});
