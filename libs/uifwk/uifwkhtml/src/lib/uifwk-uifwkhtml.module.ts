import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { GbAddressComponent } from './gb-address.component';
import { GbInputComponent } from './gb-input.component';

@NgModule({
  declarations: [
    GbAddressComponent,
    GbInputComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports: [
    GbAddressComponent,
    GbInputComponent
  ]
})
export class UifwkUifwkhtmlModule {}
