import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Injector} from '@angular/core';
import { setLibInjector } from './lib-injector';
export { AbstractDS, GBBaseService, GBBaseDataService, GBBaseDataServiceWN,GBBaseListDataService } from './services/gbbasedata.service';
export { GBBaseDBService, GBBaseDBDataService } from './services/gbbasedbdata.service';
export { ISectionCriteriaList, ISelectListCriteria } from './interfaces/ISectionCriteriaList';

export { GBDataStateAction, GBDataStateActionAbstract, GBDataStateActionFactory, GBDataStateActionFactoryWN } from './store/gbdata.actions';
export { GBDataState, GBDataStateModel, GBDataStateModelAbstract } from './store/gbdata.state';

@NgModule({
  imports: [CommonModule],
})
export class GbdataModule {

  constructor(injector: Injector) {
    setLibInjector(injector);
  }
  
  public static forRoot(environment: any): ModuleWithProviders<GbdataModule> {
    return {
      ngModule: GbdataModule,
      providers: [
        {
          provide: 'env', // you can also use InjectionToken
          useValue: environment,
        },
      ],
    };
  }
  
}
