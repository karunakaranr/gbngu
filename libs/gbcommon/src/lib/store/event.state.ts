import { State, Action, StateContext, Selector } from '@ngxs/store';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { ChangeDetectorRef, Inject, Injectable } from '@angular/core';
import {
  Notify
} from './event.actions';

interface NotifyModel {
  Message: string;
  Data?: unknown;
}

interface EventModel {
  Channel: string;
  EventData?: unknown;
}

interface EventStateModel {
  Notify: NotifyModel;
  Event: EventModel;
}

@State<EventStateModel>({
  name: 'Event',
  defaults: {
    Notify: {
      Message: '',
    },
    Event: {
      Channel: '',
    },
  },
})
@Injectable()
export class EventState {
  @Selector() static NotifyMessage(state: EventStateModel): string {
    return state.Notify.Message;
  }
  @Selector() static Notify(state: EventStateModel): NotifyModel {
    return state.Notify;
  }
  @Selector() static Event(state: EventStateModel): EventModel {
    return state.Event;
  }
  static EventChannel(channel: string) {

  }

  @Action(Notify)
  Notify(context: StateContext<EventStateModel>, newMsg: Notify): void {
    const state = context.getState();
    context.setState({ ...state, Notify: {
      Message: newMsg.msg,
      Data: newMsg.data
    } });
  }

}
