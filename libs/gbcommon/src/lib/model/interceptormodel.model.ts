export class ResponseModel {
  status: null | string = null;
  statusText: null | string = null;
  body: null | APIResponse | ProxyResponse = null;
}

export class ProxyResponse {
  contents: null | APIResponse = null;
  status: null | StatusCode = null;
}

export class APIResponse {
  Body: null | string = null;
  Current: null | string = null;
  ETag: null | string = null;
  ErrorBody: null | string = null;
  ErrorInnerException: null | string = null;
  First: null | string = null;
  From: null | string = null;
  Id: null | string = null;
  Last: null | string = null;
  LastLoginUsedTime: null | string = null;
  Next: null | string = null;
  Previous: null | string = null;
  ReplyTo: null | string = null;
  ResponseObject: null | string = null;
  Status: null | string = null;
  To: null | string = null;
  Total: null | string = null;
  ValidToTime: null | string = null;
  ValidityOfSession: null | string = null;
}

export interface StatusCode {
  http_code: number;
}
