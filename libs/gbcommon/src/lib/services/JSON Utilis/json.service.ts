import { Injectable } from '@angular/core';
declare var require: any;
const globalpako = require('pako');

@Injectable({
  providedIn: 'root'
})
export class JsonService {

  constructor() { }

  public decodejson(res) {
    const getresponse = JSON.stringify(res);
    const responseparsed = JSON.parse(getresponse);
    const bodyresponse = responseparsed.Body;
    const decodedData = atob(bodyresponse);
    const Responsebodydecoded = JSON.parse(globalpako.inflate(decodedData, { to: 'string' }));
    const con = Responsebodydecoded.Body;
    let converter = Responsebodydecoded;
    if (responseparsed.Status === 200 || responseparsed.Status === 404) {
      converter = JSON.parse(con);
    }
    return converter;
  }

  public DecodeBody(res) {
    const getresponse = JSON.stringify(res);
    const responseparsed = JSON.parse(getresponse);
    const bodyresponse = responseparsed.Body;
    const decodedData = atob(bodyresponse);
    const Responsebodydecoded = JSON.parse(globalpako.inflate(decodedData, { to: 'string' }));
    const con = Responsebodydecoded.Body;
    return con;
  }
}
