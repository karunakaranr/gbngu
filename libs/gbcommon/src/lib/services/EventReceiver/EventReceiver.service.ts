import { Injectable } from '@angular/core';
import { Select, Store } from '@ngxs/store';
import { modulestate } from 'libs/gbui/src/lib/store/modulelist.state';
import { ButtonVisibility, OpenMenuBar, Path , Title } from 'features/layout/store/layout.actions';
import { Reportid } from 'features/commonreport/datastore/commonreport.action';
import { ActivatedRoute, NavigationExtras, Router } from '@angular/router';
const reportjson = require("./Report.json")
const formjson = require("./Form.json")
@Injectable({
    providedIn: 'root'
  })
export class EventReceiver {
    @Select(modulestate.getmoduleid) selectedId: any;
    constructor(private store: Store, private route: ActivatedRoute, private router: Router) { }
    public DrillDownFunction(DrillDownType : string, EventData : any){
        if(DrillDownType == 'None'){
            console.log("Drill Down Type:",DrillDownType)
            console.log("Event Data:", EventData)
        }
        else if(DrillDownType == 'Form'){
            console.log("Drill Down Type:",DrillDownType)
            let FormData=formjson;
            console.log("Data:", FormData)
            let path = FormData.Path
            let view = FormData.View
            this.router.navigate([path]);
            this.store.dispatch(new Title(view))
            this.store.dispatch(new ButtonVisibility("Forms"))
            console.log("Event Data:", EventData)
        }
        else if(DrillDownType == 'Report'){
            console.log("Drill Down Type:",DrillDownType)
            let reportid=reportjson
            console.log("Report ID:", reportid);
            this.store.dispatch(new ButtonVisibility("Reports"))
            let id = reportid.Id;
            let name = reportid.View;
            this.store.dispatch(new Title(name))
            const queryParams: any = {};
            queryParams.Reportid = JSON.stringify(id);
            const navigationExtras: NavigationExtras = {
            queryParams
            };
            this.router.navigate([`commonreport/${id}`]);
            this.store.dispatch(new OpenMenuBar)
            this.store.dispatch(new Reportid(id))
            console.log("Event Data:", EventData)
        }
        else if(DrillDownType == 'LightBox'){
            console.log("Drill Down Type:",DrillDownType)
            console.log("Event Data:", EventData)
        }
        else{
            console.log("Drill Down Type:",DrillDownType)
        }
    }
}