import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { Injector, ModuleWithProviders, NgModule } from '@angular/core';
import { interceptorProviders } from './services/HTTPInterceptor/Interceptorproviders';
import { AgeFromDatePipe } from './age-from-date.pipe';
import { setLibInjector } from './lib-injector';

import { ToastrModule } from 'ngx-toastr';
import { NgxSpinnerModule } from "ngx-spinner";
import { NgxsModule } from '@ngxs/store';
import { EventState } from './store/event.state';

export { GBHttpService } from './services/HTTPService/GBHttp.service';
export { GbToasterService } from './services/toaster/gbtoaster.service';
export { EventState } from './store/event.state';
export { Notify } from './store/event.actions';
export { LibInjector } from './lib-injector';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    NgxSpinnerModule,
    ToastrModule.forRoot(),
    NgxsModule.forFeature([EventState]),
  ],
  declarations: [
    AgeFromDatePipe
  ],
  exports: [
    AgeFromDatePipe
  ],
  providers: [ interceptorProviders],
})
export class GbcommonModule {

  constructor(injector: Injector) {
    setLibInjector(injector);
  }

  public static forRoot(environment: any): ModuleWithProviders<GbcommonModule> {
    return {
      ngModule: GbcommonModule,
      providers: [
        {
          provide: 'env', // you can also use InjectionToken
          useValue: environment,
        },
      ],
    };
  }
}
