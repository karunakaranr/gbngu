import { Component, OnInit , Input} from '@angular/core';
import { SidebarService } from './../../sidebar/sidebar.service';
import { MenuOption } from './../../../store/modulelist.action';
import { Store } from '@ngxs/store';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  constructor(public sidenav:SidebarService , public store: Store) {
   }
  ngOnInit(): void {
}

clickMenu() { 
  this.store.dispatch(new MenuOption(this.sidenav.toggle));
}
}
