import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'goodbooks-classic',
  templateUrl: './classic.component.html',
  styleUrls: ['./classic.component.scss']
})
export class ClassicComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
