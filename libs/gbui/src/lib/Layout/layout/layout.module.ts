import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule} from '@angular/material/icon';
import { MatListModule} from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule} from '@angular/material/sidenav';
import { MatToolbarModule} from '@angular/material/toolbar';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatDividerModule} from '@angular/material/divider';
import {MatSelectModule} from '@angular/material/select';
import { RouterModule } from '@angular/router';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsModule } from '@ngxs/store';
import { NgxsResetPluginModule } from 'ngxs-reset-plugin';
import {modulestate} from './../../store/modulelist.state'


import { FooterComponent } from './../footer/footer.component';
import { HeaderComponent } from './../Classic/header/header.component';
import { FormHeaderComponent } from './../FormHeader/formheader.component';
import { InfoBarComponent } from './../info-bar/info-bar.component';
import { MainBarComponent } from './../Modern/main-bar/main-bar.component';
import { MenuBarComponent } from './../Modern/menu-bar/menu-bar.component';
import { ScreenComponent } from './../screen/screen.component';
import { SidebarComponent } from './../sidebar/sidebar.component';
import { TitleBarComponent } from './../Modern/title-bar/title-bar.component';
import { ToolBarComponent } from './../Modern/tool-bar/tool-bar.component';
import { LayoutComponent } from './layout.component';
import { NotificationComponent } from './../Classic/header/notification/notification.component';
import { settingmenuComponent } from './../Classic/header/settingmenu/settingmenu.component';
// import {MenudetailsModule} from './../../components/menu/menudetails/menudetails.module'
import { GBsearchComponent } from './../Classic/header/gbsearch/gbsearch.component';
import {usersettingComponent} from './../usersetting/usersetting.component';
@NgModule({
  declarations: [
    MainBarComponent,
    MenuBarComponent,
    TitleBarComponent,
    ToolBarComponent,
    InfoBarComponent,
    LayoutComponent,
    ScreenComponent,
    FooterComponent,
    FormHeaderComponent,
    HeaderComponent,
    SidebarComponent,
    NotificationComponent,
    settingmenuComponent,
    GBsearchComponent,
    usersettingComponent
  ],
  imports: [CommonModule,
    RouterModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatMenuModule,
    MatListModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDividerModule,
    // MenudetailsModule,
    MatDividerModule,
    NgxsModule.forFeature([modulestate]),
   // NgxsReduxDevtoolsPluginModule.forRoot(),
   // NgxsResetPluginModule.forRoot()
  ],
  exports: [
    MainBarComponent,
    MenuBarComponent,
    TitleBarComponent,
    ToolBarComponent,
    InfoBarComponent,
    LayoutComponent,
    FooterComponent,
    FormHeaderComponent,
    HeaderComponent,
    SidebarComponent,
    settingmenuComponent,
    GBsearchComponent,
    usersettingComponent
  ],
})
export class LayoutModule {}
