/*import { Component, OnInit } from '@angular/core';

//import { Loginservice } from './../Services/Login.service';
//import { ILoginDetail } from './../Model/LoginDetail';
import { Loginservice } from '../../../../Goodbooks/Goodbooks/src/app/sharedscreens/Shared/services/Login/Login.service';
import { ILoginDetail } from '../../../../Goodbooks/Goodbooks/src/app/sharedscreens/Shared/models/Login/LoginDetail';


@Component({
  selector: 'gbmw-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  title = 'test';
  LoginTitle ='Login title'
  Forget="forget password"

  data = []
  public logindata: ILoginDetail
  private password: string;
  constructor(private loginservice: Loginservice) {
    this.logindata = {

      Server: 'gb4sc',
      UserName: 'ADMIN',
      Password: 'admin'
    }
  }



  login() {
    alert("logindata")
    //this.logindata = {"Server":"Unisoftgb4","UserName":"E368","Password":"ssss"};
    console.log(this.logindata)
    //alert("logindata" + JSON.stringify(this.logindata))
    this.loginservice.LoginService(this.logindata);
  }
}*/


import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GBBasePageComponentNG, GBBasePageComponent, GBDataPageServiceNG, GBFormGroup } from '@goodbooks/uicore';
import { ILoginDetail } from './../../../../../Features/Common/shared/models/Login/LoginDetail';
import { GBBaseFormGroup } from '@goodbooks/uicore';
import { Versionservice } from './../../../../../Features/Common/shared/services/Login/Version.service';
import { Select, Store } from '@ngxs/store'
import { Observable, Subscription } from 'rxjs';
import {AppState} from './../Model/app.state'
import { UserAccount } from './../../../../../libs/gbui/src/lib/models/addaccount.model'
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  providers: [
    { provide: 'PageService', useClass: GBDataPageServiceNG},
    {provide: 'DataService', useClass: Versionservice}
  ]
})
export class LoginComponent extends GBBasePageComponentNG {
  form: GBBaseFormGroup;
  title = 'login';
  label = 'Login';
  @Select(AppState.Getuseraccount) useraccount$: Observable<UserAccount>;
  thisConstructor() {
    this.form = this.gbps.fb.group({
      Server: ['GB4SC', Validators.required],
      UserName: ['ADMIN', Validators.required],
      Password: ['admin', Validators.required]
    }) as GBBaseFormGroup;
  }
  public login() {
    const logindata: ILoginDetail = this.form.value as ILoginDetail;
    (this.gbps.dataService as Versionservice).getversioninfo(logindata);
  }
  public clear() {
    this.form.reset();
  }
}

