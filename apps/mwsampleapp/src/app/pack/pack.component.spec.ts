import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { packComponent } from './pack.component';

describe('packComponent', () => {
  let component: packComponent;
  let fixture: ComponentFixture<packComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ packComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(packComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
