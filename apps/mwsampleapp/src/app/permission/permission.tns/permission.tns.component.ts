import { Component, Injector, OnInit } from '@angular/core';
import { permissionComponent } from '../permission.component';
import { GBDataPageServiceNG } from '../../../../../../libs/uicore/src/lib/uicore.module';
//import { permissionservice } from '../../../../../../libs/gbui/src/lib/services/permission/permission.service';
import { FormControl, FormGroup } from "@angular/forms";
import { permissiondetail } from './../../../../../../Features/erpmodules/samplescreens/models/permission/ipermission.model';

@Component({
    selector: 'app-permissiontns',
    templateUrl: './permission.tns.component.tns.html',
    providers: [
        { provide: 'PageService', useClass: GBDataPageServiceNG },
        //{ provide: 'DataService', useClass: permissionservice }
    ]
})
export class permissiontnsComponent {
    timeslipid = '0'
    timeslipnumber = '0'
    public Permissiondetails: permissiondetail;
    form = new FormGroup({
        type: new FormControl(''),
        date: new FormControl(''),
        fromtime: new FormControl(''),
        totime: new FormControl(''),
        duration: new FormControl(''),
        remarks: new FormControl(''),
        location: new FormControl(''),


    });



    thisConstructor() { }
    ngOnInit() {
        //this.form.setValue({ remarks: 'hi' });
        //this.form.controls.remarks.setValue('abc');
    }


    save() {
        console.log(this.form.value)

        /*this.Permissiondetails = {
            "TimeSlipId": this.timeslipid,  //starting will be 0 updated by get service 
            "OuId": 0,    //from logindto
            "PeriodId": 0,//from logindto
            "TimeslipNumber": this.timeslipnumber,//starting will be 0 updated by get service
            "TimeslipDate": getEPOCDate(new Date),
            "EmployeeId": 0,//this.loginDTO.UserId
            "AttendanceDate": getEPOCDate(this.form.value.date),//selected date will be in epoch
            "Shiftid": -1,
            "Type": this.permissiontype, // 1 - permission 0 - onduty
            "FromTime": this.fromtimemins, // 
            "ToTime": this.totimemins,//
            "Duration": this.duration,//
            "Reason": this.form.value.remarks,//remarks from form
            "ApprovedbyId": "-1",
            "Version": 1,
            "Status": 5,
            "TimeslipStartTime": this.fromtimemins, //
            "TimeslipEndTime": this.totimemins,//
            "OndutyLocation": this.form.value.location// if onduty location will get from form
           
        }*/
    }


    dfPropertyCommitted(args) {

        if (args.propertyName === "fromtime") {

            this.duration()
        }
        if (args.propertyName === "totime") {
            this.duration()
        }
    }
    duration() {

        if (this.form.value.fromtime != "" && this.form.value.totime != "") {
            // alert(this.form.value.fromtime+"and"+this.form.value.totime)

            var fromtime = this.form.value.fromtime
            var totime = this.form.value.totime
            var totalduration

            var splitfromtime = fromtime.split(":");
            var fromtimehourintomins = splitfromtime[0] * (60);
            var fromtimesplitmins = splitfromtime[1];
            var fromtimemins = ((fromtimehourintomins) + parseInt(fromtimesplitmins))
            /////////////////////////////////////////////////////////
            var splittotime = totime.split(":");
            var totimehourintomins = splittotime[0] * (60);
            var totimesplitmins = splittotime[1];
            var totimemins = ((totimehourintomins) + parseInt(totimesplitmins))//550 
            console.log("totimemins" + totimemins)
            /*if (this.totimeglobal == this.fromtimeglobal) {
        
                alert("From time and To time should not same")
            }
            else if (this.totimemins < this.fromtimemins) {
        
                alert("To time should be greater then From time ")
            }*/
            /////////////////////////////////////////////////////////
            var duration = totimemins - fromtimemins;//60
            var durationhrs = duration / 60;
            var durationmins = duration % 60;
            if (durationmins < 10) {
                totalduration = Math.floor(durationhrs) + ":" + "0" + durationmins;
            }
            else {
                totalduration = Math.floor(durationhrs) + ":" + durationmins;
            }
            console.log("totalduration" + totalduration)



            //this.form.controls['remarks'].setValue(totalduration)
            //this.form.get('remarks').setValue(totalduration);
            this.form.controls.duration.patchValue(totalduration);
           // this.form.setValue({ remarks: 'hi' });

        }
    }
}


function getEPOCDate(date) {
    if (date == "") {
        date = new Date();
    }
    if (date == undefined) {
        date = new Date();
    }
    var DDt = new Date(date);
    var DDtYear = DDt.getFullYear();
    var DDtMonth = DDt.getMonth();
    var DDtDay = DDt.getDate();
    return ("/Date(" + Date.UTC(DDtYear, DDtMonth, DDtDay) + ")/");
}

