
import { GBDataFormGroupStoreWN, GBDataFormGroupWN } from '@goodbooks/uicore';
import { HttpClient } from '@angular/common/http';
import { Select, Store } from '@ngxs/store';
import { GBDataStateActionFactoryWN } from '@goodbooks/gbdata';
import { permissiondetail } from './../../../../../Features/erpmodules/samplescreens/models/permission/ipermission.model';

import { permissionservice } from './../../../../../Features/erpmodules/samplescreens/services/permission/permission.service'

import { PermissionStateActionFactory } from './../../../../../Features/erpmodules/samplescreens/stores/permission/permission.actionfactory';
import { Observable } from 'rxjs';
import { PermissionState } from "./../../../../../Features/erpmodules/samplescreens/stores/permission/permission.state"
export class PermissionFormgroup extends GBDataFormGroupWN<permissiondetail> {
  constructor(http: HttpClient, dataService: permissionservice) {
    super(http, 'SC005', {}, dataService)
  }

  public clear() {
    super.clear();
    // .
    // or below code
    // .
    // this.reset();
  }

  public delete() {
    super.delete();
    // .
    // or below code
    // .
    // const pdata: IPack = this.value as IPack;
    // this.ds.deleteData(pdata[this.ds.IdField]);
    // this.reset();
  }

  public save() {
    super.save();
    // .
    // or below code
    // .
    // const pdata: IPack = this.value as IPack;
    // this.ds.saveData(pdata);
  }

  public moveFirst() {
    super.moveFirst();
    // .
    // or below code
    // .
    // this.ds.moveFirst().subscribe(r => {
    //   const pk: IPack = r;
    //   this.patchValue(pk);
    // });
  }

  public movePrev() {
    super.movePrev();
    // .
    // or below code
    // .
    // this.ds.movePrev().subscribe(r => {
    //   const pk: IPack = r;
    //   this.patchValue(pk);
    // });
  }

  public moveNext() {
    super.moveNext();
    // .
    // or below code
    // .
    // this.ds.moveNext().subscribe(r => {
    //   const pk: IPack = r;
    //   this.patchValue(pk);
    // });
  }

  public moveLast() {
    super.moveLast();
    // .
    // or below code
    // .
    // this.ds.moveLast().subscribe(r => {
    //   const pk: IPack = r;
    //   this.patchValue(pk);
    // });
  }
}

export class PermissionFormgroupStore extends GBDataFormGroupStoreWN<permissiondetail> {
  constructor(http: HttpClient, store: Store) {
    super(http, 'SC005', {}, store, new PermissionStateActionFactory())
  }

  public clear() {
    super.clear();
    // .
    // or below code
    // .
    // this.store.dispatch(this.af.ClearData())
  }

  public delete() {
    super.delete();
    // .
    // or below code
    // .
    // const pdata: IPack = this.value as IPack;
    // this.ds.deleteData(pdata[this.ds.IdField]);
    // this.reset();
  }

  public save() {
    super.save();
    // .
    // or below code
    // .
    // const pdata: IPack = this.value as IPack;
    // this.ds.saveData(pdata);
  }

  public moveFirst() {
    super.moveFirst();
    // .
    // or below code
    // .
    // this.store.dispatch(this.af.MoveFirst());
  }

  public movePrev() {
    super.movePrev();
    // .
    // or below code
    // .
    // this.store.dispatch(this.af.movePrev());
  }

  public moveNext() {
    super.moveNext();
    // .
    // or below code
    // .
    // this.store.dispatch(this.af.moveNext());
  }

  public moveLast() {
    super.moveLast();
    // .
    // or below code
    // .
    // this.store.dispatch(this.af.moveLast());
  }
}

