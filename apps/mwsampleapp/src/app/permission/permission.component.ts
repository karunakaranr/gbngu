

import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import { GBBasePageComponentNG, GBBasePageComponent, GBDataPageServiceNG,GBBaseDataPageStoreComponentWN } from '@goodbooks/uicore';
import { ILoginDetail } from './../../../../../Features/Common/shared/models/Login/LoginDetail';
import { GBBaseFormGroup } from '@goodbooks/uicore';
import { Versionservice } from './../../../../../Features/Common/shared/services/Login/Version.service';

declare let require: any;
import { AbstractDS } from '@goodbooks/gbdata';
import { GBPageService } from '@goodbooks/uicore';

import { Time } from "@angular/common";
import { permissiondetail } from './../../../../../Features/erpmodules/samplescreens/models/permission/ipermission.model';

import { permissionservice } from './../../../../../Features/erpmodules/samplescreens/services/permission/permission.service'

import { permissiondbservice } from './../../../../../Features/erpmodules/samplescreens/dbservices/permission/permissiondb.service'
import { HttpClient } from '@angular/common/http';


import {
  GBBaseDataPageComponentWN,
  GBBaseDataPageStoreComponentWN,
  GBDataFormGroupWN,
} from '@goodbooks/uicore';


import {
  getThisDataServiceWN,
  getThisDBDataService,
  getThisDataService,
  getThisGBPageService,
} from './permission.factory';
import {
  PermissionFormgroup,
  PermissionFormgroupStore,
} from './permission.formgroup';
//import { Time } from "@angular/common";

import { Select, Store } from '@ngxs/store';
import { GBHttpService } from '@goodbooks/gbcommon';
import { Observable, Subscription } from 'rxjs';
import { tap } from 'rxjs/operators';
import { PermissionState } from "./../../../../../Features/erpmodules/samplescreens/stores/permission/permission.state";
import { GetAll, GetData, MoveFirst } from './../../../../../Features/erpmodules/samplescreens/stores/permission/permission.action';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';


@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,//*
  providers: [
    { provide: 'IdField', useValue: 'idfield' },
    { provide: 'url', useValue: 'url' },
    { provide: 'Store', useClass: Store },
    { provide: 'DBDataService', useClass: permissiondbservice, deps: [GBHttpService], },
    { provide: 'DataService', useClass: permissionservice, deps: ['DBDataService'] },
    {
      provide: 'PageService', useFactory: getThisGBPageService, deps: [Store,
        'DataService',
        'DBDataService',
        FormBuilder,
        GBHttpService,
        ActivatedRoute,
        Router,
      ],
    },
  ]
})
export class permissionComponent extends GBBaseDataPageStoreComponentWN<permissiondetail>
  implements OnInit {
  isDataPage = true;
  hasNavigation = false;
  title = 'Permission';
  dataService: AbstractDS;

  form: PermissionFormgroupStore = new PermissionFormgroupStore(
    this.gbps.gbhttp.http,
    this.gbps.store
  );
  @Select(PermissionState) currentData$;


  thisConstructor() { }
  loadScreen(recid?: string) {
    if (recid) {
      const id = parseInt(recid);
      this.gbps.store.dispatch(new GetData(id));
    }
    else {
      this.gbps.store.dispatch(new GetAll(null)).subscribe(() => {
        this.gbps.store.dispatch(new MoveFirst());
      });
    }
  }

  ngOnInit(): void {
    let id = '';
    this.gbps.activeroute.params.subscribe((params: any) => {
      id = params['id'];
    });
    this.gbps.activeroute.paramMap.subscribe((params: ParamMap) => {
      id = params.get('id')
    });
    if (this.gbps.activeroute.firstChild) {
      this.gbps.activeroute.firstChild.params.subscribe((params: ParamMap) => {
        id = params['id'];
      });
    }
    this.loadScreen(id);
  }

  save() {
    console.log("save")
    console.log(this.form.value)
    //this.permission.permissionservice(this.form.value):permissiondetail
    // const permissionjson: permissiondetail = this.form.value as permissiondetail;
    // (this.gbps.dataService as permissionservice).permissionservice(permissionjson);
  }

}


/*
import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GBBasePageComponentNG, GBBasePageComponent, GBDataPageServiceNG,GBBaseDataPageStoreComponentWN } from '@goodbooks/uicore';
import { ILoginDetail } from './../../../../../Features/Common/shared/models/Login/LoginDetail';
import { GBBaseFormGroup } from '@goodbooks/uicore';
import { Versionservice } from './../../../../../Features/Common/shared/services/Login/Version.service';
import { GBBaseDataPageComponentWN, GBDataFormGroupWN, GBDataPageService, GBFormGroup } from '@goodbooks/uicore';
//import {
 //// PackFormgroup,
 // PackFormgroupStore,
//} from './permission.formgroup';
import { SelectedIndexChangedEventData, DropDown } from "nativescript-drop-down";
declare let require: any;
import { AbstractDS } from '@goodbooks/gbdata';
import {  GBPageService } from '@goodbooks/uicore';
//var ModalPicker = require("nativescript-modal-datetimepicker").ModalDatetimepicker;
//var picker = new ModalPicker();
import {permissionservice} from './../../../../../libs/gbui/src/lib/services/permission/permission.service'
import { Time } from "@angular/common";
import { permissiondetail } from './../../../../../libs/gbui/src/lib/models/ipermission.model';
@Component({
  selector: 'app-permission',
  templateUrl: './permission.component.html',
  styleUrls: ['./permission.component.scss'],

})
export class permissionComponent  {
    isDataPage = true;
    hasNavigation = false;
    title = 'Profile';
    dataService: AbstractDS;
    form: GBFormGroup

    save()
    {
        console.log(this.form.value)

    }

}

*/
