// import { HttpClient } from '@angular/common/http';
import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NativeScriptModule } from '@nativescript/angular';

import { AppRoutingModule } from '@src/app/app-routing.module';
import { AppComponent } from '@src/app/app.component';
import { HomeComponent } from '@src/app/home/home.component';
import { LoginComponent } from './login/login.component';
import { packComponent } from './pack/pack.component'
import { permissionComponent } from './permission/permission.component'
import { permissiontnsComponent } from './permission/permission.tns/permission.tns.component'
import { addaccountComponent } from './addaccount/addaccount.component'
import { HttpClientModule } from '@angular/common/http';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsStoragePluginModule, StorageOption } from '@ngxs/storage-plugin';
import { NgxsResetPluginModule } from 'ngxs-reset-plugin';
import { AppState } from './Model/app.state';
import { environment } from '../environments/environment';
import { NativeScriptFormsModule } from "@nativescript/angular";
import { GbcommonModule } from '@goodbooks/gbcommon';
import { ReactiveFormsModule } from '@angular/forms';
import { STORAGE_ENGINE } from '@ngxs/storage-plugin';
//import { AngularFileUploaderModule } from "angular-file-uploader";
import { sqliteStorage } from './Model/storage'
//import { DropDownModule } from "nativescript-drop-down/angular";
//import {FeatureCommonmodule} from './../../../../Features/Common/featurecommon.module'
//(global as any).localStorage=new Storage()
import { NativeScriptCommonModule } from "@nativescript/angular";
import { NativeScriptUIDataFormModule } from 'nativescript-ui-dataform/angular';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    packComponent,
    permissionComponent,
    addaccountComponent,
    permissiontnsComponent,
  ],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
    NativeScriptModule,
    //  AngularFileUploaderModule,
    ReactiveFormsModule,
    NativeScriptFormsModule,
    NativeScriptCommonModule,
    NativeScriptUIDataFormModule,
    // DropDownModule,
    // FeatureCommonmodule,
    HttpClientModule,
    NgxsModule.forRoot([AppState], { developmentMode: !environment.production }),
    GbcommonModule.forRoot(environment),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsResetPluginModule.forRoot(),
    NgxsStoragePluginModule.forRoot(),
    // NgxsStoragePluginModule.forRoot({storage: StorageOption.LocalStorage,key:'auth'}),
    // NativeScriptRouterModule,


    // DropDownModule,
    // TNSCheckBoxModule,
    // NativeScriptHttpClientModule,
    // NativeScriptUIListViewModule,
    // NativeScriptUISideDrawerModule,
    // NativeScriptRouterModule.forRoot(routes),
  ],
  providers: [
    { provide: STORAGE_ENGINE, useClass: sqliteStorage },
  ],
  bootstrap: [AppComponent],
  schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
