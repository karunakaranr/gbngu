import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
// import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";
// import { isAndroid, isIOS, Device, Screen } from '@nativescript/core';
import { GBHttpService } from '@goodbooks/gbcommon';
import { HttpClient, HttpParams } from '@angular/common/http';
//import { MenudbService } from './../../../../../libs/gbui/src/lib/dbservices/menu/menudb.service';
@Component({
  selector: 'gbmw-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  title = 'test';
  bdaydate = '2010-01-01';
  afuConfig;
  uploadedFiles: Array<File>;
  constructor(
    private router: Router,
    private http: HttpClient,
   // private menudbservice: MenudbService
  ) {
    // console.log(`Running on Android? ${isAndroid}`);
    // console.log(`Running on iOS? ${isIOS}`);
    // console.log(`device.model ${Device.model}`); // For example: "Nexus 5" or "iPhone".
    // console.log(`device.deviceType ${Device.deviceType}`); // "Phone" | "Tablet"
    // console.log(`device.os ${Device.os}`); // For example: "Android" or "iOS".
    // console.log(`device.osVersion ${Device.osVersion}`); // For example: 4.4.4(android), 8.1(ios)
    // console.log(`device.sdkVersion ${Device.sdkVersion}`); //  For example: 19(android), 8.1(ios).
    // console.log(`device.language ${Device.language}`); // For example "en" or "en-US".
    // console.log(`device.manufacturer ${Device.manufacturer}`); // For example: "Apple" or "HTC" or "Samsung".
    // console.log(`device.uuid ${Device.uuid}`); // The unique identification number
    // console.log(`device.region ${Device.region}`); //  For example "US".
    // console.log(`screen.mainScreen.heightDIPs ${Screen.mainScreen.heightDIPs}`); // The absolute height of the screen in density independent pixels.
    // console.log(`screen.mainScreen.heightPixels ${Screen.mainScreen.heightPixels}`); // The absolute height of the screen in pixels.
    // console.log(`screen.mainScreen.scale ${Screen.mainScreen.scale}`); // The logical density of the display.
    // console.log(`screen.mainScreen.widthDIPs ${Screen.mainScreen.widthDIPs}`); // The absolute width of the screen in density independent pixels.
    // console.log(`screen.mainScreen.widthPixels ${Screen.mainScreen.widthPixels}`); // The absolute width of the screen in pixel
  }
  ngOnInit() {
    /* this.afuConfig = {
      uploadAPI: {
       url:"http://169.56.148.10:82/gb4/fws/Entity.svc/FileUploadWebWithMetadata/?ObjectId=492590354&ObjectTypeId=-1399999744"
       // url:"http://119.81.2.42:91/gb4/fws/Entity.svc/FileUploadWebWithMetadata/?ObjectId=777865077&ObjectTypeId=-1399999827"
      }
  };*/
  }

  login() {
    // console.log("alert")
    // alert("login ")
    this.router.navigate(['/login']);
  }
  gotomain() {
    this.router.navigate(['/main']);
  }
  attach()
  {
    this.router.navigate(['/attach']);
  }
  navigateTo(url) {
    this.router.navigate([url]);
  }
  
  addaccount() {
    // console.log("alert")
    // alert("login ")
    this.router.navigate(['/addaccount']);
  }

  pack() {
    this.router.navigate(['/main']);
  }

}
