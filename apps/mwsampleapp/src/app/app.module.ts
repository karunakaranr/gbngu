import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from '@src/app/app-routing.module';
import { AppComponent } from '@src/app/app.component';
import { HomeComponent } from '@src/app/home/home.component';
import { LoginComponent } from './login/login.component';
import { packComponent } from './pack/pack.component'
import { permissionComponent } from './permission/permission.component'
import { attachmentComponent } from './attachment/attachment.component'
import { HttpClientModule } from '@angular/common/http';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsStoragePluginModule, StorageOption } from '@ngxs/storage-plugin';
import { NgxsModule } from '@ngxs/store';
import { NgxsResetPluginModule } from 'ngxs-reset-plugin';
import { AppState } from './Model/app.state';
import { environment } from '../environments/environment';
import { FormsModule } from '@angular/forms';

import { GbcommonModule } from '@goodbooks/gbcommon';
// import { AgeFromDatePipe } from '../../../../libs/gbcommon/src/lib/age-from-date.pipe'
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDividerModule } from '@angular/material/divider';
import { ReactiveFormsModule } from '@angular/forms';
import { UicoreModule } from '@goodbooks/uicore';
import { UifwkUifwkmaterialModule } from '@goodbooks/uifwkmaterial';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
//import { AngularFileUploaderModule } from "angular-file-uploader";
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    packComponent,
    permissionComponent,
    attachmentComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatFormFieldModule,
    //AngularFileUploaderModule,
    HttpClientModule,
    NgxsModule.forRoot([AppState], { developmentMode: !environment.production }),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsResetPluginModule.forRoot(),
    NgxsStoragePluginModule.forRoot({ storage: StorageOption.SessionStorage }),

    GbcommonModule.forRoot(environment),
    UifwkUifwkmaterialModule,
    UicoreModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
