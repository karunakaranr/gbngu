import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "@nativescript/angular";
import { NativeScriptUIDataFormModule } from 'nativescript-ui-dataform/angular';

import { TestFormRoutingModule } from "./testform-routing.module";
import { TestFormComponent } from "./testform.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptUIDataFormModule,
        TestFormRoutingModule
    ],
    declarations: [
        TestFormComponent,
        // RadDataForm,
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class TestFormModule { }
