import { Component, OnInit } from "@angular/core";
import { FormControl, FormGroup } from "@angular/forms";
import { Person } from "./person-model";

// import { ButtonEditorHelper } from "./customeditor/button-editor-helper";
// import { android as androidApplication } from "tns-core-modules/application";

@Component({
    selector: "TestForm",
    templateUrl: "./testform.component.html"
})
export class TestFormComponent implements OnInit {
    form = new FormGroup({
        name: new FormControl('John'),
        age: new FormControl('20'),
        email:new  FormControl('john@company.com'),
        city:new  FormControl('New York'),
        street:new  FormControl('5th Avenue'),
        streetNumber:new  FormControl('22'),
        birthDate:new  FormControl('2010-11-20'),
       // married:new  FormControl(''),
        marriedDate:new  FormControl('2010-11-20')
   }); 

   // private _person: Person;
    constructor() {
        // Use the component constructor to inject providers.
      //  this._person = new Person("John", 23, "john@company.com", "New York", "5th Avenue", 11, true, "2010-11-20");
    }

    ngOnInit(): void {
        // Use the "ngOnInit" handler to initialize data for the view.
    }

   /* set person(value: Person) {
        // this.set("_person", value);
        this._person = value;
    }

    get person(): Person {
        // return this.get("_person");
        return this._person;
    }*/

    save()
    {
        console.log(this.form.value)
        alert(this.form.value)
    }


    // private _buttonEditorHelper;
    // public editorNeedsView(args) {
    //     if (androidApplication) {
    //         this._buttonEditorHelper = new ButtonEditorHelper();
    //         this._buttonEditorHelper.editor = args.object;
    //         const androidEditorView: android.widget.Button = new android.widget.Button(args.context);
    //         const that = this;
    //         androidEditorView.setOnClickListener(new android.view.View.OnClickListener({
    //             onClick(view: android.view.View) {
    //                 that.handleTap(view, args.object);
    //             }
    //         }));
    //         args.view = androidEditorView;
    //         this.updateEditorValue(androidEditorView, this._person.age);
    //     } else {
    //         this._buttonEditorHelper = new ButtonEditorHelper();
    //         this._buttonEditorHelper.editor = args.object;
    //         const iosEditorView = UIButton.buttonWithType(UIButtonType.System);
    //         iosEditorView.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left;
    //         iosEditorView.addTargetActionForControlEvents(this._buttonEditorHelper, "handleTap:", UIControlEvents.TouchUpInside);
    //         args.view = iosEditorView;
    //     }
    // }

    // public editorHasToApplyValue(args) {
    //     this._buttonEditorHelper.updateEditorValue(args.view, args.value);
    // }

    // public editorNeedsValue(args) {
    //     args.value = this._buttonEditorHelper.buttonValue;
    // }

    // public updateEditorValue(editorView, value) {
    //     this._buttonEditorHelper.buttonValue = value;
    //     editorView.setText(this._buttonEditorHelper.buttonValue + " (tap to increase)");
    // }

    // public handleTap(editorView, editor) {
    //     const newValue = this._buttonEditorHelper.buttonValue + 1;
    //     this.updateEditorValue(editorView, newValue);
    //     editor.notifyValueChanged();
    // }
}
