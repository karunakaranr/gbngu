import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateOrderDetailsComponent } from './updateorderdetails.component';

describe('UpdateOrderDetailsComponent', () => {
  let component: UpdateOrderDetailsComponent;
  let fixture: ComponentFixture<UpdateOrderDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateOrderDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateOrderDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
