import { Component, OnInit } from '@angular/core';
const orderdetailscolumndata = require('./../../assets/orderdetailscolumndata.json')
const orderdetailsrowdata = require('./../../assets/orderdetailsrowdata.json')

@Component({
  selector: 'app-updateorderdetails',
  templateUrl: './updateorderdetails.component.html',
  styleUrls: ['./updateorderdetails.component.scss']
})
export class UpdateOrderDetailsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    console.log(orderdetailscolumndata,orderdetailsrowdata);
  }
  columnDefs = orderdetailscolumndata
  rowData = orderdetailsrowdata

}
