import { Component, OnInit } from '@angular/core';

const outstandingcolumndata = require('./../../../assets/outstandingcolumndata.json');
const outstandingrowdata = require('./../../../assets/outstandingrowdata.json');
const orderstatuscolumndata = require('./../../../assets/orderstatuscolumndata.json');
const orderstatusrowdata = require('./../../../assets/orderstatusrowdata.json');

@Component({
  selector: 'app-ad',
  templateUrl: './ad.component.html',
  styleUrls: ['./ad.component.scss']
})
export class AdComponent implements OnInit {

  constructor() {

  }

  ngOnInit(): void {

    console.log(outstandingcolumndata, outstandingrowdata);
    console.log(orderstatuscolumndata,orderstatusrowdata)
  }
  columnDefs = outstandingcolumndata;
  rowData = outstandingrowdata;
  columnDefs2 = orderstatuscolumndata;
  rowData2 = orderstatusrowdata;

}
