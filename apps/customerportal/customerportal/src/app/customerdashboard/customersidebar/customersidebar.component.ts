import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Reportid } from 'features/commonreport/datastore/commonreport.action';

@Component({
  selector: 'app-customersidebar',
  templateUrl: './customersidebar.component.html',
  styleUrls: ['./customersidebar.component.scss'],
})
export class CustomerSidebarComponent implements OnInit {
  constructor(public router: Router) {}

  ngOnInit(): void {}

  public orderlist() {
    let id = -1399999494;
    this.router.navigate([`main/commonreport/${id}`]);
    // this.router.navigate(['commonreport/:-1399999494']);
  }
}
