import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerMainContentComponent } from './../customermaincontent/customermaincontent.component';
import { CustomerOrderComponent } from './../../customerorder/customerorder.component';

const routes: Routes = [
  {
    path: 'main',
    component: CustomerMainContentComponent,
  },
  {
    path: '/customerorder',
    component: CustomerOrderComponent,
  },
  { path: '', redirectTo: '/customerorder', pathMatch: 'full' },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class CustomerMainContentRoutingModule {}
