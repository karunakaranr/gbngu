import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CustomerLoginComponent } from './customerlogin/customerlogin.component';
import { CustomerMainContentComponent } from './customerdashboard/customermaincontent/customermaincontent.component';
import { CustomerDashboardComponent } from './customerdashboard/customerdashboard.component';
import { CustomerOrderStatusComponent } from './customerorderstatus/customerorderstatus.component';
import { CustomerOrderComponent } from './customerorder/customerorder.component';
import { CommonreportComponent } from 'features/commonreport/screens/commonreport.component';

const routes: Routes = [
  {
    path: '',
    component: CustomerLoginComponent,
  },
  {
    path: 'main',
    component: CustomerDashboardComponent,
    children: [
      {
        path: 'customerorder',
        component: CustomerOrderComponent,
      },
      {
        path: 'customerdispatch',
        component: CustomerOrderStatusComponent,
      },
      {
        path: 'commonreport/:id',
        component: CommonreportComponent,
      },
    ],
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
