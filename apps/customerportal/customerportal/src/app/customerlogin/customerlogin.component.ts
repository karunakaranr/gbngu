import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { Versionservice } from '../../../../../../features/common/shared/services/Login/Version.service';
import { FormBuilder } from '@angular/forms';
import { FormGroup, Validators } from '@angular/forms';
import { ILoginDetail } from 'features/common/shared/models/Login/LoginDetail';
import { Router } from '@angular/router';
import { VersionDBservice } from './../../../../../../features/common/shared/dbservices/Login/VersionDB.service';
import { GBBasePageComponentNG, GBBasePageComponent } from './../../../../../../libs/uicore/src/lib/components/Base/GBBasePage';
import { GBDataPageServiceNG } from './../../../../../../libs/uicore/src/lib/services/gbpage.service';
import { GBFormGroup, GBBaseFormGroup } from './../../../../../../libs/uicore/src/lib//classes/GBFormGroup';


@Component({
  selector: 'app-customerlogin',
  templateUrl: './customerlogin.component.html',
  styleUrls: ['./customerlogin.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
  providers: [
    { provide: 'PageService', useClass: GBDataPageServiceNG },
    { provide: 'DataService', useClass: Versionservice }
  ]
})
export class CustomerLoginComponent extends GBBasePageComponentNG {
  form: GBBaseFormGroup;
  title = 'login';
  // label = 'Login';
  // componentName = 'user';
  thisConstructor() {
    this.form = this.gbps.fb.group({
      Server: ['', Validators.required],
      UserName: ['', Validators.required],
      Password: ['', Validators.required]
    }) as GBBaseFormGroup;

  }
  public Login() {
    const logindata: ILoginDetail = this.form.value as ILoginDetail;
    (this.gbps.dataService as Versionservice).getversioninfo(logindata);
  }
  public clear() {
    this.form.reset();
  }
  hide = true;

}












  // gbps: any;

  // constructor(public fb: FormBuilder, public router: Router) {

  //   this.form = this.fb.group({
  //     Server: [''],
  //     UserName: [''],
  //     Password: ['']

  //   })




    // alert(this.form = this.fb.group)

    // if (this.form.get('UserName').value == "ADMIN" && this.form.get('Password').value == "ADMIN") {

    //   alert("login successfully")
    // }
    // else {
    //   alert("please check username or password")
    // }
