import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
// import { 2CustomerMainContentRoutingModule } from './customerdashboard/customermaincontent/customermaincontent.routing.module';
import { CustomerLoginComponent } from './customerlogin/customerlogin.component';
import { AppComponent } from './app.component';
import { CustomerDashboardComponent } from './customerdashboard/customerdashboard.component';
import { CustomerHeaderComponent } from './customerdashboard/customerheader/customerheader.component';
import { AdComponent } from './customerdashboard/ad/ad.component';
import { CustomerNavbarComponent } from './customerdashboard/customernavbar/customernavbar.component';
import { CustomerSidebarComponent } from './customerdashboard/customersidebar/customersidebar.component';
import { CustomerMainContentComponent } from './customerdashboard/customermaincontent/customermaincontent.component';
import { CustomerOrderComponent } from './customerorder/customerorder.component';
import { CustomerOrderStatusComponent } from './customerorderstatus/customerorderstatus.component';
import { UpdateOrderDetailsComponent } from './updateorderdetails/updateorderdetails.component';
import { CustomerFooterComponent } from './customerdashboard/customerfooter/customerfooter.component';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { AgGridModule } from 'ag-grid-angular';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { GbcommonModule } from '@goodbooks/gbcommon';
import { UicoreModule } from '@goodbooks/uicore';
import { UifwkUifwkmaterialModule } from '@goodbooks/uifwk/material';
import { GbuiModule } from '@goodbooks/gbui';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsStoragePluginModule, StorageOption } from '@ngxs/storage-plugin';
import { NgxsModule } from '@ngxs/store';
import { NgxsResetPluginModule } from 'ngxs-reset-plugin';
import { environment } from './../../../../Goodbooks/Goodbooks/src/environments/environment';
import { AppState } from './../../../../Goodbooks/Goodbooks/src/app/Datastore/app.state';
import { AuthState } from './../../../../../features/common/shared/stores/auth.state';
import { GbgridModule } from './../../../../../features/gbgrid/components/gbgrid/gbgrid.module';
import { GbSharedModule } from './../../../../Goodbooks/Goodbooks/src/app/sharedscreens/Shared/gbshared.module';
// import { TranslocoRootModule } from './transloco/transloco-root.module';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HttpClientModule } from '@angular/common/http';
// import {DateFormaterPipe} from './../../../../../libs/gbcommon/src/lib/Date-Formater.pipe'
// import { APP_INITIALIZER } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';
import { UOMlistComponent } from 'features/erpmodules/inventory/screens/UOM/UOMList/uomlist.component';
import { GBConfigService } from './../../../../../libs/gbcommon/src/lib/services/config/gbconfig.service';
import { CommonReportModule } from './../../../../../features/commonreport/commonreport.module';
import { DatewiseminComponent } from 'features/erpmodules/inventory/screens/Register/DateWise/Datewise Min and Inward/Datewisemin.component';
import { DateFormaterPipe } from './../../../../../libs/gbcommon/src/lib/Date-Formater.pipe';
import {ShortNumberPipe} from './../../../../../libs/gbcommon/src/lib/short-number.pipe';
import {TruncatePipe} from './../../../../../libs/gbcommon/src/lib/TruncateText.pipe'; 
import {NumberFormatPipe} from './../../../../../libs/gbcommon/src/lib/Number-Formatter.pipe';

export function initializeApp(appConfig: GBConfigService) {
  return () => appConfig.load();
}

@NgModule({
  declarations: [
    DateFormaterPipe,
    AppComponent,
    CustomerDashboardComponent,
    CustomerHeaderComponent,
    CustomerNavbarComponent,
    CustomerSidebarComponent,
    UpdateOrderDetailsComponent,
    UOMlistComponent,
    DatewiseminComponent,
    AdComponent,
    CustomerMainContentComponent,
    CustomerOrderComponent,
    CustomerOrderStatusComponent,
    CustomerFooterComponent,
    CustomerLoginComponent,
    ShortNumberPipe,
    TruncatePipe,
    NumberFormatPipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    // RouterModule.forRoot([], { initialNavigation: 'enabled', relativeLinkResolution: 'legacy' }),
    MatIconModule,
    GbgridModule,
    MatMenuModule,
    MatInputModule,
    MatSelectModule,
    // DateFormaterPipe,
    MatButtonModule,
    MatCardModule,
    AgGridModule,
    CommonReportModule,
    MatFormFieldModule,
    AppRoutingModule,
    // MainContentRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    AppRoutingModule,
    //RouterModule.forRoot([], { initialNavigation: 'enabled' }),
    UifwkUifwkmaterialModule,
    UicoreModule,
    ReactiveFormsModule,
    GbcommonModule.forRoot(environment),
    GbuiModule,
    GbSharedModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
    // TranslocoRootModule,
    NgxsModule.forRoot([AuthState], {
      developmentMode: !environment.production,
    }),
    NgxsReduxDevtoolsPluginModule.forRoot({ disabled: environment.production }),
    NgxsResetPluginModule.forRoot(),
    NgxsStoragePluginModule.forRoot({ storage: StorageOption.SessionStorage }),
    NgxSpinnerModule,
    // FeatureCommonmodule,
    // GBLayoutModule,
  ],
  exports :[
    DateFormaterPipe,
    ShortNumberPipe,
    TruncatePipe,
    NumberFormatPipe
  ],
  providers: [
    GBConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [GBConfigService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
