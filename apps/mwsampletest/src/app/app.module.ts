import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from '@src/app/app-routing.module';
import { AppComponent } from '@src/app/app.component';
import { HomeComponent } from '@src/app/home/home.component';

import { HttpClientModule } from '@angular/common/http';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';
import { NgxsLoggerPluginModule } from '@ngxs/logger-plugin';
import { NgxsStoragePluginModule, StorageOption } from '@ngxs/storage-plugin';
import { NgxsModule } from '@ngxs/store';
import { NgxsResetPluginModule } from 'ngxs-reset-plugin';
import { AppState } from './Model/app.state';
import { environment } from '../environments/environment';
import { FormsModule } from '@angular/forms';


import { BarcelonaModule } from '@src/app/barcelona/barcelona.module';
import { BrandComponent } from '@src/app/brand/brand.component';
import { BrandlistComponent } from '@src/app/brandlist/brandlist.component';
import { GbcommonModule } from '@goodbooks/gbcommon';
import { FeatureCommonmodule } from './../../../../Features/Common/featurecommon.module';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDividerModule } from '@angular/material/divider';
import { ReactiveFormsModule } from '@angular/forms';
//import { UicoreModule } from '@goodbooks/uicore';
//import { UifwkUifwkmaterialModule } from '@goodbooks/uifwkmaterial';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BrandComponent,
    BrandlistComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,

    MatButtonModule,
    MatIconModule,
    MatCardModule,
    MatDividerModule,
    MatFormFieldModule,
    //AngularFileUploaderModule,
    HttpClientModule,
    FeatureCommonmodule,
    NgxsModule.forRoot([AppState], { developmentMode: !environment.production }),
    NgxsReduxDevtoolsPluginModule.forRoot(),
    NgxsResetPluginModule.forRoot(),
    NgxsStoragePluginModule.forRoot({ storage: StorageOption.SessionStorage }),

    GbcommonModule.forRoot(environment),
   // UifwkUifwkmaterialModule,
    //UicoreModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
