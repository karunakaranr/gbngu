import { Routes } from '@angular/router';

import { HomeComponent } from '@src/app/home/home.component';
import { BrandComponent } from './brand/brand.component';
import {BrandlistComponent} from './brandlist/brandlist.component'
export const routes: Routes = [
  {
      path: 'player',
      redirectTo: '/players',
      pathMatch: 'full',
  },
  {
      path: 'home',
      component: HomeComponent,
  },
  {
    path : '',
    component: BrandlistComponent
  },

  {
    path:"brandform",
    component:BrandComponent
  }
];
