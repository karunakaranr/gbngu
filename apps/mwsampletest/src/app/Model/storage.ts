
import { NgxsStoragePluginModule, StorageEngine, STORAGE_ENGINE } from '@ngxs/storage-plugin';
var Sqlite = require("nativescript-sqlite");

export class sqliteStorage implements StorageEngine {
  public defaultdatabase = "Unisoft.db"
  public database: any;
  public constructor() {
    (new Sqlite(this.defaultdatabase)).then(db => {
      db.execSQL("CREATE TABLE IF NOT EXISTS AppState ( KEY NVARCHAR(100),VALUE NVARCHAR(500)  )").then(id => {
        this.database = db;
        console.log("TABLE AppState  CREATED SUCCESSFULLY");

      }, error => {
       
        console.log("CREATE TABLE ERROR", error);
      });
    }, error => {
      
      console.log("OPEN DB ERROR", error);
    });;
  }

  get length(): number {
    // SELECT COUNT(*) FROM AppState;
    // TODO: Sathya: Return record count of AppState Table
    return 1
  }

  getItem(key: string): any {
    console.log("getitem");
    (new Sqlite(this.defaultdatabase)).then(db => {
      db.execSQL("SELECT * FROM sqlite_master").then(res => {

        console.log(" AppState getitem res" + res);
      }, error => {
    
        console.log("getitem TABLE ERROR" + error);
        this.database.all("SELECT VALUE FROM AppState WHERE key=?", key).then(res => {
          return res

        })

      });
    }, error => {
      console.log("OPEN DB ERROR", error);
    });;

  }


  setItem(key: string, val: any): void {

    this.database.all("SELECT count(*) FROM AppState where key=?", key).then(rows => {
      console.log(rows)
      if (rows == 1) {
        console.log("update")
        this.database.execSQL("UPDATE AppState SET VALUE=? WHERE key =?", [val, key]).then(id => {

          console.log("UPDATE AppState RESULT", id);
        },
          error => {
           
            console.log("UPDATE AppState ERROR", error);
          });
        this.database.all("SELECT * FROM AppState").then(rows => {

          for (var row in rows) {
            console.log("UPDATE AppState value" + rows[row]);
          }
        })

      }
      else {
        console.log("ädd")
        this.database.execSQL("INSERT INTO AppState (KEY,VALUE) VALUES (?,?)", [key, val]).then(id => {

          console.log("INSERT RESULT", id);
         
        },
          error => {
            
            console.log("Insert AppState ERROR", error);
          });
        this.database.all("SELECT * FROM AppState").then(rows => {

          for (var row in rows) {
            console.log("insertvalue" + rows[row]);
          }
        })
      }

    }, error => {
      console.log(error)
    });

  }

  removeItem(key: string): void {
    // TODO: Sathya: Remove record from AppState Table where key matches parameter

    // Your logic here
  }

  clear(): void {
    // TODO: Sathya: Remove all records from AppState Table

    // Your logic here
  }

  key(val: number): string {
    // TODO: Sathya: return nth record's key field

    // Your logic here
    let a = 'keyvalue'
    return a
  }
}


