import { Injectable } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import {Adduseraccount} from "./app.action"

import {Store} from '@ngxs/store';

@Injectable({
  providedIn: 'root',
})
export class addaccountservice {


  constructor(public store:Store, private router:Router) {
   
  }


  public adduseraccount(userdetails) {
    
    this.store.dispatch(new Adduseraccount(userdetails));
    this.router.navigate(["/login"]);
      }
    }