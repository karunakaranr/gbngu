export class URLS {
  public static AUTHENTICATION = 'http://119.81.2.42:91/gb4/fws/Server.svc/AuthenticationKey';
  public static USERLOGIN = "http://119.81.2.42:91/gb4/fws/User.svc/Authenticate/?UserCode=";
  public static USERSETTING = 'http://119.81.2.42:91/gb4/fws/User.svc/UserSettingDetail/?UserId=';
  public static MODULELIST = 'prox/fws/Module.svc/ModuleList';
  public static MENULIST = 'prox/fws/Menu.svc/UserModuleMenuTree/?UserId=-1499999441&ModuleId=-1399999954';

  /* public static AUTHENTICATION =
     'prox/fws/Server.svc/AuthenticationKey';
  // public static USERLOGIN = 'http://169.56.148.10:82/gb4/fws/User.svc/Authenticate/?UserCode=';
   public static USERLOGIN = "prox/fws/User.svc/Authenticate/?UserCode=";
   public static USERSETTING = 'prox/fws/User.svc/UserSettingDetail/?UserId=';
   public static MODULELIST = 'prox/fws/Module.svc/ModuleList';
   public static MENULIST = 'prox/fws/Menu.svc/UserModuleMenuTree/?UserId=-1499999441&ModuleId=-1399999954';
 */
}
