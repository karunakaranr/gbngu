import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { GBBaseDataPageStoreComponentWN } from './../../../../../libs/uicore/src/lib/components/Base/GBBasePage';
import { IBrand } from './../../../../../Features/erpmodules/inventory/models/IBrand';
import { getThisGBPageService } from './brand.factory';
import { BrandFormgroupStore } from './brand.formgroup';
import { BrandService } from './../../../../../Features/erpmodules/inventory/services/brand/brand.service';
import { BrandDBService } from './../../../../../Features/erpmodules/inventory/dbservices/brand/branddb.service';
import { Select, Store } from '@ngxs/store';
import { GBHttpService } from './../../../../../libs/gbcommon/src/lib/services/HTTPService/GBHttp.service';
import { BrandState } from './../../../../../Features/erpmodules/inventory/stores/brand/brand.state';
import { GetAll, GetData, MoveFirst } from './../../../../../Features/erpmodules/inventory/stores/brand/brand.actions';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { RuleEngine } from './../../../../../libs/ng-bre/src/lib/core/rule-engine';
import { EnumComparers, EnumValueType, IActionSetError, IActionSetLock, IActionSetRequired } from './../../../../../libs/ng-bre/src/lib/interfaces/RulesModel';
//const brandrules = require('./brandrules.json');
declare let require: any;
const datajson = require('./../brandapiresponse.json'); // for Testing
const testjson = require('./../brandtest.json'); // for Testing

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css'],
  providers: [
    { provide: 'IdField', useValue: 'idfield' },
    { provide: 'url', useValue: 'url' },
    { provide: 'Store', useClass: Store },
    { provide: 'DBDataService', useClass: BrandDBService, deps: [GBHttpService], },
    { provide: 'DataService', useClass: BrandService, deps: ['DBDataService'] },
    {
      provide: 'PageService', useFactory: getThisGBPageService, deps: [Store, 'DataService', 'DBDataService',
        FormBuilder, GBHttpService, ActivatedRoute, Router,],
    },],
})
export class BrandComponent extends GBBaseDataPageStoreComponentWN<IBrand> implements OnInit {
  title = 'BRAND';
  re: RuleEngine;
  form: BrandFormgroupStore = new BrandFormgroupStore(                            //Hide on test
    this.gbps.gbhttp.http,                                                        //Hide on test
    this.gbps.store                                                               //Hide on test
  );                                                                              //Hide on test
  @Select(BrandState) currentData$;
  public currentdatatest = testjson[0];                                //Testing
  public withoutid = testjson[1];                                      //Testing
  public withoutidcode = testjson[2];                                  //Testing
  public nodata = testjson[3];                                         //Testing

  thisConstructor() { }

 
  ngOnInit(): void {
   
    this.re.setContext(this.form);
    let id = '';
    this.gbps.activeroute.params.subscribe((params: any) => {
      id = params['id'];
    });
    this.gbps.activeroute.paramMap.subscribe((params: ParamMap) => {
      id = params.get('id')
    });
    if (this.gbps.activeroute.firstChild) {
      this.gbps.activeroute.firstChild.params.subscribe((params: ParamMap) => {
        id = params['id'];
      });
    }
    this.loadScreen(id);
  }


  loadScreen(recid?: string) {
    if (recid) {
      const id = parseInt(recid);
      this.gbps.store.dispatch(new GetData(id));
    }
    else {
      this.gbps.store.dispatch(new GetAll(null)).subscribe(() => {
        this.gbps.store.dispatch(new MoveFirst());
      });
    }
    let json = datajson;
    return json;
  };

  
}
