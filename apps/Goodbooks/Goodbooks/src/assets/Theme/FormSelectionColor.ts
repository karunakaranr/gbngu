export class FormSelectionColor {
    public static Default = '#ED1152';
    public static Blue = '#dee8fa';
    public static Pink = '#fec4ce';
    public static Red = '#fdd8d6';
    public static Aqua = '#d6fdf9';
    public static Yellow = '#f8fa9b';
    public static Teal  = '#3ffdfd';
    public static Green  = '#4dff4d';
}