import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './AuthGuard/authguard.service';
import { LoginComponent } from './../../../../../features/common/shared/screens/Login/login.component';



const routes: Routes = [
  { path: 'Login', component: LoginComponent },
  { path: 'main', redirectTo: 'GoodBooks', pathMatch: 'full' },
  { path: 'GoodBooks', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/samplescreens/samplescreens.module`).then(m => m.SamplescreensModule) },
  { path: 'WMS', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/WMS/WMS.module`).then(m => m.WMSModule) },
  { path: 'FAM', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/FAM/FAM.module`).then(m => m.FAMModule) },
  { path: 'crmsales', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/crmsales/crmsales.module`).then(m => m.CrmsalesModule) },
  { path: 'maintenance', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/maintenance/maintenance.module`).then(m => m.MaintenanceModule) },
  { path: 'production', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/production/production.module`).then(m => m.ProductionModule) },
  { path: 'projects', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/projects/projects.module`).then(m => m.ProjectModule) },
  { path: 'purchase', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/purchase/purchase.module`).then(m => m.PurchaseModule) },
  { path: 'hrms', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/hrms/hrms.module`).then(m => m.HrmsModule) },
  { path: 'inventory', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/inventory/inventory.module`).then(m => m.InventoryModule) },
  { path: 'sales', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/sales/sales.module`).then(m => m.SalesModule) },
  { path: 'finance', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/finance/finance.module`).then(m => m.FinanceModule) },
  { path: 'admin', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/admin/admin.module`).then(m => m.AdminModule) },
  { path: 'analytic', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/analytics/analytics.module`).then(m => m.AnalyticsModule) },
  { path: 'facility', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/facility/facility.module`).then(m => m.facilityModule) },
  { path: 'costing', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/costing/costing.module`).then(m => m.CostingModule) },
  { path: 'framework', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/framework/framework.module`).then(m => m.FrameWorkModule) },
  { path: '', canActivate: [AuthGuard], redirectTo: 'main', pathMatch: 'full' },
  { path: 'crmservice', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/crmservice/crmservice.module`).then(m => m.CRMServiceModule) },
  { path: 'planning', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/planning/planning.module`).then(m => m.PlanningModule) },
  { path: 'logistics', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/Logistics/logistics.module`).then(m => m.logisticsModule) },
  { path: 'Fabric', canActivate: [AuthGuard], loadChildren: () => import(`./../../../../../features/erpmodules/Fabric/fabric.module`).then(m => m.FabricModule) }


];

@NgModule({
  imports: [RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
