// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/angular/types-6-0';
import { GbInputComponent } from '../../../../libs/uifwk/uifwkhtml/src/lib/gb-input.component';

export default {
  title: 'Design systems/Atoms/HTML/Input',
  component: GbInputComponent,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<GbInputComponent> = (args: GbInputComponent) => ({
  component: GbInputComponent,
  props: args,
});

export const Simple = Template.bind({});
Simple.args = {
  primary: true,
  label: 'Button',
};
//...