// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/angular/types-6-0';
import { GbAddressComponent } from '../../../../libs/uifwk/uifwkhtml/src/lib/gb-address.component';

export default {
  title: 'Design systems/Atoms/HTML/Address',
  component: GbAddressComponent,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<GbAddressComponent> = (args: GbAddressComponent) => ({
  component: GbAddressComponent,
  props: args,
});

export const Simple = Template.bind({});
Simple.args = {
  primary: true,
  label: 'Button',
};
//first change
//second change
//third change (online)
//fourth change (after protecting branch)
//thilak changes to check merge conflict
//sathyan changes
//checking....
//second try conflict
//...