// also exported from '@storybook/angular' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/angular/types-6-0';
import { GbAddressComponent } from '../../../../libs/uifwk/uifwkmaterial/src/lib/gb-address/gb-address.component';
import { UifwkUifwkmaterialModule } from '../../../../libs/uifwk/uifwkmaterial/src/lib/uifwk-uifwkmaterial.module';

export default {
  title: 'Design systems/Atoms/Material/Address',
  component: GbAddressComponent,
  argTypes: {
    backgroundColor: { control: 'color' },
  },
} as Meta;

const Template: Story<GbAddressComponent> = (args: GbAddressComponent) => ({
  component: GbAddressComponent,
  props: args,
});

export const Simple = Template.bind({});
Simple.args = {
  primary: true,
  label: 'Button',
};
